package com.homedepot.di.dl.support.pr4ReplicationScripts.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Properties;

import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage.RecipientType;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;


//import com.datastax.driver.core.PreparedStatement;
import com.homedepot.di.dl.support.util.CipherEncrypter;
import com.homedepot.di.dl.support.util.Constants;
import com.homedepot.di.dl.support.util.DBConnectionUtil;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.core.util.MultivaluedMapImpl;

public class RemoveDupSVCSTATHistOrdersInPR4 {
	private static final Logger logger = Logger
			.getLogger(CheckOrdersInPR4.class);
	private static final Logger oLogger = Logger.getLogger("splunkLog");

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		fixDuplicates();
	}

	public static void fixDuplicates() {

		String msgContent = "";

		HashMap<String, String> duplicateDetailMap = findDuplicateInPR4();// calling
																			// method
																			// which
																			// pulls
																			// records
																			// from
																			// ODS
																			// and
																			// stores
																			// in
																			// a
																			// map


				msgContent = "Please find below orders which were replicated to PR4.<br/><br/>Number of orders having duplicate records: "
								+ duplicateDetailMap.size() + "<br/><br/>";
				msgContent = msgContent
								+ "COM_ORDER,CUSTORDNBR,SVCLINE,TYPE,CODE<br/>";
		
				for (String order : duplicateDetailMap.keySet()) {
					System.out.println("replicateOrdersToPR4="+order);
					replicateOrdersToPR4(order); // Calling method to
					// replicate orders to PR4
					// which having duplicates
					msgContent = msgContent + order + ","
							+ duplicateDetailMap.get(order) + "<br/>";
				}

				System.out.println("Number of records having duplicates : "
						+ duplicateDetailMap.size());
				logger.info("Number of records having duplicates : "
							+ duplicateDetailMap.size());

					
					sendMail(msgContent); // Sending email report
					System.out.println("sent mail");
//					oLogger.info("Program=CheckOrdersInPR4 status=success Records_pulled_from_ODS="
//							+ sterlingDetailMap.size()
//							+ " Records_in_sync_with_PR4_Xref_table="
//							+ pr4DetailMap.size()
//							+ " Missing_records_replicated_to_PR4_Xref_table="
//							+ diffMap.size()
//							+ " Order_details_replicated = "
//							+ diffMap);

			
		

	}


	
	
	public static HashMap<String, String> findDuplicateInPR4() {
		HashMap<String, String> duplicateDetailMap = new HashMap<String, String>();
		final String driverClass = "com.ibm.db2.jcc.DB2Driver";
		final String connectionURLThin = "jdbc:db2://ibdyprb0.sysplex.homedepot.com:5136/PR4";
		//final String hostUserId = "cprjs";
		final String hostUserId = "cpach";

		//CipherEncrypter encrypter = new CipherEncrypter("PR6_Rakesh"); // key
																		// for
																		// encryption
		
		CipherEncrypter encrypter = new CipherEncrypter("PR4_Madhu");

		final String hostPassword = encrypter.decrypt("90gTPcpctlJK6+zJCqSPMA==");
		// final String hostPassword = "Twq4SkKm76XCeDuzlfWpCQ==";

		Connection con = null;

		try {

			Class.forName(driverClass);
			con = DriverManager.getConnection(connectionURLThin, hostUserId,
					hostPassword);


			int i = 1;

			PreparedStatement stmt = null;
			ResultSet rset = null;
			
			String timestamp = getDateForDB2();
			ArrayList storeList = getStoreList();
			String query = " SELECT y.CUST_ORD_NBR,y.SVC_LINE_NBR,y.SVC_TYP_CD,y.SVC_STAT_CD,x.cust_ord_id " 
					+" FROM prhdb.comt_cust_ord_xref x,prhdb.svc_stat_hist y " 
					+" WHERE x.str_nbr = y.str_nbr "
					+" and x.cust_ord_nbr = y.cust_ord_nbr "
					+" and y.SVC_STAT_CD = 100 "
					+" and  y.STR_NBR = ? " 
					+" and y.STAT_EFF_TS > '"+timestamp+"' "
					+" group by y.CUST_ORD_NBR,y.SVC_LINE_NBR,y.SVC_TYP_CD,y.SVC_STAT_CD,x.cust_ord_id "
					+" having count(*) > 1 ";
			
			for (Iterator<String> iter = storeList.iterator(); iter.hasNext(); ) {

				String strNbr = iter.next();
				System.out.println("strNbr="+strNbr);
				stmt = con.prepareStatement(query);
				stmt.setString(1, strNbr);

			
			logger.info("Executing query in PR4 . . .");
			rset = stmt.executeQuery();
			String custOrdNbr="";
			String svcLineNo="";
			String svctypCD="";
			String svcstatCD="";
			String custOrdId="";
			
			while (rset.next()) {
				custOrdNbr = rset.getString(1);
				svcLineNo = rset.getString(2);
				svctypCD = rset.getString(3);
				svcstatCD = rset.getString(4);
				custOrdId = rset.getString(5);
				
				duplicateDetailMap.put(custOrdId, custOrdNbr+","+svcLineNo+","+svctypCD);
				
			}

			rset.close();
			stmt.close();
			}

			logger.info("Total number of records from PR4 : "	+ duplicateDetailMap.size());
			con.close();
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		}
		return duplicateDetailMap;
	}



	


	

	
	public static void replicateOrdersToPR4(String comOrder) {
		// String sterling = null;
		String legacy = null;

		String storeNo =  comOrder.substring(1,5);;// fileName.substring(0,length-4);
		// String timestamp = "13-07-2016";
		int iCount = 0;
		Client client = new Client();
		client.setConnectTimeout(30000);
		client.setReadTimeout(30000);
		ClientResponse clientResponse = null;

		DateTime dt = new DateTime();
		DateTimeFormatter fmtdate = DateTimeFormat.forPattern("MM-dd-yyyy");

		String timestamp = fmtdate.print(dt);
		

		String url = "http://st"+storeNo+".homedepot.com:12130/COS2CODReplicationStr/replicateorders?orders="+comOrder;
		try{
				WebResource webResource = client.resource(url);
				MultivaluedMapImpl formParams = new MultivaluedMapImpl();

				clientResponse = webResource.post(ClientResponse.class,
													formParams);

				
		} catch (Exception e) {
				
				e.printStackTrace();
			}

		
		logger.info("Number of orders replicated to PR4 : " + iCount);
	}

	public static void sendMail(String msgContent) {

		logger.info("CheckOrdersInPR4: Preparing to send email");
		final String user = "horizon@cpliisad.homedepot.com";
		final String password = "xxx";// change accordingly
		String host = "mail1.homedepot.com";
		Properties props = new Properties();
		props.put("mail.host", host);
		Session session = Session.getInstance(props,
				new javax.mail.Authenticator() {
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(user, password);
					}
				});
		try {

			String[] Cc = {"SATHISH_RADHAKRISHNAN@homedepot.com"};
			//String[] to = { "_2fc77b@homedepot.com" };
			String[] to = { "manoj_sadangi@homedepot.com" };
			// String[] Cc = {};
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress(user));
			DateFormat dateFormat = new SimpleDateFormat(
					"MM/dd/yyyy hh:mm:ss a");
			Date today = new Date();
			String date1 = dateFormat.format(today);
			message.setSubject("Orders Replicated to PR4 for SVCSTAT Table for removing duplicates- " + date1);
			// message.setText("This is simple program of sending email using JavaMail API");
			message.setFrom(new InternetAddress(user));
			InternetAddress[] addressTo = new InternetAddress[to.length];
			InternetAddress[] addressCc = new InternetAddress[Cc.length];
			for (int i = 0; i < to.length; i++) {
				addressTo[i] = new InternetAddress(to[i]);
				logger.warn("To Address " + to[i]);
			}
			for (int j = 0; j < Cc.length; j++) {
				addressCc[j] = new InternetAddress(Cc[j]);
				logger.warn("Cc Address " + Cc[j]);
			}
			message.setRecipients(RecipientType.TO, addressTo);
			message.setRecipients(RecipientType.CC, addressCc);
			message.setContent(msgContent, "text/html");
			Transport.send(message);
			for (int i = 0; i < to.length; i++) {
				addressTo[i] = new InternetAddress(to[i]);
				logger.warn("Message delivered to--- " + to[i]);
			}
			for (int j = 0; j < Cc.length; j++) {
				addressCc[j] = new InternetAddress(Cc[j]);
				logger.warn("Message delivered to cc recepients--- " + Cc[j]);
			}

		} catch (MessagingException e) {
			e.printStackTrace();
		}

	}
	
	public static ArrayList getStoreList(){
	    //load all the stores to Arraylist
		ArrayList<String> storeList = new ArrayList<String>();
	    try{
	    		String line = "";
	    		File storefile=new File("storelist.txt");
	    		FileReader fileReader = new FileReader(storefile);
	    		BufferedReader bufferedReader = new BufferedReader(fileReader);
	    		
				while ((line = bufferedReader.readLine()) != null)
				{
					storeList.add(line);
					 
					 
				}
				bufferedReader.close();
				fileReader.close();
	        
	    	}catch(Exception e){
	    		e.printStackTrace();
	    	}
	    return storeList;
	  }	
	
	public static String getDateForDB2() {

		DateTime dt = new DateTime();//2016-11-21 19:54:44
		
		DateTimeFormatter fmtdate = DateTimeFormat.forPattern("YYYY-MM-dd HH:mm:ss");

		String timestamp = fmtdate.print(dt.minusDays(1));
		
		return timestamp;
	}
}
