package com.homedepot.di.dl.support.pr4ReplicationScripts.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.core.util.MultivaluedMapImpl;


public class XREFInsertUtility {
	final static Logger logger = Logger.getLogger(XREFInsertUtility.class);
	
	public static void main(String...args)
	{
		File folder = new File("C:\\Users\\mxs3285\\Documents\\COM Support\\xref");
		File[] listOfFiles = folder.listFiles();
		
		for(int i=0;i<listOfFiles.length;i++)
		{
			File file = listOfFiles[i];
			if(file.isFile() && file.getName().endsWith(".txt"))
			{
				readFileContentAndPushDataToXREF(file);	
			}
		}
		
	}
	
	
	public static void readFileContentAndPushDataToXREF(File file)  {
		 
		
		try{
			String fileName = null;
			int length = 0;
			if(null!=file)
			{
				fileName=file.getName();
				length=fileName.length();
				logger.info("length  is:::::::::"+length);
			
			}
			long startTime = System.currentTimeMillis();
			DateTime dt = new DateTime();
			DateTimeFormatter fmt = DateTimeFormat.forPattern("dd-MMM-yyyy HH:mm:ss");
			String strTm = fmt.print(dt);			
			logger.info(" Start time ="+strTm);
			
			
			//File file = new File("C:\\Users\\axj8041\\Desktop\\0358.txt");
			FileReader fileReader = new FileReader(file);
			BufferedReader bufferedReader = new BufferedReader(fileReader);
			//StringBuilder builder = new StringBuilder();
			String line;
			String sterling = null;
			String legacy = null;
			
		
			String storeNo = "";//fileName.substring(0,length-4);
			String timestamp = "13-07-2016";
			int iCount =0;
			Client client = new Client();
			client.setConnectTimeout(30000);
			client.setReadTimeout(30000);
			ClientResponse clientResponse = null;
			String url="http://multichannel-batch.homedepot.com/COM2PR4ReplicationBatches/ComOrderXRefInsertBatch?start";
			while((line = bufferedReader.readLine()) != null)
            {
					        
			 try {
				String[] lineData=line.split(",");
				 sterling= lineData[0];
				 legacy = lineData[1];
				 //timestamp = lineData[2];
				 //timestamp = timestamp.substring(0, 11);
				 sterling = sterling.trim();
				 legacy=legacy.trim();
				 storeNo = sterling.substring(1,5);
				 iCount++;
				 if(iCount > 500 && (iCount % 500 ==0) ){
					 logger.debug("sleep ..."+1*10*1000);
					 Thread.sleep(1*10*1000);
				 }
				 
				 logger.info(iCount+":sterling:"+sterling+"legacy:"+legacy+"storeNumber:"+storeNo+":timestamp:"+timestamp);
				 
				String xmlXref = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><custordref><custOrdID>"+sterling+
							    			"</custOrdID><custOrdNbr>"+legacy+
							    			"</custOrdNbr><strnbr>"+storeNo+"</strnbr><ordCrtDt>"+timestamp+"</ordCrtDt></custordref>";
				logger.info(xmlXref);
							    	//System.out.println("need to insert .... "+legacy + " => " + sterling +" calling ..."+CommonUtils.getMultichannelURL());
							    	//System.out.println("need to insert .... "+legacy );
							    	
							    	
				
				
				WebResource webResource = client.resource(url);
				MultivaluedMapImpl formParams = new MultivaluedMapImpl();
				
				formParams.add("request", xmlXref);
				clientResponse = webResource.post(ClientResponse.class,
						formParams);
				
				
				
				//getCODDispatch().invokeCRossRefService(xmlXref);
			} catch (Exception e) {
			    //skip whatever records are missing H Numbers in the file..
				e.printStackTrace();
			}
						    	
            }// end of while...
						    	
			//Close the BufferedReader..
			if(null!=bufferedReader)
			{
				bufferedReader.close();
			}
			
			//At the end rename the file to another extension.So that the same file cannot be used..
			file.renameTo(new File("C:\\Users\\axj8041\\Desktop\\Missing_XREF\\"+fileName+".txtp"));
			
			long endTime = System.currentTimeMillis()-startTime;
			logger.info("Time taken for this process is::::::"+endTime);
			
			
					   
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		
		
		
		
/*		Client client = new Client();
		client.setConnectTimeout(30000);
		client.setReadTimeout(30000);
		ClientResponse clientResponse = null;
		String url="http://multichannel-batch.homedepot.com/COM2PR4ReplicationBatches/ComOrderXRefInsertBatch?start";
		
		String xmlXref = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><custordref><custOrdID>"+"H3314-18498"+"</custOrdID><custOrdNbr>"+196184+"</custOrdNbr><strnbr>"+"3314"+"</strnbr><ordCrtDt>"+"05-29-2015"+"</ordCrtDt></custordref>";
		
		WebResource webResource = client.resource(url);
		MultivaluedMapImpl formParams = new MultivaluedMapImpl();
		
		formParams.add("request", xmlXref);
		clientResponse = webResource.post(ClientResponse.class,
				formParams);
		System.out.println("done.........");*/

	}
//
//	
//	public static CODDispatch getCODDispatch()
//	{
//		return CODDispatch.getInstance();
//	}
}
