package com.homedepot.di.dl.support.pr4ReplicationScripts.service;

import java.io.FileReader;
import java.io.FileWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Properties;

import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage.RecipientType;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.homedepot.di.dl.support.util.CipherEncrypter;
import com.homedepot.di.dl.support.util.Constants;
import com.homedepot.di.dl.support.util.DBConnectionUtil;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.core.util.MultivaluedMapImpl;

public class CheckOrdersInPR4 {
	private static final Logger logger = Logger
			.getLogger(CheckOrdersInPR4.class);
	private static final Logger oLogger = Logger.getLogger("splunkLog");

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		checkOrders();
	}

	public static void checkOrders() {

		String msgContent = "";

		HashMap<String, String> sterlingDetailMap = checkOrdersInSterling();// calling
																			// method
																			// which
																			// pulls
																			// records
																			// from
																			// ODS
																			// and
																			// stores
																			// in
																			// a
																			// map

		if (!(sterlingDetailMap.isEmpty())) {

			ArrayList<String> orderList = getSets(sterlingDetailMap);

			HashMap<String, String> pr4DetailMap = checkOrdersInPR4(orderList);// calling
																				// method
																				// which
																				// pulls
																				// records
																				// from
																				// PR4
																				// and
																				// stores
																				// in
																				// a
																				// map

			if (!(pr4DetailMap.isEmpty())) {

				HashMap<String, String> diffMap = new HashMap<String, String>();

				for (String order : sterlingDetailMap.keySet()) {
					if (!(pr4DetailMap.containsKey(order))) {
						diffMap.put(order, sterlingDetailMap.get(order));
					}
				}

				if (!(diffMap.isEmpty())) {

					// String filelocation = "C:\\test\\OrdersToReplicate.txt";
					//
					// try {
					//
					// FileWriter writer = new FileWriter(filelocation);
					//
					// for (String order : diffMap.keySet()) {
					// writer.append(order + "," + diffMap.get(order));
					// writer.append("\n");
					// }
					//
					// writer.flush();
					// writer.close();
					// logger.debug("File created at " + filelocation);
					//
					// } catch (Exception e) {
					// logger.error(e);
					// e.printStackTrace();
					// }

					logger.info("Number of records not in Sync with PR4 : "
							+ diffMap.size());

					replicateOrdersToPR4(diffMap); // Calling method to
													// replicate orders to PR4
													// which are not in sync

					msgContent = "Please find below orders which were replicated to PR4.<br/><br/>Number of orders missing in PR4 Xref table: "
							+ diffMap.size() + "<br/><br/>";
					msgContent = msgContent
							+ "EXTN_HOST_ORDER_REF,EXTN_LEGACY_ORD_REF<br/>";
					for (String record : diffMap.keySet()) {
						msgContent = msgContent + record + ","
								+ diffMap.get(record) + "<br/>";
					}

					sendMail(msgContent); // Sending email report

					oLogger.info("Program=CheckOrdersInPR4 status=success Records_pulled_from_ODS="
							+ sterlingDetailMap.size()
							+ " Records_in_sync_with_PR4_Xref_table="
							+ pr4DetailMap.size()
							+ " Missing_records_replicated_to_PR4_Xref_table="
							+ diffMap.size()
							+ " Order_details_replicated = "
							+ diffMap);

				} else {
					msgContent = "All records pulled from ODS are in sync with PR4.";
					logger.info("All records pulled from ODS are in sync with PR4.");
					sendMail(msgContent); // Sending email report
					
					oLogger.info("Program=CheckOrdersInPR4 status=success Records_pulled_from_ODS="
							+ sterlingDetailMap.size()
							+ " Records_in_sync_with_PR4_Xref_table="
							+ pr4DetailMap.size()
							+ " statusDescription=\"All records pulled from ODS are in sync with PR4\"");
				}

			} else {
				msgContent = "No records pulled from PR4. Possible connection issues. Please check with COM Support Team.";
				logger.error("No records pulled from PR4. Possible connection issues. Please check with COM Support Team.");
				sendMail(msgContent); // Sending email report
				
				oLogger.info("Program=CheckOrdersInPR4 status=failure statusDescription=\"No records pulled from PR4. Possible connection issues.\"");
			}

		} else {
			msgContent = "No records pulled from ODS. Possible connection issues. Please check with COM Support Team.";
			logger.error("No records pulled from ODS. Possible connection issues. Please check with COM Support Team.");
			sendMail(msgContent); // Sending email report
			
			oLogger.info("Program=CheckOrdersInPR4 status=failure statusDescription=\"No records pulled from ODS. Possible connection issues.\"");
		}

	}

	public static HashMap<String, String> checkOrdersInSterling() {
		HashMap<String, String> sterlingDetailMap = new HashMap<String, String>();
		try {
			DBConnectionUtil dbConn = new DBConnectionUtil();
			Connection con = dbConn
					.getJNDIConnection(Constants.ODS_STAND_BY_JNDI);

			int i = 1;

			Statement stmt = null;
			ResultSet rset = null;

			stmt = con.createStatement();

			String query = " SELECT EXTN_HOST_ORDER_REF,EXTN_LEGACY_ORD_REF "
					+ " FROM YFS_ORDER_HEADER A " + " WHERE 1=1 "
					+ " AND A.EXTN_HOST_SRC_PROCESS IN (0, 32) "
					+ " AND A.DOCUMENT_TYPE='0001'  "
					+ " AND A.EXTN_LEGACY_ORD_REF is not null  "
					+ " AND A.DRAFT_ORDER_FLAG != 'Y'  "
					+ " and A.modifyts >= (sysdate-1) ";
			logger.info("Executing query in ODS . . .");
			rset = stmt.executeQuery(query);
			while (rset.next()) {
				sterlingDetailMap.put(rset.getString(1).trim(),
						rset.getString(2).trim());
			}

			rset.close();
			stmt.close();

			logger.info("Total number of records from ODS : "
					+ sterlingDetailMap.size());
			con.close();
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		}
		return sterlingDetailMap;
	}

	public static HashMap<String, String> checkOrdersInPR4(
			ArrayList<String> orderList) {

		HashMap<String, String> pr4DetailMap = new HashMap<String, String>();

		final String driverClass = "com.ibm.db2.jcc.DB2Driver";
		final String connectionURLThin = "jdbc:db2://ibdyprb0.sysplex.homedepot.com:5136/PR4";
		//final String hostUserId = "cprjs";
		final String hostUserId = "cpach";

		//CipherEncrypter encrypter = new CipherEncrypter("PR6_Rakesh"); // key
																		// for
																		// encryption
		
		CipherEncrypter encrypter = new CipherEncrypter("PR4_Madhu");

		final String hostPassword = encrypter.decrypt("90gTPcpctlJK6+zJCqSPMA==");
		// final String hostPassword = "Twq4SkKm76XCeDuzlfWpCQ==";

		Connection con = null;

		try {

			Class.forName(driverClass);
			con = DriverManager.getConnection(connectionURLThin, hostUserId,
					hostPassword);
			int i = 1;
			logger.info("Executing query in PR4 . . .");
			for (String set : orderList) {

				Statement stmt = null;
				ResultSet rset = null;
				String query = null;
				stmt = con.createStatement();
				query = "select distinct cust_ord_id, cust_ord_nbr from prhdb.comt_cust_ord_xref where cust_ord_id in ("
						+ set + ")  ";
				// logger.info(query);
				rset = stmt.executeQuery(query);
				while (rset.next()) {
					pr4DetailMap.put(rset.getString(1).trim(), rset
							.getString(2).trim());
				}
				rset.close();
				stmt.close();
				logger.info("PR4 Sets completed so far " + i + " out of "
						+ orderList.size());
				i++;
			}
			logger.info("Total number of records from PR4 : "
					+ pr4DetailMap.size());
			con.close();

		} catch (Exception e) {
			logger.error("Exception occured: " + e);
			e.printStackTrace();
		}

		return pr4DetailMap;
	}

	public static ArrayList<String> getSets(
			HashMap<String, String> sterlingDetailMap) {
		ArrayList<String> orderList = new ArrayList<String>();

		long i = 1;
		String set = "";

		for (String order : sterlingDetailMap.keySet()) {
			set = set + "'" + order + "',";
			if (i % 30000 == 0) {
				set = set.substring(0, (set.length() - 1));
				orderList.add(set);
				set = "";
			}
			i++;
		}
		set = set.substring(0, (set.length() - 1));
		orderList.add(set);

		return orderList;
	}

	public static void replicateOrdersToPR4(HashMap<String, String> diffMap) {
		// String sterling = null;
		String legacy = null;

		String storeNo = "";// fileName.substring(0,length-4);
		// String timestamp = "13-07-2016";
		int iCount = 0;
		Client client = new Client();
		client.setConnectTimeout(30000);
		client.setReadTimeout(30000);
		ClientResponse clientResponse = null;

		DateTime dt = new DateTime();
		DateTimeFormatter fmtdate = DateTimeFormat.forPattern("MM-dd-yyyy");

		String timestamp = fmtdate.print(dt);

		String url = "http://multichannel-batch.homedepot.com/COM2PR4ReplicationBatches/ComOrderXRefInsertBatch?start";
		for (String sterling : diffMap.keySet()) {

			try {
				// String[] lineData=line.split(",");
				// sterling= lineData[0];
				legacy = diffMap.get(sterling);
				// timestamp = lineData[2];
				// timestamp = timestamp.substring(0, 11);
				sterling = sterling.trim();
				legacy = legacy.trim();
				storeNo = sterling.substring(1, 5);
				iCount++;
				if (iCount > 500 && (iCount % 500 == 0)) {
					logger.info("sleep ..." + 1 * 10 * 1000);
					Thread.sleep(1 * 10 * 1000);
				}

				logger.debug(iCount + ":sterling:" + sterling + "legacy:"
						+ legacy + "storeNumber:" + storeNo + ":timestamp:"
						+ timestamp);

				String xmlXref = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><custordref><custOrdID>"
						+ sterling
						+ "</custOrdID><custOrdNbr>"
						+ legacy
						+ "</custOrdNbr><strnbr>"
						+ storeNo
						+ "</strnbr><ordCrtDt>"
						+ timestamp
						+ "</ordCrtDt></custordref>";
				logger.debug(xmlXref);
				// System.out.println("need to insert .... "+legacy + " => " +
				// sterling +" calling ..."+CommonUtils.getMultichannelURL());
				// System.out.println("need to insert .... "+legacy );

				WebResource webResource = client.resource(url);
				MultivaluedMapImpl formParams = new MultivaluedMapImpl();

				formParams.add("request", xmlXref);
				clientResponse = webResource.post(ClientResponse.class,
						formParams);

				// getCODDispatch().invokeCRossRefService(xmlXref);
			} catch (Exception e) {
				// skip whatever records are missing H Numbers in the file..
				e.printStackTrace();
			}

		}// end of for...
		logger.info("Number of orders replicated to PR4 : " + iCount);
	}

	public static void sendMail(String msgContent) {

		logger.info("CheckOrdersInPR4: Preparing to send email");
		final String user = "horizon@cpliisad.homedepot.com";
		final String password = "xxx";// change accordingly
		String host = "mail1.homedepot.com";
		Properties props = new Properties();
		props.put("mail.host", host);
		Session session = Session.getInstance(props,
				new javax.mail.Authenticator() {
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(user, password);
					}
				});
		try {

			String[] Cc = {};
			String[] to = { "_2fc77b@homedepot.com" };
			// String[] Cc = {};
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress(user));
			DateFormat dateFormat = new SimpleDateFormat(
					"MM/dd/yyyy hh:mm:ss a");
			Date today = new Date();
			String date1 = dateFormat.format(today);
			message.setSubject("Orders Replicated to PR4 Xref Table- " + date1);
			// message.setText("This is simple program of sending email using JavaMail API");
			message.setFrom(new InternetAddress(user));
			InternetAddress[] addressTo = new InternetAddress[to.length];
			InternetAddress[] addressCc = new InternetAddress[Cc.length];
			for (int i = 0; i < to.length; i++) {
				addressTo[i] = new InternetAddress(to[i]);
				logger.warn("To Address " + to[i]);
			}
			for (int j = 0; j < Cc.length; j++) {
				addressCc[j] = new InternetAddress(Cc[j]);
				logger.warn("Cc Address " + Cc[j]);
			}
			message.setRecipients(RecipientType.TO, addressTo);
			message.setRecipients(RecipientType.CC, addressCc);
			message.setContent(msgContent, "text/html");
			Transport.send(message);
			for (int i = 0; i < to.length; i++) {
				addressTo[i] = new InternetAddress(to[i]);
				logger.warn("Message delivered to--- " + to[i]);
			}
			for (int j = 0; j < Cc.length; j++) {
				addressCc[j] = new InternetAddress(Cc[j]);
				logger.warn("Message delivered to cc recepients--- " + Cc[j]);
			}

		} catch (MessagingException e) {
			e.printStackTrace();
		}

	}

}
