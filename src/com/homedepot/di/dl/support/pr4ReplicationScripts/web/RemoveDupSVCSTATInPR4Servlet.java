package com.homedepot.di.dl.support.pr4ReplicationScripts.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.homedepot.di.dl.support.pr4ReplicationScripts.service.CheckOrdersInPR4;
import com.homedepot.di.dl.support.pr4ReplicationScripts.service.RemoveDupSVCSTATHistOrdersInPR4;

/**
 * Servlet implementation class RemoveDupSVCSTATInPR4Servlet
 */
public class RemoveDupSVCSTATInPR4Servlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = Logger
			.getLogger(RemoveDupSVCSTATInPR4Servlet.class);
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RemoveDupSVCSTATInPR4Servlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		logger.debug("RemoveDupSVCSTATInPR4Servlet Program Started at "+java.util.GregorianCalendar.getInstance().getTime());	
		RemoveDupSVCSTATHistOrdersInPR4.fixDuplicates();
		logger.debug("RemoveDupSVCSTATInPR4Servlet Program Completed at "+java.util.GregorianCalendar.getInstance().getTime());
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
