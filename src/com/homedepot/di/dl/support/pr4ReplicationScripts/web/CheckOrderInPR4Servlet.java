package com.homedepot.di.dl.support.pr4ReplicationScripts.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.homedepot.di.dl.support.pr4ReplicationScripts.service.CheckOrdersInPR4;

/**
 * Servlet implementation class CheckOrderInPR4Servlet
 */
public class CheckOrderInPR4Servlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = Logger
			.getLogger(CheckOrderInPR4Servlet.class);
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CheckOrderInPR4Servlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		logger.debug("CheckOrderInPR4Servlet Program Started at "+java.util.GregorianCalendar.getInstance().getTime());	
		CheckOrdersInPR4.checkOrders();
		logger.debug("CheckOrderInPR4Servlet Program Completed at "+java.util.GregorianCalendar.getInstance().getTime());
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
