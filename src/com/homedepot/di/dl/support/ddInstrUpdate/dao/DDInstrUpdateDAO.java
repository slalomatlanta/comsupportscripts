package com.homedepot.di.dl.support.ddInstrUpdate.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.homedepot.di.dl.support.util.Constants;
import com.homedepot.di.dl.support.util.DBConnectionUtil;

public class DDInstrUpdateDAO extends Constants {
	private static final Logger logger = Logger
			.getLogger(DDInstrUpdateDAO.class);

	public void ddInstrUpdate() {
		DBConnectionUtil dbConn = new DBConnectionUtil();
		String instrKeyString = "";
		Connection sscConn = dbConn.getJNDIConnection(STERLING_SSC_RO_JNDI);
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList<String> instrKeyArr = null;
		try {
			stmt = sscConn.createStatement();
			logger.debug("Executing query: " + DD_INSTR_SELECT_UPDATE);
			rs = stmt.executeQuery(DD_INSTR_SELECT_UPDATE);
			instrKeyArr = new ArrayList<String>();
			while (rs.next()) {
				//instrKeyString = instrKeyString + ",'" + rs.getString(1) + "'";
				instrKeyArr.add(rs.getString(1));
			}
			rs.close();
			stmt.close();
			sscConn.close();
		} catch (SQLException e) {
			logger.debug("Caught exception while executing query" + e);
		}

		// String updateQuery = String.format(DD_INSTR_UPDATE,
		// instrKeyString.substring(1));
		int rows=0;
		Connection atcConn = dbConn.getJNDIConnection(STERLING_ATC_RW_JNDI);
		try {
			stmt = atcConn.createStatement();
			
			for (String updateQuery : instrKeyArr) {
				logger.debug("Executing query: " + updateQuery);

				rows += stmt.executeUpdate(updateQuery);

			}
			logger.debug("Rows updated " + rows);
			stmt.close();
			atcConn.close();
		} catch (SQLException e) {
			logger.debug("Caught exception while executing update" + e);
		}

	}
}
