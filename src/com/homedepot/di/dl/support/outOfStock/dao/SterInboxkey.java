package com.homedepot.di.dl.support.outOfStock.dao;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;

import javax.mail.*;
import javax.activation.*;
import javax.mail.internet.*;
import javax.mail.internet.MimeMessage.RecipientType;

import org.apache.log4j.Logger;

import com.homedepot.di.dl.support.outOfStock.web.OutOfStock;
import com.homedepot.di.dl.support.util.Constants;
import com.homedepot.di.dl.support.util.DBConnectionUtil;


public class SterInboxkey {

	/**
	 * @param args
	 */
	
	final static Logger logger = Logger.getLogger(SterInboxkey.class);

		public static void main(String[] args) throws SQLException, IOException, Exception {
			deleteUnclosedInboxKeys();
		}
		
		
			
			 public static  void deleteUnclosedInboxKeys() throws 
			Exception {
		StringBuilder emailText = new StringBuilder();
		int totalRowsDeleted=0;
		int rowsDeleted=0;
		//String outputFile = "C:\\Store\\InboxKey.csv";
		String outputFile = "/opt/isv/tomcat-6.0.18/temp/InboxKeyWorkList.csv";
			try {
	
			FileWriter writer = new FileWriter(outputFile);

			SterInboxkey ikey=new SterInboxkey();
						
			DBConnectionUtil dbConn = new DBConnectionUtil();
			
			Connection sterCon = dbConn.getJNDIConnection(Constants.STERLING_SSC_RO_JNDI);
			
			emailText.append(System.getProperty("line.separator"));

			Statement state = null;
			state = sterCon.createStatement();
			ResultSet result = null;
			String Query = "select * from thd01.hd_inbox where INBOX_KEY in " 
							+ "(select i.INBOX_KEY from thd01.hd_inbox i, yfs_order_line l where i.inbox_key > '20150625' and EXCEPTION_TYPE='DS_250' and l.ORDER_LINE_KEY=i.ORDER_LINE_KEY "
							+ "and l.PROMISED_APPT_START_DATE > (sysdate +4))";

			
			result = state.executeQuery(Query);

			writer.append("InboxKey");
			writer.append(',');
			writer.append("Generated ON");
			writer.append(',');
			writer.append("ShipNodeKey");
			writer.append(',');
			writer.append("ExceptionType");
			writer.append(',');
			writer.append("Status");
			writer.append(',');
			writer.append("FollowupDate");
			writer.append(',');
			writer.append("ErrorReason");
			writer.append(',');
			writer.append("CreateTs");
			writer.append(',');
			writer.append("ModifyTs");
			writer.append(',');
			writer.append("CreateUserId");
			writer.append(',');
			writer.append("ModifyUserId");
			writer.append(',');
			writer.append("CreateProgId");
			writer.append(',');
			writer.append("ModifyProgId");
			writer.append(',');
			writer.append("OrderHeaderKey");
			writer.append(',');
			writer.append("OrderNo");
			writer.append(',');
			writer.append("OrderLineKey");
			writer.append(',');
			writer.append("WorkOrderKey");
			writer.append(',');
			writer.append("WorkOrderNo");
			writer.append(',');
			writer.append("ServiceType");
			writer.append(',');
			writer.append("POHeaderKey");
			writer.append(',');
			writer.append("HDWillCallKey");
			writer.append(',');
			writer.append("AlertTransLevelType");
			writer.append(',');
			writer.append("WorkOrderType");
			writer.append(',');
			writer.append("CustomWorkOrderRef");
			writer.append(',');
			writer.append("ROHdrKey");
			writer.append(',');
			writer.append("ROLineKey");
			writer.append(',');
			writer.append("TagId");
			writer.append(',');
			writer.append("SellerOrganisationCode");
			writer.append('\n');
             
			while (result.next()) {
				
				writer.append("'"+result.getString(1));
				writer.append(',');
				writer.append(result.getString(2));
				writer.append(',');
				writer.append(result.getString(3));
				writer.append(',');
				writer.append(result.getString(4));
				writer.append(',');
				writer.append(result.getString(5));
				writer.append(',');
				writer.append(result.getString(6));
				writer.append(',');
				writer.append(result.getString(7));
				writer.append(',');
				writer.append(result.getString(8));
				writer.append(',');
				writer.append(result.getString(9));
				writer.append(',');
				writer.append(result.getString(10));
				writer.append(',');
				writer.append(result.getString(11));
				writer.append(',');
				writer.append(result.getString(12));
				writer.append(',');
				writer.append(result.getString(13));
				writer.append(',');
				writer.append("'"+result.getString(14));
				writer.append(',');
				writer.append(result.getString(15));
				writer.append(',');
				writer.append("'"+result.getString(16));
				writer.append(',');
				writer.append("'"+result.getString(17));
				writer.append(',');
				writer.append(result.getString(18));
				writer.append(',');
				writer.append(result.getString(19));
				writer.append(',');
				writer.append("'"+result.getString(20));
				writer.append(',');
				writer.append("'"+result.getString(21));
				writer.append(',');
				writer.append(result.getString(22));
				writer.append(',');
				writer.append(result.getString(23));
				writer.append(',');
				writer.append(result.getString(24));
				writer.append(',');
				writer.append("'"+result.getString(25));
				writer.append(',');
				writer.append("'"+result.getString(26));
				writer.append(',');
				writer.append(result.getString(27));
				writer.append(',');
				writer.append(result.getString(28));
				writer.append(',');
				writer.append('\n');

			
				 try
					{	 

			  
			
			DBConnectionUtil dbConn1 = new DBConnectionUtil();
    		
    		Connection con1 = dbConn1.getJNDIConnection(Constants.STERLING_ATC_RW_JNDI);
						         
			

			final String delQuery = "delete from thd01.hd_inbox where INBOX_KEY in '" +result.getString(1) + "'";
			
			Statement stmt1 = null;
			stmt1 = con1.createStatement();
			rowsDeleted = stmt1.executeUpdate(delQuery);
            totalRowsDeleted = totalRowsDeleted + rowsDeleted;
            con1.commit();
			stmt1.close();
			con1.close();
			
			
						
			} catch (Exception e) {
	             // TODO Auto-generated catch block
				logger.debug("SterInboxKey Exception - Update part : " + e.getMessage());
	             
	      }
			}
			result.close();
			state.close();
			sterCon.close();
			
			writer.flush();
			writer.close();
			}
			catch(Exception e)
			{
				logger.debug("SterInboxKey Exception : " + e.getMessage());
			}
			 

		    if(totalRowsDeleted > 1)
		    {
		    	
		    	sendAttachment(totalRowsDeleted);
		    }
		    
		    else
		    {
		    	logger.info("SterInboxKey : No records found to delete");
		    }
			

			
	}
		
		

		public static void sendAttachment(int rowsDeleted) throws Exception{String host = "mail1.homedepot.com";
		  String from = "horizon@cpliisad.homedepot.com";
		 		  //String[] Cc ={"_275769@homedepot.com"};
		  String[] to = {"_2fc77b@homedepot.com"};
		  
		  //String[] to ={"_275769@homedepot.com"};
		  // Get system properties
		  Properties properties = System.getProperties();

		  // Setup mail server
		  properties.setProperty("mail.smtp.host", host);

		  // Get the default Session object.
		  Session session = Session.getDefaultInstance(properties);

		  // Define message
		  Message message = new MimeMessage(session);
		  message.setFrom(new InternetAddress(from));
		  InternetAddress[] addressTo = new InternetAddress[to.length];
		  //InternetAddress[] addressCc = new InternetAddress[Cc.length];
		  for (int i = 0; i < to.length; i++) 
		  { addressTo[i] = new InternetAddress(to[i]); 
		  System.out.println("To Address "+to[i]);
		  } 
		  /*for (int j = 0; j < Cc.length; j++) 
		  { addressCc[j] = new InternetAddress(Cc[j]); 
		  System.out.println("Cc Address "+Cc[j]);
		  }*/
		  message.setRecipients(RecipientType.TO, addressTo); 
		  //message.setRecipients(RecipientType.CC, addressCc); 
		  message.setSubject("Script for Handling the Out of stock work list");

		  // Create the message part 
		  BodyPart messageBodyPart = new MimeBodyPart();
		  String msgContent = "Hi All,<br/><br/>PFA report for inbox keys which were deleted today.<br/>Number of rows deleted: "+rowsDeleted+"<br/><br/>Thanks<br/>";
	      msgContent = msgContent + "COM Multichannel Support";
		  // Fill the message
		  messageBodyPart.setContent(msgContent ,"text/html");
		  	

		  Multipart multipart = new MimeMultipart();
		  multipart.addBodyPart(messageBodyPart);

		  // Part two is attachment
		  messageBodyPart = new MimeBodyPart();
		  //String fileName = "C:\\Store\\InboxKey.csv";
		  String fileName = "/opt/isv/tomcat-6.0.18/temp/InboxKeyWorkList.csv";
		  DataSource source = new FileDataSource(fileName);
		  messageBodyPart.setDataHandler(new DataHandler(source));
		  messageBodyPart.setFileName(source.getName());
		  multipart.addBodyPart(messageBodyPart);

		  // Put parts in message
		  message.setContent(multipart);

		  // Send the message
		  Transport.send(message);
		   System.out.println("Msg Send ...."); }

	
	
}
