package com.homedepot.di.dl.support.outOfStock.web;

import java.io.IOException;
import java.sql.SQLException;
import java.util.GregorianCalendar;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.homedepot.di.dl.support.outOfStock.dao.*;



//import di.dl.com.outofstock.dao.SterInboxKey;

/**
 * Servlet implementation class OutOfStock
 */
public class OutOfStock extends HttpServlet {
	private static final long serialVersionUID = 1L;
	final static Logger logger = Logger.getLogger(OutOfStock.class);
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public OutOfStock() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		logger.info("OutOfStockServlet :: Started "+GregorianCalendar.getInstance().getTime());
		SterInboxkey inbox = new SterInboxkey();
		try {
			SterInboxkey.deleteUnclosedInboxKeys();
		} catch (SQLException e) {
			logger.info("OutOfStockServlet Exception ::"+GregorianCalendar.getInstance().getTime()+e);
			e.printStackTrace();
		} catch (Exception e) {
			logger.info("OutOfStockServlet Exception ::"+GregorianCalendar.getInstance().getTime()+e);
			e.printStackTrace();
	}// TODO Auto-generated method stub
		
		logger.info("OutOfStockServlet :: Completed "+GregorianCalendar.getInstance().getTime());
	}

}
