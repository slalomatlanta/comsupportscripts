package com.homedepot.di.dl.support.compareDataReplication.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import com.homedepot.di.dl.support.compareDataReplication.dao.CompareDataReplicationDao;
import com.homedepot.di.dl.support.util.ConstantsCompareDataReplication;

public class CompareDataReplicationUtil {
	
	final static Logger logger = Logger.getLogger(CompareDataReplicationUtil.class);
	
	CompareDataReplicationDao cdrd = new CompareDataReplicationDao();
	
	private List <CompareDataReplicationPojo> warnList = new ArrayList<CompareDataReplicationPojo>();
	private List <CompareDataReplicationPojo> criticalList = new ArrayList<CompareDataReplicationPojo>();
	
	private boolean isWarn;
	private boolean isCritical;
	
	long minDifference;
	
	long secDifference;
	
	
	

	public List<CompareDataReplicationPojo> compareReplicationData() {
		
		logger.info("Executing compareReplicationData method");
	
		List <CompareDataReplicationPojo> resultList = new ArrayList<CompareDataReplicationPojo>();
		
		executeThreads();
		
		try {
			
			cdrd.OrderHeader.join();
			cdrd.baseCommonCode.join();
			cdrd.chargeCategory.join();
			cdrd.chargeName.join();
			cdrd.commonCode.join();
			cdrd.commonCodeAttribute.join();	
			cdrd.enterprise.join();
			cdrd.errorCode.join();
			cdrd.holdType.join();				
			cdrd.inventoryItem.join();
			cdrd.inventoryNodeControl.join();
			cdrd.inventorySupplyType.join();			
			cdrd.item.join();			
			cdrd.itemAlias.join();
			cdrd.itemNodeDefn.join();
			cdrd.itemUom.join();
			cdrd.lineRelationshipType.join();
			cdrd.orderLineType.join();
			cdrd.organization.join();	
			cdrd.orgEnterprise.join();
			cdrd.paymentType.join();
			cdrd.scac.join();			
			cdrd.scacAndService.join();			
			cdrd.shipNode.join();
			cdrd.status.join();
			cdrd.uom.join();			
			cdrd.uomConversion.join();			
			cdrd.hdInbox.join();
			cdrd.orderLine.join();
			cdrd.hdWCInstruction.join();
			cdrd.personInfo.join();
			cdrd.hdWillCall.join();	
			cdrd.hdSplOrd.join();
			cdrd.workOrder.join();			
			cdrd.hdRelatedWorkOrder.join();		
			cdrd.inboxReference.join();	
			cdrd.instructionDetail.join();		
			cdrd.hdEvent.join();
			
		} catch (InterruptedException e1) {
		
			e1.printStackTrace();
			logger.error(e1.getMessage());
			
		}
		
		
		int counter = 0;
		
		for(CompareDataReplicationPojo x: cdrd.getResultList()){

			SimpleDateFormat format;

			String sterlingKey = x.getSterlingKey();
			String odsKey = x.getOdsKey();
			
			format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			
			sterlingKey =	sterlingKey.substring(0, 19);
			odsKey = odsKey.substring(0, 19);

			cdrd.getResultList().get(counter).setSterlingKey(sterlingKey);
			cdrd.getResultList().get(counter).setOdsKey(odsKey);
			
			if (x.getTableName().equals("YFS_ORDER_HEADER")){
				
				 format = new SimpleDateFormat("yyyyMMddHHmmss");
				
				sterlingKey =	sterlingKey.substring(0, 14);	
				
				String sterlingYear = sterlingKey.substring(0, 4);	
				
				String sterlingMonth = sterlingKey.substring(4, 6);	
				
				String sterlingDay = sterlingKey.substring(6, 8);
				
				String sterlingHour = sterlingKey.substring(8, 10);	
				
				String sterlingMinutes = sterlingKey.substring(10, 12);	
				
				String sterlingSeconds = sterlingKey.substring(12, 14);	
				
				odsKey = odsKey.substring(0, 14);
				
				String odsYear = odsKey.substring(0, 4);	
				
				String odsMonth = odsKey.substring(4, 6);	
				
				String odsDay =	odsKey.substring(6, 8);
				
				String odsHour = odsKey.substring(8, 10);	
				
				String odsMinutes =	odsKey.substring(10, 12);	
				
				String odsSeconds =	odsKey.substring(12, 14);	
								
				cdrd.getResultList().get(counter).setSterlingKey(sterlingYear +"-"+ sterlingMonth +"-"+ sterlingDay+" "+sterlingHour+":"+sterlingMinutes+":"+sterlingSeconds);
				cdrd.getResultList().get(counter).setOdsKey(odsYear +"-"+ odsMonth +"-"+ odsDay+" "+odsHour+":"+odsMinutes+":"+odsSeconds);

			}	
		
		Date sterlingDate = null;
		Date odsDate = null;

		
		try {
			sterlingDate = format.parse(sterlingKey);
			odsDate = format.parse(odsKey);
			
		} catch (ParseException e) {
			
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		
		String status = "";
		
		if(sterlingDate.compareTo(odsDate)==0){
			
			 status = "ODS Replication is in sync";
			 
		}else if(sterlingDate.compareTo(odsDate)>0){
			
			 status = "Delay in the ODS Replication Data";
			 
			 
		}else if(sterlingDate.compareTo(odsDate)<0){
			
			 status = "Manual Change in ODS Replication Data";
			 
		}
		
		cdrd.getResultList().get(counter).setStatus(status);
		
		 minDifference = delayMin(sterlingDate, odsDate);
		
		 secDifference = delaySec(sterlingDate, odsDate);
		
		 
		 cdrd.getResultList().get(counter).setDelayMin(minDifference);
		 cdrd.getResultList().get(counter).setDelaySec(secDifference);
		
		 
		 String minDiffString = String.valueOf(minDifference);
		 String secDiffString = String.valueOf(secDifference);
		 
		if(minDiffString.contains("-") ){
			
			minDiffString = minDiffString.replace("-", "");
			
			minDifference = Long.valueOf(minDiffString);
					
		}
		
		
		if( secDiffString.contains("-") ){
			
			secDiffString = secDiffString.replace("-", "");
			secDifference = Long.valueOf(secDiffString);
			
		}
		
		
		if(secDifference > ConstantsCompareDataReplication.MAX_WARN_SECONDS && secDifference < ConstantsCompareDataReplication.MAX_CRITICAL_SECONDS ){
			
			
			cdrd.getResultList().get(counter).setWarningYellow(true);
			setWarn(true);
			
			warnList.add(x);
			
		}else if(secDifference > ConstantsCompareDataReplication.MAX_CRITICAL_SECONDS){
			
			
			cdrd.getResultList().get(counter).setWarningRed(true);
			
			setCritical(true);
			criticalList.add(x);
		}
		
			resultList.add(x);
			
			counter++;
		
		}

		
		return resultList;

	}

	public void executeThreads() {
		
			cdrd.inboxReference.start();	
			cdrd.instructionDetail.start();
			cdrd.hdEvent.start();
			cdrd.OrderHeader.start();
			cdrd.baseCommonCode.start();
			cdrd.chargeCategory.start();
			cdrd.chargeName.start();
			cdrd.commonCode.start();
			cdrd.commonCodeAttribute.start();	
			cdrd.enterprise.start();
			cdrd.errorCode.start();
			cdrd.holdType.start();					
			cdrd.inventoryItem.start();
			cdrd.inventoryNodeControl.start();
			cdrd.inventorySupplyType.start();			
			cdrd.item.start();			
			cdrd.itemAlias.start();
			cdrd.itemNodeDefn.start();
			cdrd.itemUom.start();
			cdrd.lineRelationshipType.start();
			cdrd.orderLineType.start();
			cdrd.organization.start();	
			cdrd.orgEnterprise.start();
			cdrd.paymentType.start();
			cdrd.scac.start();			
			cdrd.scacAndService.start();			
			cdrd.shipNode.start();
			cdrd.status.start();
			cdrd.uom.start();			
			cdrd.uomConversion.start();			
			cdrd.hdInbox.start();
			cdrd.orderLine.start();
			cdrd.hdWCInstruction.start();
			cdrd.personInfo.start();
			cdrd.hdWillCall.start();	
			cdrd.hdSplOrd.start();
			cdrd.workOrder.start();			
			cdrd.hdRelatedWorkOrder.start();
		
		}	

	private long delayMin(Date sterlingDate, Date odsDate) {

		long diff = sterlingDate.getTime() - odsDate.getTime();
		long diffMinutes = diff / (60 * 1000);

		return diffMinutes;
	}

	private long delaySec(Date sterlingDate, Date odsDate) {

		long diff = sterlingDate.getTime() - odsDate.getTime();
		long diffSeconds = diff / 1000;

		return diffSeconds;
	}
	 

	public boolean isWarn() {
		return isWarn;
	}

	public void setWarn(boolean isWarn) {
		this.isWarn = isWarn;
	}

	public boolean isCritical() {
		return isCritical;
	}

	public void setCritical(boolean isCritical) {
		this.isCritical = isCritical;
	}

	public List<CompareDataReplicationPojo> getWarnList() {
		return warnList;
	}

	public void setWarnList(List<CompareDataReplicationPojo> warnList) {
		this.warnList = warnList;
	}

	public List<CompareDataReplicationPojo> getCriticalList() {
		return criticalList;
	}

	public void setCriticalList(List<CompareDataReplicationPojo> criticalList) {
		this.criticalList = criticalList;
	}
	 
	 
}
