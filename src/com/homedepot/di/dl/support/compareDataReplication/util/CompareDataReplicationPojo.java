package com.homedepot.di.dl.support.compareDataReplication.util;

public class CompareDataReplicationPojo {
	
	public String tableName;
	public String sterlingKey;
	public String odsKey;
	public String Status;
	public long delayMin;
	public long delaySec;
	public boolean warningYellow;
	public boolean warningRed;
	
	
	public String getTableName() {
		return tableName;
	}
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	public String getSterlingKey() {
		return sterlingKey;
	}
	public void setSterlingKey(String sterlingKey) {
		this.sterlingKey = sterlingKey;
	}
	public String getOdsKey() {
		return odsKey;
	}
	public void setOdsKey(String odsKey) {
		this.odsKey = odsKey;
	}
	public String getStatus() {
		return Status;
	}
	public void setStatus(String status) {
		Status = status;
	}
	public long getDelayMin() {
		return delayMin;
	}
	public void setDelayMin(long delayMin) {
		this.delayMin = delayMin;
	}
	public long getDelaySec() {
		return delaySec;
	}
	public void setDelaySec(long delaySec) {
		this.delaySec = delaySec;
	}
	public boolean isWarningYellow() {
		return warningYellow;
	}
	public void setWarningYellow(boolean warningYellow) {
		this.warningYellow = warningYellow;
	}
	public boolean isWarningRed() {
		return warningRed;
	}
	public void setWarningRed(boolean warningRed) {
		this.warningRed = warningRed;
	}
	

}
