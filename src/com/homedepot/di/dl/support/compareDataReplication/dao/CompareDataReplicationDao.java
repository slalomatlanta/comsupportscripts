package com.homedepot.di.dl.support.compareDataReplication.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CyclicBarrier;

import org.apache.log4j.Logger;

import com.homedepot.di.dl.support.compareDataReplication.util.CompareDataReplicationPojo;
import com.homedepot.di.dl.support.util.Constants;
import com.homedepot.di.dl.support.util.ConstantsCompareDataReplication;
import com.homedepot.di.dl.support.util.DBConnectionUtil;

public class CompareDataReplicationDao {
	
	final static Logger logger = Logger.getLogger(CompareDataReplicationDao.class);
	
	List <CompareDataReplicationPojo> resultList = new ArrayList<CompareDataReplicationPojo>();
	
	DBConnectionUtil dbConn = new DBConnectionUtil();

	final CyclicBarrier gate = new CyclicBarrier(39);
	

	
	public List<String> OrderHeaderQueries(){
	  
	  List <String> yfsOrderHeaderList = new ArrayList<String>();
	  
	  yfsOrderHeaderList.add(ConstantsCompareDataReplication.STERLING_ORDER_HEADER_KEY_SELECT);
	  yfsOrderHeaderList.add(ConstantsCompareDataReplication.ODS_ORDER_HEADER_KEY_SELECT);
	   
	return yfsOrderHeaderList;
	  
  }
  
	public List<String> baseCommonCodeQueries(){
	  
	  List <String> baseCommonCodeList = new ArrayList<String>();

	  baseCommonCodeList.add(ConstantsCompareDataReplication.STERLING_BASE_COMMON_CODE_SELECT);
	  baseCommonCodeList.add(ConstantsCompareDataReplication.ODS_BASE_COMMON_CODE_SELECT);
	   
	return baseCommonCodeList;
	  
  }
  
	public List<String> chargeCategoryQueries(){
	  
	  List <String> chargeCategoryList = new ArrayList<String>();

	  chargeCategoryList.add(ConstantsCompareDataReplication.STERLING_CHARGE_CATEGORY_SELECT);
	  chargeCategoryList.add(ConstantsCompareDataReplication.ODS_CHARGE_CATEGORY_SELECT);
	   
	return chargeCategoryList;
	  
  }
  
	public List<String> chargeNameQueries(){
	  
	  List <String> chargeNameList = new ArrayList<String>();

	  chargeNameList.add(ConstantsCompareDataReplication.STERLING_CHARGE_NAME_SELECT);
	  chargeNameList.add(ConstantsCompareDataReplication.ODS_CHARGE_NAME_SELECT);
	   
	return chargeNameList;		
	  
  }
  
	public List<String> commonCodeQueries(){
	  
	  List <String>  commonCodeList = new ArrayList<String>();

	  commonCodeList.add(ConstantsCompareDataReplication.STERLING_COMMON_CODE_SELECT);
	  commonCodeList.add(ConstantsCompareDataReplication.ODS_COMMON_CODE_SELECT);

	return commonCodeList;		
	  
  }
	
 	public List<String> commonCodeAttributeQueries(){
	  
	  List <String> commonCodeAttributeList = new ArrayList<String>();

	  commonCodeAttributeList.add(ConstantsCompareDataReplication.STERLING_COMMON_CODE_ATTRIBUTE_SELECT);
	  commonCodeAttributeList.add(ConstantsCompareDataReplication.ODS_COMMON_CODE_ATTRIBUTE_SELECT);

	return commonCodeAttributeList;		
	
	
  }

 	public List<String> enterpriseQueries(){
	  
	  List <String> enterpriseList = new ArrayList<String>();

	  enterpriseList.add(ConstantsCompareDataReplication.STERLING_ENTERPRISE_SELECT);
	  enterpriseList.add(ConstantsCompareDataReplication.ODS_ENTERPRISE_SELECT);

	return enterpriseList;		
	

	
 }
 
	public List<String> errorCodeQueries(){
		  
		  List <String> errorCodeList = new ArrayList<String>();
	
		  errorCodeList.add(ConstantsCompareDataReplication.STERLING_ERROR_CODE_SELECT);
		  errorCodeList.add(ConstantsCompareDataReplication.ODS_ERROR_CODE_SELECT);
	
		return errorCodeList;		
		
	
		
	}
	 
	 public List<String> holdTypeQueries(){
		  
		  List <String> holdTypeList = new ArrayList<String>();
	
		  holdTypeList.add(ConstantsCompareDataReplication.STERLING_HOLD_TYPE_SELECT);
		  holdTypeList.add(ConstantsCompareDataReplication.ODS_HOLD_TYPE_SELECT);
	
		return holdTypeList;		
		
	
		
	}
	 
	 public List<String> inboxReferenceQueries(){
		  
		  List <String> inboxReferenceList = new ArrayList<String>();
	
		  inboxReferenceList.add(ConstantsCompareDataReplication.STERLING_INBOX_REFERENCE_SELECT);
		  inboxReferenceList.add(ConstantsCompareDataReplication.ODS_INBOX_REFERENCE_SELECT);
	
		return inboxReferenceList;		
		
	}

	 public List<String> inventoryItemQueries(){
		  
		  List <String> inventoryItemList = new ArrayList<String>();
	
		  inventoryItemList.add(ConstantsCompareDataReplication.STERLING_INVENTORY_ITEM_SELECT);
		  inventoryItemList.add(ConstantsCompareDataReplication.ODS_INVENTORY_ITEM_SELECT);
	
		return inventoryItemList;		
		
	}

	 public List<String> inventoryNodeControlQueries(){
		  
		  List <String> inventoryNodeControlList = new ArrayList<String>();
	
		  inventoryNodeControlList.add(ConstantsCompareDataReplication.STERLING_INVENTORY_NODE_CONTROL_SELECT);
		  inventoryNodeControlList.add(ConstantsCompareDataReplication.ODS_INVENTORY_NODE_CONTROL_SELECT);
	
		return inventoryNodeControlList;		
		
	}

	 public List<String> inventorySupplyTypeQueries(){
		  
		  List <String> inventorySupplyTypeList = new ArrayList<String>();
	
		  inventorySupplyTypeList.add(ConstantsCompareDataReplication.STERLING_INVENTORY_SUPPLY_TYPE_SELECT);
		  inventorySupplyTypeList.add(ConstantsCompareDataReplication.ODS_INVENTORY_SUPPLY_TYPE_SELECT);
	
		return inventorySupplyTypeList;		
		
	}
	 
	 public List<String> itemQueries(){
		  
		  List <String> itemList = new ArrayList<String>();
	
		  itemList.add(ConstantsCompareDataReplication.STERLING_ITEM_SELECT);
		  itemList.add(ConstantsCompareDataReplication.ODS_ITEM_SELECT);
	
		return itemList;		
		
	}

	 public List<String> itemAliasQueries(){
		  
		  List <String> itemAliasList = new ArrayList<String>();
	
		  itemAliasList.add(ConstantsCompareDataReplication.STERLING_ITEM_ALIAS_SELECT);
		  itemAliasList.add(ConstantsCompareDataReplication.ODS_ITEM_ALIAS_SELECT);
	
		return itemAliasList;		
		
	}
	 
	 public List<String> itemNodeDefnQueries(){
		  
		  List <String> itemNodeDefnList = new ArrayList<String>();
	
		  itemNodeDefnList.add(ConstantsCompareDataReplication.STERLING_ITEM_NODE_DEFN_SELECT);
		  itemNodeDefnList.add(ConstantsCompareDataReplication.ODS_ITEM_NODE_DEFN_SELECT);
	
		return itemNodeDefnList;		
		
	}
 
	 public List<String> itemUomQueries(){
		  
		  List <String> itemUomList = new ArrayList<String>();
	
		  itemUomList.add(ConstantsCompareDataReplication.STERLING_ITEM_UOM_SELECT);
		  itemUomList.add(ConstantsCompareDataReplication.ODS_ITEM_UOM_SELECT);
	
		return itemUomList;		
		
	}
	 
	 public List<String> lineRelationshipTypeQueries(){
		  
		  List <String> lineRelationshipTypeList = new ArrayList<String>();
	
		  lineRelationshipTypeList.add(ConstantsCompareDataReplication.STERLING_LINE_RELATIONSHIP_TYPE_SELECT);
		  lineRelationshipTypeList.add(ConstantsCompareDataReplication.ODS_LINE_RELATIONSHIP_TYPE_SELECT);
	
		return lineRelationshipTypeList;		
		
	}
	 
	 public List<String> orderLineTypeQueries(){
		  
		  List <String> orderLineTypeList = new ArrayList<String>();
	
		  orderLineTypeList.add(ConstantsCompareDataReplication.STERLING_ORDER_LINE_TYPE_SELECT);
		  orderLineTypeList.add(ConstantsCompareDataReplication.ODS_ORDER_LINE_TYPE_SELECT);
	
		return orderLineTypeList;		
		
	}
	  
	 public List<String> organizationQueries(){
		  
		  List <String> organizationList = new ArrayList<String>();
	
		  organizationList.add(ConstantsCompareDataReplication.STERLING_ORGANIZATION_SELECT);
		  organizationList.add(ConstantsCompareDataReplication.ODS_ORGANIZATION_SELECT);
	
		return organizationList;		
		
	}
 
	 public List<String> orgEnterpriseQueries(){
		  
		  List <String> orgEnterpriseList = new ArrayList<String>();
	
		  orgEnterpriseList.add(ConstantsCompareDataReplication.STERLING_ORG_ENTERPRISE_SELECT);
		  orgEnterpriseList.add(ConstantsCompareDataReplication.ODS_ORG_ENTERPRISE_SELECT);
	
		return orgEnterpriseList;		
		
	}
	  
	 public List<String> paymentTypeQueries(){
		  
		  List <String> paymentTypeList = new ArrayList<String>();
	
		  paymentTypeList.add(ConstantsCompareDataReplication.STERLING_PAYMENT_TYPE_SELECT);
		  paymentTypeList.add(ConstantsCompareDataReplication.ODS_PAYMENT_TYPE_SELECT);
	
		return paymentTypeList;		
		
	}
	  
	 public List<String> scacQueries(){
		  
		  List <String> scacList = new ArrayList<String>();
	
		  scacList.add(ConstantsCompareDataReplication.STERLING_SCAC_SELECT);
		  scacList.add(ConstantsCompareDataReplication.ODS_SCAC_SELECT);
	
		return scacList;		
		
	}
	 
	 public List<String> scacAndServiceQueries(){
		  
		  List <String> scacAndServiceList = new ArrayList<String>();
	
		  scacAndServiceList.add(ConstantsCompareDataReplication.STERLING_SCAC_AND_SERVICE_SELECT);
		  scacAndServiceList.add(ConstantsCompareDataReplication.ODS_SCAC_AND_SERVICE_SELECT);
	
		return scacAndServiceList;		
		
	}
	  
	 public List<String> shipNodeQueries(){
		  
		  List <String> shipNodeList = new ArrayList<String>();
	
		  shipNodeList.add(ConstantsCompareDataReplication.STERLING_SHIPNODE_SELECT);
		  shipNodeList.add(ConstantsCompareDataReplication.ODS_SHIPNODE_SELECT);
	
		return shipNodeList;		
		
	}
	 	 
	 public List<String> statusQueries(){
		  
		  List <String> statusList = new ArrayList<String>();
	
		  statusList.add(ConstantsCompareDataReplication.STERLING_STATUS_SELECT);
		  statusList.add(ConstantsCompareDataReplication.ODS_STATUS_SELECT);
	
		return statusList;		
		
	}
	 	 
	 public List<String> uomQueries(){
		  
		  List <String> uomList = new ArrayList<String>();
	
		  uomList.add(ConstantsCompareDataReplication.STERLING_UOM_SELECT);
		  uomList.add(ConstantsCompareDataReplication.ODS_UOM_SELECT);
	
		return uomList;		
		
	}
	 
	 public List<String> uomConversionQueries(){
		  
		  List <String> uomConversionList = new ArrayList<String>();
	
		  uomConversionList.add(ConstantsCompareDataReplication.STERLING_UOM_CONVERSION_SELECT);
		  uomConversionList.add(ConstantsCompareDataReplication.ODS_UOM_CONVERSION_SELECT);
	
		return uomConversionList;		
		
	}
	 
	 public List<String> hdInboxQueries(){
		  
		  List <String> hdInboxList = new ArrayList<String>();
	
		  hdInboxList.add(ConstantsCompareDataReplication.STERLING_HD_INBOX_SELECT);
		  hdInboxList.add(ConstantsCompareDataReplication.ODS_HD_INBOX_SELECT);
	
		return hdInboxList;		
		
	}
	 
	 public List<String> orderLineQueries(){
		  
		  List <String> orderLineList = new ArrayList<String>();
	
		  orderLineList.add(ConstantsCompareDataReplication.STERLING_ORDER_LINE_SELECT);
		  orderLineList.add(ConstantsCompareDataReplication.ODS_ORDER_LINE_SELECT);
	
		return orderLineList;		
		
	}
	 
	 public List<String> instructionDetailQueries(){
		  
		  List <String> instructionDetailList = new ArrayList<String>();
	
		  instructionDetailList.add(ConstantsCompareDataReplication.STERLING_INSTRUCTION_DETAIL_SELECT);
		  instructionDetailList.add(ConstantsCompareDataReplication.ODS_INSTRUCTION_DETAIL_SELECT);
	
		return instructionDetailList;		
		
	}
	 
	 public List<String> hdWCInstructionQueries(){
		  
		  List <String> hdWCInstructionList = new ArrayList<String>();
	
		  hdWCInstructionList.add(ConstantsCompareDataReplication.STERLING_HD_W_C_INSTRUCTION_SELECT);
		  hdWCInstructionList.add(ConstantsCompareDataReplication.ODS_HD_W_C_INSTRUCTION_SELECT);
	
		return hdWCInstructionList;		
		
	}
	 
	 public List<String> personInfoQueries(){
		  
		  List <String> personInfoList = new ArrayList<String>();
	
		  personInfoList.add(ConstantsCompareDataReplication.STERLING_PERSON_INFO_SELECT);
		  personInfoList.add(ConstantsCompareDataReplication.ODS_PERSON_INFO_SELECT);
	
		return personInfoList;		
		
	}
	 	 
	 public List<String> hdWillCallQueries(){
		  
		  List <String> hdWillCallList = new ArrayList<String>();
	
		  hdWillCallList.add(ConstantsCompareDataReplication.STERLING_HD_WILL_CALL_SELECT);
		  hdWillCallList.add(ConstantsCompareDataReplication.ODS_HD_WILL_CALL_SELECT);
	
		return hdWillCallList;		
		
	}
	 	 
	 public List<String> hdSplOrdQueries(){
		  
		  List <String> hdSplOrdList = new ArrayList<String>();
	
		  hdSplOrdList.add(ConstantsCompareDataReplication.STERLING_HD_SPL_ORD_SELECT);
		  hdSplOrdList.add(ConstantsCompareDataReplication.ODS_HD_SPL_ORD_SELECT);
	
		return hdSplOrdList;		
		
	}
	 	 
	 public List<String> hdEventQueries(){
		  
		  List <String> hdEventList = new ArrayList<String>();
	
		  hdEventList.add(ConstantsCompareDataReplication.STERLING_HD_EVENT_SELECT);
		  hdEventList.add(ConstantsCompareDataReplication.ODS_HD_EVENT_SELECT);
	
		return hdEventList;		
		
	}
	 
	 public List<String> workOrderQueries(){
		  
		  List <String> workOrderList = new ArrayList<String>();
	
		  workOrderList.add(ConstantsCompareDataReplication.STERLING_WORK_ORDER_SELECT);
		  workOrderList.add(ConstantsCompareDataReplication.ODS_WORK_ORDER_SELECT);
	
		return workOrderList;		
		
	}
	 
	 public List<String> hdRelatedWorkOrderQueries(){
		  
		  List <String> hdRelatedWorkOrderList = new ArrayList<String>();
	
		  hdRelatedWorkOrderList.add(ConstantsCompareDataReplication.STERLING_HD_RELATED_WORK_ORDER_SELECT);
		  hdRelatedWorkOrderList.add(ConstantsCompareDataReplication.ODS_HD_RELATED_WORK_ORDER_SELECT);
	
		return hdRelatedWorkOrderList;		
		
	}

	public String getStrlingKeyDao(String query) {

		Connection sscConnStrling = dbConn.getJNDIConnection(Constants.STERLING_SSC_RO_JNDI);
		logger.info(" Sterling Connections created");
		String sterlingKey = "";
		Statement stmt = null;
		ResultSet rs = null;

		try {

			stmt = sscConnStrling.createStatement();
			rs = stmt.executeQuery(query);
			while (rs.next()) {
				sterlingKey = rs.getString(1);

			}
			rs.close();
			stmt.close();
			sscConnStrling.close();
			logger.info(" Sterling Connections Closed");
		} catch (SQLException e) {

			e.printStackTrace();
			logger.error(e.getMessage());
		}

		return sterlingKey;

	}

	public String getODSKeyDao(String query) {

		Connection sscConnODS = dbConn.getJNDIConnection(Constants.ODS_JNDI);
		logger.info(" ods Connections created");
		String odsKey = "";
		Statement stmt = null;
		ResultSet rs = null;

		try {

			stmt = sscConnODS.createStatement();
			rs = stmt.executeQuery(query);

			while (rs.next()) {

				odsKey = rs.getString(1);

			}

			rs.close();
			stmt.close();
			sscConnODS.close();
			logger.info(" ods Connections Closed");
		} catch (SQLException e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}

		return odsKey;

	}

	public Thread OrderHeader = new Thread() {
		public void run() {

			CompareDataReplicationPojo cdrp = new CompareDataReplicationPojo();

			cdrp.setTableName("YFS_ORDER_HEADER");

			List<String> list = OrderHeaderQueries();

			String sterlingTimestamp = list.get(0);
			String odsTimestamp = list.get(1);

			String sterlingTime = getStrlingKeyDao(sterlingTimestamp);
			String odsTime = getODSKeyDao(odsTimestamp);

			cdrp.setSterlingKey(sterlingTime);
			cdrp.setOdsKey(odsTime);

			resultList.add(cdrp);

		}
	};

	public Thread baseCommonCode = new Thread() {
		public void run() {

			CompareDataReplicationPojo cdrp = new CompareDataReplicationPojo();

			cdrp.setTableName("YFS_BASE_COMMON_CODE");

			List<String> list = baseCommonCodeQueries();

			String sterlingTimestamp = list.get(0);
			String odsTimestamp = list.get(1);

			String sterlingTime = getStrlingKeyDao(sterlingTimestamp);
			String odsTime = getODSKeyDao(odsTimestamp);

			cdrp.setSterlingKey(sterlingTime);
			cdrp.setOdsKey(odsTime);

			resultList.add(cdrp);

		}
	};

	public Thread chargeCategory = new Thread() {
		public void run() {

			CompareDataReplicationPojo cdrp = new CompareDataReplicationPojo();

			cdrp.setTableName("YFS_CHARGE_CATEGORY");

			List<String> list = chargeCategoryQueries();

			String sterlingTimestamp = list.get(0);
			String odsTimestamp = list.get(1);

			String sterlingTime = getStrlingKeyDao(sterlingTimestamp);
			String odsTime = getODSKeyDao(odsTimestamp);

			cdrp.setSterlingKey(sterlingTime);
			cdrp.setOdsKey(odsTime);

			resultList.add(cdrp);

		}
	};

	public Thread chargeName = new Thread() {
		public void run() {

			CompareDataReplicationPojo cdrp = new CompareDataReplicationPojo();

			cdrp.setTableName("YFS_CHARGE_NAME");

			List<String> list = chargeNameQueries();

			String sterlingTimestamp = list.get(0);
			String odsTimestamp = list.get(1);

			String sterlingTime = getStrlingKeyDao(sterlingTimestamp);
			String odsTime = getODSKeyDao(odsTimestamp);

			cdrp.setSterlingKey(sterlingTime);
			cdrp.setOdsKey(odsTime);

			resultList.add(cdrp);

		}
	};

	public Thread commonCode = new Thread() {

		public void run() {

			CompareDataReplicationPojo cdrp = new CompareDataReplicationPojo();

			cdrp.setTableName("YFS_COMMON_CODE");

			List<String> list = commonCodeQueries();

			String sterlingTimestamp = list.get(0);
			String odsTimestamp = list.get(1);

			String sterlingTime = getStrlingKeyDao(sterlingTimestamp);
			String odsTime = getODSKeyDao(odsTimestamp);

			cdrp.setSterlingKey(sterlingTime);
			cdrp.setOdsKey(odsTime);

			resultList.add(cdrp);

		}
	};

	public Thread commonCodeAttribute = new Thread() {
		public void run() {

			CompareDataReplicationPojo cdrp = new CompareDataReplicationPojo();

			cdrp.setTableName("YFS_COMMON_CODE_ATTRIBUTE");

			List<String> list = commonCodeAttributeQueries();

			String sterlingTimestamp = list.get(0);
			String odsTimestamp = list.get(1);

			String sterlingTime = getStrlingKeyDao(sterlingTimestamp);
			String odsTime = getODSKeyDao(odsTimestamp);

			cdrp.setSterlingKey(sterlingTime);
			cdrp.setOdsKey(odsTime);

			resultList.add(cdrp);
		}
	};

	public Thread enterprise = new Thread() {
		public void run() {


			CompareDataReplicationPojo cdrp = new CompareDataReplicationPojo();

			cdrp.setTableName("YFS_ENTERPRISE");

			List<String> list = enterpriseQueries();

			String sterlingTimestamp = list.get(0);
			String odsTimestamp = list.get(1);

			String sterlingTime = getStrlingKeyDao(sterlingTimestamp);
			String odsTime = getODSKeyDao(odsTimestamp);

			cdrp.setSterlingKey(sterlingTime);
			cdrp.setOdsKey(odsTime);

			resultList.add(cdrp);
		}
	};

	public Thread errorCode = new Thread() {
		public void run() {

			CompareDataReplicationPojo cdrp = new CompareDataReplicationPojo();

			cdrp.setTableName("YFS_ERROR_CODE");

			List<String> list = errorCodeQueries();

			String sterlingTimestamp = list.get(0);
			String odsTimestamp = list.get(1);

			String sterlingTime = getStrlingKeyDao(sterlingTimestamp);
			String odsTime = getODSKeyDao(odsTimestamp);

			cdrp.setSterlingKey(sterlingTime);
			cdrp.setOdsKey(odsTime);

			resultList.add(cdrp);
		}
	};

	public Thread holdType = new Thread() {
		public void run() {
			
			CompareDataReplicationPojo cdrp = new CompareDataReplicationPojo();

			cdrp.setTableName("YFS_HOLD_TYPE");

			List<String> list = holdTypeQueries();

			String sterlingTimestamp = list.get(0);
			String odsTimestamp = list.get(1);

			String sterlingTime = getStrlingKeyDao(sterlingTimestamp);
			String odsTime = getODSKeyDao(odsTimestamp);

			cdrp.setSterlingKey(sterlingTime);
			cdrp.setOdsKey(odsTime);

			resultList.add(cdrp);

		}
	};

	public Thread inboxReference = new Thread() {
		public void run() {

			CompareDataReplicationPojo cdrp = new CompareDataReplicationPojo();

			cdrp.setTableName("YFS_INBOX_REFERENCE");

			List<String> list = inboxReferenceQueries();

			String sterlingTimestamp = list.get(0);
			String odsTimestamp = list.get(1);

			String sterlingTime = getStrlingKeyDao(sterlingTimestamp);
			String odsTime = getODSKeyDao(odsTimestamp);

			cdrp.setSterlingKey(sterlingTime);
			cdrp.setOdsKey(odsTime);

			resultList.add(cdrp);
		}
	};

	public Thread inventoryItem = new Thread() {
		public void run() {

			CompareDataReplicationPojo cdrp = new CompareDataReplicationPojo();

			cdrp.setTableName("YFS_INVENTORY_ITEM");

			List<String> list = inventoryItemQueries();

			String sterlingTimestamp = list.get(0);
			String odsTimestamp = list.get(1);

			String sterlingTime = getStrlingKeyDao(sterlingTimestamp);
			String odsTime = getODSKeyDao(odsTimestamp);

			cdrp.setSterlingKey(sterlingTime);
			cdrp.setOdsKey(odsTime);

			resultList.add(cdrp);

		}
	};

	public Thread inventoryNodeControl = new Thread() {
		public void run() {

			CompareDataReplicationPojo cdrp = new CompareDataReplicationPojo();

			cdrp.setTableName("YFS_INVENTORY_NODE_CONTROL");

			List<String> list = inventoryNodeControlQueries();

			String sterlingTimestamp = list.get(0);
			String odsTimestamp = list.get(1);

			String sterlingTime = getStrlingKeyDao(sterlingTimestamp);
			String odsTime = getODSKeyDao(odsTimestamp);

			cdrp.setSterlingKey(sterlingTime);
			cdrp.setOdsKey(odsTime);

			resultList.add(cdrp);

		}
	};

	public Thread inventorySupplyType = new Thread() {
		public void run() {

			CompareDataReplicationPojo cdrp = new CompareDataReplicationPojo();

			cdrp.setTableName("YFS_INVENTORY_SUPPLY_TYPE");

			List<String> list = inventorySupplyTypeQueries();

			String sterlingTimestamp = list.get(0);
			String odsTimestamp = list.get(1);

			String sterlingTime = getStrlingKeyDao(sterlingTimestamp);
			String odsTime = getODSKeyDao(odsTimestamp);

			cdrp.setSterlingKey(sterlingTime);
			cdrp.setOdsKey(odsTime);

			resultList.add(cdrp);

		}
	};

	public Thread item = new Thread() {
		public void run() {

			CompareDataReplicationPojo cdrp = new CompareDataReplicationPojo();

			cdrp.setTableName("YFS_ITEM");

			List<String> list = itemQueries();

			String sterlingTimestamp = list.get(0);
			String odsTimestamp = list.get(1);

			String sterlingTime = getStrlingKeyDao(sterlingTimestamp);
			String odsTime = getODSKeyDao(odsTimestamp);

			cdrp.setSterlingKey(sterlingTime);
			cdrp.setOdsKey(odsTime);

			resultList.add(cdrp);

		}
	};

	public Thread itemAlias = new Thread() {
		public void run() {

			CompareDataReplicationPojo cdrp = new CompareDataReplicationPojo();

			cdrp.setTableName("YFS_ITEM_ALIAS");

			List<String> list = itemAliasQueries();

			String sterlingTimestamp = list.get(0);
			String odsTimestamp = list.get(1);

			String sterlingTime = getStrlingKeyDao(sterlingTimestamp);
			String odsTime = getODSKeyDao(odsTimestamp);

			cdrp.setSterlingKey(sterlingTime);
			cdrp.setOdsKey(odsTime);

			resultList.add(cdrp);

		}
	};

	public Thread itemNodeDefn = new Thread() {
		public void run() {

			CompareDataReplicationPojo cdrp = new CompareDataReplicationPojo();

			cdrp.setTableName("YFS_ITEM_NODE_DEFN");

			List<String> list = itemNodeDefnQueries();

			String sterlingTimestamp = list.get(0);
			String odsTimestamp = list.get(1);

			String sterlingTime = getStrlingKeyDao(sterlingTimestamp);
			String odsTime = getODSKeyDao(odsTimestamp);

			cdrp.setSterlingKey(sterlingTime);
			cdrp.setOdsKey(odsTime);

			resultList.add(cdrp);

		}
	};

	public Thread itemUom = new Thread() {
		public void run() {


			CompareDataReplicationPojo cdrp = new CompareDataReplicationPojo();

			cdrp.setTableName("YFS_ITEM_UOM");

			List<String> list = itemUomQueries();

			String sterlingTimestamp = list.get(0);
			String odsTimestamp = list.get(1);

			String sterlingTime = getStrlingKeyDao(sterlingTimestamp);
			String odsTime = getODSKeyDao(odsTimestamp);

			cdrp.setSterlingKey(sterlingTime);
			cdrp.setOdsKey(odsTime);

			resultList.add(cdrp);

		}
	};

	public Thread lineRelationshipType = new Thread() {
		public void run() {

			CompareDataReplicationPojo cdrp = new CompareDataReplicationPojo();

			cdrp.setTableName("YFS_LINE_RELATIONSHIP_TYPE");

			List<String> list = lineRelationshipTypeQueries();

			String sterlingTimestamp = list.get(0);
			String odsTimestamp = list.get(1);

			String sterlingTime = getStrlingKeyDao(sterlingTimestamp);
			String odsTime = getODSKeyDao(odsTimestamp);

			cdrp.setSterlingKey(sterlingTime);
			cdrp.setOdsKey(odsTime);

			resultList.add(cdrp);
		}
	};

	public Thread orderLineType = new Thread() {
		public void run() {

			CompareDataReplicationPojo cdrp = new CompareDataReplicationPojo();

			cdrp.setTableName("YFS_ORDER_LINE_TYPE");

			List<String> list = orderLineTypeQueries();

			String sterlingTimestamp = list.get(0);
			String odsTimestamp = list.get(1);

			String sterlingTime = getStrlingKeyDao(sterlingTimestamp);
			String odsTime = getODSKeyDao(odsTimestamp);

			cdrp.setSterlingKey(sterlingTime);
			cdrp.setOdsKey(odsTime);

			resultList.add(cdrp);
		}
	};

	public Thread organization = new Thread() {
		public void run() {

			CompareDataReplicationPojo cdrp = new CompareDataReplicationPojo();

			cdrp.setTableName("YFS_ORGANIZATION");

			List<String> list = organizationQueries();

			String sterlingTimestamp = list.get(0);
			String odsTimestamp = list.get(1);

			String sterlingTime = getStrlingKeyDao(sterlingTimestamp);
			String odsTime = getODSKeyDao(odsTimestamp);

			cdrp.setSterlingKey(sterlingTime);
			cdrp.setOdsKey(odsTime);

			resultList.add(cdrp);
		}
	};

	public Thread orgEnterprise = new Thread() {
		public void run() {

			CompareDataReplicationPojo cdrp = new CompareDataReplicationPojo();

			cdrp.setTableName("YFS_ORG_ENTERPRISE");

			List<String> list = orgEnterpriseQueries();

			String sterlingTimestamp = list.get(0);
			String odsTimestamp = list.get(1);

			String sterlingTime = getStrlingKeyDao(sterlingTimestamp);
			String odsTime = getODSKeyDao(odsTimestamp);

			cdrp.setSterlingKey(sterlingTime);
			cdrp.setOdsKey(odsTime);

			resultList.add(cdrp);
		}
	};

	public Thread paymentType = new Thread() {
		public void run() {

			CompareDataReplicationPojo cdrp = new CompareDataReplicationPojo();

			cdrp.setTableName("YFS_PAYMENT_TYPE");

			List<String> list = paymentTypeQueries();

			String sterlingTimestamp = list.get(0);
			String odsTimestamp = list.get(1);

			String sterlingTime = getStrlingKeyDao(sterlingTimestamp);
			String odsTime = getODSKeyDao(odsTimestamp);

			cdrp.setSterlingKey(sterlingTime);
			cdrp.setOdsKey(odsTime);

			resultList.add(cdrp);
		}
	};

	public Thread scac = new Thread() {
		public void run() {

			CompareDataReplicationPojo cdrp = new CompareDataReplicationPojo();

			cdrp.setTableName("YFS_SCAC");

			List<String> list = scacQueries();

			String sterlingTimestamp = list.get(0);
			String odsTimestamp = list.get(1);

			String sterlingTime = getStrlingKeyDao(sterlingTimestamp);
			String odsTime = getODSKeyDao(odsTimestamp);

			cdrp.setSterlingKey(sterlingTime);
			cdrp.setOdsKey(odsTime);

			resultList.add(cdrp);
		}
	};

	public Thread scacAndService = new Thread() {
		public void run() {

			CompareDataReplicationPojo cdrp = new CompareDataReplicationPojo();

			cdrp.setTableName("YFS_SCAC_AND_SERVICE");

			List<String> list = scacAndServiceQueries();

			String sterlingTimestamp = list.get(0);
			String odsTimestamp = list.get(1);

			String sterlingTime = getStrlingKeyDao(sterlingTimestamp);
			String odsTime = getODSKeyDao(odsTimestamp);

			cdrp.setSterlingKey(sterlingTime);
			cdrp.setOdsKey(odsTime);

			resultList.add(cdrp);
		}
	};

	public Thread shipNode = new Thread() {
		public void run() {

			CompareDataReplicationPojo cdrp = new CompareDataReplicationPojo();

			cdrp.setTableName("YFS_SHIPNODE");

			List<String> list = shipNodeQueries();

			String sterlingTimestamp = list.get(0);
			String odsTimestamp = list.get(1);

			String sterlingTime = getStrlingKeyDao(sterlingTimestamp);
			String odsTime = getODSKeyDao(odsTimestamp);

			cdrp.setSterlingKey(sterlingTime);
			cdrp.setOdsKey(odsTime);

			resultList.add(cdrp);

		}
	};

	public Thread status = new Thread() {
		public void run() {

			CompareDataReplicationPojo cdrp = new CompareDataReplicationPojo();

			cdrp.setTableName("YFS_STATUS");

			List<String> list = statusQueries();

			String sterlingTimestamp = list.get(0);
			String odsTimestamp = list.get(1);

			String sterlingTime = getStrlingKeyDao(sterlingTimestamp);
			String odsTime = getODSKeyDao(odsTimestamp);

			cdrp.setSterlingKey(sterlingTime);
			cdrp.setOdsKey(odsTime);

			resultList.add(cdrp);
		}
	};

	public Thread uom = new Thread() {
		public void run() {

			CompareDataReplicationPojo cdrp = new CompareDataReplicationPojo();

			cdrp.setTableName("YFS_UOM");

			List<String> list = uomQueries();

			String sterlingTimestamp = list.get(0);
			String odsTimestamp = list.get(1);

			String sterlingTime = getStrlingKeyDao(sterlingTimestamp);
			String odsTime = getODSKeyDao(odsTimestamp);

			cdrp.setSterlingKey(sterlingTime);
			cdrp.setOdsKey(odsTime);

			resultList.add(cdrp);

		}
	};

	public Thread uomConversion = new Thread() {
		public void run() {

			CompareDataReplicationPojo cdrp = new CompareDataReplicationPojo();

			cdrp.setTableName("YFS_UOM_CONVERSION");

			List<String> list = uomConversionQueries();

			String sterlingTimestamp = list.get(0);
			String odsTimestamp = list.get(1);

			String sterlingTime = getStrlingKeyDao(sterlingTimestamp);
			String odsTime = getODSKeyDao(odsTimestamp);

			cdrp.setSterlingKey(sterlingTime);
			cdrp.setOdsKey(odsTime);

			resultList.add(cdrp);
		}
	};

	public Thread hdInbox = new Thread() {
		public void run() {

			CompareDataReplicationPojo cdrp = new CompareDataReplicationPojo();

			cdrp.setTableName("HD_INBOX");

			List<String> list = hdInboxQueries();

			String sterlingTimestamp = list.get(0);
			String odsTimestamp = list.get(1);

			String sterlingTime = getStrlingKeyDao(sterlingTimestamp);
			String odsTime = getODSKeyDao(odsTimestamp);

			cdrp.setSterlingKey(sterlingTime);
			cdrp.setOdsKey(odsTime);

			resultList.add(cdrp);
		}
	};

	public Thread orderLine = new Thread() {
		public void run() {

			CompareDataReplicationPojo cdrp = new CompareDataReplicationPojo();

			cdrp.setTableName("YFS_ORDER_LINE");

			List<String> list = orderLineQueries();

			String sterlingTimestamp = list.get(0);
			String odsTimestamp = list.get(1);

			String sterlingTime = getStrlingKeyDao(sterlingTimestamp);
			String odsTime = getODSKeyDao(odsTimestamp);

			cdrp.setSterlingKey(sterlingTime);
			cdrp.setOdsKey(odsTime);

			resultList.add(cdrp);

		}
	};

	public Thread instructionDetail = new Thread() {
		public void run() {

			CompareDataReplicationPojo cdrp = new CompareDataReplicationPojo();

			cdrp.setTableName("YFS_INSTRUCTION_DETAIL");

			List<String> list = instructionDetailQueries();

			String sterlingTimestamp = list.get(0);
			String odsTimestamp = list.get(1);

			String sterlingTime = getStrlingKeyDao(sterlingTimestamp);
			String odsTime = getODSKeyDao(odsTimestamp);

			cdrp.setSterlingKey(sterlingTime);
			cdrp.setOdsKey(odsTime);

			resultList.add(cdrp);

		}
	};

	public Thread hdWCInstruction = new Thread() {
		public void run() {

			CompareDataReplicationPojo cdrp = new CompareDataReplicationPojo();

			cdrp.setTableName("HD_W_C_INSTRUCTION");

			List<String> list = hdWCInstructionQueries();

			String sterlingTimestamp = list.get(0);
			String odsTimestamp = list.get(1);

			String sterlingTime = getStrlingKeyDao(sterlingTimestamp);
			String odsTime = getODSKeyDao(odsTimestamp);

			cdrp.setSterlingKey(sterlingTime);
			cdrp.setOdsKey(odsTime);

			resultList.add(cdrp);

		}
	};

	public Thread personInfo = new Thread() {
		public void run() {

			CompareDataReplicationPojo cdrp = new CompareDataReplicationPojo();

			cdrp.setTableName("YFS_PERSON_INFO");

			List<String> list = personInfoQueries();

			String sterlingTimestamp = list.get(0);
			String odsTimestamp = list.get(1);

			String sterlingTime = getStrlingKeyDao(sterlingTimestamp);
			String odsTime = getODSKeyDao(odsTimestamp);

			cdrp.setSterlingKey(sterlingTime);
			cdrp.setOdsKey(odsTime);

			resultList.add(cdrp);
		}
	};

	public Thread hdWillCall = new Thread() {
		public void run() {

			CompareDataReplicationPojo cdrp = new CompareDataReplicationPojo();

			cdrp.setTableName("HD_WILL_CALL");

			List<String> list = hdWillCallQueries();

			String sterlingTimestamp = list.get(0);
			String odsTimestamp = list.get(1);

			String sterlingTime = getStrlingKeyDao(sterlingTimestamp);
			String odsTime = getODSKeyDao(odsTimestamp);

			cdrp.setSterlingKey(sterlingTime);
			cdrp.setOdsKey(odsTime);

			resultList.add(cdrp);

		}
	};

	public Thread hdSplOrd = new Thread() {
		public void run() {
			
			CompareDataReplicationPojo cdrp = new CompareDataReplicationPojo();

			cdrp.setTableName("HD_SPL_ORD");

			List<String> list = hdSplOrdQueries();

			String sterlingTimestamp = list.get(0);
			String odsTimestamp = list.get(1);

			String sterlingTime = getStrlingKeyDao(sterlingTimestamp);
			String odsTime = getODSKeyDao(odsTimestamp);

			cdrp.setSterlingKey(sterlingTime);
			cdrp.setOdsKey(odsTime);

			resultList.add(cdrp);

		}
	};

	public Thread hdEvent = new Thread() {
		public void run() {

			CompareDataReplicationPojo cdrp = new CompareDataReplicationPojo();

			cdrp.setTableName("HD_EVENT");

			List<String> list = hdEventQueries();

			String sterlingTimestamp = list.get(0);
			String odsTimestamp = list.get(1);

			String sterlingTime = getStrlingKeyDao(sterlingTimestamp);
			String odsTime = getODSKeyDao(odsTimestamp);

			cdrp.setSterlingKey(sterlingTime);
			cdrp.setOdsKey(odsTime);

			resultList.add(cdrp);

		}
	};

	public Thread workOrder = new Thread() {
		public void run() {

			CompareDataReplicationPojo cdrp = new CompareDataReplicationPojo();

			cdrp.setTableName("YFS_WORK_ORDER");

			List<String> list = workOrderQueries();

			String sterlingTimestamp = list.get(0);
			String odsTimestamp = list.get(1);

			String sterlingTime = getStrlingKeyDao(sterlingTimestamp);
			String odsTime = getODSKeyDao(odsTimestamp);

			cdrp.setSterlingKey(sterlingTime);
			cdrp.setOdsKey(odsTime);

			resultList.add(cdrp);

		}
	};

	public Thread hdRelatedWorkOrder = new Thread() {
		public void run() {

			CompareDataReplicationPojo cdrp = new CompareDataReplicationPojo();

			cdrp.setTableName("HD_RELATED_WORK_ORDER");

			List<String> list = hdRelatedWorkOrderQueries();

			String sterlingTimestamp = list.get(0);
			String odsTimestamp = list.get(1);

			String sterlingTime = getStrlingKeyDao(sterlingTimestamp);
			String odsTime = getODSKeyDao(odsTimestamp);

			cdrp.setSterlingKey(sterlingTime);
			cdrp.setOdsKey(odsTime);

			resultList.add(cdrp);
		}
	};

	public List<CompareDataReplicationPojo> getResultList() {
		return resultList;
	}

	public void setResultList(List<CompareDataReplicationPojo> resultList) {
		this.resultList = resultList;
	}

}
