package com.homedepot.di.dl.support.compareDataReplication.web;


import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Properties;

import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage.RecipientType;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.homedepot.di.dl.support.compareDataReplication.util.CompareDataReplicationPojo;
import com.homedepot.di.dl.support.compareDataReplication.util.CompareDataReplicationUtil;

/**
 * Servlet implementation class CompareDataReplicationServlet
 */

public class CompareDataReplicationServlet extends HttpServlet {
	
	final static Logger logger = Logger.getLogger(CompareDataReplicationServlet.class);
	private static final long serialVersionUID = 1L;

    /**
     * Default constructor. 
     */
    public CompareDataReplicationServlet() {
     
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		logger.info("Executing CompareDataReplicationServlet doGet method");
		
		 long start = System.currentTimeMillis();
		 
		CompareDataReplicationUtil cdpu = new CompareDataReplicationUtil();
		
		List<CompareDataReplicationPojo> cpdrList = cdpu.compareReplicationData();
		
		response.setContentType("text/html");
		
		PrintWriter out = null;
		
		String display = "";
		String comparisonTable = ""; 
		
		display = "<html><head><body bgcolor=\"#676767\">" + "<font face=\"calibri\">";
    	display = display + "<h2><center><b><font color=\"White\">Sterling to ODS Replication Data Comparison</b></center></h2><br/>";
		String tableHdr = "<table align=\"center\" border=\"1\" cellpadding=\"1\" cellspacing=\"0\" width=\"100%\" style=\"border-collapse:collapse;\" bgcolor=\"#F2F2F2\">"
				+ "<font face=\"calibri\">"
				+ "<tr bgcolor=\"#FF8C26\">"
				+ "<th>Table Name Count</th>"
				+ "<th>Sterling Key</th>"
				+ "<th>ODS Key</th>"
				+ "<th>Status</th>"
				+ "<th>Delay (Min) </th>"
				+ "<th>Delay (Sec) </th>"
				+ "</tr>";
		for( CompareDataReplicationPojo cdrp: cpdrList){
			
			
			String colorRow = "";
			String boldTextStart="";
			String boldTextEnd="";
			
			if(cdrp.isWarningYellow()){
				
				colorRow = "bgcolor=\"#e6b800\"";
				boldTextStart = "<b>";
				boldTextEnd="</b>";
				
			}else if(cdrp.isWarningRed()){
				
				colorRow = "bgcolor=\"#cc0000\"";
				boldTextStart = "<b>";
				boldTextEnd="</b>";
			}
			
			comparisonTable = comparisonTable + "<tr " +colorRow+ ">"
				+ "<td align=\"center\">"+ boldTextStart + cdrp.getTableName() + boldTextEnd + "</td>"
				+ "<td align=\"center\">"+ boldTextStart + cdrp.getSterlingKey() + boldTextEnd + "</td>"
				+ "<td align=\"center\">"+ boldTextStart + cdrp.getOdsKey() + boldTextEnd + "</td>"
				+ "<td align=\"center\">"+ boldTextStart + cdrp.getStatus() + boldTextEnd + "</td>"
				+ "<td align=\"center\">"+ boldTextStart + cdrp.getDelayMin() + boldTextEnd + "</td>"
				+ "<td align=\"center\">"+ boldTextStart + cdrp.getDelaySec() + boldTextEnd + "</td>"
				+ "</tr>";
    }
		display = display+tableHdr+comparisonTable; 
		
		display = display +"</body></head></html>";
  
		out = response.getWriter();
	
		out.println(display);
		out.close();
		
		
		
		if(cdpu.isWarn()){
			//_2fc77b@homedepot.com -- Com Support multi channel
			String[] to = { "_2fc77b@homedepot.com", "anand_rathinam@homedepot.com"};
			String subject = "WARNING - Sterling to ODS Replication Data Comparison";
			
			String bodyDetails = "There was over 5 minutes lag from Sterling to ODS. \nThe detail are in the tale below. \n \n ";

			sendMail(to, subject, bodyDetails, cdpu.getWarnList());
		}
		
		if(cdpu.isCritical()){
			
			//String[] to = { "_2fc77b@homedepot.com", "anand_rathinam@homedepot.com"};
			String[] to = { "Order_Mgmt_Support@homedepot.com", "anand_rathinam@homedepot.com"};
			String subject = "CRITICAL - Sterling to ODS Replication Data Comparison";
			String bodyDetails = "There was over 15 minutes lag from Sterling to ODS. \nThe detail are in the tale below. \n \n ";
			sendMail(to, subject, bodyDetails, cdpu.getCriticalList());
		}
		
		long elapsedTime = System.currentTimeMillis() - start;
		
		long minutes = (long) (elapsedTime / (1000*60));
		
		
		logger.info("Executing CompareDataReplicationServlet doGet method");
		
		logger.info("CompareDataReplicationServlet method execution time in minutes: " + minutes );

		logger.info("Method execution time Min: " + minutes );
		
	
	}
	
	
	

	public static void sendMail(String[] to, String Subject, String bodyDetails, List <CompareDataReplicationPojo> list) {

		Session session = null;
		
		MimeMessage mimeMessage = null;
		
	
		Properties properties = null;
		
		try {
			
			properties = System.getProperties();
			properties.put("mail.host", "mail1.homedepot.com");
			
			session = Session.getInstance(properties, null);
			
			mimeMessage = new MimeMessage(session);
			
			mimeMessage.setFrom(new InternetAddress("horizon@cpliisad.homedepot.com"));
			
			InternetAddress[] addressTo = new InternetAddress[to.length];
			
			for (int i = 0; i < to.length; i++) {
				addressTo[i] = new InternetAddress(to[i]);
			}

			mimeMessage.setRecipients(RecipientType.TO, addressTo);
			
			//Dynamic
			mimeMessage.setSubject(Subject);

			String display = "";
			String comparisonTable = ""; 
			
			display = "<html><head><body>" + "<font face=\"calibri\">";
	    	display = display + "<h2><center><b><font color=\"White\">Sterling to ODS Replication Data Comparison</b></center></h2><br/>";
			String tableHdr = "<table align=\"center\" border=\"1\" cellpadding=\"1\" cellspacing=\"0\" width=\"100%\" style=\"border-collapse:collapse;\" bgcolor=\"#F2F2F2\">"
					+ "<font face=\"calibri\">"
					+ "<tr bgcolor=\"#FF8C26\">"
					+ "<th>Table Name Count</th>"
					+ "<th>Sterling Key</th>"
					+ "<th>ODS Key</th>"
					+ "<th>Status</th>"
					+ "<th>Delay (Min) </th>"
					+ "<th>Delay (Sec) </th>"
					+ "</tr>";
			for( CompareDataReplicationPojo cdrp: list){
				
				
				String colorRow = "";
				
				if(cdrp.isWarningYellow()){
					colorRow = "bgcolor=\"#e6b800\"";
				}else if(cdrp.isWarningRed()){
					
					colorRow = "bgcolor=\"#cc0000\"";
				}
				
				comparisonTable = comparisonTable + "<tr " +colorRow+ ">"
					+ "<td align=\"center\">" + cdrp.getTableName() + "</td>"
					+ "<td align=\"center\">" + cdrp.getSterlingKey() + "</td>"
					+ "<td align=\"center\">" + cdrp.getOdsKey() + "</td>"
					+ "<td align=\"center\">" + cdrp.getStatus() + "</td>"
					+ "<td align=\"center\">" + cdrp.getDelayMin() + "</td>"
					+ "<td align=\"center\">" + cdrp.getDelaySec() + "</td>"
					+ "</tr>";
	    }
			display = display+tableHdr+comparisonTable; 
			
			display = display +"</body></head></html>";
			
			
			//Dynaminc
			mimeMessage.setContent(bodyDetails+ display, "text/html");
			
		     
			Transport.send(mimeMessage);
			logger.info(Subject+ "Email Sent");
			
		} catch (Exception e) {
			
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}
	

}
