package com.homedepot.di.dl.support.lagCheck.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Properties;

import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeMessage.RecipientType;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.homedepot.di.dl.support.ErrorDeltas.web.DeltaServlet;
import com.homedepot.di.dl.support.lagCheck.dao.LagCheck;
import com.homedepot.di.dl.support.longRunningquery.service.LongRunningQuery;
import com.homedepot.di.dl.support.util.Constants;

/**
 * Servlet implementation class LagCheckServlet
 */
public class LagCheckServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	final static Logger logger = Logger.getLogger(LagCheckServlet.class); 
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LagCheckServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		logger.info("Lag Check Started : "+ GregorianCalendar.getInstance().getTime());
		LagCheck check = new LagCheck();
		
		String timeDiff=null;
		String message=null;
		

		try 
		{
			message=check.lagCheck();
		} 
		catch (Exception e) 
		{
			// TODO Auto-generated catch block
			
			logger.debug("Exception : "+e);
		}

		
		
		try {
			logger.debug("Start Time: Mail:"+java.util.GregorianCalendar.getInstance().getTime());
	        String mailContent;
			
	        
			
			
	        timeDiff=check.lagCheck();
				mailContent = check.lagCheck();
				int timeDelay = Integer.parseInt(mailContent);
				
				logger.debug("timeDelay" +timeDelay);
				 if(timeDelay>=20)
					{
						
			
	        if(!(mailContent.equals("NoHiPri"))){
	        String eMailSubject= "Critical Alert: Lag Difference";
	        sendMail(mailContent, true, eMailSubject);
	        logger.debug("end Time::"+java.util.GregorianCalendar.getInstance().getTime());
	        }
					
	        logger.debug("Servlet:::"+mailContent);
	        logger.debug("end Time:Mail:"+java.util.GregorianCalendar.getInstance().getTime());
					}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		PrintWriter out = response.getWriter(  ); 
	    response.setContentType("text/html"); 
	    //out.println("Lag between Sterling and ODS" );
	    //out.println("<br/>");
	    out.println(message);
	    out.close();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}
	
	public static void sendMail(String emailContent, boolean isCritical,
			String eMailSubject) {
		String text = emailContent;
		//String[] to = { "NIRAL_PATEL2@homedepot.com" , "DINESH_E@homedepot.com" , "jagatdeep_chakraborty@homedepot.com" , "MAHESH_VYAKARANAM@homedepot.com" , "PURUSHOTHAMAN_RAGHUNATH@homedepot.com" , "RAKESH_J_SATYA@homedepot.com" , "MANOJ_SHUNMUGAM@homedepot.com" };
		String[] to = { "anandhakumar_chinnadurai@homedepot.com"};
		String[] Cc = {  };
		Session session = null;
		MimeMessage mimeMessage = null;
		MimeBodyPart mimeBodyPart = null;
		Multipart multiPart = null;
		String date = null;
		Calendar calendar = null;
		Properties properties = null;
		String msg = null;
		try {
			msg = "Success";
			calendar = Calendar.getInstance();
			date = calendar.getTime().toString();
			properties = System.getProperties();
			properties.put("mail.host", "mail1.homedepot.com");
			session = Session.getInstance(properties, null);
			mimeMessage = new MimeMessage(session);
			mimeMessage.setFrom(new InternetAddress(
					"horizon@cpliisad.homedepot.com"));
			InternetAddress[] addressTo = new InternetAddress[to.length];
			InternetAddress[] addressCc = new InternetAddress[Cc.length];
			for (int i = 0; i < to.length; i++) {
				addressTo[i] = new InternetAddress(to[i]);
				logger.debug("To Address " + to[i]);
			}
			for (int j = 0; j < Cc.length; j++) {
				addressCc[j] = new InternetAddress(Cc[j]);
				logger.debug("Cc Address " + Cc[j]);
			}
			mimeMessage.setRecipients(RecipientType.TO, addressTo);
			mimeMessage.setRecipients(RecipientType.CC, addressCc);
			mimeMessage.setSubject(eMailSubject + date);
			mimeBodyPart = new MimeBodyPart();
			mimeBodyPart.setText(text.toString());
			multiPart = new MimeMultipart();
			multiPart.addBodyPart(mimeBodyPart);
			mimeMessage.setContent(text.toString(), "text/html");
			if (text.length() > 0) {
				if (isCritical) {
					mimeMessage.setHeader("X-Priority", "3");
					      
					logger.info("Sending mail as high priority - "+java.util.GregorianCalendar.getInstance().getTime());
				} else {
					mimeMessage.setHeader("X-Priority", "3");
					logger.info("Sending mail... - "+java.util.GregorianCalendar.getInstance().getTime());
				}
				Transport.send(mimeMessage);
				logger.info("Sent mail succesfully...");
			}
		} catch (Exception e) {
			msg = "Failure";
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

}
