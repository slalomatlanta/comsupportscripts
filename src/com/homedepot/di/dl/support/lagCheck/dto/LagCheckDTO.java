package com.homedepot.di.dl.support.lagCheck.dto;

import org.joda.time.DateTime;

public class LagCheckDTO 
{
	
	private DateTime sterlingTime;
	private DateTime odsTime;
	
	public DateTime getSterlingTime() {
		return sterlingTime;
	}
	public void setSterlingTime(DateTime sterlingTime) {
		this.sterlingTime = sterlingTime;
	}
	public DateTime getOdsTime() {
		return odsTime;
	}
	public void setOdsTime(DateTime odsTime) {
		this.odsTime = odsTime;
	}
	
	
	
	

}
