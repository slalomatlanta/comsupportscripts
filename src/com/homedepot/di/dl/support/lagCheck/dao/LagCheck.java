package com.homedepot.di.dl.support.lagCheck.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Properties;

import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage.RecipientType;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.Period;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.PeriodFormat;
import org.joda.time.format.PeriodFormatter;



import com.homedepot.di.dl.support.lagCheck.dto.LagCheckDTO;
import com.homedepot.di.dl.support.lagCheck.web.LagCheckServlet;
import com.homedepot.di.dl.support.util.Constants;
import com.homedepot.di.dl.support.util.ConstantsQuery;
import com.homedepot.di.dl.support.util.DBConnectionUtil;

public class LagCheck 
{
	final static Logger logger = Logger.getLogger(LagCheck.class);
	
	public static String lagCheck() throws 
	Exception 
	{
		String message= "<html><title>Sterling/ODS lag Check</title>";
		message = message + "<body bgcolor=\"#676767\">" + "<font face=\"calibri\">" ;
		message = message + "<h2><center><b><font color=\"White\">Lag between Sterling and ODS</b></center></h2><br/>";
		message = message + "<br/><center><table border=\"1\" cellpadding=\"1\" cellspacing=\"0\" width=\"200px\" style=\"border-collapse:collapse;\" bgcolor=\"#F2F2F2\"><font face=\"calibri\">" ;
		message = message + "<tr bgcolor=\"#FF8C26\"><th> Table </th> <th> LagTime (in minutes) </th></tr>";
		LagCheckDTO dto;
		//System.out.println("dao");
		
		ConstantsQuery constant = new ConstantsQuery();
		
		
		dto = getDetails(constant.orderHeader);
		int headerTabletimeDiff = calculateDifference(dto);
		//System.out.println("YFS_ORDER_HEADER : "+headerTabletimeDiff);
		message = message + "<tr><td> YFS_ORDER_HEADER </td><td>"+headerTabletimeDiff+"</td></tr>" ;
			
		
		dto = getDetails(constant.hdInbox);
		int inboxTabletimeDiff = calculateDifference(dto);
		//System.out.println("HD_INBOX : "+inboxTabletimeDiff);
		message = message + "<tr><td> HD_INBOX </td><td>"+inboxTabletimeDiff+"</td></tr>" ;
		
		dto = getDetails(constant.orderLine);
		int lineTabletimeDiff = calculateDifference(dto);
		//System.out.println("lineTabletimeDiff : "+lineTabletimeDiff);
		message = message + "<tr><td> YFS_ORDER_LINE </td><td>"+lineTabletimeDiff+"</td></tr>" ;
		
		dto = getDetails(constant.instructionDetail);
		int instructionTabletimeDiff = calculateDifference(dto);
		//System.out.println("instructionTabletimeDiff : "+instructionTabletimeDiff);
		message = message + "<tr><td> YFS_INSTRUCTION_DETAIL </td><td>"+instructionTabletimeDiff+"</td></tr>" ;
		
		dto = getDetails(constant.wcInstruction);
		int wcInstructionTabletimeDiff = calculateDifference(dto);
		//System.out.println("wcInstructionTabletimeDiff : "+wcInstructionTabletimeDiff);
		message = message + "<tr><td> HD_W_C_INSTRUCTION </td><td>"+wcInstructionTabletimeDiff+"</td></tr>" ;
		
		dto = getDetails(constant.personInfo);
		int personInfoTabletimeDiff = calculateDifference(dto);
		//System.out.println("personInfoTabletimeDiff"+personInfoTabletimeDiff);
		message = message + "<tr><td> YFS_PERSON_INFO </td><td>"+personInfoTabletimeDiff+"</td></tr>" ;
		
		dto = getDetails(constant.willCall);
		int willCallTabletimeDiff = calculateDifference(dto);
		//System.out.println("willCallTabletimeDiff : "+willCallTabletimeDiff);
		message = message + "<tr><td> HD_WILL_CALL </td><td>"+willCallTabletimeDiff+"</td></tr>" ;
		
		dto = getDetails(constant.splOrd);
		int splOrdTabletimeDiff = calculateDifference(dto);
		//System.out.println("splOrdTabletimeDiff : "+splOrdTabletimeDiff);
		message = message + "<tr><td> HD_SPL_ORD </td><td>"+splOrdTabletimeDiff+"</td></tr>" ;
		
		dto = getDetails(constant.hdEvent);
		int eventTabletimeDiff = calculateDifference(dto);
		//System.out.println("eventTabletimeDiff : "+eventTabletimeDiff);
		message = message + "<tr><td> HD_EVENT </td><td>"+eventTabletimeDiff+"</td></tr>" ;
		
		dto = getDetails(constant.yfsWO);
		int yfsWoTabletimeDiff = calculateDifference(dto);
		//System.out.println("YFS_WORK_ORDER : "+yfsWoTabletimeDiff);
		message = message + "<tr><td> YFS_WORK_ORDER </td><td>"+yfsWoTabletimeDiff+"</td></tr>" ;
		
		dto = getDetails(constant.relatedWO);
		int relatedWOTabletimeDiff = calculateDifference(dto);
		//System.out.println("RELATED WO  : "+relatedWOTabletimeDiff);
		message = message + "<tr><td> HD_RELATED_WORK_ORDER </td><td>"+relatedWOTabletimeDiff+"</td></tr>" ;
		
		message = message + "</table></center></body><html>";
		
		//System.out.println(message);
		if(headerTabletimeDiff >= 20 || inboxTabletimeDiff >= 20 || lineTabletimeDiff  >= 20 || instructionTabletimeDiff  >= 20 || wcInstructionTabletimeDiff  >= 20 || personInfoTabletimeDiff  >= 20 || willCallTabletimeDiff  >= 20 || splOrdTabletimeDiff  >= 20 || eventTabletimeDiff  >= 20 || yfsWoTabletimeDiff  >= 20 || relatedWOTabletimeDiff  >= 20)
		{
			sendReport(message);
		}
		
		return message;
		
		
	}
	
	public static LagCheckDTO getDetails(String query) 
	{
		LagCheckDTO dto = new LagCheckDTO();
		DateTimeFormatter formatter1 = DateTimeFormat.forPattern( "yy/MM/dd HH:mm:ss" );
		
		//System.out.println("running ODS");
		String odsTime = null;
		try {
			odsTime = getODSDB(query);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			logger.debug("Exception : "+e);
			e.printStackTrace();
		}
		
		odsTime = odsTime.replace("-", "/");
		DateTime ods = formatter1.parseDateTime( odsTime );
		
		dto.setOdsTime(ods);
		
		
		//System.out.println("running Sterling");
		String sterlingTime = null;
		
		
		
		query = query.replace("from ", "from thd01.");
		
		//System.out.println(query);
		
		try {
			sterlingTime = getSterlingDB(query);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			logger.debug("Exception : "+e);
			e.printStackTrace();
		}
		
		sterlingTime = sterlingTime.replace("-", "/");
		logger.debug("before"+sterlingTime);
		DateTime sterling = formatter1.parseDateTime( sterlingTime );
		logger.debug("thread-check"+sterling);
		
		dto.setSterlingTime(sterling);
		
		
		
		
		return dto;
		
		
	}

public static String getODSDB(String query) throws SQLException {
		
		DBConnectionUtil dbConn = new DBConnectionUtil();
				
				Connection odsCon = dbConn.getJNDIConnection(Constants.ODS_JNDI);
				
				Statement st = null;
				st = odsCon.createStatement();
				ResultSet result = null;
				
				
				
				result = st.executeQuery(query);
				
				DateFormat dateFormat = new SimpleDateFormat(
						"MM/dd/yyyy hh:mm:ss a");
				
				String odsDate = null;
				
				while (result.next()) 
				{
					 odsDate = result.getString(1);
				}
				
				result.close();
				st.close();
				odsCon.close();
				logger.debug("ODS Date : "+odsDate);
				
				return odsDate;
				
			}

			public static String getSterlingDB(String query) throws SQLException 
			{
				DBConnectionUtil dbConn = new DBConnectionUtil();
				
				Connection sterCon = dbConn.getJNDIConnection(Constants.STERLING_SSC_RO_JNDI);
				
				Statement st = null;
				st = sterCon.createStatement();
				ResultSet result = null;
				
				
				
				result = st.executeQuery(query);

				DateFormat dateFormat = new SimpleDateFormat(
						"MM/dd/yyyy hh:mm:ss a");
				
				String sterlingDate = null;
				
				while (result.next()) 
				{
					sterlingDate = result.getString(1);
					
				}
				
				result.close();
				st.close();
				sterCon.close();
				
				logger.debug("Sterling Date : "+sterlingDate);
				
				return sterlingDate;
			}
	
	public static int calculateDifference(LagCheckDTO dto)
	{
		//LagCheckDTO dto = new LagCheckDTO();
		logger.debug("sterling: " + dto.getSterlingTime());
		logger.debug("ods: " + dto.getOdsTime());
		Period period = new Period( dto.getOdsTime() , dto.getSterlingTime());
		PeriodFormatter periodFormatter = PeriodFormat.getDefault();
		String timeDiff = periodFormatter.print( period );
		int minutes = period.getMinutes();
		logger.debug(period.getMinutes());
		
		return minutes;
	}
	
	
	public static void sendReport(String msgContent) {
		
		final String user = "horizon@cpliisad.homedepot.com";// change
																// accordingly
		final String password = "xxx";// change accordingly

		String host = "mail1.homedepot.com";
		Properties props = new Properties();
		props.put("mail.host", host);
		Session session = Session.getInstance(props,
				new javax.mail.Authenticator() {
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(user, password);
					}
				});

		try {

			String[] to = {"_2fc77b@homedepot.com"};
			//String[] Cc = {""};
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress(user));
			DateFormat dateFormat = new SimpleDateFormat(
					"MM/dd/yyyy hh:mm:ss a");
			// get current date time with Date()
			Date today = new Date();
			String date1 = dateFormat.format(today);
			message.setSubject("Sterling/ODS 20 or more minutes lag in replication- " + date1);
			// message.setText("This is simple program of sending email using JavaMail API");
			message.setFrom(new InternetAddress(user));
			InternetAddress[] addressTo = new InternetAddress[to.length];
			//InternetAddress[] addressCc = new InternetAddress[Cc.length];
			for (int i = 0; i < to.length; i++) {
				addressTo[i] = new InternetAddress(to[i]);
				logger.debug("To Address " + to[i]);
			}
			/*for (int j = 0; j < Cc.length; j++) {
				addressCc[j] = new InternetAddress(Cc[j]);
				System.out.println("Cc Address " + Cc[j]);
			}*/
			message.setRecipients(RecipientType.TO, addressTo);
			//message.setRecipients(RecipientType.CC, addressCc);
			message.setContent(msgContent, "text/html");
			Transport.send(message);
			for (int i = 0; i < to.length; i++) {
				addressTo[i] = new InternetAddress(to[i]);
				logger.debug("Message delivered to--- " + to[i]);
			}
			/*for (int j = 0; j < Cc.length; j++) {
				addressCc[j] = new InternetAddress(Cc[j]);
				System.out.println("Message delivered to cc recepients--- "
						+ Cc[j]);
			}*/

		} catch (MessagingException e) {
			
			logger.debug("Exception : "+e);
			e.printStackTrace();
		}

	}	

}