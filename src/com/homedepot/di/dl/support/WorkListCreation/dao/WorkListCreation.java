package com.homedepot.di.dl.support.WorkListCreation.dao;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import com.homedepot.di.dl.support.WorkListCreation.dao.DBConnectionUtil;
import com.homedepot.di.dl.support.WorkListCreation.dao.SterlingJerseyClient;
import com.homedepot.di.dl.support.WorkListCreation.dao.Utils;
import com.homedepot.di.dl.support.util.Constants;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.sun.jersey.api.client.WebResource;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeMessage.RecipientType;
import org.apache.log4j.Logger;

public class WorkListCreation {
	final static Logger logger = Logger.getLogger(WorkListCreation.class);
                public static void main(String args[]) throws Exception {
                                
                                String workListCreate = workListCrt();
                                String workListUpdate = workListUpd();
                                
                               
                                
                              
                }
                
               

               
                public static String workListCrt() throws IOException, SQLException {

                                String table4 = "<br/>WorkList Create:<br/>"
                                                                + "<table border=\"1\" cellpadding=\"0\" cellspacing=\"0\" width=\"200px\" style=\"border-collapse:collapse;\"><tr><th>Order No</th><th>Line No</th></tr>";
                                String msgContent2 ="";

                                Connection con = null;
                                String sellCode = null;

                                try {
                                                DBConnectionUtil dbConn = new DBConnectionUtil();

                                                con = dbConn.getJNDIConnection(Constants.ODS_STAND_BY_JNDI);

                                                Statement stmt = null;
                                                ResultSet result = null;

                                                stmt = con.createStatement();

                                                // Below Query fetches the install orders to create worklist

                                                String wlCreate = "select hdr.order_header_key,line.order_line_key,line.extn_svs_line_status_eff_ts + 5,trim(hdr.seller_organization_code),hdr.EXTN_HOST_ORDER_REF,line.extn_host_order_line_ref "
                                                                                + "  from yfs_order_header hdr, yfs_order_line line "
                                                                                + "where hdr.document_type = '0001' "
                                                                                + "   and hdr.order_header_key = line.order_header_key "
                                                                                + "and line.ORDER_LINE_KEY > '20160412' "
                                                                                + "and line.ORDER_HEADER_KEY > '20160412' "
                                                                                + "and hdr.ORDER_HEADER_KEY > '20160412' " 

                                                                                + "and line.LINE_TYPE = 'IN' "
                                                                                + "and line.EXTN_SVS_LINE_STATUS = '352' "
                                                                                + "and order_line_key not in ( "
                                                                                + "select order_line_key from hd_inbox "
                                                                                + " where exception_type = '352' "
                                                                                + " and order_line_key = line.order_line_key) ";

                                                // Request xml forms the MultiApi to create worklist based on the output fetched from the above query.
                                                
                                                
                                                String requestXML = "<MultiApi>";

                                                int noOfwlCreated = 0;

                                                logger.info(wlCreate);
                                                result = stmt.executeQuery(wlCreate);

                                                while (result.next()) {

                                                                String orderNumber = result.getString(5).trim();
                                                                String LineNumber = result.getString(6).trim();
                                                                sellCode = result.getString(4).trim();

                                                               // System.out.println("getsring3" + result.getString(3).trim());
                                                                noOfwlCreated++;

                                                                Calendar cal = Calendar.getInstance();
                                                                DateFormat dateFormat = new SimpleDateFormat(
                                                                                                "yyyy-MM-dd'T'HH:mm:ss");
                                                                String SysDate = dateFormat.format(cal.getTime());

                                                                SimpleDateFormat formatter = new SimpleDateFormat(
                                                                                                "yyyy-MM-dd HH:mm:ss");
                                                                String SysDate1 = (result.getString(3).trim());
                                                                //System.out.println("SysDate1" + SysDate1);
                                                                String Str = SysDate1.substring(0, 10);
                                                                String Str1 = SysDate1.substring(11, 19);
                                                                String StrT = Str + "T" + Str1;
                                                                //System.out.println("StrT" + StrT);

                                                                table4 = table4 + "<tr><td>" + orderNumber + "</td><td>"
                                                                                                + LineNumber + "</td>" + "</tr>";

                                                                try {

                                                                                Date date = formatter.parse(SysDate1);

                                                                } catch (ParseException e) {
                                                                                e.printStackTrace();
                                                                }
                                                                
                                                                
                                                                requestXML = requestXML
                                                                                                + "<API FlowName='HDCreateHDInbox'>"
                                                                                                + "          <Input>                <Inbox AlertTransLevelType='INSTALL' Createprogid=''"
                                                                                                + " Createts='"
                                                                                                + SysDate
                                                                                                + "'"
                                                                                                + " Createuserid='' ErrorReason=''  ExceptionType='352'"

                                                                                                + " FollowupDate='" + StrT + "'" + " GeneratedOn='"
                                                                                                + SysDate + "'"
                                                                                                + "  InboxKey='' Modifyprogid='' Modifyts='"
                                                                                                + SysDate + "'"
                                                                                                + " Modifyuserid='COMSupport' OrderHeaderKey='"
                                                                                                + result.getString(1).trim() + "'" + " OrderLineKey ='"
                                                                                                + result.getString(2).trim() + "'"
                                                                                                + " SellerOrganizationCode='"
                                                                                                + result.getString(4).trim() + "'"
                                                                                                + " Status='OPEN' ShipnodeKey=''" + ">"
                                                                                                + "          <Extn ExtnAlertTransLevelType='INSTALL'"
                                                                                                + "/>" + "</Inbox></Input></API>";

                                                }

                                                requestXML = requestXML + "</MultiApi>";
                                                logger.debug("requestXML" + requestXML);
                                                logger.info("sellCode" + sellCode);

                                                
                                                
                                                if (sellCode == null) {
                                                	

                                                                logger.info(GregorianCalendar.getInstance().getTime()
                                                                                                + " : No Orders");

                                                }

                                                // Creating  worklist and sending mail
                                                
                                                else {

                                                                String responseXml = submitReturnReqToSterling(requestXML);

                                                                table4 = table4 + "</table></br>";
                                                                msgContent2 = msgContent2 + table4 +"</body></html>";

                                                              
                                                }
                                                result.close();
                                                stmt.close();
                                }

                                catch (Exception e) {
                                                e.printStackTrace();
                                }

                                con.close();
                                return msgContent2;

                }

                public static String submitReturnReqToSterling(String requestXML) {

                                String sterlingResponseXML = "";
                                String url = null;
                                String sterlingInputXML = null;

                                sterlingInputXML = requestXML;

                                String queryString = Utils.createQueryString("multiApi",
                                                                sterlingInputXML);
                                SterlingJerseyClient sterlingJerseyClient = new SterlingJerseyClient();

                                url = String
                                                                .format("http://multichannel-sterling.homedepot.com/smcfs/interop/InteropHttpServlet");
                                logger.info("URL : " + url);

                                sterlingResponseXML = sterlingJerseyClient.send(url, queryString);

                                logger.debug("Response : " + sterlingResponseXML);

                                sterlingResponseXML = Utils.removeXMLDeclaration(sterlingResponseXML);
                                return sterlingResponseXML;

                }
                
                
                
            	
    				
    				public static String workListUpd() throws Exception {
    					String table2 = "<br/>WorkList Update:<br/>" + "<table border=\"1\" cellpadding=\"0\" cellspacing=\"0\" width=\"200px\" style=\"border-collapse:collapse;\"><tr><th>Order No</th></tr>";
    					String msgContent1="";
    					String rowupd=null;
    					String UserOrder=null;
    					
    					int total = 0;
    					int rowsUpdated = 0 ;
    					try {
    					
    						DBConnectionUtil dbConn = new DBConnectionUtil();    						
    						Connection sscConn = dbConn.getJNDIConnection(Constants.ODS_STAND_BY_JNDI);
    						
    						Statement st = null;
    						st = sscConn.createStatement();
    						ResultSet result = null;
    						String wrkUpdate = " WITH HIB AS "
												+ " (SELECT INBOX_KEY, "
												+ "         ORDER_LINE_KEY "
												+ "  FROM HD_INBOX "
												+ "  WHERE ORDER_HEADER_KEY IS NULL "
												+ "    AND EXCEPTION_TYPE= '352' "
												+ "    AND status='OPEN' "
												+ "    AND INBOX_KEY > '20160518') ( "
												+ "    SELECT yoh.extn_host_order_ref,yol.ORDER_HEADER_KEY,yoh.seller_organization_code ,HIB.INBOX_KEY "
												+ "      from yfs_order_header yoh , yfs_order_line yol,HIB where yoh.order_header_key = yol.order_header_key and HIB.ORDER_LINE_KEY =  yol.ORDER_LINE_KEY and " + " yoh.document_type ='0001')";
												


			logger.info(wrkUpdate);
			result = st.executeQuery(wrkUpdate);
			while (result.next()) {
				//System.out.println("Output:" + result.getString(1));
				UserOrder = result.getString(1);

				table2 = table2 + "<tr><td>" + UserOrder + "</td></tr>";

				Connection atcConn = dbConn
						.getJNDIConnection(Constants.STERLING_ATC_RW_JNDI);

				final String UpdQuery = " update thd01.HD_INBOX set order_header_key  ='"
						+ result.getString(2).trim()
						+ "' , SELLER_ORGANIZATION_CODE ='"
						+ result.getString(3).trim()
						+ "' , modifyts=sysdate, modifyuserid='NullOrderHeaderUpdate' where INBOX_KEY='"
						+ result.getString(4).trim() + "' ";

    							
    							Statement stmt1 = null;
    							stmt1 = atcConn.createStatement();
    							logger.debug(UpdQuery);
    							rowsUpdated = stmt1.executeUpdate(UpdQuery);
    							total = total + rowsUpdated;
    							atcConn.commit();
    							stmt1.close();
    							atcConn.close();
    							logger.info(rowsUpdated + " Rows updated");
    							

    						}
        					result.close();
        					st.close();
        					sscConn.close();
    					
    					if(UserOrder==null)
    					{
    						  logger.info(GregorianCalendar.getInstance().getTime()
                                      + " : No Orders");	
    					}
    					else
    					{
    						rowupd="Number of rows updated:" + total;
    						logger.info("Number of rows updated" + rowupd );
        					
        					table2 = table2 + "</table></br>";
        					msgContent1 = msgContent1 + table2 + "</body></html>";	
    					}
        					
    						
    					} catch (Exception e) {
    						logger.error("Error occured while Executing WorkListUpdate Servlet  "
    										+ GregorianCalendar.getInstance());
    						e.printStackTrace();
    					}
						return msgContent1;
    					
    					
    				}
    				


               

}
