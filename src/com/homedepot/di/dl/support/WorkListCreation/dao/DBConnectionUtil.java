package com.homedepot.di.dl.support.WorkListCreation.dao;

import java.sql.*;

import javax.naming.*;
import javax.sql.*;
import org.apache.log4j.Logger;

public class DBConnectionUtil extends Constants {
	private static final Logger logger = Logger
			.getLogger(DBConnectionUtil.class);

	public Connection getJNDIConnection(String contextName) {
		String DATASOURCE_CONTEXT = contextName;

		Connection conn = null;
		try {
			Context initialContext = new InitialContext();
			Context envContext = (Context) initialContext
					.lookup("java:comp/env");

			DataSource datasource = (DataSource) envContext
					.lookup(DATASOURCE_CONTEXT);
			if (datasource != null) {
				conn = datasource.getConnection();
				logger.info("Connected: " + conn);
			} else {
				logger.info("Failed to lookup datasource.");
			}
		} catch (NamingException ex) {
			logger.info("Cannot get connection: " + ex);
		} catch (SQLException ex) {
			logger.info("Cannot get connection: " + ex);
		}
		return conn;
	}

}

