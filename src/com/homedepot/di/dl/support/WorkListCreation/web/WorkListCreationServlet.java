package com.homedepot.di.dl.support.WorkListCreation.web;

import java.io.IOException;
import java.util.GregorianCalendar;
import java.util.Properties;

import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeMessage.RecipientType;
import javax.servlet.ServletException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.homedepot.di.dl.support.WorkListCreation.dao.WorkListCreation;
import org.apache.log4j.Logger;

/**
 * Servlet implementation class workListInboxCreationServlet
 */

public class WorkListCreationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	final static Logger logger = Logger.getLogger(WorkListCreationServlet.class);

       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public WorkListCreationServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		WorkListCreation l=new WorkListCreation();
		try {
			
			 
			String workListCreate=l.workListCrt();
			String workListUpdate=l.workListUpd();
			
			logger.info("workListCreate"+ workListCreate);
			logger.info("workListUpdate"+ workListUpdate);
			
			 String emailPart=null;
			 
			 emailPart=workListCreate+workListUpdate;
			 logger.info("emailPart"+ emailPart);
			 if(emailPart.equalsIgnoreCase(""))
			 {
				 logger.info(GregorianCalendar.getInstance().getTime()
                         + " : No Orders");
			 
			 }
			 else
			 {
				 logger.info("ElsePart"+ emailPart);
				 sendAttachment(emailPart);
			 }
			 
				 
			
			 logger.info(GregorianCalendar.getInstance().getTime() +" : Program Completed");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}
	
	 public static void sendAttachment(String emailPart) throws Exception {
         String host = "mail1.homedepot.com";
         String from = "horizon@cpliisad.homedepot.com";
         String[] to = { "_2fc77b@homedepot.com" };
        // String[] to = { "ANANDHAKUMAR_CHINNADURAI@homedepot.com"};
         // Get system properties
         Properties properties = System.getProperties();

         // Setup mail server
         properties.setProperty("mail.smtp.host", host);

         // Get the default Session object.
         Session session = Session.getInstance(properties);

         // Define message
         Message message = new MimeMessage(session);
         message.setFrom(new InternetAddress(from));
         InternetAddress[] addressTo = new InternetAddress[to.length];
         // InternetAddress[] addressCc = new InternetAddress[Cc.length];
         for (int i = 0; i < to.length; i++) {
                         addressTo[i] = new InternetAddress(to[i]);
                         logger.debug("To Address " + to[i]);
         }

         message.setRecipients(RecipientType.TO, addressTo);

         message.setSubject("Worklist Creation/Updating for Install Orders - COM-4893");

         BodyPart messageBodyPart = new MimeBodyPart();
         String msgContent = "Hi All,<br/><br/>PFB Install Orders for which WorkList has been Created/Updated<br/>";
         msgContent = msgContent + emailPart
                                         + "Thanks<br/> COM Multichannel Support";

         messageBodyPart.setContent(msgContent, "text/html");

         Multipart multipart = new MimeMultipart();
         multipart.addBodyPart(messageBodyPart);

         message.setContent(multipart);

         // Send the message
         Transport.send(message);
         logger.info("Msg Send ....");
}

}
