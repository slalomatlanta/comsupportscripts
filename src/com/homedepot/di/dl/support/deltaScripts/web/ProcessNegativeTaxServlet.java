package com.homedepot.di.dl.support.deltaScripts.web;

import java.io.IOException;
import java.util.GregorianCalendar;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.homedepot.di.dl.support.deltaScripts.service.ProcessNegativeTax;

/**
 * Servlet implementation class ProcessNegativeTaxServlet
 */
public class ProcessNegativeTaxServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = Logger
			.getLogger(ProcessNegativeTaxServlet.class);
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ProcessNegativeTaxServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		logger.debug("ProcessNegativeTax Program Started at "+java.util.GregorianCalendar.getInstance().getTime());
		try {
			ProcessNegativeTax.getOrdersFromCSV();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.debug("ProcessNegativeTaxServlet::"+GregorianCalendar.getInstance().getTime()+e);
		}
		logger.debug("ProcessNegativeTax Program Completed at "+java.util.GregorianCalendar.getInstance().getTime());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
