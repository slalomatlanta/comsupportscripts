package com.homedepot.di.dl.support.deltaScripts.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.homedepot.di.dl.support.deltaScripts.service.TransStillInProgressToN;

/**
 * Servlet implementation class TransStillInProgressToNServlet
 */
public class TransStillInProgressToNServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = Logger
			.getLogger(TransStillInProgressToNServlet.class);
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TransStillInProgressToNServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		logger.debug("UpdateDeltasToN Program Started at "+java.util.GregorianCalendar.getInstance().getTime());
		TransStillInProgressToN.updateDeltas();
		logger.debug("UpdateDeltasToN Program Completed at "+java.util.GregorianCalendar.getInstance().getTime());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
