package com.homedepot.di.dl.support.deltaScripts.service;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;



public class GetOrderDetailsFromDB {


	/**
	 * This method will collect the order number - line No combination and form it as String builder.
	 * And it gets the DB results in map and send back to the caller 
	 * @throws Exception 
	 * */
	public static Map<String, COMReleaseDTO> getDetailsFromSterling(String orderNumber, String lineRef) throws Exception {

		Map<String, COMReleaseDTO> finalMap = new HashMap<String, COMReleaseDTO>();

		// final String driverClass = "oracle.jdbc.driver.OracleDriver";
		final String connectionURL = "jdbc:oracle:thin:@(DESCRIPTION_LIST=(LOAD_BALANCE=off)(FAILOVER=on)(DESCRIPTION=(ADDRESS_LIST=(LOAD_BALANCE=on)(ADDRESS=(PROTOCOL=TCP)(HOST=spragor10-scan.homedepot.com)(PORT=1521)))(CONNECT_DATA=(SERVICE_NAME=dpr77mm_sro01)))(DESCRIPTION=(ADDRESS_LIST=(LOAD_BALANCE=on)(ADDRESS=(PROTOCOL=TCP)(HOST=pprmm78x.homedepot.com)(PORT=1521)))(CONNECT_DATA=(SERVICE_NAME=dpr77mm_sro01))))";
		final String uName = "MMUSR01";
		final String uPassword = "COMS_MMUSR01";
		

		final String query = "select yol.extn_sku_code, ysl.product_class, yoh.extn_host_order_ref, " +
				"trim(yol.shipnode_key), trim(yoh.seller_organization_code) as sellingStore, yol.extn_host_order_line_ref,  yoh.document_type, trim(yoh.enterprise_key), " +
				"ysl.shipment_line_no, ysl.quantity, to_char(yoh.order_date,'yyyy-mm-dd')||'T'||to_char(yoh.order_date,'hh24:mi:ss')||'Z', " +
				"yol.delivery_method, yol.line_type, yol.extn_release_type, yol.extn_will_call_line_num, yol.extn_create_src_process, trim(ys.shipment_no) " +
				"from THD01.yfs_order_header yoh, THD01.yfs_order_line yol, THD01.yfs_shipment ys, THD01.yfs_shipment_line ysl, " +
				"THD01.yfs_order_release_status yrs " +
				"where yol.order_header_key = yoh.order_header_key and ysl.order_header_key = yoh.order_header_key " +
				"and yol.order_line_key = ysl.order_line_key and yol.line_type is not null and yol.order_line_key = yrs.order_line_key " +
				"and ysl.is_pickable = 'Y' and ysl.shipment_key = ys.shipment_key  and (yoh.extn_host_order_ref, yol.extn_host_order_line_ref) in (('" +
				orderNumber + "', '" + lineRef + 
				"')) and ysl.quantity > 0 and yrs.status in ('3350.200', '3350') " +
				"and ys.status < '1400' and yol.delivery_method = 'PICK' " +
				"group by yol.extn_sku_code, ysl.product_class,  yoh.extn_host_order_ref, " +
				"yol.shipnode_key, yoh.seller_organization_code, yol.extn_host_order_line_ref, " +
				"yoh.document_type, yoh.enterprise_key, ysl.shipment_line_no, ysl.quantity, yoh.order_date, yol.delivery_method, yol.line_type, " +
				"yol.extn_release_type, yol.extn_will_call_line_num, yol.extn_create_src_process, ys.shipment_no";

		//System.out.println(query);
		Connection con = null;
		try {
			
			con = DriverManager.getConnection(connectionURL, uName, uPassword);
			// System.out.println("Sterling Connection Opened");
			Statement stmt = null;
			stmt = con.createStatement();
			ResultSet result = null;
			result = stmt.executeQuery(query);
			while (result.next()) {
				
				COMReleaseDTO cOMReleaseDTO = new COMReleaseDTO();
				cOMReleaseDTO.setExtnSkuCode(isEmpty(result.getString(1)));
				cOMReleaseDTO.setProductClass(isEmpty(result.getString(2)));
				cOMReleaseDTO.setExtnHostOrderRef(result.getString(3));
				cOMReleaseDTO.setShipnodeKey(result.getString(4));
				cOMReleaseDTO.setSellingStore(result.getString(5));
				cOMReleaseDTO.setExtnHostOrderLineRef(result.getString(6));
				cOMReleaseDTO.setDocumentType(result.getString(7));
				cOMReleaseDTO.setEnterpriseKey(result.getString(8));
				cOMReleaseDTO.setShipmentLineNo(result.getString(9));
				cOMReleaseDTO.setQuantity(result.getString(10));
				cOMReleaseDTO.setOrderDate(result.getString(11));
				cOMReleaseDTO.setDeliveryMethod(result.getString(12));
				cOMReleaseDTO.setLineType(result.getString(13));
				cOMReleaseDTO.setExtnReleaseType(result.getString(14));
				cOMReleaseDTO.setExtnWillCallLineNum(result.getString(15));
				cOMReleaseDTO.setExtnCreateSrcProcess(result.getString(16));
				cOMReleaseDTO.setShipmentNo(result.getString(17));
				
				
				String key = cOMReleaseDTO.getExtnHostOrderRef() + "-"
						+ cOMReleaseDTO.getExtnHostOrderLineRef();
				finalMap.put(key, cOMReleaseDTO);
			}
			stmt.close();
			result.close();
		} catch (Exception e) {
			throw e;
			
		}finally{
			con.close();
		}

		return finalMap;
	}


	/**
	 * This method check whether the given string is null, if not, the input string will be trimmed.
	 * */
	public static String isEmpty(String str) {
		if (str == null) {
			str = "";
		} else {
			str = str.trim();
		}
		return str;
	}

}

