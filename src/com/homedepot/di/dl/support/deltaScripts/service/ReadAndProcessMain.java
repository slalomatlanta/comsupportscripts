package com.homedepot.di.dl.support.deltaScripts.service;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;

import com.homedepot.di.dl.support.util.DBConnectionUtil;

public class ReadAndProcessMain {

	private static final Class DocumentBuilderFactory = null;

	/**
	 * @param args
	 */
	public static void main(String[] args)
	
	{
		// TODO Auto-generated method stub

        String fileLocn = "c:\\Tax\\storeOrderlist.csv";
        
        try {
                        System.out.println(getStoreOrdersFromCSV(fileLocn)+" total rows deleted");
        } catch (Exception e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
        }


	}
	
	public static int getStoreOrdersFromCSV(
            String fileLocn) throws Exception {



int totalNoOfRowsUpdated = 0;
try {
            BufferedReader br = new BufferedReader(new FileReader(fileLocn));

            String line = "";
            
            while ((line = br.readLine()) != null) {
                            if (!(",,".equalsIgnoreCase(line))) {
                                            int noOfRowsUpdated = 0;
                                            String tokens[] = line.split(",");
                                            String store = tokens[0].trim();
                                            String orderNumber = tokens[1].trim();
                                            System.out.println(store);
                                            int len = store.length();
                                            if (len < 4) {
                                                            for (int i = 0; i < (4 - len); i++) {
                                                                            store = "0" + store;
                                                            }
                                            }
                                            
                                             
                                            noOfRowsUpdated = getDeltas(orderNumber,store);
                                            //System.out.println(store+"  "+noOfRowsUpdated+" rows deleted");
                                            totalNoOfRowsUpdated += noOfRowsUpdated;
                            }
            }
            br.close();
} catch (Exception e) {
            e.printStackTrace();
            throw e;

}

return totalNoOfRowsUpdated;
}
	
	public static int getDeltas(String orderNumber, String store) throws Exception {
    	
		//ParseDocForNegativeCharge parse = new ParseDocForNegativeCharge();
		
			
		ParseDocForQuantityToCancel parse = new ParseDocForQuantityToCancel();
		
		//String outputFile = "c:\\Tax\\Response.csv";
				
				//FileWriter fw = new FileWriter(outputFile, true);
				
		                    int noOfRowsUpdated = 0;
		                    
		                    Connection con = null;
		                    Statement stmt = null;
		                    ResultSet result = null;
		                    String modifiedXML = null;
		                    String query = "select * from comt_ord_chg_msg where cust_ord_id in ('"+orderNumber+"') and trnsm_stat_ind = 'E'";
		                    /*fw.append(orderNumber);
		                	fw.append(',');
		                	fw.append(store);
		                	fw.append(',');*/
		                    try {

		                    	DBConnectionUtil dbConn = new DBConnectionUtil();
		                    	con = dbConn.getStoreconnection(store);
		                                    stmt = con.createStatement();
		                                    result = stmt.executeQuery(query);
		                                    
		                                    while(result.next())
		                                    {
		                                    	
		                                    	String xml = result.getString(7);
		                                    	//System.out.println("Before : "+xml);
		                                    	
		                                    	modifiedXML = parse.parseDoc(xml,store,orderNumber);
		                                    	
		                                    	//System.out.println("After : "+modifiedXML);    
		                                    		                        				 
		                        			}
		                                    
		                                    stmt.close();
		                                    
		                                    con.close();

		                    } catch (Exception e) {

		                                    e.printStackTrace();
		                                    

		                    }
		                    //fw.close();
		                    return noOfRowsUpdated;
		                    
	}

}
