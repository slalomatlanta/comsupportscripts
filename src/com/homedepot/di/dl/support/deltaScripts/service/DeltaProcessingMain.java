package com.homedepot.di.dl.support.deltaScripts.service;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;

public class DeltaProcessingMain {

	/**
	 * @param args
	 */
	public static void main(String[] args)

	{
		// TODO Auto-generated method stub

		String fileLocn = "c:\\Tax\\storeOrderlist.csv";

		try {
			System.out.println(getStoreOrdersFromCSV(fileLocn)
					+ " total rows deleted");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static int getStoreOrdersFromCSV(String fileLocn) throws Exception {

		int totalNoOfRowsUpdated = 0;
		try {
			BufferedReader br = new BufferedReader(new FileReader(fileLocn));

			String line = "";

			while ((line = br.readLine()) != null) {
				if (!(",,".equalsIgnoreCase(line))) {
					int noOfRowsUpdated = 0;
					String tokens[] = line.split(",");
					String store = tokens[0].trim();
					String orderNumber = tokens[1].trim();
					System.out.println(store);
					int len = store.length();
					if (len < 4) {
						for (int i = 0; i < (4 - len); i++) {
							store = "0" + store;
						}
					}
					System.out.println(orderNumber);
					noOfRowsUpdated = getDeltas(orderNumber, store);
					System.out.println(store + "  " + noOfRowsUpdated
							+ " rows deleted");
					totalNoOfRowsUpdated += noOfRowsUpdated;
					System.out.print("\n");
				}
			}
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
			throw e;

		}

		return totalNoOfRowsUpdated;
	}

	public static int getDeltas(String orderNumber, String store)
			throws Exception {

		// ParseDocForNegativeCharge parse = new ParseDocForNegativeCharge();
		ParseDocForQuantityToCancel parse = new ParseDocForQuantityToCancel();
		// ParseDocForNotEnoughQuantity parse = new ParseDocForNotEnoughQuantity();
		// ParseDocForErrorDescNotAvailbl parse = new ParseDocForErrorDescNotAvailbl();
		// ParseDocForCannotUnconfirmShipmnt parse = new ParseDocForCannotUnconfirmShipmnt();
		// ParseDocForRecordDoesNotExist parse = new ParseDocForRecordDoesNotExist();
		// ParseDocForYNEQ parse = new ParseDocForYNEQ();

		ProcessXML process = new ProcessXML();
		CreateLock lockObj = new CreateLock();

		String outputFile = "c:\\Tax\\Response.csv";
		String inputXMLBckup = "c:\\Tax\\InputBackup.xml";
		String modifiedXMLBckup = "c:\\Tax\\ModifiedBackup.xml";

		FileWriter fw = new FileWriter(outputFile, true);
		FileWriter fw1 = new FileWriter(inputXMLBckup, true);
		FileWriter fw2 = new FileWriter(modifiedXMLBckup, true);

		int noOfRowsUpdated = 0;
		DBConnectionUtil conn = new DBConnectionUtil();

		Connection con = null;
		Statement stmt = null;
		ResultSet result = null;
		String modifiedXML = null;

		String query = "select * from comt_ord_chg_msg where cust_ord_id in ('"
				+ orderNumber + "') and trnsm_stat_ind in ('E', 'F', 'L')"; // order
																			// by
																			// chg_seq_nbr";

		fw.append(orderNumber);
		fw.append(',');
		fw.append(store);
		fw.append(',');

		try {

			con = conn.getStoreconnection(store);

			stmt = con.createStatement();
			result = stmt.executeQuery(query);

			while (result.next()) {

				String xml = result.getString(7);
				// System.out.println("Before : "+xml);
				// fw1.append(xml);
				modifiedXML = parse.parseDoc(xml, store, orderNumber);
				// modifiedXML = xml;
				// fw2.append(modifiedXML);
				String dupStr = modifiedXML;

				// System.out.println("After : "+modifiedXML);

				String newLockid = lockObj.applyLockOnOrder(orderNumber, store);

				if (modifiedXML.contains("ExtnLockID")) {
					int firstIndex = dupStr.indexOf("ExtnLockID");
					firstIndex = firstIndex + 12;
					int lastIndex = firstIndex + 9;
					String lockId = dupStr.substring(firstIndex, lastIndex);
					modifiedXML = modifiedXML.replace(lockId, newLockid);
				}

				else {

					modifiedXML = modifiedXML.replaceFirst("<Extn",
							"<Extn ExtnLockID=\"" + newLockid + "\"");
				}
				// System.out.println("XML to be processed : "+modifiedXML);

				String responseXML = null;

				if (modifiedXML
						.contains("ExtnDerivedTransactionType=\"ORDER_CREATE\"")) {

					fw.append("ORDER_CREATE");
					fw.append(',');
					responseXML = process.callTocreateOrder(modifiedXML);
				}

				else if (modifiedXML
						.contains("ExtnDerivedTransactionType=\"ORDER_MODIFY\"")) {
					fw.append("ORDER_MODIFY");
					fw.append(',');
					responseXML = process.callTomodifyOrder(modifiedXML);
				}

				else if (modifiedXML.contains("<UnlockOrderRequest>")) {
					responseXML = "ResponseDescription=\"SUCCESS\">";
				}

				String response = CheckResponseType(responseXML);
				fw.append(response);
				
				 if(!(modifiedXML.contains("<OrderLines>"))){ 
					 fw.append(',');
					 fw.append("DeleteNeeded");
				 if(modifiedXML.contains("<PaymentMethods>")){ 
					 fw.append(',');
					 fw.append("PaymentNeeded"); 
					 } 
				 }
				 
				//fw.append("\n");
				System.out.println(response);

				if (response.equalsIgnoreCase("SUCCESS")) {
					noOfRowsUpdated = deleteDelta(orderNumber, store);

				} else {
					System.out.println(orderNumber
							+ " : XML was not processed successfully : "
							+ response);
				}

				fw.append("\n");
			}

			stmt.close();

			con.close();

		} catch (Exception e) {

			e.printStackTrace();

		}
		fw.close();
		return noOfRowsUpdated;

	}

	public static int deleteDelta(String orderNumber, String store)
			throws Exception {

		int noOfRowsUpdated = 0;
		DBConnectionUtil conn = new DBConnectionUtil();

		String query = "delete from comt_ord_chg_msg where cust_ord_id in ('"
				+ orderNumber + "') and trnsm_stat_ind in ('E', 'F', 'L')";
		Connection con = null;
		try {

			con = conn.getStoreconnection(store);

			Statement stmt = null;
			stmt = con.createStatement();
			noOfRowsUpdated = stmt.executeUpdate(query);

			stmt.close();
			if (!con.getAutoCommit()) {
				con.commit();
			}
			con.close();

			System.out.println("Delta deleted");

		} catch (Exception e) {

			System.out.println("unable to connect to store : " + store);
			System.out.println(e.getMessage());

		}

		return noOfRowsUpdated;
	}

	private static String CheckResponseType(String response) {

		String result = "";
		if (response == null || "".equalsIgnoreCase(response)) {
			result = "No response";
		} else if (response.contains("ResponseDescription=\"SUCCESS\">")) {
			result = "SUCCESS";
		} else if (response.contains("Error")) {
			int firstIndex = response.indexOf("ResponseDescription");
			firstIndex = firstIndex + 21;
			int lastIndex = response.indexOf("<Errors>");
			result = "Failure - "
					+ response.substring(firstIndex, lastIndex - 5);
		}

		System.out.println("Response status : " + result);
		return result;

	}

}
