package com.homedepot.di.dl.support.deltaScripts.service;

import java.io.FileWriter;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class ParseDocForErrorDescNotAvailbl {
	/**
	 * @param args
	 * @throws TransformerException
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws IOException
	 */
	public static String parseDoc(String xml, String store, String orderNumber)
			throws TransformerException, ParserConfigurationException,
			SAXException, IOException {
		
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(new InputSource(new StringReader(xml)));

		Element orders = doc.getDocumentElement();
		Node ordersNode = (Node) orders;
		
		Node extnNode = (Node) doc.createElement("Extn");
		
		
		if(!(ordersNode.getFirstChild().getNodeName().equals("Extn"))){
			ordersNode.insertBefore(extnNode, ordersNode.getFirstChild());
		}
		
		String modifiedXML = getStringFromDocument(doc);
		String outputFile = "c:\\Tax\\Response\\" + orderNumber + ".xml";
		FileWriter fw = new FileWriter(outputFile, true);
		fw.write(modifiedXML);
		fw.close();
		return modifiedXML;
	}
	
	public static String getStringFromDocument(Document doc)
			throws TransformerException {
		DOMSource domSource = new DOMSource(doc);
		StringWriter writer = new StringWriter();
		StreamResult result = new StreamResult(writer);
		TransformerFactory tf = TransformerFactory.newInstance();
		Transformer transformer = tf.newTransformer();
		transformer.transform(domSource, result);
		return writer.toString();
	}
}
