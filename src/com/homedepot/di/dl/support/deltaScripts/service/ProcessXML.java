package com.homedepot.di.dl.support.deltaScripts.service;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;

public class ProcessXML {

	public static String callTocreateOrder(String inputXML) {
		String extnLockID = null;
		String envName = "PR";
		String url = "http://multichannel-custom.homedepot.com/COMOrder/rs/createSalesOrderForServicesSynchronously";

		String headerValue = "F4EJPD2Z7TbyLUP4llcrQsxPaE6yMjM0y8KkbvP8cwCN9YKBs7Qxdm3lrRppaPiGzrq7djLoSp5IfA8pJMFYtuVCHsTWEX4W6v5GpfL6E8J57j0ZfJQmhwbZJla1cbzPKvaAShYH1KIxhhmxg1RsesWqVH54fyPZRviJFURRVhfx0FuqPYxdaaGSL4GgnPym1da7lti6bOblN0EOxHK45fm5gkf5vV3eQmNaq5C7NqtSVbiaMIZUJgkGTdrdeY7HVqx7FmFNj8rphDVQuNfeUHSt2euMu6zeB4y";

		String headerName = "THDService-Auth";
		String response = null;

		try {
			Client client = ClientManager.getClient();

			WebResource webResource = ((com.sun.jersey.api.client.Client) client)
					.resource(url);

			response = webResource.header(headerName, headerValue)
					.type("text/xml").post(String.class, inputXML);

			// System.out.println("orderRecallNoLock : " + orderRecallNoLock);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}

	public static String callTomodifyOrder(String inputXML) {
		String extnLockID = null;
		String envName = "PR";
		String url = "http://multichannel-custom.homedepot.com/COMOrder/rs/modifySalesOrderForServicesSynchronously";

		String headerValue = "F4EJPD2Z7TbyLUP4llcrQsxPaE6yMjM0y8KkbvP8cwCN9YKBs7Qxdm3lrRppaPiGzrq7djLoSp5IfA8pJMFYtuVCHsTWEX4W6v5GpfL6E8J57j0ZfJQmhwbZJla1cbzPKvaAShYH1KIxhhmxg1RsesWqVH54fyPZRviJFURRVhfx0FuqPYxdaaGSL4GgnPym1da7lti6bOblN0EOxHK45fm5gkf5vV3eQmNaq5C7NqtSVbiaMIZUJgkGTdrdeY7HVqx7FmFNj8rphDVQuNfeUHSt2euMu6zeB4y";

		String headerName = "THDService-Auth";
		String response = null;

		try {
			Client client = ClientManager.getClient();

			WebResource webResource = ((com.sun.jersey.api.client.Client) client)
					.resource(url);

			response = webResource.header(headerName, headerValue)
					.type("text/xml").post(String.class, inputXML);

			// System.out.println("orderRecallNoLock : " + orderRecallNoLock);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}

}
