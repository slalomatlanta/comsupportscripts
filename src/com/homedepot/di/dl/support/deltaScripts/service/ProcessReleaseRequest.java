package com.homedepot.di.dl.support.deltaScripts.service;

import java.util.Map;

public class ProcessReleaseRequest {
	

	/**
	 * @param args
	 */
	public static String processReleaseShipment(String orderNumber,
			String lineRef) {
		
		String status = null;
		
		try {

			Map<String, COMReleaseDTO> inputMap = GetOrderDetailsFromDB
					.getDetailsFromSterling(orderNumber, lineRef);

			ProcessXML process = new ProcessXML();
			CreateLock lockObj = new CreateLock();

			for (Map.Entry<String, COMReleaseDTO> entry : inputMap.entrySet()) {
				System.out.println("Releaseing the order-line : "
						+ entry.getKey());
				COMReleaseDTO request = entry.getValue();
				
				//String extnLockID;
				//System.out.println("ExtnlockId" + extnLockID);
				String extnLockID = lockObj
						.applyLockOnOrder(request.getExtnHostOrderRef(),
								request.getShipnodeKey().trim());

				if (!(extnLockID.contains("getOrderDetail failed"))) {

					String requestXML = "<Orders>" + "	<Extn ExtnLockID='"
							+ extnLockID
							+ "'/>"
							+ "	<Order DocumentType='"
							+ request.getDocumentType()
							+ "' EnterpriseCode='"
							+ request.getEnterpriseKey()
							+ "'>"
							+ "		<Extn ExtnDerivedTransactionType='ORDER_MODIFY' ExtnHostOrderReference='"
							+ request.getExtnHostOrderRef()
							+ "' DocumentType='"
							+ request.getDocumentType()
							+ "' "
							+ "EnterpriseCode='"
							+ request.getEnterpriseKey()
							+ "' SellerOrganizationCode='"
							+ request.getSellingStore()
							+ "'/>"
							+ "		<OrderLines>"
							+ "			<OrderLine ShipNode='"
							+ request.getShipnodeKey().trim()
							+ "' LineType='"
							+ request.getLineType()
							+ "'>"
							+ "				<Extn ExtnHostOrderLineReference='"
							+ request.getExtnHostOrderLineRef()
							+ "' ExtnTransactionType='RELEASE' ExtnSKUCode='"
							+ request.getExtnSkuCode()
							+ "' "
							+ "ExtnProductTypeCode='PRODUCT' ExtnHostUserID='Admin' ExtnSellingLocation='"
							+ request.getSellingStore()
							+ "' ExtnModifySrcProcess='0' >"
							+ "				</Extn>"
							+ "			</OrderLine>"
							+ "		</OrderLines>"
							+ "	</Order>"
							+ "	<Shipment DocumentType='"
							+ request.getDocumentType()
							+ "' EnterpriseCode='"
							+ request.getEnterpriseKey()
							+ "' SellerOrganizationCode='"
							+ request.getSellingStore()
							+ "' "
							+ "ShipNode='"
							+ request.getShipnodeKey().trim()
							+ "' ShipmentNo='"
							+ request.getShipmentNo().trim()
							+ "'>"
							+ "		<Extn ExtnHostOrderReference='"
							+ request.getExtnHostOrderRef()
							+ "' ExtnDerivedTransactionType='RELEASE' ExtnSignatureBlob='abcd'/>"
							+ "		<ShipmentLines>"
							+ "			<ShipmentLine DocumentType='"
							+ request.getDocumentType()
							+ "' LineType='"
							+ request.getLineType()
							+ "' Quantity='"
							+ request.getQuantity()
							+ "' "
							+ "ProductClass='"
							+ request.getProductClass()
							+ "' ShipmentLineNo='"
							+ request.getShipmentLineNo()
							+ "' >"
							+ "				<Extn ExtnModifySrcProcess='0' ExtnHostOrderLineReference='"
							+ request.getExtnHostOrderLineRef()
							+ "' ExtnTransactionType='RELEASE' "
							+ "ExtnSKUCode='"
							+ request.getExtnSkuCode()
							+ "' ExtnPOSItemType='SKU' ExtnHostUserID='Admin'/>"
							+ "			</ShipmentLine>" + "		</ShipmentLines>"
							+ "	</Shipment>" + "</Orders>";
					//System.out.println("Request : " + requestXML);
					String response = process.callTomodifyOrder(requestXML);
					status = CheckResponseType(response);
					request.setStatusOfRelease(status);

				} else {
					System.out.println(extnLockID);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return "SUCCESS";
	}

	/**
	 * Method to check the response whether it is a success or failure
	 * */
	private static String CheckResponseType(String response) {

		String result = "";
		if (response == null || "".equalsIgnoreCase(response)) {
			result = "No response";
		} else if (response.contains("ResponseDescription=\"SUCCESS\">")) {
			result = "SUCCESS";
		} else if (response.contains("Error")) {
			int firstIndex = response.indexOf("ResponseDescription");
			firstIndex = firstIndex + 21;
			int lastIndex = response.indexOf("<Errors>");
			result = "Failure - "
					+ response.substring(firstIndex, lastIndex - 5);
		}

		System.out.println("Response status : " + result);
		return result;

	}

}
