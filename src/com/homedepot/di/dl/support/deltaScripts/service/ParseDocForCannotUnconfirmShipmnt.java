package com.homedepot.di.dl.support.deltaScripts.service;

import java.io.FileWriter;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class ParseDocForCannotUnconfirmShipmnt {

	public static String parseDoc(String xml, String store, String orderNumber)
			throws TransformerException, ParserConfigurationException,
			SAXException, IOException {

		ArrayList<String> unreleaseLineList = new ArrayList<String>();
		Map<String, Double> returnLineDetails = new HashMap<String, Double>();

		String docType = null;

		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		// Document doc = dBuilder.parse(new InputSource(xml));
		Document doc = dBuilder.parse(new InputSource(new StringReader(xml)));
		// System.out.println("File read" + doc);

		Element orders = doc.getDocumentElement();
		Node ordersNode = (Node) orders;
		NodeList orderList = orders.getElementsByTagName("Order");

		NodeList shipmentList = orders.getElementsByTagName("Shipment");

		try {
			if (shipmentList != null) {
				for (int i = 0; i < shipmentList.getLength(); i++) {
					Element shipment = (Element) shipmentList.item(i);
					NodeList shipmentExtnList = shipment
							.getElementsByTagName("Extn");
					Element shipmentExtn = (Element) shipmentExtnList.item(0);
					/*System.out.println(shipmentExtn
							.getAttribute("ExtnDerivedTransactionType"));*/
					if (shipmentExtn.getAttribute("ExtnDerivedTransactionType")
							.equalsIgnoreCase("UNRELEASE")) {
						NodeList shipmentLinesList = shipment
								.getElementsByTagName("ShipmentLines");
						Element shipmentLine = (Element) shipmentLinesList
								.item(0);
						NodeList shipmentLineList = shipmentLine
								.getElementsByTagName("ShipmentLine");
						for (int j = 0; j < shipmentLineList.getLength(); j++) {
							Element releaseLine = (Element) shipmentLineList
									.item(j);
							NodeList lines = releaseLine
									.getElementsByTagName("Extn");
							Element lineInfo = (Element) lines.item(0);
							String line = lineInfo
									.getAttribute("ExtnHostOrderLineReference");
							//System.out.println("Line:" + line);
							unreleaseLineList.add(line);
						}
						ordersNode.removeChild(shipmentList.item(i));
						System.out.println("Removed Unrelease Shipment");
					}
				}
			}

			for (int k = 0; k < orderList.getLength(); k++) {
				Element order = (Element) orderList.item(k);
				docType = order.getAttribute("DocumentType");
				if (docType.equalsIgnoreCase("0003")) {
					System.out.println("DocType 0003 found");
					NodeList orderLinesList = order
							.getElementsByTagName("OrderLines");
					Element orderLines = (Element) orderLinesList.item(0);
					NodeList orderLineList = orderLines
							.getElementsByTagName("OrderLine");
					for (int l = 0; l < orderLineList.getLength(); l++) {
						Element orderLine = (Element) orderLineList.item(l);
						NodeList lineExtnList = orderLine
								.getElementsByTagName("Extn");
						Element lineExtn = (Element) lineExtnList.item(0);
						String returnLineRef = lineExtn
								.getAttribute("ExtnReturnOrderLineReference");
						Double returnLineQty = Double.parseDouble(lineExtn
								.getAttribute("ExtnReturnReceivedQuantity"));
						returnLineDetails.put(returnLineRef, returnLineQty);
					}
				}
			}

			Element order = (Element) orderList.item(0);
			NodeList orderExtnList = order
					.getElementsByTagName("Extn");
			Element orderExtn = (Element) orderExtnList.item(0);
			
			NodeList willCallsList = orderExtn
					.getElementsByTagName("HDWillCallList");
			Element willCall = (Element) willCallsList.item(0);
			NodeList willCallList = null;

			if (willCall != null) {
				willCallList = willCall.getElementsByTagName("HDWillCall");
			}

			Set<String> WillCallLineNumList = new HashSet<String>();
			docType = order.getAttribute("DocumentType");
			String willCallLineNum = null;

			if (docType.equalsIgnoreCase("0001")) {
				//System.out.println("DocType 0001 found");
				NodeList orderLinesList = order
						.getElementsByTagName("OrderLines");
				Element orderLines = (Element) orderLinesList.item(0);
				NodeList orderLineList = orderLines
						.getElementsByTagName("OrderLine");
				for (int l = 0; l < orderLineList.getLength(); l++) {
					Element orderLine = (Element) orderLineList.item(l);
					NodeList lineExtnList = orderLine
							.getElementsByTagName("Extn");
					Element lineExtn = (Element) lineExtnList.item(0);
					String orderLineRef = lineExtn
							.getAttribute("ExtnHostOrderLineReference");
					System.out.println("ExtnHostOrderLineReference"
							+ orderLineRef);
					if (unreleaseLineList.contains(orderLineRef)
							&& !(returnLineDetails.containsKey(orderLineRef))) {
						/*System.out
								.println("Entering Unreleasing OrderLine part");*/
						willCallLineNum = lineExtn
								.getAttribute("ExtnWillCallLineNum");
						WillCallLineNumList.add(willCallLineNum);
						lineExtn
						.removeAttribute("ExtnWillCallLineNum");
						NodeList hdEventList = lineExtn
								.getElementsByTagName("HDEventList");
						Node hdEventNode = hdEventList.item(0);
						Element hdEvent = (Element) hdEventList.item(0);
						if (hdEvent != null) {
							System.out.println("Entering event part");
							NodeList eventList = hdEvent
									.getElementsByTagName("HDEvent");
							for (int m = 0; m < eventList.getLength(); m++) {
								Element event = (Element) eventList.item(m);
								if ((event.getAttribute("EventTrkCode"))
										.equalsIgnoreCase("R")) {
									hdEventNode.removeChild(eventList.item(m));
									System.out.println("Removed R event");

								}
							}
						}
					}

				}
			}

			if (willCallList != null) {
				for (int w = 0; w < willCallList.getLength(); w++) {
					Element willCallItem = (Element) willCallList.item(0);
					if (WillCallLineNumList.contains(willCallItem
							.getAttribute("WillCallLineNum"))) {
						willCallItem.getParentNode().removeChild(willCallItem);

						System.out.println("Removed willcall line no." + willCallItem
								.getAttribute("WillCallLineNum") );
					}

				}

				if (willCallList.getLength() == 0) {
					orderExtn.removeChild(willCall);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		String modifiedXML = getStringFromDocument(doc);
		String outputFile = "c:\\Tax\\Response\\" + orderNumber + ".xml";
		FileWriter fw = new FileWriter(outputFile, true);
		fw.write(modifiedXML);
		fw.close();
		return modifiedXML;
	}

	public static String getStringFromDocument(Document doc)
			throws TransformerException {
		DOMSource domSource = new DOMSource(doc);
		StringWriter writer = new StringWriter();
		StreamResult result = new StreamResult(writer);
		TransformerFactory tf = TransformerFactory.newInstance();
		Transformer transformer = tf.newTransformer();
		transformer.transform(domSource, result);
		return writer.toString();
	}

}
