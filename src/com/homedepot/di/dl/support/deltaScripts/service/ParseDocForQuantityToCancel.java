package com.homedepot.di.dl.support.deltaScripts.service;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;


public class ParseDocForQuantityToCancel {
	
	public static String parseDoc(String xml, String store, String orderNumber)
			throws TransformerException, ParserConfigurationException,
			SAXException, IOException {

		// System.out.println("Inside Main");

		Map<String, Double> lineStatusInfo = new HashMap<String, Double>();
		Map<String, String> statusCodeDetails = new HashMap<String, String>();
		Map<String, Double> cancelNReturnMap = new HashMap<String, Double>();
		Map<String, Double> eventActual = new HashMap<String, Double>();
		Map<String, Double> eventPresent = new HashMap<String, Double>();
		Map<String, Double> eventNeeded = new HashMap<String, Double>();
		Boolean rcEventNeeded = false;

		statusCodeDetails.put("1100", "Created");
		statusCodeDetails.put("1200", "Reserved");
		statusCodeDetails.put("1300", "Backordered");
		statusCodeDetails.put("1400", "Backordered from node");
		statusCodeDetails.put("1500", "Scheduled");
		statusCodeDetails.put("3200", "Released");
		statusCodeDetails.put("3350", "Included in Shipment");
		statusCodeDetails.put("3350.200", "Shipment Picked");
		statusCodeDetails.put("3700", "Shipped");
		statusCodeDetails.put("3700.7777", "Order Dlelivered");
		statusCodeDetails.put("3700.02", "Return Received");
		statusCodeDetails.put("9000", "Cancelled");
		String returnCreate = "";
		String returnLinecreate = "";

		String orderRef = null;
		String qtyCancel = "";
		String cancelLoc = null;
		String skuCode = null;
		String lineRef = null;
		String returnOrderno = null;
		String billToinfo = "";
		String timeStamp;

		String rsnCode = null;
		String eventTs = null;
		String auditID = null;
		String cEventQty = null;
		String rEventstore = null;
		String rEventQty = null;
		
		System.out.println(orderNumber);

		billToinfo = createPersoninfo(orderNumber, store);

		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(new InputSource(new StringReader(xml)));
		Element orders = doc.getDocumentElement();
		NodeList order = orders.getElementsByTagName("Order");

		for (int i = 0; i < order.getLength(); i++) {

			Element order1 = (Element) order.item(i);

			String docType = order1.getAttribute("DocumentType");
			String sellerCode = order1.getAttribute("SellerOrganizationCode");

			if (docType.equalsIgnoreCase("0001")) {

				NodeList orderExtn = order1.getElementsByTagName("Extn");

				Element orderExtn1 = (Element) orderExtn.item(0);

				orderRef = orderExtn1.getAttribute("ExtnHostOrderReference");
				String orderPublishTs = orderExtn1
						.getAttribute("ExtnSVSPublishTimeStamp");

				NodeList orderLines = order1.getElementsByTagName("OrderLines");
				NodeList willCallsList = orderExtn1
						.getElementsByTagName("HDWillCallList");
				Element willCall = (Element) willCallsList.item(0);
				NodeList willCallList = null;
				if (willCall != null) {
					willCallList = willCall.getElementsByTagName("HDWillCall");
				}

				Element hdEventList = null;
				Element orderLines1 = (Element) orderLines.item(0);
				NodeList orderLine = orderLines1
						.getElementsByTagName("OrderLine");

				Set<String> WillCallLineNumList = new HashSet<String>();

				for (int j = 0; j < orderLine.getLength(); j++) {
					Element orderLine1 = (Element) orderLine.item(j);
					String lineType = orderLine1.getAttribute("LineType");

					NodeList extnLine = orderLine1.getElementsByTagName("Extn");
					Element extnLine1 = (Element) extnLine.item(0);

					lineRef = extnLine1
							.getAttribute("ExtnHostOrderLineReference");

					NodeList eventList = extnLine1
							.getElementsByTagName("HDEventList");
					Element eventList1 = (Element) eventList.item(0);

					String willCallLineNum = null;
					


					System.out.println("Line-" + lineRef);
					lineStatusInfo = getStatus(orderRef, lineRef);

					try {
						
						NodeList event = null;
						if(eventList1 != null){
						event = eventList1
								.getElementsByTagName("HDEvent");
						
						for (int x = 0; x < event.getLength(); x++) {
							Element event1 = (Element) event.item(x);

							String trkCode = event1
									.getAttribute("EventTrkCode");
							if (trkCode.equalsIgnoreCase("C")) {
								rsnCode = event1
										.getAttribute("EventRsnCode");
								eventTs = event1.getAttribute("EventTs");
								auditID = event1
										.getAttribute("EventUserAuditID");
								cEventQty = event1.getAttribute("MerQty");
								rEventQty = cEventQty.replace("-", "");
								rEventstore = event1.getAttribute("Store");
							}
						}
						}
						

							if (extnLine1.hasAttribute("ExtnQuantityToCancel")) {
								qtyCancel = extnLine1
										.getAttribute("ExtnQuantityToCancel");
								cancelLoc = extnLine1
										.getAttribute("ExtnCancelLocation");
								// System.out.println("cancel location:" +
								// cancelLoc);
								if (cancelLoc.trim().equals("")) {
									cancelLoc = sellerCode;
								}
								skuCode = extnLine1.getAttribute("ExtnSKUCode");
								if (!qtyCancel.equalsIgnoreCase("0.0")) {
									cancelNReturnMap = getCancelableNReturnableQty(
											lineStatusInfo, statusCodeDetails,
											qtyCancel);
									String cancelableQty = String.format(
											"%.2f", cancelNReturnMap
													.get("CancelableQty"));
									String returnableQty = String.format(
											"%.2f", cancelNReturnMap
													.get("ReturnableQty"));
									System.out.print("CancelableQty:"
											+ cancelableQty);
									System.out.println("  ReturnableQty:"
											+ returnableQty);
									eventActual.put("cEventQty",
											cancelNReturnMap.get("cEventQty"));
									eventActual.put("rEventQty",
											cancelNReturnMap.get("rEventQty"));
									eventNeeded
											.put("cEventQty",
													Double.parseDouble(cancelableQty)
															+ Double.parseDouble(returnableQty));
									eventNeeded.put("rEventQty",
											Double.parseDouble(returnableQty));
									eventPresent = getRnCEventInfo(orderRef,
											lineRef);
									rcEventNeeded = checkRCEventNeeded(
											eventActual, eventPresent,
											eventNeeded);
									System.out.println("EventPresent"
											+ eventPresent);
									System.out.println("EventActual"
											+ eventActual);
									System.out.println("EventNeeded"
											+ eventNeeded);
									System.out.println("rcEventNeeded: "
											+ rcEventNeeded);
									String cancelQty = "-" + cancelableQty;
									if (!(cancelableQty
											.equalsIgnoreCase(qtyCancel))) {
										if (cancelableQty
												.equalsIgnoreCase("0.00")
												&& returnableQty
														.equalsIgnoreCase("0.00")) {
											extnLine1
													.removeAttribute("ExtnQuantityToCancel");
											if(eventList1 != null){
												extnLine1.removeChild(eventList1);
											}
										} else if (!(returnableQty
												.equalsIgnoreCase("0.00"))) {
											if (!(cancelableQty
													.equalsIgnoreCase("0.00"))) {
												rEventQty = returnableQty;
												extnLine1.setAttribute(
														"ExtnQuantityToCancel",
														cancelableQty);
												
												if(eventList1 != null){
												for (int x = 0; x < event
														.getLength(); x++) {
													Element event1 = (Element) event
															.item(x);
													String trkCode = event1
															.getAttribute("EventTrkCode");
													if (trkCode
															.equalsIgnoreCase("C")) {
														if (rcEventNeeded) {
															event1.setAttribute(
																	"MerQty",
																	cancelQty);
														} else {
															eventList1
																	.removeChild(event1);
														}
													}
												}
												}

											} else {
												extnLine1
														.removeAttribute("ExtnQuantityToCancel");
												if(eventList1 != null){
												for (int x = 0; x < event
														.getLength(); x++) {
													Element event1 = (Element) event
															.item(x);
													String trkCode = event1
															.getAttribute("EventTrkCode");
													if (trkCode
															.equalsIgnoreCase("C")) {
														if (!rcEventNeeded) {
															eventList1
																	.removeChild(event1);
														}
													}
												}
												}

											}
											/*
											 * System.out.print("CancelQty:" +
											 * cancelQty);
											 * System.out.println("ReturnQty:" +
											 * rEventQty);
											 */
											if (rcEventNeeded && eventList1 != null) {
												Element hdEvent = doc
														.createElement("HDEvent");
												eventList1.appendChild(hdEvent);
												hdEvent.setAttribute(
														"EventRsnCode", rsnCode);
												hdEvent.setAttribute(
														"EventTrkCode", "R");
												hdEvent.setAttribute("EventTs",
														eventTs);
												hdEvent.setAttribute(
														"EventUserAuditID",
														auditID);
												hdEvent.setAttribute("MerQty",
														rEventQty);
												hdEvent.setAttribute("Store",
														rEventstore);
											}
											Calendar cal = Calendar
													.getInstance();
											DateFormat dateFmat = new SimpleDateFormat(
													"yyyy-MM-dd");
											DateFormat dateFormat = new SimpleDateFormat(
													"HH:mm:ss");
											timeStamp = dateFmat.format(cal
													.getTime())
													+ "T"
													+ dateFormat.format(cal
															.getTime()) + "Z";
											returnLinecreate = returnLinecreate
													+ createReturnLineInfo(
															lineRef, skuCode,
															rEventQty,
															cancelLoc,
															timeStamp);

										}
									}
								
								}
							}

						if (extnLine1
								.hasAttribute("ExtnHasServiceLineReferenceChanged")
								&& checkShipped(lineStatusInfo,
										statusCodeDetails)) {
							String quantity = null;
							String releaseType = null;
							willCallLineNum = extnLine1
									.getAttribute("ExtnWillCallLineNum");
							System.out.println("WillCallLineNum"
									+ willCallLineNum);
							WillCallLineNumList.add(willCallLineNum);
							extnLine1
									.removeAttribute("ExtnHasServiceLineReferenceChanged");
							extnLine1.removeAttribute("ExtnWillCallLineNum");

							Map<String, String> ordMap = getLineInfo(orderRef,
									lineRef);
							System.out.println("ordMap" + ordMap);
							releaseType = ordMap.get("releaseType");
							quantity = ordMap.get("orderedQty");
							
							extnLine1.setAttribute("ExtnReleaseType",releaseType);

							if (releaseType.equals("S")
									&& lineType.equalsIgnoreCase("SO")) {
								if (eventList.getLength() == 0) {
									hdEventList = (Element) doc
											.createElement("HDEventList");
								} else {
									hdEventList = eventList1;
								}

								Element hdEvent = doc.createElement("HDEvent");
								hdEventList.appendChild(hdEvent);
								hdEvent.setAttribute("EventTrkCode", "MO");
								hdEvent.setAttribute("EventTs", orderPublishTs);
								hdEvent.setAttribute("EventUserAuditID",
										"SYSCOM");

								hdEvent.setAttribute("MerQty", quantity);
								hdEvent.setAttribute("Store", cancelLoc);

								extnLine1.appendChild(hdEventList);

							}
							
							NodeList hdLineTaxListList = extnLine1
									.getElementsByTagName("HDLineTaxList");
							Element hdLineTaxList1 = (Element) hdLineTaxListList.item(0);
							if (hdLineTaxList1 != null) {
								NodeList hdLineTaxList = hdLineTaxList1
										.getElementsByTagName("HDLineTax");

								for (int k = 0; k < hdLineTaxList.getLength(); k++) {
									Element hdLineTax = (Element) hdLineTaxList.item(k);
									if (hdLineTax.hasAttribute("Operation")) {
										if (hdLineTax.getAttribute("Operation")
												.equalsIgnoreCase("Delete")) {
											System.out.println("HDLineTax-Delete");
											extnLine1.removeChild(hdLineTaxList1);
										}
									}
								}
							}
						}
						
						
						if (returnLinecreate != "") {

							// System.out.println(billToinfo);
							// billToinfo = "</PersonInfoBillTo>";

							if (billToinfo != "") {

								// returnOrderno="H0122-47296";

								returnCreate = " <Order DocumentType='0003' EnterpriseCode='HDUS' SellerOrganizationCode='"
										+ cancelLoc
										+ "'>"
										+ " <Extn ExtnHostOrderReference='"
										+ orderRef
										+ "' ExtnHostOrderSystemIdentifier='80677'"
										+ " ExtnReturnOrderReference='returnOrderno' ExtnDerivedTransactionType='ORDER_CREATE'/>"
										+ " <OrderLines>"
										+ returnLinecreate
										+ " </OrderLines>"
										+ billToinfo
										+ "</Order>";
							}

						}

					} catch (Exception e) {
						e.printStackTrace();
					}
				}

				if (willCallList != null) {

					for (int w = 0; w < willCallList.getLength(); w++) {

						Element willCallItem = (Element) willCallList.item(0);

						if (WillCallLineNumList.contains(willCallItem
								.getAttribute("WillCallLineNum"))) {

							willCallItem.getParentNode().removeChild(
									willCallItem);
						}

					}

					if (willCallList.getLength() == 0) {
						orderExtn1.removeChild(willCall);
					}

				}

			}

			else if (docType.equalsIgnoreCase("0005")) {
				NodeList orderExtn = order1.getElementsByTagName("Extn");

				Element orderExtn1 = (Element) orderExtn.item(0);

				orderRef = orderExtn1.getAttribute("ExtnHostOrderReference");

				NodeList orderLines = order1.getElementsByTagName("OrderLines");

				try {
					Element orderLines1 = (Element) orderLines.item(0);

					if (orderLines1 != null) {
						NodeList orderLine = orderLines1
								.getElementsByTagName("OrderLine");

						for (int j = 0; j < orderLine.getLength(); j++) {
							Element orderLine1 = (Element) orderLine.item(j);
							String lineType = orderLine1
									.getAttribute("LineType");

							NodeList extnLine = orderLine1
									.getElementsByTagName("Extn");
							Element extnLine1 = (Element) extnLine.item(0);

							if (extnLine1.hasAttribute("ExtnQuantityToCancel")) {
								qtyCancel = extnLine1
										.getAttribute("ExtnQuantityToCancel");
								cancelLoc = extnLine1
										.getAttribute("ExtnCancelLocation");
								skuCode = extnLine1.getAttribute("ExtnSKUCode");
								if (!qtyCancel.equalsIgnoreCase("0.0")) {

									lineRef = extnLine1
											.getAttribute("ExtnHostOrderLineReference");
									lineStatusInfo = getStatus(orderRef,
											lineRef);
									cancelNReturnMap = getCancelableNReturnableQty(
											lineStatusInfo, statusCodeDetails,
											qtyCancel);
									String cancelableQty = String.format(
											"%.2f", cancelNReturnMap
													.get("CancelableQty"));

									if (cancelableQty.equalsIgnoreCase("0.00")) {
										extnLine1
												.removeAttribute("ExtnQuantityToCancel");
									} else {
										extnLine1.setAttribute(
												"ExtnQuantityToCancel",
												cancelableQty);
									}
								}
							}
						}
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

		}

		String modifiedXML = getStringFromDocument(doc);
		modifiedXML = modifiedXML.replace("</Orders>", returnCreate
				+ "</Orders>");
		if (modifiedXML.contains("returnOrderno")) {
			String returnOrder = createOrdernumber(store);
			modifiedXML = modifiedXML.replace("returnOrderno", returnOrder);
		}
		

		String outputFile = "c:\\Tax\\Response\\" + orderNumber + ".xml";
		FileWriter fw = new FileWriter(outputFile, false);
		fw.write(modifiedXML);
		fw.close();
		return modifiedXML;

	}

	public static Map<String, Double> getCancelableNReturnableQty(
			Map<String, Double> lineStatusInfo,
			Map<String, String> statusCodeDetails, String qtyCancel) {
		Double status;
		Double qty;
		String statusDesc;
		Double returnableQty = 0.00;
		Double cancelableQty = 0.00;
		Double cancelQty = Double.parseDouble(qtyCancel);
		System.out.println("cancelQty" + cancelQty);
		Double rEventQty = 0.00;
		Double cEventQty = 0.00;
		Map<String, Double> cancelNReturnMap = new HashMap<String, Double>();
		DecimalFormat decim = new DecimalFormat("0.00");
		System.out.print("	LineStatus-Qty  ");
		for (Map.Entry<String, Double> en : lineStatusInfo.entrySet()) {
			status = Double.parseDouble(en.getKey());
			statusDesc = (statusCodeDetails.get(en.getKey()) != null) ? statusCodeDetails
					.get(en.getKey()) : "Other";
			qty = en.getValue();
			System.out.print(statusDesc + "-" + qty + "  ");
			if (status < 3700) {
				if (status != 1400)
					cancelableQty = cancelableQty + qty;
			} else if (status == 3700 || status == 3700.7777) {
				returnableQty = returnableQty + qty;
			} else if (status == 9000) {
				cEventQty = cEventQty + qty;
			} else if (status == 3700.02) {
				rEventQty = rEventQty + qty;
				cEventQty = cEventQty + qty;
			}
		}
		System.out.print("\n");
		if (cancelableQty >= cancelQty) {
			cancelNReturnMap.put("CancelableQty", cancelQty);
			cancelNReturnMap.put("ReturnableQty", 0.00);
		} else {
			if (returnableQty >= Double.parseDouble(decim.format(cancelQty - cancelableQty))) {
				cancelNReturnMap.put("CancelableQty", cancelableQty);
				cancelNReturnMap.put("ReturnableQty", Double.parseDouble(decim.format(cancelQty - cancelableQty)));
			} else {
				cancelNReturnMap.put("CancelableQty", cancelableQty);
				cancelNReturnMap.put("ReturnableQty", 0.00);
			}
		}
		cancelNReturnMap.put("cEventQty", cEventQty);
		cancelNReturnMap.put("rEventQty", rEventQty);
		System.out.println("CancelReturnMap:" + cancelNReturnMap);

		return cancelNReturnMap;
	}

	public static Boolean checkRCEventNeeded(Map<String, Double> eventActual,
			Map<String, Double> eventPresent, Map<String, Double> eventNeeded) {

		Double extraRQty = 0.00;
		Double extraCQty = 0.00;

		extraCQty = eventPresent.get("cEventQty")
				- eventActual.get("cEventQty");
		extraRQty = eventPresent.get("rEventQty")
				- eventActual.get("rEventQty");

		if (extraCQty >= eventNeeded.get("cEventQty")
				&& extraRQty >= eventNeeded.get("rEventQty")) {
			return false;
		}
		return true;
	}

	public static Boolean checkShipped(Map<String, Double> lineStatusInfo,
			Map<String, String> statusCodeDetails) {

		Double status;
		String statusDesc;
		Double qty;
		Boolean isShipped = true;
		System.out.println("lineStatusInfo" + lineStatusInfo);
		

		System.out.print("	LineStatus ");
		
		for (Map.Entry<String, Double> en : lineStatusInfo.entrySet()) {
			status = Double.parseDouble(en.getKey());
			statusDesc = (statusCodeDetails.get(en.getKey()) != null) ? statusCodeDetails
					.get(en.getKey()) : "Other";
			qty = en.getValue();
			System.out.print(statusDesc + "-" + qty + "  ");
			if (status < 3700) {
				isShipped = false;
			}
		}
		System.out.print("\n");

		return isShipped;
	}

	public static String createReturnLineInfo(String lineRef, String skuCode,
			String rEventQty, String cancelLoc, String timeStamp) {
		String returnLineInfo = "<OrderLine ShipNode='"
				+ cancelLoc
				+ "'>"
				+ " <Extn ExtnTransactionType='RETURN' ExtnHostOrderLineReference='"
				+ lineRef
				+ "' ExtnSKUCode='"
				+ skuCode
				+ "' "
				+ " ExtnReturnReceivedQuantity='"
				+ rEventQty
				+ "' "
				+ " ExtnSVSRemEntriesReqd='N'>"
				+ " <HDReturnPolicyList>"
				+ " <HDReturnPolicy Store='"
				+ cancelLoc
				+ "' RtnPolicySequenceNo='1' CreateTimeStamp='"
				+ timeStamp
				+ "' "
				+ " LastUpdatedSysUserID='RXV8320' "
				+ " LastUpdatedTimeStamp='"
				+ timeStamp
				+ "' RTVQty='0' "
				+ " RTVReasonCode='0' MkdnTagStoreNo='0' ReturnLocationCode='0' VendorNegotiatedRefundAmt='0.0'/>"
				+ " </HDReturnPolicyList></Extn></OrderLine>";

		return returnLineInfo;
	}

	public static String createPersoninfo(String orderRef, String cancelLoc) {
		String billInfo = null;
		String lockReq = "N";
		String url = "http://multichannel-custom.homedepot.com/COMOrder/rs/getOrderDetail";
		String inputXML = "<Order DocumentType='0001' EnterpriseCode='HDUS'><Extn ExtnHostOrderReference='"
				+ orderRef
				+ "' ExtnPutOrderOnLock='"
				+ lockReq
				+ "' ExtnStoreRequestingLock='"
				+ cancelLoc.trim()
				+ "' ExtnUserId='COMSupport'/></Order>";

		String headerValue = "F4EJPD2Z7TbyLUP4llcrQsxPaE6yMjM0y8KkbvP8cwCN9YKBs7Qxdm3lrRppaPiGzrq7djLoSp5IfA8pJMFYtuVCHsTWEX4W6v5GpfL6E8J57j0ZfJQmhwbZJla1cbzPKvaAShYH1KIxhhmxg1RsesWqVH54fyPZRviJFURRVhfx0FuqPYxdaaGSL4GgnPym1da7lti6bOblN0EOxHK45fm5gkf5vV3eQmNaq5C7NqtSVbiaMIZUJgkGTdrdeY7HVqx7FmFNj8rphDVQuNfeUHSt2euMu6zeB4y";

		String headerName = "THDService-Auth";
		String outputXML = null;
		try {
			Client client = ClientManager.getClient();

			WebResource webResource = ((com.sun.jersey.api.client.Client) client)
					.resource(url);

			outputXML = webResource.header(headerName, headerValue)
					.type("text/xml").post(String.class, inputXML);

			if (outputXML.contains("ResponseDescription=\"SUCCESS\"")) {
				int firstIndex = outputXML.indexOf("<PersonInfoBillTo");
				int lastIndex = outputXML.indexOf("</PersonInfoBillTo>");
				lastIndex = lastIndex + 19;
				billInfo = outputXML.substring(firstIndex, lastIndex);
			}

			else {
				System.out.println("getOrderDetail failed for " + orderRef
						+ "	" + cancelLoc);

			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return billInfo;

	}

	private static String createOrdernumber(String cancelLoc) {

		String store = cancelLoc;
		String url = "http://st" + store
				+ ".homedepot.com:12060/StoreCOMOrder/rs/generateOrderID";

		String headerValue = "COMSupport";

		String headerName = "Referer";

		Client client = ClientManager.getClient();

		WebResource webResource = ((com.sun.jersey.api.client.Client) client)
				.resource(url);

		String orderNo = webResource.header(headerName, headerValue).get(
				String.class);

		//System.out.println(orderNo);

		String no = null;

		// String oNo = null;
		if (orderNo.contains("OrderId")) {
			String dupStr = orderNo;

			int firstIndex = dupStr.indexOf("<OrderId>");
			firstIndex = firstIndex + 9;
			int lastIndex = dupStr.indexOf("</OrderId>");
			no = dupStr.substring(firstIndex, lastIndex);

			no = no.trim();
			System.out.println("ReturnOrdNumber" + no);

		}
		return no;

	}

	public static Map<String, Double> getStatus(String orderRef, String lineRef) {
		// TODO Auto-generated method stub

		final String connectionURL = "jdbc:oracle:thin://@apragor11-scan.homedepot.com:1521/dpr77mm_sro01";
		final String uName = "MMUSR01";
		final String uPassword = "COMS_MMUSR01";

		String statusCheckquery = "select yol.extn_host_order_line_ref, yors.status, yors.status_quantity from yfs_order_line yol, thd01.yfs_order_release_status yors"
				+ " where yol.order_header_key in (select order_header_key from thd01.yfs_order_header where extn_host_order_ref ='"
				+ orderRef
				+ "' and document_type = '0001') and yol.extn_host_order_line_ref in('"
				+ lineRef
				+ "') and yors.status_quantity > 0 and yol.order_line_key = yors.order_line_key";

		// System.out.println(statusCheckquery);

		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;
		Map<String, Double> statusMap = new HashMap<String, Double>();
		String status = null;
		Double qty = null;
		Double statusQty = null;

		try {

			con = DriverManager.getConnection(connectionURL, uName, uPassword);
			// System.out.println("Sterling Connection Opened");

			stmt = con.createStatement();

			rs = stmt.executeQuery(statusCheckquery);

			while (rs.next()) {
				status = rs.getString(2).trim();
				qty = Double.parseDouble(rs.getString(3));
				statusQty = statusMap.get(status);
				statusMap.put(status, (statusQty == null) ? qty : statusQty
						+ qty);
			}
			
			stmt.close();
			con.close();

		}

		catch (Exception e) {
			e.printStackTrace();
		}

		return statusMap;

	}

	public static Map<String, Double> getRnCEventInfo(String orderRef,
			String lineRef) {

		final String connectionURL = "jdbc:oracle:thin://@apragor11-scan.homedepot.com:1521/dpr77mm_sro01";
		final String uName = "MMUSR01";
		final String uPassword = "COMS_MMUSR01";

		String statusCheckquery = "select yol.extn_host_order_line_ref,he.event_trk_code,he.mer_qty from THD01.hd_event he, THD01.yfs_order_line yol where "
				+ "yol.order_header_key in (select order_header_key from thd01.yfs_order_header where extn_host_order_ref ='"
				+ orderRef
				+ "' and document_type = '0001')and yol.extn_host_order_line_ref in ('"
				+ lineRef
				+ "') and yol.order_line_key = he.order_line_key and he.event_trk_code in ('R','C')";

		// System.out.println(statusCheckquery);

		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;
		Map<String, Double> rcEventMap = new HashMap<String, Double>();
		rcEventMap.put("cEventQty", 0.0);
		rcEventMap.put("rEventQty", 0.0);
		String eventTrkCode = null;
		Double qty = null;
		Double eventQty = null;

		try {

			con = DriverManager.getConnection(connectionURL, uName, uPassword);
			// System.out.println("Sterling Connection Opened");

			stmt = con.createStatement();

			rs = stmt.executeQuery(statusCheckquery);

			while (rs.next()) {
				eventTrkCode = rs.getString(2).trim();
				qty = Double.parseDouble(rs.getString(3).replace("-", ""));
				eventQty = rcEventMap
						.get(eventTrkCode.equals("R") ? "rEventQty"
								: "cEventQty");
				// System.out.println("eventTrkCode-" + eventTrkCode + "qty" +
				// qty + "eventQty" + eventQty);
				rcEventMap.put((eventTrkCode.equals("R") ? "rEventQty"
						: "cEventQty"), (eventQty == 0.0) ? qty : eventQty
						+ qty);
				// System.out.println("rcEventMap" + rcEventMap);
			}
			
			stmt.close();
			con.close();

		}

		catch (Exception e) {
			e.printStackTrace();
		}

		return rcEventMap;
	}

	public static Map<String, String> getLineInfo(String orderRef,
			String lineRef) {

		final String connectionURL = "jdbc:oracle:thin://@apragor11-scan.homedepot.com:1521/dpr77mm_sro01";
		final String uName = "MMUSR01";
		final String uPassword = "COMS_MMUSR01";

		String query = "select extn_host_order_line_ref, ordered_qty, extn_release_type from thd01.yfs_order_line where order_header_key in "
				+ "(select order_header_key from thd01.yfs_order_header where extn_host_order_ref ='"
				+ orderRef
				+ "' and document_type = '0001') and extn_host_order_line_ref in ('"
				+ lineRef + "')";

		//System.out.println(query);

		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;
		Map<String, String> lineInfoMap = new HashMap<String, String>();
		String orderedQty = null;
		String releaseType = null;

		try {

			con = DriverManager.getConnection(connectionURL, uName, uPassword);
			//System.out.println("Sterling Connection Opened");

			stmt = con.createStatement();

			rs = stmt.executeQuery(query);
			
			while (rs.next()) {
				orderedQty = rs.getString(2).trim();
				releaseType = rs.getString(3).trim();
				lineInfoMap.put("orderedQty", orderedQty);
				lineInfoMap.put("releaseType", releaseType);
				//System.out.println("lineInfoMap" + lineInfoMap);
			}
			
			stmt.close();
			con.close();

		}

		catch (Exception e) {
			e.printStackTrace();
		}

		return lineInfoMap;
	}

	public static String getStringFromDocument(Document doc)
			throws TransformerException {
		DOMSource domSource = new DOMSource(doc);
		StringWriter writer = new StringWriter();
		StreamResult result = new StreamResult(writer);
		TransformerFactory tf = TransformerFactory.newInstance();
		Transformer transformer = tf.newTransformer();
		transformer.transform(domSource, result);
		return writer.toString();
	}
}
