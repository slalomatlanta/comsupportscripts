package com.homedepot.di.dl.support.deltaScripts.service;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class ParseDocForYNEQ {
	
	/*public static void main(String[] args) throws TransformerException, ParserConfigurationException, SAXException, IOException {
		String fileLocn = "c:\\Test\\test.xml";
		String xml = "";
		String modifiedXML = "";
		
		xml = readFile(fileLocn);
		modifiedXML = parseDoc(xml,"1220","W495506086");
		
		System.out.println("----Completed-----");
				
	}
	
	private static String readFile(String file) throws IOException {
		BufferedReader reader = new BufferedReader(new FileReader(file));
		String line = null;
		StringBuilder stringBuilder = new StringBuilder();
		String ls = System.getProperty("line.separator");

		try {
			while ((line = reader.readLine()) != null) {
				stringBuilder.append(line);
				stringBuilder.append(ls);
			}

			return stringBuilder.toString();
		} finally {
			reader.close();
		}
	}*/
	
	public static String parseDoc(String xml, String store, String orderNumber)
			throws TransformerException, ParserConfigurationException,
			SAXException, IOException {

		Map<String, Double> returnLineDetails = new HashMap<String, Double>();
		Map<String, Double> lineStatusInfo = new HashMap<String, Double>();
		Map<String, String> lineStatusCodeDetails = new HashMap<String, String>();
		Set<Element> invalidReturnLines = new HashSet<Element>();
		Set<Element> invalidPaymentMethods = new HashSet<Element>();
		lineStatusCodeDetails.put("1100", "Created");
		lineStatusCodeDetails.put("1200", "Reserved");
		lineStatusCodeDetails.put("1300", "Backordered");
		lineStatusCodeDetails.put("1500", "Scheduled");
		lineStatusCodeDetails.put("3200", "Released");
		lineStatusCodeDetails.put("3350", "Included in Shipment");
		lineStatusCodeDetails.put("3350.200", "Shipment Picked");
		lineStatusCodeDetails.put("3700", "Shipped");
		lineStatusCodeDetails.put("3700.7777", "Order Dlelivered");
		lineStatusCodeDetails.put("3700.02", "Return Received");
		lineStatusCodeDetails.put("9000", "Cancelled");

		String docType = null;

		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(new InputSource(new StringReader(xml)));

		Element orders = doc.getDocumentElement();
		NodeList orderList = orders.getElementsByTagName("Order");
		
		Element order1 = (Element) orderList.item(0);

		try {
			docType = order1.getAttribute("DocumentType");
			System.out.println("DocType:" + docType);
			if (docType.equalsIgnoreCase("0003")) {
				NodeList orderLinesList = order1
						.getElementsByTagName("OrderLines");
				Element orderLines = (Element) orderLinesList.item(0);
				
				NodeList paymentMethodsList = order1
						.getElementsByTagName("PaymentMethods");
				Element paymentMethods = (Element) paymentMethodsList.item(0);
				
				NodeList orderLineList = orderLines
						.getElementsByTagName("OrderLine");
				Boolean isPaymentNeeded = false;
				for (int l = 0; l < orderLineList.getLength(); l++) {
					Element orderLine = (Element) orderLineList.item(l);
					NodeList lineExtnList = orderLine
							.getElementsByTagName("Extn");
					Element lineExtn = (Element) lineExtnList.item(0);
					String returnLineRef = lineExtn
							.getAttribute("ExtnHostOrderLineReference");
					Double returnLineQty = Double.parseDouble(lineExtn
							.getAttribute("ExtnReturnReceivedQuantity"));
					lineStatusInfo = getStatus(orderNumber, returnLineRef,
							docType);
					Double refundQty = getRefundQty(orderNumber, returnLineRef,
							docType);
					if (!checkReturnable(lineStatusInfo,
							lineStatusCodeDetails, orderNumber,
							returnLineRef, returnLineQty)) {
						invalidReturnLines.add(orderLine);
					}
					if(checkRefundNeeded(lineStatusInfo,refundQty)){
						isPaymentNeeded = true;
					}
				}
				for (Element e : invalidReturnLines) {
					e.getParentNode().removeChild(e);
				}

				if (orderLineList.getLength() == 0) {
					order1.removeChild(orderLines);
					if(isPaymentNeeded == false){
						order1.removeChild(paymentMethods);
					}
				}
			}
			

		} catch (Exception e) {
			e.printStackTrace();
		}

		String modifiedXML = getStringFromDocument(doc);
		String outputFile = "c:\\Tax\\Response\\" + orderNumber + ".xml";
		FileWriter fw = new FileWriter(outputFile, true);
		fw.write(modifiedXML);
		fw.close();
		return modifiedXML;
	}

	

	public static Map<String, Double> getStatus(String orderRef,
			String lineRef, String docType) {
		// TODO Auto-generated method stub

		final String connectionURL = "jdbc:oracle:thin:@(DESCRIPTION_LIST=(LOAD_BALANCE=off)(FAILOVER=on)(DESCRIPTION=(ADDRESS_LIST=(LOAD_BALANCE=on)(ADDRESS=(PROTOCOL=TCP)(HOST=spragor10-scan.homedepot.com)(PORT=1521)))(CONNECT_DATA=(SERVICE_NAME=dpr77mm_sro01)))(DESCRIPTION=(ADDRESS_LIST=(LOAD_BALANCE=on)(ADDRESS=(PROTOCOL=TCP)(HOST=pprmm78x.homedepot.com)(PORT=1521)))(CONNECT_DATA=(SERVICE_NAME=dpr77mm_sro01))))";
		final String uName = "MMUSR01";
		final String uPassword = "COMS_MMUSR01";
		docType = "0001";
		
		String statusCheckquery = "select yol.extn_host_order_line_ref, yors.status, yors.status_quantity from yfs_order_line yol, thd01.yfs_order_release_status yors"
				+ " where yol.order_header_key in (select order_header_key from thd01.yfs_order_header where extn_host_order_ref ='"
				+ orderRef
				+ "' and document_type = '"
				+ docType
				+ "') and yol.extn_host_order_line_ref in('"
				+ lineRef
				+ "') and yors.status_quantity > 0 and yol.order_line_key = yors.order_line_key";

		//System.out.println(statusCheckquery);

		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;
		Map<String, Double> statusMap = new HashMap<String, Double>();
		String status = null;
		Double qty = null;
		Double statusQty = null;

		try {

			con = DriverManager.getConnection(connectionURL, uName, uPassword);
			// System.out.println("Sterling Connection Opened");

			stmt = con.createStatement();

			rs = stmt.executeQuery(statusCheckquery);

			while (rs.next()) {
				status = rs.getString(2).trim();
				qty = Double.parseDouble(rs.getString(3));
				statusQty = statusMap.get(status);
				statusMap.put(status, (statusQty == null) ? qty : statusQty
						+ qty);
				//System.out.println(status + qty);
			}
			
			stmt.close();
			con.close();

		}

		catch (Exception e) {
			e.printStackTrace();
		}

		return statusMap;

	}
	
	public static Double getRefundQty(String orderRef,
			String lineRef, String docType) {
		// TODO Auto-generated method stub

		final String connectionURL = "jdbc:oracle:thin:@(DESCRIPTION_LIST=(LOAD_BALANCE=off)(FAILOVER=on)(DESCRIPTION=(ADDRESS_LIST=(LOAD_BALANCE=on)(ADDRESS=(PROTOCOL=TCP)(HOST=spragor10-scan.homedepot.com)(PORT=1521)))(CONNECT_DATA=(SERVICE_NAME=dpr77mm_sro01)))(DESCRIPTION=(ADDRESS_LIST=(LOAD_BALANCE=on)(ADDRESS=(PROTOCOL=TCP)(HOST=pprmm78x.homedepot.com)(PORT=1521)))(CONNECT_DATA=(SERVICE_NAME=dpr77mm_sro01))))";
		final String uName = "MMUSR01";
		final String uPassword = "COMS_MMUSR01";
		docType = "0001";
		
		String statusCheckquery = "select extn_refund_quantity from yfs_order_line"
				+ " where order_header_key in (select order_header_key from thd01.yfs_order_header where extn_host_order_ref ='"
				+ orderRef
				+ "' and document_type = '"
				+ docType
				+ "') and extn_host_order_line_ref in('"
				+ lineRef
				+ "')";

		//System.out.println(statusCheckquery);

		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;
		Double refundQty = null;

		try {

			con = DriverManager.getConnection(connectionURL, uName, uPassword);
			// System.out.println("Sterling Connection Opened");

			stmt = con.createStatement();

			rs = stmt.executeQuery(statusCheckquery);

			while (rs.next()) {
				if(rs.getString(1) != null){
				refundQty = Double.parseDouble(rs.getString(1));
				} else {
					refundQty= 0.0;
				}
			}
			
			stmt.close();
			con.close();

		}

		catch (Exception e) {
			e.printStackTrace();
		}

		return refundQty;

	}

	
	public static boolean checkReturnable(Map<String, Double> lineStatusInfo,
			Map<String, String> lineStatusCodeDetails, String orderNumber,
			String lineRef, Double returnQty) {

		ProcessReleaseRequest releaseObj = new ProcessReleaseRequest();
		Double returnableQty = 0.0;
		Double releasableQty = 0.0;
		Double otherQty = 0.0;
		Double qty = 0.0;
		Double status;
		String statusDesc;
		System.out.println("ReturnQty:" + returnQty);
		System.out.print("	LineStatus - ");
		for (Map.Entry<String, Double> en : lineStatusInfo.entrySet()) {
			status = Double.parseDouble(en.getKey());
			qty = en.getValue();
			statusDesc = (lineStatusCodeDetails.get(en.getKey()) != null) ? lineStatusCodeDetails
					.get(en.getKey()) : "Other";
			System.out.print(statusDesc + " " + qty);
			if (status >= 3700) {
				if (status == 3700.7777 || status == 3700){
					returnableQty = returnableQty + qty;
				} 
			} else if (status < 3700 && status != 1400){ 
				 if (status == 3350
							|| status == 3350.200) {
						releasableQty = releasableQty + qty;
				} else {
					otherQty = otherQty + qty;
				}
			}
		}

		System.out.println(" ReturnableQty:" + returnableQty + " ReleasableQty:"
				+ releasableQty);

		if (returnableQty < returnQty) {
			if (releasableQty < (returnQty - returnableQty)) {
				//System.out.println("Insufficient ReleasableQty");
			} else {
				if (releaseObj.processReleaseShipment(orderNumber, lineRef).equalsIgnoreCase("SUCCESS")) {
					returnableQty = returnableQty + releasableQty;
				}
			}
		}
		returnableQty = returnableQty + otherQty;
		if (returnableQty == 0.0) {
			return false;
		} else {
			return true;
		}
	}
	
	private static boolean checkRefundNeeded(Map<String, Double> lineStatusInfo,
			Double refundQty) {
		
		Double status;
		Double qty;
		Double refundQtyActual = 0.0;
		Double refundQtyPresent = refundQty;
		
		for (Map.Entry<String, Double> en : lineStatusInfo.entrySet()) {
			status = Double.parseDouble(en.getKey());
			qty = en.getValue();
			if(status == 3700.02 || status == 9000){
					refundQtyActual = refundQtyActual + qty;
			}
		}
		System.out.println("RefundQtyActual:" + refundQtyActual + "  " + "RefundQtyPresent:" + refundQtyPresent);
		if(refundQtyPresent >= refundQtyActual){
			return false;
		} else{
			return true;	
		}
	}

	public static String getStringFromDocument(Document doc)
			throws TransformerException {
		DOMSource domSource = new DOMSource(doc);
		StringWriter writer = new StringWriter();
		StreamResult result = new StreamResult(writer);
		TransformerFactory tf = TransformerFactory.newInstance();
		Transformer transformer = tf.newTransformer();
		transformer.transform(domSource, result);
		return writer.toString();
	}



}
