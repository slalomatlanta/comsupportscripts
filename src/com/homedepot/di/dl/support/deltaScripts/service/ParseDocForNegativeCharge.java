package com.homedepot.di.dl.support.deltaScripts.service;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class ParseDocForNegativeCharge {

	/**
	 * @param args
	 * @throws TransformerException 
	 * @throws ParserConfigurationException 
	 * @throws SAXException 
	 * @throws IOException 
	 */
	public static String parseDoc(String xml) throws TransformerException, ParserConfigurationException, SAXException, IOException {
		
	//System.out.println("Inside Main");
	
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			//Document doc = dBuilder.parse(new InputSource(xml));
			Document doc = dBuilder.parse(new InputSource(new StringReader(xml)));
			//System.out.println("File read" + doc);
			Element orders = doc.getDocumentElement();
			NodeList order = orders.getElementsByTagName("Order");
						
			for (int i = 0; i < order.getLength(); i++) {
				Element order1 = (Element) order.item(i);
				
                NodeList orderLines = order1.getElementsByTagName("OrderLines");
                
                
                	Element orderLines1 = (Element) orderLines.item(0);
	                NodeList orderLine = orderLines1.getElementsByTagName("OrderLine");
	                
	                for (int j = 0; j < orderLine.getLength(); j++)
	                {
	                	Element orderLine1 = (Element) orderLine.item(j);
	                	String lineType = orderLine1.getAttribute("LineType");
	                	
	                	NodeList extnLine = orderLine1.getElementsByTagName("Extn");
            			Element extnLine1 = (Element) extnLine.item(0);
            			
	                	if("IN".equalsIgnoreCase(lineType))
	                	{
	                		NodeList lineCharges = orderLine1.getElementsByTagName("LineCharges");
	                		Element lineCharges1 = (Element) lineCharges.item(0);
	    	                NodeList lineCharge = lineCharges1.getElementsByTagName("LineCharge");
	    	                
	                		
	                		for (int k = 0; k < lineCharge.getLength(); k++)
	    	                {
	                			Element lineCharge1 = (Element) lineCharge.item(k);
	                			NodeList extn = lineCharge1.getElementsByTagName("Extn");
	                			Element extn1 = (Element) extn.item(0);
	                			
	                			NodeList instOptlist = extn1.getElementsByTagName("HDInstallOptionList");
	                			Element instOptlist1 = (Element) instOptlist.item(0);
		    	                NodeList instOpt = instOptlist1.getElementsByTagName("HDInstallOption");
		    	                
		    	                for (int l = 0; l < instOpt.getLength(); l++)
		    	                {
		    	                	Element instOpt1 = (Element) instOpt.item(l);
		    	                	String insQty = instOpt1.getAttribute("Quantity");
		    	                	{
		    	                		if (insQty.contains("-"))
		    	                		{
		    	                			instOpt1.setAttribute("Quantity", "0");
		    	                			lineCharge1.setAttribute("ChargeAmount", "0.0");
		    	                			extn1.setAttribute("ExtnComputedChargePerUnit", "0.0");
		    	                			Element orderAddnlist = doc.createElement("HDOrderlineAddnlList");
		    	                			extnLine1.appendChild(orderAddnlist);
		    	                			Element orderAddn = doc.createElement("HDOrderlineAddnl");
		    	                			orderAddnlist.appendChild(orderAddn);
		    	                			orderAddn.setAttribute("ReviewCompleted", "Y");
		    	                		}
		    	                	}
		    	                }
		    	                	
		    	                	
	                		}
	                	}
	                	
	                		                	
                }
                
				
				
			}
			
			String modifiedXML = getStringFromDocument(doc);
			return modifiedXML;
			
	}
	
	public static String getStringFromDocument(Document doc) throws TransformerException {
	    DOMSource domSource = new DOMSource(doc);
	    StringWriter writer = new StringWriter();
	    StreamResult result = new StreamResult(writer);
	    TransformerFactory tf = TransformerFactory.newInstance();
	    Transformer transformer = tf.newTransformer();
	    transformer.transform(domSource, result);
	    return writer.toString();
	}
	

}


