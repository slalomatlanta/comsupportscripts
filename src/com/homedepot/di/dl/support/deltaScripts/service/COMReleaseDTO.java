package com.homedepot.di.dl.support.deltaScripts.service;

public class COMReleaseDTO {

	String extnHostOrderRef;
	String shipnodeKey;
	String sellingStore;
	String extnHostOrderLineRef;
	String documentType;
	String enterpriseKey;
	String shipmentLineNo;
	String quantity;
	String extnSkuCode;
	String productClass;
	String extnUcmId;
	String extnUcmType;
	String orderDate;
	String deliveryMethod;
	String lineType;
	String extnReleaseType;
	String extnWillCallLineNum;
	String extnCreateSrcProcess;
	String shipmentNo;
	
	String inputXML;
	String outputXML;
	String status;
	String statusOfRelease;
	
	
	
	public String getStatusOfRelease() {
		return statusOfRelease;
	}
	public void setStatusOfRelease(String statusOfRelease) {
		this.statusOfRelease = statusOfRelease;
	}
	public String getInputXML() {
		return inputXML;
	}
	public void setInputXML(String inputXML) {
		this.inputXML = inputXML;
	}
	public String getOutputXML() {
		return outputXML;
	}
	public void setOutputXML(String outputXML) {
		this.outputXML = outputXML;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(String orderDate) {
		this.orderDate = orderDate;
	}
	public String getDeliveryMethod() {
		return deliveryMethod;
	}
	public void setDeliveryMethod(String deliveryMethod) {
		this.deliveryMethod = deliveryMethod;
	}
	public String getLineType() {
		return lineType;
	}
	public void setLineType(String lineType) {
		this.lineType = lineType;
	}
	public String getExtnReleaseType() {
		return extnReleaseType;
	}
	public void setExtnReleaseType(String extnReleaseType) {
		this.extnReleaseType = extnReleaseType;
	}
	public String getExtnWillCallLineNum() {
		return extnWillCallLineNum;
	}
	public void setExtnWillCallLineNum(String extnWillCallLineNum) {
		this.extnWillCallLineNum = extnWillCallLineNum;
	}
	public String getExtnCreateSrcProcess() {
		return extnCreateSrcProcess;
	}
	public void setExtnCreateSrcProcess(String extnCreateSrcProcess) {
		this.extnCreateSrcProcess = extnCreateSrcProcess;
	}
	public String getShipmentNo() {
		return shipmentNo;
	}
	public void setShipmentNo(String shipmentNo) {
		this.shipmentNo = shipmentNo;
	}
	
	public String getExtnHostOrderRef() {
		return extnHostOrderRef;
	}
	public void setExtnHostOrderRef(String extnHostOrderRef) {
		this.extnHostOrderRef = extnHostOrderRef;
	}
	public String getShipnodeKey() {
		return shipnodeKey;
	}
	public void setShipnodeKey(String shipnodeKey) {
		this.shipnodeKey = shipnodeKey;
	}
	public String getSellingStore() {
		return sellingStore;
	}
	public void setSellingStore(String sellingStore) {
		this.sellingStore = sellingStore;
	}
	public String getExtnHostOrderLineRef() {
		return extnHostOrderLineRef;
	}
	public void setExtnHostOrderLineRef(String extnHostOrderLineRef) {
		this.extnHostOrderLineRef = extnHostOrderLineRef;
	}
	public String getDocumentType() {
		return documentType;
	}
	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}
	public String getEnterpriseKey() {
		return enterpriseKey;
	}
	public void setEnterpriseKey(String enterpriseKey) {
		this.enterpriseKey = enterpriseKey;
	}
	public String getShipmentLineNo() {
		return shipmentLineNo;
	}
	public void setShipmentLineNo(String shipmentLineNo) {
		this.shipmentLineNo = shipmentLineNo;
	}
	public String getQuantity() {
		return quantity;
	}
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
	public String getExtnSkuCode() {
		return extnSkuCode;
	}
	public void setExtnSkuCode(String extnSkuCode) {
		this.extnSkuCode = extnSkuCode;
	}
	public String getProductClass() {
		return productClass;
	}
	public void setProductClass(String productClass) {
		this.productClass = productClass;
	}
	public String getExtnUcmId() {
		return extnUcmId;
	}
	public void setExtnUcmId(String extnUcmId) {
		this.extnUcmId = extnUcmId;
	}
	public String getExtnUcmType() {
		return extnUcmType;
	}
	public void setExtnUcmType(String extnUcmType) {
		this.extnUcmType = extnUcmType;
	}
	
}

