package com.homedepot.di.dl.support.deltaScripts.service;

import java.io.FileReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.homedepot.di.dl.support.util.Constants;
import com.homedepot.di.dl.support.util.DBConnectionUtil;

import au.com.bytecode.opencsv.CSVReader;

public class UpdateDeltasToN {
	private static final Logger logger = Logger
			.getLogger(UpdateDeltasToN.class);
	private static final Logger oLogger = Logger.getLogger("splunkLog");

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		logger.debug("UpdateDeltasToN Program Started at "
				+ java.util.GregorianCalendar.getInstance().getTime());
		updateDeltas();
		logger.debug("UpdateDeltasToN Program Completed at "
				+ java.util.GregorianCalendar.getInstance().getTime());
	}

	public static void updateDeltas() {
		ArrayList<String> storeList = new ArrayList<String>();

		// storeList.add("0097");
		// storeList.add("0014");
		// storeList.add("0017");
		// storeList.add("0035");
		// storeList.add("0097");

		// String inputfilelocation = "C:\\test\\storeList.csv";
		// String inputfilelocation = "/opt/isv/tomcat/temp/storeList.csv";

		storeList = getStoreList();

		StoreThreads.threadList.clear();
		StoreThreads.nameList.clear();
		StoreThreads.totalNoOfRowsUpdated = 0;

		int count = 1;
		for (String store : storeList) {
			String name = "" + count;
			StoreThreads st = new StoreThreads(store, name);
			st.startThread();
			count++;
		}

		try {

			for (Thread thread : StoreThreads.threadList) {
				thread.join();
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		logger.info("Total number of rows updated: "
				+ StoreThreads.totalNoOfRowsUpdated);
		
		oLogger.info("Program=UpdateDeltasToN status=Success Number_of_stores="+storeList.size()+" number_of_rows_updated="+StoreThreads.totalNoOfRowsUpdated);
	}

	public static ArrayList<String> getStoreList() {
		ArrayList<String> storeList = new ArrayList<String>();
		// String[] row = null;
		// CSVReader csvReader = null;
		// boolean headerRow = true;
		try {
			// csvReader = new CSVReader(new FileReader(filelocation));
			// while ((row = csvReader.readNext()) != null) {
			// if (headerRow) {
			// headerRow = false;
			// } else {
			// if ((row[0].trim()).length() == 4) {
			// storeList.add(row[0]);
			// } else {
			// storeList.add("0" + row[0]);
			// }
			// }
			// }
			// csvReader.close();
			
			/*DBConnectionUtil dbConn = new DBConnectionUtil();
			Connection con = dbConn
					.getJNDIConnection(Constants.COMT_SSC_RO_JNDI);*/
			
			final String driverClass = "oracle.jdbc.driver.OracleDriver";
			final String connectionURLThin = "jdbc:oracle:thin://@spragor10-scan.homedepot.com:1521/dpr78mm_sro01";
			//jdbc:oracle:thin:@pprmm77x.homedepot.com:1521/dpr78mm_srw02- ATC
			final String userID = "MMUSR01";
			final String userPassword = "COMS_MMUSR01";
			Connection con = null;
			con = DriverManager.getConnection(connectionURLThin, userID,
					userPassword);
			Class.forName(driverClass);
			
			Statement stmt = null;
			stmt = con.createStatement();
			ResultSet result = null;
			String query = "select distinct loc_nbr from thd01.comt_loc_capbl where COMT_CAPBL_ID = 7 order by loc_nbr";
			result = stmt.executeQuery(query);
			if (result != null) {
				while (result.next()) {
					storeList.add(result.getString(1));
				}
			}

		} catch (Exception e) {
			logger.error("Exception Occured " + e.getMessage());
			e.printStackTrace();
		}
		return storeList;
	}

}

class StoreThreads implements Runnable {
	private static final Logger logger = Logger.getLogger(StoreThreads.class);
	private Thread t;
	private String storeNumber;
	private String name;
	static int totalNoOfRowsUpdated;
	static ArrayList<Thread> threadList = new ArrayList<Thread>();
	static ArrayList<String> nameList = new ArrayList<String>();

	StoreThreads(String store, String no) {
		this.storeNumber = store;
		this.name = no;
	}

	public void run() {
		

		Connection con = null;
		int rowsUpdated = 0;

		try {

			DBConnectionUtil dbConn = new DBConnectionUtil();
			con = dbConn.getStoreconnection(storeNumber);

			String query = "update comt_ord_chg_msg set trnsm_stat_ind = 'N', last_upd_ts = sysdate, last_upd_sysusr_id = 'COMSupport' "
					+ "where trnsm_stat_ind in ('M','A')";

			Statement stmt = null;
			stmt = con.createStatement();
			rowsUpdated = stmt.executeUpdate(query);
			totalNoOfRowsUpdated = totalNoOfRowsUpdated + rowsUpdated;

			stmt.close();
			if (!con.getAutoCommit()) {
				con.commit();
			}
			con.close();

		} catch (Exception e) {
			logger.error("unable to connect to store : " + storeNumber);
			logger.error(e.getMessage());
			e.printStackTrace();
		}

		logger.debug("Thread " + name + ") Completed for " + storeNumber
				+ " - " + rowsUpdated + " rows updated");
	}

	public void startThread() {
		// System.out.println("Starting " + storeNumber);
		if (t == null) {
			t = new Thread(this, storeNumber);
			threadList.add(t);
			t.setName(name);
			nameList.add(name);
			t.start();
			logger.debug("Thread " + name + ") Submitted for " + storeNumber);
		}
	}
}
