package com.homedepot.di.dl.support.deltaScripts.service;

import java.io.FileWriter;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class ParseDocForRecordDoesNotExist {
	public static String parseDoc(String xml, String store, String orderNumber)
			throws TransformerException, ParserConfigurationException,
			SAXException, IOException {

		String hdPaymentStageInfoKey = null;
		String hdLineTaxKey = null;
		String lineRef = "";
		String isRecordPresent = null;

		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(new InputSource(new StringReader(xml)));

		Element orders = doc.getDocumentElement();

		NodeList orderList = orders.getElementsByTagName("Order");
		Element order = (Element) orderList.item(0);

		NodeList orderExtnList = order.getElementsByTagName("Extn");
		Element orderExtn = (Element) orderExtnList.item(0);

		try {
			// Record Already Exists - HDPaymentStageInfo
			NodeList paymentStgInfoListList = orderExtn
					.getElementsByTagName("HDPaymentStageInfoList");
			Element paymentStgInfoList1 = (Element) paymentStgInfoListList
					.item(0);

			if (paymentStgInfoList1 != null) {
				NodeList paymentStgInfoList = paymentStgInfoList1
						.getElementsByTagName("HDPaymentStageInfo");
				Set<Element> invalidPaymentStgInfo = new HashSet<Element>();

				for (int i = 0; i < paymentStgInfoList.getLength(); i++) {
					Element paymentStgInfo = (Element) paymentStgInfoList
							.item(i);
					if (paymentStgInfo.hasAttribute("Operation")) {
						if (paymentStgInfo.getAttribute("Operation")
								.equalsIgnoreCase("Delete")) {
							System.out.println("HDPaymentStageInfo-Delete");
							hdPaymentStageInfoKey = paymentStgInfo
									.getAttribute("HDPaymentStageInfoKey");

							isRecordPresent = checkRecordPresent(
									hdPaymentStageInfoKey, null);
							System.out.println("IsRecordPresent:"
									+ isRecordPresent);

							if (isRecordPresent != null
									&& isRecordPresent
											.equalsIgnoreCase("false")) {
								invalidPaymentStgInfo.add(paymentStgInfo);
							}
						}
					}
				}
				for (Element e : invalidPaymentStgInfo) {
					e.getParentNode().removeChild(e);
				}
			}

			// Record Already Exists - HDLineTax
			NodeList orderLinesList = order.getElementsByTagName("OrderLines");
			Element orderLines1 = (Element) orderLinesList.item(0);

			NodeList orderLineList = orderLines1
					.getElementsByTagName("OrderLine");

			for (int j = 0; j < orderLineList.getLength(); j++) {
				Element orderLine = (Element) orderLineList.item(j);

				NodeList orderLineExtnList = orderLine
						.getElementsByTagName("Extn");
				Element orderLineExtn = (Element) orderLineExtnList.item(0);

				NodeList lineTaxesList = orderLineExtn
						.getElementsByTagName("LineTaxes");
				Element lineTaxes1 = (Element) lineTaxesList.item(0);
				NodeList lineTaxList = lineTaxes1
						.getElementsByTagName("LineTax");
				Element lineTax1 = (Element) lineTaxList.item(0);
				String lineTaxValue = lineTax1.getAttribute("Tax");

				NodeList hdLineTaxListList = orderLineExtn
						.getElementsByTagName("HDLineTaxList");
				Element hdLineTaxList1 = (Element) hdLineTaxListList.item(0);
				if (hdLineTaxList1 != null) {
					NodeList hdLineTaxList = hdLineTaxList1
							.getElementsByTagName("HDLineTax");
					Set<Element> invalidHdLineTax = new HashSet<Element>();

					for (int k = 0; k < hdLineTaxList.getLength(); k++) {
						Element hdLineTax = (Element) hdLineTaxList.item(k);
						if (hdLineTax.hasAttribute("Operation")) {
							if (hdLineTax.getAttribute("Operation")
									.equalsIgnoreCase("Delete")) {
								lineRef = orderLineExtn
										.getAttribute("ExtnHostOrderLineReference");
								System.out.println("Line-" + lineRef);
								System.out.println("HDLineTax-Delete");
								hdLineTaxKey = hdLineTax
										.getAttribute("HDLineTaxKey");

								isRecordPresent = checkRecordPresent(null,
										hdLineTaxKey);
								System.out.println("IsRecordPresent:"
										+ isRecordPresent);

								if (isRecordPresent != null
										&& isRecordPresent
												.equalsIgnoreCase("false")) {
									invalidHdLineTax.add(hdLineTax);
								} else if (isRecordPresent != null
										&& isRecordPresent
												.equalsIgnoreCase("true")) {
									for (int l = 0; l < hdLineTaxList
											.getLength(); l++) {
										Element hdLineTax2 = (Element) hdLineTaxList
												.item(l);
										if (hdLineTax2
												.hasAttribute("Operation")) {
											if (hdLineTax2.getAttribute(
													"Operation")
													.equalsIgnoreCase("Delete")) {
												hdLineTax2
														.removeAttribute("Operation");
												if (lineTaxValue.equals("0")) {
													hdLineTax2.setAttribute(
															"Tax", "0.0");
												} else {
													hdLineTax2
															.setAttribute(
																	"Tax",
																	lineTaxValue);
												}
											}
										} else {
											invalidHdLineTax.add(hdLineTax2);
										}
									}
								}
							}
						}
					}
					for (Element e : invalidHdLineTax) {
						e.getParentNode().removeChild(e);
					}
				}
			}

			// Record Already Exists - OrderLineRelationShip
			NodeList orderLineRelationshipsList = order
					.getElementsByTagName("OrderLineRelationships");
			Element orderLineRelationships1 = (Element) orderLineRelationshipsList
					.item(0);

			if (paymentStgInfoList1 != null) {

				NodeList orderLineRelationshipList = orderLineRelationships1
						.getElementsByTagName("OrderLineRelationship");
				Set<Element> invalidLineRelationship = new HashSet<Element>();

				for (int m = 0; m < orderLineRelationshipList.getLength(); m++) {
					Element orderLineRelationship = (Element) orderLineRelationshipList
							.item(m);
					if (orderLineRelationship.hasAttribute("Operation")) {
						if (orderLineRelationship.getAttribute("Operation")
								.equalsIgnoreCase("Delete")) {
							System.out.println("OrderLineRelationship-Delete");

							NodeList parentLineList = orderLineRelationship
									.getElementsByTagName("ParentLine");
							Element parentLine1 = (Element) parentLineList
									.item(0);
							NodeList parentLineExtnList = parentLine1
									.getElementsByTagName("Extn");
							Element parentLineExtn1 = (Element) parentLineExtnList
									.item(0);
							String parentLineRef = parentLineExtn1
									.getAttribute("ExtnHostOrderLineReference");

							NodeList childtLineList = orderLineRelationship
									.getElementsByTagName("ChildLine");
							Element childtLine1 = (Element) childtLineList
									.item(0);
							NodeList childLineExtnList = childtLine1
									.getElementsByTagName("Extn");
							Element childLineExtn1 = (Element) childLineExtnList
									.item(0);
							String childLineRef = childLineExtn1
									.getAttribute("ExtnHostOrderLineReference");

							isRecordPresent = checkRelationshipPresent(
									orderNumber, parentLineRef, childLineRef);
							System.out.println("IsRecordPresent:"
									+ isRecordPresent);

							if (isRecordPresent != null
									&& isRecordPresent
											.equalsIgnoreCase("false")) {
								invalidLineRelationship
										.add(orderLineRelationship);
							}
						}
					}
				}
				for (Element e : invalidLineRelationship) {
					e.getParentNode().removeChild(e);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		String modifiedXML = getStringFromDocument(doc);
		String outputFile = "c:\\Tax\\Response\\" + orderNumber + ".xml";
		FileWriter fw = new FileWriter(outputFile, true);
		fw.write(modifiedXML);
		fw.close();
		return modifiedXML;

	}

	public static String checkRecordPresent(String paymentStginfoKey,
			String hdLineTaxKey) {
		// TODO Auto-generated method stub

		final String connectionURL = "jdbc:oracle:thin://@apragor11-scan.homedepot.com:1521/dpr77mm_sro01";
		final String uName = "MMUSR01";
		final String uPassword = "COMS_MMUSR01";

		String query = "";

		if (hdLineTaxKey == null) {
			query = "select hd_payment_stage_info_key from thd01.HD_PAYMENT_STAGE_INFO where hd_payment_stage_info_key in ('"
					+ paymentStginfoKey + "')";
		} else {
			query = "select hd_line_tax_key from THD01.hd_line_tax where hd_line_tax_key in ('"
					+ hdLineTaxKey + "')";
		}

		// System.out.println(query);

		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;
		String isRecordPresent = "false";

		try {

			con = DriverManager.getConnection(connectionURL, uName, uPassword);
			// System.out.println("Sterling Connection Opened");

			stmt = con.createStatement();

			rs = stmt.executeQuery(query);

			while (rs.next()) {
				isRecordPresent = "true";
			}

			stmt.close();
			con.close();
		}

		catch (Exception e) {
			e.printStackTrace();
			isRecordPresent = null;
		}

		return isRecordPresent;

	}

	public static String checkRelationshipPresent(String orderNumber,
			String parentLineRef, String childLineRef) {
		// TODO Auto-generated method stub

		final String connectionURL = "jdbc:oracle:thin://@apragor11-scan.homedepot.com:1521/dpr77mm_sro01";
		final String uName = "MMUSR01";
		final String uPassword = "COMS_MMUSR01";

		String query = "select * from thd01.yfs_order_line_relationship where "
				+ "parent_order_line_key in (select order_line_key from THD01.yfs_order_line where "
				+ "order_header_key in (select order_header_key from thd01.yfs_order_header where "
				+ "extn_host_order_ref ='"
				+ orderNumber
				+ "' and document_type = '0001') and extn_host_order_line_ref in ('"
				+ parentLineRef
				+ "')) and child_order_line_key in(select order_line_key from THD01.yfs_order_line where "
				+ "order_header_key in (select order_header_key from thd01.yfs_order_header where "
				+ "extn_host_order_ref ='"
				+ orderNumber
				+ "' and document_type = '0001') and extn_host_order_line_ref in ('"
				+ childLineRef + "1'))";

		// System.out.println(query);

		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;
		String isRecordPresent = "false";

		try {

			con = DriverManager.getConnection(connectionURL, uName, uPassword);
			// System.out.println("Sterling Connection Opened");

			stmt = con.createStatement();

			rs = stmt.executeQuery(query);

			while (rs.next()) {
				isRecordPresent = "true";
			}

			stmt.close();
			con.close();
		}

		catch (Exception e) {
			e.printStackTrace();
			isRecordPresent = null;
		}

		return isRecordPresent;
	}

	public static String getStringFromDocument(Document doc)
			throws TransformerException {
		DOMSource domSource = new DOMSource(doc);
		StringWriter writer = new StringWriter();
		StreamResult result = new StreamResult(writer);
		TransformerFactory tf = TransformerFactory.newInstance();
		Transformer transformer = tf.newTransformer();
		transformer.transform(domSource, result);
		return writer.toString();
	}
}
