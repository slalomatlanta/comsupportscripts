package com.homedepot.di.dl.support.deltaScripts.service;

import java.io.FileWriter;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;

public class ParseDocForNotEnoughQuantity {

	/**
	 * @param args
	 * @throws TransformerException
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws IOException
	 */
	public static String parseDoc(String xml, String store, String orderNumber)
			throws TransformerException, ParserConfigurationException,
			SAXException, IOException {

		// System.out.println("Inside Main");

		Map<String, Double> releaseLineDetails = new HashMap<String, Double>();
		Map<String, Double> lineStatusInfo = new HashMap<String, Double>();
		Map<String, String> statusCodeDetails = new HashMap<String, String>();
		statusCodeDetails.put("1100", "Created");
		statusCodeDetails.put("1200", "Reserved");
		statusCodeDetails.put("1300", "Backordered");
		statusCodeDetails.put("1400", "Backordered from node");
		statusCodeDetails.put("1500", "Scheduled");
		statusCodeDetails.put("3200", "Released");
		statusCodeDetails.put("3350", "Included in Shipment");
		statusCodeDetails.put("3350.200", "Shipment Picked");
		statusCodeDetails.put("3700", "Shipped");
		statusCodeDetails.put("3700.7777", "Order Dlelivered");
		statusCodeDetails.put("3700.02", "Return Received");
		statusCodeDetails.put("9000", "Cancelled");
		Double releasableQty = null;
		String eventMerQty = null;

		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		// Document doc = dBuilder.parse(new InputSource(xml));
		Document doc = dBuilder.parse(new InputSource(new StringReader(xml)));
		// System.out.println("File read" + doc);

		Element orders = doc.getDocumentElement();
		Node ordersNode = (Node) orders;
		NodeList orderList = orders.getElementsByTagName("Order");
		NodeList shipmentList = orders.getElementsByTagName("Shipment");

		Element orderLines = (Element) orderList.item(0);
		NodeList orderLinesList = orderLines.getElementsByTagName("OrderLines");

		Element orderLine = (Element) orderLinesList.item(0);
		NodeList orderLineList = orderLine.getElementsByTagName("OrderLine");
		try {

			for (int h = 0; h < shipmentList.getLength(); h++) {
				Element shipment = (Element) shipmentList.item(h);
				NodeList shipmentLinesList = shipment
						.getElementsByTagName("ShipmentLines");
				Element shipmentLine = (Element) shipmentLinesList.item(0);
				NodeList shipmentLineList = shipmentLine
						.getElementsByTagName("ShipmentLine");
				for (int i = 0; i < shipmentLineList.getLength(); i++) {
					Element releaseLine = (Element) shipmentLineList.item(i);
					Double qty = Double.parseDouble(releaseLine
							.getAttribute("Quantity"));
					NodeList lines = releaseLine.getElementsByTagName("Extn");
					Element lineInfo = (Element) lines.item(0);
					String line = lineInfo
							.getAttribute("ExtnHostOrderLineReference");
					// System.out.println("Line:" + line);
					releaseLineDetails.put(line, qty);
				}
			}

			for (Map.Entry<String, Double> en : releaseLineDetails.entrySet()) {
				String releaseLineNo = en.getKey();
				Double releaseQty = en.getValue();
				System.out.print("ReleaseLine: " + releaseLineNo);
				System.out.print("	ReleaseQty: " + releaseQty);
				lineStatusInfo = getStatus(orderNumber, releaseLineNo);
				releasableQty = getReleasableQty(releaseQty, lineStatusInfo,
						statusCodeDetails);
				System.out.println("ReleasableQty:" + releasableQty.toString());
				String newMerQty = "-" + releasableQty + "0";
				// System.out.println("newMerQty:" + newMerQty);
				boolean hasMultipleCPEvent = false;
				boolean isFirstEvent = true;

				for (int j = 0; j < orderLineList.getLength(); j++) {
					Element hostOrderLine = (Element) orderLineList.item(j);
					NodeList lineExtnList = hostOrderLine
							.getElementsByTagName("Extn");
					Element lineExtn = (Element) lineExtnList.item(0);
					String lineRef = lineExtn
							.getAttribute("ExtnHostOrderLineReference");
					if (lineRef.equalsIgnoreCase(releaseLineNo)) {
						NodeList hdEventList = lineExtn
								.getElementsByTagName("HDEventList");
						Element hdEvent = (Element) hdEventList.item(0);
						if (hdEvent != null) {
							NodeList eventList = hdEvent
									.getElementsByTagName("HDEvent");
							Set<Element> invalidEvents = new HashSet<Element>();
							for (int k = 0; k < eventList.getLength(); k++) {
								Element event = (Element) eventList.item(k);
								if ((event.getAttribute("EventTrkCode"))
										.equalsIgnoreCase("CP")
										|| (event.getAttribute("EventTrkCode"))
												.equalsIgnoreCase("IP")) {
									if (releasableQty == 0.0) {
										invalidEvents.add(event);
									} else if (releasableQty > 0.0
											&& isFirstEvent) {
										eventMerQty = event
												.getAttribute("MerQty");
										event.setAttribute("MerQty", newMerQty);
										isFirstEvent = false;
									} else if (eventMerQty
											.equalsIgnoreCase(event
													.getAttribute("MerQty"))) {
										isFirstEvent = false;
										invalidEvents.add(event);
										hasMultipleCPEvent = true;
									}
								}
								else if((event.getAttribute("EventTrkCode"))
										.equalsIgnoreCase("CO")
										|| (event.getAttribute("EventTrkCode"))
												.equalsIgnoreCase("V")){
									if (releasableQty == 0.0) {
										invalidEvents.add(event);
									}
								}
							}
							if (hasMultipleCPEvent)
								System.out
										.println("Multiple CP Event Scenario");

							for (Element e : invalidEvents) {
								e.getParentNode().removeChild(e);
							}

							if (eventList.getLength() == 0) {
								lineExtn.removeChild(hdEvent);
							}
						}
					}
				}

				for (int m = 0; m < shipmentList.getLength(); m++) {
					Element shipment = (Element) shipmentList.item(m);
					NodeList shipmentLinesList = shipment
							.getElementsByTagName("ShipmentLines");
					Element shipmentLine = (Element) shipmentLinesList.item(0);
					Node shipmentNode = shipmentLinesList.item(0);
					NodeList shipmentLineList = shipmentLine
							.getElementsByTagName("ShipmentLine");
					Set<Element> invalidShipmentLines = new HashSet<Element>();
					for (int l = 0; l < shipmentLineList.getLength(); l++) {
						getInvalidShipmentLines(releasableQty, shipment,
								invalidShipmentLines, releaseLineNo, m,
								shipmentNode, shipmentLineList, l);
					}
					for (Element e : invalidShipmentLines) {
						e.getParentNode().removeChild(e);
					}

					if (shipmentLineList.getLength() == 0) {
						ordersNode.removeChild(shipment);
					}

				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		String modifiedXML = getStringFromDocument(doc);
		String outputFile = "c:\\Tax\\Response\\" + orderNumber + ".xml";
		FileWriter fw = new FileWriter(outputFile, true);
		fw.write(modifiedXML);
		fw.close();
		return modifiedXML;

	}

	public static void getInvalidShipmentLines(Double releasableQty,
			Element shipment, Set<Element> invalidShipments,
			String releaseLineNo, int m, Node shipmentNode,
			NodeList shipmentLineList, int l) {
		Element relLine = (Element) shipmentLineList.item(l);
		NodeList lines = relLine.getElementsByTagName("Extn");
		Element lineInfo = (Element) lines.item(0);
		String line = lineInfo.getAttribute("ExtnHostOrderLineReference");
		if (line.equalsIgnoreCase(releaseLineNo)) {
			if (releasableQty == 0.0) {
				invalidShipments.add(relLine);
			} else if (releasableQty > 0.0) {
				relLine.setAttribute("Quantity", releasableQty.toString());
			}
		}
	}

	public static Double getReleasableQty(Double releaseQty,
			Map<String, Double> lineStatusInfo,
			Map<String, String> statusCodeDetails) {
		// System.out.println("Inside getReleasableQty fn");
		// System.out.println("StatuCodeMap:" + statusCodeDetails);
		Double status;
		Double qty;
		String statusCode;
		String statusDesc;
		Double releasableQty = 0.0;
		System.out.print("	LineStatus-Qty  ");
		for (Map.Entry<String, Double> en : lineStatusInfo.entrySet()) {
			status = Double.parseDouble(en.getKey());
			statusDesc = (statusCodeDetails.get(en.getKey()) != null) ? statusCodeDetails
					.get(en.getKey()) : "Other";
			qty = en.getValue();
			System.out.print(statusDesc + "-" + qty + "  ");
			if (status < 3700) {
				if (status != 1400)
					releasableQty = releasableQty + qty;
			}
		}
		if (releasableQty > releaseQty) {
			releasableQty = releaseQty;
		}
		return releasableQty;
	}

	public static Map<String, Double> getStatus(String orderRef, String lineRef) {
		// TODO Auto-generated method stub

		final String connectionURL = "jdbc:oracle:thin://@apragor11-scan.homedepot.com:1521/dpr77mm_sro01";
		final String uName = "MMUSR01";
		final String uPassword = "COMS_MMUSR01";

		String statusCheckquery = "select yol.extn_host_order_line_ref, yors.status, yors.status_quantity from yfs_order_line yol, thd01.yfs_order_release_status yors"
				+ " where yol.order_header_key in (select order_header_key from thd01.yfs_order_header where extn_host_order_ref ='"
				+ orderRef
				+ "' and document_type = '0001') and yol.extn_host_order_line_ref in('"
				+ lineRef
				+ "') and yors.status_quantity > 0 and yol.order_line_key = yors.order_line_key";

		// System.out.println(statusCheckquery);

		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;
		Map<String, Double> statusMap = new HashMap<String, Double>();
		String status = null;
		Double qty = null;
		Double statusQty = null;

		try {

			con = DriverManager.getConnection(connectionURL, uName, uPassword);
			// System.out.println("Sterling Connection Opened");

			stmt = con.createStatement();

			rs = stmt.executeQuery(statusCheckquery);

			while (rs.next()) {
				status = rs.getString(2).trim();
				qty = Double.parseDouble(rs.getString(3));
				statusQty = statusMap.get(status);
				statusMap.put(status, (statusQty == null) ? qty : statusQty
						+ qty);
			}
			
			stmt.close();
			con.close();

		}

		catch (Exception e) {
			e.printStackTrace();
		}

		return statusMap;

	}

	public static String getStringFromDocument(Document doc)
			throws TransformerException {
		DOMSource domSource = new DOMSource(doc);
		StringWriter writer = new StringWriter();
		StreamResult result = new StreamResult(writer);
		TransformerFactory tf = TransformerFactory.newInstance();
		Transformer transformer = tf.newTransformer();
		transformer.transform(domSource, result);
		return writer.toString();
	}

}
