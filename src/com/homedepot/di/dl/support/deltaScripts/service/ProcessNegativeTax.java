package com.homedepot.di.dl.support.deltaScripts.service;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;

import com.homedepot.di.dl.support.util.DBConnectionUtil;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;

/*
 * This is the query that will be executed in the database to get the input file
select trim(yol.shipnode_key),',', yoh.extn_host_order_ref 
from THD01.yfs_order_header yoh, THD01.yfs_order_line yol				
where yol.order_header_key = yoh.order_header_key and 
yoh.DOCUMENT_TYPE = '0001' and yol.PRIME_LINE_NO = 1 and
yoh.extn_host_order_ref in ('H0222-18088');

 */
public class ProcessNegativeTax {
	private static final Logger logger = Logger.getLogger(ProcessNegativeTax.class);
	private static final Date currentDate = new Date();
	
    public static void main (String args[]){
       logger.debug("ProcessNegativeTax Program Started at "+java.util.GregorianCalendar.getInstance().getTime());
       try {
              getOrdersFromCSV();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
       logger.debug("ProcessNegativeTax Program Completed at "+java.util.GregorianCalendar.getInstance().getTime());
    }
    
    public static void getOrdersFromCSV() throws Exception 
    {
    	String inputFileLocation = "c:\\NegativeTax\\NegativeTaxInputOrderList.csv";
    	int totalNoOfRowsUpdated = 0;
        try {
                BufferedReader br = new BufferedReader(new FileReader(inputFileLocation));

                String line = "";
                
                while ((line = br.readLine()) != null) {
                    if (!(",,".equalsIgnoreCase(line))) {
                        int noOfRowsUpdated = 0;
                        String tokens[] = line.split(",");
                        String store = tokens[0].trim();
                        String orderNumber = tokens[1].trim();
                        if(store != null && "DEFAULT_VENDOR_NODE".equalsIgnoreCase(store)){
                        	//get the store number from the order number
                        	store = orderNumber.substring(1, 5);
                        }                        
                        int len = store.length();
                        if (len < 4) {
                            for (int i = 0; i < (4 - len); i++) {
                                            store = "0" + store;
                            }
                        }
                        noOfRowsUpdated = getDeltas(orderNumber,store);
                        logger.info(store+"  "+noOfRowsUpdated+" rows deleted");
                        totalNoOfRowsUpdated += noOfRowsUpdated;
                    }
                }
                br.close();
        } catch (Exception e) {
                e.printStackTrace();
                throw e;
        }
        logger.info("ProcessNegativeTax : Total number of row updates: " + totalNoOfRowsUpdated);
        return;
    }

    
    public static int getDeltas(String orderNumber, String store) throws Exception {

    	DateFormat dFormat = new SimpleDateFormat("yyyyMMdd-HHmmss");

    	String outputFile = "c:\\NegativeTax\\NegativeTaxProcessedOrderList" + dFormat.format(currentDate) + ".csv";

		FileWriter fw = new FileWriter(outputFile, true);

        int noOfRowsUpdated = 0;
       
        Connection con = null;
        Statement stmt = null;
        ResultSet result = null;

        fw.append(orderNumber);
    	fw.append(',');
    	fw.append(store);
    	fw.append(',');
  	
        String query = "select * from comt_ord_chg_msg where cust_ord_id in ('"+orderNumber+"') and trnsm_stat_ind = 'E'";
        int resultCount = 0;
        
        try {

        	DBConnectionUtil dbConn = new DBConnectionUtil();
        	con = dbConn.getStoreconnection(store);

            stmt = con.createStatement();
            result = stmt.executeQuery(query);
            
            if(result != null ){
	            while(result.next())
	            {
	            	resultCount++;
	            	
	            	String xmlDelta = result.getString(7);
	
	            	String dupStr = xmlDelta;
	            	
	            	//parse messageId
	            	int messageIdFirstIndex = xmlDelta.indexOf("MessageId=\"");
	            	messageIdFirstIndex = messageIdFirstIndex + "MessageId=\"".length();
	            	int messageIdLastIndex = xmlDelta.indexOf("\"", messageIdFirstIndex);
	            	String messageId = xmlDelta.substring(messageIdFirstIndex, messageIdLastIndex);
	            	//String messageId = xmlDelta.substring(messageIdFirstIndex, messageIdFirstIndex+36);
		        	fw.append(messageId);
		        	fw.append(',');  
		        	
	            	String newLockId = applyLockOnOrder(orderNumber,store);
	            	String response = null;
	            	
	            	if (newLockId != null && !newLockId.isEmpty())
	            	{
		            	if(xmlDelta.contains("ExtnLockID"))
		            	{
		                	int firstIndex = dupStr.indexOf("ExtnLockID");
		                	firstIndex = firstIndex + 12;
		                	int lastIndex = firstIndex + 9;
		                	String lockId = dupStr.substring(firstIndex, lastIndex);
		                	xmlDelta = xmlDelta.replace(lockId, newLockId);
		            	}
		            	else
		            	{
		            		xmlDelta = xmlDelta.replaceFirst("<Extn", "<Extn ExtnLockID=\""+newLockId+"\"");
		            	}
            	
		            	String xml = removeNegativeTax(xmlDelta);
		            	
		            	String responseXML = null;
		            	
		            	if(xml.contains("ExtnDerivedTransactionType=\"ORDER_CREATE\""))
		            	{
		            		
		            	fw.append("ORDER_CREATE");
		            	fw.append(',');
		            	responseXML = callTocreateOrder(xml);
		            	}
		            	
		            	else if (xml.contains("ExtnDerivedTransactionType=\"ORDER_MODIFY\""))
		            	{
		            		fw.append("ORDER_MODIFY");
		            		fw.append(',');
		                	responseXML = callTomodifyOrder(xml);
		            	}
		            	
		            	
		            	response = CheckResponseType(responseXML);
	            	} else {
	            		response = "Unable to obtain lock for order";
	            	}
	            	fw.append(response);
	            	fw.append("\n");
	            	
	            	if(response.contains("SUCCESS"))
	            	{
	            		noOfRowsUpdated = deleteDelta(orderNumber,store);
	            	}
	            	else
	            	{
	            		logger.debug(orderNumber +" : XML was not processed successfully : "+ response);
	            	}
	            }

            	//fw.append(",");
            	//fw.append("\n");
            } 
            stmt.close();
            
            con.close();

        } catch (Exception e) {
            logger.error("unable to connect to store : " + store);
            logger.error(e.getMessage());
        	fw.append(",");
        	fw.append(",");
        	fw.append("Failure - INFORMIX DB: unable to connect to store : " + store);
        }
        if(resultCount == 0){
        	fw.append(",");
        	fw.append("\n");
        }
        fw.close();
        return noOfRowsUpdated;
    }
    
    
    public static int deleteDelta(String orderNumber,String store) throws Exception {

        int noOfRowsUpdated = 0;
      
        String query = "update comt_ord_chg_msg set trnsm_stat_ind = 'X', last_upd_ts = sysdate, last_upd_sysusr_id = 'COMSupport' where cust_ord_id in ('"+orderNumber+"') and trnsm_stat_ind = 'E'";
        //String query = "delete from comt_ord_chg_msg where cust_ord_id in ('"+orderNumber+"') and trnsm_stat_ind = 'E'";
        Connection con = null;
        try {

        	DBConnectionUtil dbConn = new DBConnectionUtil();
        	con = dbConn.getStoreconnection(store);
                        
            Statement stmt = null;
            stmt = con.createStatement();
            noOfRowsUpdated = stmt.executeUpdate(query);

            stmt.close();
            if (!con.getAutoCommit()) {
              con.commit();
            }
            con.close();
            
            logger.debug("Delta deleted for order number: " + orderNumber);

        } catch (Exception e) {
	        logger.error("unable to connect to store : " + store);
	        logger.error(e.getMessage());
        }
        
        return noOfRowsUpdated;
}
    
    public static String applyLockOnOrder(String extnHostOrderRef,
			String shipnodeKey) {
	    String extnLockID = null;
	
		String url = "http://multichannel-custom.homedepot.com/COMOrder/rs/getOrderDetail";
		String inputXML = "<Order DocumentType='0001' EnterpriseCode='HDUS'><Extn ExtnHostOrderReference='"
				+ extnHostOrderRef
				+ "' ExtnPutOrderOnLock='N' ExtnStoreRequestingLock='"
				+ shipnodeKey.trim() + "' ExtnUserId='COMSupport'/></Order>";
	
		String headerValue = "F4EJPD2Z7TbyLUP4llcrQsxPaE6yMjM0y8KkbvP8cwCN9YKBs7Qxdm3lrRppaPiGzrq7djLoSp5IfA8pJMFYtuVCHsTWEX4W6v5GpfL6E8J57j0ZfJQmhwbZJla1cbzPKvaAShYH1KIxhhmxg1RsesWqVH54fyPZRviJFURRVhfx0FuqPYxdaaGSL4GgnPym1da7lti6bOblN0EOxHK45fm5gkf5vV3eQmNaq5C7NqtSVbiaMIZUJgkGTdrdeY7HVqx7FmFNj8rphDVQuNfeUHSt2euMu6zeB4y";
	
		String headerName = "THDService-Auth";
		String orderRecallNoLock = null;
		
		try{
			Client client = ClientManager.getClient();
		
			WebResource webResource = ((com.sun.jersey.api.client.Client) client)
					.resource(url);
		
			orderRecallNoLock = webResource.header(headerName, headerValue)
					.type("text/xml").post(String.class, inputXML);
		
			if (orderRecallNoLock.contains("ExtnLockID")) {
				extnLockID = getLockId(orderRecallNoLock);
			} else {
				extnLockID = getOrderDetail(extnHostOrderRef, shipnodeKey, true);
			}
			logger.debug("External lockID: " + extnLockID); 
		}catch(Exception e){
			e.printStackTrace();
		}
		return extnLockID;
    }
    
    
    private static String getOrderDetail(String extnHostOrderRef,
			String shipnodeKey, boolean lockRequired) throws Exception {

		String extnLockID = null;
		String lockReq = "N";
		if (lockRequired) {
			lockReq = "Y";
		}

		String url = "http://multichannel-custom.homedepot.com/COMOrder/rs/getOrderDetail";
		String inputXML = "<Order DocumentType='0001' EnterpriseCode='HDUS'><Extn ExtnHostOrderReference='"
				+ extnHostOrderRef
				+ "' ExtnPutOrderOnLock='"
				+ lockReq
				+ "' ExtnStoreRequestingLock='"
				+ shipnodeKey.trim()
				+ "' ExtnUserId='COMSupport'/></Order>";

		String headerValue = "F4EJPD2Z7TbyLUP4llcrQsxPaE6yMjM0y8KkbvP8cwCN9YKBs7Qxdm3lrRppaPiGzrq7djLoSp5IfA8pJMFYtuVCHsTWEX4W6v5GpfL6E8J57j0ZfJQmhwbZJla1cbzPKvaAShYH1KIxhhmxg1RsesWqVH54fyPZRviJFURRVhfx0FuqPYxdaaGSL4GgnPym1da7lti6bOblN0EOxHK45fm5gkf5vV3eQmNaq5C7NqtSVbiaMIZUJgkGTdrdeY7HVqx7FmFNj8rphDVQuNfeUHSt2euMu6zeB4y";

		String headerName = "THDService-Auth";
		String outputXML = null;
		try {
			Client client = ClientManager.getClient();

			WebResource webResource = ((com.sun.jersey.api.client.Client) client)
					.resource(url);

			outputXML = webResource.header(headerName, headerValue)
					.type("text/xml").post(String.class, inputXML);

			if (outputXML.contains("ResponseDescription=\"SUCCESS\"")) {
				extnLockID = getLockId(outputXML);

			} else {
				logger.error("ProcessegativeTax: getOrderDetail failed for "
						+ extnHostOrderRef + "	" + shipnodeKey);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return extnLockID;
	}

	/**
	 * This method looks for the attribute ExtnLockID in the input provided and
	 * returns its value
	 */

	public static String getLockId(String str) {
		String extnLockID = null;
		if (str.contains("ExtnLockID")) {
			String dupStr = str;
			String lock = null;
			int firstIndex = dupStr.indexOf("ExtnLockID");
			firstIndex = firstIndex + 12;
			int lastIndex = firstIndex + 11;
			lock = dupStr.substring(firstIndex, lastIndex);
			lastIndex = lock.indexOf("\"");
			lock = lock.substring(0, lastIndex);
			extnLockID = lock;
		}
		return extnLockID;
	}

	
	public static String callTocreateOrder(String inputXML) {
	String url = "http://multichannel-custom.homedepot.com/COMOrder/rs/createSalesOrderForServicesSynchronously";

	String headerValue = "F4EJPD2Z7TbyLUP4llcrQsxPaE6yMjM0y8KkbvP8cwCN9YKBs7Qxdm3lrRppaPiGzrq7djLoSp5IfA8pJMFYtuVCHsTWEX4W6v5GpfL6E8J57j0ZfJQmhwbZJla1cbzPKvaAShYH1KIxhhmxg1RsesWqVH54fyPZRviJFURRVhfx0FuqPYxdaaGSL4GgnPym1da7lti6bOblN0EOxHK45fm5gkf5vV3eQmNaq5C7NqtSVbiaMIZUJgkGTdrdeY7HVqx7FmFNj8rphDVQuNfeUHSt2euMu6zeB4y";

	String headerName = "THDService-Auth";
	String response = null;
	
	try{
		Client client = ClientManager.getClient();
	
		WebResource webResource = ((com.sun.jersey.api.client.Client) client)
				.resource(url);
	
		response = webResource.header(headerName, headerValue)
				.type("text/xml").post(String.class, inputXML);

	}catch(Exception e){
		e.printStackTrace();
	}
	return response;
    }
	
	
	public static String callTomodifyOrder(String inputXML) {
		String url = "http://multichannel-custom.homedepot.com/COMOrder/rs/modifySalesOrderForServicesSynchronously";

		String headerValue = "F4EJPD2Z7TbyLUP4llcrQsxPaE6yMjM0y8KkbvP8cwCN9YKBs7Qxdm3lrRppaPiGzrq7djLoSp5IfA8pJMFYtuVCHsTWEX4W6v5GpfL6E8J57j0ZfJQmhwbZJla1cbzPKvaAShYH1KIxhhmxg1RsesWqVH54fyPZRviJFURRVhfx0FuqPYxdaaGSL4GgnPym1da7lti6bOblN0EOxHK45fm5gkf5vV3eQmNaq5C7NqtSVbiaMIZUJgkGTdrdeY7HVqx7FmFNj8rphDVQuNfeUHSt2euMu6zeB4y";

		String headerName = "THDService-Auth";
		String response = null;
		
		try{
			Client client = ClientManager.getClient();
	
			WebResource webResource = ((com.sun.jersey.api.client.Client) client)
					.resource(url);
	
			response = webResource.header(headerName, headerValue)
					.type("text/xml").post(String.class, inputXML);
	
		}catch(Exception e){
			e.printStackTrace();
		}
		return response;
	    }
	
	private static String CheckResponseType(String response) {

		String result = "";
		if (response == null || "".equalsIgnoreCase(response)) {
			result = "No response";
		} else if (response.contains("ResponseDescription=\"SUCCESS\">")) {
			result = "SUCCESS";
		} else if (response.contains("Error")) {
			int firstIndex = response.indexOf("ResponseDescription");
			firstIndex = firstIndex + 21;
			int lastIndex = response.indexOf("<Errors>");
			if("DUPLICATE ORD".equalsIgnoreCase(response.substring(firstIndex, lastIndex - 5))){
				result = "SUCCESS - "
						+ response.substring(firstIndex, lastIndex - 5);
			} else {
			result = "Failure - "
					+ response.substring(firstIndex, lastIndex - 5);
			}
		}
		logger.debug("ProcessNegativeTax: Response status : " + result);
		return result;
	}

	//remove the <LineTax> element corresponding to the negative tax item
	public static String removeNegativeTax(String xml){
		String str = xml;
		// <LineTax Tax="-0.01" TaxName="Tax"/>
		String tagStart = "<LineTax Tax=\"-";
		String tagEnd =  "/>";

		while (str.contains(tagStart)) {
			int startIndex = str.indexOf(tagStart);
			int endIndex = str.indexOf("/>", startIndex+tagStart.length());
			str = str.replaceFirst(str.substring(startIndex, endIndex+tagEnd.length()), "");
		}
		//logger.debug("ProcessNegativeTax: xml with negative tax removed " + str);
		return str;
	}

}
