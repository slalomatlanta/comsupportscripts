package com.homedepot.di.dl.support.deltaScripts.service;

import java.io.FileReader;
import java.sql.Array;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.homedepot.di.dl.support.util.Constants;
import com.homedepot.di.dl.support.util.DBConnectionUtil;

import au.com.bytecode.opencsv.CSVReader;

public class TransStillInProgressToN {
  private static final Logger logger = Logger
      .getLogger(TransStillInProgressToN.class);
  private static final Logger oLogger = Logger.getLogger("splunkLog");

  public static void main(String[] args) {
    //log program started
    logger.debug("TransStillInProgressToN Program Started at " + java.util.GregorianCalendar.getInstance().getTime());

    //run program
    updateDeltas();

    //log program completed
    logger.debug("TransStillInProgressToN Program Completed at " + java.util.GregorianCalendar.getInstance().getTime());
  }

  public static void updateDeltas() {

    //ArrayList for store list
    ArrayList<String> storeList = new ArrayList<String>();
    
    //QA stores
//    storeList.add("0097");
//    storeList.add("0014");
//    storeList.add("0017");
//    storeList.add("0035");
    
    //get complete store list from db 
    storeList = getStoreList();

    StoreThreads1.threadList.clear();
    StoreThreads1.nameList.clear();
    StoreThreads1.totalNoOfRowsUpdated = 0;

    int count = 1;
    for (String store : storeList) {
      String name = "" + count;
      StoreThreads1 st = new StoreThreads1(store, name);
      st.startThread();
      count++;
    }

    try {

      for (Thread thread : StoreThreads1.threadList) {
        thread.join();
      }
    } catch (InterruptedException e) {
      e.printStackTrace();
    }

    logger.info("Total number of rows updated: " + StoreThreads1.totalNoOfRowsUpdated);
        
    oLogger.info("Program=TransStillInProgressToN status=Success Number_of_stores="+storeList.size()+" number_of_rows_updated="+StoreThreads1.totalNoOfRowsUpdated);
}

  public static ArrayList<String> getStoreList() {
    ArrayList<String> storeList = new ArrayList<String>();
    
    try {
      
      final String driverClass = "oracle.jdbc.driver.OracleDriver";
      final String connectionURLThin = "jdbc:oracle:thin://@spragor10-scan.homedepot.com:1521/dpr78mm_sro01";
      //jdbc:oracle:thin:@pprmm77x.homedepot.com:1521/dpr78mm_srw02- ATC
      final String userID = "MMUSR01";
      final String userPassword = "COMS_MMUSR01";
      Connection con = null;
      con = DriverManager.getConnection(connectionURLThin, userID,
          userPassword);
      Class.forName(driverClass);
      
      Statement stmt = null;
      stmt = con.createStatement();
      String query = "select distinct loc_nbr from thd01.comt_loc_capbl where COMT_CAPBL_ID = 7 order by loc_nbr";
      ResultSet result = stmt.executeQuery(query);
      
      if (result != null) {
        while (result.next()) {
          storeList.add(result.getString(1));
        }
      }

    } catch (Exception e) {
    logger.error("Exception Occured " + e.getMessage());
      e.printStackTrace();
    }
    return storeList;
  }

}

class StoreThreads1 implements Runnable {
  private static final Logger logger = Logger.getLogger(StoreThreads1.class);
  private Thread t;
  private String storeNumber;
  private String name;
  static int totalNoOfRowsUpdated;
  static ArrayList<Thread> threadList = new ArrayList<Thread>();
  static ArrayList<String> nameList = new ArrayList<String>();

  StoreThreads1(String store, String no) {
    this.storeNumber = store;
    this.name = no;
  }

  public void run() {

   

    Connection con = null;
    ResultSet result = null;
    ArrayList<String> orderNumbers = new ArrayList<String>();
    int rowsUpdated = 0;
    
    try {

    	DBConnectionUtil dbConn = new DBConnectionUtil();
    	con = dbConn.getStoreconnection(storeNumber);

      String selectQuery = "select cust_ord_id, trnsltn_excpt_txt from comt_ord_trnsltn_err";
      StringBuilder updateQuery = new StringBuilder("update comt_ord_chg_msg set trnsm_stat_ind = 'N', last_upd_ts = sysdate, last_upd_sysusr_id = 'COMSupport' "
              + "where cust_ord_id in (");

      Statement stmt = con.createStatement();
      result = stmt.executeQuery(selectQuery);
      
      while (result.next()) {
        String orderNumber = result.getString(1);
        String error = result.getString(2);
        if (error.equalsIgnoreCase("STERLING ERROR: Cannot Process Request. Previous Transaction Still In Progress For Order.") ) {
            orderNumbers.add(orderNumber);
        }
      }      
      
      if (orderNumbers.size() > 0){
        for (int i = 0; i < orderNumbers.size(); i++) {
            updateQuery.append ("'" +  orderNumbers.get(i) + "'");
            if (i != orderNumbers.size() - 1) {
              updateQuery.append(", ");
            } else {
              updateQuery.append(")");
            }
          }
          
          rowsUpdated = stmt.executeUpdate(updateQuery.toString());
      }
      
      totalNoOfRowsUpdated += rowsUpdated;
      
      System.out.println("Rows updated at store " + storeNumber + ": " + rowsUpdated);
      System.out.println("Total rows updated: " + totalNoOfRowsUpdated);
      
      stmt.close();
      con.close();
      
      

    } catch (Exception e) {
        logger.error("unable to connect to store : " + storeNumber);
        logger.error(e.getMessage());
        e.printStackTrace();
    }

    logger.debug("Thread " + name + ") Completed for " + storeNumber + " - " + rowsUpdated + " rows updated");
  }

  public void startThread() {
    // System.out.println("Starting " + storeNumber);
    if (t == null) {
      t = new Thread(this, storeNumber);
      threadList.add(t);
      t.setName(name);
      nameList.add(name);
      t.start();
      logger.debug("Thread " + name + ") Submitted for " + storeNumber);
    }
  }
}
