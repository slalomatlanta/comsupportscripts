package com.homedepot.di.dl.support.deltaScripts.service;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import com.homedepot.di.dl.support.util.DBConnectionUtil;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;

public class CreateLock {
	
	
    
    public static void main (String args[]){
                    //String fileLocn = "c:\\Tax\\ReturnWithoutReleaseDeltas.csv";
    	
                    String fileLocn = "c:\\Tax\\storeOrderlist.csv";
                    
                    try {
                                    System.out.println(getStoreOrdersFromCSV(fileLocn)+" total rows deleted");
                    } catch (Exception e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                    }
    }
    
    public static int getStoreOrdersFromCSV(
                                    String fileLocn) throws Exception {
    	

		
                    int totalNoOfRowsUpdated = 0;
                    try {
                                    BufferedReader br = new BufferedReader(new FileReader(fileLocn));

                                    String line = "";
                                    
                                    while ((line = br.readLine()) != null) {
                                                    if (!(",,".equalsIgnoreCase(line))) {
                                                                    int noOfRowsUpdated = 0;
                                                                    String tokens[] = line.split(",");
                                                                    String store = tokens[0].trim();
                                                                    String orderNumber = tokens[1].trim();
                                                                    System.out.println(store);
                                                                    int len = store.length();
                                                                    if (len < 4) {
                                                                                    for (int i = 0; i < (4 - len); i++) {
                                                                                                    store = "0" + store;
                                                                                    }
                                                                    }
                                                                    
                                                                     
                                                                    noOfRowsUpdated = getDeltas(orderNumber,store);
                                                                    System.out.println(store+"  "+noOfRowsUpdated+" rows deleted");
                                                                    totalNoOfRowsUpdated += noOfRowsUpdated;
                                                    }
                                    }
                                    br.close();
                    } catch (Exception e) {
                                    e.printStackTrace();
                                    throw e;

                    }
                    
                    return totalNoOfRowsUpdated;
    }

    
    public static int getDeltas(String orderNumber, String store) throws Exception {
    	
    				
String outputFile = "c:\\Tax\\Response.csv";
		
		FileWriter fw = new FileWriter(outputFile, true);
		
                    int noOfRowsUpdated = 0;
                   
                    Connection con = null;
                    Statement stmt = null;
                    ResultSet result = null;
                    String query = "select * from comt_ord_chg_msg where cust_ord_id in ('"+orderNumber+"') and trnsm_stat_ind = 'F'";
                    fw.append(orderNumber);
                	fw.append(',');
                	fw.append(store);
                	fw.append(',');
                    try {

                    	DBConnectionUtil dbConn = new DBConnectionUtil();
                    	con = dbConn.getStoreconnection(store);

                                    stmt = con.createStatement();
                                    result = stmt.executeQuery(query);
                                    while(result.next())
                                    {
                                    	
                                    	String xml = result.getString(7);
                                    	//System.out.println(xml);
                                    	
                                    	
                                    	String dupStr = xml;
                                    	
                                    	String newLockid = applyLockOnOrder(orderNumber,store);
                                    	
                                    	if(xml.contains("ExtnLockID"))
                                    	{
                                    	int firstIndex = dupStr.indexOf("ExtnLockID");
                                    	firstIndex = firstIndex + 12;
                                    	int lastIndex = firstIndex + 9;
                                    	String lockId = dupStr.substring(firstIndex, lastIndex);
                                    	xml = xml.replace(lockId, newLockid);
                                    	}
                                    	
                                    	else
                                    	{
                                    		
                                    		xml = xml.replaceFirst("<Extn", "<Extn ExtnLockID=\""+newLockid+"\"");
                                    	}
                                    	//System.out.println("XML to be processed : "+xml);
                                    	
                                    	String responseXML = null;
                                    	
                                    	if(xml.contains("ExtnDerivedTransactionType=\"ORDER_CREATE\""))
                                    	{
                                    		
                                    	fw.append("ORDER_CREATE");
                                    	fw.append(',');
                                    	responseXML = callTocreateOrder(xml);
                                    	}
                                    	
                                    	else if (xml.contains("ExtnDerivedTransactionType=\"ORDER_MODIFY\""))
                                    	{
                                    		fw.append("ORDER_MODIFY");
                                    		fw.append(',');
                                        	responseXML = callTomodifyOrder(xml);
                                    	}
                                    	
                                    	
                                    	String response = CheckResponseType(responseXML);
                                    	fw.append(response);
                                    	fw.append("\n");
                                    	//System.out.println(response);
                                    	
                                    	if(response.equalsIgnoreCase("SUCCESS"))
                                    	{
                                    		noOfRowsUpdated = deleteDelta(orderNumber,store);
                                    		
                                    	}
                                    	else
                                    	{
                                    		System.out.println(orderNumber +" : XML was not processed successfully : "+ response);
                                    	}
                                    	
                                    	
                                    }
                                    stmt.close();
                                    
                                    con.close();

                    } catch (Exception e) {

                                    System.out.println("unable to connect to store : " + store);
                                    System.out.println(e.getMessage());
                                    

                    }
                    fw.close();
                    return noOfRowsUpdated;
    }
    
    
    public static int deleteDelta(String orderNumber,String store) throws Exception {

        int noOfRowsUpdated = 0;
      
        String query = "delete from comt_ord_chg_msg where cust_ord_id in ('"+orderNumber+"') and trnsm_stat_ind = 'F'";
        Connection con = null;
        try {

        	DBConnectionUtil dbConn = new DBConnectionUtil();
        	con = dbConn.getStoreconnection(store);
                        
                        Statement stmt = null;
                        stmt = con.createStatement();
                        noOfRowsUpdated = stmt.executeUpdate(query);

                        stmt.close();
                        if (!con.getAutoCommit()) {
                                        con.commit();
                        }
                        con.close();
                        
                        System.out.println("Delta deleted");

        } catch (Exception e) {

                        System.out.println("unable to connect to store : " + store);
                        System.out.println(e.getMessage());
                        

        }
        
        return noOfRowsUpdated;
}
    
    public static String applyLockOnOrder(String extnHostOrderRef,
			String shipnodeKey) {
    String extnLockID = null;
	String envName = "PR";
	String url = "http://multichannel-custom.homedepot.com/COMOrder/rs/getOrderDetail";
	String inputXML = "<Order DocumentType='0001' EnterpriseCode='HDUS'><Extn ExtnHostOrderReference='"
			+ extnHostOrderRef
			+ "' ExtnPutOrderOnLock='N' ExtnStoreRequestingLock='"
			+ shipnodeKey.trim() + "' ExtnUserId='COMSupport'/></Order>";

	String headerValue = "F4EJPD2Z7TbyLUP4llcrQsxPaE6yMjM0y8KkbvP8cwCN9YKBs7Qxdm3lrRppaPiGzrq7djLoSp5IfA8pJMFYtuVCHsTWEX4W6v5GpfL6E8J57j0ZfJQmhwbZJla1cbzPKvaAShYH1KIxhhmxg1RsesWqVH54fyPZRviJFURRVhfx0FuqPYxdaaGSL4GgnPym1da7lti6bOblN0EOxHK45fm5gkf5vV3eQmNaq5C7NqtSVbiaMIZUJgkGTdrdeY7HVqx7FmFNj8rphDVQuNfeUHSt2euMu6zeB4y";

	String headerName = "THDService-Auth";
	String orderRecallNoLock = null;
	
	try{
	Client client = ClientManager.getClient();

	WebResource webResource = ((com.sun.jersey.api.client.Client) client)
			.resource(url);

	orderRecallNoLock = webResource.header(headerName, headerValue)
			.type("text/xml").post(String.class, inputXML);

//	System.out.println("orderRecallNoLock : " + orderRecallNoLock);

	if (orderRecallNoLock.contains("ExtnLockID")) {

		extnLockID = getLockId(orderRecallNoLock);
//		System.out.println(extnLockID); //
//		System.out.println("getOrderDetail :  " + orderRecallNoLock);
	} else {
		extnLockID = getOrderDetail(extnHostOrderRef, shipnodeKey, true);
	}
	
	}catch(Exception e){
		e.printStackTrace();
	}
	return extnLockID;
    }
    
    
    private static String getOrderDetail(String extnHostOrderRef,
			String shipnodeKey, boolean lockRequired) throws Exception {

		String extnLockID = null;
		String lockReq = "N";
		if (lockRequired) {
			lockReq = "Y";
		}

		String url = "http://multichannel-custom.homedepot.com/COMOrder/rs/getOrderDetail";
		String inputXML = "<Order DocumentType='0001' EnterpriseCode='HDUS'><Extn ExtnHostOrderReference='"
				+ extnHostOrderRef
				+ "' ExtnPutOrderOnLock='"
				+ lockReq
				+ "' ExtnStoreRequestingLock='"
				+ shipnodeKey.trim()
				+ "' ExtnUserId='COMSupport'/></Order>";

		String headerValue = "F4EJPD2Z7TbyLUP4llcrQsxPaE6yMjM0y8KkbvP8cwCN9YKBs7Qxdm3lrRppaPiGzrq7djLoSp5IfA8pJMFYtuVCHsTWEX4W6v5GpfL6E8J57j0ZfJQmhwbZJla1cbzPKvaAShYH1KIxhhmxg1RsesWqVH54fyPZRviJFURRVhfx0FuqPYxdaaGSL4GgnPym1da7lti6bOblN0EOxHK45fm5gkf5vV3eQmNaq5C7NqtSVbiaMIZUJgkGTdrdeY7HVqx7FmFNj8rphDVQuNfeUHSt2euMu6zeB4y";

		String headerName = "THDService-Auth";
		String outputXML = null;
		try {
			Client client = ClientManager.getClient();

			WebResource webResource = ((com.sun.jersey.api.client.Client) client)
					.resource(url);

			outputXML = webResource.header(headerName, headerValue)
					.type("text/xml").post(String.class, inputXML);

			if (outputXML.contains("ResponseDescription=\"SUCCESS\"")) {
				extnLockID = getLockId(outputXML);
				return extnLockID;
			} else {
				System.out.println("getOrderDetail failed for "
						+ extnHostOrderRef + "	" + shipnodeKey);
				extnLockID = "getOrderDetail failed for " + extnHostOrderRef
						+ "	" + shipnodeKey;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return extnLockID;
	}

	/**
	 * This method looks for the attribute ExtnLockID in the input provided and
	 * returns its value
	 */

	public static String getLockId(String str) {
		// System.out.println(str);
		String extnLockID = null;
		if (str.contains("ExtnLockID")) {
			String dupStr = str;
			String lock = null;
			int firstIndex = dupStr.indexOf("ExtnLockID");
			firstIndex = firstIndex + 12;
			int lastIndex = firstIndex + 11;
			lock = dupStr.substring(firstIndex, lastIndex);
			lastIndex = lock.indexOf("\"");
			lock = lock.substring(0, lastIndex);
			// System.out.println(lock);
			extnLockID = lock;
		}
		return extnLockID;
	}

	
	public static String callTocreateOrder(String inputXML) {
    String extnLockID = null;
	String envName = "PR";
	String url = "http://multichannel-custom.homedepot.com/COMOrder/rs/createSalesOrderForServicesSynchronously";
	

	String headerValue = "F4EJPD2Z7TbyLUP4llcrQsxPaE6yMjM0y8KkbvP8cwCN9YKBs7Qxdm3lrRppaPiGzrq7djLoSp5IfA8pJMFYtuVCHsTWEX4W6v5GpfL6E8J57j0ZfJQmhwbZJla1cbzPKvaAShYH1KIxhhmxg1RsesWqVH54fyPZRviJFURRVhfx0FuqPYxdaaGSL4GgnPym1da7lti6bOblN0EOxHK45fm5gkf5vV3eQmNaq5C7NqtSVbiaMIZUJgkGTdrdeY7HVqx7FmFNj8rphDVQuNfeUHSt2euMu6zeB4y";

	String headerName = "THDService-Auth";
	String response = null;
	
	try{
	Client client = ClientManager.getClient();

	WebResource webResource = ((com.sun.jersey.api.client.Client) client)
			.resource(url);

	response = webResource.header(headerName, headerValue)
			.type("text/xml").post(String.class, inputXML);

//	System.out.println("orderRecallNoLock : " + orderRecallNoLock);

	
	}catch(Exception e){
		e.printStackTrace();
	}
	return response;
    }
	
	
	public static String callTomodifyOrder(String inputXML) {
	    String extnLockID = null;
		String envName = "PR";
		String url = "http://multichannel-custom.homedepot.com/COMOrder/rs/modifySalesOrderForServicesSynchronously";
		

		String headerValue = "F4EJPD2Z7TbyLUP4llcrQsxPaE6yMjM0y8KkbvP8cwCN9YKBs7Qxdm3lrRppaPiGzrq7djLoSp5IfA8pJMFYtuVCHsTWEX4W6v5GpfL6E8J57j0ZfJQmhwbZJla1cbzPKvaAShYH1KIxhhmxg1RsesWqVH54fyPZRviJFURRVhfx0FuqPYxdaaGSL4GgnPym1da7lti6bOblN0EOxHK45fm5gkf5vV3eQmNaq5C7NqtSVbiaMIZUJgkGTdrdeY7HVqx7FmFNj8rphDVQuNfeUHSt2euMu6zeB4y";

		String headerName = "THDService-Auth";
		String response = null;
		
		try{
		Client client = ClientManager.getClient();

		WebResource webResource = ((com.sun.jersey.api.client.Client) client)
				.resource(url);

		response = webResource.header(headerName, headerValue)
				.type("text/xml").post(String.class, inputXML);

//		System.out.println("orderRecallNoLock : " + orderRecallNoLock);

		
		}catch(Exception e){
			e.printStackTrace();
		}
		return response;
	    }
	
	private static String CheckResponseType(String response) {

		String result = "";
		if (response == null || "".equalsIgnoreCase(response)) {
			result = "No response";
		} else if (response.contains("ResponseDescription=\"SUCCESS\">")) {
			result = "SUCCESS";
		} else if (response.contains("Error")) {
			int firstIndex = response.indexOf("ResponseDescription");
			firstIndex = firstIndex + 21;
			int lastIndex = response.indexOf("<Errors>");
			result = "Failure - "
					+ response.substring(firstIndex, lastIndex - 5);
		}

		System.out.println("Response status : " + result);
		return result;

	}
	
   }
