package com.homedepot.di.dl.support.deltaScripts.service;

public class Constants {
	// JNDI
	public static String STERLING_SSC_RO_JNDI = "jdbc/ORCL.PPRMM78X-DPR077MM.001"; // Sterling SSC Read only DB Connection
	public static String STERLING_ATC_RW_JNDI = "jdbc/ORCL.PPRMM77X-DPR077MM.001";//Sterling ATC Read Write DB Connection
	public static String COMT_SSC_RO_JNDI = "jdbc/ORCL.PPRMM78X-DPR078MM.001";// Comt SSC Read only DB connection
	public static String COMT_ATC_RO_JNDI = "jdbc/ORCL.PPRMM77X-DPR078MM.001";// Comt ATC Read write DB connection
	public static String COM_SUPPORT_MYSQL_JNDI = "jdbc/MYSQL-DB";// COM Support MySql DB connection
	public static String COM_SUPPORT_APPLLOG_JNDI = "jdbc/APPLLOG-DB";// COM Support APPL LOG DB connection
	public static String ODS_STAND_BY_JNDI = "jdbc/ORCL.PPRMM77X-DPR078MM-ODS-RO";// ODS standby DB connection
	public static String ODS_JNDI = "jdbc/ORCL.PPRMM77X-DPR078MM-ODS.001";// ODS standby REad Write	
	
	
	public static String STERLING_ATC_RW1_JNDI = "jdbc/ORCL.PPRMM77X-DPR77MM_SRW01.001";
	public static String STERLING_ATC_RW2_JNDI = "jdbc/ORCL.PPRMM77X-DPR77MM_SRW02.001";
	public static String STERLING_ATC_RW3_JNDI = "jdbc/ORCL.PPRMM77X-DPR77MM_SRW03.001";
	public static String COM_ATC_RW1_JNDI = "jdbc/ORCL.PPRMM77X-DPR78MM_SRW01.001";
	
	
	//Host URL
	public static final String STERLING_URL = String
			.format("http://multichannel-sterling.homedepot.com/smcfs/interop/InteropHttpServlet");
	
	//PR TOKEN
	
	
	public static final String TOKEN_PR =
			 "F4EJPD2Z7TbyLUP4llcrQsxPaE6yMjM0y8KkbvP8cwCN9YKBs7Qxdm3lrRppaPiGzrq7djLoSp5IfA8pJMFYtuVCHsTWEX4W6v5GpfL6E8J57j0ZfJQmhwbZJla1cbzPKvaAShYH1KIxhhmxg1RsesWqVH54fyPZRviJFURRVhfx0FuqPYxdaaGSL4GgnPym1da7lti6bOblN0EOxHK45fm5gkf5vV3eQmNaq5C7NqtSVbiaMIZUJgkGTdrdeY7HVqx7FmFNj8rphDVQuNfeUHSt2euMu6zeB4y";

	public static String driverClass = "com.informix.jdbc.IfxDriver";
	public static String userID = "dburisad";
	public static String userPassword = "466yAwd5ADo=";
	public static String storeJDBCUrl = "jdbc:informix-sqli://ispb.st%s.homedepot.com:25001/store:INFORMIXSERVER=onconfig_tcp";
	
	public static String PURGE_THRESHOLD_LONG_RUNNING_QUERY="06:30";
	public static int STERLING_CONNECTION_COUNT=120;
	
	// Queries
	public static String DD_INSTR_SELECT = "select instruction_detail_key from THD01.YFS_INSTRUCTION_DETAIL a where a.INSTRUCTION_DETAIL_KEY > '20151130' and a.TABLE_NAME = 'YFS_ORDER_HEADER' "
			+ " and a.INSTRUCTION_TYPE = 'D'  and a.INSTRUCTION_TEXT like 'Appliance Ordering System%'  and a.INSTRUCTION_TEXT not in ('Appliance Ordering System - Select Resend Existing Order Button"
			+ " and Select A Delivery Date To Resend This Order!')"
			+ " and a.EXTN_CLOSE_DATE is null   and a.EXTN_USER_ID = 'ApplOE'";
	public static String DD_INSTR_UPDATE = "update THD01.YFS_INSTRUCTION_DETAIL set EXTN_CLOSE_DATE = sysdate, EXTN_CLOSE_USER_ID = 'SYSCOM', modifyts = sysdate, MODIFYUSERID = 'COMSupport' where INSTRUCTION_DETAIL_KEY in (%s)";

	public static String DD_INSTR_SELECT_UPDATE = "select 'update THD01.YFS_INSTRUCTION_DETAIL set EXTN_CLOSE_DATE = sysdate, EXTN_CLOSE_USER_ID = ''SYSCOM'', modifyts = sysdate, MODIFYUSERID = ''COMSupport'' where INSTRUCTION_DETAIL_KEY='''||"
			+ "instruction_detail_key||'''' from THD01.YFS_INSTRUCTION_DETAIL a where a.INSTRUCTION_DETAIL_KEY > '20151130' and a.TABLE_NAME = 'YFS_ORDER_HEADER' "
			+ " and a.INSTRUCTION_TYPE = 'D'  and a.INSTRUCTION_TEXT like 'Appliance Ordering System%'  and a.INSTRUCTION_TEXT not in ('Appliance Ordering System - Select Resend Existing Order Button"
			+ " and Select A Delivery Date To Resend This Order!')"
			+ " and a.EXTN_CLOSE_DATE is null   and a.EXTN_USER_ID = 'ApplOE'";
	
	public static String getMessageFromErrorTxnId = "select message from thd01.yfs_reprocess_error where errortxnid = ";
	public static String updateDummyPONumber_1 = "update thd01.yfs_order_header set extn_po_number = ";
	public static String updateDummyPONumber_2 = ", modifyts = sysdate, modifyuserid = 'JDA_DupePO_Fix' where order_header_key = ";
	public static String getPOOrderHeaderKey = "select max(yoh.order_header_key) from thd01.yfs_order_header yoh, thd01.yfs_order_line yol " +
			"where yoh.order_header_key = yol.order_header_key and yoh.DOCUMENT_TYPE = '0005' and (yoh.extn_host_order_ref, yol.EXTN_HOST_ORDER_LINE_REF, yoh.extn_po_number) in ";
	
	public static String getInputForManageInventoryNodeControlAPI = "select  trim(yol.ITEM_ID), 'MVNDR-'||trim(yol.EXTN_MVENDOR_NO), trim(yol.PRODUCT_CLASS), trim(yol.UOM) " +
			"from thd01.yfs_order_header yoh, thd01.yfs_order_line yol " +
			"where yoh.order_header_key = yol.order_header_key and yoh.DOCUMENT_TYPE = '0001' and " +
			"(yoh.extn_host_order_ref, yol.EXTN_HOST_ORDER_LINE_REF) in ";
	
	public static String expireReservation = "update thd01.yfs_inventory_reservation set " +
			"expiration_date=sysdate-30, modifyts=sysdate, MODIFYUSERID='JDA_DupePO_Fix' where reservation_id = "; 
	
	//Queue
	
	public static String rtamQueue = "jms/DI.DL.STERLING.RTAMOP3.INT_put";
	public static String rtamQueue_q1 = "jms/DI.DL.STERLING.RTAMOP3.INT.Q1_put";
	public static String rtamQueue_q3 = "jms/DI.DL.STERLING.RTAMOP3.INT.Q3_put";
	
	public static String changeOrderQueue_PR = "jms/DI.DL.STERLING.CHANGEORDER.PR_put";
	public static String poCreateQueue_PR = "jms/DI.DL.STERLING.STH.POCREATE.PR_put";
	
	//Webservices
	public static String NON_STERLING_DEMAND_LOADER_SERVICE_URL = "http://webbatch.homedepot.com/COMNonSterlingDemandLoader/LoadStoreDemand?list";
	

}
	