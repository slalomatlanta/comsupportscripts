package com.homedepot.di.dl.support.YmodeErrorDeltas.web;

import java.io.IOException;
import java.util.GregorianCalendar;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.homedepot.di.dl.support.ErrorDeltas.web.DeltaServlet;
import com.homedepot.di.dl.support.YmodeErrorDeltas.dao.DeltaError;

/**
 * Servlet implementation class DeltaServlet
 */
public class DeltaServletYmode extends HttpServlet {
	private static final long serialVersionUID = 1L;
	final static Logger logger = Logger.getLogger(DeltaServletYmode.class);
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeltaServletYmode() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		logger.info("Y Mode Delta Error Report Started : "+ GregorianCalendar.getInstance().getTime());
		DeltaError de = new DeltaError();
		
		try {
			de.deltaReport();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.info("Y Mode Delta Error Report Exception : "+ GregorianCalendar.getInstance().getTime()+e);
		}
		
		logger.info("Y Mode Delta Error Report Completed : "+ GregorianCalendar.getInstance().getTime());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
