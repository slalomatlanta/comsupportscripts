package com.homedepot.di.dl.support.sterlingProbe.dao;

import org.apache.log4j.Logger;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

	
	/**
	 * Jersey Client to call the Sterling services for the Order Interface
	 * <br>
	 * This class handles the communication to and from the service
	 *  
	 */

	public class SterlingJerseyClient {
		
		private static final Logger logger = Logger
				.getLogger(SterlingJerseyClient.class);
		
		/**
		 * This method creates a jersey client and hits the Sterling. It
		 * gets the response for the postRequest sent.
		 * 
		 * @param url
		 * @param postRequest
		 * @throws ProcessingException
		 */
		public ClientResponse send(String url, String postRequest)	{	
			
			long startTime = System.currentTimeMillis();
			
			//Map<KeyNameEnum, Object> infoLogger = OrangeLogManager.getLoggerMap(Thread.currentThread().getName());
			
			
			// Create Jersey client
			Client client = Client.create();
			client.setConnectTimeout(30000);
			client.setReadTimeout(30000);
			String token = Constants.PRToken;
			
			// Create WebResource 
			WebResource webResource = client.resource(url);
			ClientResponse response = null;
			
			try {				
//				response = webResource.header("THDService-Auth", token)
//				.type("application/x-www-form-urlencoded")
//				.post(ClientResponse.class, postRequest);
				
				response = webResource
						.header("THDService-Auth", token) 
						.type("text/xml").post(ClientResponse.class, postRequest);
			} 
			catch (Exception e)
			{
				logger.info("Unexpected error from webresource: " + e.getMessage());
				logger.info("Response Time: " + (System.currentTimeMillis()-startTime));
				
			}
			
			logger.info("Client response: " + response);
			// Retrieve status and check if it is OK
			int status = response.getStatus();
			
			logger.info("Callout HTTP Response: " + status);

			if (status != 200) {
				logger.info("Unsuccesful Status response : " + status);
				logger.info("Response Time: " + (System.currentTimeMillis()-startTime));
				 
			}
			// Log results
			
//			logger.info("Sterling Response Time: " + (System.currentTimeMillis()-startTime));
			
			return response;
		}
		
		
public ClientResponse send(String url)	{	
			
			long startTime = System.currentTimeMillis();
			
			//Map<KeyNameEnum, Object> infoLogger = OrangeLogManager.getLoggerMap(Thread.currentThread().getName());
			
			
			// Create Jersey client
			Client client = Client.create();
			client.setConnectTimeout(30000);
			client.setReadTimeout(30000);
			
			int status = 201;
			// Create WebResource 
			WebResource webResource = client.resource(url);
			ClientResponse response = null;
			
			try {				
//				response = webResource.header("THDService-Auth", token)
//				.type("application/x-www-form-urlencoded")
//				.post(ClientResponse.class, postRequest);
				
				response = webResource
						.get(ClientResponse.class);
			} 
			catch (Exception e)
			{
				logger.info("Unexpected error from webresource: " + e.getMessage());
				logger.info("Response Time: " + (System.currentTimeMillis()-startTime));
				
			}
			
			logger.info("Client response: " + response);
			// Retrieve status and check if it is OK
			if(response!=null)
			status = response.getStatus();
			
			logger.info("Callout HTTP Response: " + status);

			if (status != 200) {
				logger.info("Unsuccesful Status response : " + status);
				logger.info("Response Time: " + (System.currentTimeMillis()-startTime));
				 
			}
			// Log results
			
//			logger.info("Sterling Response Time: " + (System.currentTimeMillis()-startTime));
			
			return response;
		}
}
