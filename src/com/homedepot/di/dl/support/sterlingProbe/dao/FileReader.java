package com.homedepot.di.dl.support.sterlingProbe.dao;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;


public class FileReader 
{

	public FileReaderDTO readFromTxtFile(String file)  {
   		// TODO Auto-generated method stub
		FileReaderDTO dto = new FileReaderDTO();
   		ArrayList<String> serverList = new ArrayList<String>();
   		InputStream stream;
   		int counter=0;
		try {
			stream = new FileInputStream(file);
		
   		InputStreamReader ipsr=new InputStreamReader(stream);
   		BufferedReader br=new BufferedReader(ipsr);
           String line = "";
           while ((line=br.readLine())!=null){
               //System.out.println(line);
           	serverList.add(line);
           	counter++;
           	
           }
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		dto.setNoOfServers(counter);
		dto.setServerList(serverList);
		
   		return dto;
   	}
}
