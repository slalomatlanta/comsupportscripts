package com.homedepot.di.dl.support.sterlingProbe.dao;

import java.io.File;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;

import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeMessage.RecipientType;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.homedepot.di.dl.support.blockingLockmonitor.dto.MonitorLockDTO;
import com.homedepot.di.dl.support.sterlingProbe.dao.*;


import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;


public class SterlingProbeCheck 
{

	private static final Logger logger = Logger.getLogger(SterlingProbeCheck.class);
	private static Client client = null;
	public String getProbeResult() 
	{
		
		FileReaderDTO fileDTO = new FileReaderDTO();
		String serversUp = null;
		int upCounter = 0;
		int downCounter = 0;
		String serversDown = null;
		FileReader reader = new FileReader();
			
		
		String mailContent = " Probe check Passed on all Sterling Servers";
		
		ClassLoader cl = getClass().getClassLoader();
		String file = cl.getResource("/SterlingServers.txt").getFile();
				
		fileDTO = reader.readFromTxtFile(file);
   		
   		for (String server : fileDTO.getServerList())
   		{
   			int status = 201;
		try 
		{
			
			String url = "http://"+server+".homedepot.com:15400/COMSterlingProbe/ProbeSterlingServer?start";
			
			SterlingJerseyClient clnt = new SterlingJerseyClient();
			ClientResponse response = null;

			
			
			response = clnt.send(url);
			if(response!=null)
			{
			status = response.getStatus();
			String probeResponse = response.getEntity(String.class);
			logger.info("Response : " + probeResponse);
			JSONObject xmlJSONObj = new JSONObject(probeResponse);
			
		    String overallStatus = xmlJSONObj.getString("OverallStatus").toString();
		    
		    String appAvailability = xmlJSONObj.getString("ApplicationAvailabilityCheck").toString();
		    
		    logger.info("Status : " + overallStatus + "; Availability : "+ appAvailability);
			}
		    
		    
		    if (status == 200) 
		    {
				
		    	serversUp = serversUp + ","+server;
		    	serversUp = serversUp.replace("null,", "");
		    	upCounter+=1;
			}
		    
		    else 
		    {
		    	logger.info("Unsuccesful Status response for Server "+server);
				
				serversDown = serversDown +","+server;
				serversDown = serversDown.replace("null,", "");
				
				downCounter+=1;
		    }
		    
		    
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
   		
   		if(downCounter > 0)
   		{
   			mailContent = "Hi Team,"+"<br/><br/>"+" Below Sterling Servers are reporting Down." + "<br/><br/>" + serversDown+ "<br/><br/>"+"Thanks,<br/>COM Multichannel Support";
   			
   			sendMail(mailContent,false,null);
   			
   			String mailId =  getPrimaryDetails();
   			
   			sendMail(mailContent,true,mailId);
   		}
   		
   		
		return mailContent;
	}
	
	
public static String getPrimaryDetails() 
	
	{
		
		
		String mailID =  "Order_Mgmt_Support@homedepot.com";
		String response = null;
		
		String url = "http://cpliis6t.homedepot.com:5001/api/groups/ORDER_MANAGEMENT_SUPPORT";
		
		Client client = getClient();

		try {
		WebResource webResource = ((com.sun.jersey.api.client.Client) client)
				.resource(url);

		response = webResource.type("text/plain").get(String.class);
		
		JSONObject xmlJSONObj = new JSONObject(response);
		
	     
		
	       String primary = xmlJSONObj.getString("Primary").toString();
	       
	       int firstIndex = primary.indexOf(";") +1;
	       int lastIndex = primary.length();
	       
	       //mailID = new String[] {primary.substring(firstIndex, lastIndex)};
	       
	       mailID = primary.substring(firstIndex, lastIndex);
	       
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.debug("Probe Check - Getting primary mail details : "+e.getMessage());
		}
		
		return mailID;
	}


public static Client getClient(){
	if(client == null){
		client = Client.create();
	}
	return client;		
}
	

	public void sendMail(String mailContent , boolean isCritical , String mailID) {
		// TODO Auto-generated method stub
		
		
			String text = mailContent;
			// String[] to = { "NIRAL_PATEL2@homedepot.com" ,
			// "DINESH_E@homedepot.com" , "jagatdeep_chakraborty@homedepot.com" ,
			// "MAHESH_VYAKARANAM@homedepot.com" ,
			// "PURUSHOTHAMAN_RAGHUNATH@homedepot.com" ,
			// "RAKESH_J_SATYA@homedepot.com" , "MANOJ_SHUNMUGAM@homedepot.com" };
			//String[] to ={"steven_l_kaneti@homedepot.com"};
			//String[] to = { "mohanraj_gurusamy@homedepot.com" };
			String[] to = {"_2fc77b@homedepot.com"};
			
			if(mailID!=null)
			{
				to = new String[] {mailID};
			}
			
						
			String[] Cc = {};
			Session session = null;
			MimeMessage mimeMessage = null;
			MimeBodyPart mimeBodyPart = null;
			Multipart multiPart = null;
			String date = null;
			Calendar calendar = null;
			Properties properties = null;
			String msg = null;
			try {
				msg = "Success";
				calendar = Calendar.getInstance();
				date = calendar.getTime().toString();
				properties = System.getProperties();
				properties.put("mail.host", "mail1.homedepot.com");
				session = Session.getInstance(properties, null);
				mimeMessage = new MimeMessage(session);
				mimeMessage.setFrom(new InternetAddress(
						"horizon@cpliisad.homedepot.com"));
				InternetAddress[] addressTo = new InternetAddress[to.length];
				InternetAddress[] addressCc = new InternetAddress[Cc.length];
				for (int i = 0; i < to.length; i++) {
					addressTo[i] = new InternetAddress(to[i]);
					logger.debug("To Address " + to[i]);
				}
				for (int j = 0; j < Cc.length; j++) {
					addressCc[j] = new InternetAddress(Cc[j]);
					logger.debug("Cc Address " + Cc[j]);
				}
				mimeMessage.setRecipients(RecipientType.TO, addressTo);
				mimeMessage.setRecipients(RecipientType.CC, addressCc);
				mimeMessage.setSubject("Sterling Probe Check : " + date);
				mimeBodyPart = new MimeBodyPart();
				mimeBodyPart.setText(text.toString());
				multiPart = new MimeMultipart();
				multiPart.addBodyPart(mimeBodyPart);
				mimeMessage.setContent(text.toString(), "text/html");
				if (text.length() > 0) {
					if (isCritical) {
						mimeMessage.setHeader("X-Priority", "1");

						logger.info("Sending mail as high priority - "
								+ java.util.GregorianCalendar.getInstance()
										.getTime());
					} else {
						mimeMessage.setHeader("X-Priority", "3");
						logger.info("Sending mail... - "
								+ java.util.GregorianCalendar.getInstance()
										.getTime());
					}
					Transport.send(mimeMessage);
					logger.info("Sent mail succesfully...");
				}
			} catch (Exception e) {
				msg = "Failure";
				e.printStackTrace();
				logger.error(e.getMessage());
			}
		
	}

}
