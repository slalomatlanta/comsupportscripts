package com.homedepot.di.dl.support.sterlingProbe.dao;


public class Constants {
	
	//JNDI Connection Strings
	public static String STERLING_SSC_RO_JNDI = "jdbc/ORCL.PPRMM78X-DPR077MM.001"; // Sterling SSC Read only DB Connection
	public static String STERLING_ATC_RW_JNDI = "jdbc/ORCL.PPRMM77X-DPR077MM.001";//Sterling ATC Read Write DB Connection
	public static String COMT_SSC_RO_JNDI = "jdbc/ORCL.PPRMM78X-DPR078MM.001";// Comt SSC Read only DB connection
	public static String COMT_ATC_RO_JNDI = "jdbc/ORCL.PPRMM77X-DPR078MM.001";// Comt ATC Read write DB connection
	public static String COM_SUPPORT_MYSQL_JNDI = "jdbc/MYSQL-DB";// COM Support MySql DB connection
	public static String COM_SUPPORT_APPLLOG_JNDI = "jdbc/APPLLOG-DB";// COM Support APPL LOG DB connection
	public static String ODS_STAND_BY_JNDI = "jdbc/ORCL.PPRMM77X-DPR078MM-ODS-RO";// ODS standby DB connection
	public static String ODS_JNDI = "jdbc/ORCL.PPRMM77X-DPR078MM-ODS.001";// ODS standby REad Write	
	
	public static String STERLING_ATC_RW1_JNDI = "jdbc/ORCL.PPRMM77X-DPR77MM_SRW01.001";
	public static String STERLING_ATC_RW2_JNDI = "jdbc/ORCL.PPRMM77X-DPR77MM_SRW02.001";
	public static String STERLING_ATC_RW3_JNDI = "jdbc/ORCL.PPRMM77X-DPR77MM_SRW03.001";
	public static String COM_ATC_RW1_JNDI = "jdbc/ORCL.PPRMM77X-DPR78MM_SRW01.001";
	public static String COM_ATC_RO_ZoneA_JNDI = "jdbc/ORCL.PPRMM77X-DPR78MM_SRO01.001"; //Comt ATC ZoneA Read only DB Connection
	
	public static final  String checkAvailabilityURL = "http://multichannel-online.homedepot.com/COMInventory/rs/checkAvailability";
	
	public static final  String checkAvailabilityRequest = "<CheckAvailabilityRequest version=\"2\">" +
			"<Buffered>true</Buffered> <Items> <Item ItemType=\"SKU\" ItemID=\"161640\"> <Locations> <Location LocationNumber=\"6986\" LocationTypeCode=\"STR\"/> </Locations> </Item> </Items>" +
			"</CheckAvailabilityRequest>";
	
	public static final String PRToken = "F4EJPD2Z7TbyLUP4llcrQsxPaE6yMjM0y8KkbvP8cwCN9YKBs7Qxdm3lrRppaPiGzrq7djLoSp5IfA8pJMFYtuVCHsTWEX4W6v5GpfL6E8J57j0ZfJQmhwbZJla1cbzPKvaAShYH1KIxhhmxg1RsesWqVH54fyPZRviJFURRVhfx0FuqPYxdaaGSL4GgnPym1da7lti6bOblN0EOxHK45fm5gkf5vV3eQmNaq5C7NqtSVbiaMIZUJgkGTdrdeY7HVqx7FmFNj8rphDVQuNfeUHSt2euMu6zeB4y";
	
	//DB Queries
	public static final String GET_SERVER_CONNECTION_COUNT_QUERY = "select count(1), machine from gv$session where module like '%APPSERVER%' and client_info not like '%HM%' group by machine order by machine asc";
	public static final String GET_STERLING_INVENTORY_ADJUSTMENT_COUNT_QUERY = "select count(*) from THD01.yfs_inventory_audit where modifyts > (sysdate-5/(24*60))";
	public static final String GET_COM_INVENTORY_ADJUSTMENT_COUNT_QUERY = "select count(*) from THD01.inv_adj_trnsm where CRT_TS > (sysdate-5/(24*60))";
	public static final String GET_STORES_OFFLINE_COUNT = "select comt_str_mntr_stat_ind, count(*) from thd01.comt_str_mntr where comt_str_mntr_stat_ind in('OFF') group by comt_str_mntr_stat_ind";
	public static final String TRANSMISSION_STATE_QUERY = "select * from bth_sess_parm where attr_nm = 'TransmitState'"; 
	public static final String NEW_EXCEPTION_QUERY = "select flow_name,count(*) from thd01.yfs_reprocess_error where state='Initial'and to_char(createts,'YYYYMMDD') >= to_char(sysdate-(1/24),'YYYYMMDD') group by flow_name order by count(*) desc";
	public static final String GET_DELIVERY_CONFIRMED_COUNT = "select count(*) from thd01.YFS_WORK_ORDER where status='1100.675' and EXTN_HAS_RELATED_WO_FLAG in ('N','Y') and modifyts < (sysdate-12/24) order by modifyts desc";
	public static final String GET_STERLING_ORDER_CREATE_COUNT = "select extn_profile_id , count(*) from thd01.yfs_order_header where  document_type='0001' and createts >= sysdate-(1/24) group by extn_profile_id";
	public static final String GET_STORE_LIST_QUERY = "select loc_nbr from THD01.comt_loc_capbl where comt_capbl_id=7 and loc_nbr not in ('3706') order by loc_nbr";
	public static final String BLOCKING_LOCK_CHK_QUERY = "select /*+ rule */"
			+ " a.inst_id ||',' || a.sid || ',' || a.serial# blk_sess,a.status  BLOCKER_STATUS,"
			+ " a.username blocker,a.machine,a.module,"
			+ " w.ctime DURATION_SECONDS,"
			+ "'select * from '||OWNER||'.'||OBJECT_NAME||' where rowid = '''||"
			+ " dbms_rowid.rowid_create( 1, b.ROW_WAIT_OBJ#,b.ROW_WAIT_FILE#,b.ROW_WAIT_BLOCK#,b.ROW_WAIT_ROW#)||''';' Record, "
			+ " b.inst_id||','||b.sid || ',' || b.serial# wtr_sess,b.status WAITER_STATUS,"
			+ " b.username waiter,b.ROW_WAIT_FILE#,b.ROW_WAIT_BLOCK#,b.ROW_WAIT_ROW#,"
			+ " o.owner || '.' || o.object_name ||"
			+ " nvl2 (subobject_name, '.' || subobject_name, null) blocked_object,"
			+ " a.client_info  BLOCKER_CLIENT_INFO,b.client_info WAITER_CLIENT_INFO"
			+ " from gv$lock h, gv$lock w, gv$session a, gv$session b, dba_objects o"
			+ " where h.block   != 0" + " and h.lmode   != 0"
			+ " and h.lmode   != 1" + " and w.request != 0 "
			+ " and w.id1     = h.id1" + " and w.id2     = h.id2"
			+ " and h.sid     = a.sid"
			+ " and w.sid     = b.sid and h.inst_id = a.inst_id"
			+ " and decode (w.type, 'TX', b.row_wait_obj#," + " 'TM', w.id1)"
			+ " = o.object_id" + " order by w.ctime desc";
	public static String REPLICATION_LAG = "Select value from v$dataguard_stats where name = 'apply lag'";
	public static String STATUS = "select distinct status,status_name from yfs_status where status in ('3700')";
	public static String STRLING_DB_CONN = "select distinct status,status_name from thd01.yfs_status where status in ('3700')";
	public static String COM_DB_CONN = "select * from thd01.bth_sess";
	
	// Splunk Queries
	public static final String STORE_COM_RESYNC_CHECK_QUERY = "index=app src_lcp=\"PR\" context=\"StoreComResync\" host=\"stuxsh01.st*\" status=\"SUCCESS\" earliest=-15m | DEDUP host | stats values(host) by _time | reverse";
	public static final String PO_CREATE_CHECK_QUERY = "index=app context=\"COMOrder\" sourcetype=\"orangelog\" com_event_type=\"POCreate\" (customer_order_number=W* OR customer_order_number=C*) earliest = -4h | table _time,ExtnHostOrderReference | sort _time desc";
	public static final String EJ_UPDATE_CHECK_QUERY = "index=app sourcetype=orangelog context=COMOrder com_event_type=ChangeOrder (Shipped AND EJDate) (customer_order_number=W* OR customer_order_number=C*) earliest = -4h | table _time,ExtnHostOrderReference | sort _time desc";
	public static final String COM_QUEUE_DEPTH_QUERY = "broker=MP04 QUEUE=DI.DL.COM* CURDEPTH > 0 earliest=-1h | dedup QUEUE | stats values(QUEUE) by _time,CURDEPTH | reverse | sort by CURDEPTH desc | head 5";
	public static final String STERLING_QUEUE_DEPTH_QUERY = "broker=MP08 QUEUE=DI.DL.STERLING* CURDEPTH > 0  NOT (QUEUE=*PRG* OR QUEUE=*PURGE* OR QUEUE=*INT)  earliest=-1h | dedup QUEUE | stats values(QUEUE) by _time,CURDEPTH | reverse | sort by CURDEPTH desc | head 5";
	public static final String TRANSMIT_QUEUE_DEPTH_QUERY = "sourcetype=wmq_qstatus  QUEUE=SYSTEM.CLUSTER.TRANSMIT.QUEUE (broker=QM0.PR.US.CPLIIS7C OR broker=QM0.PR.US.CPLIIS7D) earliest=-1h | dedup broker| stats values(broker) by _time,CURDEPTH | reverse"; 
	
	public static final int STERLING_CONNECTION_COUNT = 120;
	
	public static final String GET_ORDER_DETAIL_URL = "http://multichannel-custom.homedepot.com/COMOrder/rs/getOrderDetail";
	public static final String HEADER_VALUE = "F4EJPD2Z7TbyLUP4llcrQsxPaE6yMjM0y8KkbvP8cwCN9YKBs7Qxdm3lrRppaPiGzrq7djLoSp5IfA8pJMFYtuVCHsTWEX4W6v5GpfL6E8J57j0ZfJQmhwbZJla1cbzPKvaAShYH1KIxhhmxg1RsesWqVH54fyPZRviJFURRVhfx0FuqPYxdaaGSL4GgnPym1da7lti6bOblN0EOxHK45fm5gkf5vV3eQmNaq5C7NqtSVbiaMIZUJgkGTdrdeY7HVqx7FmFNj8rphDVQuNfeUHSt2euMu6zeB4y";
	public static final String HEADER_NAME = "THDService-Auth";
	
	public static final String ORDER_RECALL_XML = "<Order DocumentType='0001' EnterpriseCode='HDUS'><Extn ExtnHostOrderReference='H0130-37286' ExtnPutOrderOnLock='N' ExtnStoreRequestingLock='0130' ExtnUserId='COMSupport'/></Order>";
	
	public static String driverClass = "com.informix.jdbc.IfxDriver";
	public static String userID = "dburisad";
	public static String userPassword = "466yAwd5ADo=";
	public static String storeJDBCUrl = "jdbc:informix-sqli://ispb.st%s.homedepot.com:25001/store:INFORMIXSERVER=onconfig_tcp";

}
