package com.homedepot.di.dl.support.sterlingProbe.dao;

import java.util.ArrayList;

public class FileReaderDTO 
{
	private int noOfServers;
	public ArrayList<String> serverList;

	public ArrayList<String> getServerList() {
		return serverList;
	}

	public void setServerList(ArrayList<String> serverList) {
		this.serverList = serverList;
	}

	public int getNoOfServers() {
		return noOfServers;
	}

	public void setNoOfServers(int noOfServers) {
		this.noOfServers = noOfServers;
	}

	
}
