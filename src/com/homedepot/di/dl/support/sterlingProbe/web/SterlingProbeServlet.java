package com.homedepot.di.dl.support.sterlingProbe.web;


import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.homedepot.di.dl.support.ServerConnectionCount.service.ServerConnectionCountServlet;
import com.homedepot.di.dl.support.sterlingProbe.dao.SterlingProbeCheck;

/**
 * Servlet implementation class SterlingProbeServlet
 */
public class SterlingProbeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	final static Logger logger = Logger.getLogger(SterlingProbeServlet.class);
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SterlingProbeServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		logger.info("SterlingProbeServlet Start Time::"+java.util.GregorianCalendar.getInstance().getTime());

		
		SterlingProbeCheck probe = new SterlingProbeCheck();
		
		probe.getProbeResult();
		
		logger.info("SterlingProbeServlet End Time::"+java.util.GregorianCalendar.getInstance().getTime());
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
