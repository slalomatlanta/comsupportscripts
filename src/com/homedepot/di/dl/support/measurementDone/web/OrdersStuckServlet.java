package com.homedepot.di.dl.support.measurementDone.web;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.homedepot.di.dl.support.measurementDone.dao.OrdersStuckInAwaitingMeasurement;




/**
 * Servlet implementation class OrdersStuckServlet
 */
public class OrdersStuckServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	final static Logger logger = Logger.getLogger(OrdersStuckServlet.class);
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public OrdersStuckServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		logger.info("OrdersStuckInAwaitingMeasurement program started at : "
				+ java.util.GregorianCalendar.getInstance().getTime());
		try {
			OrdersStuckInAwaitingMeasurement.postXML();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		logger.info("OrdersStuckInAwaitingMeasurement program completed at : "
				+ java.util.GregorianCalendar.getInstance().getTime());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
