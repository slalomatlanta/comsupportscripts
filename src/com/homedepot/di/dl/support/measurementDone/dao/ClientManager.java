package com.homedepot.di.dl.support.measurementDone.dao;


import com.sun.jersey.api.client.Client;

public class ClientManager {
	private static Client client = null;

	public static Client getClient(){
		if(client == null){
			client = Client.create();
		}
		return client;		
	}
}
