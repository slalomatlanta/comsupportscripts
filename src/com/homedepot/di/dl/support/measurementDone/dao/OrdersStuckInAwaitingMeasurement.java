package com.homedepot.di.dl.support.measurementDone.dao;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeMessage.RecipientType;

import org.apache.log4j.Logger;

import com.homedepot.di.dl.support.measurementDone.web.OrdersStuckServlet;
import com.homedepot.di.dl.support.util.*;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;

public class OrdersStuckInAwaitingMeasurement {

	/**
	 * @param args
	 * @throws Exception 
	 */
	
	final static Logger logger = Logger.getLogger(OrdersStuckInAwaitingMeasurement.class);
	
	public static void postXML() throws Exception
	{
		//String outputFile = "c:\\store\\qa\\Response.csv";
		String outputFile = "/opt/isv/tomcat-6.0.18/temp/MovingToMeasurementDoneResponse.csv";
		FileWriter fw = new FileWriter(outputFile, true);

		fw.append("InstOrderNo");
		fw.append(',');
		fw.append("MeasureOrderNo");
		fw.append(',');
		fw.append("DraftOrderFlag");
		fw.append(',');
		fw.append("LineNo");
		fw.append(',');
		fw.append("InstOrderStatus");
		fw.append(',');
		fw.append("MeasureOrderStatus");
		fw.append(',');
		fw.append("Response");
		fw.append('\n');

		try {
			DBConnectionUtil dbConn = new DBConnectionUtil();

			Connection con = dbConn
					.getJNDIConnection(Constants.STERLING_SSC_RO_JNDI);

			Statement stmt = null;
			ResultSet result = null;

			stmt = con.createStatement();

			String fetchOrders = "select yoh.DRAFT_ORDER_FLAG,"
					+ " yoh.SELLER_ORGANIZATION_CODE, "
					+ " yoh.extn_host_order_ref Install_order, "
					+ " yol.extn_host_order_line_ref install_line, "
					+ " yol.line_type, trim(yol.shipnode_key), "
					+ " yoh.EXTN_SVS_ORDER_STATUS_DESC Install_order_status, "
					+ " yol.EXTN_REL_INST_MSR_ORD_NO Measure_order, "
					+ " mo.EXTN_SVS_ORDER_STATUS_DESC Measure_order_status,"
					+ " yoh.order_header_key, "
					+ " yol.order_line_key, "
					+ " mo.order_header_key,"
					+ " yoh.order_date, "
					+ " mo.order_date, "
					+ " yoh.document_type, "
					+ " yol.ORDERED_QTY, "
					+ " yol.ORIGINAL_ORDERED_QTY, "
					+ " yol.EXTN_TENDER_QUANTITY, "
					+ " yol.EXTN_REFUND_QUANTITY, "
					+ " yol.EXTN_UOM_CONV_QTY,"
					+ " yol.line_total "
					+ " from thd01.yfs_order_header yoh, "
					+ " thd01.yfs_order_line yol, "
					+ " THD01.yfs_order_header mo"
					+ " where yoh.order_header_key = yol.order_header_key "
					+ " and yol.extn_rel_inst_msr_ord_no is not null "
					+ " and yoh.order_header_key > '20160515' "
					+ " and mo.order_header_key > '20160515'"
					+ " and yol.extn_rel_inst_msr_ord_no=mo.extn_host_order_ref "
					+ " and mo.document_type='0001' "
					+ " and mo.extn_svs_order_status='1000' "
					+ " and yoh.EXTN_SVS_ORDER_STATUS = '200'";
			
			Calendar cal = Calendar.getInstance();
	    	DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
	    	
			logger.info("Executing query : " + fetchOrders);
			result = stmt.executeQuery(fetchOrders);
			
			while(result.next())
			{
				
				String currTs = dateFormat.format(cal.getTime());
				String lockId = getLockID(result.getString(3),result.getString(2)); 
				String requestXML= "<?xml version='1.0' encoding='UTF-8'?> "
						+" <Orders> "
						+" <Extn ExtnSVSRemEntriesReqd='N' ExtnLockID='"
						+lockId
						+"' ExtnLockRefStore='"
						+result.getString(2)
						+"' ExtnLockRefUser='CXN8024'/> "
						+" <Order DocumentType='0001' EnterpriseCode='HDUS' OrderDate='"
						+result.getString(13)
						+"' SellerOrganizationCode='"
						+result.getString(2)
						+"' DraftOrderFlag='"
						+result.getString(1)
						+"' >"
						+" <Extn ExtnDerivedTransactionType='ORDER_MODIFY' ExtnHostOrderReference='" 
						+result.getString(3)
						+"' ExtnSVSOrderStatus='310' "
						+" ExtnSVSOrderStatusDesc='Measurement done' ExtnStatusEffectiveTs='" 
						+currTs
						+"' >"
						+" <HDOrderStatusList>"
						+" <HDOrderStatus StatusDescription='Measurement done' StatusEffectiveTs='" 
						+currTs
						+"' StoreNumber='" 
						+result.getString(3)
						+"' StatusCode='310' UserAuditID='SYSCOM'/>"
						+" </HDOrderStatusList>"
						+" </Extn>"
						+" </Order> "
						+" </Orders>";
				
				fw.append(result.getString(3));
				fw.append(',');
				fw.append(result.getString(8));
				fw.append(',');
				fw.append(result.getString(1));
				fw.append(',');
				fw.append(result.getString(4));
				fw.append(',');
				fw.append(result.getString(7));
				fw.append(',');
				fw.append(result.getString(9));
				fw.append(',');
				System.out.println(requestXML);
				//String responseXml = submitReturnReqToSterling(requestXML);
				//String response = CheckResponseType(responseXml);
				String response =null;
				fw.append(response);
				fw.append('\n');
								
			}
			
			result.close();
			stmt.close();
			con.close();
			

		} 
		catch (Exception e) 
		{
			logger.debug("OrdersStuckInAwaitingMeasurement Exception : "+ e);
		}
		
		fw.flush();
		fw.close();
		
		BufferedReader br = new BufferedReader(new FileReader(outputFile));
		String line = "";
	    int count = 0;
	    while ((line = br.readLine()) != null) 
	    {
	    	count+=1;
	    }

	    if(count > 1)
	    {
	    	
	    	sendAttachment();
	    }
	    
	    else
	    {
	    	logger.info("No records found");
	    }
		
	}

		
	public static String CheckResponseType(String response) {

		String result = "";
		if (response == null || "".equalsIgnoreCase(response)) {
			result = "No response";
		} else if (response.contains("<Order DocumentType")) {
			result = "SUCCESS";
		} else if (response.contains("Error")) {
			int firstIndex = response.indexOf("ErrorCode");
			firstIndex = firstIndex + 10;
			int lastIndex = response.indexOf("ErrorDescription");
			result = "Failure - "
					+ response.substring(firstIndex, lastIndex);
		}

		// System.out.println("Response status : " + result);
		return result;

	}
	
	public static String getLockID(String extnHostOrderRef,
			String shipnodeKey) {
    String extnLockID = null;
	String envName = "PR";
	String url = "http://multichannel-custom.homedepot.com/COMOrder/rs/getOrderDetail";
	String inputXML = "<Order DocumentType='0001' EnterpriseCode='HDUS'><Extn ExtnHostOrderReference='"
			+ extnHostOrderRef
			+ "' ExtnPutOrderOnLock='N' ExtnStoreRequestingLock='"
			+ shipnodeKey.trim() + "' ExtnUserId='COMSupport'/></Order>";

	String headerValue = "F4EJPD2Z7TbyLUP4llcrQsxPaE6yMjM0y8KkbvP8cwCN9YKBs7Qxdm3lrRppaPiGzrq7djLoSp5IfA8pJMFYtuVCHsTWEX4W6v5GpfL6E8J57j0ZfJQmhwbZJla1cbzPKvaAShYH1KIxhhmxg1RsesWqVH54fyPZRviJFURRVhfx0FuqPYxdaaGSL4GgnPym1da7lti6bOblN0EOxHK45fm5gkf5vV3eQmNaq5C7NqtSVbiaMIZUJgkGTdrdeY7HVqx7FmFNj8rphDVQuNfeUHSt2euMu6zeB4y";

	String headerName = "THDService-Auth";
	String orderRecallNoLock = null;
	
	try{
	Client client = ClientManager.getClient();

	WebResource webResource = ((com.sun.jersey.api.client.Client) client)
			.resource(url);

	orderRecallNoLock = webResource.header(headerName, headerValue).type("text/xml").post(String.class, inputXML);

//	System.out.println("orderRecallNoLock : " + orderRecallNoLock);

	if (orderRecallNoLock.contains("ExtnLockID")) {

		extnLockID = getLockId(orderRecallNoLock);
//		System.out.println(extnLockID); //
//		System.out.println("getOrderDetail :  " + orderRecallNoLock);
	} else {
		extnLockID = getOrderDetail(extnHostOrderRef, shipnodeKey, true);
	}
	
	}catch(Exception e){
		e.printStackTrace();
	}
	return extnLockID;
    }
    
    
    private static String getOrderDetail(String extnHostOrderRef,
			String shipnodeKey, boolean lockRequired) throws Exception {

		String extnLockID = null;
		String lockReq = "N";
		if (lockRequired) {
			lockReq = "Y";
		}

		String url = "http://multichannel-custom.homedepot.com/COMOrder/rs/getOrderDetail";
		String inputXML = "<Order DocumentType='0001' EnterpriseCode='HDUS'><Extn ExtnHostOrderReference='"
				+ extnHostOrderRef
				+ "' ExtnPutOrderOnLock='"
				+ lockReq
				+ "' ExtnStoreRequestingLock='"
				+ shipnodeKey.trim()
				+ "' ExtnUserId='COMSupport'/></Order>";

		String headerValue = "F4EJPD2Z7TbyLUP4llcrQsxPaE6yMjM0y8KkbvP8cwCN9YKBs7Qxdm3lrRppaPiGzrq7djLoSp5IfA8pJMFYtuVCHsTWEX4W6v5GpfL6E8J57j0ZfJQmhwbZJla1cbzPKvaAShYH1KIxhhmxg1RsesWqVH54fyPZRviJFURRVhfx0FuqPYxdaaGSL4GgnPym1da7lti6bOblN0EOxHK45fm5gkf5vV3eQmNaq5C7NqtSVbiaMIZUJgkGTdrdeY7HVqx7FmFNj8rphDVQuNfeUHSt2euMu6zeB4y";

		String headerName = "THDService-Auth";
		String outputXML = null;
		try {
			Client client = ClientManager.getClient();

			WebResource webResource = ((com.sun.jersey.api.client.Client) client)
					.resource(url);

			outputXML = webResource.header(headerName, headerValue)
					.type("text/xml").post(String.class, inputXML);

			if (outputXML.contains("ResponseDescription=\"SUCCESS\"")) {
				extnLockID = getLockId(outputXML);
				return extnLockID;
			} else {
				System.out.println("getOrderDetail failed for "
						+ extnHostOrderRef + "	" + shipnodeKey);
				extnLockID = "getOrderDetail failed for " + extnHostOrderRef
						+ "	" + shipnodeKey;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return extnLockID;
	}

	/**
	 * This method looks for the attribute ExtnLockID in the input provided and
	 * returns its value
	 */

	public static String getLockId(String str) {
		// System.out.println(str);
		String extnLockID = null;
		if (str.contains("ExtnLockID")) {
			String dupStr = str;
			String lock = null;
			int firstIndex = dupStr.indexOf("ExtnLockID");
			firstIndex = firstIndex + 12;
			int lastIndex = firstIndex + 11;
			lock = dupStr.substring(firstIndex, lastIndex);
			lastIndex = lock.indexOf("\"");
			lock = lock.substring(0, lastIndex);
			// System.out.println(lock);
			extnLockID = lock;
		}
		return extnLockID;
	}
	
	

	public static String submitReturnReqToSterling(String requestXML) {

		String sterlingResponseXML = "";
		String url = null;
		String sterlingInputXML = null;

			sterlingInputXML = requestXML;

		String queryString = Utils.createQueryString("HDProcessSVSTransactionSync",
				sterlingInputXML);
		SterlingJerseyClient sterlingJerseyClient = new SterlingJerseyClient();

		url = String
				.format("http://multichannel-sterling.homedepot.com/smcfs/interop/InteropHttpServlet");
		System.out.println("URL : " + url);

		sterlingResponseXML = sterlingJerseyClient.send(url, queryString);

		System.out.println("Sterling HDProcessTransaction Response : " + sterlingResponseXML);
		
		sterlingResponseXML = Utils.removeXMLDeclaration(sterlingResponseXML);
		return sterlingResponseXML;

	}
	
	public static void sendAttachment() throws Exception{
		 String host = "mail1.homedepot.com";
	  String from = "horizon@cpliisad.homedepot.com";
	  String[] to = {"mohanraj_gurusamy@homedepot.com" };
	  //String[] Cc ={""};
	  // Get system properties
	  Properties properties = System.getProperties();

	  // Setup mail server
	  properties.setProperty("mail.smtp.host", host);

	  // Get the default Session object.
	  Session session = Session.getInstance(properties);

	  // Define message
	  Message message = new MimeMessage(session);
	  message.setFrom(new InternetAddress(from));
	  InternetAddress[] addressTo = new InternetAddress[to.length];
	 // InternetAddress[] addressCc = new InternetAddress[Cc.length];
	  for (int i = 0; i < to.length; i++) 
	  { addressTo[i] = new InternetAddress(to[i]); 
	  //System.out.println("To Address "+to[i]);
	  } 
//	  for (int j = 0; j < Cc.length; j++) 
//	  { addressCc[j] = new InternetAddress(Cc[j]); 
//	  System.out.println("Cc Address "+Cc[j]);
//	  }
	  message.setRecipients(RecipientType.TO, addressTo); 
	  //message.setRecipients(RecipientType.CC, addressCc); 
	  message.setSubject("Orders stuck in Awaiting Measurement");

	  // Create the message part 
	  BodyPart messageBodyPart = new MimeBodyPart();
	  String msgContent = "Hi All,<br/><br/>PFA orders fixed which were stuck in Awaiting Measurement.<br/><br/>Thanks<br/>";
    msgContent = msgContent + "COM Multichannel Support";
	  // Fill the message
	  messageBodyPart.setContent(msgContent ,"text/html");
	  	

	  Multipart multipart = new MimeMultipart();
	  multipart.addBodyPart(messageBodyPart);

	  // Part two is attachment
	  messageBodyPart = new MimeBodyPart();
	  //String filename = "C:\\store\\HDPaymentStageInfo.csv";
	  String filename = "/opt/isv/tomcat-6.0.18/temp/Response.csv";
	  DataSource source = new FileDataSource(filename);
	  messageBodyPart.setDataHandler(new DataHandler(source));
	  messageBodyPart.setFileName(source.getName());
	  multipart.addBodyPart(messageBodyPart);

	  // Put parts in message
	  message.setContent(multipart);

	  // Send the message
	  Transport.send(message);
	   //System.out.println("Msg Send ....");
	  }
}
