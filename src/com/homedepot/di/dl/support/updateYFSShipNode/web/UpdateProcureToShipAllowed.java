package com.homedepot.di.dl.support.updateYFSShipNode.web;

import java.io.IOException;
import java.util.GregorianCalendar;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.homedepot.di.dl.support.updateCustPONumber.web.UpdateCustPONoServlet;
import com.homedepot.di.dl.support.updateYFSShipNode.dao.YFSShipNode;

/**
 * Servlet implementation class UpdateProcureToShipAllowed
 */
public class UpdateProcureToShipAllowed extends HttpServlet {
	private static final Logger logger = Logger
			.getLogger(UpdateProcureToShipAllowed.class);
	
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateProcureToShipAllowed() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		YFSShipNode yfssfipnode=new YFSShipNode();
		logger.info("UpdateProcureToShipAllowed - Started : "+GregorianCalendar.getInstance().getTime());
		
		yfssfipnode.updateShipNode();
		
		logger.info("UpdateProcureToShipAllowed - Completed : "+GregorianCalendar.getInstance().getTime());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
