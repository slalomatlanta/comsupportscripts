package com.homedepot.di.dl.support.updateYFSShipNode.dao;

public class YFSShipNodeDTO {

	String ShipNodeKey;
	
	String ShipNode;
	String ProcureToShipNode;
	public String getShipNodeKey() {
		return ShipNodeKey;
	}
	public void setShipNodeKey(String shipNodeKey) {
		ShipNodeKey = shipNodeKey;
	}
	public String getShipNode() {
		return ShipNode;
	}
	public void setShipNode(String shipNode) {
		ShipNode = shipNode;
	}
	public String getProcureToShipNode() {
		return ProcureToShipNode;
	}
	public YFSShipNodeDTO(String shipNodeKey, String shipNode,
			String procureToShipNode) {
		super();
		ShipNodeKey = shipNodeKey;
		ShipNode = shipNode;
		ProcureToShipNode = procureToShipNode;
	}
	public void setProcureToShipNode(String procureToShipNode) {
		ProcureToShipNode = procureToShipNode;
	}
}
