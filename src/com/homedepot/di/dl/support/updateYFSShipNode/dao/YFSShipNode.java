package com.homedepot.di.dl.support.updateYFSShipNode.dao;

import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.homedepot.di.dl.support.util.Constants;
import com.homedepot.di.dl.support.util.DBConnectionUtil;



public class YFSShipNode {

	private static final Logger logger = Logger
			.getLogger(YFSShipNode.class);
	
public void updateShipNode() {
		
		SendMail mailObj = new SendMail();

		ArrayList<YFSShipNodeDTO> dtoList = new ArrayList<YFSShipNodeDTO>();
		dtoList = getYfSShipDetails();
		int noOfRowsUpdated = 0;
		System.out.print("out");
		System.out.print(noOfRowsUpdated);

		try {
			
			//String outputPath = "C:\\store\\UpdateShipNode.csv";
			String outputPath = "/opt/isv/apache-tomcat/temp/UpdateShipNode.csv";
			FileWriter fw = new FileWriter(outputPath);

			fw.append("Ship_Node_key");
			fw.append(',');
			fw.append("Ship_Node");
			fw.append(',');
			fw.append("ProcureToShipNode");
			fw.append('\n');

			for (YFSShipNodeDTO dto : dtoList) {
				
				noOfRowsUpdated = noOfRowsUpdated + updateYFSShipNodeDetails(dto);
				
				System.out.print("inside");
				System.out.println(noOfRowsUpdated);
				
				fw.append(dto.getShipNodeKey());
				fw.append(',');
				fw.append(dto.getShipNode());
				fw.append(',');
				fw.append(dto.getProcureToShipNode());
				fw.append('\n');
			}
			
			fw.close();

		} catch (IOException e) {
			logger.debug(e);
			e.printStackTrace();
		}
		
		try {
			if (dtoList.size() != 0){
			mailObj.sendAttachment(noOfRowsUpdated);
			}
		} catch (Exception e) {
			logger.debug(e);
			e.printStackTrace();
		}

	}
private ArrayList<YFSShipNodeDTO> getYfSShipDetails() {

	ArrayList<YFSShipNodeDTO> dtoList = new ArrayList<YFSShipNodeDTO>();

	DBConnectionUtil dbConn = new DBConnectionUtil();

	String query = "select SHIPNODE_KEY,SHIP_NODE,PROCURE_TO_SHIP_ALLOWED from YFS_SHIP_NODE where PROCURE_TO_SHIP_ALLOWED in('Y') and SHIPNODE_KEY not in ('0106','0153','6986')";

	Connection conn = null;
	Statement stmt = null;
	ResultSet rs = null;

	try {
		
		
		conn = dbConn.getJNDIConnection(Constants.STERLING_SSC_RO_JNDI);
		Class.forName("oracle.jdbc.driver.OracleDriver");
		
		stmt = conn.createStatement();
		logger.info("DB connection success");
		//System.out.println("DB connection success");
		System.out.println(query);
		rs = stmt.executeQuery(query);

		while (rs.next()) {			
			dtoList.add(new YFSShipNodeDTO(rs.getString(1), rs.getString(2),rs.getString(3)));
		}
		rs.close();
		stmt.close();

		conn.close();

	} catch (Exception e) {
		logger.debug(e);
		e.printStackTrace();
	}

	return dtoList;
}
private int updateYFSShipNodeDetails(YFSShipNodeDTO dto) {
	
	int noOfRowsUpdated = 0;
	

	String query = "update thd01.YFS_SHIP_NODE set PROCURE_TO_SHIP_ALLOWED='N',"
			+ "MODIFYUSERID='COMSupport' where PROCURE_TO_SHIP_ALLOWED='Y' and SHIPNODE_KEY not in ('0106','0153','6986')";
			
	System.out.println("query : " + query);

	DBConnectionUtil dbConn = new DBConnectionUtil();
	Connection conn = dbConn
			.getJNDIConnection(Constants.STERLING_ATC_RW_JNDI);

	Statement stmt = null;

	try {

		stmt = conn.createStatement();
		noOfRowsUpdated = stmt.executeUpdate(query);

		stmt.close();
		if (!conn.getAutoCommit()) {
			conn.commit();
		}
		conn.close();

	} catch (SQLException e) {
		e.printStackTrace();
	}

	return noOfRowsUpdated;
}
}
