package com.homedepot.di.dl.support.thresholdCleanup.service;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import org.apache.log4j.Logger;
import au.com.bytecode.opencsv.CSVReader;

import com.homedepot.di.dl.support.thresholdCleanup.dto.ThresholdDTO;

public class ThresholdCleanupService {
	final static Logger logger = Logger.getLogger(ThresholdCleanupService.class);
	public static void main(String args[]) {
		logger.info("Threshold program started "
				+ java.util.GregorianCalendar.getInstance().getTime());
		callThreshold();
		logger.info("Threshold program completed "
				+ java.util.GregorianCalendar.getInstance().getTime());
	}

	public static void callThreshold() {
		String fileLocation = "C://test//InputSkuList.csv";
		String outputFileLocation = "C://test//SkuOutput.csv";
		// ArrayList<ThresholdDTO> skuList = readInputSpreadSheet(fileLocation);
		HashMap<String, ArrayList<ThresholdDTO>> inputMap = readInputSpreadSheet(fileLocation);
		HashMap<String, ThresholdDTO> outputMap = checkReservationEntry(inputMap);
		try {
			FileWriter writer = new FileWriter(outputFileLocation);
			
			writer.append("SKU");
			writer.append(",");
			writer.append("MVNDR");
			writer.append(",");
			writer.append("Value");
			writer.append("\n");
			
			for (String key : outputMap.keySet()){
				ThresholdDTO td = outputMap.get(key);
				
				writer.append(td.getSku());
				writer.append(",");
				writer.append(td.getMvndr());
				writer.append(",");
				writer.append(""+td.getQuantity());
				writer.append("\n");
			}
			
			writer.flush();
			writer.close();
			logger.info("File created at " + outputFileLocation);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static HashMap<String, ArrayList<ThresholdDTO>> readInputSpreadSheet(
			String fileLocation) {

		HashMap<String, ArrayList<ThresholdDTO>> inputMap = new HashMap<String, ArrayList<ThresholdDTO>>();

		String[] row = null;
		CSVReader csvReader = null;
		boolean header = true;
		try {
			csvReader = new CSVReader(new FileReader(fileLocation));

			while ((row = csvReader.readNext()) != null) {
				if (!header) {
					ArrayList<ThresholdDTO> reservationList = new ArrayList<ThresholdDTO>();

					ThresholdDTO od = new ThresholdDTO();
					od.setSku(row[0]);
					od.setMvndr(row[1]);
					od.setReservationId(row[2]);
					od.setQuantity(Integer.parseInt(row[4]));
					od.setCreatets(row[5]);

					String key = od.getReservationId().trim();

					if (inputMap.containsKey(key)) {
						reservationList = inputMap.get(key);
						reservationList.add(od);
						inputMap.remove(key);
						inputMap.put(key, reservationList);
					}

					else {
						reservationList.add(od);
						inputMap.put(key, reservationList);
					}

				}
				header = false;
			}

			csvReader.close();
		} catch (FileNotFoundException e) {
			return inputMap;
		} catch (IOException e) {
			return inputMap;
		} catch (Exception e) {
			return inputMap;
		}
		return inputMap;
	}

	public static HashMap<String, ThresholdDTO> checkReservationEntry(
			HashMap<String, ArrayList<ThresholdDTO>> inputMap) {
		HashMap<String, ThresholdDTO> outputMap = new HashMap<String, ThresholdDTO>();
		Connection con = null;
		Statement stmt = null;
		ResultSet rset = null;

		try {
			final String driverClass = "oracle.jdbc.driver.OracleDriver";
			final String connectionURL = "jdbc:oracle:thin:@spragor10-scan.homedepot.com:1521/DPR78MM_RO_CODS_01";
			final String uName = "CODSRO01";
			final String uPassword = "juspu6Ew";
			Class.forName(driverClass);
			con = DriverManager.getConnection(connectionURL, uName, uPassword);
			// DBConnectionUtil dbConn = new DBConnectionUtil();
			// Connection con = dbConn
			// .getJNDIConnection(Constants.ODS_STAND_BY_JNDI);
			for (String reservationId : inputMap.keySet()) {
				boolean isRecordPresent = false;
				stmt = null;
				rset = null;
				stmt = con.createStatement();
				String query = "select  * from yfs_inventory_reservation where RESERVATION_ID ='"
						+ reservationId + "' ";
				rset = stmt.executeQuery(query);
				while (rset.next()) {
					isRecordPresent = true;
				}

				if (!isRecordPresent) {
					ArrayList<ThresholdDTO> reservationList = inputMap
							.get(reservationId);
					if (reservationList.size() == 1) {
						ThresholdDTO td1 = reservationList.get(0);
						outputMap.put(reservationId, td1);
					} else {
						Date latestDate = new Date();
						SimpleDateFormat formatter = new SimpleDateFormat(
								"MM/dd/yyyy hh:mm:ss");
						String dateInString = "3/13/2014  7:04:00 PM";
						latestDate = formatter.parse(dateInString);
						ThresholdDTO td2 = new ThresholdDTO();
						// for (int i=0; i<reservationList.size(); i++){
						// ThresholdDTO td1 = reservationList.get(i);
						// ThresholdDTO td2 = new ThresholdDTO();
						// if ((i++) < reservationList.size()){
						// td2 = reservationList.get(i++);
						// }
						//
						// }
						for (ThresholdDTO td : reservationList) {
							String date = td.getCreatets();
							SimpleDateFormat formatter1 = new SimpleDateFormat(
									"MM/dd/yyyy HH:mm");
							Date tdDate = formatter1.parse(date);
							int x = tdDate.compareTo(latestDate);
							if (x > 0) {
								latestDate = tdDate;
								td2 = td;
							} else if (x < 0) {
								// no need any action
							} else if (x == 0) {
								td2 = td;
							}
						}

						outputMap.put(reservationId, td2);
					}
				}
				rset.close();
				stmt.close();
			}
			con.close();
		} catch (Exception e) {
			logger.error("Exception Occurred: " + e.getMessage());
			e.printStackTrace();
		}
		return outputMap;
	}

}
