package com.homedepot.di.dl.support.thresholdCleanup.dto;

public class ThresholdDTO {
	private String sku;
	private String mvndr;
	private String reservationId;
	private int quantity;
	private String createts;
	
	

	public String getReservationId() {
		return reservationId;
	}

	public void setReservationId(String reservationId) {
		this.reservationId = reservationId;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public String getCreatets() {
		return createts;
	}

	public void setCreatets(String createts) {
		this.createts = createts;
	}

	public String getSku() {
		return sku;
	}

	public void setSku(String sku) {
		this.sku = sku;
	}

	public String getMvndr() {
		return mvndr;
	}

	public void setMvndr(String mvndr) {
		this.mvndr = mvndr;
	}

}
