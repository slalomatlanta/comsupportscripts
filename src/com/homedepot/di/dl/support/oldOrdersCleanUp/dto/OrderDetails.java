package com.homedepot.di.dl.support.oldOrdersCleanUp.dto;

public class OrderDetails {
	private String orderRef;
	private String poNumber;
	private String lineRef;
	private String skuCode;
	int qty;
	private String poLineRef;
	private String primeLineNo;
	private String receivingNode;
	private String subLineNo;
	private String uom;
	private String xmlPut;
	
	
	
	public String getXmlPut() {
		return xmlPut;
	}
	public void setXmlPut(String xmlPut) {
		this.xmlPut = xmlPut;
	}
	public String getSubLineNo() {
		return subLineNo;
	}
	public void setSubLineNo(String subLineNo) {
		this.subLineNo = subLineNo;
	}
	public String getUom() {
		return uom;
	}
	public void setUom(String uom) {
		this.uom = uom;
	}
	public String getReceivingNode() {
		return receivingNode;
	}
	public void setReceivingNode(String receivingNode) {
		this.receivingNode = receivingNode;
	}
	public String getOrderRef() {
		return orderRef;
	}
	public void setOrderRef(String orderRef) {
		this.orderRef = orderRef;
	}
	public String getPoNumber() {
		return poNumber;
	}
	public void setPoNumber(String poNumber) {
		this.poNumber = poNumber;
	}
	public String getLineRef() {
		return lineRef;
	}
	public void setLineRef(String lineRef) {
		this.lineRef = lineRef;
	}
	public String getSkuCode() {
		return skuCode;
	}
	public void setSkuCode(String skuCode) {
		this.skuCode = skuCode;
	}
	public int getQty() {
		return qty;
	}
	public void setQty(int qty) {
		this.qty = qty;
	}
	public String getPoLineRef() {
		return poLineRef;
	}
	public void setPoLineRef(String poLineRef) {
		this.poLineRef = poLineRef;
	}
	public String getPrimeLineNo() {
		return primeLineNo;
	}
	public void setPrimeLineNo(String primeLineNo) {
		this.primeLineNo = primeLineNo;
	}
	
}
