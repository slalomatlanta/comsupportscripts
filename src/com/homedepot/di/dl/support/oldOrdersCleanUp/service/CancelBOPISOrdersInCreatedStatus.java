package com.homedepot.di.dl.support.oldOrdersCleanUp.service;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.log4j.Logger;

import com.homedepot.di.dl.support.insertHDEventsAndServiceValidation.service.InsertHDEventsAndUpdateHDPSI;
import com.homedepot.di.dl.support.util.DBConnectionUtil;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.sun.jersey.api.client.WebResource;


public class CancelBOPISOrdersInCreatedStatus {

	final static Logger logger = Logger.getLogger(CancelBOPISOrdersInCreatedStatus.class);

	/**
	 * @param args
	 */
public static void main(String[] args) throws SQLException, IOException, Exception {

		orderCheck();
	}
	
	
	
	public static void orderCheck() throws Exception,ArrayIndexOutOfBoundsException {
		

		 String outputFile = "C:\\test\\OutPut.csv";
		//String outputFile = "/opt/isv/tomcat-6.0.18/temp/OutPut.csv";

		FileWriter fw = new FileWriter(outputFile, false);

		fw.append("OrderNo");
		fw.append(',');
		fw.append("LineNo");
		fw.append(',');
		fw.append("QuantityToCancel");
		fw.append(',');
		fw.append("Response");
		fw.append('\n');
		

		try {
			//final String STERLING_SSC_RO_JNDI = "jdbc/ORCL.PPRMM78X-DPR077MM.001";
			
			  /*final String connectionURL =
			  "jdbc:oracle:thin:@snpagor05-scan:1521/QA01SVC_COMORD01";
			  
			  final String uName = "QA01_STERLING"; 
			  final String uPassword ="QA01_STERLING";
			 */
			
			 final String connectionURL ="jdbc:oracle:thin://@spragor10-scan.homedepot.com:1521/dpr77mm_sro01";
			 final String uName = "MMUSR01";
			 final String uPassword = "COMS_MMUSR01";
			 Connection con = null;
			 Statement stmt = null;
			 ResultSet result = null;
			try {
				//DBConnectionUtil dbConn = new DBConnectionUtil();
				//con = dbConn.getJNDIConnection(STERLING_SSC_RO_JNDI);

				 con = DriverManager.getConnection(connectionURL,uName,uPassword);
				 
				logger.debug("Sterling Connection Opened");
				
				stmt = con.createStatement();
				
				String inputFile = "C:\\test\\OrderList.csv";
				//String outputFile = "/opt/isv/tomcat-6.0.18/temp/OrderList.csv";
				BufferedReader br = new BufferedReader(new FileReader(inputFile));
				
				String strLine = "";
				
					while ((strLine = br.readLine()) != null)
					{
						if (!(",,".equalsIgnoreCase(strLine))) {
							
							String tokens[] = strLine.split(",");
							String orderNumber = tokens[0].trim();
							String lineNo=tokens[1].trim();
							
							
							final String futurePick="SELECT h.extn_host_order_ref AS ExtnHostOrderReference, "
													+ " l.EXTN_HOST_ORDER_LINE_REF AS ExtnHostOrderLineReference,"
													+ " l.EXTN_SKU_CODE AS ExtnSKUCode, "
													+ " l.ORDERED_QTY AS Quantity, "
													+ " l.SHIPNODE_KEY AS ShipNode "
													+ " from thd01.yfs_order_header h, "
													+ " thd01.YFS_ORDER_LINE l "
													+ " WHERE h.document_type='0001' and h.order_header_key=l.order_header_key "
													+ " and h.extn_host_order_ref='"+orderNumber+"'"
													+ " and l.EXTN_HOST_ORDER_LINE_REF='"+lineNo+"'";
							logger.debug("Query : " + futurePick);
					result = stmt.executeQuery(futurePick);
					while (result.next()) {
						orderNumber = result.getString(1);
						logger.debug(orderNumber);
						String Shipnode = result.getString(5);
						String Qty = result.getString(4);
						lineNo = result.getString(2);
						String skuCode=result.getString(3);
						//String extnLockID = result.getString(6).trim();
						String extnLockID = applyLockOnOrder(orderNumber.trim(),
								Shipnode.trim());
						int len = Shipnode.length();
	                    if (len < 4) 
	                            {
	                                  Shipnode = "0" + Shipnode;
	                           }
						
					String requestXML = "<Orders> "
							+ "<Extn ExtnLockID='"
							+ extnLockID
							+ "'/>"
							+ " <Order DocumentType='0001' EnterpriseCode='HDUS' >" 
							+ "<Extn ExtnDerivedTransactionType='ORDER_MODIFY' ExtnHostOrderReference='"
							+ orderNumber
							+ "'>" 
							+ "</Extn>"
							+ "<OrderLines>"
							+ "<OrderLine>"
							+ "           <Extn  ExtnCancelLocation='"
							+ Shipnode.trim()
							+ "' ExtnHostOrderLineReference='"
							+ lineNo
							+ "' ExtnIsTenderSuccessful='N' ExtnProductTypeCode='PRODUCT'  ExtnQuantityToCancel='"
							+ Qty.trim()
							+ "' ExtnSKUCode='"
							+ skuCode
							+ "' ExtnReasonText='Order line is cancelled as part of old orders clean up script' ExtnTransactionType='RETURN'/>"
							+ " <LineTaxes>"
							+ " <LineTax Tax='0' TaxName='Sales' TaxPercentage='0'/>"
							+ " </LineTaxes> "
							+ "    						</OrderLine>"
							+ "   				</OrderLines>"
							+ "    			</Order>"
							+ "    </Orders>" ;
					
					//logger.debug("Request XML: "+requestXML);
					String responseXml = callOrderModifyService(requestXML);
					//logger.debug("Response XML: "+responseXml);
					String response = CheckResponseType(responseXml);
					logger.debug("Response: "+response);
					fw.append(orderNumber);
					fw.append(',');
					fw.append(lineNo);
					fw.append(',');
					fw.append(Qty);
					fw.append(',');
					fw.append(response);
					fw.append("\n");
								}
						}		
				}
					
			}
			
			catch (Exception e) {
				e.printStackTrace();
			}
			result.close();
			stmt.close();
			con.close();

		}catch (Exception e)
			{
				e.printStackTrace();
			}

		// TODO Auto-generated method stub
		fw.flush();
		fw.close();

		//sendAttachment();
	}

	
	private static String CheckResponseType(String response) {

		String result = "";
		if (response == null || "".equalsIgnoreCase(response)) {
			result = "No response";
		} else if (response.contains("<Orders> <Extn ExtnLockID")) {
			result = "SUCCESS";

		} 
		else if (response.contains("Error")) {
			int firstIndex = response.indexOf("ResponseDescription");
			firstIndex = firstIndex + 21;
			int lastIndex = response.indexOf("<Errors>");
			result = "Failure - "
					+ response.substring(firstIndex, lastIndex - 5);
		}
		//System.out.println("Response status : " + result);
		return result;
	}


	private static String applyLockOnOrder(String extnHostOrderRef,
			String shipnodeKey) {

		String extnLockID = null;
		String envName = "PR";
		String url = "http://multichannel-custom.homedepot.com/COMOrder/rs/getOrderDetail";
		String inputXML = "<Order DocumentType='0001' EnterpriseCode='HDUS'><Extn ExtnHostOrderReference='"
				+ extnHostOrderRef
				+ "' ExtnPutOrderOnLock='N' ExtnStoreRequestingLock='"
				+ shipnodeKey.trim() + "' ExtnUserId='COMSupport'/></Order>";

		String headerValue = "F4EJPD2Z7TbyLUP4llcrQsxPaE6yMjM0y8KkbvP8cwCN9YKBs7Qxdm3lrRppaPiGzrq7djLoSp5IfA8pJMFYtuVCHsTWEX4W6v5GpfL6E8J57j0ZfJQmhwbZJla1cbzPKvaAShYH1KIxhhmxg1RsesWqVH54fyPZRviJFURRVhfx0FuqPYxdaaGSL4GgnPym1da7lti6bOblN0EOxHK45fm5gkf5vV3eQmNaq5C7NqtSVbiaMIZUJgkGTdrdeY7HVqx7FmFNj8rphDVQuNfeUHSt2euMu6zeB4y";

		//String headerValue="0UF3C4qryss7pIXu2A2G6iE13eYSiyKtgQ08A2x4PlnOJchufHxU1GsC5NA8MaJJgrq8X46A3B7QS9rXnyPRSPla9ZyVfUZWwmUf6XEvZMAFUeXtXgY61QEnYXSSGpHJ79JvnrpgRa2Bc3WRL9ZXRIKPdtxto1uauZbYuTK9vRPg9u8arRRedWmqYXmjjCaa7ZPVn3OqIdHLWZnyTGEvGr92fZv8mAgqwHvrmFjWsgts7ICJvw96NiU2FhrWwuDFl0tQwLIBDldLXd1uKXXISG9lshDLKkhPr661";
		String headerName = "THDService-Auth";

		Client client = ClientManager.getClient();

		WebResource webResource = ((com.sun.jersey.api.client.Client) client)
				.resource(url);

		String outputXML = webResource.header(headerName,headerValue).type("text/xml")
				.post(String.class, inputXML);

		if (outputXML.contains("ExtnLockID")) {

			extnLockID = getLockId(outputXML);
			logger.debug(extnLockID);
			// System.out.println("getOrderDetail :  " + outputXML);
		} else {
			extnLockID = getOrderDetailWithLock(extnHostOrderRef, shipnodeKey);
		}

		return extnLockID;
	}

	/**
	 * This method calls the COMOrder - getOrderDetail service and requests for
	 * a lock and returns its value
	 */

	private static String getOrderDetailWithLock(String extnHostOrderRef,
			String shipnodeKey) {

		String extnLockID = null;
		String url = "http://multichannel-custom.homedepot.com/COMOrder/rs/getOrderDetail";
		String inputXML = "<Order DocumentType='0001' EnterpriseCode='HDUS'><Extn ExtnHostOrderReference='"
				+ extnHostOrderRef
				+ "' ExtnPutOrderOnLock='Y' ExtnStoreRequestingLock='"
				+ shipnodeKey.trim() + "' ExtnUserId='COMSupport'/></Order>";

		 /*pr */
		String headerValue = "F4EJPD2Z7TbyLUP4llcrQsxPaE6yMjM0y8KkbvP8cwCN9YKBs7Qxdm3lrRppaPiGzrq7djLoSp5IfA8pJMFYtuVCHsTWEX4W6v5GpfL6E8J57j0ZfJQmhwbZJla1cbzPKvaAShYH1KIxhhmxg1RsesWqVH54fyPZRviJFURRVhfx0FuqPYxdaaGSL4GgnPym1da7lti6bOblN0EOxHK45fm5gkf5vV3eQmNaq5C7NqtSVbiaMIZUJgkGTdrdeY7HVqx7FmFNj8rphDVQuNfeUHSt2euMu6zeB4y";
		//String headerValue="0UF3C4qryss7pIXu2A2G6iE13eYSiyKtgQ08A2x4PlnOJchufHxU1GsC5NA8MaJJgrq8X46A3B7QS9rXnyPRSPla9ZyVfUZWwmUf6XEvZMAFUeXtXgY61QEnYXSSGpHJ79JvnrpgRa2Bc3WRL9ZXRIKPdtxto1uauZbYuTK9vRPg9u8arRRedWmqYXmjjCaa7ZPVn3OqIdHLWZnyTGEvGr92fZv8mAgqwHvrmFjWsgts7ICJvw96NiU2FhrWwuDFl0tQwLIBDldLXd1uKXXISG9lshDLKkhPr661";
		String headerName = "THDService-Auth";

		Client client = ClientManager.getClient();

		WebResource webResource = ((com.sun.jersey.api.client.Client) client)
				.resource(url);

		String outputXML = webResource.header(headerName, headerValue).type("text/xml")
				.post(String.class, inputXML);

		if (outputXML.contains("ResponseDescription=\"SUCCESS\"")) {
			extnLockID = getLockId(outputXML);
		} else {
			logger.debug("getOrderDetail failed for " + extnHostOrderRef
					+ "	" + shipnodeKey);
			extnLockID = "getOrderDetail failed for " + extnHostOrderRef + "	"
					+ shipnodeKey;
		}

		return extnLockID;
	}

	/**
	 * This method looks for the attribute ExtnLockID in the input provided and
	 * returns its value
	 */

	public static String getLockId(String str) {
		// System.out.println(str);
		String extnLockID = null;
		if (str.contains("ExtnLockID")) {
			String dupStr = str;
			String lock = null;
			int firstIndex = dupStr.indexOf("ExtnLockID");
			firstIndex = firstIndex + 12;
			int lastIndex = firstIndex + 11;
			lock = dupStr.substring(firstIndex, lastIndex);
			lastIndex = lock.indexOf("\"");
			lock = lock.substring(0, lastIndex);
//			System.out.println(lock);
			extnLockID = lock;
		}
		return extnLockID;
	}
	
	public static String callOrderModifyService(String requestXML) {

		String sterlingResponseXML = "";
		String url = null;
		String sterlingInputXML = null;

		sterlingInputXML = requestXML;

		String queryString = Utils.createQueryString("HDProcessSVSTransactionSync",
				sterlingInputXML);
		SterlingJerseyClient sterlingJerseyClient = new SterlingJerseyClient();

		url = String
				.format("http://multichannel-sterling.homedepot.com/smcfs/interop/InteropHttpServlet");
		logger.info("URL : " + url);

		sterlingResponseXML = sterlingJerseyClient.send(url, queryString);
		
		/*System.out.println("Sterling HDProcessSVSTransactionSync Response : "
				+ sterlingResponseXML);*/
		sterlingResponseXML = Utils.removeXMLDeclaration(sterlingResponseXML);
		return sterlingResponseXML;

	}
}
