package com.homedepot.di.dl.support.oldOrdersCleanUp.service;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import au.com.bytecode.opencsv.CSVReader;

import com.homedepot.di.dl.support.insertBossDummyPayment.dto.OrderDTO;
import com.homedepot.di.dl.support.oldOrdersCleanUp.dto.OrderDetails;
import com.homedepot.di.dl.support.util.XMLUtil;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class KeyReceiveBossOrders {
	private static final Logger logger = Logger
			.getLogger(KeyReceiveBossOrders.class);

	public static void main(String args[]) {
		logger.debug("Program Started");
		ProcessKeyRecRequest();
	}

	public static void ProcessKeyRecRequest() {
		String fileLocation = "C:/test/NoKeyRecOrders.csv";
		ArrayList<OrderDetails> orderList = new ArrayList<OrderDetails>();
		orderList = readOrdersFromCSV(fileLocation);
		for (OrderDetails od : orderList) {
			String requestXML = getRequestXML(od);
			boolean xmlPut = processKeyRecXML(requestXML);
			if (xmlPut) {
				od.setXmlPut("True");
			} else {
				od.setXmlPut("False");
			}
		}
		generateReport(orderList);
	}

	public static void generateReport(ArrayList<OrderDetails> orderList) {
		String outputLocation = "C:/test/KeyRecOrdersFinalReport.csv";
		try {
			FileWriter writer = new FileWriter(outputLocation);
			
			writer.append("Order Number");
			writer.append(",");
			writer.append("Line Number");
			writer.append(",");
			writer.append("XML Put?");
			writer.append("\n");
			
			for (OrderDetails od : orderList){
				writer.append(od.getOrderRef());
				writer.append(",");
				writer.append(od.getLineRef());
				writer.append(",");
				writer.append(od.getXmlPut());
				writer.append("\n");
			}
			
			writer.flush();
			writer.close();
			logger.debug("File created at " + outputLocation);
			
		} catch (Exception e) {
			logger.debug("Exception occurred while generating report : " + e);
			e.printStackTrace();
		}
	}

	public static ArrayList<OrderDetails> readOrdersFromCSV(String fileLocation) {
		ArrayList<OrderDetails> orderList = new ArrayList<OrderDetails>();
		String[] row = null;
		CSVReader csvReader = null;
		boolean header = true;
		try {
			csvReader = new CSVReader(new FileReader(fileLocation));
			while ((row = csvReader.readNext()) != null) {
				if (!header) {
					OrderDetails od = new OrderDetails();
					od.setOrderRef(row[0]);
					od.setLineRef(row[1]);
					if (row[2].length() == 8) {
						od.setPoNumber(row[2]);
					} else {
						od.setPoNumber("0" + row[2]);
					}
					od.setQty(Integer.parseInt(row[3]));
					od.setPoLineRef(row[4]);
					od.setSkuCode(row[5]);
					if ((row[6].trim()).length() == 4) {
						od.setReceivingNode(row[6]);
					} else {
						od.setReceivingNode("0" + row[6]);
					}
					od.setUom(row[7]);
					od.setPrimeLineNo(row[8]);
					od.setSubLineNo(row[9]);

					orderList.add(od);
				} else {
					header = false;
				}
			}
			csvReader.close();
		} catch (FileNotFoundException e) {
			return orderList;
		} catch (IOException e) {
			return orderList;
		} catch (Exception e) {
			return orderList;
		}
		return orderList;
	}

	public static boolean processKeyRecXML(String requestXML) {
		boolean xmlPut = false;
//		String url = "http://ccliqas2:15400/smcfs/interop/InteropHttpServlet";
		 String url =
		 "http://cclidis2:15400/smcfs/interop/InteropHttpServlet";

		String URLEncodedInputXML = null;
		try {
			URLEncodedInputXML = URLEncoder.encode(requestXML, "UTF-8");
		} catch (Exception ex) {
			logger.debug("Exception occurred in send method: " + ex);
		}

		StringBuilder queryString = new StringBuilder();
		queryString.append("InteropApiName=");
		queryString.append("REC");
		queryString.append("&IsFlow=Y");
		queryString.append("&YFSEnvironment.userId=" + "admin");
		queryString.append("&InteropApiData=");
		queryString.append(URLEncodedInputXML);

		String outXml = send(url, queryString.toString());
		if (outXml.contains("ApiSuccess")) {
			xmlPut = true;
		}
		logger.debug("Response XML: " + outXml);
		return xmlPut;
	}

	public static String send(String url, String postRequest) {
		String sterlingResponse = null;
		// Create Jersey client
		Client client = Client.create();

//		String token = "eS6AxTugxondtSBi8KhzUnE2NVPEB4jj1wB7liUQEOUKf4Bn9UWopWx7zJnDbqzN53BG7mkXOgWoJuLH0Py37Ekohwol41hn7qUWh5KU4Mmw14KwQwfOOIgpKFwRnFbSkNk86nYH0g";
		 String token =
		 "F4EJPD2Z7TbyLUP4llcrQsxPaE6yMjM0y8KkbvP8cwCN9YKBs7Qxdm3lrRppaPiGzrq7djLoSp5IfA8pJMFYtuVCHsTWEX4W6v5GpfL6E8J57j0ZfJQmhwbZJla1cbzPKvaAShYH1KIxhhmxg1RsesWqVH54fyPZRviJFURRVhfx0FuqPYxdaaGSL4GgnPym1da7lti6bOblN0EOxHK45fm5gkf5vV3eQmNaq5C7NqtSVbiaMIZUJgkGTdrdeY7HVqx7FmFNj8rphDVQuNfeUHSt2euMu6zeB4y";
		// Create WebResource
		WebResource webResource = client.resource(url);
		ClientResponse response = null;
		try {
			response = webResource.header("THDService-Auth", token)
					.type("application/x-www-form-urlencoded")
					.post(ClientResponse.class, postRequest);

			// Retrieve status and check if it is OK
			int status = 0;
			if (response != null) {
				status = response.getStatus();
			}
		} catch (Exception ae) {
			logger.debug("Exception occurred in send method: " + ae);
		}

		if (response != null) {
			sterlingResponse = response.getEntity(String.class);
		}
		return sterlingResponse;
	}

	public static String getRequestXML(OrderDetails od) {
		String requestXML = "";
		try {

			Document keyRecXML = XMLUtil.createDocument("Receipt");
			Element Receipt = keyRecXML.getDocumentElement();

			Element extnOutElem = XMLUtil.createChild(Receipt, "Extn");
			Element ReceiptLines = XMLUtil.createChild(Receipt, "ReceiptLines");
			Element ReceiptLine = XMLUtil.createChild(ReceiptLines,
					"ReceiptLine");
			Element extnInElem = XMLUtil.createChild(ReceiptLine, "Extn");
			Element ReceiptLineTranQuantity = XMLUtil.createChild(ReceiptLine,
					"ReceiptLineTranQuantity");

			ReceiptLineTranQuantity.setAttribute("Quantity", "" + od.getQty());
			ReceiptLineTranQuantity.setAttribute("TransactionalUOM",
					od.getUom());

			extnInElem.setAttribute("ExtnKeyRecNumber", "88888888");
			extnInElem.setAttribute("ExtnSKUCode", od.getSkuCode());
			extnInElem.setAttribute("ExtnPOLineReference", od.getPoLineRef());
			extnInElem.setAttribute("ExtnHostOrderLineReference",
					od.getLineRef());
			extnInElem.setAttribute("ExtnStoreReceivedLocation",
					od.getReceivingNode());

			ReceiptLine.setAttribute("DispositionCode", "GOOD");
			ReceiptLine.setAttribute("PrimeLineNo", od.getPrimeLineNo());
			ReceiptLine.setAttribute("SubLineNo", od.getSubLineNo());

			extnOutElem
					.setAttribute("ExtnHostOrderReference", od.getOrderRef());
			extnOutElem.setAttribute("ExtnPONumber", od.getPoNumber());

			Receipt.setAttribute("EnterpriseCode", "HDUS");
			Receipt.setAttribute("DocumentType", "0005");
			Receipt.setAttribute("ReceiptNo", "88888888");
			Receipt.setAttribute("ReceivingNode", od.getReceivingNode());
			Receipt.setAttribute("eaisuppress", "Y");

			requestXML = XMLUtil.getXMLString(keyRecXML);
			logger.debug("requestXML: " + requestXML);

		} catch (Exception e) {
			logger.debug("Exception occurred: " + e);
			e.printStackTrace();
		}
		return requestXML;
	}
}
