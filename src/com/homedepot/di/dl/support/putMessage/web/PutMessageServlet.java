package com.homedepot.di.dl.support.putMessage.web;

import java.io.IOException;
import java.io.PrintWriter;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.homedepot.di.dl.support.insertHDEventsAndServiceValidation.service.InsertHDEventsAndUpdateHDPSI;
import com.homedepot.di.dl.support.putMessage.dao.ProcessingException;
import com.homedepot.di.dl.support.putMessage.dao.PutMessage;
import com.homedepot.di.dl.support.util.Constants;

/**
 * Servlet implementation class PutMessageServlet
 */
public class PutMessageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	final static Logger logger = Logger.getLogger(PutMessageServlet.class);
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PutMessageServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		// TODO Auto-generated method stub
		//System.out.println("servlet");
		PutMessage put = new PutMessage();
		int noOfmessages = 0;
		try {
			noOfmessages=put.readMessage();
		} catch (ProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		logger.debug("messages put successfully");
		PrintWriter out = response.getWriter(  ); 
	    response.setContentType("text/html"); 
	    out.println("No of messages put in "+ Constants.rtamQueue_q3 + " : " + noOfmessages);
	    out.close();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
