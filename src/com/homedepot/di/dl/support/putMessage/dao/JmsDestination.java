package com.homedepot.di.dl.support.putMessage.dao;

import javax.jms.JMSException;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueSender;
import javax.jms.QueueSession;
import javax.jms.TextMessage;

import org.apache.log4j.Logger;

import com.homedepot.ta.aa.log4j.ApplLogMessage;
/**
 * Class that Creates a new JMS connection and session. The message is then sent to the destination queue. 
 * If the connection is not established re-tries 10 times before throwing a exception.
 * 
 * @author sandeep_vattikonda@homedepot.com
 *
 */
public class JmsDestination implements Destination {

	private static Logger logger = Logger.getLogger(JmsDestination.class);
	private static final int MAX_ATTEMPTS = 3;
	
	private String qcfName;
	private String qName;

	private QueueConnection qConnection;
	private QueueSession qSession;
	private QueueSender qSender;
	
	private boolean open = false;
	
	public JmsDestination(String qcfName, String qName) {
	
		this.qcfName = qcfName;
		this.qName = qName;
		
	}
	
	@Override
	public void open() throws ProcessingException {

		if ( logger.isDebugEnabled() ) {
			logger.debug(String.format("open() started for qcf name[%s] queue name[%s]", qcfName, qName));
		}

		int attempts = 0;
		
		//Retries 10 times if the connection was not established.
		while ( !open && attempts++ <= MAX_ATTEMPTS ) {

			try {
				// Wait an ever increasing number of seconds on subsequent attempts
				if ( attempts > 1 ) {
					try {
						logger.error(String.format("Will retry to connect queue[%s] in %s seconds", qName, 6*attempts));
						Thread.sleep(6000L*attempts);
					} catch ( InterruptedException interrupt ) {
						logger.warn("Sleep interrupted", interrupt);
					}
				}
				/*
				 * Steps
				 *  (1) Connection to the QueueManager.
				 *  (2) Create a Session using the connection, for sending the message to the Queue.
				 *  (3) Create a sender using the session, which will be used to send the message.
				 */
				qConnection = QCFConnector.getConnection(qcfName);
				qSession = qConnection.createQueueSession(false, javax.jms.Session.AUTO_ACKNOWLEDGE);
				Queue queue = QueueContextCache.getInstance().getQueue(qName);
				qSender = qSession.createSender(queue);
				
				open = true;

			} catch (ProcessingException error ) {

				QCFConnector.close(qConnection);

				// Only log error after all retry attempts have been made
				if ( attempts >= MAX_ATTEMPTS ) {
					logger.error("Error connecting to queue", error);
					throw new ProcessingException(ErrorCodeEnum.JMS_ERROR, "JMS Error connecting to queue: " + error.getMessage(), error,false);
				}
				
			} catch ( Exception error ) {

				QCFConnector.close(qConnection);
				
				throw new ProcessingException(ErrorCodeEnum.JMS_ERROR, "Application Error connecting to queue: " + error.getMessage(), error,false);
				
			}
			
		}
		
		logger.debug(String.format("open() finished for qcf name[%s] queue name[%s]", qcfName, qName));
	}

	/**
	 * Receives a String and converts to TextMessage before sending it to the destination Queue.
	 */
	
	@Override
	public void send(String content) throws ProcessingException {

		if ( logger.isDebugEnabled() ) {
			logger.debug(String.format("send(content) started for qcf name[%s] queue name[%s]", qcfName, qName));
			logger.debug("  content - " + content);
		}

		if ( content != null ) {
			
			if ( ! open ) {
				throw new ProcessingException(ErrorCodeEnum.JMS_ERROR, "JMS Destination is not open - must call open() first",false);
			}
			
			try {

				TextMessage text = qSession.createTextMessage();
				text.setText(content); 
				qSender.send(text);

				if ( logger.isDebugEnabled() ) {
					logger.debug(String.format("Message sent for qcf name[%s] queue name[%s]", qcfName, qName));
				}
				
			} catch ( JMSException jmsError ) {

				logger.error( jmsError);

				// The JMS LinkedException contains the root cause of the error
				// including implementation specific error codes, so it's very important
				// to log this information in order to troubleshoot
				if ( jmsError.getLinkedException() != null ) {
					logger.warn( jmsError.getLinkedException());
				}

				throw new ProcessingException(ErrorCodeEnum.JMS_ERROR, "JMS Error writing message to queue: " + jmsError.getMessage(), jmsError,false);
				
			} catch ( Exception error ) {

				logger.error("Application Error writing message to queue", error);
				
				throw new ProcessingException(ErrorCodeEnum.JMS_ERROR, "Application Error writing message to queue: " + error.getMessage(), error,false);

			}
				
		}
		
		logger.debug(String.format("send(content) finished for qcf name[%s] queue name[%s]", qcfName, qName));
		
	}
	
	@Override
	public void close() throws ProcessingException {
		
		logger.debug(String.format("close() started for qcf name[%s] queue name[%s]", qcfName, qName));
		
		try {
			
			QCFConnector.close(qConnection);
			
		} catch ( Exception error ) {
			
			logger.warn(String.format("Error closing connection close() for qcf name[%s] queue name[%s]", qcfName, qName));
			
		} finally {

			qConnection = null;
			qSession = null;
			qSender = null;
			
			open = false;
			
		}
		
		logger.debug(String.format("close() finished for qcf name[%s] queue name[%s]", qcfName, qName));
		
	}

}