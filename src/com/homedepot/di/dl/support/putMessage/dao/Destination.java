package com.homedepot.di.dl.support.putMessage.dao;



/**
 * Interface will be used as a standard to implement standard Connection Classes 
 * @author SXK8346
 *
 */
public interface Destination {
	
	public void open() throws ProcessingException;
	
	public void send(String content) throws ProcessingException;
	
	public void close() throws ProcessingException;

}
