package com.homedepot.di.dl.support.putMessage.dao;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;

import com.homedepot.di.dl.support.switchToNormal.dao.SwitchTransmissionState;
import com.homedepot.di.dl.support.util.Constants;

import org.apache.log4j.Logger;

public class PutMessage 
{
	final static Logger logger = Logger.getLogger(PutMessage.class);
	public static int readMessage() throws IOException, ProcessingException
	{
		FileInputStream input = new FileInputStream("C:\\test\\queueInput.txt");
		logger.info("reading from file : " + input);
		BufferedReader br = new BufferedReader(new InputStreamReader(input));
		
		String line;
		
		int noOfmsg=0;
		//Read Line By Line
		while ((line = br.readLine()) != null)   
		{

		  //System.out.println (line);
		  putMessage(line);
		  noOfmsg++;
		  
		}

		
		br.close();	
		
		return noOfmsg;
	}

public static void putMessage(String line) throws ProcessingException {
		
		JmsDestination destination = null;
		//PR queue
		/*destination = new JmsDestination("jms/MQGetQCF",
				Constants.rtamQueue);*/
		
		//Q1 queue
		destination = new JmsDestination("jms/MQGetQCF_Q1",
				Constants.rtamQueue_q3);
		
		
		destination.open();
		
		try {
			
			destination.send(line);
			//System.out.println(line);
					
			}
		finally{
		
			destination.close();
		}
		 
		
	}

}
