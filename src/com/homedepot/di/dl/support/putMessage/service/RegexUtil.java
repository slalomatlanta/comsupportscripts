package com.homedepot.di.dl.support.putMessage.service;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import com.homedepot.di.dl.support.switchToNormal.dao.SwitchTransmissionState;

public class RegexUtil {
	final static Logger logger = Logger.getLogger(RegexUtil.class);
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String pattern1 = "SQLException while executing PreparedStatement for Inventory Item Key ";
		String pattern2 = " : ORA-01000: maximum open cursors exceeded";
		String pattern3 = "<Inventory InventoryItemKey=\"";
		String pattern4 = "\" Type";
		String pattern5 =  "/>";
		String text = null;
		if(args.length>0) {
			ArrayList<String> al = new ArrayList<String>();
			try {
				FileInputStream fstream = new FileInputStream(args[0]);
				BufferedReader br = new BufferedReader(new InputStreamReader(fstream));

				//Read File Line By Line
				while ((text = br.readLine()) != null)   {
					Pattern p = Pattern.compile(Pattern.quote(pattern1) + "(.*?)" + Pattern.quote(pattern2));
					Matcher m = p.matcher(text);
					while (m.find()) {
					  al.add(m.group(1));
					}
				}

				//Close the input stream
				br.close();
				fstream.close();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if(al.size()>0){
				ArrayList<String> xmlList = new ArrayList<String>();
				try {
					FileWriter writer = new FileWriter("queueInput.txt",true); 
					FileInputStream fstream = new FileInputStream(args[0]);
					BufferedReader br = new BufferedReader(new InputStreamReader(fstream));

					//Read File Line By Line
					while ((text = br.readLine()) != null)   {
						Pattern p = Pattern.compile(Pattern.quote(pattern3) + "(.*?)" + Pattern.quote(pattern4));
						Matcher m = p.matcher(text);
						while (m.find()) {
							//System.out.println(m.group(1));
							if(al.contains(m.group(1))) {
								//System.out.println(m.group(1));
								Pattern p1 = Pattern.compile(Pattern.quote(pattern3) + "(.*?)" + Pattern.quote(pattern5));
								Matcher m1 = p1.matcher(text);
								while (m1.find()) {
									//System.out.println(m1.group());
								    writer.write(m1.group());
									writer.write("\n");
								}
							}
						}
					}

					//Close the input stream
					br.close();

					fstream.close();
					writer.close();
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			
			}
		
		} else {
			logger.error("Error: Please pass the log file name");
		}
		

	}

}
