package com.homedepot.di.dl.support.ErrorDeltas.dao;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;
import java.util.TreeMap;

import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage.RecipientType;

import org.apache.log4j.Logger;

import com.homedepot.di.dl.support.compareDataReplication.dao.CompareDataReplicationDao;
import com.homedepot.di.dl.support.util.Constants;
import com.homedepot.di.dl.support.util.DBConnectionUtil;

public class DeltaError {

	final static Logger logger = Logger.getLogger(DeltaError.class);
	/**
	 * @param args
	 * @throws Exception
	 */
	public static void deltaReport() throws Exception {
		
		String errorDeltas = getExceptions();
		String table = "<html><head><body>" + errorDeltas
				+ "</html></head></body>";
		sendReport(table);
		
		

	}

	public static void main(String[] args) throws Exception {

		deltaReport();
	}

	/**
	 * Method to get store numbers from MySQL DB and pass it to
	 * getExceptionsFromStores. And with results, fill the table with values and
	 * return it
	 * **/

	public static String getExceptions() throws ClassNotFoundException,
			SQLException {

		
		List<String> storeList = new ArrayList<String>();

		DBConnectionUtil dbConn = new DBConnectionUtil();
		
		Connection mysql = dbConn.getJNDIConnection(Constants.COM_SUPPORT_MYSQL_JNDI);
		
		Statement stmt1 = null;
		stmt1 = mysql.createStatement();
		ResultSet rs = stmt1
				.executeQuery("select store from store where country_cd='US' and store not like '9%';");

		String msgContent = "";
		String table1 = "", table2 = "";
		msgContent = msgContent
				+ "<!DOCTYPE html>"
				+ "<html>"
				+ "<body>"
				+ "<p>Hi All,</p>"
				+ "<p>Please find below the E state delta orders with failure reasons </p>"
				+ "<br/>";

		while (rs.next()) {

			storeList.add(rs.getString(1));

		}
		mysql.close();

		// storeList.add("6861");
		// storeList.add("1809");
		// storeList.add("3911");

		//System.out.println("Got store list :" + storeList.size());
		ArrayList<Map<String, Integer>> mapList = getExceptionsFromStores(storeList);

		Map<String, Integer> map = mapList.get(0);
		Map<String, Integer> yesterdayDeltaInflowMap = mapList.get(1);

		/*System.out.println(map.size());
		System.out.println(yesterdayDeltaInflowMap.size());*/
		if (map.size() > 0) {
			table1 = "<p>Break up of all error deltas</p>"
					+ "<table border=\"1\" cellpadding=\"0\" cellspacing=\"0\" width=\"200px\" style=\"border-collapse:collapse;\">"
					+ "<tr><th>Exception Type</th><th>Count</th></tr>";
			for (Map.Entry<String, Integer> entry : map.entrySet()) {
				// System.out.println(entry.getKey() + ": " + entry.getValue());

				table1 = table1 + "<tr><td>" + entry.getKey() + "</td>";
				table1 = table1 + "<td>" + entry.getValue() + "</td>";
			}
			table1 = table1 + "</table>";
		} else {
			table1 = table1
					+ "<p>There are no error deltas present in stores</p>";
		}

		if (yesterdayDeltaInflowMap.size() > 0) {
			table2 = "<p>Inflow of error deltas for yesterday</p>"
					+ "<table border=\"1\" cellpadding=\"0\" cellspacing=\"0\" width=\"200px\" style=\"border-collapse:collapse;\">"
					+ "<tr><th>Exception Type</th><th>Count</th></tr>";
			for (Map.Entry<String, Integer> entry : yesterdayDeltaInflowMap
					.entrySet()) {
				// System.out.println(entry.getKey() + ": " + entry.getValue());

				table2 = table2 + "<tr><td>" + entry.getKey() + "</td>";
				table2 = table2 + "<td>" + entry.getValue() + "</td>";
			}
			table2 = table2 + "</table>";
		} else {
			table2 = table2 + "<p>There are no error deltas for yesterday</p>";
		}

		msgContent = msgContent + table2 + "<br/>" + table1;
		return msgContent;
	}

	/**
	 * Method to get Exceptions From Stores and count them. returns a map with
	 * exception with count
	 * **/

	public static ArrayList<Map<String, Integer>> getExceptionsFromStores(
			List<String> storeList) {

		

		Calendar cal = Calendar.getInstance();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		String appToDate = dateFormat.format(cal.getTime());
		Date today = cal.getTime();
		cal.add(Calendar.DATE, -1);
		// cal.add(Calendar.YEAR, -1);
		String appFrmDate = dateFormat.format(cal.getTime());
		Date yesterday = cal.getTime();
		/*System.out.println("Today " + today);

		System.out.println("Yesterday " + yesterday);*/
		// System.out.println("appFrmDate :" + appFrmDate);

		ArrayList<Map<String, Integer>> mapList = new ArrayList<Map<String, Integer>>();

		Map<String, Integer> map = new HashMap<String, Integer>();
		Map<String, Integer> yesterdayDeltaInflowMap = new HashMap<String, Integer>();

		boolean isFirst = true;
		boolean isFirstInYesterdayDeltaMap = true;

		for (String storeNo : storeList) {

			
			Connection con = null;
			Statement stmt = null;
			ResultSet rset = null;

			try {
				DBConnectionUtil dbConn = new DBConnectionUtil();
				con = dbConn.getStoreconnection(storeNo);
				stmt = con.createStatement();

				// String queryString =
				// "select e.cust_ord_id, e.trnsltn_excpt_txt, e.last_upd_ts from comt_ord_chg_msg d, comt_ord_trnsltn_err e "
				// +
				// "where d.cust_ord_id=e.cust_ord_id and d.trnsm_stat_ind='E' and e.last_upd_ts > '"
				// + appFrmDate
				// + " 00:00:00.000' and e.last_upd_ts < '"
				// + appToDate + " 00:00:00.000';";
				String queryString = "select e.cust_ord_id, e.trnsltn_excpt_txt, e.last_upd_ts from comt_ord_chg_msg d, comt_ord_trnsltn_err e "
						+ "where d.cust_ord_id=e.cust_ord_id and d.trnsm_stat_ind='E';";
				// System.out.println(queryString);
				rset = stmt.executeQuery(queryString);
				while (rset.next()) {

					String orderNo = rset.getString(1);
					String exception = rset.getString(2);
					Date lastUpdateTs = rset.getTimestamp(3);
					//System.out.println(storeNo +"|"+orderNo+"|"+exception);
					if (orderNo.startsWith("H") || orderNo.startsWith("C")
							|| orderNo.startsWith("W")) {
						if (lastUpdateTs.after(yesterday)
								&& lastUpdateTs.before(today)) {
							if (isFirstInYesterdayDeltaMap) {
								yesterdayDeltaInflowMap.put(exception, 1);
							} else {
								if (yesterdayDeltaInflowMap
										.containsKey(exception)) {
									int count = yesterdayDeltaInflowMap
											.get(exception);
									yesterdayDeltaInflowMap.put(exception,
											count + 1);
								} else {
									yesterdayDeltaInflowMap.put(exception, 1);
								}
							}
							isFirstInYesterdayDeltaMap = false;
						}
						if (isFirst) {
							map.put(exception, 1);

						} else {
							if (map.containsKey(exception)) {
								int count = map.get(exception);
								map.put(exception, count + 1);
							} else {
								map.put(exception, 1);
							}
						}

					}
					isFirst = false;

				}
				con.close();

			} catch (SQLException e) {
				logger.debug(storeNo + " : " + e.getMessage());
			}
		}
		Map<String, Integer> totalExcetions = sortByValues(map);
		Map<String, Integer> yesterdayCount = sortByValues(yesterdayDeltaInflowMap);

		mapList.add(totalExcetions);
		mapList.add(yesterdayCount);
		return mapList;

	}

	public static <String extends Comparable, Integer extends Comparable> Map<String, Integer> sortByValues(
			Map<String, Integer> map) {
		List<Map.Entry<String, Integer>> entries = new LinkedList<Map.Entry<String, Integer>>(
				(Collection<? extends Entry<String, Integer>>) map.entrySet());

		Collections.sort(entries, new Comparator<Map.Entry<String, Integer>>() {

			@Override
			public int compare(Entry<String, Integer> o1,
					Entry<String, Integer> o2) {
				return o2.getValue().compareTo(o1.getValue());
			}
		});

		// LinkedHashMap will keep the keys in the order they are inserted
		// which is currently sorted on natural ordering
		Map<String, Integer> sortedMap = new LinkedHashMap<String, Integer>();

		for (Map.Entry<String, Integer> entry : entries) {
			sortedMap.put(entry.getKey(), entry.getValue());
		}

		return sortedMap;
	}

	public static void sendReport(String msgContent) throws Exception {
		final String user = "HorizonApp@homedepot.com";// change accordingly
		final String password = "xxx";// change accordingly

		String host = "mail1.homedepot.com";
		Properties props = new Properties();
		props.put("mail.host", host);
		Session session = Session.getInstance(props,
				new javax.mail.Authenticator() {
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(user, password);
					}
				});

		try {

			String[] to = {"_2fc77b@homedepot.com"};
			String[] Cc = {};
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress(user));
			DateFormat dateFormat = new SimpleDateFormat(
					"MM/dd/yyyy hh:mm:ss a");
			// get current date time with Date()
			Date today = new Date();
			String date1 = dateFormat.format(today);
			message.setSubject("E state deltas exception types " + date1);
			message.setFrom(new InternetAddress(user));
			 InternetAddress[] addressTo = new InternetAddress[to.length];
			// InternetAddress[] addressCc = new InternetAddress[Cc.length];
			 for (int i = 0; i < to.length; i++) {
			 addressTo[i] = new InternetAddress(to[i]);
			 //System.out.println("To Address " + to[i]);
			 }
			// for (int j = 0; j < Cc.length; j++) {
			// addressCc[j] = new InternetAddress(Cc[j]);
			// }
			message.setRecipients(RecipientType.TO, addressTo);
			// message.setRecipients(RecipientType.CC, addressCc);
			message.setContent(msgContent, "text/html");
			Transport.send(message);
			// for (int i = 0; i < to.length; i++) {
			// addressTo[i] = new InternetAddress(to[i]);
			// System.out.println("Message delivered to--- " + to[i]);
			// }
			// for (int j = 0; j < Cc.length; j++) {
			// addressCc[j] = new InternetAddress(Cc[j]);
			// }

		} catch (MessagingException e) {
			e.printStackTrace();
		}

	}

	
}
