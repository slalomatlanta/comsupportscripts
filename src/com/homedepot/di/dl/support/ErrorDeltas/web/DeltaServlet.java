package com.homedepot.di.dl.support.ErrorDeltas.web;

import java.io.IOException;
import java.util.GregorianCalendar;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.homedepot.di.dl.support.ErrorDeltas.dao.DeltaError;
import com.homedepot.di.dl.support.compareDataReplication.dao.CompareDataReplicationDao;

/**
 * Servlet implementation class DeltaServlet
 */
public class DeltaServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeltaServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    final static Logger logger = Logger.getLogger(DeltaServlet.class);

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		logger.info("Delta Error Report Started : "+ GregorianCalendar.getInstance().getTime());
		DeltaError de = new DeltaError();
		
		
		try {
			de.deltaReport();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.debug("Delta Error Report Exception Caught : "+ GregorianCalendar.getInstance().getTime() + " : " + e);
		}
		logger.info("Delta Error Report Completed : "+ GregorianCalendar.getInstance().getTime());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
