package com.homedepot.di.dl.support.checkofflinestores.service;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage.RecipientType;

import org.apache.log4j.Logger;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;

public class CheckOfflineStoresCount {

	private static final Logger logger = Logger
			.getLogger(CheckOfflineStoresCount.class);
	private static final Logger oLogger = Logger.getLogger("splunkLog");

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		checkOfflineStoresCount();
	}

	public static void checkOfflineStoresCount() {

		String msgContent = "";
		logger.info("checking checkOfflineStoresCount svc : ");
		String numOfLocalStores = "0";
		String listofStores = "";

		try {
			String sURL = "https://checkstore.apps.homedepot.com/stat";
			HttpURLConnection request = null;
			URL url = new URL(sURL);
			request = (HttpURLConnection) url.openConnection();
			request.connect();
			JsonParser jp = new JsonParser(); // from gson
			JsonElement root = jp.parse(new InputStreamReader((InputStream) request.getContent())); 
			JsonObject rootobj = root.getAsJsonObject(); 
			numOfLocalStores = rootobj.get("Local").getAsString(); 
			logger.debug("numOfLocalStores: "+numOfLocalStores);

			String storesURL = "https://checkstore.apps.homedepot.com/offline";
			Client client = Client.create();
			WebResource webResource = client.resource(storesURL);
			listofStores = webResource.get(String.class);
			logger.debug("listofStores: " + listofStores);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		msgContent = "<p>Hello All,</p>"
				+ "Number of Stores which are currently in Local Mode : "
				+ numOfLocalStores
				+ "<br/><br/>"
				+ "<p>Below are the Stores which are in <b>local mode:</b> </p>"
				+ listofStores;
		int count = Integer.parseInt(numOfLocalStores);
		// Below two lines of code is for testing purposes only. No matter the local mode store count is, this will send email. 
		// sendMail(msgContent,true); 
		//	sendMailToPrimary(msgContent, true);


		
		if (count > 15 && count < 50) {
			//send Mail to COM DL
			sendMail(msgContent, false);
		} else if (count > 50) {
			// sendMail to Primary pager
			sendMailToPrimary(msgContent, true);

		}

	}

	public static void sendMailToPrimary(String msgContent, boolean isHipri) {
		String[] mailID =  {"Order_Mgmt_Support@homedepot.com"};
		logger.info("CheckOfflineStoresCount: Preparing to send email to Primary");
		final String user = "horizon@cpliisad.homedepot.com";
		final String password = "xxx";// change accordingly
		String host = "mail1.homedepot.com";
		Properties props = new Properties();
		props.put("mail.host", host);

		String sub = "CRITICAL";
		String pri = "3";

		if (isHipri) {
			sub = "CRITICAL";
			pri = "1";
		}

		Session session = Session.getInstance(props,
				new javax.mail.Authenticator() {
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(user, password);
					}
				});
		try {

			String[] Cc = {};
			// String[] to = { "_2fc77b@homedepot.com" };
			String[] to = mailID;
			MimeMessage message = new MimeMessage(session);
			message.setHeader("X-Priority", pri);
			message.setFrom(new InternetAddress(user));
			DateFormat dateFormat = new SimpleDateFormat(
					"MM/dd/yyyy hh:mm:ss a");
			Date today = new Date();
			String date1 = dateFormat.format(today);
			message.setSubject("CRITICAL : LOCAL MODE STORES COUNT: " + date1);
			// message.setText("This is simple program of sending email using JavaMail API");
			message.setFrom(new InternetAddress(user));
			InternetAddress[] addressTo = new InternetAddress[to.length];
			InternetAddress[] addressCc = new InternetAddress[Cc.length];
			for (int i = 0; i < to.length; i++) {
				addressTo[i] = new InternetAddress(to[i]);
				logger.warn("To Address " + to[i]);
			}
			for (int j = 0; j < Cc.length; j++) {
				addressCc[j] = new InternetAddress(Cc[j]);
				logger.warn("Cc Address " + Cc[j]);
			}
			message.setRecipients(RecipientType.TO, addressTo);
			message.setRecipients(RecipientType.CC, addressCc);
			message.setContent(msgContent, "text/html");
			Transport.send(message);
			for (int i = 0; i < to.length; i++) {
				addressTo[i] = new InternetAddress(to[i]);
				logger.warn("Message delivered to--- " + to[i]);
			}
			for (int j = 0; j < Cc.length; j++) {
				addressCc[j] = new InternetAddress(Cc[j]);
				logger.warn("Message delivered to cc recepients--- " + Cc[j]);
			}

		} catch (MessagingException e) {
			e.printStackTrace();
		}

	}
	
	
	public static void sendMail(String msgContent, boolean isHipri) {

		logger.info("CheckOfflineStoresCount: Preparing to send email");
		final String user = "horizon@cpliisad.homedepot.com";
		final String password = "xxx";// change accordingly
		String host = "mail1.homedepot.com";
		Properties props = new Properties();
		props.put("mail.host", host);

		String sub = "WARN";
		String pri = "3";

		if (isHipri) {
			sub = "CRITICAL";
			pri = "1";
		}

		Session session = Session.getInstance(props,
				new javax.mail.Authenticator() {
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(user, password);
					}
				});
		try {

			String[] Cc = {};
			 String[] to = { "_2fc77b@homedepot.com" };
			//String[] to = { "madhurima_ch@homedepot.com" };
			MimeMessage message = new MimeMessage(session);
			message.setHeader("X-Priority", pri);
			message.setFrom(new InternetAddress(user));
			DateFormat dateFormat = new SimpleDateFormat(
					"MM/dd/yyyy hh:mm:ss a");
			Date today = new Date();
			String date1 = dateFormat.format(today);
			message.setSubject("LOCAL MODE STORES COUNT: " + date1);
			message.setFrom(new InternetAddress(user));
			InternetAddress[] addressTo = new InternetAddress[to.length];
			InternetAddress[] addressCc = new InternetAddress[Cc.length];
			for (int i = 0; i < to.length; i++) {
				addressTo[i] = new InternetAddress(to[i]);
				logger.warn("To Address " + to[i]);
			}
			for (int j = 0; j < Cc.length; j++) {
				addressCc[j] = new InternetAddress(Cc[j]);
				logger.warn("Cc Address " + Cc[j]);
			}
			message.setRecipients(RecipientType.TO, addressTo);
			message.setRecipients(RecipientType.CC, addressCc);
			message.setContent(msgContent, "text/html");
			Transport.send(message);
			for (int i = 0; i < to.length; i++) {
				addressTo[i] = new InternetAddress(to[i]);
				logger.warn("Message delivered to--- " + to[i]);
			}
			for (int j = 0; j < Cc.length; j++) {
				addressCc[j] = new InternetAddress(Cc[j]);
				logger.warn("Message delivered to cc recepients--- " + Cc[j]);
			}

		} catch (MessagingException e) {
			e.printStackTrace();
		}

	}
}
