package com.homedepot.di.dl.support.checkofflinestores.web;



import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.homedepot.di.dl.support.checkofflinestores.service.CheckOfflineStoresCount;

/**
 * Servlet implementation class CheckOfflineStoresServlet
 */
public class CheckOfflineStoresServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = Logger
			.getLogger(CheckOfflineStoresServlet.class);
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CheckOfflineStoresServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		logger.debug("CheckOfflineStoresServlet Program Started at "+java.util.GregorianCalendar.getInstance().getTime());	
		CheckOfflineStoresCount.checkOfflineStoresCount();
		logger.debug("CheckOfflineStoresServlet Program Completed at "+java.util.GregorianCalendar.getInstance().getTime());
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
