package com.homedepot.di.dl.support.agenthealthcheck.main;

import java.sql.Connection;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage.RecipientType;

import org.apache.log4j.Logger;
import org.json.JSONObject;

import com.homedepot.di.dl.support.agenthealthcheck.dao.HealthMonitorDAO;
import com.homedepot.di.dl.support.util.Constants;
import com.homedepot.di.dl.support.util.DBConnectionUtil;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;

public class HealthMonitor {

	private static final Logger logger = Logger.getLogger(HealthMonitor.class);
	private static Client client = null;
	
	public void checkAgentStatus() {

		HealthMonitorDAO daoObject = new HealthMonitorDAO();
		DBConnectionUtil dbUtils = new DBConnectionUtil();
		

		Connection con = null;
		List<String> serverHostList = new ArrayList<String>();
		String msgContent = "";

		try {

			con = dbUtils.getJNDIConnection(Constants.STERLING_SSC_RO_JNDI);

			serverHostList = daoObject.getAgentStatus(con);
			List<String> hostList = daoObject.getHostServerListFromFile();
			if (null != serverHostList 
					&& !hostList.isEmpty()){
				hostList.removeAll(serverHostList);
			}
			if (!hostList.isEmpty()) {
				
				logger.info("sending mail.......");
				 
				msgContent = "<html><body>Hi All,<br/><br/>Please find the Agents that are down <br/><br/> "
						+ "<table border=1><tr><td>Host Name</td><td>Server Name</td></tr>";
				for (String e : serverHostList) {
					String[] hostServerNameArray = e.split("#");
					// logger.debug("host name:" + hostServerNameArray[0]);
					// logger.debug("server name:" + hostServerNameArray[1]);
					msgContent = msgContent + " <tr><td>"
							+ hostServerNameArray[0] + "</td><td>"
							+ hostServerNameArray[1] + "</td></tr>";
				}
				

				String[] to = getPrimaryDetails();

				if (to != null) {
					msgContent = msgContent
							+ "</table><br/><br/>Thanks,<br/>COM Multichannel Support</body></html>";
					sendMail(to, msgContent, true);
				
				} else {
					//to = new String[]{ "Vidhya_remy@homedepot.com" };
					to = new String[]{ "_2fc77b@homedepot.com" };
					msgContent = msgContent
							+ "</table><br/> Note : Couldn't fetch on-call details. Mailing COM_Multichannel_Environment_and_Support" + "<br/><br/>Thanks,<br/>COM Multichannel Support</body></html>";
					sendMail(to, msgContent, false);
				}
			} 

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		}

		finally {
			try {
				dbUtils.closeResources(null, null, con);

			} catch (SQLException e) {
				logger.error(e);
				e.printStackTrace();
			}
		}
	}
	
	public void checkHBAgentStatus() {

		HealthMonitorDAO daoObject = new HealthMonitorDAO();
		DBConnectionUtil dbUtils = new DBConnectionUtil();
		

		Connection con = null;
		List<String> serverHostList = new ArrayList<String>();
		String msgContent = "";

		try {

			con = dbUtils.getJNDIConnection(Constants.STERLING_SSC_RO_JNDI);

			serverHostList = daoObject.getAgentsHBStatus(con);
			if (null != serverHostList 
					&& !serverHostList.isEmpty()) {
				
				logger.info("sending mail.......");
				 
				msgContent = "<html><body>Hi All,<br/><br/>Please find the Agents that are up but no Heartebeat for last 20 mins <br/><br/> "
						+ "<table border=1><tr><td>Host Name</td><td>Server Name</td><td>Last Heartbeat</td></tr>";
				for (String e : serverHostList) {
					String[] hostServerNameArray = e.split("#");
					// logger.debug("host name:" + hostServerNameArray[0]);
					// logger.debug("server name:" + hostServerNameArray[1]);
					// logger.debug("Lastheartbeat:" + hostServerNameArray[2]);
					msgContent = msgContent + " <tr><td>"
							+ hostServerNameArray[0] + "</td><td>"
							+ hostServerNameArray[1] + "</td><td>"
							+ hostServerNameArray[2] + "</td></tr>";
				}
				

				String[] to = getPrimaryDetails();

				if (to != null) {
					msgContent = msgContent
							+ "</table><br/><br/>Thanks,<br/>COM Multichannel Support</body></html>";
					sendMail(to, msgContent, true);
				
				} else {
					//to = new String[]{ "Vidhya_remy@homedepot.com" };
					to = new String[]{ "_2fc77b@homedepot.com" };
					msgContent = msgContent
							+ "</table><br/> Note : Couldn't fetch on-call details. Mailing COM_Multichannel_Environment_and_Support" + "<br/><br/>Thanks,<br/>COM Multichannel Support</body></html>";
					sendMail(to, msgContent, false);
				}
			} else{
				logger.info("All agents up and Heartbeating properly");
			}


		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		}

		finally {
			try {
				dbUtils.closeResources(null, null, con);

			} catch (SQLException e) {
				logger.error(e);
				e.printStackTrace();
			}
		}
	}

	public String[] getPrimaryDetails() {
		String[] mailID = null;
		String response = null;

		String url = "http://cpliis6t.homedepot.com:5001/api/groups/ORDER_MANAGEMENT_SUPPORT";

		Client client = getClient();

		try {
			WebResource webResource = ((com.sun.jersey.api.client.Client) client)
					.resource(url);

			response = webResource.type("text/plain").get(String.class);

			JSONObject xmlJSONObj = new JSONObject(response);

			String primary = xmlJSONObj.getString("Primary").toString();

			int firstIndex = primary.indexOf(";") + 1;
			int lastIndex = primary.length();

			mailID = new String[] { primary.substring(firstIndex, lastIndex) };

		} catch (Exception e) {
			logger.debug("HealthMonitor - Getting primary mail details : "
					+ e.getMessage());
		}

		return mailID;
	}

	public static Client getClient() {
		if (client == null) {
			client = Client.create();
		}
		return client;
	}

	public static void sendMail(String[] to, String msgContent, Boolean isHipri)
			throws Exception {
		final String user = "HorizonApp@homedepot.com";// change accordingly
		final String password = "xxx";// change accordingly
		String sub = "WARN";
		String pri = "3";

		if (isHipri) {
			sub = "CRITICAL";
			pri = "1";
		}

		String host = "mail1.homedepot.com";
		Properties props = new Properties();
		props.put("mail.host", host);
		Session session = Session.getInstance(props,
				new javax.mail.Authenticator() {
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(user, password);
					}
				});

		try {

			MimeMessage message = new MimeMessage(session);
			message.setHeader("X-Priority", pri);
			message.setFrom(new InternetAddress(user));
			DateFormat dateFormat = new SimpleDateFormat(
					"MM/dd/yyyy hh:mm:ss a");
			// get current date time with Date()
			Date today = new Date();
			String date1 = dateFormat.format(today);
			message.setSubject(sub + " : Agent Health Check: " + date1);
			message.setFrom(new InternetAddress(user));
			InternetAddress[] addressTo = new InternetAddress[to.length];
			// InternetAddress[] addressCc = new InternetAddress[Cc.length];
			for (int i = 0; i < to.length; i++) {
				addressTo[i] = new InternetAddress(to[i]);
				logger.info("To Address " + to[i]);
			}
			// for (int j = 0; j < Cc.length; j++) {
			// addressCc[j] = new InternetAddress(Cc[j]);
			// }
			message.setRecipients(RecipientType.TO, addressTo);
			// message.setRecipients(RecipientType.CC, addressCc);
			message.setContent(msgContent, "text/html");
			Transport.send(message);
			// for (int i = 0; i < to.length; i++) {
			// addressTo[i] = new InternetAddress(to[i]);
			// System.out.println("Message delivered to--- " + to[i]);
			// }
			// for (int j = 0; j < Cc.length; j++) {
			// addressCc[j] = new InternetAddress(Cc[j]);
			// }

		} catch (MessagingException e) {
			e.printStackTrace();
		}

	}

}
