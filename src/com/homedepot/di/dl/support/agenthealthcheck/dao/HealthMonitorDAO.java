package com.homedepot.di.dl.support.agenthealthcheck.dao;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.homedepot.di.dl.support.util.Constants;
import com.homedepot.di.dl.support.util.DBConnectionUtil;

public class HealthMonitorDAO {
	
	private static final Logger logger = Logger
			.getLogger(HealthMonitorDAO.class);
	
	DBConnectionUtil dbUtils = new DBConnectionUtil();

	public List<String> getAgentStatus(Connection con)
			throws SQLException {

		PreparedStatement stmt = null;
		ResultSet rs = null;
		List<String> serverHostSet = new ArrayList<String>();
		try {

			stmt = con.prepareStatement(Constants.HEALTH_CHECK_QRY);

			rs = stmt.executeQuery();

			while (rs.next()) {
				
					serverHostSet.add(rs.getString(1).trim()+"#"+rs.getString(3).trim());
				
			}

		} catch (Exception e) {
			logger.error(e.getMessage());
			serverHostSet = null;

		} finally {
			dbUtils.closeResources(stmt, rs, null);
		}

		return serverHostSet;

	}
	
	public List<String> getAgentsHBStatus(Connection con)
			throws SQLException {

		PreparedStatement stmt = null;
		ResultSet rs = null;
		List<String> serverHostSet = new ArrayList<String>();
		try {

			stmt = con.prepareStatement(Constants.HB_HEALTH_CHECK_QRY);

			rs = stmt.executeQuery();

			while (rs.next()) {
				
					serverHostSet.add(rs.getString(1).trim()+"#"+rs.getString(3).trim()+"#"+rs.getString(2).trim());
				
			}

		} catch (Exception e) {
			logger.error(e.getMessage());
			serverHostSet = null;

		} finally {
			dbUtils.closeResources(stmt, rs, null);
		}

		return serverHostSet;

	}
	
	public ArrayList<String> getHostServerListFromFile() {

        logger.info("Getting HostServer List From Text File......");

        ArrayList<String> hostServerList = new ArrayList<String>();
        InputStream stream; 

        try {
        	
        	stream = getClass().getClassLoader().getResourceAsStream("HostServerList.txt"); 

            InputStreamReader ipsr = new InputStreamReader(stream);
            BufferedReader br = new BufferedReader(ipsr);
            
            String line = "";
            while ((line = br.readLine()) != null) {
                hostServerList.add(line);
            }
            
            br.close();
            ipsr.close();
            stream.close();
            
        } catch (Exception e) {
            logger.error(e);
            logger.info("Unable to fetch HostServer list from File");
            e.printStackTrace();
        }

        return hostServerList;
    }
	
}
