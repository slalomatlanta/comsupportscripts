package com.homedepot.di.dl.support.agenthealthcheck.web;

import java.io.IOException;
import java.util.GregorianCalendar;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;

import com.homedepot.di.dl.support.agenthealthcheck.main.HealthMonitor;

/**
 * Servlet implementation class HBAgentHealthCheckServlet
 */
public class HBAgentHealthCheckServlet extends HttpServlet {
	private static final Logger logger = Logger
			.getLogger(AgentHealthCheckServlet.class);
	
	private static final long serialVersionUID = 1L;
	
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public HBAgentHealthCheckServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
HealthMonitor mainObjj = new HealthMonitor();
		
		logger.info("AgentHealthCheckServlet - Started : "+GregorianCalendar.getInstance().getTime());
		mainObjj.checkHBAgentStatus();
		
		logger.info("AgentHealthCheckServlet - Completed : "+GregorianCalendar.getInstance().getTime());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
