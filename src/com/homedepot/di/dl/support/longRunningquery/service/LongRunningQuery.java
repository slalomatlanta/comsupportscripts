package com.homedepot.di.dl.support.longRunningquery.service;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.homedepot.di.dl.support.util.Constants;
import com.homedepot.di.dl.support.util.DBConnectionUtil;
import com.homedepot.di.dl.support.util.DateUtil;
import org.apache.log4j.Logger;



public class LongRunningQuery extends Constants {
	private static final Logger logger = Logger.getLogger(LongRunningQuery.class);
	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		//getLongRunningQuery();
		/*String str1="43.37";
		System.out.println(Double.parseDouble(str1)>=60);
		Date currts= new Date();
		System.out.println("todays date:"+Calendar.getInstance());
		
		
		String sla="12:30";
		 boolean slaMissedOrnot = false;
	        Calendar cal = Calendar.getInstance();
	        System.out.println(sla.substring(0, 2));
	        cal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(sla.substring(0, 2)));
	        System.out.println(sla.substring(3));
	        cal.set(Calendar.MINUTE, Integer.parseInt(sla.substring(3)));
	        cal.set(Calendar.SECOND, 0);
	        cal.set(Calendar.MILLISECOND, 0);
	        if (Calendar.getInstance().after(cal)) {
	            System.out.println("it's SLA missed");
	            slaMissedOrnot = true;
	        } else {
	            System.out.println("it's fine & not SLA missed");
	        }
	        if(slaMissedOrnot){
	        System.out.println(slaMissedOrnot);
	        }*/
	}
	
	
	
	public static String getLongRunningQuery() throws Exception{

		//Gson gson = new Gson();
		String display = "", sterlingTable = "", ODSTable = "";
	    List<longrunningQueryDTO>  odsqueryList=getLongrunningqueryODS();	
	    List<longrunningQueryDTO>  sterqueryList=getLongrunningquerySter();
	    	//System.out.println("Long running Queries::"+longrunDTO.getDuration()+"::"+longrunDTO.getSqlText());
	    	display = "<html><head><body bgcolor=\"#676767\">" + "<font face=\"calibri\">";
	    	display = display + "<h2><center><b><font color=\"White\">Long Running queries</b></center></h2><br/>";
	    	display = display + "<h3><font color=\"White\">DPR77mm Long Running Queries: </h3></p>";
			String tableHdr = "<table border=\"1\" cellpadding=\"1\" cellspacing=\"0\" width=\"200px\" style=\"border-collapse:collapse;\" bgcolor=\"#F2F2F2\">"
					+ "<font face=\"calibri\">"
					+ "<tr bgcolor=\"#FF8C26\">"
					+ "<th>Instance ID</th>"
					+ "<th>SID</th>"
					+ "<th>Serial No</th>"
					+ "<th>Status</th>"
					+ "<th>User Name</th>"
					+ "<th>OS User</th>"
					+ "<th>Module</th>"
					//+ "<th>Machine</th>"
					+ "<th>Sql ID</th>"
					+ "<th>Logon Time</th>"
					+ "<th>Duration(min)</th>"
					+ "<th>SQL Query</th>"
					+ "</tr>";
			for( longrunningQueryDTO longrunDTO: sterqueryList){
				sterlingTable = sterlingTable + "<tr>"
					+ "<td align=\"center\">" + longrunDTO.getInstId() + "</td>"
					+ "<td align=\"center\">" + longrunDTO.getsId() + "</td>"
					+ "<td align=\"center\">"+ longrunDTO.getSerialNo() + "</td>" 
					+ "<td>"+ longrunDTO.getStatus() + "</td>" 
					+ "<td>"+ longrunDTO.getUserName() + "</td>"
					+ "<td>"+ longrunDTO.getOsUser() + "</td>"
					+ "<td>"+ longrunDTO.getModule() + "</td>" 
					//+ "<td>"+ longrunDTO.getMachine() + "</td>" 
					+ "<td>"+ longrunDTO.getSqlId() + "</td>" 
					+ "<td>"+ longrunDTO.getLogonTime() + "</td>" 
					+ "<td><meter value="+ longrunDTO.getDuration() + " max=\"100\">"+ longrunDTO.getDuration() + "</meter>"+ longrunDTO.getDuration() + "</td>" 
					+ "<td>"+ longrunDTO.getSqlText() + "</td>" 
					+ "</tr>";
	    }
			sterlingTable = sterlingTable + "</table><br/><hr/>";
			//display = display + "<h3>DPR78mm Long Running Queries: </h3></p>";
			String tableHdr1 = "<h3><font color=\"White\">DPR78mm Long Running Queries: </h3></p> <table border=\"1\" cellpadding=\"1\" cellspacing=\"0\" width=\"200px\" style=\"border-collapse:collapse;\" bgcolor=\"#F2F2F2\">"
					+ "<font face=\"calibri\">"
					+ "<tr bgcolor=\"#FF8C26\">"
					+ "<th>Instance ID</th>"
					+ "<th>SID</th>"
					+ "<th>Serial No</th>"
					+ "<th>Status</th>"
					+ "<th>User Name</th>"
					+ "<th>OS User</th>"
					+ "<th>Module</th>"
					//+ "<th>Machine</th>"
					+ "<th>Sql ID</th>"
					+ "<th>Logon Time</th>"
					+ "<th>Duration(min)</th>"
					+ "<th>SQL Query</th>"
					+ "</tr>";
		
			
			for( longrunningQueryDTO longrunDTO: odsqueryList){
				ODSTable = ODSTable + "<tr>"
					+ "<td align=\"center\">" + longrunDTO.getInstId() + "</td>"
					+ "<td align=\"center\">" + longrunDTO.getsId() + "</td>"
					+ "<td align=\"center\">"+ longrunDTO.getSerialNo() + "</td>" 
					+ "<td>"+ longrunDTO.getStatus() + "</td>" 
					+ "<td>"+ longrunDTO.getUserName() + "</td>"
					+ "<td>"+ longrunDTO.getOsUser() + "</td>"
					+ "<td>"+ longrunDTO.getModule() + "</td>" 
					//+ "<td>"+ longrunDTO.getMachine() + "</td>" 
					+ "<td>"+ longrunDTO.getSqlId() + "</td>" 
					+ "<td>"+ longrunDTO.getLogonTime() + "</td>" 
					+ "<td><meter value="+ longrunDTO.getDuration() + " max=\"100\">"+ longrunDTO.getDuration() + "</meter>"+ longrunDTO.getDuration() + "</td>" 
					+ "<td>"+ longrunDTO.getSqlText() + "</td>" 
					+ "</tr>";
	    }
		
			
			
			ODSTable = ODSTable + "</table><br/><hr/>";
			display = display+tableHdr+sterlingTable+tableHdr1+ODSTable;
			
			display = display +"</body></head></html>";
			logger.info("HTML:"+display);
		//String json = "hi";//gson.toJson(queryList);		
	    return display;
	}
	
	public static List<longrunningQueryDTO> getLongrunningqueryODS() throws Exception {
		
		List<longrunningQueryDTO> reprocessList = new ArrayList<longrunningQueryDTO>();

		String count = null;
		
		// ODS DB connection
		/*final String driverClass = "oracle.jdbc.driver.OracleDriver";
		final String connectionURL = "jdbc:oracle:thin:@//pprmm77x.homedepot.com:1521/DPR78MM_RW_CODS_01";
		final String uName = "CODSRO01";
		final String uPassword = "juspu6Ew";*/
		
		DBConnectionUtil dbConn = new DBConnectionUtil();
		Connection sscConn = dbConn.getJNDIConnection(ODS_JNDI_APR11);
		
		final String query = "SELECT gs.inst_id,gs.sid,gs.serial#,gs.status,gs.osuser,gs.username,gs.module,gs.machine,gs.sql_id,gs.logon_time,gs.last_call_et as Duration, gq.sql_text"+ 
" FROM GV$SESSION gs, gv$sql gq where"+
" gs.sql_id=gq.sql_id and status ='ACTIVE' and type <>'BACKGROUND' and username <>'SYS'  and last_call_et > 180 order by last_call_et desc";

		logger.info(query);
		Connection con = null;
		try {
			
			logger.debug("Before start connection");
			//Class.forName(driverClass);
			//con = DriverManager.getConnection(connectionURL, uName, uPassword);
			
			Statement stmt = null;
			//stmt = con.createStatement();
			ResultSet result = null;
			//result = stmt.executeQuery(query);
			stmt = sscConn.createStatement();
			logger.info("DBConnection success");
			result = stmt.executeQuery(query);
			
			//DecimalFormat df = new DecimalFormat("0.00##");
			while (result.next()) {
				longrunningQueryDTO lonRunningData =  new longrunningQueryDTO();
				lonRunningData.setInstId(result.getString(1));
				lonRunningData.setsId(result.getString(2));
				lonRunningData.setSerialNo(result.getString(3));
				lonRunningData.setStatus(result.getString(4));
				lonRunningData.setOsUser(result.getString(5));
				lonRunningData.setUserName(result.getString(6));
				lonRunningData.setModule(result.getString(7));
				lonRunningData.setMachine(result.getString(8));
				lonRunningData.setSqlId(result.getString(9));
				lonRunningData.setLogonTime(result.getString(10));
				//System.out.println("Duration"+result.getString(11));
				double number = Double.parseDouble(result.getString(11));
				lonRunningData.setDuration(String.format("%.2f",number/60));
				//System.out.println("Duration before::"+result.getString(11)+" After:::"+lonRunningData.getDuration());
				lonRunningData.setSqlText(result.getString(12));
				reprocessList.add(lonRunningData);				
			}
			stmt.close();
			result.close();
		} catch (Exception e) {
			logger.error(e);
			throw e;
			
		}finally{
			sscConn.close();
		}

		return reprocessList;
	}

public static List<longrunningQueryDTO> getLongrunningquerySter() throws Exception {
		
		List<longrunningQueryDTO> reprocessList = new ArrayList<longrunningQueryDTO>();

		String count = null;
		
		// Ster DB connection
		/*final String driverClass = "oracle.jdbc.driver.OracleDriver";
		final String connectionURL = "jdbc:oracle:thin:@//pprmm77x.homedepot.com:1521/dpr77mm_srw02";
		final String uName = "MMUSR01";
		final String uPassword = "COMS_MMUSR01";
		*/
		DBConnectionUtil dbConn = new DBConnectionUtil();
		Connection sscConn = dbConn.getJNDIConnection(STERLING_ATC_RW1_JNDI);
		
		
		
		final String query = "SELECT gs.inst_id,gs.sid,gs.serial#,gs.status,gs.osuser,gs.username,gs.module,gs.machine,gs.sql_id,gs.logon_time,gs.last_call_et/60 as Duration, gq.sql_text"+ 
" FROM GV$SESSION gs, gv$sql gq where"+
" gs.sql_id=gq.sql_id and status ='ACTIVE' and type <>'BACKGROUND' and username <>'SYS'  and last_call_et > 180 order by last_call_et desc";

		logger.info(query);
		Connection con = null;
		try {
			
			logger.debug("Before start connection");
			/*Class.forName(driverClass);
			con = DriverManager.getConnection(connectionURL, uName, uPassword);
			System.out.println("DBConnection success");*/
			Statement stmt = null;
			//stmt = con.createStatement();
			ResultSet result = null;
			//result = stmt.executeQuery(query);
			//DecimalFormat df = new DecimalFormat("0.00##");
			//String result = df.format(34.4959);
			
			
			stmt = sscConn.createStatement();
			logger.info("DBConnection success");
			result = stmt.executeQuery(query);
			
			
			while (result.next()) {
				longrunningQueryDTO lonRunningData =  new longrunningQueryDTO();
				lonRunningData.setInstId(result.getString(1));
				lonRunningData.setsId(result.getString(2));
				lonRunningData.setSerialNo(result.getString(3));
				lonRunningData.setStatus(result.getString(4));
				lonRunningData.setOsUser(result.getString(5));
				lonRunningData.setUserName(result.getString(6));
				lonRunningData.setModule(result.getString(7));
				lonRunningData.setMachine(result.getString(8));
				lonRunningData.setSqlId(result.getString(9));
				lonRunningData.setLogonTime(result.getString(10));
				//System.out.println("Duration"+result.getString(11));
				double number = Double.parseDouble(result.getString(11));
				lonRunningData.setDuration(String.format("%.2f",number));
				lonRunningData.setSqlText(result.getString(12));
				reprocessList.add(lonRunningData);				
			}
			stmt.close();
			result.close();
		} catch (Exception e) {
			logger.error(e);
			throw e;
			
		}finally{
			
			sscConn.close();
		}

		return reprocessList;
	}
public static List<longrunningQueryDTO> getLongRunningQueryMailThreshSter() throws Exception{
	List finList=new ArrayList();
	List<longrunningQueryDTO> finSterList=new ArrayList<longrunningQueryDTO>();
	List<longrunningQueryDTO> finODSList=new ArrayList<longrunningQueryDTO>();
	  //List<longrunningQueryDTO>  odsqueryList=getLongrunningqueryODS();	
	    List<longrunningQueryDTO>  sterqueryList=getLongrunningquerySter();
	   //module- SQL Developer,INT&HDDelProcessAgent,didlCOMRetrieveDemand,didlSvsComServices
	    for( longrunningQueryDTO longrunDTO: sterqueryList){
	    	logger.info("77 SQL DEv Check :"+longrunDTO.getModule()+"::condition::"+longrunDTO.getModule().equals("SQL Developer")+"::: Duration check::"+(Double.parseDouble(longrunDTO.getDuration()))+"::"+(Double.parseDouble(longrunDTO.getDuration())>=15));
	    	logger.info("77 HDDelProcessAgent :"+longrunDTO.getModule()+"::condition::"+longrunDTO.getModule().equals("INT&HDDelProcessAgent")+"::: Duration check::"+(Double.parseDouble(longrunDTO.getDuration()))+"::"+(Double.parseDouble(longrunDTO.getDuration())>=60));
	    	logger.info("77 Purge :"+longrunDTO.getModule()+"::condition::"+longrunDTO.getModule().contains("Purge")+"::: Duration check::"+(Double.parseDouble(longrunDTO.getDuration()))+"::"+(Double.parseDouble(longrunDTO.getDuration())>=120));
	    	logger.info("77 others ::: Duration check::"+longrunDTO.getModule()+"::condition::"+(Double.parseDouble(longrunDTO.getDuration()))+"::"+(Double.parseDouble(longrunDTO.getDuration())>=15));
	    	
	    	logger.info("Others if condition"+(!(longrunDTO.getModule().equals("INT&HDDelProcessAgent"))||(longrunDTO.getModule().equals("SQL Developer"))||(longrunDTO.getModule().contains("Purge"))));
	    	
	    	if((longrunDTO.getModule().equals("SQL Developer"))&&(Double.parseDouble(longrunDTO.getDuration())>=15)){
	    		finSterList.add(longrunDTO);
	    	}
	    	else if((longrunDTO.getModule().equals("INT&HDDelProcessAgent"))&&(Double.parseDouble(longrunDTO.getDuration())>=60)){
	    		finSterList.add(longrunDTO);
	    	}
	    	else if((longrunDTO.getModule().contains("Purge"))&&(Double.parseDouble(longrunDTO.getDuration())>120)){
	    		
	    		
	   	        if (DateUtil.checkSlaMissedOrNot()) {
	   	         
	    		finSterList.add(longrunDTO);
	    	}
	    	}
	    	else if((!((longrunDTO.getModule().equals("INT&HDDelProcessAgent"))||(longrunDTO.getModule().equals("SQL Developer"))||(longrunDTO.getModule().contains("Purge"))))&&(Double.parseDouble(longrunDTO.getDuration())>10)){
	    		finSterList.add(longrunDTO);
	    	}
	    }
	    finList.add(finSterList);
	    
	   /* for( longrunningQueryDTO longrunnDTO: odsqueryList){
	    	
	    	System.out.println("78 SQL DEv Check :"+longrunnDTO.getModule()+"::condition::"+longrunnDTO.getModule().equals("SQL Developer")+"::: Duration check::"+(Double.parseDouble(longrunnDTO.getDuration()))+"::"+(Double.parseDouble(longrunnDTO.getDuration())>=15));
	    	System.out.println("78 didlCOMRetrieveDemand :"+longrunnDTO.getModule()+"::condition::"+longrunnDTO.getModule().equals("didlCOMRetrieveDemand")+"::: Duration check::"+(Double.parseDouble(longrunnDTO.getDuration()))+"::"+(Double.parseDouble(longrunnDTO.getDuration())>=15));
	    	System.out.println("78 didlSvsComServices :"+longrunnDTO.getModule()+"::condition::"+longrunnDTO.getModule().contains("didlSvsComServices")+"::: Duration check::"+(Double.parseDouble(longrunnDTO.getDuration()))+"::"+(Double.parseDouble(longrunnDTO.getDuration())>=15));
	    	System.out.println("78 others ::: Duration check::"+longrunnDTO.getModule()+"::condition::"+(Double.parseDouble(longrunnDTO.getDuration()))+"::"+(Double.parseDouble(longrunnDTO.getDuration())>=15));
	    	
	    	
	    	
	    	if((longrunnDTO.getModule().equals("SQL Developer"))&&(Double.parseDouble(longrunnDTO.getDuration())>15)){
	    		finODSList.add(longrunnDTO);
	    	}else if((longrunnDTO.getModule().equals("didlCOMRetrieveDemand"))&&(Double.parseDouble(longrunnDTO.getDuration())>2)){
	    		finODSList.add(longrunnDTO);
	    	}else if((longrunnDTO.getModule().equals("didlSvsComServices"))&&(Double.parseDouble(longrunnDTO.getDuration())>30)){
	    		finODSList.add(longrunnDTO);
	    	}else if((Double.parseDouble(longrunnDTO.getDuration())>1)){
	    		finSterList.add(longrunnDTO);
	    	}
	    }
	    finList.add(finODSList);
	    */
	    
	return finSterList;
	
}

public static List<longrunningQueryDTO> getLongRunningQueryMailThreshODS() throws Exception{
	List finList=new ArrayList();
	List<longrunningQueryDTO> finSterList=new ArrayList<longrunningQueryDTO>();
	List<longrunningQueryDTO> finODSList=new ArrayList<longrunningQueryDTO>();
	  List<longrunningQueryDTO>  odsqueryList=getLongrunningqueryODS();	
	   // List<longrunningQueryDTO>  sterqueryList=getLongrunningquerySter();
	   //module- SQL Developer,INT&HDDelProcessAgent,didlCOMRetrieveDemand,didlSvsComServices
	   /* for( longrunningQueryDTO longrunDTO: sterqueryList){
	    	System.out.println("77 SQL DEv Check :"+longrunDTO.getModule()+"::condition::"+longrunDTO.getModule().equals("SQL Developer")+"::: Duration check::"+(Double.parseDouble(longrunDTO.getDuration()))+"::"+(Double.parseDouble(longrunDTO.getDuration())>=15));
	    	System.out.println("77 HDDelProcessAgent :"+longrunDTO.getModule()+"::condition::"+longrunDTO.getModule().equals("INT&HDDelProcessAgent")+"::: Duration check::"+(Double.parseDouble(longrunDTO.getDuration()))+"::"+(Double.parseDouble(longrunDTO.getDuration())>=15));
	    	System.out.println("77 Purge :"+longrunDTO.getModule()+"::condition::"+longrunDTO.getModule().contains("Purge")+"::: Duration check::"+(Double.parseDouble(longrunDTO.getDuration()))+"::"+(Double.parseDouble(longrunDTO.getDuration())>=15));
	    	System.out.println("77 others ::: Duration check::"+longrunDTO.getModule()+"::condition::"+(Double.parseDouble(longrunDTO.getDuration()))+"::"+(Double.parseDouble(longrunDTO.getDuration())>=15));
	    	
	    	
	    	
	    	if((longrunDTO.getModule().equals("SQL Developer"))&&(Double.parseDouble(longrunDTO.getDuration())>=15)){
	    		finSterList.add(longrunDTO);
	    	}
	    	else if((longrunDTO.getModule().equals("INT&HDDelProcessAgent"))&&(Double.parseDouble(longrunDTO.getDuration())>=60)){
	    		finSterList.add(longrunDTO);
	    	}
	    	else if((longrunDTO.getModule().contains("Purge"))&&(Double.parseDouble(longrunDTO.getDuration())>120)){
	    		finSterList.add(longrunDTO);
	    	}
	    	else if((Double.parseDouble(longrunDTO.getDuration())>1)){
	    		finSterList.add(longrunDTO);
	    	}
	    }
	    finList.add(finSterList);*/
	    
	    for( longrunningQueryDTO longrunnDTO: odsqueryList){
	    	
	    	logger.info("78 SQL DEv Check :"+longrunnDTO.getModule()+"::condition::"+longrunnDTO.getModule().equals("SQL Developer")+"::: Duration check::"+(Double.parseDouble(longrunnDTO.getDuration()))+"::"+(Double.parseDouble(longrunnDTO.getDuration())>=15));
	    	logger.info("78 didlCOMRetrieveDemand :"+longrunnDTO.getModule()+"::condition::"+longrunnDTO.getModule().equals("didlCOMRetrieveDemand")+"::: Duration check::"+(Double.parseDouble(longrunnDTO.getDuration()))+"::"+(Double.parseDouble(longrunnDTO.getDuration())>=15));
	    	logger.info("78 didlSvsComServices :"+longrunnDTO.getModule()+"::condition::"+longrunnDTO.getModule().contains("didlSvsComServices")+"::: Duration check::"+(Double.parseDouble(longrunnDTO.getDuration()))+"::"+(Double.parseDouble(longrunnDTO.getDuration())>=15));
	    	logger.info("78 others ::: Duration check::"+longrunnDTO.getModule()+"::condition::"+(Double.parseDouble(longrunnDTO.getDuration()))+"::"+(Double.parseDouble(longrunnDTO.getDuration())>=15));
	    	logger.info("others:if condition:: "+(!(longrunnDTO.getModule().equals("didlCOMRetrieveDemand"))||(longrunnDTO.getModule().equals("SQL Developer"))||(longrunnDTO.getModule().contains("didlSvsComServices"))));
	    	
	    	
	    	if((longrunnDTO.getModule().equals("SQL Developer"))&&(Double.parseDouble(longrunnDTO.getDuration())>15)){
	    		finODSList.add(longrunnDTO);
	    	}else if((longrunnDTO.getModule().equals("didlCOMRetrieveDemand"))&&(Double.parseDouble(longrunnDTO.getDuration())>30)){
	    		finODSList.add(longrunnDTO);
	    	}else if((longrunnDTO.getModule().equals("didlSvsComServices"))&&(Double.parseDouble(longrunnDTO.getDuration())>30)){
	    		finODSList.add(longrunnDTO);
	    	}else if((!((longrunnDTO.getModule().equals("didlCOMRetrieveDemand"))||(longrunnDTO.getModule().equals("SQL Developer"))||(longrunnDTO.getModule().contains("didlSvsComServices"))))&&(Double.parseDouble(longrunnDTO.getDuration())>10)){
	    		finODSList.add(longrunnDTO);
	    	}
	    }
	    finList.add(finODSList);
	    
	    
	return finODSList;
	
}


public static String getLongRunningQueryMail() throws Exception{

	//Gson gson = new Gson();
	String display = "", sterlingTable = "", ODSTable = "";
	List finList= new ArrayList();
	/*finList=getLongRunningQueryMailThresh();
    List<longrunningQueryDTO>  odsqueryList=(List<longrunningQueryDTO>) finList.get(1);	
    List<longrunningQueryDTO>  sterqueryList=(List<longrunningQueryDTO>) finList.get(0);*/
	
	List<longrunningQueryDTO>  odsqueryList=getLongRunningQueryMailThreshODS();	
    List<longrunningQueryDTO>  sterqueryList=getLongRunningQueryMailThreshSter();
	
	
	
	if((odsqueryList.size()>0)||(sterqueryList.size()>0)){
    	//System.out.println("Long running Queries::"+longrunDTO.getDuration()+"::"+longrunDTO.getSqlText());
    	display = "<html><head><body>" + "<font face=\"calibri\">";
    	//display = display + "<h2><center><b><font color=\"White\">Long Running queries</b></center></h2><br/>";
    	display = display + "<h3><font>DPR77mm Long Running Queries: </h3></p>";
		String tableHdr = "<table border=\"1\" cellpadding=\"1\" cellspacing=\"0\" width=\"200px\" style=\"border-collapse:collapse;\" >"
				+ "<font face=\"calibri\">"
				+ "<tr>"
				+ "<th>Instance ID</th>"
				+ "<th>SID</th>"
				+ "<th>Serial No</th>"
				+ "<th>Status</th>"
				+ "<th>User Name</th>"
				+ "<th>OS User</th>"
				+ "<th>Module</th>"
				//+ "<th>Machine</th>"
				+ "<th>Sql ID</th>"
				+ "<th>Logon Time</th>"
				+ "<th>Duration(min)</th>"
				+ "<th>SQL Query</th>"
				+ "</tr>";
		for( longrunningQueryDTO longrunDTO: sterqueryList){
			sterlingTable = sterlingTable + "<tr>"
				+ "<td align=\"center\">" + longrunDTO.getInstId() + "</td>"
				+ "<td align=\"center\">" + longrunDTO.getsId() + "</td>"
				+ "<td align=\"center\">"+ longrunDTO.getSerialNo() + "</td>" 
				+ "<td>"+ longrunDTO.getStatus() + "</td>" 
				+ "<td>"+ longrunDTO.getUserName() + "</td>"
				+ "<td>"+ longrunDTO.getOsUser() + "</td>"
				+ "<td>"+ longrunDTO.getModule() + "</td>" 
				//+ "<td>"+ longrunDTO.getMachine() + "</td>" 
				+ "<td>"+ longrunDTO.getSqlId() + "</td>" 
				+ "<td>"+ longrunDTO.getLogonTime() + "</td>" 
				+ "<td><meter value="+ longrunDTO.getDuration() + " max=\"100\">"+ longrunDTO.getDuration() + "</meter> </td>" 
				+ "<td>"+ longrunDTO.getSqlText() + "</td>" 
				+ "</tr>";
			logger.info("Duration::main:77:"+longrunDTO.getDuration());
    }
		sterlingTable = sterlingTable + "</table><br/><hr/>";
		//display = display + "<h3>DPR78mm Long Running Queries: </h3></p>";
		String tableHdr1 = "<h3><font>DPR78mm Long Running Queries: </h3></p> <table border=\"1\" cellpadding=\"1\" cellspacing=\"0\" width=\"200px\" style=\"border-collapse:collapse;\">"
				+ "<font face=\"calibri\">"
				+ "<tr>"
				+ "<th>Instance ID</th>"
				+ "<th>SID</th>"
				+ "<th>Serial No</th>"
				+ "<th>Status</th>"
				+ "<th>User Name</th>"
				+ "<th>OS User</th>"
				+ "<th>Module</th>"
				//+ "<th>Machine</th>"
				+ "<th>Sql ID</th>"
				+ "<th>Logon Time</th>"
				+ "<th>Duration(min)</th>"
				+ "<th>SQL Query</th>"
				+ "</tr>";
	
		
		for( longrunningQueryDTO longrunDTO: odsqueryList){
			ODSTable = ODSTable + "<tr>"
				+ "<td align=\"center\">" + longrunDTO.getInstId() + "</td>"
				+ "<td align=\"center\">" + longrunDTO.getsId() + "</td>"
				+ "<td align=\"center\">"+ longrunDTO.getSerialNo() + "</td>" 
				+ "<td>"+ longrunDTO.getStatus() + "</td>" 
				+ "<td>"+ longrunDTO.getUserName() + "</td>"
				+ "<td>"+ longrunDTO.getOsUser() + "</td>"
				+ "<td>"+ longrunDTO.getModule() + "</td>" 
				//+ "<td>"+ longrunDTO.getMachine() + "</td>" 
				+ "<td>"+ longrunDTO.getSqlId() + "</td>" 
				+ "<td>"+ longrunDTO.getLogonTime() + "</td>" 
				+ "<td><meter value="+ longrunDTO.getDuration() + " max=\"100\">"+ longrunDTO.getDuration() + "</meter></td>" 
				+ "<td>"+ longrunDTO.getSqlText() + "</td>" 
				+ "</tr>";
			logger.info("Duration::main:78:"+longrunDTO.getDuration());
    }
	
		
		
		ODSTable = ODSTable + "</table><br/><hr/>";
		display = display+tableHdr+sterlingTable+tableHdr1+ODSTable;
		
		display = display +"</body></head></html>";
		logger.info("HTML:"+display);
	}else {
		display="NoHiPri";
	}
	//String json = "hi";//gson.toJson(queryList);		
    return display;
}

}
