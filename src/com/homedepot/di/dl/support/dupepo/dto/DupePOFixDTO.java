package com.homedepot.di.dl.support.dupepo.dto;

public class DupePOFixDTO {

	String poCancelErrorTxnId;
	String poCreateErrorTxnId;
	String extnHostOrderRef;
	String extnHostOrderLineRef;
	String rawMessage;
	String messageToBeProcessed;
	String existingPO;
	String updatedPO;
	String poOrderHeaderKey;
	String isPOCreatePresent;
	String isPOCancelPresent;
	String messagePutToPOCreateQueue = "N";
	String messagePutToChangeOrderQueue = "N";
	boolean IsDirtyNodeTakenCare = false;
	boolean IsreservarionExpired = false;
	
	public String getPoOrderHeaderKey() {
		return poOrderHeaderKey;
	}
	public void setPoOrderHeaderKey(String poOrderHeaderKey) {
		this.poOrderHeaderKey = poOrderHeaderKey;
	}
	public boolean isIsreservarionExpired() {
		return IsreservarionExpired;
	}
	public void setIsreservarionExpired(boolean isreservarionExpired) {
		IsreservarionExpired = isreservarionExpired;
	}
	public boolean isIsDirtyNodeTakenCare() {
		return IsDirtyNodeTakenCare;
	}
	public void setIsDirtyNodeTakenCare(boolean isDirtyNodeTakenCare) {
		IsDirtyNodeTakenCare = isDirtyNodeTakenCare;
	}
	public String getPoCancelErrorTxnId() {
		return poCancelErrorTxnId;
	}
	public void setPoCancelErrorTxnId(String poCancelErrorTxnId) {
		this.poCancelErrorTxnId = poCancelErrorTxnId;
	}
	public String getPoCreateErrorTxnId() {
		return poCreateErrorTxnId;
	}
	public void setPoCreateErrorTxnId(String poCreateErrorTxnId) {
		this.poCreateErrorTxnId = poCreateErrorTxnId;
	}
	public String getExtnHostOrderRef() {
		return extnHostOrderRef;
	}
	public void setExtnHostOrderRef(String extnHostOrderRef) {
		this.extnHostOrderRef = extnHostOrderRef;
	}
	public String getExtnHostOrderLineRef() {
		return extnHostOrderLineRef;
	}
	public void setExtnHostOrderLineRef(String extnHostOrderLineRef) {
		this.extnHostOrderLineRef = extnHostOrderLineRef;
	}
	public String getRawMessage() {
		return rawMessage;
	}
	public void setRawMessage(String rawMessage) {
		this.rawMessage = rawMessage;
	}
	public String getMessageToBeProcessed() {
		return messageToBeProcessed;
	}
	public void setMessageToBeProcessed(String messageToBeProcessed) {
		this.messageToBeProcessed = messageToBeProcessed;
	}
	public String getExistingPO() {
		return existingPO;
	}
	public void setExistingPO(String existingPO) {
		this.existingPO = existingPO;
	}
	public String getUpdatedPO() {
		return updatedPO;
	}
	public void setUpdatedPO(String updatedPO) {
		this.updatedPO = updatedPO;
	}
	public String getIsPOCreatePresent() {
		return isPOCreatePresent;
	}
	public void setIsPOCreatePresent(String isPOCreatePresent) {
		this.isPOCreatePresent = isPOCreatePresent;
	}
	public String getIsPOCancelPresent() {
		return isPOCancelPresent;
	}
	public void setIsPOCancelPresent(String isPOCancelPresent) {
		this.isPOCancelPresent = isPOCancelPresent;
	}
	public String getMessagePutToPOCreateQueue() {
		return messagePutToPOCreateQueue;
	}
	public void setMessagePutToPOCreateQueue(String messagePutToPOCreateQueue) {
		this.messagePutToPOCreateQueue = messagePutToPOCreateQueue;
	}
	public String getMessagePutToChangeOrderQueue() {
		return messagePutToChangeOrderQueue;
	}
	public void setMessagePutToChangeOrderQueue(String messagePutToChangeOrderQueue) {
		this.messagePutToChangeOrderQueue = messagePutToChangeOrderQueue;
	}
	
	

		
}
