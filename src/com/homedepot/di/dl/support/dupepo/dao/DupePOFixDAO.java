package com.homedepot.di.dl.support.dupepo.dao;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import au.com.bytecode.opencsv.CSVReader;

import com.homedepot.di.dl.support.util.Constants;
import com.homedepot.di.dl.support.dupepo.dto.DupePOFixDTO;
import com.homedepot.di.dl.support.insertBossDummyPayment.service.JmsDestination;
import com.homedepot.di.dl.support.insertBossDummyPayment.service.ProcessingException;
import com.homedepot.di.dl.support.util.DBConnectionUtil;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class DupePOFixDAO {

	private static final Logger logger = Logger.getLogger(DupePOFixDAO.class);

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String inputFileLocation = "C:\\test\\JDADuplicatePOImpactedOrdersInput.csv";
		String outputFileLocation = "C:\\test\\JDADuplicatePOImpactedOrdersOutput.csv";
		List<DupePOFixDTO> cancelPOImpactedList = new ArrayList<DupePOFixDTO>();
		List<DupePOFixDTO> finalCancelPOImpactedList = new ArrayList<DupePOFixDTO>();

		cancelPOImpactedList = readImpactedPOCancelsFromCSV(inputFileLocation);
		finalCancelPOImpactedList = startLoopingThrough(cancelPOImpactedList);

		writeToCSV(outputFileLocation, finalCancelPOImpactedList);
	}

	public static String startProcessing() {
		String msg = "Failure";
		String inputFileLocation = "C:\\test\\JDADuplicatePOImpactedOrdersInput.csv";
		String outputFileLocation = "C:\\test\\JDADuplicatePOImpactedOrdersOutput.csv";
		List<DupePOFixDTO> cancelPOImpactedList = new ArrayList<DupePOFixDTO>();
		List<DupePOFixDTO> finalCancelPOImpactedList = new ArrayList<DupePOFixDTO>();

		cancelPOImpactedList = readImpactedPOCancelsFromCSV(inputFileLocation);
		finalCancelPOImpactedList = startLoopingThrough(cancelPOImpactedList);

		writeToCSV(outputFileLocation, finalCancelPOImpactedList);

		msg = "Success";

		return msg;
	}

	public static List<DupePOFixDTO> readImpactedPOCancelsFromCSV(
			String fileLocation) {

		List<DupePOFixDTO> cancelPOImpactedList = new ArrayList<DupePOFixDTO>();

		String[] row = null;
		CSVReader csvReader = null;
		boolean headerRow = true;
		try {
			csvReader = new CSVReader(new FileReader(fileLocation));
			DupePOFixDTO dto = null;
			while ((row = csvReader.readNext()) != null) {
				if (headerRow) {
					headerRow = false;
				} else {
					dto = new DupePOFixDTO();
					dto.setExtnHostOrderRef(row[0].trim());
					dto.setExtnHostOrderLineRef(row[1].trim());
					dto.setIsPOCancelPresent(row[2].trim());
					dto.setIsPOCreatePresent(row[3].trim());
					dto.setPoCancelErrorTxnId(row[4].trim());
					dto.setPoCreateErrorTxnId(row[5].trim());
					cancelPOImpactedList.add(dto);
				}
			}
			csvReader.close();
		} catch (Exception e) {
			e.getStackTrace();
			// logger.debug("Exception Occured " + e.getStackTrace());
		}

		logger.info(cancelPOImpactedList.size()
				+ " records found from input file - " + fileLocation);

		return cancelPOImpactedList;
	}

	public static List<DupePOFixDTO> startLoopingThrough(
			List<DupePOFixDTO> cancelPOImpactedList) {

		List<DupePOFixDTO> finalCancelPOImpactedList = new ArrayList<DupePOFixDTO>();

		Map<String, DupePOFixDTO> poCancelSuccessMap = new HashMap<String, DupePOFixDTO>();
		Map<String, DupePOFixDTO> poCreateSuccessMap = new HashMap<String, DupePOFixDTO>();

		try {

			String changeOrderQueue = "jms/DI.DL.STERLING.CHANGEORDER.STH_put";
			String sthPOCreateQueue = "jms/DI.DL.STERLING.RCVCREATE.PO.STH_put";

			for (DupePOFixDTO dto : cancelPOImpactedList) {

				// Start processing only if we find the PO cancel message
				logger.info("Started processing for order - line : "
						+ dto.getExtnHostOrderRef() + " - "
						+ dto.getExtnHostOrderLineRef());
				try {
					// check to see if po cancel is present and po cancel
					// errortxnid is not processed.
					if (dto.getIsPOCancelPresent().equalsIgnoreCase("Y")
							&& !poCancelSuccessMap.containsKey(dto
									.getPoCancelErrorTxnId())) {
						logger.info("PO Cancel present for order - line : "
								+ dto.getExtnHostOrderRef() + " - "
								+ dto.getExtnHostOrderLineRef());
						String message = getMessageFromSterling(dto
								.getPoCancelErrorTxnId());
						String existingPO = getExtnPONumber(message);
						String updatedPO = getDummyExtnPONumber(existingPO,
								dto.getExtnHostOrderRef(),
								dto.getExtnHostOrderLineRef());
						dto.setRawMessage(message);
						dto.setExistingPO(existingPO);
						dto.setUpdatedPO(updatedPO);

						String messageToBeProcessed = message.replace(
								existingPO, updatedPO);
						dto.setMessageToBeProcessed(messageToBeProcessed);

						logger.info("MessageToBeProcessed for PO cancellation - "
								+ messageToBeProcessed);

						String poOrderHeaderKey = getPOOrderHeaderKey(
								dto.getExtnHostOrderRef(),
								dto.getExtnHostOrderLineRef(), existingPO);
						
						dto.setPoOrderHeaderKey(poOrderHeaderKey);
						if(poOrderHeaderKey.length() > 0){
						int rowsUpdated = updateDummyPONumberInSterling(
								poOrderHeaderKey, updatedPO);
						
						if (rowsUpdated > 0) {
							String messagePutToChangeOrderQueue = putMessageInQueue(
									messageToBeProcessed, changeOrderQueue);
							dto.setMessagePutToChangeOrderQueue(messagePutToChangeOrderQueue);

							if (messagePutToChangeOrderQueue
									.equalsIgnoreCase("Y")) {
								poCancelSuccessMap
										.put(dto.getPoCancelErrorTxnId(), dto);
							}

						}
					}

						// String inputAPI = null;
						// inputAPI = getInputForManageInventoryNodeControlAPI(
						// dto.getExtnHostOrderRef(),
						// dto.getExtnHostOrderLineRef());
						//
						// boolean isDirtyNodeTakenCare = processXML(inputAPI);
						// dto.setIsDirtyNodeTakenCare(isDirtyNodeTakenCare);
						//
						// If PO create is present, Expire reservation and
						// process
						// PO create
						if (dto.getIsPOCreatePresent().equalsIgnoreCase("Y")
								&& !poCreateSuccessMap.containsKey(dto
										.getPoCreateErrorTxnId())) {
							// logger.info("PO Create present for order - line : "
							// + dto.getExtnHostOrderRef() + " - "
							// + dto.getExtnHostOrderLineRef());
							// int rowsUpdatedForReservationExpiration =
							// expireReservationInSterling(
							// dto.getExtnHostOrderRef(),
							// dto.getExtnHostOrderLineRef());
							//
							// if (rowsUpdatedForReservationExpiration > 0) {
							// dto.setIsreservarionExpired(true);
							// }

							// String poCreateMessage =
							// getMessageFromSterling(dto
							// .getPoCreateErrorTxnId());
							// String messagePutToSTHPOCreateQueue = "N";
							// // messagePutToSTHPOCreateQueue =
							// putMessageInQueue(
							// // poCreateMessage, sthPOCreateQueue);
							// dto.setMessagePutToPOCreateQueue(messagePutToSTHPOCreateQueue);

							// if (messagePutToSTHPOCreateQueue
							// .equalsIgnoreCase("Y")) {
							// poCreateSuccessMap
							// .put(dto.getPoCreateErrorTxnId(),
							// "ErrorTxnId - "
							// + dto.getPoCreateErrorTxnId()
							// + " got processed on behalf of "
							// + dto.getExtnHostOrderRef()
							// + " - "
							// + dto.getExtnHostOrderLineRef());
							// }

						}

						else {
							if (poCreateSuccessMap.containsKey(dto
									.getPoCreateErrorTxnId())) {
								DupePOFixDTO fetch = new DupePOFixDTO();
								fetch = poCreateSuccessMap.get(dto
										.getPoCreateErrorTxnId());
								logger.info(dto
										.getPoCreateErrorTxnId()+ " got processed on behalf of " +fetch.getExtnHostOrderRef()+" - "+fetch.getExtnHostOrderLineRef());
								dto.setPoOrderHeaderKey(fetch.getPoOrderHeaderKey());
								dto.setExistingPO(fetch.getExistingPO());
								dto.setUpdatedPO(fetch.getUpdatedPO());
								dto.setMessagePutToChangeOrderQueue(fetch.getMessagePutToChangeOrderQueue());
								dto.setMessagePutToPOCreateQueue(fetch.getMessagePutToPOCreateQueue());
								
							}
							if (dto.getIsPOCreatePresent()
									.equalsIgnoreCase("N")) {
								logger.info("PO Create not present for "
										+ dto.getExtnHostOrderRef() + " - "
										+ dto.getExtnHostOrderLineRef());
							}

						}

						finalCancelPOImpactedList.add(dto);

					} else {
						if (poCancelSuccessMap.containsKey(dto
								.getPoCancelErrorTxnId())) {
							
							
//							logger.info(poCancelSuccessMap.get(dto
//									.getPoCancelErrorTxnId()));
							
							DupePOFixDTO fetch = new DupePOFixDTO();
							fetch = poCancelSuccessMap.get(dto
									.getPoCancelErrorTxnId());
							logger.info(dto
									.getPoCancelErrorTxnId()+ " got processed on behalf of " +fetch.getExtnHostOrderRef()+" - "+fetch.getExtnHostOrderLineRef());
							dto.setPoOrderHeaderKey(fetch.getPoOrderHeaderKey());
							dto.setExistingPO(fetch.getExistingPO());
							dto.setUpdatedPO(fetch.getUpdatedPO());
							dto.setMessagePutToChangeOrderQueue(fetch.getMessagePutToChangeOrderQueue());
							dto.setMessagePutToPOCreateQueue(fetch.getMessagePutToPOCreateQueue());
							
						}
						if (dto.getIsPOCancelPresent().equalsIgnoreCase("N")) {
							logger.info("PO Cancel not present for "
									+ dto.getExtnHostOrderRef() + " - "
									+ dto.getExtnHostOrderLineRef());
						}
						
						finalCancelPOImpactedList.add(dto);
					}
				} catch (Exception e) {
					logger.error("Exception occured while processing order - line : "
							+ dto.getExtnHostOrderRef()
							+ " - "
							+ dto.getExtnHostOrderLineRef());
					e.getStackTrace();
					// logger.debug("Exception Occured " + e.getStackTrace());
				}
			}

			long slp = 150000;
			logger.info("Thread sleeping for "
					+ slp
					/ 1000
					+ " secs.. Waiting for PO cancels to process so that we can start working on reservation expiration and dirtynode");
			Thread.sleep(slp);
			logger.info("Thread resumed...");
			DupePOFixDTO obj = null;

			for (int i = 0; i < finalCancelPOImpactedList.size(); i++) {
				obj = new DupePOFixDTO();
				obj = finalCancelPOImpactedList.get(i);

				if (obj.getMessagePutToChangeOrderQueue().equalsIgnoreCase("Y")) {

					if (obj.getIsPOCreatePresent().equalsIgnoreCase("Y")) {
						logger.info("PO Create present for order - line : "
								+ obj.getExtnHostOrderRef() + " - "
								+ obj.getExtnHostOrderLineRef());
						int rowsUpdatedForReservationExpiration = expireReservationInSterling(
								obj.getExtnHostOrderRef(),
								obj.getExtnHostOrderLineRef());

						if (rowsUpdatedForReservationExpiration > 0) {
							obj.setIsreservarionExpired(true);
						}
					}

					String inputAPI = null;
					inputAPI = getInputForManageInventoryNodeControlAPI(
							obj.getExtnHostOrderRef(),
							obj.getExtnHostOrderLineRef());

					boolean isDirtyNodeTakenCare = processXML(inputAPI);
					obj.setIsDirtyNodeTakenCare(isDirtyNodeTakenCare);

				}

			}

			logger.info("Thread sleeping for "
					+ slp
					/ 1000
					+ " secs.. Waiting for PO cancels to process so that we can start processing PO Create's");
			Thread.sleep(slp);
			logger.info("Thread resumed...");
			DupePOFixDTO dto = null;

			for (int i = 0; i < finalCancelPOImpactedList.size(); i++) {

				dto = new DupePOFixDTO();

				dto = finalCancelPOImpactedList.get(i);

				if (dto.getMessagePutToChangeOrderQueue().equalsIgnoreCase("Y")) {

					if (dto.getIsPOCreatePresent().equalsIgnoreCase("Y")
							&& !poCreateSuccessMap.containsKey(dto
									.getPoCreateErrorTxnId())) {
						logger.info("PO Create present for order - line : "
								+ dto.getExtnHostOrderRef() + " - "
								+ dto.getExtnHostOrderLineRef());

						String poCreateMessage = getMessageFromSterling(dto
								.getPoCreateErrorTxnId());
						String messagePutToSTHPOCreateQueue = "N";
						messagePutToSTHPOCreateQueue = putMessageInQueue(
								poCreateMessage, sthPOCreateQueue);
						dto.setMessagePutToPOCreateQueue(messagePutToSTHPOCreateQueue);

						if (messagePutToSTHPOCreateQueue.equalsIgnoreCase("Y")) {
							poCreateSuccessMap.put(
									dto.getPoCreateErrorTxnId(), dto);
						}

					} else {
						if (poCreateSuccessMap.containsKey(dto
								.getPoCreateErrorTxnId())) {

							DupePOFixDTO fetch = new DupePOFixDTO();
							fetch = poCreateSuccessMap.get(dto
									.getPoCreateErrorTxnId());
							logger.info(dto
									.getPoCreateErrorTxnId()+ " got processed on behalf of " +fetch.getExtnHostOrderRef()+" - "+fetch.getExtnHostOrderLineRef());
							dto.setPoOrderHeaderKey(fetch.getPoOrderHeaderKey());
							dto.setExistingPO(fetch.getExistingPO());
							dto.setUpdatedPO(fetch.getUpdatedPO());
							dto.setMessagePutToChangeOrderQueue(fetch.getMessagePutToChangeOrderQueue());
							dto.setMessagePutToPOCreateQueue(fetch.getMessagePutToPOCreateQueue());
							
						}
						if (dto.getIsPOCreatePresent().equalsIgnoreCase("N")) {
							logger.info("PO Create not present for "
									+ dto.getExtnHostOrderRef() + " - "
									+ dto.getExtnHostOrderLineRef());
						}

					}

				}
			}

		} catch (Exception e) {
			e.getStackTrace();
			// logger.debug("Exception Occured " + e.getStackTrace());
		}
		return finalCancelPOImpactedList;
	}

	private static String getMessageFromSterling(String errorTxnId) {

		String message = null;
		Connection con = null;
		ResultSet rs = null;
		Statement stmt = null;
		final String driverClass = "oracle.jdbc.driver.OracleDriver";

		try {
			Class.forName(driverClass);
			String STERLING_SSC_RO_JNDI = Constants.STERLING_SSC_RO_JNDI;
			String query = Constants.getMessageFromErrorTxnId;
			query = query + " '" + errorTxnId + "'";

			DBConnectionUtil db = new DBConnectionUtil();
			con = db.getJNDIConnection(STERLING_SSC_RO_JNDI);
			;
			stmt = con.createStatement();

			rs = stmt.executeQuery(query);

			while (rs.next()) {
				message = rs.getString(1);
			}

			stmt.close();
			rs.close();
			con.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
		logger.info("Message getched for ErrorTxnId - " + errorTxnId + " : "
				+ message);
		return message;
	}

	private static String getPOOrderHeaderKey(String order, String line,
			String existingPO) {

		String poOrderHeaderKey = "";
		Connection con = null;
		ResultSet rs = null;
		Statement stmt = null;
		final String driverClass = "oracle.jdbc.driver.OracleDriver";

		try {
			Class.forName(driverClass);
			String STERLING_SSC_RO_JNDI = Constants.STERLING_SSC_RO_JNDI;
			String query = Constants.getPOOrderHeaderKey;
			query = query + " (('" + order + "', '" + line + "', '"
					+ existingPO + "'))";

			DBConnectionUtil db = new DBConnectionUtil();
			con = db.getJNDIConnection(STERLING_SSC_RO_JNDI);
			;
			stmt = con.createStatement();

			rs = stmt.executeQuery(query);

			while (rs.next()) {
				poOrderHeaderKey = rs.getString(1);
			}

			stmt.close();
			rs.close();
			con.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
		logger.info("PO OrderHeaderKey fetched for " + order + " - " + line
				+ " - " + existingPO + " is  - " + poOrderHeaderKey);
		return poOrderHeaderKey;
	}

	private static String getInputForManageInventoryNodeControlAPI(
			String order, String line) {

		String inputAPI = null;
		Connection con = null;
		ResultSet rs = null;
		Statement stmt = null;
		final String driverClass = "oracle.jdbc.driver.OracleDriver";

		try {
			Class.forName(driverClass);
			String STERLING_SSC_RO_JNDI = Constants.STERLING_SSC_RO_JNDI;
			String query = Constants.getInputForManageInventoryNodeControlAPI;
			query = query + " (('" + order + "', '" + line + "'))";

			DBConnectionUtil db = new DBConnectionUtil();
			con = db.getJNDIConnection(STERLING_SSC_RO_JNDI);
			;
			stmt = con.createStatement();

			rs = stmt.executeQuery(query);

			while (rs.next()) {
				// inputAPI = rs.getString(1);
				inputAPI = "<InventoryNodeControl InventoryPictureCorrect=\"Y\" ItemID=\""
						+ rs.getString(1)
						+ "\" Node=\""
						+ rs.getString(2)
						+ "\" "
						+ "NodeControlType=\"ON_HOLD\" OrganizationCode=\"HDUS\" ProductClass=\""
						+ rs.getString(3)
						+ "\" UnitOfMeasure=\""
						+ rs.getString(4) + "\"/>";
			}

			stmt.close();
			rs.close();
			con.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
		logger.info("Input for manageInventoryNodeControl  API - " + inputAPI);
		return inputAPI;
	}

	private static int updateDummyPONumberInSterling(String poOrderHeaderKey,
			String updatedPO) {

		String message = null;
		Connection con = null;
		int rowsUpdated = 0;
		Statement stmt = null;
		final String driverClass = "oracle.jdbc.driver.OracleDriver";

		try {
			Class.forName(driverClass);
			String STERLING_ATC_RW_JNDI = Constants.STERLING_ATC_RW_JNDI;
			String query1 = Constants.updateDummyPONumber_1;
			String query2 = Constants.updateDummyPONumber_2;
			String finalQuery = "";
			finalQuery = query1 + " '" + updatedPO + "' " + query2 + "'"
					+ poOrderHeaderKey + "'";

			DBConnectionUtil db = new DBConnectionUtil();
			con = db.getJNDIConnection(STERLING_ATC_RW_JNDI);
			stmt = con.createStatement();

			rowsUpdated = stmt.executeUpdate(finalQuery);

			stmt.close();
			con.commit();
			con.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
		if (rowsUpdated > 0) {
			logger.info("ExtnPONumber updated to - " + updatedPO
					+ " for PO orderheaderkey - " + poOrderHeaderKey);
		}
		return rowsUpdated;
	}

	private static int expireReservationInSterling(String order, String line) {

		String message = null;
		Connection con = null;
		int rowsUpdated = 0;
		Statement stmt = null;
		final String driverClass = "oracle.jdbc.driver.OracleDriver";
		String reservationId = "ORD_" + order + "_" + line;

		try {
			Class.forName(driverClass);
			String STERLING_ATC_RW_JNDI = Constants.STERLING_ATC_RW_JNDI;
			String query = Constants.expireReservation;

			query = query + " '" + reservationId + "'";

			DBConnectionUtil db = new DBConnectionUtil();
			con = db.getJNDIConnection(STERLING_ATC_RW_JNDI);
			stmt = con.createStatement();
			logger.info("query for expireReservationInSterling - " + query);
			rowsUpdated = stmt.executeUpdate(query);

			stmt.close();
			con.commit();
			con.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
		if (rowsUpdated > 0) {
			logger.info("Reservation expired for id - " + reservationId
					+ " for order-line - " + order + "-" + line);
		}
		return rowsUpdated;
	}

	public static String getExtnPONumber(String str) {
		// System.out.println(str);
		String extnPONumber = null;
		if (str.contains("ExtnPONumber")) {
			String dupStr = str;
			String lock = null;
			int firstIndex = dupStr.indexOf("ExtnPONumber");
			firstIndex = firstIndex + 14;
			int lastIndex = firstIndex + 11;
			lock = dupStr.substring(firstIndex, lastIndex);
			lastIndex = lock.indexOf("\"");
			lock = lock.substring(0, lastIndex);
			// System.out.println(lock);
			extnPONumber = lock;
		}
		logger.info("ExtnPONumber found in the message - " + extnPONumber);
		return extnPONumber;
	}

	public static String getDummyExtnPONumber(String existingPO, String order,
			String line) {
		// System.out.println(str);
		String updatedPO = null;
		String first3Aplha = existingPO.substring(0, 3);
		String last5Digits = order
				.substring(order.length() - 4, order.length());
		updatedPO = first3Aplha + last5Digits + line;
		logger.info("Dummy PO number formed for the PO " + existingPO + " - "
				+ updatedPO);
		return updatedPO;
	}

	public static String putMessageInQueue(String messageToBeProcessed,
			String changeOrderQueue) {
		String isQueuePutSuccess = "N";
		JmsDestination destination = null;
		destination = new JmsDestination("jms/MQPutQCF_Str", changeOrderQueue);
		try {
			destination.open();
			destination.send(messageToBeProcessed);
			isQueuePutSuccess = "Y";
			destination.close();
		} catch (ProcessingException e) {
			// logger.debug("Caught exception while putting into queue");
		}
		if (isQueuePutSuccess.equalsIgnoreCase("Y")) {
			logger.info("Message put to - " + changeOrderQueue);
		}
		return isQueuePutSuccess;
	}

	public static boolean processXML(String requestXML) {
		boolean xmlPut = false;
		// String url =
		// "http://ccliqas4:15400/smcfs/interop/InteropHttpServlet";

		String url = Constants.STERLING_URL;
		String URLEncodedInputXML = null;
		try {
			URLEncodedInputXML = URLEncoder.encode(requestXML, "UTF-8");
		} catch (Exception ex) {
			logger.debug("Exception occurred in send method: " + ex);
		}

		StringBuilder queryString = new StringBuilder();
		queryString.append("InteropApiName=");
		queryString.append("manageInventoryNodeControl");
		queryString.append("&IsFlow=N");
		queryString.append("&YFSEnvironment.userId=admin");
		queryString.append("&InteropApiData=");
		queryString.append(URLEncodedInputXML);

		String outXml = send(url, queryString.toString());
		xmlPut = true;
		logger.info("Sterling manageInventoryNodeControl(dirty node) API Response XML: "
				+ outXml);
		return xmlPut;
	}

	public static String send(String url, String postRequest) {
		String sterlingResponse = null;
		// Create Jersey client
		Client client = Client.create();

		// String token =
		// "eS6AxTugxondtSBi8KhzUnE2NVPEB4jj1wB7liUQEOUKf4Bn9UWopWx7zJnDbqzN53BG7mkXOgWoJuLH0Py37Ekohwol41hn7qUWh5KU4Mmw14KwQwfOOIgpKFwRnFbSkNk86nYH0g";
		String token = Constants.TOKEN_PR;
		// Create WebResource
		WebResource webResource = client.resource(url);
		ClientResponse response = null;
		try {
			response = webResource.header("THDService-Auth", token)
					.type("application/x-www-form-urlencoded")
					.post(ClientResponse.class, postRequest);

			// Retrieve status and check if it is OK
			int status = 0;
			if (response != null) {
				status = response.getStatus();
			}
		} catch (Exception ae) {
			logger.debug("Exception occurred in send method: " + ae);
		}

		if (response != null) {
			sterlingResponse = response.getEntity(String.class);
		}
		return sterlingResponse;
	}

	public static void writeToCSV(String fileLocation,
			List<DupePOFixDTO> finalCancelPOImpactedList) {

		FileWriter writer;
		try {
			writer = new FileWriter(fileLocation, true);

//			writer.append("ExtnHostOrderRef");
//			writer.append(",");
//			writer.append("ExtnHostOrderLineRef");
//			writer.append(",");
////			writer.append("RawMessage");
////			writer.append(",");
////			writer.append("MessageToBeProcessed");
////			writer.append(",");
//			writer.append("POOrderHeaderKey");
//			writer.append(",");
//			writer.append("ExistingPO");
//			writer.append(",");
//			writer.append("UpdatedPO");
//			writer.append(",");
//			writer.append("IsPOCancelPresent");
//			writer.append(",");
//			writer.append("IsPOCreatePresent");
//			writer.append(",");
//			writer.append("POCancelErrorTxnId");
//			writer.append(",");
//			writer.append("POCreateErrorTxnId");
//			writer.append(",");
//			writer.append("MessagePutToChangeOrderQueue");
//			writer.append(",");
//			writer.append("MessagePutToPOCreateQueue");
//			writer.append(",");
//			writer.append("IsDirtyNodeTakenCare");
//			writer.append(",");
//			writer.append("IsreservarionExpired");
//			writer.append("\n");

			DupePOFixDTO dto = null;

			for (int i = 0; i < finalCancelPOImpactedList.size(); i++) {
				dto = new DupePOFixDTO();
				dto = finalCancelPOImpactedList.get(i);
				writer.append(dto.getExtnHostOrderRef());
				writer.append(",");
				writer.append(dto.getExtnHostOrderLineRef());
				writer.append(",");
//				writer.append(dto.getRawMessage());
//				writer.append(",");
//				writer.append(dto.getMessageToBeProcessed());
//				writer.append(",");
				writer.append(dto.getPoOrderHeaderKey());
				writer.append(",");
				writer.append(dto.getExistingPO());
				writer.append(",");
				writer.append(dto.getUpdatedPO());
				writer.append(",");
				writer.append(dto.getIsPOCancelPresent());
				writer.append(",");
				writer.append(dto.getIsPOCreatePresent());
				writer.append(",");
				writer.append(dto.getPoCancelErrorTxnId());
				writer.append(",");
				writer.append(dto.getPoCreateErrorTxnId());
				writer.append(",");
				writer.append(dto.getMessagePutToChangeOrderQueue());
				writer.append(",");
				writer.append(dto.getMessagePutToPOCreateQueue());
				writer.append(",");
				writer.append(dto.isIsDirtyNodeTakenCare() + "");
				writer.append(",");
				writer.append(dto.isIsreservarionExpired() + "");
				writer.append("\n");
			}

			writer.flush();
			writer.close();
			logger.info("File created at " + fileLocation);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static String processEJUpdates(){
		String status = "Failure";
		
		String fileLocation = "C:\\test\\POEJUpdatesErrorID.csv";
		List<String> errorTxnIdList = new ArrayList<String>();
		String changeOrderQueue = "jms/DI.DL.STERLING.CHANGEORDER.STH_put";
		
		String[] row = null;
		CSVReader csvReader = null;
		boolean headerRow = true;
		try {
			csvReader = new CSVReader(new FileReader(fileLocation));
			DupePOFixDTO dto = null;
			while ((row = csvReader.readNext()) != null) {
				if (headerRow) {
					headerRow = false;
				} else {
					errorTxnIdList.add(row[0].trim());
				}
			}
			csvReader.close();
			
			logger.info(errorTxnIdList.size()+" errorTxnId's found from the file - "+fileLocation);
			
			FileWriter writer;
			writer = new FileWriter(fileLocation);

			
			writer.append("ErrorTxnId");
			writer.append(",");
			writer.append("Status");
			writer.append("\n");
			
			String msg = "";
			for (int i = 0; i < errorTxnIdList.size(); i++) {
				msg = getMessageFromSterling(errorTxnIdList.get(i));
				
				String messagePutToChangeOrderQueue = putMessageInQueue(
						msg, changeOrderQueue);
				
				writer.append(errorTxnIdList.get(i));
				writer.append(",");
				writer.append(messagePutToChangeOrderQueue);
				writer.append("\n");
				
				logger.info(errorTxnIdList.get(i)+" - "+messagePutToChangeOrderQueue);
			}
			
			writer.flush();
			writer.close();
			logger.info("File created at " + fileLocation);
			
			status = "Success";
			
		} catch (Exception e) {
			e.getStackTrace();
			// logger.debug("Exception Occured " + e.getStackTrace());
		}
		
		return status;
	}
}
