package com.homedepot.di.dl.support.appllogs.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.homedepot.di.dl.support.appllogs.dao.GetLogs;

public class GetApplLog extends HttpServlet {

	private int returnValue;
	private static final long serialVersionUID = 1L;

	public void init(ServletConfig config) throws ServletException {
		super.init(config);
	}

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		GetLogs log = new GetLogs();
		try {
			log.getGridLogs();
			returnValue = 0;
		} catch (Exception e) {
			returnValue = 1;
			e.printStackTrace();

		}
		response.setContentType("text/html");

		PrintWriter out = response.getWriter();
		/*
		 * Write the HTML to the response
		 */
		out.println(returnValue);
		out.close();
	}

	public void destroy() {

	}
}
