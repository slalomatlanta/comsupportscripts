package com.homedepot.di.dl.support.appllogs.dao;

import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.apache.log4j.Logger;

import com.homedepot.di.dl.support.switchToNormal.dao.SwitchTransmissionState;
import com.homedepot.di.dl.support.util.Constants;
import com.homedepot.di.dl.support.util.DBConnectionUtil;

public class GetLogs {
	final static Logger logger = Logger.getLogger(GetLogs.class);

	/*public static void main(String[] args) throws IOException, SQLException,
			ClassNotFoundException {
		getGridLogs();
	}*/

	public void getGridLogs() throws IOException, SQLException,
			ClassNotFoundException {
		
		Connection appllog=null;

		try {
			
DBConnectionUtil dbConn = new DBConnectionUtil();
			
			appllog = dbConn.getJNDIConnection(Constants.COM_SUPPORT_APPLLOG_JNDI);
			/*
			 * con = DriverManager .getConnection(
			 * "jdbc:informix-sqli://appllog-qa:25005/dpr060ta:informixserver=ta70_inst"
			 * , "tadata01", "kc9wvu5");
			 */

			Statement stmt4 = null;
			stmt4 = appllog.createStatement();
			ResultSet rs4 = null;

			Calendar cal = Calendar.getInstance();
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			String date = dateFormat.format(cal.getTime());
			int hour = cal.get(Calendar.HOUR_OF_DAY);
			if (hour == 0) {
				hour = 24;
				cal.add(Calendar.DATE, -1);
				date = dateFormat.format(cal.getTime());
			}
			String currentTime = date + " " + (hour - 1) + ":00:00";
			String oneHourOldTime = date + " " + (hour - 1) + ":59:59";
			// System.out.println("currentTime :" + currentTime);
			// System.out.println("oneHourTime :" + oneHourOldTime);

			String query = "Select evnt_ts, pgm_id, msg_data_val from appl_log where sub_sys_cd = 'dl' "
					+ "and pgm_id in ('COMAutoCancelHandl','COMDeferStager','COMDeferralExtract',"
					+ "'COMEmailHandler', 'COMFulfillmentHand', 'COMInventoryServic', 'COMLocCapability', "
					+ " 'COMNSDemandLoader', 'COMOrderIfc', 'COMParmManager', "
					+ "'COMQueueRetry', 'COMRefundHandler', 'COMRetrieveDemand' , "
					+ "'COMLogicalTransfer', 'COMStrInvAdjHandle'"
					+ ") and evnt_typ_ind ='F' and msg_data_val not like 'Fatal error occurred during processing % message to ERROR queue%'"
					+ "and evnt_ts between '"
					+ currentTime
					+ "' and '"
					+ oneHourOldTime + "'";

			//System.out.println("Query :" + query);
			rs4 = stmt4.executeQuery(query);

			String outputFile = "/opt/isv/tomcat-6.0.18/temp/ApplLogs.csv";
			//String outputFile = "C:\\test\\Logs.csv";
			FileWriter writer = new FileWriter(outputFile);
			/*
			 * writer.append("Date"); writer.append(',');
			 * writer.append("Program Id"); writer.append(',');
			 * writer.append("Error Message"); writer.append('\n');
			 */

			try {
				while (rs4.next()) {

					writer.append(rs4.getString(1));
					writer.append(',');
					writer.append(rs4.getString(2));
					writer.append(',');
					writer.append(rs4.getString(3).replace(",", ";"));
					writer.append('\n');

					// System.out.println("Message :" + rs4.getString(2));
				}
			} catch (Exception e) {
				logger.error("Exception while reading csv file: " + e);
			}

			// writer.append("test");
			// writer.append(',');
			// writer.append("test");
			// writer.append(',');
			// writer.append("test");
			// writer.append('\n');

			writer.flush();
			writer.close();
			// System.out.println("All Stores Error messages retrived");

			rs4.close();
			stmt4.close();

			appllog.close();

		} finally {

			if (appllog != null) {
				try {
					logger.info("  Closing down all connections...\n\n");
					appllog.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
