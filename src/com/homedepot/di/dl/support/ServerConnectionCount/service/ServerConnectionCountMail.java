package com.homedepot.di.dl.support.ServerConnectionCount.service;

import java.io.IOException;
import java.util.Calendar;
import java.util.Properties;

import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage.RecipientType;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.mail.Message;

import javax.mail.Transport;

import javax.mail.internet.MimeBodyPart;

import javax.mail.internet.MimeMultipart;

import org.apache.log4j.Logger;

/**
 * Servlet implementation class LongRunningQueryMail
 */
public class ServerConnectionCountMail extends HttpServlet {
	private static final long serialVersionUID = 1L;
	final static Logger logger = Logger.getLogger(ServerConnectionCountMail.class);

	/**
	 * @throws Exception
	 * @see HttpServlet#HttpServlet()
	 */
	public ServerConnectionCountMail() throws Exception {
		super();
		// TODO Auto-generated constructor stub

	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		try {
			logger.debug("Start Time: Mail:"
					+ java.util.GregorianCalendar.getInstance().getTime());
			String mailContent;

			mailContent = ServerConnectionCount
					.getSterlingConnectionCountMail();

			if (!(mailContent.equals("NoHiPri"))) {
				String eMailSubject = "Critical Alert: Sterling Server Connection Count- High";
				sendMail(mailContent, true, eMailSubject);
				logger.debug("end Time::"
						+ java.util.GregorianCalendar.getInstance().getTime());
			}
			//logger.debug("Servlet:::" + mailContent);
			logger.debug("end Time:Mail:"
					+ java.util.GregorianCalendar.getInstance().getTime());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	public static void sendMail(String emailContent, boolean isCritical,
			String eMailSubject) {
		String text = emailContent;
		// String[] to = { "NIRAL_PATEL2@homedepot.com" ,
		// "DINESH_E@homedepot.com" , "jagatdeep_chakraborty@homedepot.com" ,
		// "MAHESH_VYAKARANAM@homedepot.com" ,
		// "PURUSHOTHAMAN_RAGHUNATH@homedepot.com" ,
		// "RAKESH_J_SATYA@homedepot.com" , "MANOJ_SHUNMUGAM@homedepot.com" };
		//String[] to ={"steven_l_kaneti@homedepot.com"};
		//String[] to = { "_2fc77b@homedepot.com" };
		String[] to = { "Order_Mgmt_Support@homedepot.com" };
		String[] Cc = {};
		Session session = null;
		MimeMessage mimeMessage = null;
		MimeBodyPart mimeBodyPart = null;
		Multipart multiPart = null;
		String date = null;
		Calendar calendar = null;
		Properties properties = null;
		String msg = null;
		try {
			msg = "Success";
			calendar = Calendar.getInstance();
			date = calendar.getTime().toString();
			properties = System.getProperties();
			properties.put("mail.host", "mail1.homedepot.com");
			session = Session.getInstance(properties, null);
			mimeMessage = new MimeMessage(session);
			mimeMessage.setFrom(new InternetAddress(
					"horizon@cpliisad.homedepot.com"));
			InternetAddress[] addressTo = new InternetAddress[to.length];
			InternetAddress[] addressCc = new InternetAddress[Cc.length];
			for (int i = 0; i < to.length; i++) {
				addressTo[i] = new InternetAddress(to[i]);
				logger.debug("To Address " + to[i]);
			}
			for (int j = 0; j < Cc.length; j++) {
				addressCc[j] = new InternetAddress(Cc[j]);
				logger.debug("Cc Address " + Cc[j]);
			}
			mimeMessage.setRecipients(RecipientType.TO, addressTo);
			mimeMessage.setRecipients(RecipientType.CC, addressCc);
			mimeMessage.setSubject(eMailSubject + date);
			mimeBodyPart = new MimeBodyPart();
			mimeBodyPart.setText(text.toString());
			multiPart = new MimeMultipart();
			multiPart.addBodyPart(mimeBodyPart);
			mimeMessage.setContent(text.toString(), "text/html");
			if (text.length() > 0) {
				if (isCritical) {
					mimeMessage.setHeader("X-Priority", "1");

					logger.info("Sending mail as high priority - "
							+ java.util.GregorianCalendar.getInstance()
									.getTime());
				} else {
					mimeMessage.setHeader("X-Priority", "3");
					logger.info("Sending mail... - "
							+ java.util.GregorianCalendar.getInstance()
									.getTime());
				}
				Transport.send(mimeMessage);
				logger.info("Sent mail succesfully...");
			}
		} catch (Exception e) {
			msg = "Failure";
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

}
