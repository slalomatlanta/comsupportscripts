package com.homedepot.di.dl.support.ServerConnectionCount.service;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.homedepot.di.dl.support.util.Constants;
import com.homedepot.di.dl.support.util.DBConnectionUtil;



public class ServerConnectionCount extends Constants {

	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		//getLongRunningQuery();
		/*String str1="43.37";
		System.out.println(Double.parseDouble(str1)>=60);
		Date currts= new Date();
		System.out.println("todays date:"+Calendar.getInstance());
		
		
		String sla="12:30";
		 boolean slaMissedOrnot = false;
	        Calendar cal = Calendar.getInstance();
	        System.out.println(sla.substring(0, 2));
	        cal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(sla.substring(0, 2)));
	        System.out.println(sla.substring(3));
	        cal.set(Calendar.MINUTE, Integer.parseInt(sla.substring(3)));
	        cal.set(Calendar.SECOND, 0);
	        cal.set(Calendar.MILLISECOND, 0);
	        if (Calendar.getInstance().after(cal)) {
	            System.out.println("it's SLA missed");
	            slaMissedOrnot = true;
	        } else {
	            System.out.println("it's fine & not SLA missed");
	        }
	        if(slaMissedOrnot){
	        System.out.println(slaMissedOrnot);
	        }*/
		//getSterlingConnectionCount();
		
	}
	
	private static final Logger oLogger = Logger.getLogger("splunkLog");
	final static Logger logger = Logger.getLogger(ServerConnectionCount.class);
	
	public static String getSterlingConnectionCount() throws Exception{

		//Gson gson = new Gson();
		String display = "", sterlingTable = "";		
		Map<String,String> orangeLogMsgMap = new HashMap<String, String>();	
		
		
	    List<ServerConnectionCountDTO>  sterqueryList=getServerConnectionCount();
	    	display = "<html><head><body bgcolor=\"#676767\">" + "<font face=\"calibri\">";
	    	display = display + "<h2><center><b><font color=\"White\">Sterling Connection Count</b></center></h2><br/>";
	    	display = display + "<h3><font color=\"White\">Production: </h3></p>";
			String tableHdr = "<table border=\"1\" cellpadding=\"1\" cellspacing=\"0\" width=\"200px\" style=\"border-collapse:collapse;\" bgcolor=\"#F2F2F2\">"
					+ "<font face=\"calibri\">"
					+ "<tr bgcolor=\"#FF8C26\">"
					+ "<th>Connection Count</th>"
					+ "<th>Server</th>"
					+ "</tr>";			
			for( ServerConnectionCountDTO serCountDTO: sterqueryList){
				sterlingTable = sterlingTable + "<tr>"
					+ "<td align=\"center\">" + serCountDTO.getTotConnCount() + "</td>"
					+ "<td align=\"center\">" + serCountDTO.getServerName() + "</td>"
					+ "</tr>";
				orangeLogMsgMap.put(serCountDTO.getServerName() , serCountDTO.getTotConnCount());
	    }
		display = display+tableHdr+sterlingTable; //tableHdr1+ODSTable;
			
			display = display +"</body></head></html>";
			oLogger.info("Program=getSterlingConnectionCount ConnectionCounts=" + orangeLogMsgMap);
//	    System.out.println("HTML:"+display);
		//String json = "hi";//gson.toJson(queryList);		
	    return display;
	}
	
	
public static List<ServerConnectionCountDTO> getServerConnectionCount() throws Exception {
		
		List<ServerConnectionCountDTO> reprocessList = new ArrayList<ServerConnectionCountDTO>();

		String count = null;
		
		DBConnectionUtil dbConn = new DBConnectionUtil();
		Connection sscConn = dbConn.getJNDIConnection(STERLING_ATC_RW2_JNDI);
		
		
		
		final String query = "select count(1), machine from gv$session where module like '%APPSERVER%' and client_info not like '%HM%' group by machine order by machine asc";

//		System.out.println(query);
		Connection con = null;
		try {
			
//			System.out.println("Before start connection");
			/*Class.forName(driverClass);
			con = DriverManager.getConnection(connectionURL, uName, uPassword);
			System.out.println("DBConnection success");*/
			Statement stmt = null;
			//stmt = con.createStatement();
			ResultSet result = null;
			//result = stmt.executeQuery(query);
			//DecimalFormat df = new DecimalFormat("0.00##");
			//String result = df.format(34.4959);
			
			
			stmt = sscConn.createStatement();
			logger.info("DBConnection success");
			result = stmt.executeQuery(query);
			
			
			while (result.next()) {
				ServerConnectionCountDTO connCountData =  new ServerConnectionCountDTO();
				connCountData.setTotConnCount(result.getString(1));
				connCountData.setServerName(result.getString(2));
				
				reprocessList.add(connCountData);				
			}
			stmt.close();
			result.close();
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
			
		}finally{
			
			sscConn.close();
		}

		return reprocessList;
	}
public static List<ServerConnectionCountDTO> getServerConnectionCountMailThreshSter() throws Exception{
	List finList=new ArrayList();
	List<ServerConnectionCountDTO> finSterList=new ArrayList<ServerConnectionCountDTO>();
	List<ServerConnectionCountDTO> finODSList=new ArrayList<ServerConnectionCountDTO>();
	  //List<longrunningQueryDTO>  odsqueryList=getLongrunningqueryODS();	
	    List<ServerConnectionCountDTO>  connList=getServerConnectionCount();
	   //module- SQL Developer,INT&HDDelProcessAgent,didlCOMRetrieveDemand,didlSvsComServices
	    for( ServerConnectionCountDTO connDTO: connList){
//	    	System.out.println("Server::"+connDTO.getServerName()+"::connectionCount::"+connDTO.getTotConnCount());
	    	
	    	
	    	if(Integer.parseInt(connDTO.getTotConnCount()) >=STERLING_CONNECTION_COUNT){
	    		finSterList.add(connDTO);
	    	}
	    }
	    finList.add(finSterList);
	    
	  
	    
	return finSterList;
	
}



public static String getSterlingConnectionCountMail() throws Exception{

	//Gson gson = new Gson();
	String display = "", sterlingTable = "", ODSTable = "";
	List finList= new ArrayList();
	
    List<ServerConnectionCountDTO>  sterqueryList=getServerConnectionCountMailThreshSter();
	
	
	
	if(sterqueryList.size()>0){
    	display = "<html><head><body>" + "<font face=\"calibri\">";
    	display = display + "<h3><font>Sterling Connection Count(exceeds "+STERLING_CONNECTION_COUNT +"): </h3></p>";
		String tableHdr = "<table border=\"1\" cellpadding=\"1\" cellspacing=\"0\" width=\"200px\" style=\"border-collapse:collapse;\" >"
				+ "<font face=\"calibri\">"
				+ "<tr>"
				+ "<th>Connection Count</th>"
				+ "<th>Server Name</th>"
				+ "</tr>";
		for( ServerConnectionCountDTO connDTO: sterqueryList){
			sterlingTable = sterlingTable + "<tr>"
				+ "<td align=\"center\">" + connDTO.getTotConnCount()+ "</td>"
				+ "<td align=\"center\">" + connDTO.getServerName() + "</td>"
				+ "</tr>";
			
    }
			
		
		ODSTable = ODSTable + "</table><br/><hr/>";
		display = display+tableHdr+sterlingTable;
		
		display = display +"</body></head></html>";
    }else {
		display="NoHiPri";
	}
	//String json = "hi";//gson.toJson(queryList);		
    return display;
}

}
