package com.homedepot.di.dl.support.ServerConnectionCount.service;

public class ServerConnectionCountDTO {
	
	private String 	totConnCount;
	private String 	ServerName;
	
	
	public String getTotConnCount() {
		return totConnCount;
	}
	public void setTotConnCount(String totConnCount) {
		this.totConnCount = totConnCount;
	}
	public String getServerName() {
		return ServerName;
	}
	public void setServerName(String serverName) {
		ServerName = serverName;
	}
	
	
	
	}
