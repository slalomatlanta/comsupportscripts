package com.homedepot.di.dl.support.ServerConnectionCount.service;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
/**
 * Servlet implementation class LongRunningQueryServlet
 */
public class ServerConnectionCountServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	final static Logger logger = Logger.getLogger(ServerConnectionCountServlet.class);
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServerConnectionCountServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html");
		//response.setHeader("Pragma", "no-cache");
		PrintWriter out = null;
		out = response.getWriter();
		String DispCont="";
		try {
			logger.info("Start Time::"+java.util.GregorianCalendar.getInstance().getTime());
			DispCont = ServerConnectionCount.getSterlingConnectionCount();
			logger.info("End Time::"+java.util.GregorianCalendar.getInstance().getTime());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		out.println(DispCont);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
