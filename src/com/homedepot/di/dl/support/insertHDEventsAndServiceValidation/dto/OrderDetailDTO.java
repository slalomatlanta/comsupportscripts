package com.homedepot.di.dl.support.insertHDEventsAndServiceValidation.dto;

import java.sql.Date;

/**
 * @author PXR8854
 * 
 */
public class OrderDetailDTO {
	private String orderRef;
	private String ordLineKey;
	private String orderLineRef;
	private String ordHeadKey;
	private String sellOrgCode;
	private String sellLocation;
	private String deliveryMd;
	private String itemGrpCode;
	private String lineType;
	private String shipNodeKey;
	private String extnSKUCode;
	private String createTs;
	private String orderedQty;
	private String hdpsiKey;
	private String hdpsiStatus;
	private String quoteEffts;
	private String doneEffts;
	private String vEventts;
	

	public String getvEventts() {
		return vEventts;
	}
	public void setvEventts(String vEventts) {
		this.vEventts = vEventts;
	}
	public String getQuoteEffts() {
		return quoteEffts;
	}
	public void setQuoteEffts(String quoteEffts) {
		this.quoteEffts = quoteEffts;
	}
	public String getDoneEffts() {
		return doneEffts;
	}
	public void setDoneEffts(String doneEffts) {
		this.doneEffts = doneEffts;
	}
	public String getOrderRef() {
		return orderRef;
	}
	public void setOrderRef(String orderRef) {
		this.orderRef = orderRef;
	}
	public String getOrdLineKey() {
		return ordLineKey;
	}
	public void setOrdLineKey(String ordLineKey) {
		this.ordLineKey = ordLineKey;
	}
	public String getOrderLineRef() {
		return orderLineRef;
	}
	public void setOrderLineRef(String orderLineRef) {
		this.orderLineRef = orderLineRef;
	}
	public String getOrdHeadKey() {
		return ordHeadKey;
	}
	public void setOrdHeadKey(String ordHeadKey) {
		this.ordHeadKey = ordHeadKey;
	}
	public String getSellOrgCode() {
		return sellOrgCode;
	}
	public void setSellOrgCode(String sellOrgCode) {
		this.sellOrgCode = sellOrgCode;
	}
	public String getSellLocation() {
		return sellLocation;
	}
	public void setSellLocation(String sellLocation) {
		this.sellLocation = sellLocation;
	}
	public String getDeliveryMd() {
		return deliveryMd;
	}
	public void setDeliveryMd(String deliveryMd) {
		this.deliveryMd = deliveryMd;
	}
	public String getItemGrpCode() {
		return itemGrpCode;
	}
	public void setItemGrpCode(String itemGrpCode) {
		this.itemGrpCode = itemGrpCode;
	}
	public String getLineType() {
		return lineType;
	}
	public void setLineType(String lineType) {
		this.lineType = lineType;
	}
	public String getShipNodeKey() {
		return shipNodeKey;
	}
	public void setShipNodeKey(String shipNodeKey) {
		this.shipNodeKey = shipNodeKey;
	}
	public String getExtnSKUCode() {
		return extnSKUCode;
	}
	public void setExtnSKUCode(String extnSKUCode) {
		this.extnSKUCode = extnSKUCode;
	}
	public String getCreateTs() {
		return createTs;
	}
	public void setCreateTs(String createTs) {
		this.createTs = createTs;
	}
	public String getOrderedQty() {
		return orderedQty;
	}
	public void setOrderedQty(String orderedQty) {
		this.orderedQty = orderedQty;
	}
	public String getHdpsiKey() {
		return hdpsiKey;
	}
	public void setHdpsiKey(String hdpsiKey) {
		this.hdpsiKey = hdpsiKey;
	}
	public String getHdpsiStatus() {
		return hdpsiStatus;
	}
	public void setHdpsiStatus(String hdpsiStatus) {
		this.hdpsiStatus = hdpsiStatus;
	}
	
	}
