package com.homedepot.di.dl.support.insertHDEventsAndServiceValidation.service;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import au.com.bytecode.opencsv.CSVReader;

import com.homedepot.di.dl.support.compareDataReplication.web.CompareDataReplicationServlet;
import com.homedepot.di.dl.support.insertHDEventsAndServiceValidation.dao.PaymentStageDao;
import com.homedepot.di.dl.support.insertHDEventsAndServiceValidation.dto.OrderDetailDTO;
import com.homedepot.di.dl.support.util.DateUtil;
import com.homedepot.di.dl.support.util.OrderUtil;

import org.apache.log4j.Logger;

public class InsertHDEventsAndUpdateHDPSI {
	
	final static Logger logger = Logger.getLogger(InsertHDEventsAndUpdateHDPSI.class);
	

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//InsertHDEventsAndUpdateHDPSI();
String store="DEFAULT";
String st="140";
Boolean check=(store.length()>=6);

//System.out.println(""+check);

	}
	
	
	public static void InsertHDEventsAndUpdateHDPSI(){
		
		ArrayList<OrderDetailDTO> orderList = new ArrayList<OrderDetailDTO>();
		PaymentStageDao PaymentStageDao= new PaymentStageDao();
		DateUtil dateUtil= new DateUtil();
		OrderUtil recall=new OrderUtil();
		
		orderList=readInputSpreadSheet();
	
		logger.debug("List size:"+orderList.size());
		if(orderList.size()>0){
			for(OrderDetailDTO orderDet: orderList){
				if(orderDet.getHdpsiStatus().toString().equals("1")){
					//updating hdpsi status to 0 in the hdpsi table
					PaymentStageDao.updateHDPSIStatus(orderDet.getHdpsiKey());
					logger.debug("HDPSIKey::;"+orderDet.getHdpsiKey());
					if((orderDet.getItemGrpCode().equals("DS")) || (orderDet.getItemGrpCode().equals("PS")) ){
						if((orderDet.getLineType().equals("IN")) || (orderDet.getLineType().equals("DL"))){
						
							logger.debug("Inside if condition of DS and PS::"+orderDet.getItemGrpCode()+"::LineType::"+orderDet.getLineType());
						
						//Get the timestamp from the hdlinestatus table to STatusEffectiveTs - which should be between and quote and done status
						
						OrderDetailDTO hdLinestatus=PaymentStageDao.retrieveHDlinestatusEffeTs(orderDet.getOrdLineKey());
						//Get the random dates between the quote and Done status
						Date effdt=(dateUtil.getDaysBetweenDates(hdLinestatus.getQuoteEffts().toString(),hdLinestatus.getDoneEffts().toString())).get(1);
						SimpleDateFormat Dtformat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
						String effts=Dtformat.format(effdt);
						logger.debug("Random date::"+effts);
						//Lock the order and get the lock Id
						String ordRecall=recall.getOrderDetailWithLock(orderDet.getOrderRef(),orderDet.getShipNodeKey());
						//String extnLockID =recall.getLockId((recall.getOrderDetailWithLock(orderDet.getOrderRef(),orderDet.getShipNodeKey())));
						
						if (!ordRecall.contains("Error")
								&& ordRecall.contains("DocumentType=\"0001\"")) {
							
						String extnLockID=recall.getLockId(ordRecall);
						logger.debug("extnLockID::"+extnLockID);
						String modifyXml=servicvalidxml(orderDet,effts,extnLockID);
						
						logger.debug("modifyxml:"+modifyXml);
						//String responseXml=recall.callOrderModifyService(modifyXml);
						//String responseStatus = recall.checkResponseType(responseXml);
						
						/*if (responseStatus.equals("SUCCESS")) {
							System.out.println(":::XmlProcessed Succefully:::::");
							System.out.println(responseStatus);
							
						}*/
						}
							
						}
					}else{
						//For other group code
						
						logger.debug("Inside else part" +
								"::"+orderDet.getItemGrpCode()+"::LineType::"+orderDet.getLineType());		
						OrderDetailDTO hdLinestatus=PaymentStageDao.retrieveVEventsEffeTs(orderDet.getOrdLineKey());
						
						SimpleDateFormat Dtformat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
						String effts=Dtformat.format(hdLinestatus.getvEventts());
						logger.debug("V event date after conversion::"+effts);
						//Lock the order 
						String ordRecall=recall.getOrderDetailWithLock(orderDet.getOrderRef(),orderDet.getShipNodeKey());
						if (!ordRecall.contains("Error")
								&& ordRecall.contains("DocumentType=\"0001\"")) {
							//Retrieving the lock ID
						String extnLockID=recall.getLockId(ordRecall);
						logger.debug("extnLockID::"+extnLockID);
						String modifyXml=vEventxml(orderDet,effts,extnLockID);
						
						logger.debug("modifyxml:"+modifyXml);
						//String responseXml=recall.callOrderModifyService(modifyXml);
						//String responseStatus = recall.checkResponseType(responseXml);
						
						/*if (responseStatus.equals("SUCCESS")) {
							System.out.println(":::XmlProcessed Succefully:::::");
							System.out.println(responseStatus);
							
						}*/
						
					}
					
					
				}
				
			}
			
			
		}
		
		
		}
		
		
	}
	
	
	public static String servicvalidxml(OrderDetailDTO orderDetailDTO, String DateToStr,String extnLockID) {
		
		//logger.debug("Before conversion::"+DateToStr);
		
		String serXML ="<?xml version=\"1.0\" encoding=\"UTF-8\"?>"+
		"<Orders>"+
		 "<Extn ExtnLockID=\""+extnLockID+"\" ExtnLockRefStore=\"9760\" ExtnLockRefUser=\"ASM001\" />"+
		 "<Order DocumentType=\"0001\" DraftOrderFlag=\"N\" EnterpriseCode=\"HDUS\" SellerOrganizationCode=\""+orderDetailDTO.getSellOrgCode().trim()+"\">"+
		  "<Extn ExtnDerivedTransactionType=\"ORDER_MODIFY\" ExtnHostOrderReference=\""+orderDetailDTO.getOrderRef()+"\" >"+
		  "</Extn>"+
		  "<OrderLines>"+
		   "<OrderLine ItemGroupCode=\""+orderDetailDTO.getItemGrpCode()+"\" LineType=\""+orderDetailDTO.getLineType()+"\"  ShipNode=\""+orderDetailDTO.getShipNodeKey().trim()+"\">"+
		   " <Extn ExtnHostOrderLineReference=\""+orderDetailDTO.getOrderLineRef()+"\" ExtnSKUCode=\""+orderDetailDTO.getExtnSKUCode()+"\" >"+
		     "<HDLineStatusList>"+
		        "<HDLineStatus SrcModifyID=\"sv\" StatusCode=\"130\" StatusDescription=\"Service validated\" StatusEffectiveTs=\""+DateToStr+"\" StoreNumber=\""+orderDetailDTO.getSellOrgCode().trim()+"\" UserAuditID=\"STM001\"/>"+ 
		    " </HDLineStatusList>"+
		    "</Extn>"+
		    "<Item ProductClass=\"GOOD\"/>"+
		   "</OrderLine>"+
		  "</OrderLines>"+
		 "</Order>"+
		"</Orders>";
		
		return serXML;
		
		
	}
	
public static String vEventxml(OrderDetailDTO orderDetailDTO, String DateToStr,String extnLockID) {
		
	//logger.debug("Before conversion::"+DateToStr);
		String serXML ="<?xml version=\"1.0\" encoding=\"UTF-8\"?>"+
		"<Orders>"+
		 "<Extn ExtnLockID=\""+extnLockID+"\" ExtnLockRefStore=\"9760\" ExtnLockRefUser=\"ASM001\" />"+
		 "<Order DocumentType=\"0001\" DraftOrderFlag=\"N\" EnterpriseCode=\"HDUS\" SellerOrganizationCode=\""+orderDetailDTO.getSellOrgCode().trim()+"\">"+
		  "<Extn ExtnDerivedTransactionType=\"ORDER_MODIFY\" ExtnHostOrderReference=\""+orderDetailDTO.getOrderRef()+"\" >"+
		  "</Extn>"+
		  "<OrderLines>"+
		   "<OrderLine DeliveryMethod=\""+orderDetailDTO.getDeliveryMd()+" ItemGroupCode=\""+orderDetailDTO.getItemGrpCode()+"\" LineType=\""+orderDetailDTO.getLineType()+"\"  ShipNode=\""+orderDetailDTO.getShipNodeKey().trim()+"\">"+
		   " <Extn ExtnHostOrderLineReference=\""+orderDetailDTO.getOrderLineRef()+"\" ExtnSKUCode=\""+orderDetailDTO.getExtnSKUCode()+"\" >"+
		   "<HDEventList>"+
		   "<HDEvent EventLocationNbr=\""+orderDetailDTO.getSellOrgCode().trim()+"\" EventReturnLocation=\"0\" EventRsnCode=\"0\" EventTrkCode=\"V\" EventTs=\""+DateToStr+"\" EventUserAuditID=\"ASM001\" MerQty=\""+orderDetailDTO.getOrderedQty()+"\" Store=\""+orderDetailDTO.getShipNodeKey().trim()+"\"/>"+
		   "</HDEventList>"+
		    "</Extn>"+
		    "<Item ProductClass=\"GOOD\"/>"+
		   "</OrderLine>"+
		  "</OrderLines>"+
		 "</Order>"+
		"</Orders>";
		
		return serXML;
		
		
	}
	


	public static ArrayList<OrderDetailDTO> readInputSpreadSheet() {

		String csvFilename = "C:\\paymentstageInfo\\paymentstageInfo.csv";
		String[] row = null;
		CSVReader csvReader = null;
		OrderDetailDTO orderDetailDTO = null;
		boolean col = true;
		ArrayList<OrderDetailDTO> orderDetailDTOList = new ArrayList<OrderDetailDTO>();

		try {
			csvReader = new CSVReader(new FileReader(csvFilename));
			while ((row = csvReader.readNext()) != null) {
				if (!col) {
					orderDetailDTO = new OrderDetailDTO();
					logger.debug("row 0::"+row[0].trim());
					logger.debug("row1::"+row[1].trim());
					logger.debug("row2::"+row[2].trim());
					
					orderDetailDTO.setOrderRef(row[0].trim());
					orderDetailDTO.setOrdHeadKey(row[1].trim());
					orderDetailDTO.setOrdLineKey(row[2].trim());
					orderDetailDTO.setOrderLineRef(row[3].trim());
					if (row[4].trim().length() == 4) {
						orderDetailDTO.setSellOrgCode(row[4].trim());
					} else {
						orderDetailDTO.setSellOrgCode("0" + row[4].trim());
					}
					
					if (row[5].trim().length() == 4) {
						orderDetailDTO.setSellLocation(row[5].trim());
					} else {
						orderDetailDTO.setSellLocation("0" + row[5].trim());
					}
					
					
					orderDetailDTO.setDeliveryMd(row[6].trim());
					orderDetailDTO.setItemGrpCode(row[7].trim());
					orderDetailDTO.setLineType(row[8].trim());
					
					if (row[9].trim().length() >= 4) {
						orderDetailDTO.setShipNodeKey(row[9].trim());
					} else {
						orderDetailDTO.setShipNodeKey("0" + row[9].trim());
					}
					
					
					orderDetailDTO.setExtnSKUCode(row[10].trim());
					orderDetailDTO.setCreateTs(row[11].trim());
					orderDetailDTO.setOrderedQty(row[12].trim());
					orderDetailDTO.setHdpsiKey(row[13].trim());
					orderDetailDTO.setHdpsiStatus(row[14].trim());
					
					
					
					
					
					orderDetailDTOList.add(orderDetailDTO);
				}

				col = false;
			}
			csvReader.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return orderDetailDTOList;
	}


}
