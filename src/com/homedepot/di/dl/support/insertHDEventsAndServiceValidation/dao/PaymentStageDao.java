package com.homedepot.di.dl.support.insertHDEventsAndServiceValidation.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.log4j.Logger;

import com.homedepot.di.dl.support.compareDataReplication.web.CompareDataReplicationServlet;
import com.homedepot.di.dl.support.insertHDEventsAndServiceValidation.dto.OrderDetailDTO;
import com.sun.jmx.snmp.Timestamp;

public class PaymentStageDao {

	final static Logger logger = Logger.getLogger(PaymentStageDao.class);
	
	final String driverClass = "oracle.jdbc.driver.OracleDriver";
	final String connectionURL = "jdbc:oracle:thin:@//pprmm78x.homedepot.com:1521/dpr77mm_sro01";
	final String uName = "MMUSR01";
	final String uPassword = "COMS_MMUSR01";

	final String driverClassRW = "oracle.jdbc.driver.OracleDriver";
	final String connectionURLRW = "jdbc:oracle:thin:@//pprmm77x.homedepot.com:1521/dpr77mm_srw02";
	final String uNameRW = "THD03";
	final String uPasswordRW = "ra1nySumm3r";
	
	
	
	public OrderDetailDTO retrieveHDlinestatusEffeTs(String orderLineKey) {
		Connection sterCon = null;
		logger.info("before opening select connection " + new Timestamp(System.currentTimeMillis()));
		Statement stmt1 = null;
		OrderDetailDTO hdLineDTO = new OrderDetailDTO();
		String hdLineEffTs = "select q.status_effective_ts quotets,d.status_effective_ts statusts from thd01.hd_line_status q,THD01.hd_line_status d where q.status_code=100 and  d.status_code=1000 and"
				+ " q.order_line_key=d.order_line_key and q.order_line_key='"+orderLineKey+"'";		
		try {
			Class.forName(driverClass);
			// Sterling connection opens here
			sterCon = DriverManager.getConnection(connectionURL, uName,
					uPassword);

			stmt1 = null;
			stmt1 = sterCon.createStatement();
			ResultSet result1 = null;
			logger.info("Running query: " + hdLineEffTs);
			result1 = stmt1.executeQuery(hdLineEffTs);
			while (result1.next()) {
				//OrderDetailDTO hdLineDTO = new OrderDetailDTO();
			hdLineDTO.setQuoteEffts(result1.getString(1));
			hdLineDTO.setDoneEffts(result1.getString(2));
			}
			logger.info("Quote Date:::"+hdLineDTO.getQuoteEffts()+":: Donedate"+hdLineDTO.getDoneEffts());
						stmt1.close();
			sterCon.close();
			result1.close();
		} catch (Exception e) {
			e.printStackTrace();
			if (sterCon != null) {
				try {
					sterCon.close();
				} catch (SQLException e1) {
					e.printStackTrace();
				}
			}
		}finally {

			if (sterCon != null) {
				try {
					sterCon.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return hdLineDTO;
	}

	
	
	
	
	public void updateHDPSIStatus(String HDPSIKey) {
		Connection sterCon = null;
		logger.info("before opening a connection "
				+ new Timestamp(System.currentTimeMillis()));
		Statement stmt1 = null;
		//BossSkuFixDTO bossSkuFixDTO = null;
		String bossSkuMismatch = "update thd01.hd_payment_stage_info set status='0',modifyts=sysdate, modifyuserid='ProdRepOrdScriptHDPSI' where HD_PAYMENT_STAGE_INFO_KEY='"
				+ HDPSIKey + "'";
		
		
		try {
			Class.forName(driverClassRW);
			// Sterling connection opens here
			sterCon = DriverManager.getConnection(connectionURLRW, uNameRW,
					uPasswordRW);

			stmt1 = null;
			stmt1 = sterCon.createStatement();
			int result1 = 0;

			result1 = stmt1.executeUpdate(bossSkuMismatch);
			logger.info("Updated HDPSI key to status 0:: " + HDPSIKey+ "::updated rows:"+result1);
			sterCon.commit();

			stmt1.close();

			sterCon.close();

		} catch (Exception e) {
			e.printStackTrace();
			if (sterCon != null) {
				try {
					sterCon.close();
				} catch (SQLException e1) {
					e.printStackTrace();
				}
			}
		}finally {

			if (sterCon != null) {
				try {
					sterCon.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}

	}
	
	public OrderDetailDTO retrieveVEventsEffeTs(String orderLineKey) {
		Connection sterCon = null;
		logger.info("before opening select connection "
				+ new Timestamp(System.currentTimeMillis()));
		Statement stmt1 = null;
		OrderDetailDTO hdLineDTO = new OrderDetailDTO();
		String hdLineEffTs = "select distinct event_ts + interval '2' minute from thd01.hd_event where order_line_key='"+orderLineKey+"' and event_trk_code='CO'";
				
		try {
			Class.forName(driverClass);
			// Sterling connection opens here
			sterCon = DriverManager.getConnection(connectionURL, uName,
					uPassword);

			stmt1 = null;
			stmt1 = sterCon.createStatement();
			ResultSet result1 = null;
			logger.info("Running query: " + hdLineEffTs);
			result1 = stmt1.executeQuery(hdLineEffTs);
			while (result1.next()) {
				//OrderDetailDTO hdLineDTO = new OrderDetailDTO();
			hdLineDTO.setvEventts(result1.getString(1));
			}
			logger.info("V events Date:::"+hdLineDTO.getvEventts());
						stmt1.close();
			sterCon.close();
			result1.close();
		} catch (Exception e) {
			e.printStackTrace();
			if (sterCon != null) {
				try {
					sterCon.close();
				} catch (SQLException e1) {
					e.printStackTrace();
				}
			}
		}finally {

			if (sterCon != null) {
				try {
					sterCon.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return hdLineDTO;
	}


	

}
