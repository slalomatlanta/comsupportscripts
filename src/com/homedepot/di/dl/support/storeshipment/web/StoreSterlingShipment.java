package com.homedepot.di.dl.support.storeshipment.web;

import java.io.IOException;
import java.sql.SQLException;
import java.util.GregorianCalendar;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.homedepot.di.dl.support.storeshipment.dao.SterlingShipment;
import com.homedepot.di.dl.support.storeshipment.dao.UpdateShipments;

import org.apache.log4j.Logger;

/**
 * Servlet implementation class StoreSterlingShipment
 */
public class StoreSterlingShipment extends HttpServlet {
	private static final long serialVersionUID = 1L;
	final static Logger logger = Logger.getLogger(StoreSterlingShipment.class);

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public StoreSterlingShipment() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response)  {
		
		logger.info("StoreSterlingServlet Started :: "+GregorianCalendar.getInstance().getTime());
		UpdateShipments ship = new UpdateShipments();
		String inpStore = null;
		if (null != request.getParameter("store")) {
			inpStore = request.getParameter("store");
		}
		try {

			ship.storeSterlingShipment(inpStore);
		} catch (Exception e) {
			logger.error("StoreSterlingServlet ::" + e.getMessage());
			e.printStackTrace();
		}
		
		logger.info("StoreSterlingServlet Completed :: "+GregorianCalendar.getInstance().getTime());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
