package com.homedepot.di.dl.support.storeshipment.dao;

import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeMessage.RecipientType;

import org.apache.log4j.Logger;

import com.homedepot.di.dl.support.storeshipment.dto.SterlingShipmentDTO;
import com.homedepot.di.dl.support.util.DBConnectionUtil;

public class StoreShipmentUpdate 
{
	final static Logger logger = Logger.getLogger(StoreShipmentUpdate.class);
	
	public void storeSterlingShipment(String inpStore)
	{
		ArrayList<String> storeList = new ArrayList<String>();
		ArrayList<String> storeFI = new ArrayList<String>();
		Map<String, ArrayList<String>> storeFIMap = new HashMap<String,ArrayList<String>>();
		ArrayList<SterlingShipmentDTO> sterlingFI = new ArrayList<SterlingShipmentDTO>();
		int fiUpdatedAtStore=1;
		if (null != inpStore && inpStore.length() == 4) {
			storeList.add(inpStore);
		} else {
		
		logger.info("Reading storeList");
		storeList = readStoreList();
		logger.info("Read storeList complete : " + storeList.size());
		}
		logger.info("Reading Open FI from store");
		storeFIMap = checkOpenShipmentsAtStore(storeList);
		logger.info("Reading Open FI from store complete : " + storeFI.size());
		
		logger.info("Checking Open FI status in Sterling");
		sterlingFI = checkShipmentStatusAtSterling(storeFIMap);
		logger.info("Checking Open FI status in Sterling complete : " + sterlingFI.size());
		
		//logger.info("Updating Shipments at store");
		//fiUpdatedAtStore = updateShipments(sterlingFI);
		//logger.info("Updating Shipments at store complete : " + fiUpdatedAtStore);
		
		logger.info("mailing details");
		sendAttachment(fiUpdatedAtStore);
		
	}

	public int updateShipments(ArrayList<SterlingShipmentDTO> sterlingFI) 
	{
		int count = 0;
		Connection constore = null;
		Statement stmt1 = null;
		Statement stmtselect = null;
		Statement stmtupdate = null;
		ResultSet resultstoreSel = null;
		ResultSet resultstoreUpd = null;
		String statusBefore = null;
		String statusAfter = null;

		
		try
		{
		FileWriter writer = new FileWriter("C:\\store\\ShipmentStatus.csv");
		//FileWriter writer = new FileWriter("/opt/isv/apache-tomcat/temp/ShipmentStatus.csv");


		writer.append("OrderNumber");
		writer.append(',');
		writer.append("Sterling Shipment Number");
		writer.append(',');
		writer.append("Sterling ShipmentStatus");
		writer.append(',');
		writer.append("Store Status Old");
		writer.append(',');
		writer.append("Store Status New");
		writer.append(',');
		writer.append("Store Number");
		writer.append(',');

		writer.append('\n');

		for (SterlingShipmentDTO ship : sterlingFI) {

			
				String shipmentNo = ship.getshipmentNumber();
				
				String status = ship.getstatus();
				String shipNode = ship.getshipnode().trim();
				String OrderNo = ship.getOrderNo();
				statusBefore = null;
				
				DBConnectionUtil dbConn = new DBConnectionUtil();
				
				try
				{
				constore = dbConn.getStoreconnection(shipNode);

				stmtselect = constore.createStatement();

				String queryString = "select finstr_Stat_Cd from FINSTR_PCK_LIST where finstr_Stat_Cd < 600 and fulfl_instr_id ='"
						+ shipmentNo + "'";
				resultstoreSel = stmtselect.executeQuery(queryString);

				while (resultstoreSel.next()) {
					// System.out.println("store shipments: "+
					// resultstoreSel.getString(1));

					statusBefore = resultstoreSel.getString(1);
				}

				if (statusBefore != null) {

					if (status.contains("1400")) {
						String Storequery = "UPDATE FINSTR_PCK_LIST SET finstr_Stat_Cd ='600', LAST_UPD_TS=sysdate, LAST_UPD_SYSUSR_ID='COMSupport' where fulfl_instr_id ='"
								+ shipmentNo + "'";

						stmt1 = constore.createStatement();
						stmt1.executeUpdate(Storequery);
						
						if (!constore.getAutoCommit()) 
						{
							constore.commit();
						}

						
					} else if (status.contains("9000")) {
						String Storequery = "UPDATE FINSTR_PCK_LIST SET finstr_Stat_Cd ='700', LAST_UPD_TS=sysdate, LAST_UPD_SYSUSR_ID='COMSupport' where fulfl_instr_id ='"
								+ shipmentNo + "'";

						stmt1 = constore.createStatement();
						stmt1.executeUpdate(Storequery);

						if (!constore.getAutoCommit()) 
						{
							constore.commit();
						}

						

					} 
					
					else
					{
						logger.info("Shipment is open in Sterling");
					}

					String queryString1 = "select finstr_Stat_Cd from FINSTR_PCK_LIST where fulfl_instr_id ='"
							+ shipmentNo + "'";

					stmtupdate = constore.createStatement();
					resultstoreUpd = stmtupdate.executeQuery(queryString1);
					
					while (resultstoreUpd.next()) {
						
						statusAfter = resultstoreUpd.getString(1);
					}
					
					writer.append(OrderNo);
					writer.append(',');
					writer.append(shipmentNo);
					writer.append(',');
					writer.append(status);
					writer.append(',');
					writer.append(statusBefore);
					writer.append(',');
					writer.append(statusAfter);
					writer.append(',');
					writer.append(shipNode);
					writer.append(',');

					writer.append('\n');
					
					
					
					count++;
					
				}
				
		}
		catch(Exception e)
		{
			logger.debug(e);
		}

					}
		writer.flush();
		writer.close();	
		}
		catch(Exception e)
		{
			e.printStackTrace();
			logger.debug(e);
		}
		
		closeConnections(constore,stmt1,null);
		closeConnections(null,stmtselect,resultstoreSel);
		closeConnections(null,stmtupdate,resultstoreUpd);
		
		return count;
	}

	public ArrayList<SterlingShipmentDTO> checkShipmentStatusAtSterling(Map<String, ArrayList<String>> storeFIMap) 
	{
		
		ArrayList<SterlingShipmentDTO> sterlingFI = new ArrayList<SterlingShipmentDTO>();
		ArrayList<String> storeFI = new ArrayList<String>();
		ArrayList<String> sterFI = new ArrayList<String>();
		String store;
		Connection sscConn = null;
		Statement stmt = null;
		ResultSet rset = null;
		
		
		
		try {
			
		FileWriter writer = new FileWriter("C:\\store\\SterlingShipment.csv");
		
		FileWriter writer1 = new FileWriter("C:\\store\\ShipmentsNotInSterling.csv");
		//FileWriter writer = new FileWriter("/opt/isv/apache-tomcat/temp/SterlingShipment.csv");

		writer.append("Shipment Number");
		writer.append(',');
		writer.append("ShipmentStatus");
		writer.append(',');
		writer.append("Store Number");
		writer.append(',');
		writer.append("Order Number");
		writer.append('\n');
		
		
		writer1.append("Shipment Number");
		writer1.append(',');
		writer1.append("Store Number");
		writer1.append('\n');

		String shipmentNumber = null;
		String status = null;
		String shipnode = null;
		String OrderNo = null;
		
			final String url = "jdbc:oracle:thin://@aprbgor12-scan.homedepot.com:1521/dpr77mm_sro01";

			final String uName = "MMUSR01";
			final String uPassword = "COMS_MMUSR01";
			final String driverClass = "oracle.jdbc.driver.OracleDriver";
			Class.forName(driverClass);

			sscConn = DriverManager.getConnection(url, uName, uPassword);


			for (Entry<String, ArrayList<String>> entry : storeFIMap.entrySet()) 
				
			{
			    sterFI.clear();
			    store = entry.getKey();
			    storeFI = storeFIMap.get(store);
			    
			    System.out.println(store + " : " + storeFI.size());
			
			    
			
			StringBuffer buffer = new StringBuffer();
			
			
			
			
			for (String shipmentID : storeFI) {

				
				buffer.append("('" + shipmentID + "','"+ store + "'), ");
				
				}
			
			String shipmentIDs = buffer.toString().substring(0,buffer.length() - 2);
			
			//for (String shipmentIDs : shipmentList) {
				
				
				String queryString = "select distinct ys.shipment_no,ys.status,ys.shipnode_key,yoh.extn_host_order_ref "
						+ "from THD01.yfs_shipment_line ysl,thd01.yfs_shipment ys ,thd01.yfs_order_header yoh where (ys.shipment_no,ys.shipnode_key) in ("
						+ shipmentIDs
						+ ")"
						+ "and ys.shipment_key=ysl.shipment_key  and yoh.order_no=ysl.order_no "
						+ "and yoh.order_header_key=ysl.order_header_key";

				//System.out.println(queryString);
				stmt = sscConn.createStatement();
				rset = stmt.executeQuery(queryString);
								
				while (rset.next()) {
					
					SterlingShipmentDTO shipmentDTO = new SterlingShipmentDTO();
					
					shipmentNumber = rset.getString(1);
					status = rset.getString(2);
					shipnode = rset.getString(3);
					OrderNo = rset.getString(4);
					
					sterFI.add(shipmentNumber.trim());
					
					shipmentDTO.setshipmentNumber(shipmentNumber);
					shipmentDTO.setstatus(status);
					shipmentDTO.setshipnode(shipnode);
					shipmentDTO.setOrderNo(OrderNo);
					
					sterlingFI.add(shipmentDTO);

					writer.append(rset.getString(1));
					writer.append(',');
					writer.append(rset.getString(2));
					writer.append(',');
					writer.append(rset.getString(3));
					writer.append(',');
					writer.append(rset.getString(4));
					writer.append(',');
					writer.append('\n');
					
				}
				
				System.out.println(storeFI.size() + " , " + sterFI.size());
				//System.out.println(sterFI.size());
				storeFI.removeAll(sterFI);
				//System.out.println(storeFI.removeAll(sterFI));
				//System.out.println(storeFI.size());
				
				if(storeFI.size() > 0)
				{
					
					for(String storeList : storeFI)
					{
						writer1.append(storeList);
						writer1.append(',');
						writer1.append(store);
						writer1.append('\n');
						
					}
					
				}
				
				else
				{
					logger.info(store +" :All the Shipments in store are available in sterling");
				}
			//}
			
			
			}
			writer.flush();
			writer.close();
			writer1.flush();
			writer1.close();

		} catch (Exception e) {

			e.printStackTrace();
			logger.error(e);

		}
		
		
		closeConnections(sscConn,stmt,rset);
		return sterlingFI;

	}

	public Map<String, ArrayList<String>> checkOpenShipmentsAtStore(ArrayList<String> storeList) 
	{
		
		Map<String, ArrayList<String>> storeFIMap = new HashMap<String,ArrayList<String>>();
		Statement stmtStore = null;
		ResultSet resultStore = null;
		Connection conStore = null;
		
		try
		{
		FileWriter writer1 = new FileWriter("C:\\store\\StoreShipment.csv");
		//FileWriter writer1 = new FileWriter("/opt/isv/apache-tomcat/temp/StoreShipment.csv");
		writer1.append("ShipmentId");
		writer1.append(',');
		writer1.append("Store");
		writer1.append(',');
		writer1.append("StoreStatus");
		writer1.append(',');
		writer1.append("Last_upd_ts");
		writer1.append('\n');

		for (String store : storeList) {
			// get the shipments from stores older than 2 months
			ArrayList<String> storeFI = new ArrayList<String>();
						
				{
					
					DBConnectionUtil dbConn = new DBConnectionUtil();
					
					try{
				
					conStore = dbConn.getStoreconnection(store);
					
					stmtStore = conStore.createStatement();

					String queryString = "select fulfl_instr_id,finstr_Stat_Cd,last_upd_ts from FINSTR_PCK_LIST WHERE finstr_Stat_Cd < 600 and date(LAST_UPD_TS)<date(current)";
					resultStore = stmtStore.executeQuery(queryString);

					// System.out.println(queryString);

					
					while (resultStore.next()) {
						// System.out.println("store shipments: "+
						// resultstore.getString(1));

						writer1.append(resultStore.getString(1));
						storeFI.add(resultStore.getString(1));
						writer1.append(',');
						writer1.append(store);
						writer1.append(',');
						writer1.append(resultStore.getString(2));
						writer1.append(',');
						writer1.append(resultStore.getString(3));
						writer1.append('\n');
						

					}
					// System.out.println("rescheck : " + rescheck);
					storeFIMap.put(store,storeFI);
				}
					catch(Exception e)
					{
						logger.info(e);
					}
					
					
				}
				
				
			
				//System.out.println(storeFIMap);
		}
		writer1.flush();
		writer1.close();
		}
		catch(Exception e)
		{
			logger.debug(e);
			e.printStackTrace();
		}
		
		closeConnections(conStore,stmtStore,resultStore);
		return storeFIMap;
	}

	private ArrayList<String> readStoreList() {
		// TODO Auto-generated method stub
		ArrayList<String> storeList = new ArrayList<String>();
		
		final String driverClass = "oracle.jdbc.driver.OracleDriver";
		final String connectionURLThin = "jdbc:oracle:thin:@spragor10-scan.homedepot.com:1521/dpr78mm_sro01";
		//jdbc:oracle:thin:@pprmm77x.homedepot.com:1521/dpr78mm_srw02- ATC
		final String userID = "MMUSR01";
		final String userPassword = "COMS_MMUSR01";
		
		String getStore = "select distinct loc_nbr from THD01.comt_loc_capbl where loc_nbr not in ('0387','6873','3704','8119','0682') and loc_nbr not like '9%' and comt_capbl_id='10' order by loc_nbr";
		Connection comCon = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			Class.forName(driverClass);
		
		
		comCon = DriverManager.getConnection(connectionURLThin, userID,
				userPassword);
		
		stmt= comCon.createStatement();
		rs = stmt.executeQuery(getStore);
		
		while(rs.next())
		{
			storeList.add(rs.getString(1));
		}
		
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.debug(e);
		}
		
		closeConnections(comCon,stmt,rs);
		return storeList;
	}
	

	public void closeConnections(Connection comCon, Statement stmt,
			ResultSet rs) {
		// TODO Auto-generated method stub
		
		
	    
	        try {
				if (null != rs)
				{
				    rs.close();
				}
				if (null != stmt)
				{
				    stmt.close();
				}
				if (null != comCon)
				{
					comCon.close();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				logger.debug(e);
			}
	    
		
	}

	
	public static void sendAttachment(int count) {

		try {
			String host = "mail1.homedepot.com";
			String from = "horizon@cpliisad.homedepot.com";
			String[] to = { "mohanraj_gurusamy@homedepot.com" };
			//String[] to = { "_2fc77b@homedepot.com" };
			//String[] Cc = { "_275769@homedepot.com" };
			// String[] to = {"Anandhakumar_Chinnadurai@homedepot.com"};
			// String[] Cc ={"mohanraj_gurusamy@homedepot.com"};
			// Get system properties
			Properties properties = System.getProperties();

			// Setup mail server
			properties.setProperty("mail.smtp.host", host);

			// Get the default Session object.
			Session session = Session.getDefaultInstance(properties);

			// Define message
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(from));
			InternetAddress[] addressTo = new InternetAddress[to.length];
			//InternetAddress[] addressCc = new InternetAddress[Cc.length];
			for (int i = 0; i < to.length; i++) {
				addressTo[i] = new InternetAddress(to[i]);
				logger.debug("To Address " + to[i]);
			}
			/*for (int j = 0; j < Cc.length; j++) {
				addressCc[j] = new InternetAddress(Cc[j]);
				logger.debug("Cc Address " + Cc[j]);
			}*/
			message.setRecipients(RecipientType.TO, addressTo);
			//message.setRecipients(RecipientType.CC, addressCc);
			DateFormat dateFormat = new SimpleDateFormat(
					"MM/dd/yyyy hh:mm:ss a");
			// get current date time with Date()
			Date today = new Date();
			String date1 = dateFormat.format(today);
			message.setSubject("Unavailable Shipments in Sterling" + date1);

			// message.setRecipients(RecipientType.CC, Cc);
			BodyPart messageBodyPart = new MimeBodyPart();
			Multipart multipart = new MimeMultipart();

			if (count > 0) {

				String msgContent = "Hi All,<br/><br/>PFA report for shipments which are not available in sterling.<br/>"
						 + "<br/><br/>Thanks<br/>";
				msgContent = msgContent + "COM Multichannel Support";

				messageBodyPart.setContent(msgContent, "text/html");

				multipart.addBodyPart(messageBodyPart);

				// Part two is attachment
				messageBodyPart = new MimeBodyPart();
				String filename = "C:\\store\\ShipmentsNotInSterling.csv";

				//String filename = "C:\\store\\ShipmentStatus.csv";
				//String filename = "/opt/isv/apache-tomcat/temp/ShipmentStatus.csv";
				DataSource source = new FileDataSource(filename);
				messageBodyPart.setDataHandler(new DataHandler(source));
				messageBodyPart.setFileName(source.getName());

				multipart.addBodyPart(messageBodyPart);
			} else {
				String msgContent = "Hi All,<br/><br/>Today, there was no shipment found to update. "
						+ "<br/><br/>Thanks<br/>";
				msgContent = msgContent + "COM Multichannel Support";

				messageBodyPart.setContent(msgContent, "text/html");

				multipart.addBodyPart(messageBodyPart);
			}

			// Put parts in message
			message.setContent(multipart);

			// Send the message
			Transport.send(message);

		} catch (Exception e) {
			e.printStackTrace();
			logger.info(e);
		}

	}
}
