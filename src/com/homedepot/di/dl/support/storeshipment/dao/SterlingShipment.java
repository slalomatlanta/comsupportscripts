package com.homedepot.di.dl.support.storeshipment.dao;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.ResourceBundle;
import org.apache.log4j.Logger;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage.RecipientType;
import javax.mail.internet.MimeMultipart;

import com.homedepot.di.dl.support.storeshipment.dto.SterlingShipmentDTO;
import com.homedepot.di.dl.support.util.Constants;
import com.homedepot.di.dl.support.util.DBConnectionUtil;

public class SterlingShipment {

	final static Logger logger = Logger.getLogger(SterlingShipment.class);

	/*
	 * public static void main(String[] args) throws SQLException, IOException,
	 * Exception {
	 * 
	 * shipmentAdjustment(String store); }
	 */

	public static void shipmentAdjustment(String inpStore) throws Exception {

		List<String> storeList = new ArrayList<String>();
		List<String> FinsterList = new ArrayList<String>();
		List<SterlingShipmentDTO> ShipmentList = new ArrayList<SterlingShipmentDTO>();
		List<SterlingShipmentDTO> ShipmentgetList = new ArrayList<SterlingShipmentDTO>();

		// String store;
		if (null != inpStore && inpStore.length() == 4) {
			storeList.add(inpStore);
		} else {

			try {

				final String driverClass = "oracle.jdbc.driver.OracleDriver";
				final String connectionURLThin = "jdbc:oracle:thin:@spragor10-scan.homedepot.com:1521/dpr78mm_sro01";
				//jdbc:oracle:thin:@pprmm77x.homedepot.com:1521/dpr78mm_srw02- ATC
				final String userID = "MMUSR01";
				final String userPassword = "COMS_MMUSR01";
				Class.forName(driverClass);
				Connection mysql = null;
				mysql = DriverManager.getConnection(connectionURLThin, userID,
						userPassword);
				

				

				/*
				 * String driverClass1 = "com.mysql.jdbc.Driver"; final String
				 * connectionURL =
				 * "jdbc:mysql://cpliisad.homedepot.com:3306/multichannel_PR";
				 * final String uName = "COMSupport"; final String uPassword =
				 * "su990rt";
				 * 
				 * Connection con1 = null; Class.forName(driverClass1); con1 =
				 * DriverManager.getConnection(connectionURL, uName, uPassword);
				 */
				// System.out.println("hi");

				Statement stmt1 = null;
				stmt1 = mysql.createStatement();
				ResultSet rs = stmt1
						.executeQuery("select distinct loc_nbr from THD01.comt_loc_capbl where loc_nbr not in ('0387','6873','3704','8119','0682') and loc_nbr not like '9%' and comt_capbl_id='10' order by loc_nbr");

				while (rs.next()) {

					storeList.add(rs.getString(1).trim());

					// System.out.println(storeList);

				}
				mysql.close();

			} catch (Exception e) {
				e.printStackTrace();
				throw e;
			}

		}

		/*
		 * storeList.add("0121"); storeList.add("0143"); storeList.add("0122");
		 * storeList.add("6996"); storeList.add("0105");
		 */

		// String strFile = "C:\\store\\storeconnection.csv";
		// String strFile = "/opt/isv/tomcat-6.0.18/temp/storeconnection.csv";

		// BufferedReader br = new BufferedReader(new FileReader(strFile));

		FileWriter writer1 = new FileWriter("C:\\store\\StoreShipment.csv");
		//FileWriter writer1 = new FileWriter("/opt/isv/apache-tomcat/temp/StoreShipment.csv");
		writer1.append("ShipmentId");
		writer1.append(',');
		writer1.append("Store");
		writer1.append(',');
		writer1.append("StoreStatus");
		writer1.append(',');
		writer1.append("Last_upd_ts");
		writer1.append('\n');

		for (String store : storeList) {
			// get the shipments from stores older than 2 months
			try {

				logger.info("Stores:" + store);

				Statement stmtstore = null;
				ResultSet resultstore = null;

				String FinsterId = null;

				{
				
					SterlingShipment ikey = new SterlingShipment();
					Connection constore = null;
					DBConnectionUtil dbConn = new DBConnectionUtil();
					constore = dbConn.getStoreconnection(store);
					stmtstore = constore.createStatement();

					String queryString = "select fulfl_instr_id,finstr_Stat_Cd,last_upd_ts from FINSTR_PCK_LIST WHERE finstr_Stat_Cd < 600 and date(LAST_UPD_TS)<date(current)";
					resultstore = stmtstore.executeQuery(queryString);

					// System.out.println(queryString);

					boolean rescheck = false;
					while (resultstore.next()) {
						// System.out.println("store shipments: "+
						// resultstore.getString(1));

						writer1.append(resultstore.getString(1));
						FinsterList.add(resultstore.getString(1));
						writer1.append(',');
						writer1.append(store);
						writer1.append(',');
						writer1.append(resultstore.getString(2));
						writer1.append(',');
						writer1.append(resultstore.getString(3));
						writer1.append('\n');
						rescheck = true;

					}
					// System.out.println("rescheck : " + rescheck);

					constore.close();
				}
			} catch (IOException e) {
				logger.error("Exception while reading csv file: " + e);
			} catch (SQLException e) {
				logger.error("SQLException: " + e);
			}
		}
		writer1.flush();
		writer1.close();
		logger.info("Store part Program completed");

		// get the sterling shipments from sterling for 1400 and 9000 status
		try {

			FileWriter writer = new FileWriter("C:\\store\\SterlingShipment.csv");
			//FileWriter writer = new FileWriter("/opt/isv/apache-tomcat/temp/SterlingShipment.csv");

			writer.append("Shipment Number");
			writer.append(',');
			writer.append("ShipmentStatus");
			writer.append(',');
			writer.append("Store Number");
			writer.append(',');
			writer.append("Order Number");
			writer.append('\n');

			StringBuilder emailText = new StringBuilder();
			String shipmentNumber = null;
			String status = null;
			String shipnode = null;
			String OrderNo = null;
			try {
				/*
				 * SterlingShipment ikey = new SterlingShipment(); Connection
				 * sterCon = ikey .getConnection("sterlingConnectionURL_SSC");
				 */

				final String url = "jdbc:oracle:thin://@apragor11-scan.homedepot.com:1521/dpr77mm_sro01";

				final String uName = "MMUSR01";
				final String uPassword = "COMS_MMUSR01";
				final String driverClass = "oracle.jdbc.driver.OracleDriver";
				Class.forName(driverClass);

				Connection sscConn = DriverManager.getConnection(url, uName, uPassword);

				

				emailText.append(System.getProperty("line.separator"));

				Statement stmt = null;
				ResultSet rset = null;

				int size = 990, counter = 1;
				StringBuffer buffer = new StringBuffer();
				List<String> FIList = new ArrayList<String>();

				if (size > FinsterList.size()) {
					size = FinsterList.size();
				}

				int j = 1;
				for (String StFinsterId : FinsterList) {

					if (counter < size) {
						buffer.append("'" + StFinsterId + "', ");
						counter++;

					} else if (counter == size) {
						buffer.append("'" + StFinsterId + "' ");
						FIList.add(buffer.toString());
						counter = 1;
						buffer = new StringBuffer();
					}
					if (j == FinsterList.size() && buffer.length() > 1) {

						FIList.add((buffer.toString()).substring(0,
								buffer.length() - 2));
						buffer = new StringBuffer();
					}
					j++;
				}
				
				for (String StFinsterId : FIList) {

					// System.out.println("StFinsterId :" + StFinsterId);

					/*
					 * String queryString =
					 * " select shipment_no,status,shipnode_key from yfs_shipment where shipment_no in ("
					 * + StFinsterId + ")" + " and status in ('1400','9000') ";
					 */
					
					String queryString = "select distinct ys.shipment_no,ys.status,ys.shipnode_key,yoh.extn_host_order_ref "
							+ "from THD01.yfs_shipment_line ysl,thd01.yfs_shipment ys ,thd01.yfs_order_header yoh where shipment_no in ("
							+ StFinsterId
							+ ")"
							+ " and status in ('1400','9000') "
							+ "and ys.shipment_key=ysl.shipment_key  and yoh.order_no=ysl.order_no "
							+ "and yoh.order_header_key=ysl.order_header_key";

					stmt = sscConn.createStatement();
					rset = stmt.executeQuery(queryString);
					System.out.println(queryString);
					// System.out.println(queryString);

					boolean rescheck = false;
					while (rset.next()) {
						// System.out.println("Sterling Status: "+
						// rset.getString(1));
						SterlingShipmentDTO ShipmentId1 = new SterlingShipmentDTO();

						shipmentNumber = rset.getString(1);
						status = rset.getString(2);
						shipnode = rset.getString(3);
						OrderNo = rset.getString(4);

						ShipmentId1.setshipmentNumber(shipmentNumber);
						ShipmentId1.setstatus(status);
						ShipmentId1.setshipnode(shipnode);
						ShipmentId1.setOrderNo(OrderNo);
						ShipmentList.add(ShipmentId1);

						writer.append(rset.getString(1));
						writer.append(',');
						writer.append(rset.getString(2));
						writer.append(',');
						writer.append(rset.getString(3));
						writer.append(',');
						writer.append(rset.getString(4));
						writer.append(',');
						writer.append('\n');
						rescheck = true;
					}
					// System.out.println("rescheck : " + rescheck);

				}
				sscConn.close();

			} catch (Exception e) {

				logger.error("Getting ShipmentNumber" + e.getMessage());

			}
			writer.flush();
			writer.close();
			logger.info("Sterling part Program completed");

		} catch (IOException e) {
			logger.error("Exception while reading csv file: " + e.getMessage());
		} catch (Exception e) {
			logger.error("SQLException: " + e.getMessage());

		}

		int count = 0;
		Connection constore = null;
		Statement stmt1 = null;
		Statement stmtselect = null;
		Statement stmtupdate = null;
		ResultSet resultstoreSel = null;
		ResultSet resultstoreUpd = null;
		String StoreOld = null;
		String StoreNew = null;

		FileWriter writer = new FileWriter("C:\\store\\ShipmentStatus.csv");
		//FileWriter writer = new FileWriter("/opt/isv/apache-tomcat/temp/ShipmentStatus.csv");

		// updating the shipment status at store to sync with sterling

		writer.append("OrderNumber");
		writer.append(',');
		writer.append("Sterling Shipment Number");
		writer.append(',');
		writer.append("Sterling ShipmentStatus");
		writer.append(',');
		writer.append("Store Status Old");
		writer.append(',');
		writer.append("Store Status New");
		writer.append(',');
		writer.append("Store Number");
		writer.append(',');

		writer.append('\n');

		for (SterlingShipmentDTO ship : ShipmentList) {

			try {
				String shipmentNo = ship.getshipmentNumber();
				String status = ship.getstatus();
				String shipNode = ship.getshipnode().trim();
				String OrderNo = ship.getOrderNo();

				DBConnectionUtil dbConn = new DBConnectionUtil();
				constore = dbConn.getStoreconnection(shipNode);

				stmtselect = constore.createStatement();

				String queryString = "select finstr_Stat_Cd from FINSTR_PCK_LIST where finstr_Stat_Cd < 600 and fulfl_instr_id ='"
						+ shipmentNo + "'";
				resultstoreSel = stmtselect.executeQuery(queryString);

				while (resultstoreSel.next()) {
					// System.out.println("store shipments: "+
					// resultstoreSel.getString(1));

					StoreOld = resultstoreSel.getString(1);
				}

				if (StoreOld != null) {

					if (status.contains("1400")) {
						String Storequery = "UPDATE FINSTR_PCK_LIST SET finstr_Stat_Cd ='600', LAST_UPD_TS=sysdate, LAST_UPD_SYSUSR_ID='COMSupport' where fulfl_instr_id ='"
								+ shipmentNo + "'";

						stmt1 = constore.createStatement();
						stmt1.executeUpdate(Storequery);
						// con1.commit();
						//constore.commit();
						if (!constore.getAutoCommit()) 
						{
							constore.commit();
						}

						// System.out.println(Storequery);
					} else if (status.contains("9000")) {
						String Storequery = "UPDATE FINSTR_PCK_LIST SET finstr_Stat_Cd ='700', LAST_UPD_TS=sysdate, LAST_UPD_SYSUSR_ID='COMSupport' where fulfl_instr_id ='"
								+ shipmentNo + "'";

						stmt1 = constore.createStatement();
						stmt1.executeUpdate(Storequery);
						// con1.commit();
						if (!constore.getAutoCommit()) 
						{
							constore.commit();
						}

						// System.out.println(Storequery);

					} else {
						logger.info("Other status");
					}

					String queryString1 = "select finstr_Stat_Cd from FINSTR_PCK_LIST where fulfl_instr_id ='"
							+ shipmentNo + "'";

					stmtupdate = constore.createStatement();
					resultstoreUpd = stmtupdate.executeQuery(queryString1);
					
					while (resultstoreUpd.next()) {
						// System.out.println("store shipments: "+
						// resultstore.getString(1));

						StoreNew = resultstoreUpd.getString(1);
					}
					
					writer.append(OrderNo);
					writer.append(',');
					writer.append(shipmentNo);
					writer.append(',');
					writer.append(status);
					writer.append(',');
					writer.append(StoreOld);
					writer.append(',');
					writer.append(StoreNew);
					writer.append(',');
					writer.append(shipNode);
					writer.append(',');

					writer.append('\n');
					
					// to get the count of rows updated
					StoreOld = null;
					count++;
					
				}
				logger.info("Store Update part Complete");

			}

			catch (SQLException e) {
				logger.error("SQLException: " + e.getMessage());
			} finally {
				
					stmt1.close();
					stmtupdate.close();
					stmtselect.close();

				
				if (constore != null) {
					constore.close();
				}

			}
		}
		writer.close();
		logger.info("Program completed finally");

		sendAttachment(count);

	}

	/*
	 * public Connection getConnection1(String connectionURLThin) throws
	 * Exception { Connection cons1 = null;
	 * 
	 * try { ResourceBundle bundle = getBundle();
	 * 
	 * final String storeDriverClass = bundle .getString("informixDriverClass");
	 * final String connectionURL = connectionURLThin; final String uName =
	 * bundle.getString("StoreUserID"); final String uPassword =
	 * bundle.getString("StoreUserPassword"); Class.forName(storeDriverClass);
	 * 
	 * cons1 = DriverManager .getConnection(connectionURL, uName, uPassword);
	 * 
	 * 
	 * } catch (Exception e) { System.out.println("Exception while creating " +
	 * e); throw e; } return cons1; }
	 */

	public static void sendAttachment(int count) throws Exception {

		try {
			String host = "mail1.homedepot.com";
			String from = "horizon@cpliisad.homedepot.com";
			String[] to = { "_2fc77b@homedepot.com" };
			//String[] to = { "mohanraj_gurusamy@homedepot.com" };
			String[] Cc = { "_275769@homedepot.com" };
			// String[] to = {"Anandhakumar_Chinnadurai@homedepot.com"};
			// String[] Cc ={"mohanraj_gurusamy@homedepot.com"};
			// Get system properties
			Properties properties = System.getProperties();

			// Setup mail server
			properties.setProperty("mail.smtp.host", host);

			// Get the default Session object.
			Session session = Session.getDefaultInstance(properties);

			// Define message
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(from));
			InternetAddress[] addressTo = new InternetAddress[to.length];
			InternetAddress[] addressCc = new InternetAddress[Cc.length];
			for (int i = 0; i < to.length; i++) {
				addressTo[i] = new InternetAddress(to[i]);
				logger.debug("To Address " + to[i]);
			}
			for (int j = 0; j < Cc.length; j++) {
				addressCc[j] = new InternetAddress(Cc[j]);
				logger.debug("Cc Address " + Cc[j]);
			}
			message.setRecipients(RecipientType.TO, addressTo);
			message.setRecipients(RecipientType.CC, addressCc);
			DateFormat dateFormat = new SimpleDateFormat(
					"MM/dd/yyyy hh:mm:ss a");
			// get current date time with Date()
			Date today = new Date();
			String date1 = dateFormat.format(today);
			message.setSubject("Updating the shipment status at store " + date1);

			// message.setRecipients(RecipientType.CC, Cc);
			BodyPart messageBodyPart = new MimeBodyPart();
			Multipart multipart = new MimeMultipart();

			if (count > 0) {

				String msgContent = "Hi All,<br/><br/>PFA report for shipments which were updated today.<br/>Number of rows updated: "
						+ count + "<br/><br/>Thanks<br/>";
				msgContent = msgContent + "COM Multichannel Support";

				messageBodyPart.setContent(msgContent, "text/html");

				multipart.addBodyPart(messageBodyPart);

				// Part two is attachment
				messageBodyPart = new MimeBodyPart();
				// String filename = "C:\\store\\SterlingShipment.csv";

				String filename = "C:\\store\\ShipmentStatus.csv";
				//String filename = "/opt/isv/apache-tomcat/temp/ShipmentStatus.csv";
				DataSource source = new FileDataSource(filename);
				messageBodyPart.setDataHandler(new DataHandler(source));
				messageBodyPart.setFileName(source.getName());

				multipart.addBodyPart(messageBodyPart);
			} else {
				String msgContent = "Hi All,<br/><br/>Today, there was no shipment found to update. "
						+ "<br/><br/>Thanks<br/>";
				msgContent = msgContent + "COM Multichannel Support";

				messageBodyPart.setContent(msgContent, "text/html");

				multipart.addBodyPart(messageBodyPart);
			}

			// Put parts in message
			message.setContent(multipart);

			// Send the message
			Transport.send(message);

		} catch (MessagingException e) {
			e.printStackTrace();
		}

	}

	/*
	 * private static ResourceBundle getBundle() {
	 * 
	 * // File file = new File("C:\\Store");
	 * 
	 * File file = new File("/opt/isv/tomcat-6.0.18/webapps/ConnProperties");
	 * 
	 * ResourceBundle bundle = null;
	 * 
	 * try { URL[] urls = { file.toURI().toURL() }; ClassLoader loader = new
	 * URLClassLoader(urls); bundle = ResourceBundle.getBundle("DBConnection",
	 * Locale.getDefault(), loader); } catch (MalformedURLException e) {
	 * System.out.println(new
	 * Date()+" Error in ShipmentUpdate's getBundle() :"+e);
	 * e.printStackTrace(); } return bundle;
	 * 
	 * }
	 */
	/*
	 * to add the recipients in CC private static java.lang.String getEmail1()
	 * throws Exception {
	 * 
	 * File file = new File("C:\\Store");
	 * 
	 * // File file = new //
	 * File("/opt/isv/tomcat-6.0.18/webapps/ConnProperties");
	 * 
	 * URL[] urls = { file.toURI().toURL() }; ClassLoader loader = new
	 * URLClassLoader(urls); ResourceBundle bundle =
	 * ResourceBundle.getBundle("DBConnection", Locale.getDefault(), loader);
	 * 
	 * 
	 * String emailCOM1 = bundle.getString("Test1");
	 * System.out.println("sent email to : " + emailCOM1);
	 * 
	 * return emailCOM1; }
	 */

}
