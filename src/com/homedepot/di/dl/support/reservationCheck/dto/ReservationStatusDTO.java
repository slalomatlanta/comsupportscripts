package com.homedepot.di.dl.support.reservationCheck.dto;

public class ReservationStatusDTO 
{

	
	public String response;
	public String[] mailID;

	public String[] getMailID() {
		return mailID;
	}

	public void setMailID(String[] mailID) {
		this.mailID = mailID;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	
}
