package com.homedepot.di.dl.support.reservationCheck.web;

import java.io.IOException;
import java.util.GregorianCalendar;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.homedepot.di.dl.support.reservationCheck.dao.ComReservationMode;

/**
 * Servlet implementation class ComReservationModeServlet
 */
public class ComReservationModeServlet extends HttpServlet {
	
	final static Logger logger = Logger.getLogger(ComReservationModeServlet.class);
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ComReservationModeServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		logger.info("COMReservation Status Check Started : "+ GregorianCalendar.getInstance().getTime());
		
		ComReservationMode reservation = new ComReservationMode();
		try {
			reservation.getReservtaionStatus();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		logger.info("COMReservation Status Check Completed : "+ GregorianCalendar.getInstance().getTime());
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
