package com.homedepot.di.dl.support.reservationCheck.dao;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.Properties;

import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeMessage.RecipientType;

import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;



import com.homedepot.di.dl.support.reservationCheck.dto.ReservationStatusDTO;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;

public class ComReservationMode 
{
	
	private static Client client = null;
	final static Logger logger = Logger.getLogger(ComReservationMode.class);
	public static void getReservtaionStatus() throws Exception
	{
		
		
		String mail="<html>"
				+ "<body>";
				
		String[] dl = {"_2fc77b@homedepot.com"};
		ReservationStatusDTO dto = new ReservationStatusDTO();
		JSONObject jsonObject = new JSONObject();
		JSONObject json;
		try {
			json = readJsonFromUrl("http://webapps.homedepot.com/COMReservation/service/control/getParameter?output=json");
			jsonObject.putOpt("COMReservationMode",
					json.getString("COMReservationMode"));
			String status = json.getString("COMReservationMode");
			logger.info("COMReservation status : " + status);
			if (status.contains("OFFLINE")) 
			{
				dto = getPrimaryDetails();
				String[] to = dto.getMailID();
				if(dto.getResponse()==null)
            	{
					mail = mail + "<p>Bring COM Reservation Back to ONLINE :  <a href=\"https://multichannel-custom.homedepot.com/COMReservation/service/control/setParameter?name=COMReservationMode&value=ONLINE\">Click Here</a></p><br/>Thanks,<br/>COM Multichannel Support</body></html>" ;
					sendAttachment(to,status,true,mail);
					
              	}
				
				else
				{
					mail = mail + "<p>Bring COM Reservation Back to ONLINE :  <a href=\"https://multichannel-custom.homedepot.com/COMReservation/service/control/setParameter?name=COMReservationMode&value=ONLINE\">Click Here</a></p><br/>Thanks,<br/>COM Multichannel Support</body></html>" ;
					sendAttachment(to,status,true,mail);
					sendAttachment(dl,status,false,mail);
					
				}
				
				
				
			} 
			else if (status.contains("SYNC"))
			{
				dto = getPrimaryDetails();
				String[] to = dto.getMailID();
				
				if(dto.getResponse()==null)
            	{
					mail = mail + "<br/><br/>Thanks,<br/>COM Multichannel Support</body></html>" ;
              	}
				
				else
				{
					mail = mail + "<br/><br/>Thanks,<br/>COM Multichannel Support</body></html>" ;
				}
				
				sendAttachment(to,status,false,mail);
				
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.debug(e);
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			logger.debug(e);
			e.printStackTrace();
		}
	}
	
	
	public static JSONObject readJsonFromUrl(String url)  {
		

JSONObject json = null;
InputStream is = null;
try {
	is = new URL(url).openStream();
	BufferedReader rd = new BufferedReader(new InputStreamReader(is,
			Charset.forName("UTF-8")));
	String jsonText = readAll(rd);
	json = new JSONObject(jsonText);
	
} 
catch (IOException e) {
	// TODO Auto-generated catch block
	logger.debug(e);
	e.printStackTrace();
} catch (JSONException e) {
	// TODO Auto-generated catch block
	logger.debug(e);
	e.printStackTrace();
}finally {
	try {
		is.close();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
}
return json;
}

	private static String readAll(Reader rd) throws IOException {
		StringBuilder sb = new StringBuilder();
		int cp;
		while ((cp = rd.read()) != -1) {
			sb.append((char) cp);
		}
		return sb.toString();
	}
	
public static ReservationStatusDTO getPrimaryDetails() 
	
	{
		
	ReservationStatusDTO dto = new ReservationStatusDTO();
		String[] mailID =  {"_2fc77b@homedepot.com"};
		String response = null;
		
		String url = "http://cpliis6t.homedepot.com:5001/api/groups/ORDER_MANAGEMENT_SUPPORT";
		
		Client client = getClient();

		try {
		WebResource webResource = ((com.sun.jersey.api.client.Client) client)
				.resource(url);

		response = webResource.type("text/plain").get(String.class);
		
		JSONObject xmlJSONObj = new JSONObject(response);
		
	     
		
	       String primary = xmlJSONObj.getString("Primary").toString();
	       
	       int firstIndex = primary.indexOf(";") +1;
	       int lastIndex = primary.length();
	       
	       mailID = new String[] {primary.substring(firstIndex, lastIndex)};
	       
		} catch (Exception e) {
			// TODO Auto-generated catch block
			
		}
		dto.setResponse(response);
		dto.setMailID(mailID);
		
		return dto;
	}

public static Client getClient(){
	if(client == null){
		client = Client.create();
	}
	return client;		
}
	
	public static void sendAttachment(String[] to, String subject, Boolean isHipri, String mail) throws Exception {
		
		String sub = "WARN";
		String  pri = "3";
		
		if(isHipri)
		{
			sub = "CRITICAL";
			pri="1";
		}
		
		String host = "mail1.homedepot.com";
		String from = "horizon@cpliisad.homedepot.com";
		//String[] to = { "mohanraj_gurusamy@homedepot.com" };
		//String[] to ={"_2fc77b@homedepot.com"};
		//String[] Cc ={"RAKESH_J_SATYA@homedepot.com"};

		// Get system properties
		Properties properties = System.getProperties();

		// Setup mail server
		properties.setProperty("mail.smtp.host", host);

		// Get the default Session object.
		Session session = Session.getInstance(properties);

		// Define message
		Message message = new MimeMessage(session);
		message.setHeader("X-Priority", pri);
		message.setFrom(new InternetAddress(from));
		InternetAddress[] addressTo = new InternetAddress[to.length];
		 //InternetAddress[] addressCc = new InternetAddress[Cc.length];
		for (int i = 0; i < to.length; i++) {
			addressTo[i] = new InternetAddress(to[i]);
			
		}
		 /*for (int j = 0; j < Cc.length; j++)
		 { addressCc[j] = new InternetAddress(Cc[j]);
		// System.out.println("Cc Address "+Cc[j]);
		 }*/
		message.setRecipients(RecipientType.TO, addressTo);
		//message.setRecipients(RecipientType.CC, addressCc);
		message.setSubject(sub+" : COM Reservation is in " + subject + " status");

		// Create the message part
		BodyPart messageBodyPart = new MimeBodyPart();
		
		// Fill the message
		messageBodyPart.setContent(mail, "text/html");

		Multipart multipart = new MimeMultipart();
		multipart.addBodyPart(messageBodyPart);

		// Part two is attachment
		//messageBodyPart = new MimeBodyPart();
		//String filename = "C:\\store\\Response.csv";
		//String filename = "/opt/isv/tomcat-6.0.18/temp/ResponseLock.csv";
		/*DataSource source = new FileDataSource(outputFileLocation);
		messageBodyPart.setDataHandler(new DataHandler(source));
		messageBodyPart.setFileName(source.getName());*/
		//multipart.addBodyPart(messageBodyPart);

		// Put parts in message
		message.setContent(multipart);

		// Send the message
		Transport.send(message);
		//System.out.println("Msg Send ....");
	}
}
