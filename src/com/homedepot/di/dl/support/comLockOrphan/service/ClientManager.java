package com.homedepot.di.dl.support.comLockOrphan.service;


import com.sun.jersey.api.client.Client;

public class ClientManager {
	private static Client client = null;

	public static Client getClient(){
		if(client == null){
			client = Client.create();
		}
		return client;		
	}
}
