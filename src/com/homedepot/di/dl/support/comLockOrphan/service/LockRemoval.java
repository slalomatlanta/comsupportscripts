package com.homedepot.di.dl.support.comLockOrphan.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.LineNumberReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import au.com.bytecode.opencsv.CSVReader;

import com.homedepot.di.dl.support.ErrorDeltas.dao.DeltaError;
import com.homedepot.di.dl.support.util.DateUtil;
import com.homedepot.di.dl.support.util.OrderUtil;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.sun.jersey.api.client.WebResource;

import java.util.Date;
import java.util.Properties;
import com.homedepot.di.dl.support.util.Constants;
import com.homedepot.di.dl.support.util.DBConnectionUtil;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeMessage.RecipientType;

import org.apache.log4j.Logger;

public class LockRemoval {

	final static Logger logger = Logger.getLogger(LockRemoval.class);
	/**
	 * @param args
	 * @throws Exception
	 */

	public static void main(String args[]) throws Exception {

		lockRemoval();
	}

	public static void lockRemoval() throws Exception {
		
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd_HH.mm.ss");
		//String outputFile = "C:\\store\\Response"+df.format(new Date())+".csv";
		String outputFile = "/opt/isv/tomcat-6.0.18/temp/ResponseLock"+df.format(new Date())+".csv";

		FileWriter fw = new FileWriter(outputFile, false);

		fw.append("Order Number");
		fw.append(',');
		fw.append("Store");
		fw.append(',');
		fw.append("HD_LOCK_ID");
		fw.append(',');
		fw.append("Is Delta Present");
		fw.append(',');
		fw.append("Is Sterling Lock Removed");
		fw.append(',');
		fw.append("Is Store Lock Present");
		fw.append(',');
		fw.append("Is Store Lock Removed");
		fw.append('\n');

		try {
			final String STERLING_SSC_RO_JNDI = "jdbc/ORCL.PPRMM78X-DPR077MM.001";
			// final String connectionURL =
			// "jdbc:oracle:thin:@//pprmm78x.homedepot.com:1521/dpr77mm_sro01";
			// final String uName = "MMUSR01";
			// final String uPassword = "COMS_MMUSR01";

			/*
			 * final String connectionURL =
			 * "jdbc:oracle:thin:@pnpmm77z.homedepot.com:1521/QA01SVC_COMORD01";
			 * final String uName = "QA01_STERLING"; final String uPassword =
			 * "QA01_STERLING";
			 */
			Connection con = null;
			try {
				DBConnectionUtil dbConn = new DBConnectionUtil();
				con = dbConn.getJNDIConnection(STERLING_SSC_RO_JNDI);

				// con = DriverManager.getConnection(connectionURL,
				// uName,uPassword);
				//System.out.println("Sterling Connection Opened");
				Statement stmt = null;
				stmt = con.createStatement();
				ResultSet rs = null;
				final String lockDetails = "select yoh.EXTN_HOST_ORDER_REF, loc.STORE, loc.HD_LOCK_ID, loc.HD_LOCK_PROTECT_KEY from THD01.HD_LOCK_PROTECT loc, THD01.YFS_ORDER_HEADER yoh"
						+ " where loc.hd_lock_id is not null and loc.store is not null and yoh.ORDER_HEADER_KEY = loc.ORDER_HEADER_KEY and loc.MODIFYTS < sysdate-3/24";

				// System.out.println(lockDetails);
				ResultSet result = null;
				result = stmt.executeQuery(lockDetails);
				String checkFlag;
				String storeLockid;

				while (result.next()) {
					try {
						String orderNumber = result.getString(1);
						fw.append(orderNumber);
						fw.append(',');
						//System.out.println(orderNumber);
						String store = result.getString(2);
						fw.append(store);
						fw.append(',');

						String sterLock = result.getString(3).trim();
						fw.append(sterLock);
						fw.append(',');
						String lockKey = result.getString(4).trim();
						// System.out.println(orderNumber);
						checkFlag = checkFordeltas(orderNumber, store);
						// System.out.println("Delta Present : "+checkFlag);
						fw.append(checkFlag);
						fw.append(',');
						storeLockid = getStorelock(orderNumber, store);
						//System.out.println("Store lock id : " + storeLockid);
						if (checkFlag.equalsIgnoreCase("N")
								&& storeLockid != null) {

							//int rowDeletedster = 0; 
									int rowDeletedster = delSterlinglock(lockKey);
							if (rowDeletedster != 0) {
								fw.append("Y");
								fw.append(',');
							}

							fw.append("Y");
							fw.append(',');
							String storeResponse = delStorelock(orderNumber,
									store, storeLockid);
							fw.append(storeResponse);
							fw.append('\n');

						}

						else if (checkFlag.equalsIgnoreCase("N")
								&& storeLockid == null) {
							int rowDeletedster = delSterlinglock(lockKey);
							if (rowDeletedster != 0) {
								fw.append("Y");
								fw.append(',');
							}

							fw.append("N");
							fw.append(',');
							fw.append("N");
							fw.append('\n');

						}

						else if (checkFlag.equalsIgnoreCase("Y")
								&& storeLockid != null) {
							fw.append("N");
							fw.append(',');
							fw.append("Y");
							fw.append(',');
							fw.append("N");
							fw.append('\n');

						}

						else if (checkFlag.equalsIgnoreCase("Y")
								&& storeLockid == null) {
							fw.append("N");
							fw.append(',');
							fw.append("N");
							fw.append(',');
							fw.append("N");
							fw.append('\n');

						}
						
						else
						{
							fw.append('\n');
						}
					} catch (Exception e) {
						fw.append('\n');
						logger.info("Exception : "+e);
					}

				}
				stmt.close();
				con.close();

			}

			catch (Exception e) {
				logger.info("Exception : "+e);
			}

		}

		catch (Exception e)

		{
			logger.info("Exception : "+e);
		}

		// TODO Auto-generated method stub
		fw.flush();
		fw.close();
		
		BufferedReader br = new BufferedReader(new FileReader(outputFile));
		String line = "";
	    int count = 0;
	    while ((line = br.readLine()) != null) 
	    {
	    	count+=1;
	    }

	    if(count > 1)
	    {
	    	
	    	sendAttachment(outputFile);
	    }
	    
	    else
	    {
	    	logger.info("No records found in hd_lock_protect table");
	    }
	}

	public static String delStorelock(String orderNumber, String store,
			String storeLock) throws SQLException, ClassNotFoundException {

		String flag;

		String url = "http://st" + store
				+ ".homedepot.com:12130/ComStoreServices/order/manualUnlockOrder";

		Client client = ClientManager.getClient();

		WebResource webResource = ((com.sun.jersey.api.client.Client) client)
				.resource(url);
		String inputXML = "<ManualOrderUnlockRequest><orderId>"+orderNumber
				+"</orderId><unlockingLdapId>MSQ9999</unlockingLdapId><unlockingStoreId>"+store
				+"</unlockingStoreId><reasonToUnlock>REASON</reasonToUnlock></ManualOrderUnlockRequest>";
		
		

		//System.out.println(inputXML);
		String responseXML = webResource.type("text/xml").post(String.class,
				inputXML);

		//System.out.println("store lock removal response : " + responseXML);

		 String response = null;

		// String oNo = null;
		if (responseXML.contains("responseCode")) {
			
			  String dupStr = responseXML;
			  
			  int firstIndex = dupStr.indexOf("<responseCode>");
			  firstIndex = firstIndex + 14; 
			  int lastIndex = dupStr.indexOf("</responseCode>"); 
			  response = dupStr.substring(firstIndex, lastIndex);
			  
			  response = response.trim(); 
			  //System.out.println(response);
			 

			flag = response;

		}

		else {
			flag = "N";
		}

		return flag;

	}

	public static int delSterlinglock(String lockKey) throws SQLException {

		DBConnectionUtil dbConn = new DBConnectionUtil();

		Connection con = dbConn
				.getJNDIConnection(Constants.STERLING_ATC_RW_JNDI);
		Statement stmt = null;
		stmt = con.createStatement();
		int rowsDeleted = 0;

		String delQuery = "delete from THD01.hd_lock_protect where hd_lock_protect_key='"
				+ lockKey + "'";

		rowsDeleted = stmt.executeUpdate(delQuery);

		con.commit();
		//System.out.println(delQuery + ": rowsDeleted sterling lock deleted");
		stmt.close();
		con.close();

		return rowsDeleted;
	}

public static String getStorelock(String orderNumber, String storeNumber)

	{
		Statement stmt = null;
		ResultSet result = null;
		Connection con = null;
		String lockID = null;
		try {
			DBConnectionUtil dbConn = new DBConnectionUtil();
			con = dbConn.getStoreconnection(storeNumber);

			String query = "select * from comt_locl_ord_lock where cust_ord_id in ('"
					+ orderNumber + "')";

			if (con != null) {

				stmt = con.createStatement();
				result = stmt.executeQuery(query);
				while (result.next())

				{
					lockID = result.getString(1);
				}

				stmt.close();

				con.close();
			}
		} catch (Exception e) 
		{
			logger.info("Exception : "+ e.getMessage());
//			System.out.println("unable to connect to store : " + storeNumber);
//			System.out.println(e.getMessage());

		}

		return lockID;

	}

	public static String checkFordeltas(String orderNumber, String store)
			throws Exception {

		String flag = null;

		Statement stmt = null;
		ResultSet result = null;
		Connection con = null;
		try {
			DBConnectionUtil dbConn = new DBConnectionUtil();
			con = dbConn.getStoreconnection(store);

			String query = "select * from comt_ord_chg_msg where cust_ord_id in ('"
					+ orderNumber + "')";
			// System.out.println(query);

			if (con != null) {

				stmt = con.createStatement();
				result = stmt.executeQuery(query);
				if (result.next()) {
					flag = "Y";
				} else {
					flag = "N";
				}

				stmt.close();

				con.close();
			}
			
			else
			{
				flag = "Not able to connect to store "+store;
			}

		} catch (Exception e) {

			logger.error("Exception : "+ e.getMessage());
			/*System.out.println("unable to connect to store : " + store);
			System.out.println(e.getMessage());*/

		}
		logger.info("Delta Present : " + flag);
		return flag;
	}

	public static void sendAttachment(String outputFile) throws Exception {
		String host = "mail1.homedepot.com";
		String from = "horizon@cpliisad.homedepot.com";
		//String[] to = { "mohanraj_gurusamy@homedepot.com" };
		String[] to ={"_2fc77b@homedepot.com"};
		// String[] Cc ={""};

		// Get system properties
		Properties properties = System.getProperties();

		// Setup mail server
		properties.setProperty("mail.smtp.host", host);

		// Get the default Session object.
		Session session = Session.getInstance(properties);

		// Define message
		Message message = new MimeMessage(session);
		message.setFrom(new InternetAddress(from));
		InternetAddress[] addressTo = new InternetAddress[to.length];
		// InternetAddress[] addressCc = new InternetAddress[Cc.length];
		for (int i = 0; i < to.length; i++) {
			addressTo[i] = new InternetAddress(to[i]);
			logger.debug("To Address " + to[i]);
		}
		// for (int j = 0; j < Cc.length; j++)
		// { addressCc[j] = new InternetAddress(Cc[j]);
		// System.out.println("Cc Address "+Cc[j]);
		// }
		message.setRecipients(RecipientType.TO, addressTo);
		// message.setRecipients(RecipientType.CC, addressCc);
		message.setSubject("Orphan Orderlock unlocked");

		// Create the message part
		BodyPart messageBodyPart = new MimeBodyPart();
		String msgContent = "Hi All,<br/><br/>PFA report for Orphan Orderlock's which are unlocked <br/><br/>Thanks<br/>";
		msgContent = msgContent + "COM Multichannel Support";
		// Fill the message
		messageBodyPart.setContent(msgContent, "text/html");

		Multipart multipart = new MimeMultipart();
		multipart.addBodyPart(messageBodyPart);

		// Part two is attachment
		messageBodyPart = new MimeBodyPart();
		//String filename = "C:\\store\\Response.csv";
		//String filename = "/opt/isv/tomcat-6.0.18/temp/ResponseLock.csv";
		DataSource source = new FileDataSource(outputFile);
		messageBodyPart.setDataHandler(new DataHandler(source));
		messageBodyPart.setFileName(source.getName());
		multipart.addBodyPart(messageBodyPart);

		// Put parts in message
		message.setContent(multipart);

		// Send the message
		Transport.send(message);
		//System.out.println("Msg Send ....");
	}
}
