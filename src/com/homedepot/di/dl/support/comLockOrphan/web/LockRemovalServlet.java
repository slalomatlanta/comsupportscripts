package com.homedepot.di.dl.support.comLockOrphan.web;

import java.io.IOException;
import java.util.GregorianCalendar;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.homedepot.di.dl.support.ErrorDeltas.dao.DeltaError;
import com.homedepot.di.dl.support.comLockOrphan.service.LockRemoval;

/**
 * Servlet implementation class LockRemovalServlet
 */
public class LockRemovalServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	final static Logger logger = Logger.getLogger(LockRemovalServlet.class);
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LockRemovalServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// TODO Auto-generated method stub
		logger.info("Orphan Lock Removal Started :: "+GregorianCalendar.getInstance().getTime());
		LockRemoval l=new LockRemoval();
		try {
			l.lockRemoval();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.info("Orphan Lock Removal Exception :: "+GregorianCalendar.getInstance().getTime() + e);
		}
		
		logger.info("Orphan Lock Removal Completed :: "+GregorianCalendar.getInstance().getTime());
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
