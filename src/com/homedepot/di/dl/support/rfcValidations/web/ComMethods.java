package com.homedepot.di.dl.support.rfcValidations.web;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.log4j.Logger;

public class ComMethods {
	final static Logger logger = Logger.getLogger(ComMethods.class);
	public static int getDateFormat(String value) {
		Date date = null;
		int minutes = 15;
		try {
			date = new SimpleDateFormat("EEE MMM dd HH:mm:ss Z yyyy")
					.parse(value);
			logger.info("actual run date:" + date);
			Date currenttime = new Date();
			logger.info("current date:" + currenttime);
			long differenceInMillis = currenttime.getTime() - date.getTime();
			minutes = (int) (differenceInMillis / (1000 * 60));

			logger.info("minutes :" + minutes);
			return minutes;

		} catch (ParseException e) {
			e.printStackTrace();

			return 15;
		}
	}
}
