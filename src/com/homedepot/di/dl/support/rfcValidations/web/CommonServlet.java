package com.homedepot.di.dl.support.rfcValidations.web;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLConnection;
import java.util.StringTokenizer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;

import org.apache.log4j.Logger;

/**
 * Servlet implementation class CommonServlet
 */
public class CommonServlet extends HttpServlet {
	
	final static Logger logger = Logger.getLogger(CommonServlet.class);
	
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CommonServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String storeList = request.getParameter("storeList");
		logger.debug(storeList);
		logger.debug(request.getParameter("job"));
		char flagArray[] = request.getParameter("job").toCharArray();
		char flag = flagArray[0];
		switch(flag){
			case 'A':
			{
				logger.info("User selected Store Order Recall");


				// TODO Auto-generated method stub
				String store="";
				String outputXML="";
				String url="";
				String requestXML="";
				StringTokenizer st = null;
				String strLine = "";
				String orderToRecall = request.getParameter("OrderNo").toString().trim();
				String outXLS = "/opt/isv/tomcat/temp/StoreOrderRecall.xls";		
//				String outXLS = "c:/test/StoreOrderRecall.xls";
				HSSFWorkbook hwb = new HSSFWorkbook();
				HSSFSheet sheet = hwb.createSheet("StoreOrderRecall");
				
				HSSFCellStyle my_style = hwb.createCellStyle();
				HSSFFont my_font = hwb.createFont();
				my_font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
				
				response.setContentType("text/html");
				PrintWriter out = response.getWriter();
				out.println("storeNumber|Status");
				out.println("<br/>");
				
				HSSFRow rowhead1 = sheet.createRow((short) 0);
				rowhead1.createCell((short) 0).setCellValue("STORE");
				rowhead1.createCell((short) 1).setCellValue("Status");
				rowhead1.createCell((short) 2).setCellValue("Hitting URL");
				rowhead1.createCell((short) 3).setCellValue("Request-XML");
				rowhead1.createCell((short) 4).setCellValue("Response-XML");
				rowhead1.setRowStyle(my_style);
				int j = 1;
				
					st = new StringTokenizer(storeList, ",");
					while (st.hasMoreElements()) {
						store = st.nextElement().toString().trim();
						//logger.debug(store);
						int len = store.length();
						if (len < 4) {
							for (int i = 0; i < (4 - len); i++) {
								store = "0" + store;
							}
						}
						try{
							//System.out.println("\nProcessing for Store: "+store);
					        requestXML = "<OrderDetailRequest>" +
					        		"<HostOrderNumber>"+orderToRecall+"</HostOrderNumber>" +
					        		"<POOrSONumber/>" +
					        		"<RepairTag/>" +
					        		"<LockRequired>false</LockRequired>" +
					        		"<UserID>COMSupport</UserID>" +
					        		"<RecallingStoreNo>"+store+"</RecallingStoreNo>" +
					        		"<XmlType>Sterling</XmlType>" +
					        		"<IncludeStatusDetails>true</IncludeStatusDetails>" +
					        		"<IncludeEventDetails>true</IncludeEventDetails>" +
					        		"<IncludeAlertDetails>true</IncludeAlertDetails>" +
					        		"<IncludeAuditDetails>false</IncludeAuditDetails>" +
					        		"<IncludeReleaseDetails>false</IncludeReleaseDetails>" +
					        		"<IncludeInstructions>true</IncludeInstructions>" +
					        		"<IncludeWorkOrderDetails>false</IncludeWorkOrderDetails> " +
					        		"<IncludeOrderBusinessRules>false</IncludeOrderBusinessRules>" +
					        		"</OrderDetailRequest>";
					        url = "http://st"+store+".homedepot.com:12130/ComStoreServices/order/recall";
					        logger.debug("Hitting to URL: \n"+url);
					        Client client = (Client) ClientManager.getClient();
					        WebResource webResource = ((com.sun.jersey.api.client.Client) client).resource(url);
					        outputXML = webResource.type("application/xml").post(String.class,requestXML);
							}catch (Exception e) {
								logger.error(store + ": Exception occured while recalling for store: "+e);
								HSSFRow row = sheet.createRow((short) j+1);
								row.createCell((short) 0).setCellValue(store);
								row.createCell((short) 1).setCellValue("Error Occured");
								out.println(store+"|Exception Occured: "+e);
								out.println("<br/>");
							continue;
							} 
					        String status = "";
					        if(outputXML.contains("responseDescription")){
						        int firstIndex = outputXML.indexOf("responseDescription");
						        firstIndex  = firstIndex + 20;
						        int lastIndex = firstIndex+42;
						        status = outputXML.substring(firstIndex , lastIndex);
					        }
					        logger.debug("Status = "+status);
					        HSSFRow row = sheet.createRow((short) j);
							row.createCell((short) 0).setCellValue(store);
							row.createCell((short) 1).setCellValue(status);
							row.createCell((short) 2).setCellValue(url);
							row.createCell((short) 3).setCellValue(requestXML);
							if(outputXML.length() > 32765){
								outputXML = outputXML.substring(0, 32765);
							}
							row.createCell((short) 4).setCellValue(outputXML);
							out.println(store+"|"+status);
							out.println("<br/>");
							j++;
							}
					
					
				FileOutputStream fileOut = new FileOutputStream(outXLS);
				hwb.write(fileOut);
				fileOut.close();
				out.println("File generated at " + outXLS);
				out.println("Completed");
				logger.debug("File generated at " + outXLS);
				logger.debug("Completed");
				
			
				break;
			}
			case 'B':
			{
				logger.info("User selected Store COM Resync");

				// TODO Auto-generated method stub
//				String outXLS = "c:/test/StoreComResync.xls";
				String outXLS = "/opt/isv/tomcat/temp/StoreComResync.xls";
				HSSFWorkbook hwb = new HSSFWorkbook();
				HSSFSheet sheet = hwb.createSheet("StoreComResync");
				HSSFCellStyle my_style = hwb.createCellStyle();
				HSSFFont my_font = hwb.createFont();
				my_font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
				
				response.setContentType("text/html");
				PrintWriter out = response.getWriter();
				out.println("storeNumber|Last Execution Time|Status");
				out.println("<br/>");
				HSSFRow rowhead1 = sheet.createRow((short) 0);
				rowhead1.createCell((short) 0).setCellValue("STORE");
				rowhead1.createCell((short) 1).setCellValue("Exceution Time");
				rowhead1.createCell((short) 2).setCellValue("Status");
				rowhead1.setRowStyle(my_style);

				
				String storeNumber="";
				StringTokenizer st = null;
				String strLine = "";
				String flag1 = "false";
				int j=0;
				
					
					st = new StringTokenizer(storeList, ",");
					
					while (st.hasMoreElements()) {
						
						try{
						storeNumber = st.nextElement().toString().trim();

						int len = storeNumber.length();
						if (len < 4) {

							for (int i = 0; i < (4 - len); i++) {
								storeNumber = "0" + storeNumber;
							}

						}

						URL resumeTrans = new URL(
								"http://st"
										+ storeNumber
										+ ".homedepot.com:12080/StoreComResync/ComResyncLauncher/THDBATCH.getStatistics");

						URLConnection resumeTransConn = resumeTrans
								.openConnection();
						BufferedReader in = new BufferedReader(
								new InputStreamReader(
										resumeTransConn.getInputStream()));
						String inputLine = "", output = "", result = "";
						while ((inputLine = in.readLine()) != null) {
							if (!(inputLine == null || ""
									.equalsIgnoreCase(inputLine))) {
								output = output + inputLine;
							}
						}
						in.close();

						int firstIndex = output.indexOf("ComResyncWorker");
						int lastIndex = output.indexOf("</worker>");
						if (lastIndex < firstIndex) {
							lastIndex = output.lastIndexOf("</worker>");
						}

						result = output.substring(firstIndex, lastIndex);

						if (result.contains("ComResyncWorker")) {
							int firstInde = result.indexOf("lastExecutionTime>");
							int lastInde = result
									.lastIndexOf("</lastExecutionTime>");
							result = result.substring(firstInde + 18, lastInde);
							logger.debug("lastExecutionTime for "
									+ storeNumber + " : " + result);
						}

						int min = ComMethods.getDateFormat(result);
						//System.out.println(min);
						if (min <= 10) {
							flag1 = "true";
						}
						else{
							flag1 = "false";
						}
						//System.out.println("flag1:"+flag1);
						HSSFRow row = sheet.createRow((short) j + 1);
						row.createCell((short) 0).setCellValue(storeNumber);
						row.createCell((short) 1).setCellValue(result);
						row.createCell((short) 2).setCellValue(flag1);
						out.println(storeNumber+"|"+result+"|"+flag1);
						out.println("<br/>");
						j++;
						
					}catch (Exception e) {
						logger.error(storeNumber + ": Exception occured while getting last resync time for store: "+e);
						HSSFRow row = sheet.createRow((short) j + 1);
						row.createCell((short) 0).setCellValue(storeNumber);
						row.createCell((short) 1).setCellValue("Exception Occured: "+e);
						row.createCell((short) 2).setCellValue("false");
						out.println(storeNumber+"|Exception Occured: "+e+"|"+flag1);
						out.println("<br/>");
						j++;
						continue;
					}
					}
					
				
				
				FileOutputStream fileOut = new FileOutputStream(outXLS);
				hwb.write(fileOut);
				fileOut.close();
				out.println("File generated at " + outXLS);
				out.println("Completed");
				logger.debug("File generated at " + outXLS);
				logger.debug("Completed");

			 

				break;
			}
			case 'C':
			{
				logger.debug("User selected COM Alert Services");
				

				// TODO Auto-generated method stub
//					String outXLS = "c:/test/ComAlertServices.xls";
					String outXLS = "/opt/isv/tomcat/temp/ComAlertServices.xls";
					HSSFWorkbook hwb = new HSSFWorkbook();
					HSSFSheet sheet = hwb.createSheet("ComAlertServices");
					HSSFCellStyle my_style = hwb.createCellStyle();
					HSSFFont my_font = hwb.createFont();
					my_font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
					
					response.setContentType("text/html");
					PrintWriter out = response.getWriter();
					out.println("storeNumber|Last Execution Time|Status");
					out.println("<br/>");
					HSSFRow rowhead1 = sheet.createRow((short) 0);
					rowhead1.createCell((short) 0).setCellValue("STORE");
					rowhead1.createCell((short) 1).setCellValue("Exceution Time");
					rowhead1.createCell((short) 2).setCellValue("Status");
					rowhead1.setRowStyle(my_style);

					
					String storeNumber="";
					StringTokenizer st = null;
					String strLine = "";
					String flag1 = "false";
					int j=0;
					
						
						st = new StringTokenizer(storeList, ",");
						while (st.hasMoreElements()) {
							
							try {
							storeNumber = st.nextElement().toString().trim();

							int len = storeNumber.length();
							if (len < 4) {

								for (int i = 0; i < (4 - len); i++) {
									storeNumber = "0" + storeNumber;
								}

							}

							URL resumeTrans = new URL(
									"http://st"
											+ storeNumber
											+ ".homedepot.com:12080/ComAlertServices/ComAlertServicesLauncher/THDBATCH.getStatistics");

							URLConnection resumeTransConn = resumeTrans
									.openConnection();
							BufferedReader in = new BufferedReader(
									new InputStreamReader(
											resumeTransConn.getInputStream()));
							String inputLine = "", output = "", result = "";
							while ((inputLine = in.readLine()) != null) {
								if (!(inputLine == null || ""
										.equalsIgnoreCase(inputLine))) {
									output = output + inputLine;
								}
							}
							in.close();

							int firstIndex = output.indexOf("ComAlertServicesWorker");
							int lastIndex = output.indexOf("</worker>");
							if (lastIndex < firstIndex) {
								lastIndex = output.lastIndexOf("</worker>");
							}

							result = output.substring(firstIndex, lastIndex);

							if (result.contains("ComAlertServicesWorker")) {
								int firstInde = result.indexOf("lastExecutionTime>");
								int lastInde = result
										.lastIndexOf("</lastExecutionTime>");
								result = result.substring(firstInde + 18, lastInde);
								logger.debug("lastExecutionTime for "
										+ storeNumber + " : " + result);
							}

							int min = ComMethods.getDateFormat(result);

							if (min < 2) {
								flag1 = "true";
							}
							else{
								flag1 = "false";
							}
							logger.debug("flag1:"+flag1);
							HSSFRow row = sheet.createRow((short) j + 1);
							row.createCell((short) 0).setCellValue(storeNumber);
							row.createCell((short) 1).setCellValue(result);
							row.createCell((short) 2).setCellValue(flag1);
							out.println(storeNumber+"|"+result+"|"+flag1);
							out.println("<br/>");
							j++;

						}catch (Exception e) {
							logger.error(storeNumber + ": Exception occured while getting last AlertServices job time for store: "+e);
							HSSFRow row = sheet.createRow((short) j + 1);
							row.createCell((short) 0).setCellValue(storeNumber);
							row.createCell((short) 1).setCellValue("Exception Occured: "+e);
							row.createCell((short) 2).setCellValue("false");
							out.println(storeNumber+"|Exception Occured: "+e+"|"+flag1);
							out.println("<br/>");
							j++;
							continue;
						}
						}
					

					FileOutputStream fileOut = new FileOutputStream(outXLS);
					hwb.write(fileOut);
					fileOut.close();
					out.println("File generated at " + outXLS);
					out.println("Completed");
					logger.info("File generated at " + outXLS);
					logger.info("Completed");

				
			
				break;
			}
			
			case 'D': {
				logger.info("User selected Check Inventory in local stores");


				// TODO Auto-generated method stub
				String store="";
				String outputXML="";
				String url="";
				String requestXML="";
				StringTokenizer st = null;
				String strLine = "";
//				String outXLS = "c:/test/CheckInventory.xls";
				String outXLS = "/opt/isv/tomcat/temp/CheckInventory.xls";		
				HSSFWorkbook hwb = new HSSFWorkbook();
				HSSFSheet sheet = hwb.createSheet("CheckInventory");
				
				HSSFCellStyle my_style = hwb.createCellStyle();
				HSSFFont my_font = hwb.createFont();
				my_font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
				
				response.setContentType("text/html");
				PrintWriter out = response.getWriter();
				out.println("storeNumber|Status");
				out.println("<br/>");
				
				HSSFRow rowhead1 = sheet.createRow((short) 0);
				rowhead1.createCell((short) 0).setCellValue("STORE");
				rowhead1.createCell((short) 1).setCellValue("Status");
				rowhead1.createCell((short) 2).setCellValue("Hitting URL");
				rowhead1.createCell((short) 3).setCellValue("Request-XML");
				rowhead1.createCell((short) 4).setCellValue("Response-XML");
				rowhead1.setRowStyle(my_style);
				int j = 1;
				
					st = new StringTokenizer(storeList, ",");
					while (st.hasMoreElements()) {
						store = st.nextElement().toString().trim();
						//System.out.println(store);
						int len = store.length();
						if (len < 4) {
							for (int i = 0; i < (4 - len); i++) {
								store = "0" + store;
							}
						}
						try{
							logger.debug("\nProcessing for Store: "+store);
					        requestXML = "<CheckInventoryRequest>" +
					        		"	<Locations>" +
					        		"		<CheckLocalStores MaximumNumberOfLocations=\"10\" />" +
					        		"	</Locations>" +
					        		"	<Items>" +
					        		"		<Item ItemID=\"161640\" ItemType=\"SKU\" />" +
					        		"	</Items>" +
					        		"</CheckInventoryRequest>";
					        url = "http://isp.st"+store+".homedepot.com:12030/StoreCOMInventory/rs/checkInventory";
					        logger.debug("Hitting to URL: \n"+url);
					        Client client = (Client) ClientManager.getClient();
					        WebResource webResource = ((com.sun.jersey.api.client.Client) client).resource(url);
//					        outputXML = webResource.type("application/xml").post(String.class,requestXML);
					        
//					        Adding this to enable the storeCOMInventory Call 
					        outputXML = webResource.header("referer", "DXR05-SOAP-UI")
					        		.type("text/xml")
					        		.post(String.class, requestXML); 
					        
							}catch (Exception e) {
								logger.error(store + ": Exception occured while Checking Inventory in local stores: "+e);
								HSSFRow row = sheet.createRow((short) j+1);
								row.createCell((short) 0).setCellValue(store);
								row.createCell((short) 1).setCellValue("Error Occured");
								out.println(store+"|Exception Occured: "+e);
								out.println("<br/>");
							continue;
							} 
					        String status = "";
					        if(outputXML.contains("ResponseDescription")){
						        int firstIndex = outputXML.indexOf("ResponseDescription");
						        firstIndex  = firstIndex + 21;
						        int lastIndex = firstIndex+7;
						        status = outputXML.substring(firstIndex , lastIndex);
					        }
					        logger.info("Status = "+status);
					        HSSFRow row = sheet.createRow((short) j);
							row.createCell((short) 0).setCellValue(store);
							row.createCell((short) 1).setCellValue(status);
							row.createCell((short) 2).setCellValue(url);
							row.createCell((short) 3).setCellValue(requestXML);
							if(outputXML.length() > 32765){
								outputXML = outputXML.substring(0, 32765);
							}
							row.createCell((short) 4).setCellValue(outputXML);
							out.println(store+"|"+status);
							out.println("<br/>");
							j++;
							}
					
					
				FileOutputStream fileOut = new FileOutputStream(outXLS);
				hwb.write(fileOut);
				fileOut.close();

				logger.info("File generated at " + outXLS);
				logger.info("Completed");
				
			
				break;
			}
			
			case 'E': {

				logger.info("User selected Bring suspended store back to normal");
				

				// TODO Auto-generated method stub
//					String outXLS = "c:/test/SuspendToNormal.xls";
					String outXLS = "/opt/isv/tomcat/temp/SuspendToNormal.xls";
					HSSFWorkbook hwb = new HSSFWorkbook();
					HSSFSheet sheet = hwb.createSheet("SuspendToNormal");
					HSSFCellStyle my_style = hwb.createCellStyle();
					HSSFFont my_font = hwb.createFont();
					my_font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
					
					response.setContentType("text/html");
					PrintWriter out = response.getWriter();
					out.println("storeNumber|Status");
					out.println("<br/>");
					HSSFRow rowhead1 = sheet.createRow((short) 0);
					rowhead1.createCell((short) 0).setCellValue("STORE");
					rowhead1.createCell((short) 1).setCellValue("Status");
					rowhead1.setRowStyle(my_style);

					
					String storeNumber="";
					StringTokenizer st = null;
					String strLine = "";
					
					int j=0;
					
						
						st = new StringTokenizer(storeList, ",");
						while (st.hasMoreElements()) {
							
							try {
							storeNumber = st.nextElement().toString().trim();

							int len = storeNumber.length();
							if (len < 4) {

								for (int i = 0; i < (4 - len); i++) {
									storeNumber = "0" + storeNumber;
								}

							}

							URL resumeTrans = new URL(
									"http://st"+storeNumber+".homedepot.com:12058/COMSupplyExtractor/ResumeTransmission?start");

							URLConnection resumeTransConn = resumeTrans
									.openConnection();
							BufferedReader in = new BufferedReader(
									new InputStreamReader(
											resumeTransConn.getInputStream()));
							String inputLine = "", output = "";
							while ((inputLine = in.readLine()) != null) {
								if (!(inputLine == null || ""
										.equalsIgnoreCase(inputLine))) {
									output = output + inputLine;
								}
							}
							in.close();
							
							HSSFRow row = sheet.createRow((short) j + 1);
							row.createCell((short) 0).setCellValue(storeNumber);
							row.createCell((short) 1).setCellValue(output);
							out.println(storeNumber+"|"+output);
							out.println("<br/>");
							j++;

						}catch (Exception e) {
							logger.error(storeNumber + ": Exception occured while getting suspended store back to normal - store: "+e);
							HSSFRow row = sheet.createRow((short) j + 1);
							row.createCell((short) 0).setCellValue(storeNumber);
							row.createCell((short) 1).setCellValue("Exception Occured: "+e);
							out.println(storeNumber+"|Exception Occured: "+e);
							out.println("<br/>");
							j++;
							continue;
						}
						}
					

					FileOutputStream fileOut = new FileOutputStream(outXLS);
					hwb.write(fileOut);
					fileOut.close();
					out.println("File generated at " + outXLS);
					out.println("Completed");
					logger.info("File generated at " + outXLS);
					logger.info("Completed");

				
			
				break;
			}
			case 'F': {

				logger.info("User selected Custom URL");
					String customURL = storeList.trim();
					response.setContentType("text/html");
					PrintWriter out = response.getWriter();
								
							try {
							URL resumeTrans = new URL(customURL);
							URLConnection resumeTransConn = resumeTrans
									.openConnection();
							BufferedReader in = new BufferedReader(
									new InputStreamReader(
											resumeTransConn.getInputStream()));
							String inputLine = "", output = "";
							while ((inputLine = in.readLine()) != null) {
								if (!(inputLine == null || ""
										.equalsIgnoreCase(inputLine))) {
									output = output + inputLine;
								}
							}
							in.close();
							out.println(output);

						}catch (Exception e) {
							logger.error("Exception occured while hitting the custom URL :  "+customURL+"\n"+e);
							out.println("Exception occured while hitting the custom URL :  "+customURL);
							out.println("<br/>");
							out.println(e);
						}
							logger.info("Completed");
					break;
			}
		}
	}

}
