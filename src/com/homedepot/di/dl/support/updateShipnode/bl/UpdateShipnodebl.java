package com.homedepot.di.dl.support.updateShipnode.bl;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import com.homedepot.di.dl.support.updateShipnode.dao.Updateshipnodedao;

public class UpdateShipnodebl {
	public static void updateHDPSILineNo1() {
		ArrayList<String> updateQueryList = null;
		int recordsUpdated = 0;
		Updateshipnodedao updateshipnodedao = new Updateshipnodedao();
/*		File file = new File("C:\\UpdateHDPSILineNo\\logs\\UpdateHDPSILineNumbers"
				+ new Date().getTime() + ".log");
		FileOutputStream fos;
		try {
			fos = new FileOutputStream(file);
			PrintStream ps = new PrintStream(fos);
			System.setOut(ps);
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}*/
		updateQueryList = updateshipnodedao.getUpdateQueryList();
		if (updateQueryList.size() > 0) {
			recordsUpdated = updateshipnodedao
					.updateHDPSILineNumbers1(updateQueryList);
		}
		
		
			sendEmail(recordsUpdated, updateQueryList);
	}
	
	public static boolean sendEmail(int recordsUpdated, List<String> updateQueryList) {
//		String fileLocn = "c:\\Tax\\OrderListUnsupportedFormat.csv";
		StringBuilder emailBody = new StringBuilder();
		emailBody.append("Hello All,");
		emailBody.append('\n');
		emailBody
				.append("Below are the "+recordsUpdated+" keys that are updated.");
		emailBody.append('\n');
		emailBody.append('\n');
		emailBody.append("ShipNode,linekey,OrderNumber");
		emailBody.append('\n');
		if (updateQueryList.size() > 0) {
			for (String entry : updateQueryList) {
				emailBody.append(entry);
				emailBody.append('\n');
			}
		} else {
			emailBody.append(" No rows got updated ");
			emailBody.append('\n');
		}
		emailBody.append('\n');
		emailBody.append("Thank you,");
		emailBody.append('\n');
		emailBody.append("COM MultiChannel Support");
		emailBody.append('\n');
		emailBody
				.append("------------This is an automated email. Do not reply---------------");
		emailBody.append('\n');

		String eMailSubject = "Updating Shipnode on orderlines when converted from S2C  to S2S ";

		Session session = null;
		MimeMessage mimeMessage = null;
		MimeBodyPart mimeBodyPart = null;
		Multipart multiPart = null;
		String date = null;
		Calendar calendar = null;
		Properties properties = null;

		boolean isEmailSent = false;
		// response.setContentType("text/html");

		try {
			// hourStart = request.getParameter("hourStart");
			// dayStart = request.getParameter("dayStart");
			// text = dao.getHourlyReport(hourStart, dayStart);

			// text = dao.getHourlyReport();
			String text = emailBody.toString();
			calendar = Calendar.getInstance();
			date = calendar.getTime().toString();
			properties = System.getProperties();
			properties.put("mail.host", "mail1.homedepot.com");
			session = Session.getInstance(properties, null);
			mimeMessage = new MimeMessage(session);
			mimeMessage.setFrom(new InternetAddress(
					"horizon@cpliisad.homedepot.com"));
			mimeMessage.setRecipients(Message.RecipientType.TO, InternetAddress
	.parse("_2fc77b@homedepot.com", false));
		//	 mimeMessage.setRecipients(Message.RecipientType.TO,
			// InternetAddress.parse("shwetha_ravi@homedepot.com", false));

			mimeMessage.setSubject(eMailSubject + date);
			mimeBodyPart = new MimeBodyPart();
			mimeBodyPart.setText(text.toString());
			multiPart = new MimeMultipart();
			multiPart.addBodyPart(mimeBodyPart);
			mimeMessage.setContent(multiPart);
			Transport.send(mimeMessage);

			/*
			 * Write the HTML to the response
			 */
			isEmailSent = true;
		} catch (Exception e) {
			isEmailSent = false;
			e.printStackTrace();
		}

		return isEmailSent;
	}
	
}
