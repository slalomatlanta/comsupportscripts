package com.homedepot.di.dl.support.updateShipnode.dao;


import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import javax.crypto.IllegalBlockSizeException;
import javax.crypto.spec.SecretKeySpec;

import org.apache.log4j.Logger;

import com.homedepot.di.dl.support.updateShipnode.util.*;

public class Updateshipnodedao {
	/*final String sscDriverClass = "oracle.jdbc.driver.OracleDriver";
	final String sscConnectionURL = "jdbc:oracle:thin:@//pprmm78x.homedepot.com:1521/dpr77mm_sro01";
	final String sscUName = "MMUSR01";
	final String sscUPassword = "COMS_MMUSR01";
	final String atcDriverClass = "oracle.jdbc.driver.OracleDriver";
	final String atcConnectionURL = "jdbc:oracle:thin:@//pprmm77x.homedepot.com:1521/dpr77mm_srw02";
	final String atcUName = "THD03";
	final String atcUPassword = "ra1nySumm3r"; */
	DBConnectionUtil dbConn = new DBConnectionUtil();
    Connection sscConn = dbConn.getJNDIConnection(Constants.STERLING_SSC_RO_JNDI);
    Connection atcConn = dbConn.getJNDIConnection(Constants.STERLING_ATC_RW_JNDI);

	private static final Logger logger = Logger
			.getLogger(Updateshipnodedao.class);

	public ArrayList<String> getUpdateQueryList() {
		ArrayList<String> updateQueryList = null;
		Connection sterCon = null;
		Statement stmt1 = null;
		// encrypt(atcUPassword);
		/*String query = "select yol.EXTN_SPL_ORD_LINE_NUMBER, hpsi.HD_PAYMENT_STAGE_INFO_KEY, " +
				"yoh.EXTN_HOST_ORDER_REF,yoh.EXTN_SVS_ORDER_STATUS_DESC,yoh.CREATETS, "
				+ " yol.EXTN_HOST_ORDER_LINE_REF, yol.EXTN_SPL_ORD_LINE_NUMBER, hpsi.HOST_ORDER_LINE_REF,hpsi.LINE_NUMBER,hpsi.STATUS"
				+ " from THD01.yfs_order_header yoh,THD01.HD_PAYMENT_STAGE_INFO hpsi, THD01.yfs_order_line yol "
				+ " where yoh.ORDER_HEADER_KEY > '20150619' and yol.ORDER_HEADER_KEY > '20150619' and hpsi.ORDER_HEADER_KEY > '20150619'  " +
				"and yoh.DOCUMENT_TYPE='0001' and yol.ORDER_HEADER_KEY=yoh.ORDER_HEADER_KEY"
				+ " and yol.LINE_TYPE='SO' and hpsi.ORDER_HEADER_KEY=yoh.ORDER_HEADER_KEY and hpsi.HOST_ORDER_LINE_REF=yol.EXTN_HOST_ORDER_LINE_REF "
				+ " and hpsi.LINE_NUMBER <> yol.EXTN_SPL_ORD_LINE_NUMBER";*/
				String query  ="select oh.order_header_key,ol.order_line_key, oh.extn_host_order_ref, ol.extn_host_order_line_ref, ol.shipnode_key, wc.ship_node, "
						 +" 'update thd01.yfs_order_line set shipnode_key ='''||wc.ship_node||''', modifyts=sysdate, modifyuserid=''COMSupport'' where " +"order_line_key='''||ol.order_line_key||''';commit;' as UpdateQuery  from thd01.yfs_order_line ol, "
						 +"thd01.yfs_order_header oh, thd01.hd_will_call wc where oh.order_header_key > '20150805' and oh.order_header_key = ol.order_header_key and " 
						 +"oh.order_header_key = wc.order_header_key and ol.extn_will_call_line_num= wc.will_call_line_number and ol.line_type='SO' and "
						 +"ol.shipnode_key='DEFAULT_VENDOR_NODE' and ol.EXTN_WILL_CALL_LINE_NUM > 0 and ol.delivery_method='PICK'  " ; 
/*String query ="select oh.order_header_key,ol.order_line_key, oh.extn_host_order_ref, ol.extn_host_order_line_ref, ol.shipnode_key, wc.ship_node "
		+"  from thd01.yfs_order_line ol, "
		 +"thd01.yfs_order_header oh, thd01.hd_will_call wc where oh.order_header_key in ('201412021703599758254815','201503011401172740326648') and oh.order_header_key = ol.order_header_key and " 
		 +"oh.order_header_key = wc.order_header_key and ol.extn_will_call_line_num= wc.will_call_line_number and ol.line_type='SO' and "
		 +"  ol.EXTN_WILL_CALL_LINE_NUM > 0 and ol.delivery_method='PICK' and ol.order_line_key in ('201412021703599758254817','201503011401172740326715')" ;*/ 


		try {
			//Class.forName(sscDriverClass);
			// Sterling connection opens here
			//sterCon = DriverManager.getConnection(sscConnectionURL, sscUName,
				//	sscUPassword);

			stmt1 = null;
			stmt1 = sscConn.createStatement();
			ResultSet resultSet = null;

			resultSet = stmt1.executeQuery(query);
			updateQueryList = new ArrayList<String>();
			while (resultSet.next()) {
				//String lineNo = resultSet.getString(1).trim();
				//String hdpsiKey = resultSet.getString(2).trim();
				String orderNo = resultSet.getString(3).trim();
				//String ShipnodeKey=resultSet.getString(5).trim();
				String Shipnode=resultSet.getString(6).trim();
				String linekey=resultSet.getString(2).trim();
				//String linekey=resultSet.getString(3).trim();
//				updateQueryList.add(resultSet.getString(1));
				updateQueryList.add(Shipnode+","+linekey+","+orderNo);
			}
			stmt1.close();
			sscConn.close();
			resultSet.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		logger.debug(updateQueryList.size() + " records found");
		return updateQueryList;
	}

	public int updateHDPSILineNumbers1(ArrayList<String> updateQueryList) {
		Connection sterCon = null;
		Statement stmt1 = null;
		String query = null;
		ResultSet resultSet = null;
		int counter = 0;
		try {
			//Class.forName(atcDriverClass);
			// Sterling connection opens here
			//sterCon = DriverManager.getConnection(atcConnectionURL, atcUName,
				//	atcUPassword);
			logger.debug("Starting updates");
			for (String updateQuery : updateQueryList) {
				if (updateQuery.length() > 10) {
					stmt1 = null;
					resultSet = null;
					query = null;
					stmt1 = atcConn.createStatement();
//					query = updateQuery;
					String tokens[] = updateQuery.split(",");
					String Shipnode = tokens[0];
					String linekey = tokens[1];
					String orderNo = tokens[2];
					
					
					//if(Shipnode.length()<4)
					//if(Shipnode.startsWith("0"))
					//{
					//Shipnode = "0"+Shipnode;
						//query = "update thd01.YFS_ORDER_LINE  set shipnode_key='"+Shipnode+"',modifyuserid ='COMSupport',modifyts = sysdate " +
							//	"where order_line_key = '"+linekey+"'";
						
					
				//	}
					//System.out.println("ShipNode,query"+Shipnode+query);
					query = "update thd01.YFS_ORDER_LINE  set shipnode_key='"+Shipnode+"',modifyuserid ='COMSupport',modifyts = sysdate " +
							"where order_line_key = '"+linekey+"'";
					
					logger.debug("Update #"
							+ updateQueryList.indexOf(updateQuery) + " of "
							+ updateQueryList.size() + ":  " + updateQuery);
					//System.out.println("Update #"
					//		+ updateQueryList.indexOf(updateQuery) + " of "
					//		+ updateQueryList.size() + ":  " + updateQuery);
					counter += 1;
					resultSet = stmt1.executeQuery(query);
					atcConn.commit();
					stmt1.close();
				}

			}
			logger.debug("Completed updates successfully for " + counter
					+ " records");
			atcConn.close();
			resultSet.close();
		} catch (Exception e) {
			e.printStackTrace();
			logger.debug("Got exception but completed updates successfully for "
					+ counter + " records");
		}

		return counter;

	}

	/*
	 * public String encrypt(String str) { try { // Encode the string into bytes
	 * using utf-8 byte[] utf8 = str.getBytes("UTF8");
	 * 
	 * // Encrypt byte[] enc = ecipher.doFinal(utf8);
	 * 
	 * // Encode bytes to base64 to get a string return new
	 * sun.misc.BASE64Encoder().encode(enc); } catch
	 * (javax.crypto.BadPaddingException e) { } catch (IllegalBlockSizeException
	 * e) { } catch (UnsupportedEncodingException e) { } catch
	 * (java.io.IOException e) { } return null; }
	 * 
	 * public String decrypt(String str) { try { // Decode base64 to get bytes
	 * byte[] dec = new sun.misc.BASE64Decoder().decodeBuffer(str);
	 * 
	 * // Decrypt byte[] utf8 = dcipher.doFinal(dec);
	 * 
	 * // Decode using utf-8 return new String(utf8, "UTF8"); } catch
	 * (javax.crypto.BadPaddingException e) { } catch (IllegalBlockSizeException
	 * e) { } catch (UnsupportedEncodingException e) { } catch
	 * (java.io.IOException e) { } return null; }
	 */
}
