package com.homedepot.di.dl.support.QOCDraftOrderDeletion.dao;

import java.io.BufferedReader;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import org.apache.log4j.Logger;

public class QOCDraftOrderDeletion {
	final static Logger logger = Logger.getLogger(QOCDraftOrderDeletion.class);
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		/*String filelocation = "C:\\Tax\\StoreNos.csv";
		try {
			QOCDraftOrderDeletionMethod(filelocation);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
	}
		
		public static void QOCDraftOrderDeletionMethod(
	            String filelocation) throws Exception {

	 logger.info("Inside QOCDraftOrderDeletionMethod");

	                try {
	            BufferedReader br = new BufferedReader(new FileReader(filelocation));
	           /* ArrayList<String> AllOrders= new ArrayList<String> ();
	            ArrayList<String> WithoutHotNotesOrder= new ArrayList<String> ();
	            ArrayList<String> OrderStatusFalse = new ArrayList<String> ();
	            ArrayList<String> OrdersHotNotesInserted = new ArrayList<String> ();*/
	            String line = "";
	            StringBuilder queryBuilder = new StringBuilder();
	            int headerRowsDeleted=0;
	            int totalHeaderRowsDeleted=0;
	            int LineRowsDeleted= 0;
	            
	           int totalLineRowsDeleted = 0;
	            String HeaderDelQuery=null;
	            String LineDelQuery = null;
	            String StoreNumber=null;
	            Connection sscConn = null;
	            Statement stmt = null;
	            Statement stmt2 = null;
	            ResultSet rset = null;
	            /*DBConnectionUtil dbConn = new DBConnectionUtil();		
	    		 sscConn = dbConn.getJNDIConnection(Constants.STERLING_ATC_RW_JNDI);*/
	    		
	            final String driverClass = "oracle.jdbc.driver.OracleDriver";
	 			Class.forName(driverClass);
	 			sscConn = DriverManager.getConnection(
			 "jdbc:oracle:thin:@snpagor05-scan.homedepot.com:1521/QA01SVC_COMFLF01",
			 "QA01_STERLING" , "QA01_STERLING");
	 			

	            while ((line = br.readLine()) != null) {
	            	boolean hflag = false;
		            boolean lflag= false;
		            logger.debug("Inside first while");
	                            if (!(",,".equalsIgnoreCase(line))) {
	                                           logger.debug("Inside if");
	                                String tokens[] = line.split(",");
	                                  StoreNumber = tokens[0].trim();
	                            
	                                int len = StoreNumber.length();
	                                if (len < 4) {

	    								for (int i = 0; i < (4 - len); i++) {
	    									StoreNumber = "0" + StoreNumber;
	    								}
	    							     logger.info("StoreNumber"+StoreNumber);

	    							}
	                          //queryBuilder.append("'" + StoreNumber + "',");
	                            }
	            	
 HeaderDelQuery ="delete from HD_ORDER where HD_ORDER_KEY in (select a.HD_ORDER_KEY from hd_order a ,yfs_order_header b " 
		 +" where a.HOST_ORDER_REF = b.EXTN_HOST_ORDER_REF " 
		 	+" and a.DOC_TYPE = '0001' and b.DOCUMENT_TYPE = '0001' and b.ENTERPRISE_KEY = 'HDUS' "
		 	+" and b.DRAFT_ORDER_FLAG = 'Y' and b.SELLER_ORGANIZATION_CODE in ('"+StoreNumber+"')) and ROWNUM < 10000";
 
 LineDelQuery = "delete from HD_ORDER_LINE where HD_ORDER_LINE_KEY in ("
		 +" select a.HD_ORDER_LINE_KEY from hd_order_line a ,yfs_order_header b where a.HOST_ORDER_NUMBER = b.EXTN_HOST_ORDER_REF"
		 +" and a.DOC_TYPE = '0001' and b.DOCUMENT_TYPE = '0001' and b.ENTERPRISE_KEY = 'HDUS' " 
		 +" and b.DRAFT_ORDER_FLAG = 'Y' and b.SELLER_ORGANIZATION_CODE in ('"+StoreNumber+"')) and ROWNUM < 10000";
		
 logger.info("Query " + HeaderDelQuery);
 logger.info("Query2 " + LineDelQuery);
	            
 	
 	
 	
 	while (hflag == false || lflag == false)
 	{ logger.info("HFLAG:"+hflag);
 	logger.info("lflag:"+lflag);
 	 	if(hflag == false)
 	 	{ 
 	 		//System.out.println("Inside header if");
 	 		stmt = sscConn.createStatement();
 	headerRowsDeleted = stmt.executeUpdate(HeaderDelQuery);
 	sscConn.commit();
 	 	}
 	 	if(lflag == false){
 	 		
 	 		//System.out.println("Inside Line if");
 	 		stmt2 = sscConn.createStatement();
 	LineRowsDeleted =  stmt.executeUpdate(LineDelQuery);
 	sscConn.commit();
 	 	}
 	  
	
 		if(headerRowsDeleted != 0 || LineRowsDeleted != 0 )
 		{
 			logger.info("Checking for the no of rows deleted");
 			totalHeaderRowsDeleted = totalHeaderRowsDeleted + headerRowsDeleted;
 			totalLineRowsDeleted= totalLineRowsDeleted + LineRowsDeleted;
 		}
 		if(headerRowsDeleted == 0)
 		{
 			hflag = true;
 		}
 		if(LineRowsDeleted == 0)
 		{
 			lflag = true;
 		}
 	}
 		{
 			continue;
 		}
 	
	            		
		// TODO Auto-generated method stub

	                
	}
	            logger.info("No of header rows deleted "+totalHeaderRowsDeleted);
	            logger.info("No of line rows deleted "+totalLineRowsDeleted);
	            br.close();
}
	                
	               
catch (Exception e) {
		            e.printStackTrace();
		            throw e;
}
}
}
