package com.homedepot.di.dl.support.QOCDraftOrderDeletion.web;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.homedepot.di.dl.support.QOCDraftOrderDeletion.dao.QOCDraftOrderDeletion;


/**
 * Servlet implementation class QOCDraftOrdersDeletionServlet
 */
public class QOCDraftOrdersDeletionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public QOCDraftOrdersDeletionServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		
		String filelocation = "C:\\Tax\\StoreNos.csv";
		//QOCDraftOrderDeletion qdm= new QOCDraftOrderDeletion();
		try {
			QOCDraftOrderDeletion.QOCDraftOrderDeletionMethod(filelocation);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
