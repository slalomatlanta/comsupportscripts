package com.homedepot.di.dl.support.switchToNormal.web;

import java.io.IOException;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage.RecipientType;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.homedepot.di.dl.support.switchToNormal.dao.SwitchTransmissionState;
import org.apache.log4j.Logger;
/**
 * Servlet implementation class SwitchTransmissionStateServlet
 */
public class SwitchTransmissionStateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	final static Logger logger = Logger.getLogger(SwitchTransmissionStateServlet.class);
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SwitchTransmissionStateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{	
		
		//List<String> storeList = new ArrayList<String>();
		String stores="";
		//System.out.println("inside servlet");
		SwitchTransmissionState s = new SwitchTransmissionState();
		try {
			stores=s.transmissionState();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String message = "";
		message = message
				+ "<html>"
				+ "<body>"
				+"<p>Hi Team,</p>";
		
		if(!stores.isEmpty())
		{
			message = message + "<p>Below are the store that were brought back to normal after full resync</p>" + stores;
			
				
			
			message = message + "<br/><br/>Thanks,<br/>COM Multichannel Support</body></html>";
			//System.out.println(message);
			sendReport(message);
		}
		
		else
		{
			message = message + "<p>No stores were brought back to Normal</p><br/>";
			logger.info("No stores were brought back to Normal" );
			message = message + "<br/><br/>Thanks,<br/>COM Multichannel Support</body></html>";
			//sendReport(message);
		}
		
		
		//System.out.println(message);
		
		// TODO Auto-generated method stub
	}

		/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}
	
	public static void sendReport(String msgContent) {
		final String user = "horizon@cpliisad.homedepot.com";// change
																// accordingly
		final String password = "xxx";// change accordingly

		String host = "mail1.homedepot.com";
		Properties props = new Properties();
		props.put("mail.host", host);
		Session session = Session.getInstance(props,
				new javax.mail.Authenticator() {
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(user, password);
					}
				});

		try {

			String[] to = {"_2fc77b@homedepot.com"};
			//String[] to = {"mohanraj_gurusamy@homedepot.com"};
			//String[] Cc = {"_2fc77b@homedepot.com"};
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress(user));
			DateFormat dateFormat = new SimpleDateFormat(
					"MM/dd/yyyy hh:mm a");
			// get current date time with Date()
			Date today = new Date();
			String date1 = dateFormat.format(today);
			logger.info(date1);
			message.setSubject("Stores stuck in SUSPEND state after resync - " + date1);
			
			logger.info("Stores stuck in SUSPEND state after resync - " + date1);
			// message.setText("This is simple program of sending email using JavaMail API");
			message.setFrom(new InternetAddress(user));
			InternetAddress[] addressTo = new InternetAddress[to.length];
			//InternetAddress[] addressCc = new InternetAddress[Cc.length];
			for (int i = 0; i < to.length; i++) {
				addressTo[i] = new InternetAddress(to[i]);
				logger.debug("To Address " + to[i]);
			}
			/*for (int j = 0; j < Cc.length; j++) {
				addressCc[j] = new InternetAddress(Cc[j]);
				System.out.println("Cc Address " + Cc[j]);
			}*/
			message.setRecipients(RecipientType.TO, addressTo);
			//message.setRecipients(RecipientType.CC, addressCc);
			message.setContent(msgContent, "text/html");
			Transport.send(message);
			for (int i = 0; i < to.length; i++) {
				addressTo[i] = new InternetAddress(to[i]);
				logger.debug("Message delivered to--- " + to[i]);
			}
			/*for (int j = 0; j < Cc.length; j++) {
				addressCc[j] = new InternetAddress(Cc[j]);
				System.out.println("Message delivered to cc recepients--- "
						+ Cc[j]);
			}*/

		} catch (MessagingException e) {
			e.printStackTrace();
		}

	}

}
