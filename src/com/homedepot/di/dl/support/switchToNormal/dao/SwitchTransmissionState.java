package com.homedepot.di.dl.support.switchToNormal.dao;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.homedepot.di.dl.support.util.Constants;
import com.homedepot.di.dl.support.util.ConstantsQuery;
import com.homedepot.di.dl.support.util.DBConnectionUtil;

public class SwitchTransmissionState
{
	final static Logger logger = Logger.getLogger(SwitchTransmissionState.class);
	public static String transmissionState() throws SQLException
	//public static void main(String args[])
	{
		ConstantsQuery cons = new ConstantsQuery();
		String message="";

		
		List<String> storeList = new ArrayList<String>();
		
		Connection sscConn = null;
		Statement st = null;		
		ResultSet result = null;
		try
		{	
			DBConnectionUtil dbConn = new DBConnectionUtil();
			sscConn = dbConn.getJNDIConnection(Constants.STERLING_SSC_RO_JNDI);
			/*final String connectionURL = "jdbc:oracle:thin:@//pprmm78x.homedepot.com:1521/dpr77mm_sro01";
			final String driverClass = "oracle.jdbc.driver.OracleDriver";
			final String uName = "MMUSR01";
			final String uPassword = "COMS_MMUSR01";
			Class.forName(driverClass);
			Connection sscConn = DriverManager.getConnection(connectionURL, uName,
					uPassword);*/
			//Statement st = null;
			st = sscConn.createStatement();
			//ResultSet result = null;
			
			logger.debug(cons.yfsExport);
			result = st.executeQuery(cons.yfsExport);
			
			while (result.next()) 
			{
				
				String userRef = result.getString(6);
				String store = userRef.substring(50, 54);
				//String store ="1039";
				logger.debug(store);
				message = message + getStorestatus(store);
								
			}
		}
		
		catch(Exception e)
		{
			logger.error("Sterling : " + e.getStackTrace());
			//e.printStackTrace();
			message = "Sterling : Error connecting DB";
		}
		
		finally
		{
		result.close();
		st.close();
		sscConn.close();
		}
		
		logger.info("Below are the stores that were brought back to NORMAL");
		logger.info(message);
		
		return message;
	}

	public static String getStorestatus(String store) throws IOException, SQLException 
	{
		ConstantsQuery cons = new ConstantsQuery();
		List<String> storeList = new ArrayList<String>();
		String status=null;
		String message="";
		
        Connection con = null;
        ResultSet result = null;
        Statement stmt = null;
		try
		{
			DBConnectionUtil dbConn = new DBConnectionUtil();
			con = dbConn.getStoreconnection(store);
            logger.info(cons.transmissionState);
            
            stmt = con.createStatement();
            result = stmt.executeQuery(cons.transmissionState);
            
            while(result.next())
            {
            	status=result.getString(6).trim();
            	logger.info(status);
            }
            
		}
		
		
		catch(Exception e)
		{
			logger.error("Store : " + e.getStackTrace());
			//e.printStackTrace();
			message = "Store : Error connecting DB";
		}
		
		finally
		{
		result.close();
		stmt.close();
		con.close();
		}
		
		if("SUSPEND".equalsIgnoreCase(status))
		{
			message = message + "<br/>" + store;
			switchState(store);
		
		}
		
		return message;
	}
	
	public static void switchState(String store) throws IOException, SQLException 
	{
			
		String output="";
		
		output = hitUrl(store,true);
			
			if("".equalsIgnoreCase(output))
			{
				output = hitUrl(store,false);
			}
			
			logger.info("waiting");
			try {
				Thread.sleep(15000);                 //1000 milliseconds is one second.
				} catch(InterruptedException ex) {
				Thread.currentThread().interrupt();
				}
			logger.info("continue");
			getStorestatus(store);		
		
	}
	
	
	public static String hitUrl(String store, boolean b)
	{
		String inputLine = "", output = "";
		
		String url;
		
		if(b)
			url = "http://st"+store+".homedepot.com:12080/COMSupplyExtractor/ResumeTransmission?start";
		else
			url = "http://st"+store+".homedepot.com/COMSupplyExtractor/ResumeTransmission?start";
				
		try 
		{
			URL resumeTrans = new URL(url);

			URLConnection resumeTransConn = resumeTrans.openConnection();
			resumeTransConn.connect();
			logger.info(resumeTransConn);
			BufferedReader in = new BufferedReader(
					new InputStreamReader(
							resumeTransConn.getInputStream()));
			
			while ((inputLine = in.readLine()) != null) {
				if (!(inputLine == null || ""
						.equalsIgnoreCase(inputLine))) {
					output = output + inputLine;
				}
			}
			in.close();
			
			
			
		} 
		catch (Exception e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		logger.debug(output);
		return output;
	}

}
