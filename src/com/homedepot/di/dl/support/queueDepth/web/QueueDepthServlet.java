package com.homedepot.di.dl.support.queueDepth.web;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage.RecipientType;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.homedepot.di.dl.support.queueDepth.dao.QueueDepth;

import org.apache.log4j.Logger;
/**
 * Servlet implementation class QueueDepthServlet
 */
public class QueueDepthServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	final static Logger logger = Logger.getLogger(HttpServlet.class);
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public QueueDepthServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		logger.debug("Queue Depth Report");
		String message = "";
		message = message
				+ "<!DOCTYPE html>"
				+ "<html>"
				+ "<body>"
				+"<p>Hi Team,</p>"
				+"<p>PFB current queue depth of the order create queues at Sterling and COM.</p>"
				+"<br/>";
		QueueDepth queue = new QueueDepth();
		String msgContent = queue.getQueueDetails();
		
message = message + msgContent + "</body></html><br/><br/>Thanks,<br/>COM Multichannel Support";
	logger.debug(message);
		sendReport(message);
		
		logger.debug("Program ran successfully");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}
	
	public static void sendReport(String msgContent) {
		final String user = "horizon@cpliisad.homedepot.com";// change
																// accordingly
		final String password = "xxx";// change accordingly

		String host = "mail1.homedepot.com";
		Properties props = new Properties();
		props.put("mail.host", host);
		Session session = Session.getInstance(props,
				new javax.mail.Authenticator() {
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(user, password);
					}
				});

		try {

			//String[] to = {"anandhakumar_chinnadurai@homedepot.com"};
			//String[] Cc = {"anandhakumar_chinnadurai@homedepot.com"};
			String[] to = {"HARI_RAMAMURTHY2@homedepot.com"};
			String[] Cc = {"_2fc77b@homedepot.com"};
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress(user));
			DateFormat dateFormat = new SimpleDateFormat(
					"MM/dd/yyyy hh:mm:ss a");
			// get current date time with Date()
			Date today = new Date();
			String date1 = dateFormat.format(today);
			logger.debug(date1);
			message.setSubject("Queue depth on order creates - " + date1);
			// message.setText("This is simple program of sending email using JavaMail API");
			message.setFrom(new InternetAddress(user));
			InternetAddress[] addressTo = new InternetAddress[to.length];
			InternetAddress[] addressCc = new InternetAddress[Cc.length];
			for (int i = 0; i < to.length; i++) {
				addressTo[i] = new InternetAddress(to[i]);
				logger.debug("To Address " + to[i]);
			}
			for (int j = 0; j < Cc.length; j++) {
				addressCc[j] = new InternetAddress(Cc[j]);
				logger.debug("Cc Address " + Cc[j]);
			}
			message.setRecipients(RecipientType.TO, addressTo);
			message.setRecipients(RecipientType.CC, addressCc);
			message.setContent(msgContent, "text/html");
			Transport.send(message);
			for (int i = 0; i < to.length; i++) {
				addressTo[i] = new InternetAddress(to[i]);
				logger.debug("Message delivered to--- " + to[i]);
			}
			for (int j = 0; j < Cc.length; j++) {
				addressCc[j] = new InternetAddress(Cc[j]);
				logger.debug("Message delivered to cc recepients--- "
						+ Cc[j]);
			}

		} catch (MessagingException e) {
			e.printStackTrace();
		}

	}

}
