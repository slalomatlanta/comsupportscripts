package com.homedepot.di.dl.support.queueDepth.dao;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import javax.jms.Message;
import javax.jms.Queue;
import javax.jms.QueueBrowser;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueSession;
import javax.jms.Session;
import javax.naming.Context;
import javax.naming.InitialContext;

import org.apache.log4j.Logger;


public class QueueDepth {
	
	final static Logger logger = Logger.getLogger(QueueDepth.class);

	/**
	 * @param args
	 */
	
		
		public static String getQueueDetails() 
		{
			String msgContent = "";
			msgContent = msgContent + "<table border=\"1\" cellpadding=\"0\" cellspacing=\"0\" width=\"200px\" style=\"border-collapse:collapse;\">";
			
			msgContent = msgContent + "<tr><th>Queue Name</th>";
			msgContent = msgContent + "<th>Queue Depth</th></tr>";
			String msg="";
			
			List<String> queueList = new ArrayList<String>();
			queueList.add("DI.DL.COM.CREATEORDER");
			queueList.add("DI.DL.STERLING.CREATEORDER");
			//System.out.println(queueName);
			//int queueDepth=0;
			for (String queueName : queueList)
			{
				
				if(queueName.contains("STERLING")){
					msg =  msg + getQueue("jms/" + queueName + "_get", "jms/MQGetQCF",
					5, queueName);
					
				}
				else
				{
					msg = msg + getQueue("jms/" + queueName + "_get", "jms/MQGetQCF_COM",
							5, queueName);
				}
			}
			
			msgContent = msgContent  + msg + "</table> <br/> <br/>";
					
			
			return msgContent;
		}
		
		public static String getQueue(String resourceName, String lookIpName,
				int no, String queueName) {
			//List<QueueDTO> xmlList = new ArrayList<QueueDTO>();
			logger.info(queueName);
			Context context = null;
			InitialContext ctx = null;
			Queue queue = null;
			QueueConnectionFactory connFactory = null;
			QueueConnection queueConn = null;
			QueueSession queueSession = null;
			QueueBrowser queueBrowser = null;
			int count=0;
			try {
				ctx = new InitialContext();
				context = (Context) ctx.lookup("java:comp/env");
				// System.out.println("Context" + context);
				queue = (Queue) context.lookup(resourceName);

				// System.out.println("here 1");
				connFactory = (QueueConnectionFactory) context.lookup(lookIpName);
				// System.out.println("here 2");
				// System.out.println("Factory" + connFactory);
				queueConn = connFactory.createQueueConnection();
				// System.out.println("QueueConn" + queueConn);
				queueSession = queueConn.createQueueSession(false,
						Session.AUTO_ACKNOWLEDGE);
				queueBrowser = queueSession.createBrowser(queue);

				// System.out.println("queueConn.start() Time: " +
				// Calendar.getInstance().getTime());
				queueConn.start();

				// browse the messages
				Enumeration e = null;
				e = queueBrowser.getEnumeration();
				
				// count number of messages
				while (e.hasMoreElements()) {
					Message message = (Message) e.nextElement();
		            count++;
				}

				queueConn.close();


			} catch (Exception e1) {
				logger.error("Queue Exception: " + e1);
				e1.printStackTrace();
			} finally {

			}
			String msgContent="";
			logger.info(queueName +" has " + count +" messages ");
			msgContent = msgContent + "<tr>";
			msgContent = msgContent + "<td>"+queueName+"</td>";
			msgContent = msgContent + "<td>"+count+"</td>";
			msgContent = msgContent +"</tr>";
			
			return msgContent;

		}
		
		
}
