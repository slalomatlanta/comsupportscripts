package com.homedepot.di.dl.support.EOCPendingPublish.dao;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeMessage.RecipientType;

import org.apache.log4j.Logger;

import com.homedepot.di.dl.support.EOCPendingPublish.web.EOCPendingPublishServlet;
import com.homedepot.di.dl.support.util.Constants;
import com.homedepot.di.dl.support.util.DBConnectionUtil;

public class EOCPendingPublish 
{

	final static Logger logger = Logger.getLogger(EOCPendingPublish.class);
	
	
	public static void publishOrder() throws Exception
	
	{
		//String outputFile = "c:\\Tax\\EOC\\EOCPendingPublish.csv";
		String outputFile = "/opt/isv/apache-tomcat/temp/EOCPendingPublish.csv";
		
		FileWriter fw = new FileWriter(outputFile, true);
		
		fw.append("Order Number");
		fw.append(',');
		fw.append("EOC_PENDING_PUBLISH_KEY");
		fw.append(',');
		fw.append("ORDER_NO");
		fw.append(',');
		fw.append("ORDER_HEADER_KEY");
		fw.append(',');
		fw.append("EXTN_HOST_ORDER_REF");
		fw.append(',');
		fw.append("STATUS");
		fw.append(',');
		fw.append("EOC_PUBLISH_TIMESTAMP");
		fw.append(',');
		fw.append("EXCEPTION_CODE");
		fw.append(',');
		fw.append("EXCEPTION_MESSAGE");
		fw.append(',');
		fw.append("DELETE_ORDER_FLAG");
		fw.append(',');
		fw.append("Record Removal Status");
		fw.append("\n");
		
		int totalDeleted =0;
		int rowDeleted = 0;
		
		try {
			DBConnectionUtil dbConn = new DBConnectionUtil();

			Connection con = dbConn
					.getJNDIConnection(Constants.STERLING_SSC_RO_JNDI);

			Statement stmt = null;
			ResultSet rs = null;

			stmt = con.createStatement();
			
			String getOrder = "select yoh.extn_host_order_ref,eoc.* from thd01.EOC_PENDING_PUBLISH eoc ,"
							+" thd01.yfs_order_header yoh where eoc.order_header_key = yoh.order_header_key";
			//logger.info(getOrder);
			rs = stmt.executeQuery(getOrder);
			
			while(rs.next())
			{
				String orderNo = rs.getString(1);
				String publishKey = rs.getString(2);
				//logger.info("Order : "+ orderNo +"     publishKey : "+ publishKey);
				
				fw.append(rs.getString(1));
				fw.append(",");
				fw.append(rs.getString(2));
				fw.append(",");
				fw.append(rs.getString(3));
				fw.append(",");
				fw.append("'"+rs.getString(4));
				fw.append(",");
				fw.append(rs.getString(5));
				fw.append(",");
				fw.append(rs.getString(6));
				fw.append(",");
				fw.append(rs.getString(7));
				fw.append(",");
				fw.append(rs.getString(8));
				fw.append(",");
				fw.append(rs.getString(9));
				fw.append(",");
				fw.append(rs.getString(10));
				fw.append(",");
				String recallXML = recallOrder(orderNo);
				//logger.info(recallXML);
				String eocUpdateorderResponse = processRecallxml(recallXML);
				logger.info(eocUpdateorderResponse);
				
				if(eocUpdateorderResponse.equalsIgnoreCase("SUCCESS"))
				{
					rowDeleted = deleteFromeocPendingpublish(publishKey);
					totalDeleted = totalDeleted + rowDeleted;
					fw.append(eocUpdateorderResponse);
					fw.append("\n");
				}
				else
				{
					fw.append(eocUpdateorderResponse);
					fw.append("\n");
				}
				
				
			}
			logger.info("Total number of records deleted : "+totalDeleted);
			rs.close();
			stmt.close();
			con.close();
		}
		
		catch(Exception e)
		{
			logger.debug("EOCPendingPublish Exception : " + e);
		}
		
		fw.flush();
		fw.close();
		
		BufferedReader br = new BufferedReader(new FileReader(outputFile));
		String line = "";
	    int count = 0;
	    while ((line = br.readLine()) != null) 
	    {
	    	count+=1;
	    }

	    if(count > 1)
	    {
	    	
	    	sendAttachment(totalDeleted);
	    }
	    
	    else
	    {
	    	logger.info("No records found");
	    }
	}


	
	
	public static int deleteFromeocPendingpublish(String publishKey) {
		// TODO Auto-generated method stub
		
		String deleteQuery = "delete from thd01.eoc_pending_publish where eoc_pending_publish_key in ('"+publishKey+"')";
		int rowsDeleted = 0;
		//logger.info(deleteQuery);
		try {
			DBConnectionUtil dbConn = new DBConnectionUtil();

			Connection con = dbConn
					.getJNDIConnection(Constants.STERLING_ATC_RW_JNDI);

			Statement stmt = null;
			
			stmt = con.createStatement();
			
			rowsDeleted = stmt.executeUpdate(deleteQuery);
			con.commit();
			stmt.close();
			con.close();
						
		}
		
		catch(Exception e)
		{
			logger.debug("EOCPendingPublish Exception while deleting : " + e);
		}
		
		return rowsDeleted;
	}




	public static String processRecallxml(String recallXML) {

		String sterlingResponseXML = "";
		String url = null;
		String sterlingInputXML = null;

		sterlingInputXML = recallXML;

		String queryString = Utils.createQueryString("HDEOCUpdateOrder",
				sterlingInputXML);
		SterlingJerseyClient sterlingJerseyClient = new SterlingJerseyClient();

		url = String
				.format("http://multichannel-sterling.homedepot.com/smcfs/interop/InteropHttpServlet");
		//System.out.println("URL : " + url);

		sterlingResponseXML = sterlingJerseyClient.send(url, queryString);

		//System.out.println("Sterling HDProcessTransaction Response : " + sterlingResponseXML);
		
		sterlingResponseXML = Utils.removeXMLDeclaration(sterlingResponseXML);
		//logger.info(sterlingResponseXML);
		String response = checkResponse(sterlingResponseXML);
		return response;

	}




	public static String checkResponse(String response) {

		String result = "";
		if (response == null || "".equalsIgnoreCase(response)) {
			result = "No response";
		} else if (response.contains("<OrderList>")) {
			result = "SUCCESS";
		} else if (response.contains("<Errors>")) {
			
			result = "FAILURE";
		}

		//System.out.println("Response status : " + result);
		return result;

	}




	public static String recallOrder(String orderNo) {

		String sterlingResponseXML = "";
		String url = null;
		String sterlingInputXML = null;

		sterlingInputXML = "<Order EnterpriseCode='HDUS' DocumentType='0001' MaximumRecords='100'>"
				+" <Extn ExtnHostOrderReference='"
				+orderNo
				+"' ExtnIncludeReleaseDetails='Y' ExtnIncludeInstructions='Y' ExtnIncludeAuditDetails='Y'"
				+" ExtnIncludeAlertDetails='Y' ExtnPutOrderOnLock='N' ExtnUserId='COMSupport' ExtnIncludeEventDetails='Y' ExtnIncludeStatusDetails='Y'" 
				+" ExtnIncludeWorkOrderDetails='Y' ExtnIncludePurchaseOrderDetails='Y' ExtnIncludeReturnOrderDetails='Y'/></Order>";

		String queryString = Utils.createQueryString("HDRecall",
				sterlingInputXML);
		SterlingJerseyClient sterlingJerseyClient = new SterlingJerseyClient();

		url = String
				.format("http://multichannel-sterling.homedepot.com/smcfs/interop/InteropHttpServlet");
		//System.out.println("URL : " + url);

		sterlingResponseXML = sterlingJerseyClient.send(url, queryString);

		//System.out.println("Sterling HDProcessTransaction Response : " + sterlingResponseXML);
		
		//sterlingResponseXML = Utils.removeXMLDeclaration(sterlingResponseXML);
		return sterlingResponseXML;

	}
	

	public static void sendAttachment(int rowsDeleted) throws Exception{
		 String host = "mail1.homedepot.com";
	  String from = "horizon@cpliisad.homedepot.com";
	  String[] to = {"_2fc77b@homedepot.com" };
	  //String[] Cc ={""};
	  // Get system properties
	  Properties properties = System.getProperties();

	  // Setup mail server
	  properties.setProperty("mail.smtp.host", host);

	  // Get the default Session object.
	  Session session = Session.getInstance(properties);

	  // Define message
	  Message message = new MimeMessage(session);
	  message.setFrom(new InternetAddress(from));
	  InternetAddress[] addressTo = new InternetAddress[to.length];
	 // InternetAddress[] addressCc = new InternetAddress[Cc.length];
	  for (int i = 0; i < to.length; i++) 
	  { addressTo[i] = new InternetAddress(to[i]); 
	  //System.out.println("To Address "+to[i]);
	  } 
//	  for (int j = 0; j < Cc.length; j++) 
//	  { addressCc[j] = new InternetAddress(Cc[j]); 
//	  System.out.println("Cc Address "+Cc[j]);
//	  }
	  message.setRecipients(RecipientType.TO, addressTo); 
	  //message.setRecipients(RecipientType.CC, addressCc); 
	  message.setSubject("EOC Pending Publish");

	  // Create the message part 
	  BodyPart messageBodyPart = new MimeBodyPart();
	  String msgContent = "Hi All,<br/><br/>PFA orders deleted from EOC_PENDING_PUBLISH after processing.<br/> Total Records Deleted : "+rowsDeleted+"<br/><br/>Thanks<br/>";
   msgContent = msgContent + "COM Multichannel Support";
	  // Fill the message
	  messageBodyPart.setContent(msgContent ,"text/html");
	  	

	  Multipart multipart = new MimeMultipart();
	  multipart.addBodyPart(messageBodyPart);

	  // Part two is attachment
	  messageBodyPart = new MimeBodyPart();
	  //String filename = "c:\\Tax\\EOC\\EOCPendingPublish.csv";
	  String filename = "/opt/isv/apache-tomcat/temp/EOCPendingPublish.csv";
	  DataSource source = new FileDataSource(filename);
	  messageBodyPart.setDataHandler(new DataHandler(source));
	  messageBodyPart.setFileName(source.getName());
	  multipart.addBodyPart(messageBodyPart);

	  // Put parts in message
	  message.setContent(multipart);

	  // Send the message
	  Transport.send(message);
	   //System.out.println("Msg Send ....");
	  }

}
