package com.homedepot.di.dl.support.EOCPendingPublish.dao;


import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

	
	/**
	 * Jersey Client to call the Sterling services for the Order Interface
	 * <br>
	 * This class handles the communication to and from the service
	 *  
	 */
	public class SterlingJerseyClient {
		
		
		/**
		 * This method creates a jersey client and hits the Sterling. It
		 * gets the response for the postRequest sent.
		 * 
		 * @param url
		 * @param postRequest
		 * @throws ProcessingException
		 */
		public String send(String url, String postRequest)	{	
			
			long startTime = System.currentTimeMillis();
			
			//Map<KeyNameEnum, Object> infoLogger = OrangeLogManager.getLoggerMap(Thread.currentThread().getName());
			
			
			// Create Jersey client
			Client client = Client.create();
			client.setConnectTimeout(600000);
			client.setReadTimeout(600000);
			String token = Utils.getToken();
			
			// Create WebResource 
			WebResource webResource = client.resource(url);
			ClientResponse response = null;
			
			try {				
				response = webResource.header("THDService-Auth", token)
				.type("application/x-www-form-urlencoded")
				.post(ClientResponse.class, postRequest);
			} 
			catch (Exception e)
			{
				System.out.println("Unexpected error from webresource: " + e.getMessage());
				System.out.println("Sterling Response Time: " + (System.currentTimeMillis()-startTime));
				
			}
			// Retrieve status and check if it is OK
			int status = response.getStatus();
			
			System.out.println("Callout HTTP Response: " + status);

			if (status != 200) {
				System.out.println("Unsuccesful Status response from Sterling: " + status);
				System.out.println("Sterling Response Time: " + (System.currentTimeMillis()-startTime));
				 
			}
			String sterlingResponse = response.getEntity(String.class);
			
			if (sterlingResponse == null) {
				System.out.println("Application Error during call to Sterling - Response is null");
				System.out.println("Sterling Response Time: " + (System.currentTimeMillis()-startTime));
				}
			 
			// Log results
			
			System.out.println("Sterling Response Time: " + (System.currentTimeMillis()-startTime));
			
			return sterlingResponse;
		}
}
