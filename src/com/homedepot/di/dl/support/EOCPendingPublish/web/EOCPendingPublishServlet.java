package com.homedepot.di.dl.support.EOCPendingPublish.web;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.homedepot.di.dl.support.EOCPendingPublish.dao.EOCPendingPublish;



/**
 * Servlet implementation class EOCPendingPublishServlet
 */
public class EOCPendingPublishServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	final static Logger logger = Logger.getLogger(EOCPendingPublishServlet.class);  
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EOCPendingPublishServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		logger.info("EOCPendingPublishServlet program started at : "
				+ java.util.GregorianCalendar.getInstance().getTime());
		try {
			EOCPendingPublish.publishOrder();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		logger.info("EOCPendingPublishServlet program completed at : "
				+ java.util.GregorianCalendar.getInstance().getTime());
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
