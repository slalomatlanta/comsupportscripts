package com.homedepot.di.dl.support.storeOfflineCheck.dto;

public class StoreDetailDTO {
 private String store;
 private String storeStatusFlag;
 private String storeStatusFlagDesc;
 private String lastUpdTs;
 private String StatusUpdTs;
 private String PrevStatusUpdTs;
 
 
 public String getStore() {
	return store;
}
public void setStore(String store) {
	this.store = store;
}
public String getStoreStatusFlag() {
	return storeStatusFlag;
}
public void setStoreStatusFlag(String storeStatusFlag) {
	this.storeStatusFlag = storeStatusFlag;
}
public String getStoreStatusFlagDesc() {
	return storeStatusFlagDesc;
}
public void setStoreStatusFlagDesc(String storeStatusFlagDesc) {
	this.storeStatusFlagDesc = storeStatusFlagDesc;
}
public String getLastUpdTs() {
	return lastUpdTs;
}
public void setLastUpdTs(String lastUpdTs) {
	this.lastUpdTs = lastUpdTs;
}
public String getStatusUpdTs() {
	return StatusUpdTs;
}
public void setStatusUpdTs(String statusUpdTs) {
	StatusUpdTs = statusUpdTs;
}
public String getPrevStatusUpdTs() {
	return PrevStatusUpdTs;
}
public void setPrevStatusUpdTs(String prevStatusUpdTs) {
	PrevStatusUpdTs = prevStatusUpdTs;
}

}
