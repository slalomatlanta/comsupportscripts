package com.homedepot.di.dl.support.storeOfflineCheck.service;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import org.apache.log4j.Logger;

import au.com.bytecode.opencsv.CSVReader;

import com.homedepot.di.dl.support.storeOfflineCheck.dto.StoreDetailDTO;
import com.homedepot.di.dl.support.util.Constants;
import com.homedepot.di.dl.support.util.DBConnectionUtil;

public class StoreOfflineUsingThread {
	private static final Logger logger = Logger
			.getLogger(StoreOfflineUsingThread.class);

	public static void main(String args[]) {

		checkStoreStatus();
	}

	public static void checkStoreStatus() {
		try {
			ArrayList<String> storeList = new ArrayList<String>();
			
//			String inputfilelocation = "C:\\test\\storeList.csv";
			String inputfilelocation = "/opt/isv/tomcat/temp/storeList.csv";

			
			storeList = getStoreList(inputfilelocation);
			logger.debug("Program Started");
			// DBConnectionUtil dbConn = new DBConnectionUtil();
			// Connection con = dbConn
			// .getJNDIConnection(Constants.COM_SUPPORT_MYSQL_JNDI);
			// if (con != null) {
			RunnableDemo.threadList.clear();
			RunnableDemo.storeDetailsMap.clear();
			// RunnableDemo.updateCount = 0;
			// RunnableDemo.insertCount = 0;
			int count = 0;
			for (String store : storeList) {
//				count++;
//				if (count % 500 == 0) {
//					try {
//						System.out.println("Waiting for 2 seonds...");
//						Thread.sleep(2500); // 1000 milliseconds is one second.
//					} catch (InterruptedException ex) {
//						Thread.currentThread().interrupt();
//					}
//				}
				RunnableDemo R1 = new RunnableDemo(store);
				R1.startThread();
			}

			logger.info(RunnableDemo.threadList.size());
			for (Thread curThread : RunnableDemo.threadList) {
				try {
					// starting from the first wait for each one to finish.
					curThread.join();
				} catch (InterruptedException e) {
					logger.error("Exception occurred in thread join: "
							+ e);
				}
			}

//			 String filelocation = "C:\\test\\OfflineStoreDetails.csv";
			String filelocation = "/opt/isv/tomcat/temp/OfflineStoreDetails.csv";
			FileWriter writer = new FileWriter(filelocation);

			writer.append("Store Number");
			writer.append(",");
			writer.append("Status Flag");
			writer.append(",");
			writer.append("Status Desc");
			writer.append(",");
			writer.append("Last Upd Ts");
			writer.append(",");
			writer.append("\n");

			for (String store : RunnableDemo.storeDetailsMap.keySet()) {
				StoreDetailDTO sd = RunnableDemo.storeDetailsMap.get(store);
				writer.append(sd.getStore());
				writer.append(",");
				writer.append(sd.getStoreStatusFlag());
				writer.append(",");
				writer.append(sd.getStoreStatusFlagDesc());
				writer.append(",");
				writer.append(sd.getLastUpdTs());
				writer.append(",");
				writer.append("\n");
			}

			writer.flush();
			writer.close();
			logger.debug("File created at " + filelocation);

			logger.info("All threads got completed... Exiting Program");
			logger.info(RunnableDemo.threadList.size());
			// con.close();
			// System.out.println("Connection Closed");
			// System.out.println("Total No of rows updated: " +
			// RunnableDemo.updateCount);
			// System.out.println("Total No of rows inserted: " +
			// RunnableDemo.insertCount);

			// }
			// else{
			// System.out.println("Connection is null");
			// }
		} catch (Exception e) {
			logger.error("Exception occurred in main: " + e);
		}
	}

	public static ArrayList<String> getStoreList(String filelocation) {
		ArrayList<String> storeList = new ArrayList<String>();
//		try {
//			DBConnectionUtil dbConn = new DBConnectionUtil();
//			Connection con = null;
//			con = dbConn.getJNDIConnection(Constants.COM_SUPPORT_MYSQL_JNDI);
//			Statement stmt = null;
//			stmt = con.createStatement();
//			ResultSet rs = stmt
//					.executeQuery("select distinct STR_NBR from STORE_LIST order by STR_NBR");
//			while (rs.next()) {
//				storeList.add(rs.getString(1));
//			}
//		} catch (Exception e) {
//			System.out
//					.println("Exception occurred in getStoreList method " + e);
//			e.printStackTrace();
//		}
		String[] row = null;
		CSVReader csvReader = null;
		boolean headerRow = true;
		try {
			csvReader = new CSVReader(new FileReader(filelocation));
			while ((row = csvReader.readNext()) != null) {
				if (headerRow) {
					headerRow = false;
				} else {					
					if ((row[0].trim()).length() == 4) {
						storeList.add(row[0]);
					} else {
						storeList.add("0" + row[0]);
					}
				}
			}
			csvReader.close();
		} catch (Exception e) {
			logger.error("Exception Occured " + e);
		}
		return storeList;
	}

}

class RunnableDemo implements Runnable {
	private Thread t;
	private String storeNumber;
	// Connection con;
	static ArrayList<Thread> threadList = new ArrayList<Thread>();
	// static ArrayList<StoreDetailDTO> offlineStoreDetails = new
	// ArrayList<StoreDetailDTO>();
	static HashMap<String, StoreDetailDTO> storeDetailsMap = new HashMap<String, StoreDetailDTO>();

	// static int updateCount;
	// static int insertCount;

	RunnableDemo(String name) {
		storeNumber = name;
		// this.con = conn;
		// System.out.println("Creating " + storeNumber);
	}

	RunnableDemo() {

	}

	public void run() {
		// System.out.println("Running " + storeNumber);

		String inputLine = "", output = "", result = "";
		try {
			// resumeTrans.setConnectTimeout(2000);
			URL resumeTrans = new URL("http://st" + storeNumber
					+ ".homedepot.com/MMSVOUSWeb/rs/ous/isComOnline");
			URLConnection resumeTransConn = resumeTrans.openConnection();
			resumeTransConn.setConnectTimeout(5000);
			BufferedReader in = new BufferedReader(new InputStreamReader(
					resumeTransConn.getInputStream()));

			while ((inputLine = in.readLine()) != null) {
				if (!(inputLine == null || "".equalsIgnoreCase(inputLine))) {
					output = output + inputLine;
				}
			}
			in.close();
			int firstIndex = output.indexOf("<strOnline>");
			int lastIndex = output.indexOf("</strOnline>");
			result = output.substring((firstIndex + 11), lastIndex);
			// System.out.println("store " + store + " flag is " + result);

			DateFormat dateFormat = new SimpleDateFormat(
					"yyyy/MM/dd HH:mm:ss.SSS");
			Date today = new Date();
			String sysdate = dateFormat.format(today);
			String flagDesc = "";
			if (result.trim().equalsIgnoreCase("Y")) {
				flagDesc = "online";
			} else if (result.trim().equalsIgnoreCase("N")) {
				flagDesc = "offline";
			} else {
				flagDesc = "Timeout";
			}

			StoreDetailDTO sd = new StoreDetailDTO();
			sd.setStore(storeNumber);
			sd.setLastUpdTs(sysdate);
			sd.setStoreStatusFlag(result);
			sd.setStoreStatusFlagDesc(flagDesc);
			// offlineStoreDetails.add(sd);
			storeDetailsMap.put(storeNumber, sd);

			// System.out.println(sysdate + ":" + storeNumber + " is " +
			// result);

			// DBConnectionUtil dbConn = new DBConnectionUtil();
			// Connection con =
			// dbConn.getJNDIConnection(Constants.COM_SUPPORT_MYSQL_JNDI);

			// updateinMySqlDB(storeNumber, result, sysdate, con);

		} catch (Exception e) {
			//logger.error("Exception Occurred for st" + storeNumber	+ " : " + e);
				e.printStackTrace();
			result = "T";
			DateFormat dateFormat = new SimpleDateFormat(
					"yyyy/MM/dd HH:mm:ss.SSS");
			Date today = new Date();
			String sysdate = dateFormat.format(today);

			String flagDesc = "";
			if (result.trim().equalsIgnoreCase("Y")) {
				flagDesc = "online";
			} else if (result.trim().equalsIgnoreCase("N")) {
				flagDesc = "offline";
			} else {
				flagDesc = "Timeout";
			}

			StoreDetailDTO sd = new StoreDetailDTO();
			sd.setStore(storeNumber);
			sd.setLastUpdTs(sysdate);
			sd.setStoreStatusFlag(result);
			sd.setStoreStatusFlagDesc(flagDesc);
			// offlineStoreDetails.add(sd);
			storeDetailsMap.put(storeNumber, sd);

			// System.out.println(sysdate + ": " + storeNumber + " is " +
			// result);
			// updateinMySqlDB(storeNumber, result, sysdate, con);
		}

		// System.out.println("Thread " + storeNumber + " exiting.");
	}

	public void startThread() {
		// System.out.println("Starting " + storeNumber);
		if (t == null) {
			t = new Thread(this, storeNumber);
			threadList.add(t);
			t.start();
		}
	}

	// public synchronized void updateinMySqlDB(String storeNumber, String
	// result,
	// String sysdate, Connection con) {
	// // System.out.println("Inside updateinMySqlDB method");
	// try {
	// String flagDesc = "";
	// if (result.trim().equalsIgnoreCase("Y")) {
	// flagDesc = "online";
	// } else if (result.trim().equalsIgnoreCase("N")) {
	// flagDesc = "offline";
	// } else {
	// flagDesc = "Timeout";
	// }
	// String updateQuery = "";
	// boolean storePresent = false;
	// Statement stmt = null;
	// Statement stmt1 = null;
	// stmt = con.createStatement();
	// ResultSet rs = stmt
	// .executeQuery("SELECT * FROM STORE_OFFLINE_CHECK WHERE STR_NBR='"
	// + storeNumber.trim() + "' ");
	// while (rs.next()) {
	// String statusFlagInDb = rs.getString(2).trim();
	// if (statusFlagInDb.equals(result.trim())) {
	// updateQuery = "UPDATE STORE_OFFLINE_CHECK SET LAST_UPD_TS='"
	// + sysdate
	// + "' WHERE STR_NBR='"
	// + storeNumber.trim() + "' ";
	// } else {
	// updateQuery = "UPDATE STORE_OFFLINE_CHECK SET STR_STATUS_FLAG = '"
	// + result.trim()
	// + "', STR_STATUS_FLAG_DESC = '"
	// + flagDesc.trim()
	// + "', STAT_UPD_TS = '"
	// + sysdate
	// + "', PREV_STAT_UPD_TS = '"
	// + rs.getTimestamp(5).toString().trim()
	// + "', LAST_UPD_TS='"
	// + sysdate
	// + "' WHERE STR_NBR='" + storeNumber.trim() + "' ";
	// }
	// storePresent = true;
	// }
	// rs.close();
	// stmt.close();
	// if (storePresent) {
	// // System.out.println(updateQuery);
	// stmt1 = con.createStatement();
	// int count = stmt1.executeUpdate(updateQuery);
	// updateCount = updateCount + count;
	// System.out.println(storeNumber.trim()+" updated successfully in DB - "+updateCount);
	// } else {
	// String insertQuery =
	// "INSERT INTO STORE_OFFLINE_CHECK (STR_NBR, STR_STATUS_FLAG, STR_STATUS_FLAG_DESC, LAST_UPD_TS, STAT_UPD_TS, PREV_STAT_UPD_TS) VALUES ('"
	// + storeNumber.trim()
	// + "' , '"
	// + result.trim()
	// + "' , '"
	// + flagDesc.trim()
	// + "' , '"
	// + sysdate
	// + "' , '" + sysdate + "' , '" + sysdate + "') ";
	// // System.out.println(insertQuery);
	// stmt1 = con.createStatement();
	// int count = stmt1.executeUpdate(insertQuery);
	// insertCount = insertCount + count;
	// System.out.println(storeNumber.trim()+" inserted successfully in DB - "+insertCount);
	// }
	// stmt1.close();
	// } catch (Exception e) {
	// System.out.println("Exception occurred while updating for store "
	// + storeNumber + " " + e);
	//
	// }
	// }

}
