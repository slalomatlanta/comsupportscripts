package com.homedepot.di.dl.support.storeOfflineCheck.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import com.homedepot.di.dl.support.storeOfflineCheck.dao.UpdateStoreListToMySQLDB;

/**
 * Servlet implementation class UpdateStoreListToMySQLDBServlet
 */
public class UpdateStoreListToMySQLDBServlet extends HttpServlet {
	private static final Logger logger = Logger.getLogger(UpdateStoreListToMySQLDBServlet.class);
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateStoreListToMySQLDBServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		logger.info("UpdateStoreListToMySQLDB Servlet started at : "+java.util.GregorianCalendar.getInstance().getTime());
		UpdateStoreListToMySQLDB.updateStoreList();
		logger.info("UpdateStoreListToMySQLDB Servlet completed at : "+java.util.GregorianCalendar.getInstance().getTime());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
