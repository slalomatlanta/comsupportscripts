package com.homedepot.di.dl.support.storeOfflineCheck.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.homedepot.di.dl.support.storeOfflineCheck.dao.StoresOffline;

/**
 * Servlet implementation class StoresOfflineServlet
 */
public class StoresOfflineServlet extends HttpServlet {
	private static final Logger logger = Logger.getLogger(StoresOfflineServlet.class);
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public StoresOfflineServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		logger.info("Program started at : "+java.util.GregorianCalendar.getInstance().getTime());
		StoresOffline.updateStoreStatus();
		logger.info("Program completed at : "+java.util.GregorianCalendar.getInstance().getTime());		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
