package com.homedepot.di.dl.support.storeOfflineCheck.web;

import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import au.com.bytecode.opencsv.CSVReader;

import com.homedepot.di.dl.support.storeOfflineCheck.dto.StoreDetailDTO;

/**
 * Servlet implementation class GetStoresV2
 */
public class GetStoresV2 extends HttpServlet {
	private static final Logger logger = Logger.getLogger(GetStoresV2.class);
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public GetStoresV2() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		logger.debug("Inside GetStores Servlet");
//		String filelocation = "C:\\test\\OfflineStoreDetails.csv";
		String filelocation = "/opt/isv/tomcat/temp/OfflineStoreDetails.csv";
		HashMap<String, StoreDetailDTO> storeDetailsMap = new HashMap<String, StoreDetailDTO>();

		response.setContentType("text/html");
		PrintWriter out = null;
		out = response.getWriter();
		try {
			// ArrayList<StoreDetailDTO> offlineStoreDetails = new
			// ArrayList<StoreDetailDTO>();
			// String driverClass = "com.mysql.jdbc.Driver";
			// final String connectionURL =
			// "jdbc:mysql://cpliisad.homedepot.com:3306/multichannel_PR";
			// final String uName = "COMSupport";
			// final String uPassword = "su990rt";
			// Connection con = null;
			// Class.forName(driverClass);
			// con = DriverManager.getConnection(connectionURL, uName,
			// uPassword);
			// DBConnectionUtil dbConn = new DBConnectionUtil();
			// Connection con = dbConn
			// .getJNDIConnection(Constants.COM_SUPPORT_MYSQL_JNDI);
			// Statement stmt = null;
			// stmt = con.createStatement();
			// boolean rowsPresent = false, isCritical = false;
			int offlineStoreCount = 0;
			int timeOutStoreCount = 0;
			int onlineStoreCount = 0;
			// ResultSet rs = stmt
			// .executeQuery("SELECT * FROM STORE_OFFLINE_CHECK ORDER BY STR_STATUS_FLAG, STAT_UPD_TS ");
			// while (rs.next()) {
			// StoreDetailDTO db = new StoreDetailDTO();
			// db.setStore(rs.getString(1).trim());
			// db.setStoreStatusFlag(rs.getString(2).trim());
			// if (db.getStoreStatusFlag().equals("N")) {
			// offlineStoreCount++;
			// } else if (db.getStoreStatusFlag().equals("T")) {
			// timeOutStoreCount++;
			// } else {
			// onlineStoreCount++;
			// }
			// db.setStoreStatusFlagDesc(rs.getString(3).trim());
			// db.setLastUpdTs(rs.getTimestamp(4).toString().trim());
			// db.setStatusUpdTs(rs.getTimestamp(5).toString().trim());
			// db.setPrevStatusUpdTs(rs.getTimestamp(6).toString().trim());
			// offlineStoreDetails.add(db);
			// rowsPresent = true;
			// }

			// HashMap<String, StoreDetailDTO> inputDetails = new
			// HashMap<String, StoreDetailDTO>();
			String[] row = null;
			CSVReader csvReader = null;
			boolean headerRow = true;
			try {
				csvReader = new CSVReader(new FileReader(filelocation));
				while ((row = csvReader.readNext()) != null) {
					if (headerRow) {
						headerRow = false;
					} else {

						StoreDetailDTO sb = new StoreDetailDTO();

						if ((row[0].trim()).length() == 4) {
							sb.setStore(row[0]);
						} else {
							sb.setStore("0" + row[0]);
						}

						sb.setStoreStatusFlag(row[1].trim());

						if (sb.getStoreStatusFlag().equals("N")) {
							offlineStoreCount++;
						} else if (sb.getStoreStatusFlag().equals("T")) {
							timeOutStoreCount++;
						} else {
							onlineStoreCount++;
						}

						sb.setStoreStatusFlagDesc(row[2].trim());
						sb.setLastUpdTs(row[3].trim());
						storeDetailsMap.put(row[0].trim(), sb);
					}
				}
				csvReader.close();
			} catch (Exception e) {
				logger.error("Exception Occured " + e);
			}

			String emailContent = "", table = "", offlineTable = "", timeOutTable = "", onlineTable = "";
			emailContent = "<html><head><body>" + "<font face=\"calibri\">";
			emailContent = emailContent
					+ "<h2><center>Store Offline Check</center></h2><br/>";
			emailContent = emailContent + "<h3>Current offline stores count : "
					+ offlineStoreCount + "</h3></p>";
			String tableHeader = "<table border=\"1\" cellpadding=\"1\" cellspacing=\"0\" width=\"200px\" style=\"border-collapse:collapse;\">"
					+ "<font face=\"calibri\">"
					+ "<tr>"
					+ "<th>Store Number</th>"
					+ "<th>Store Staus Flag</th>"
					+ "<th>Status Desc</th>" + "<th>Last_upd_ts</th>"
					// + "<th>stat_upd_ts</th>"
					// + "<th>prev_stat_upd_ts</th>"
					+ "</tr>";

			for (String store : storeDetailsMap.keySet()) {
				StoreDetailDTO db = storeDetailsMap.get(store);
				if (db.getStoreStatusFlag().trim().equals("N")) {
					offlineTable = offlineTable + "<tr>"
							+ "<td align=\"center\">" + db.getStore() + "</td>"
							+ "<td align=\"center\">" + db.getStoreStatusFlag()
							+ "</td>" + "<td align=\"center\">"
							+ db.getStoreStatusFlagDesc() + "</td>" + "<td>"
							+ db.getLastUpdTs() + "</td>"
							// + "<td>"
							// + db.getStatusUpdTs() + "</td>" + "<td>"
							// + db.getPrevStatusUpdTs() + "</td>"
							+ "</tr>";
				} else if (db.getStoreStatusFlag().trim().equals("T")) {
					timeOutTable = timeOutTable + "<tr>"
							+ "<td align=\"center\">" + db.getStore() + "</td>"
							+ "<td align=\"center\">" + db.getStoreStatusFlag()
							+ "</td>" + "<td align=\"center\">"
							+ db.getStoreStatusFlagDesc() + "</td>" + "<td>"
							+ db.getLastUpdTs() + "</td>"
							// + "<td>"
							// + db.getStatusUpdTs() + "</td>" + "<td>"
							// + db.getPrevStatusUpdTs() + "</td>"
							+ "</tr>";
				} else {
					onlineTable = onlineTable + "<tr>"
							+ "<td align=\"center\">" + db.getStore() + "</td>"
							+ "<td align=\"center\">" + db.getStoreStatusFlag()
							+ "</td>" + "<td align=\"center\">"
							+ db.getStoreStatusFlagDesc() + "</td>" + "<td>"
							+ db.getLastUpdTs() + "</td>"
							// + "<td>"
							// + db.getStatusUpdTs() + "</td>" + "<td>"
							// + db.getPrevStatusUpdTs() + "</td>"
							+ "</tr>";
				}
			}
			table = table + tableHeader + offlineTable + "</table><br/><hr/>";
			table = table + "<h3>Current Timeout stores count : "
					+ timeOutStoreCount + "</h3></p>" + tableHeader
					+ timeOutTable + "</table><br/><hr/>";
			table = table + "<h3>Current Online stores count : "
					+ onlineStoreCount + "</h3></p>" + tableHeader
					+ onlineTable + "</table>";
			emailContent = emailContent + table + "</body></head></html>";
			out.println(emailContent);
		} catch (Exception e) {
			logger.error("Exception occurred in getStores servlet " + e);
			e.printStackTrace();
		}
	}

	public static HashMap<String, StoreDetailDTO> getDetailsFromCSV(
			String fileLocation) {
		HashMap<String, StoreDetailDTO> inputDetails = new HashMap<String, StoreDetailDTO>();
		String[] row = null;
		CSVReader csvReader = null;
		// boolean headerRow = true;
		try {
			csvReader = new CSVReader(new FileReader(fileLocation));
			while ((row = csvReader.readNext()) != null) {
				// if (headerRow) {
				// headerRow = false;
				// } else {
				//
				StoreDetailDTO sb = new StoreDetailDTO();

				if ((row[0].trim()).length() == 4) {
					sb.setStore(row[0]);
				} else {
					sb.setStore("0" + row[0]);
				}

				sb.setStoreStatusFlag(row[1].trim());
				sb.setStoreStatusFlagDesc(row[2].trim());
				sb.setLastUpdTs(row[3].trim());
				inputDetails.put(row[0].trim(), sb);
			}
			// }
			csvReader.close();
		} catch (Exception e) {
			logger.error("Exception Occured " + e);
		}
		return inputDetails;
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
