package com.homedepot.di.dl.support.storeOfflineCheck.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import org.apache.log4j.Logger;
import com.homedepot.di.dl.support.util.Constants;
import com.homedepot.di.dl.support.util.DBConnectionUtil;


public class UpdateStoreListToMySQLDB {
	private static final Logger logger = Logger.getLogger(UpdateStoreListToMySQLDB.class);
	public static void main (String args[]){
		updateStoreList();
	}
	
	public static void updateStoreList(){
		ArrayList<String> storeList = new ArrayList<String>();
		try {
//			final String driverClass = "oracle.jdbc.driver.OracleDriver";
//			final String connectionURL = "jdbc:oracle:thin:@pprmm78x.homedepot.com:1521/dpr78mm_sro01";
//			final String uName = "MMUSR01";
//			final String uPassword = "COMS_MMUSR01";
//			Class.forName(driverClass);
//			Connection con = null;
//			con = DriverManager.getConnection(connectionURL, uName, uPassword);
			DBConnectionUtil dbConn = new DBConnectionUtil();
			Connection con = dbConn.getJNDIConnection(Constants.COMT_SSC_RO_JNDI);
			Statement stmt = null;
			stmt = con.createStatement();
			ResultSet result = null;
			String query = "select distinct loc_nbr from thd01.comt_loc_capbl where COMT_CAPBL_ID = 7 order by loc_nbr";
			result = stmt.executeQuery(query);
			if (result != null) {
				while (result.next()) {
					storeList.add(result.getString(1));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		logger.info("Current store count from COM Tier: " + storeList.size());
		try {
//			String driverClass = "com.mysql.jdbc.Driver";
//			final String connectionURL = "jdbc:mysql://cpliisad.homedepot.com:3306/multichannel_PR";
//			final String uName = "COMSupport";
//			final String uPassword = "su990rt";
//			Connection con = null;
//			Class.forName(driverClass);
//			con = DriverManager.getConnection(connectionURL, uName, uPassword);
			DBConnectionUtil dbConn = new DBConnectionUtil();
			Connection con = dbConn.getJNDIConnection(Constants.COM_SUPPORT_MYSQL_JNDI);
			
			int updateCount = 0, insertCount = 0;
			for (String store : storeList) {
				try {
					DateFormat dateFormat = new SimpleDateFormat(
							"yyyy/MM/dd HH:mm:ss.SSS");
					Date today = new Date();
					String sysdate = dateFormat.format(today);
					String updateQuery = "";
					boolean storePresent = false;
					Statement stmt = null;
					Statement stmt1 = null;
					stmt = con.createStatement();
					ResultSet rs = stmt
							.executeQuery("SELECT * FROM STORE_LIST WHERE STR_NBR='"
									+ store.trim() + "' ");
					while (rs.next()) {
							updateQuery = "UPDATE STORE_LIST SET LAST_UPD_TS='"
									+ sysdate
									+ "' WHERE STR_NBR='"
									+ store.trim() + "' ";
						storePresent = true;
					}
					rs.close();
					stmt.close();
					if (storePresent) {
//						System.out.println(updateQuery);
						stmt1 = con.createStatement();
						int count = stmt1.executeUpdate(updateQuery);
						updateCount = updateCount + count;
					} else {
						String insertQuery = "INSERT INTO STORE_LIST (STR_NBR, LAST_UPD_TS) VALUES ('"
								+ store.trim()
								+ "' , '"
								+ sysdate
								+ "') ";
//						System.out.println(insertQuery);
						stmt1 = con.createStatement();
						int count = stmt1.executeUpdate(insertQuery);
						insertCount = insertCount + count;
					}
					stmt1.close();
				} catch (Exception e) {
					logger.error("Exception occurred while updating for store "+ store + " " + e);
					continue;
				}
			}
			logger.info("Number of rows updated : " + updateCount);
			logger.info("Number of stores inserted : " + insertCount);
		} catch (Exception e) {
			logger.error("Exception occurred while opening a connection "+ e);
			e.printStackTrace();
		}
	}
}
