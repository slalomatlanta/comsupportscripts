package com.homedepot.di.dl.support.storeOfflineCheck.dao;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeMessage.RecipientType;

import au.com.bytecode.opencsv.CSVReader;

import com.homedepot.di.dl.support.storeOfflineCheck.dto.StoreDetailDTO;
import com.homedepot.di.dl.support.storeOfflineCheck.service.*;
import com.homedepot.di.dl.support.util.Constants;
import com.homedepot.di.dl.support.util.DBConnectionUtil;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import org.apache.log4j.Logger;
public class StoresOffline {
	private static final Logger logger = Logger.getLogger(StoresOffline.class);
	
	public static void main(String args[]) {
		logger.info("Program started at : "
				+ java.util.GregorianCalendar.getInstance().getTime());
		updateStoreStatus();
		logger.info("Program completed at : "
				+ java.util.GregorianCalendar.getInstance().getTime());
	}

	public static void updateStoreStatus() {
		ArrayList<String> storeList = new ArrayList<String>();
		
//		String filelocation = "C:\\test\\storeList.csv";
		String filelocation = "/opt/isv/tomcat/temp/storeList.csv";
		storeList = getStoreList(filelocation);
		
		ArrayList<StoreDetailDTO> storeDetailsToUpdate = new ArrayList<StoreDetailDTO>();
		for (String store : storeList) {
			StoreDetailDTO db = new StoreDetailDTO();
			try {
				// resumeTrans.setConnectTimeout(2000);
				URL resumeTrans = new URL("http://st" + store
						+ ".homedepot.com/MMSVOUSWeb/rs/ous/isComOnline");
				URLConnection resumeTransConn = resumeTrans.openConnection();
				resumeTransConn.setConnectTimeout(5000);
				BufferedReader in = new BufferedReader(new InputStreamReader(
						resumeTransConn.getInputStream()));
				String inputLine = "", output = "", result = "";
				while ((inputLine = in.readLine()) != null) {
					if (!(inputLine == null || "".equalsIgnoreCase(inputLine))) {
						output = output + inputLine;
					}
				}
				in.close();
				int firstIndex = output.indexOf("<strOnline>");
				int lastIndex = output.indexOf("</strOnline>");
				result = output.substring((firstIndex + 11), lastIndex);
				// logger.info("store " + store + " flag is " + result);
				db.setStore(store);
				db.setStoreStatusFlag(result);
				DateFormat dateFormat = new SimpleDateFormat(
						"yyyy/MM/dd HH:mm:ss.SSS");
				Date today = new Date();
				String sysdate = dateFormat.format(today);
				db.setLastUpdTs(sysdate);
				if (result.trim().equalsIgnoreCase("Y")) {
					db.setStoreStatusFlagDesc("online");
				} else if (result.trim().equalsIgnoreCase("N")) {
					db.setStoreStatusFlagDesc("offline");
				} else {
					db.setStoreStatusFlagDesc("Time Out");
				}
				storeDetailsToUpdate.add(db);
			} catch (Exception e) {
				logger.error("Exception occurred on store " + store + " "
						+ e);
				db.setStore(store);
				db.setStoreStatusFlag("T");
				DateFormat dateFormat = new SimpleDateFormat(
						"yyyy/MM/dd HH:mm:ss.SSS");
				Date today = new Date();
				String sysdate = dateFormat.format(today);
				db.setLastUpdTs(sysdate);
				db.setStoreStatusFlagDesc("Timeout");
				storeDetailsToUpdate.add(db);
				continue;
			}
		}
		checkOfflineStores(storeDetailsToUpdate);
		updateStoreDetailsInDb(storeDetailsToUpdate);
	}

	public static void updateStoreDetailsInDb(
			ArrayList<StoreDetailDTO> storeDetailsToUpdate) {
		try {
//			DBConnectionUtil dbConn = new DBConnectionUtil();
//			// String driverClass = "com.mysql.jdbc.Driver";
//			// final String connectionURL =
//			// "jdbc:mysql://cpliisad.homedepot.com:3306/multichannel_PR";
//			// final String uName = "COMSupport";
//			// final String uPassword = "su990rt";
//			Connection con = null;
//			// Class.forName(driverClass);
//			// con = DriverManager.getConnection(connectionURL, uName,
//			// uPassword);
//			con = dbConn.getJNDIConnection(Constants.COM_SUPPORT_MYSQL_JNDI);
//			int updateCount = 0, insertCount = 0;
//			for (StoreDetailDTO db : storeDetailsToUpdate) {
//				try {
//					String updateQuery = "";
//					boolean storePresent = false;
//					Statement stmt = null;
//					Statement stmt1 = null;
//					stmt = con.createStatement();
//					ResultSet rs = stmt
//							.executeQuery("SELECT * FROM STORE_OFFLINE_CHECK WHERE STR_NBR='"
//									+ db.getStore().trim() + "' ");
//					while (rs.next()) {
//						String statusFlagInDb = rs.getString(2).trim();
//						if (statusFlagInDb.equals(db.getStoreStatusFlag()
//								.trim())) {
//							updateQuery = "UPDATE STORE_OFFLINE_CHECK SET LAST_UPD_TS='"
//									+ db.getLastUpdTs()
//									+ "' WHERE STR_NBR='"
//									+ db.getStore().trim() + "' ";
//						} else {
//							updateQuery = "UPDATE STORE_OFFLINE_CHECK SET STR_STATUS_FLAG = '"
//									+ db.getStoreStatusFlag()
//									+ "', STR_STATUS_FLAG_DESC = '"
//									+ db.getStoreStatusFlagDesc()
//									+ "', STAT_UPD_TS = '"
//									+ db.getLastUpdTs()
//									+ "', PREV_STAT_UPD_TS = '"
//									+ rs.getTimestamp(5).toString().trim()
//									+ "', LAST_UPD_TS='"
//									+ db.getLastUpdTs()
//									+ "' WHERE STR_NBR='"
//									+ db.getStore().trim() + "' ";
//						}
//						storePresent = true;
//					}
//					rs.close();
//					stmt.close();
//					if (storePresent) {
//						// System.out.println(updateQuery);
//						stmt1 = con.createStatement();
//						int count = stmt1.executeUpdate(updateQuery);
//						updateCount = updateCount + count;
//					} else {
//						String insertQuery = "INSERT INTO STORE_OFFLINE_CHECK (STR_NBR, STR_STATUS_FLAG, STR_STATUS_FLAG_DESC, LAST_UPD_TS, STAT_UPD_TS, PREV_STAT_UPD_TS) VALUES ('"
//								+ db.getStore().trim()
//								+ "' , '"
//								+ db.getStoreStatusFlag().trim()
//								+ "' , '"
//								+ db.getStoreStatusFlagDesc().trim()
//								+ "' , '"
//								+ db.getLastUpdTs()
//								+ "' , '"
//								+ db.getLastUpdTs()
//								+ "' , '"
//								+ db.getLastUpdTs() + "') ";
//						// System.out.println(insertQuery);
//						stmt1 = con.createStatement();
//						int count = stmt1.executeUpdate(insertQuery);
//						insertCount = insertCount + count;
//					}
//					stmt1.close();
//				} catch (Exception e) {
//					System.out
//							.println("Exception occurred while updating for store "
//									+ db.getStore() + " " + e);
//					continue;
//				}
//			}
//			System.out.println("Number of rows updated : " + updateCount);
//			System.out.println("Number of stores inserted : " + insertCount);
			
//			String filelocation = "C:\\test\\OfflineStoreDetails.csv";
			String filelocation = "/opt/isv/tomcat/temp/OfflineStoreDetails.csv";
			FileWriter writer = new FileWriter(filelocation);

			writer.append("Store Number");
			writer.append(",");
			writer.append("Status Flag");
			writer.append(",");
			writer.append("Status Desc");
			writer.append(",");
			writer.append("Last Upd Ts");
			writer.append(",");
			writer.append("\n");

			for (StoreDetailDTO sd : storeDetailsToUpdate) {
				writer.append(sd.getStore());
				writer.append(",");
				writer.append(sd.getStoreStatusFlag());
				writer.append(",");
				writer.append(sd.getStoreStatusFlagDesc());
				writer.append(",");
				writer.append(sd.getLastUpdTs());
				writer.append(",");
				writer.append("\n");
			}

			writer.flush();
			writer.close();
			logger.info("File created at " + filelocation);
			
		} catch (Exception e) {
			logger.error("Exception occurred while writing to file "
					+ e);
			e.printStackTrace();
		}
	}

	public static void checkOfflineStores(
			ArrayList<StoreDetailDTO> storeDetailsToUpdate) {
		try {
			ArrayList<StoreDetailDTO> offlineStoreDetails = new ArrayList<StoreDetailDTO>();
			// String driverClass = "com.mysql.jdbc.Driver";
			// final String connectionURL =
			// "jdbc:mysql://cpliisad.homedepot.com:3306/multichannel_PR";
			// final String uName = "COMSupport";
			// final String uPassword = "su990rt";
			// Connection con = null;
			// Class.forName(driverClass);
			// con = DriverManager.getConnection(connectionURL, uName,
			// uPassword);
			// Statement stmt = null;
			// stmt = con.createStatement();
			boolean rowsPresent = false, isCritical = false;
			int offlineStoreCount = 0;
			int timeOutStoreCount = 0;
			// ResultSet rs = stmt
			// .executeQuery("SELECT * FROM STORE_OFFLINE_CHECK WHERE STR_STATUS_FLAG in ('N','T') ORDER BY STR_STATUS_FLAG, STAT_UPD_TS ");
			for (StoreDetailDTO db : storeDetailsToUpdate) {
				StoreDetailDTO gb = new StoreDetailDTO();
				if (db.getStoreStatusFlag().equals("N")) {
					gb.setStore(db.getStore().trim());
					gb.setStoreStatusFlag(db.getStoreStatusFlag().trim());
					gb.setStoreStatusFlagDesc(db.getStoreStatusFlagDesc()
							.trim());
					gb.setLastUpdTs(db.getLastUpdTs().trim());
					offlineStoreDetails.add(gb);
					offlineStoreCount++;
				} else if (db.getStoreStatusFlag().equals("T")) {
					gb.setStore(db.getStore().trim());
					gb.setStoreStatusFlag(db.getStoreStatusFlag().trim());
					gb.setStoreStatusFlagDesc(db.getStoreStatusFlagDesc()
							.trim());
					gb.setLastUpdTs(db.getLastUpdTs().trim());
					offlineStoreDetails.add(gb);
					timeOutStoreCount++;
				}
			}
			String emailContent = "", eMailSubject = "";
			emailContent = "<html><head><body>" + "<font face=\"calibri\">";
			emailContent = emailContent + "\n";
			emailContent = emailContent + "Current offline stores count : "
					+ offlineStoreCount + "</p>";
			emailContent = emailContent + "<br/>";
			String table = "<table border=\"1\" cellpadding=\"0\" cellspacing=\"0\" width=\"200px\" style=\"border-collapse:collapse;\">"
					+ "<font face=\"calibri\">"
					+ "<tr>"
					+ "<th>Store Number</th>"
					+ "<th>Store Staus Flag</th>"
					+ "<th>Status Desc</th>" + "<th>Last_upd_ts</th>" + "</tr>";
			for (StoreDetailDTO db : offlineStoreDetails) {
				table = table + "<tr>" + "<td>" + db.getStore() + "</td>"
						+ "<td>" + db.getStoreStatusFlag() + "</td>" + "<td>"
						+ db.getStoreStatusFlagDesc() + "</td>" + "<td>"
						+ db.getLastUpdTs() + "</td>" + "<td>" + "</tr>";
			}
			table = table
					+ "</table>"
					+ "<br/>"
					+ "<a href=\"https://checkstore.apps.homedepot.com/stat\">Click here to monitor this in a browser</a>"
					+ "<br/>"
					+ "<a href=\"https://checkstore.apps.homedepot.com/offline\">Click here to get store numbers in real time</a>"
					+ "<p><i>Note: This is system generated mail. Please do not respond to this.</i></p>"
					+ "</body>" + "</html>";
			;
			emailContent = emailContent + table;
			//System.out.println(emailContent);
			if (offlineStoreCount >= 2 && offlineStoreCount < 5) {
				eMailSubject = "WARN Alert: Synch Stores Offline - ";
				sendMail(emailContent, isCritical, eMailSubject);
			} else if (offlineStoreCount >= 5) {
				isCritical = true;
				eMailSubject = "CRITICAL Alert: Synch Stores Offline - ";
				sendMail(emailContent, isCritical, eMailSubject);
			} else {
				logger.info("Mail is not triggered");
			}
		} catch (Exception e) {
			logger.error("Exception occurred in checkOfflineStores method "	+ e);
			e.printStackTrace();
		}
	}

	public static void sendMail(String emailContent, boolean isCritical,
			String eMailSubject) {
		String text = emailContent;
		String[] to = { "NIRAL_PATEL2@homedepot.com", "DINESH_E@homedepot.com",
				"jagatdeep_chakraborty@homedepot.com",
				"MAHESH_VYAKARANAM@homedepot.com",
				"PURUSHOTHAMAN_RAGHUNATH@homedepot.com",
				"RAKESH_J_SATYA@homedepot.com", "MANOJ_SHUNMUGAM@homedepot.com" };
		String[] Cc = {};
		Session session = null;
		MimeMessage mimeMessage = null;
		MimeBodyPart mimeBodyPart = null;
		Multipart multiPart = null;
		String date = null;
		Calendar calendar = null;
		Properties properties = null;
		String msg = null;
		try {
			msg = "Success";
			calendar = Calendar.getInstance();
			date = calendar.getTime().toString();
			properties = System.getProperties();
			properties.put("mail.host", "mail1.homedepot.com");
			session = Session.getInstance(properties, null);
			mimeMessage = new MimeMessage(session);
			mimeMessage.setFrom(new InternetAddress(
					"horizon@cpliisad.homedepot.com"));
			InternetAddress[] addressTo = new InternetAddress[to.length];
			InternetAddress[] addressCc = new InternetAddress[Cc.length];
			for (int i = 0; i < to.length; i++) {
				addressTo[i] = new InternetAddress(to[i]);
				logger.info("To Address " + to[i]);
			}
			for (int j = 0; j < Cc.length; j++) {
				addressCc[j] = new InternetAddress(Cc[j]);
				logger.info("Cc Address " + Cc[j]);
			}
			mimeMessage.setRecipients(RecipientType.TO, addressTo);
			mimeMessage.setRecipients(RecipientType.CC, addressCc);
			mimeMessage.setSubject(eMailSubject + date);
			mimeBodyPart = new MimeBodyPart();
			mimeBodyPart.setText(text.toString());
			multiPart = new MimeMultipart();
			multiPart.addBodyPart(mimeBodyPart);
			mimeMessage.setContent(text.toString(), "text/html");
			if (text.length() > 0) {
				if (isCritical) {
					mimeMessage.setHeader("X-Priority", "1");
					logger.info("Sending mail as high priority - "
							+ java.util.GregorianCalendar.getInstance()
									.getTime());
				} else {
					mimeMessage.setHeader("X-Priority", "3");
					logger.info("Sending mail... - "
							+ java.util.GregorianCalendar.getInstance()
									.getTime());
				}
				Transport.send(mimeMessage);
				logger.info("Sent mail succesfully...");
			}
		} catch (Exception e) {
			msg = "Failure";
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	public static ArrayList<String> getStoreList(String filelocation) {
		ArrayList<String> storeList = new ArrayList<String>();
		try {			
			// String driverClass = "com.mysql.jdbc.Driver";
			// final String connectionURL =
			// "jdbc:mysql://cpliisad.homedepot.com:3306/multichannel_PR";
			// final String uName = "COMSupport";
			// final String uPassword = "su990rt";
			// Connection con = null;
			// Class.forName(driverClass);
			// con = DriverManager.getConnection(connectionURL, uName,
			// uPassword);
			// Connection con =
//			DBConnectionUtil dbConn = new DBConnectionUtil();
			// dbConn.getJNDIConnection(Constants.COM_SUPPORT_MYSQL_JNDI);
			// Statement stmt = null;
			// stmt = con.createStatement();
			// ResultSet rs = stmt
			// .executeQuery("select distinct STR_NBR from STORE_LIST order by STR_NBR");
			// while (rs.next()) {
			// storeList.add(rs.getString(1));
			// }
			// storeList.add("0121");
//			storeList.add("0049");
			// storeList.add("2222");
//			storeList.add("9796");
			// storeList.add("8949");
			String[] row = null;
			CSVReader csvReader = null;
			try {
				csvReader = new CSVReader(new FileReader(filelocation));
				while ((row = csvReader.readNext()) != null) {
					if ((row[0].trim()).length() == 4) {
						storeList.add(row[0]);
					} else {
						storeList.add("0" + row[0]);
					}
				}
				csvReader.close();
			} catch (Exception e) {
				logger.error("Exception Occured " + e);
			}
		} catch (Exception e) {
			logger.error("Exception occurred in getStoreList method " + e);
			e.printStackTrace();
		}
		return storeList;
	}
}
