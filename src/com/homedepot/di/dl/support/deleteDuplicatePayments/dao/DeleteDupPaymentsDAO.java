package com.homedepot.di.dl.support.deleteDuplicatePayments.dao;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.homedepot.di.dl.support.util.Constants;
import com.homedepot.di.dl.support.util.DBConnectionUtil;

public class DeleteDupPaymentsDAO {
	private static Logger logger = Logger.getLogger(DeleteDupPaymentsDAO.class);

	public ArrayList<String> getImpactedOrderList(String query) {
		ArrayList<String> orderList = new ArrayList<String>();

		try {
			DBConnectionUtil dbConn = new DBConnectionUtil();
			Connection con = dbConn
					.getJNDIConnection(Constants.STERLING_SSC_RO_JNDI);
			Statement stmt = null;
			ResultSet rset = null;

			stmt = con.createStatement();
			rset = stmt.executeQuery(query);

			while (rset.next()) {
				orderList.add(rset.getString(1).trim());
			}

			rset.close();
			stmt.close();
			con.close();

		} catch (Exception e) {
			logger.error("Exception" + e);
			e.printStackTrace();
		}

		return orderList;
	}

	public void getBackUp(ArrayList<String> orderList) {
		try {
			DBConnectionUtil dbConn = new DBConnectionUtil();
			Connection con = dbConn
					.getJNDIConnection(Constants.STERLING_SSC_RO_JNDI);
			Statement stmt = null;
			ResultSet rset = null;

			//String filename1 = "C:\\test\\HD_CHARGE_TXN_ADDITIONAL.csv";
			//String filename2 = "C:\\test\\YFS_CREDIT_CARD_TRANSACTION.csv";
			//String filename3 = "C:\\test\\YFS_CHARGE_TRANSACTION.csv";
			String filename1 = "/opt/isv/apache-tomcat/temp/HD_CHARGE_TXN_ADDITIONAL.csv";
			String filename2 = "/opt/isv/apache-tomcat/temp/YFS_CREDIT_CARD_TRANSACTION.csv";
			String filename3 = "/opt/isv/apache-tomcat/temp/YFS_CHARGE_TRANSACTION.csv";

			String headerKeys = "";

			for (String orderHeaderKey : orderList) {
				headerKeys = headerKeys + "'" + orderHeaderKey + "',";
			}

			headerKeys = headerKeys.substring(0, (headerKeys.length() - 1));

			String selectQuery1 = "SELECT * FROM THD01.HD_CHARGE_TXN_ADDITIONAL WHERE ORDER_HEADER_KEY IN ("
					+ headerKeys + ") AND PAYMENT_SEQ_NO = '0' ";

			String selectQuery2 = "select * from THD01.YFS_CREDIT_CARD_TRANSACTION where CHARGE_TRANSACTION_KEY in (select CHARGE_TRANSACTION_KEY from THD01.YFS_CHARGE_TRANSACTION where ORDER_HEADER_KEY in (select order_header_key from THD01.YFS_CHARGE_TRANSACTION where ORDER_HEADER_KEY in ("
					+ headerKeys
					+ ") and CREATEUSERID = 'HDOrderAsyncServer' and MODIFYUSERID = 'HDOrderAsyncServer') and CREATEUSERID = 'admin' and MODIFYUSERID = 'admin') ";

			String selectQuery3 = "Select * from THD01.YFS_CHARGE_TRANSACTION where ORDER_HEADER_KEY in (select order_header_key from THD01.YFS_CHARGE_TRANSACTION where ORDER_HEADER_KEY in ("
					+ headerKeys
					+ ") and CREATEUSERID = 'HDOrderAsyncServer' and MODIFYUSERID = 'HDOrderAsyncServer') and CREATEUSERID = 'admin' and MODIFYUSERID = 'admin' ";

			stmt = con.createStatement();
			rset = stmt.executeQuery(selectQuery1);

			convertToCsv(rset, filename1);

			rset.close();
			stmt.close();

			rset = null;
			stmt = null;
			stmt = con.createStatement();
			rset = stmt.executeQuery(selectQuery2);

			convertToCsv(rset, filename2);

			rset.close();
			stmt.close();

			rset = null;
			stmt = null;
			stmt = con.createStatement();
			rset = stmt.executeQuery(selectQuery3);

			convertToCsv(rset, filename3);

			rset.close();
			stmt.close();
			con.close();

		} catch (Exception e) {
			logger.error("Exception" + e);
			e.printStackTrace();
		}
	}

	public static void convertToCsv(ResultSet rs, String filename)
			throws SQLException, FileNotFoundException {
		PrintWriter csvWriter = new PrintWriter(new File(filename));
		ResultSetMetaData meta = rs.getMetaData();
		int numberOfColumns = meta.getColumnCount();
		String dataHeaders = "\"" + meta.getColumnName(1) + "\"";
		for (int i = 2; i < numberOfColumns + 1; i++) {
			dataHeaders += ",\"" + meta.getColumnName(i) + "\"";
		}
		csvWriter.println(dataHeaders);
		while (rs.next()) {
			String row = "\"" + rs.getString(1) + "\"";
			for (int i = 2; i < numberOfColumns + 1; i++) {
				row += ",\"" + rs.getString(i) + "\"";
			}
			csvWriter.println(row);
		}
		csvWriter.close();

		logger.info("File written to " + filename);
	}

	public void deleteRecords(ArrayList<String> orderList) {
		try {
			int rowsDeleted = 0;
			
			DBConnectionUtil dbConn = new DBConnectionUtil();
			Connection con = dbConn
					.getJNDIConnection(Constants.STERLING_ATC_RW_JNDI);
			Statement stmt = null;

			String headerKeys = "";

			for (String orderHeaderKey : orderList) {
				headerKeys = headerKeys + "'" + orderHeaderKey + "',";
			}

			headerKeys = headerKeys.substring(0, (headerKeys.length() - 1));

			String selectQuery1 = "delete FROM THD01.HD_CHARGE_TXN_ADDITIONAL WHERE ORDER_HEADER_KEY IN ("
					+ headerKeys + ") AND PAYMENT_SEQ_NO = '0' ";

			String selectQuery2 = "delete from THD01.YFS_CREDIT_CARD_TRANSACTION where CHARGE_TRANSACTION_KEY in (select CHARGE_TRANSACTION_KEY from THD01.YFS_CHARGE_TRANSACTION where ORDER_HEADER_KEY in (select order_header_key from THD01.YFS_CHARGE_TRANSACTION where ORDER_HEADER_KEY in ("
					+ headerKeys
					+ ") and CREATEUSERID = 'HDOrderAsyncServer' and MODIFYUSERID = 'HDOrderAsyncServer') and CREATEUSERID = 'admin' and MODIFYUSERID = 'admin') ";

			String selectQuery3 = "delete from THD01.YFS_CHARGE_TRANSACTION where ORDER_HEADER_KEY in (select order_header_key from THD01.YFS_CHARGE_TRANSACTION where ORDER_HEADER_KEY in ("
					+ headerKeys
					+ ") and CREATEUSERID = 'HDOrderAsyncServer' and MODIFYUSERID = 'HDOrderAsyncServer') and CREATEUSERID = 'admin' and MODIFYUSERID = 'admin' ";

			stmt = con.createStatement();
			rowsDeleted = stmt.executeUpdate(selectQuery1);
			con.commit();
			logger.info("Number of rows deleted from HD_CHARGE_TXN_ADDITIONAL : "+rowsDeleted);
			stmt.close();

			stmt = null;
			stmt = con.createStatement();
			rowsDeleted = stmt.executeUpdate(selectQuery2);
			con.commit();
			logger.info("Number of rows deleted from YFS_CREDIT_CARD_TRANSACTION : "+rowsDeleted);
			stmt.close();

			stmt = null;
			stmt = con.createStatement();
			rowsDeleted = stmt.executeUpdate(selectQuery3);
			con.commit();
			logger.info("Number of rows deleted from YFS_CHARGE_TRANSACTION : "+rowsDeleted);
			stmt.close();

			con.close();

		} catch (Exception e) {
			logger.error("Exception" + e);
			e.printStackTrace();
		}

	}
	
	
	public ArrayList<String> getOrderNumbers (ArrayList<String> orderList){
		ArrayList<String> orderNumbers = new ArrayList<String>();
		
		String headerKeys = "";

		for (String orderHeaderKey : orderList) {
			headerKeys = headerKeys + "'" + orderHeaderKey + "',";
		}

		headerKeys = headerKeys.substring(0, (headerKeys.length() - 1));		
		
		String query = "select distinct extn_host_order_ref from thd01.yfs_order_header where ORDER_HEADER_KEY in ("+ headerKeys +")";
		
		try {
			DBConnectionUtil dbConn = new DBConnectionUtil();
			Connection con = dbConn
					.getJNDIConnection(Constants.STERLING_SSC_RO_JNDI);
			Statement stmt = null;
			ResultSet rset = null;

			stmt = con.createStatement();
			rset = stmt.executeQuery(query);

			while (rset.next()) {
				orderNumbers.add(rset.getString(1).trim());
			}

			rset.close();
			stmt.close();
			con.close();

		} catch (Exception e) {
			logger.error("Exception" + e);
			e.printStackTrace();
		}

		return orderNumbers;
	}

}
