package com.homedepot.di.dl.support.deleteDuplicatePayments.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeMessage.RecipientType;

import org.apache.log4j.Logger;

import com.homedepot.di.dl.support.deleteDuplicatePayments.dao.DeleteDupPaymentsDAO;

public class DupPaymentService {
	private static Logger logger = Logger.getLogger(DupPaymentService.class);

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		logger.info("Inside Service: Beginning of program");
		fixDuplicatePaymentRecords();
	}

	public static void fixDuplicatePaymentRecords() {
		DeleteDupPaymentsDAO serviceDAO = new DeleteDupPaymentsDAO();
		
		String msgContent = "";

		ArrayList<String> orderList = new ArrayList<String>();
		orderList = serviceDAO
				.getImpactedOrderList(DupPaymentsConstants.SELECTQUERY1);

		if (orderList.size() > 0) {
			logger.info("Number of impacted orders : " + orderList.size());

			serviceDAO.getBackUp(orderList);

			serviceDAO.deleteRecords(orderList);

			ArrayList<String> orderNumList = serviceDAO
					.getOrderNumbers(orderList);
			
			msgContent = msgContent + "<html>Hi All,<br/><br/> We have deleted duplicate payment records for below orders . PFA backup <br/><br/>";
			
			for (String orderNum : orderNumList){
				msgContent = msgContent + orderNum + "<br/>";
			}
			
			msgContent = msgContent + "<br/> <br/> Thanks <br/>Order Management Support";
			
			sendMail (msgContent);

		} else {
			logger.info("There are no impacted orders to fix");
		}
	}
	
	
	public static void sendMail(String msgContent) {

		
		final String user = "horizon@cpliisad.homedepot.com";
		final String password = "xxx";// change accordingly
		String host = "mail1.homedepot.com";
		Properties props = new Properties();
		props.put("mail.host", host);
		Session session = Session.getInstance(props,
				new javax.mail.Authenticator() {
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(user, password);
					}
				});
		try {

			String[] Cc = {};
			String[] to = { "_2fc77b@homedepot.com" };
//			String[] to = { "venkataganesh_kona@homedepot.com" };
			// String[] Cc = {};
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress(user));
			DateFormat dateFormat = new SimpleDateFormat(
					"MM/dd/yyyy hh:mm:ss a");
			Date today = new Date();
			String date1 = dateFormat.format(today);
			message.setSubject("Duplicate Payment Records - " + date1);
			// message.setText("This is simple program of sending email using JavaMail API");
			message.setFrom(new InternetAddress(user));
			InternetAddress[] addressTo = new InternetAddress[to.length];
			InternetAddress[] addressCc = new InternetAddress[Cc.length];
			for (int i = 0; i < to.length; i++) {
				addressTo[i] = new InternetAddress(to[i]);
				
			}
			for (int j = 0; j < Cc.length; j++) {
				addressCc[j] = new InternetAddress(Cc[j]);
				
			}
			message.setRecipients(RecipientType.TO, addressTo);
			message.setRecipients(RecipientType.CC, addressCc);
			
			
			BodyPart messageBodyPart = new MimeBodyPart();
			
			messageBodyPart.setContent(msgContent, "text/html");
			
			Multipart multipart = new MimeMultipart();
			multipart.addBodyPart(messageBodyPart);
			
			//String filename[] = {"C:\\test\\HD_CHARGE_TXN_ADDITIONAL.csv","C:\\test\\YFS_CREDIT_CARD_TRANSACTION.csv","C:\\test\\YFS_CHARGE_TRANSACTION.csv"};
			
			String filename[] = {"/opt/isv/apache-tomcat/temp/HD_CHARGE_TXN_ADDITIONAL.csv","/opt/isv/apache-tomcat/temp/YFS_CREDIT_CARD_TRANSACTION.csv","/opt/isv/apache-tomcat/temp/YFS_CHARGE_TRANSACTION.csv"};
			
			for(int k = 0 ; k < filename.length; k++)
			{
	           
				MimeBodyPart messageBodyPart2 = new MimeBodyPart();
				DataSource source = new FileDataSource(filename[k]);
				messageBodyPart2.setDataHandler(new DataHandler(source));
				messageBodyPart2.setFileName(source.getName());
				multipart.addBodyPart(messageBodyPart2);
				
			}
			
			
			
			//multipart.addBodyPart(messageBodyPart);
					
			// Put parts in message
			message.setContent(multipart);
			
			Transport.send(message);
			for (int i = 0; i < to.length; i++) {
				addressTo[i] = new InternetAddress(to[i]);
				
			}
			for (int j = 0; j < Cc.length; j++) {
				addressCc[j] = new InternetAddress(Cc[j]);
				
			}

		} catch (MessagingException e) {
			e.printStackTrace();
		}

	}

}
