package com.homedepot.di.dl.support.deleteDuplicatePayments.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.homedepot.di.dl.support.deleteDuplicatePayments.service.DupPaymentService;


/**
 * Servlet implementation class DeleteDupPaymentServlet
 */
public class DeleteDupPaymentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger
			.getLogger(DeleteDupPaymentServlet.class);
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeleteDupPaymentServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		logger.info("Inside Servlet: Beginning of program");
		DupPaymentService.fixDuplicatePaymentRecords();
		logger.info("Inside Servlet: Program Completed");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
