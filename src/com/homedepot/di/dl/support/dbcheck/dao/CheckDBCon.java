package com.homedepot.di.dl.support.dbcheck.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
//import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//import javax.naming.Context;
//import javax.naming.InitialContext;
//import javax.naming.NamingException;
//import javax.sql.DataSource;


import org.apache.log4j.Logger;

import com.homedepot.di.dl.support.dbcheck.dto.DBCheckDTO;
import com.homedepot.di.dl.support.util.Constants;
import com.homedepot.di.dl.support.util.DBConnectionUtil;

public class CheckDBCon {

	/**
	 * @param args
	 */
	private static final Logger logger = Logger.getLogger(CheckDBCon.class);

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Calendar cal = Calendar.getInstance();
		int dayOfMonth = cal.get(Calendar.DAY_OF_MONTH);
		int hour = cal.get(Calendar.HOUR_OF_DAY);
		int minute = cal.get(Calendar.MINUTE);
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		cal.add(Calendar.DATE, -30);
		String monthOldDate = dateFormat.format(cal.getTime());
		if (dayOfMonth == 1 && hour == 1 && minute == 15) {
			purgeOlderRecords(monthOldDate);
		}

		Map<String, String> dbDetailMap = new HashMap<String, String>();
		Map<String, String> dbConnResultMap = new HashMap<String, String>();
		Map<String, DBCheckDTO> dbFinalResultsMap = new HashMap<String, DBCheckDTO>();

		dbDetailMap = loadDBContexts();
		dbConnResultMap = checkConnectivity(dbDetailMap);
		boolean insertStatus = insertResultsInMySQLDB(dbConnResultMap);

		// if(insertStatus){
		// System.out.println("Insertion is successful :-)");
		// }

		dbFinalResultsMap = analyzeResultsOfPreviousRun(dbDetailMap);
		String finalMsg = checkIfMailNeedsToBeSent(dbFinalResultsMap);
//		logger.info("finalMsg : "+finalMsg);
	}

	public String manager() {


		Calendar cal = Calendar.getInstance();
		int dayOfMonth = cal.get(Calendar.DAY_OF_MONTH);
		int hour = cal.get(Calendar.HOUR_OF_DAY);
		int minute = cal.get(Calendar.MINUTE);
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		cal.add(Calendar.DATE, -30);
		String monthOldDate = dateFormat.format(cal.getTime());
		if (dayOfMonth == 1 && hour == 1 && minute == 15) {
			purgeOlderRecords(monthOldDate);
		}

		Map<String, String> dbDetailMap = new HashMap<String, String>();
		Map<String, String> dbConnResultMap = new HashMap<String, String>();
		Map<String, DBCheckDTO> dbFinalResultsMap = new HashMap<String, DBCheckDTO>();

		dbDetailMap = loadDBContexts();
		dbConnResultMap = checkConnectivity(dbDetailMap);
		boolean insertStatus = insertResultsInMySQLDB(dbConnResultMap);

		// if(insertStatus){
		// System.out.println("Insertion is successful :-)");
		// }

		dbFinalResultsMap = analyzeResultsOfPreviousRun(dbDetailMap);
		String finalMsg = checkIfMailNeedsToBeSent(dbFinalResultsMap);
//		logger.info("finalMsg : "+finalMsg);
		return finalMsg;
	}

	/*
	 * This method loads all the DB details that needs a DB connectivity check
	 */

	public static Map<String, String> checkConnectivity(
			Map<String, String> dbDetailMap) {

		Map<String, String> dbConnResultMap = new HashMap<String, String>();

		for (Map.Entry<String, String> entry : dbDetailMap.entrySet()) {

			String dbInstance = entry.getKey();
			String dbDetail = entry.getValue();
			boolean dbStatus = checkDB(dbDetail);
//			 logger.info(dbInstance+"   "+dbStatus);
			if (dbStatus) {
				dbConnResultMap.put(dbInstance, "Y");
			} else {
				dbConnResultMap.put(dbInstance, "N");
			}
		}

		return dbConnResultMap;
	}

	// Check the DB connectivity status in the below function

	public static boolean checkDB(String dbContext) {
		boolean connStatus = false;

		final String driverClass = "oracle.jdbc.driver.OracleDriver";
		Connection con = null;
		DBConnectionUtil db = new DBConnectionUtil();
		try {
			
			try{
				con = db.getJNDIConnection(dbContext);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return connStatus;
			}
//			logger.info("Connected: " + con);
//			logger.info("con.isClosed() : " + con.isClosed());
			
			if(!con.isClosed()){
				connStatus = true;
			}
//			con.close();
			
		} catch (Exception e1) {
			// e1.printStackTrace();
			logger.info("DBConnectivityCheck@CheckDBCon : failed to connect the DB context : "
							+ dbContext + "		" + e1.getMessage());
			return connStatus;
		}
		
		finally {
			try {
				con.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return connStatus;
	}

	// insert into mysql happens here

	public static boolean insertResultsInMySQLDB(
			Map<String, String> dbConnResultMap) {
		// System.out.println("call check");
		boolean result = false;
		DBConnectionUtil db = new DBConnectionUtil();
		
		String driverClass = "com.mysql.jdbc.Driver";
		StringBuilder queryBuilder = new StringBuilder();
		String dbContext = "jdbc/Mysql";
		Connection con = null;
		Statement stmt1 = null;
		
		con = db.getJNDIConnection(dbContext);
		
		try {
			
			Class.forName(driverClass);
			queryBuilder
					.append("insert ignore into DB_conn_check (create_ts, sterling_rw1, sterling_rw2, sterling_rw3, com_rw1, com_rw2, ods_rw1) values "
							+ "(NOW() , '"
							+ dbConnResultMap
									.get("Sterling Integration DB Read-Write Instance1")
							+ "' , "
							+ "'"
							+ dbConnResultMap
									.get("Sterling Integration DB Read-Write Instance2")
							+ "' , "
							+ "'"
							+ dbConnResultMap
									.get("Sterling Integration DB Read-Write Instance3")
							+ "', "
							+ "'"
							+ dbConnResultMap
									.get("COM Integration DB Read-Write Instance1")
							+ "' , "
							+ "'"
							+ dbConnResultMap
									.get("COM Integration DB Read-Write Instance2")
							+ "' , "
							+ "'"
							+ dbConnResultMap
									.get("ODS Integration DB Read-Write Instance1")
							+ "')");

			int rowsInserted = 0;
			
			stmt1 = con.createStatement();
			// logger.info("insert statement : "+queryBuilder.toString());
			rowsInserted = stmt1.executeUpdate(queryBuilder.toString());
			// logger.info(rowsInserted);
			if (!con.getAutoCommit()) {
				con.commit();
			}
//			stmt1.close();
//			con.close();
			// return 0;
			if (rowsInserted > 0) {
				result = true;
			}
		} catch (Exception e) {
			result = false;
		} finally {
			try {
				stmt1.close();
				con.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return result;
	}

	public static Map<String, DBCheckDTO> analyzeResultsOfPreviousRun(
			Map<String, String> dbDetailMap) {

		Map<String, DBCheckDTO> dbFinalResultsMap = new HashMap<String, DBCheckDTO>();
		Map<String, List<String>> dbResultsMap = new HashMap<String, List<String>>();

		List<String> dateRanges = new ArrayList<String>();
		List<String> sterlingRW1 = new ArrayList<String>();
		List<String> sterlingRW2 = new ArrayList<String>();
		List<String> sterlingRW3 = new ArrayList<String>();
		List<String> comRW1 = new ArrayList<String>();
		List<String> comRW2 = new ArrayList<String>();
		List<String> odsRW1 = new ArrayList<String>();
		DBConnectionUtil db = new DBConnectionUtil();
		
		String driverClass = "com.mysql.jdbc.Driver";
		String query = null;
		Connection con = null;
		String dbContext = "jdbc/Mysql";
		
		con = db.getJNDIConnection(dbContext);
		Statement stmt1 = null;
		ResultSet resultSet = null;
		
		try {
			Class.forName(driverClass);
			query = "select * from DB_conn_check order by create_ts desc limit 3";

			int rowsInserted = 0;
			
			stmt1 = con.createStatement();

			resultSet = stmt1.executeQuery(query);
			while (resultSet.next()) {
				dateRanges.add(resultSet.getString(1).trim());
				sterlingRW1.add(resultSet.getString(2).trim());
				sterlingRW2.add(resultSet.getString(3).trim());
				sterlingRW3.add(resultSet.getString(4).trim());
				comRW1.add(resultSet.getString(5).trim());
				comRW2.add(resultSet.getString(6).trim());
				odsRW1.add(resultSet.getString(7).trim());

			}
//			stmt1.close();
//			resultSet.close();
//			con.close();

			dbResultsMap.put("Sterling Integration DB Read-Write Instance1",
					sterlingRW1);
			dbResultsMap.put("Sterling Integration DB Read-Write Instance2",
					sterlingRW2);
			dbResultsMap.put("Sterling Integration DB Read-Write Instance3",
					sterlingRW3);
			dbResultsMap.put("COM Integration DB Read-Write Instance1", comRW1);
			dbResultsMap.put("COM Integration DB Read-Write Instance2", comRW2);
			dbResultsMap.put("ODS Integration DB Read-Write Instance1", odsRW1);
			List<String> resultList = new ArrayList<String>();
			for (Map.Entry<String, String> entry : dbDetailMap.entrySet()) {
				String dbInstance = entry.getKey();
				String dbDetail = entry.getValue();
				if(!dbInstance.contains("My SQL")){
				resultList = dbResultsMap.get(dbInstance);
				DBCheckDTO dbCheckDTO = new DBCheckDTO();
				dbCheckDTO.setDbSpecs(dbDetail);
				dbCheckDTO.setPrevOccurences(resultList);
				dbCheckDTO.setDateRanges(dateRanges);
				for (int i = 0; i < 3; i++) {
					String latest = resultList.get(0);
					String later = resultList.get(1);
					String late = resultList.get(2);
					if (latest.equalsIgnoreCase("N")
							&& later.equalsIgnoreCase("N")) {
						dbCheckDTO.setMailCheck(true);
					}
					if (latest.equalsIgnoreCase("N")
							&& later.equalsIgnoreCase("N")
							&& late.equalsIgnoreCase("N")) {
						dbCheckDTO.setHiPriCheck(true);
					}
				}
				dbFinalResultsMap.put(dbInstance, dbCheckDTO);
				resultList = null;
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				stmt1.close();
				resultSet.close();
				con.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		return dbFinalResultsMap;
	}

	public static String checkIfMailNeedsToBeSent(
			Map<String, DBCheckDTO> dbFinalResultsMap) {
		String finalMsg = "";
		boolean finalMailToBeSent = false;
		boolean finalHiPriToBeSent = false;
		Map<String, DBCheckDTO> dbFinalResultsMapToBeEmailed = new HashMap<String, DBCheckDTO>();
		List<String> dateRanges = new ArrayList<String>();

		for (Map.Entry<String, DBCheckDTO> entry : dbFinalResultsMap.entrySet()) {
			boolean mailToBeSent = false;
			boolean hiPriToBeSent = false;
			DBCheckDTO dbCheckDTO = new DBCheckDTO();
			String dbInstance = entry.getKey();
			dbCheckDTO = entry.getValue();
			if (dbCheckDTO.isMailCheck()) {
				mailToBeSent = true;
				finalMailToBeSent = true;
			}
			if (dbCheckDTO.isHiPriCheck()) {
				hiPriToBeSent = true;
				finalHiPriToBeSent = true;
			}
			dateRanges = dbCheckDTO.getDateRanges();
			if (mailToBeSent || hiPriToBeSent) {
				dbFinalResultsMapToBeEmailed.put(dbInstance, dbCheckDTO);
			}

		}


		if (dbFinalResultsMapToBeEmailed.size() > 0) {
			boolean headerCheck = false;
			StringBuilder emailText = new StringBuilder();
			StringBuilder startText = new StringBuilder();
			startText.append("<html><body><table>");
			startText.append("<font face=\"calibri\"> <tr>");
			if (finalHiPriToBeSent) {
				startText.append("<p>Sending a High Priority email...</p>");
			}
			startText.append("<p>Hello All,</p>");
			startText
					.append("<p>	Script failed to connect the below databases. Please check immediately</p><br></tr>");

			emailText.append("<tr><table border=\"1\">\n");
			emailText.append("<font face=\"calibri\">");

			if (!headerCheck) {
				emailText.append("<tr>");
				emailText.append("<th>");
				emailText.append("DB Instance");
				emailText.append("</th>");
				emailText.append("<th>");
				emailText.append(dateRanges.get(0));
				emailText.append("</th>");
				emailText.append("<th>");
				emailText.append(dateRanges.get(1));
				emailText.append("</th>");
				emailText.append("<th>");
				emailText.append(dateRanges.get(2));
				emailText.append("</th>");
				emailText.append("<th>");
				emailText.append("DataSource trying to connect");
				emailText.append("</th>");
				emailText.append("</tr>");
				emailText.append('\n');

				for (Map.Entry<String, DBCheckDTO> entry : dbFinalResultsMapToBeEmailed
						.entrySet()) {
					DBCheckDTO dbCheckDTO = new DBCheckDTO();
					List<String> prevOccurences = new ArrayList<String>();
					String dbInstance = entry.getKey();
					dbCheckDTO = entry.getValue();
					String dbDetail = dbCheckDTO.getDbSpecs();
					prevOccurences = dbCheckDTO.getPrevOccurences();

					emailText.append("<font face=\"calibri\">");
					emailText.append("<tr>");
					emailText.append("<td>");
					emailText.append(dbInstance);
					emailText.append("</td>");
					emailText.append("<td>");
					if (prevOccurences.get(0).equalsIgnoreCase("Y")) {
						emailText.append("Passed");
					} else {
						emailText.append("Failed");
					}
					emailText.append("</td>");
					emailText.append("<td>");
					if (prevOccurences.get(1).equalsIgnoreCase("Y")) {
						emailText.append("Passed");
					} else {
						emailText.append("Failed");
					}
					emailText.append("</td>");
					emailText.append("<td>");
					if (prevOccurences.get(2).equalsIgnoreCase("Y")) {
						emailText.append("Passed");
					} else {
						emailText.append("Failed");
					}
					emailText.append("</td>");
					emailText.append("<td>");
					emailText.append(dbDetail.trim());
					emailText.append("</td>");
					emailText.append("</tr></tr>");
				}

			}

			emailText.append("</table>");
			emailText.append("<p>&nbsp;</p>");
			emailText.append("<p>&nbsp;</p>");
			emailText.append('\n');
			emailText.append("<tr><p>Thanks,</p></tr>");
			emailText.append("<tr><p>COM Multi-Channel Support</p><br></tr>");
			emailText.append(System.getProperty("line.separator"));
			emailText
					.append("<p>Note: This is a system-generated e-mail. Please do NOT reply to it.</p><br>");
			emailText.append("</table></body>");
			emailText.append("</html>");

			finalMsg = startText.append(emailText).toString();
		}

		return finalMsg;
	}

	public static int purgeOlderRecords(String monthOldDate) {
		int rowsDeleted = 0;
		DBConnectionUtil db = new DBConnectionUtil();
		Statement stmt1 = null;
		String driverClass = "com.mysql.jdbc.Driver";
		StringBuilder queryBuilder = new StringBuilder();
		String mySQLContext = "jdbc/Mysql";
		Connection con = null;
		con = db.getJNDIConnection(mySQLContext);
		
		try {
			Class.forName(driverClass);
			queryBuilder.append("delete from DB_conn_check where create_ts < '"
					+ monthOldDate + "'");

			
			stmt1 = con.createStatement();
			rowsDeleted = stmt1.executeUpdate(queryBuilder.toString());
			if (!con.getAutoCommit()) {
				con.commit();
			}
//			stmt1.close();
//			con.close();
			// return 0;
			if (rowsDeleted > 0) {
				logger.info("Purged the DB check records older than a month. Total records deleted : "
						+ rowsDeleted);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				stmt1.close();
				con.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return rowsDeleted;
	}

	public static Map<String, String> loadDBContexts() {

		/*
		 * To add a new entry to the list Instantiate an array of strings like
		 * below String[] DBInstanceName =
		 * {"<DBUserName>","<DBPassword>","<JDBC URL>"}; Then add it to the
		 * dbDetailMap like below
		 * dbDetailMap.put("<Detailed name of the DB instance>", <new array of
		 * strings created above>);
		 */
		Map<String, String> dbDetailMap = new HashMap<String, String>();

		String sterlingRW1 = Constants.STERLING_ATC_RW1_JNDI;
		String sterlingRW2 = Constants.STERLING_ATC_RW2_JNDI;
		String sterlingRW3 = Constants.STERLING_ATC_RW3_JNDI;
		String comRW1 = Constants.COM_ATC_RW1_JNDI;
		String comRW2 = Constants.COMT_ATC_RO_JNDI;
		String odsRW1 = Constants.ODS_STAND_BY_JNDI;
		String mySQL = Constants.COM_SUPPORT_MYSQL_JNDI;

		dbDetailMap.put("Sterling Integration DB Read-Write Instance1",
				sterlingRW1);
		dbDetailMap.put("Sterling Integration DB Read-Write Instance2",
				sterlingRW2);
		dbDetailMap.put("Sterling Integration DB Read-Write Instance3",
				sterlingRW3);
		dbDetailMap.put("COM Integration DB Read-Write Instance1", comRW1);
		dbDetailMap.put("COM Integration DB Read-Write Instance2", comRW2);
		dbDetailMap.put("ODS Integration DB Read-Write Instance1", odsRW1);
		dbDetailMap.put("My SQL DB Read-Write Instance", mySQL);

		return dbDetailMap;

	}

//	private static Connection getJNDIConnection(String contextName) throws Exception {
//		String DATASOURCE_CONTEXT = contextName;
//
//		Connection conn = null;
//		try {
//			Context initialContext = new InitialContext();
//			Context envContext = (Context) initialContext
//					.lookup("java:comp/env");
//
//			DataSource datasource = (DataSource) envContext
//					.lookup(DATASOURCE_CONTEXT);
//			if (datasource != null) {
//				conn = datasource.getConnection();
////				logger.info("Connected: " + conn);
//			} 
//		} catch (NamingException ex) {
//			logger.info("Cannot get connection: " + ex);
//		} catch (SQLException ex) {
//			logger.info("Cannot get connection: " + ex);
//		}
//		return conn;
//	}

}
