package com.homedepot.di.dl.support.dbcheck.web;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Calendar;
import java.util.Locale;
import java.util.Properties;
import java.util.ResourceBundle;
import org.apache.log4j.Logger;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.homedepot.di.dl.support.compareDataReplication.web.CompareDataReplicationServlet;
import com.homedepot.di.dl.support.dbcheck.dao.CheckDBCon;

/**
 * Servlet implementation class DBCheck
 */
public class DBCheck extends HttpServlet {
	private static final long serialVersionUID = 1L;
	final static Logger logger = Logger.getLogger(DBCheck.class);
  
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DBCheck() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		String text = "";
		String eMailSubject = null;
		CheckDBCon dao = new CheckDBCon();
		eMailSubject = "Unable to connect DB ";
		
		String emailRecipient = "_2fc77b@homedepot.com"; //COM Multichannel DL
		//String emailRecipient = "_275769@homedepot.com";//custom recipient
		
		
		Session session = null;
		MimeMessage mimeMessage = null;
		MimeBodyPart mimeBodyPart = null;
		Multipart multiPart = null;
		String date = null;
		Calendar calendar = null;
		Properties properties = null;

		String msg = null;
		response.setContentType("text/html");
		PrintWriter out = null;
		
		
/*//		File file = new File("C:\\test");
		File file = new File("/opt/isv/tomcat-6.0.18/webapps/ConnProperties");
		URL[] urls = {file.toURI().toURL()};   
		ClassLoader loader = new URLClassLoader(urls);
		ResourceBundle bundle = ResourceBundle.getBundle("DBConnection", Locale.getDefault(), loader);*/

		try {
			msg = "Success";
			text = dao.manager();
			calendar = Calendar.getInstance();
			date = calendar.getTime().toString();
			properties = System.getProperties();
			properties.put("mail.host", "mail1.homedepot.com");
			session = Session.getInstance(properties, null);
			mimeMessage = new MimeMessage(session);
			mimeMessage.setFrom(new InternetAddress(
					"horizon@cpliisad.homedepot.com"));
			 
			mimeMessage.setSubject(eMailSubject + date);
			mimeBodyPart = new MimeBodyPart();
			mimeBodyPart.setText(text.toString());
			multiPart = new MimeMultipart();
			multiPart.addBodyPart(mimeBodyPart);
			mimeMessage.setContent(text.toString(), "text/html");
			if(text.length() > 0){
				if(text.contains("High Priority")){
					mimeMessage.setHeader("X-Priority", "1");
					mimeMessage.setRecipients(Message.RecipientType.TO,
							 InternetAddress.parse("Order_Mgmt_Support@homedepot.com", false));
				}
				else{
					mimeMessage.setHeader("X-Priority", "3");
					mimeMessage.setRecipients(Message.RecipientType.TO,
							 InternetAddress.parse(emailRecipient, false));
				}
				Transport.send(mimeMessage);
			}
			

			/*
			 * Write the HTML to the response
			 */
		} catch (Exception e) {
			msg = "Failure";
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		out = response.getWriter();
		out.println(msg);
		out.close();
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
