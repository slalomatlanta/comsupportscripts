package com.homedepot.di.dl.support.dbcheck.dto;

import java.util.List;

public class DBCheckDTO {
	List<String> prevOccurences;
	String dbSpecs;
	boolean mailCheck = false;
	boolean hiPriCheck = false;
	List<String> dateRanges;
	
	public List<String> getDateRanges() {
		return dateRanges;
	}
	public void setDateRanges(List<String> dateRanges) {
		this.dateRanges = dateRanges;
	}
	public List<String> getPrevOccurences() {
		return prevOccurences;
	}
	public void setPrevOccurences(List<String> prevOccurences) {
		this.prevOccurences = prevOccurences;
	}
	public String getDbSpecs() {
		return dbSpecs;
	}
	public void setDbSpecs(String dbSpecs) {
		this.dbSpecs = dbSpecs;
	}
	public boolean isMailCheck() {
		return mailCheck;
	}
	public void setMailCheck(boolean mailCheck) {
		this.mailCheck = mailCheck;
	}
	public boolean isHiPriCheck() {
		return hiPriCheck;
	}
	public void setHiPriCheck(boolean hiPriCheck) {
		this.hiPriCheck = hiPriCheck;
	}
	
}
