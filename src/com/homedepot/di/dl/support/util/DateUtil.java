package com.homedepot.di.dl.support.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import org.apache.log4j.Logger;

public class DateUtil extends Constants {
	
	private static final Logger logger = Logger.getLogger(DateUtil.class);
	
	/*public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Date startDt = new Date();
		Date endDt= new Date();
		
		String stDt = "20-OCT-15 10:55:12";
		String enDt= "27-OCT-15 10:55:12";
		DateFormat format = new SimpleDateFormat("dd-MMM-yy HH:mm:ss", Locale.ENGLISH);
		Date stdate;
		Date enDate;
		try {
			stdate = format.parse(stDt);
			enDate= format.parse(enDt);
		
		logger.info("startdate:"+stdate);
		
		getDaysBetweenDates(stdate, enDate);
		}
		 catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
	*/
	public static List<Date> getDaysBetweenDates(String startdate, String enddate)
	{
		
		Date stdate;
		Date enDate;
		List<Date> dates = new ArrayList<Date>();
	    Calendar calendar = new GregorianCalendar();
	    /*DateFormat format = new SimpleDateFormat("dd-MMM-yy HH:mm:ss", Locale.ENGLISH);*/
	    DateFormat format = new SimpleDateFormat("yyyy-mm-dd HH:mm:ss", Locale.ENGLISH);
	    try {
			stdate = format.parse(startdate);
			enDate= format.parse(enddate);
		
			logger.info("startdate:"+stdate);
		
		calendar.setTime(stdate);
	    
	    while (calendar.getTime().before(enDate))
	    {
	        Date result = calendar.getTime();
	        logger.info("between date::"+result.toString());
	        dates.add(result);
	        calendar.add(Calendar.DATE, 1);
	    }
	    }
		 catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    return dates;
	}
	
	// To compare the current time with the SLA threshold time
	public static boolean checkSlaMissedOrNot() throws ParseException {
		
		String sla=PURGE_THRESHOLD_LONG_RUNNING_QUERY;
        boolean slaMissedOrnot = false;
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(sla.substring(0, 2)));
        cal.set(Calendar.MINUTE, Integer.parseInt(sla.substring(3)));
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        if (Calendar.getInstance().after(cal)) {
        	logger.info("it's SLA missed");
            slaMissedOrnot = true;
        } else {
        	logger.info("it's fine & not SLA missed");
        }
        return slaMissedOrnot;
    }


}
