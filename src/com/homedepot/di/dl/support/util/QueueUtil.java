package com.homedepot.di.dl.support.util;

import java.util.Enumeration;

import javax.jms.Message;
import javax.jms.Queue;
import javax.jms.QueueBrowser;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueSession;
import javax.jms.Session;
import javax.naming.Context;
import javax.naming.InitialContext;

import org.apache.log4j.Logger;


public class QueueUtil {
	private static final Logger logger = Logger
			.getLogger(QueueUtil.class);
	public static int getQueueDepth(String queueName, String resourceName, String lookIpName) {

//		String queueName = "DI.DL.STERLING.NODENOTIFICATIONPERIOD";
//
//		String resourceName = "jms/" + queueName + "_get";
//		String lookIpName = "jms/MQGetQCF";
		// List<QueueDTO> xmlList = new ArrayList<QueueDTO>();
		logger.debug(queueName);
		Context context = null;
		InitialContext ctx = null;
		Queue queue = null;
		QueueConnectionFactory connFactory = null;
		QueueConnection queueConn = null;
		QueueSession queueSession = null;
		QueueBrowser queueBrowser = null;
		int count = 0;
		try {
			ctx = new InitialContext();
			context = (Context) ctx.lookup("java:comp/env");
			queue = (Queue) context.lookup(resourceName);
			connFactory = (QueueConnectionFactory) context.lookup(lookIpName);
			queueConn = connFactory.createQueueConnection();
			queueSession = queueConn.createQueueSession(false,
					Session.AUTO_ACKNOWLEDGE);
			queueBrowser = queueSession.createBrowser(queue);
			queueConn.start();

			// browse the messages
			Enumeration e = null;
			e = queueBrowser.getEnumeration();

			// count number of messages
			while (e.hasMoreElements()) {
				Message message = (Message) e.nextElement();
				count++;
			}

			queueConn.close();

		} catch (Exception e1) {
			logger.debug("Queue Exception: " + e1);
			e1.printStackTrace();
		} finally {

		}
		logger.debug(queueName + " has " + count + " messages ");
		
		return count;
	}

}
