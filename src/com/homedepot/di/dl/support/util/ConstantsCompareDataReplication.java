package com.homedepot.di.dl.support.util;


public class ConstantsCompareDataReplication {

	public static long MAX_WARN_SECONDS = 300;
	public static long MAX_CRITICAL_SECONDS= 900;


	//Sterling Queries
	public static String STERLING_ORDER_HEADER_KEY_SELECT = "select max(order_header_key) from THD01.YFS_ORDER_HEADER";
	public static String STERLING_BASE_COMMON_CODE_SELECT = "select max(MODIFYTS) from THD01.YFS_BASE_COMMON_CODE";
	public static String STERLING_CHARGE_CATEGORY_SELECT = "select max(MODIFYTS) from THD01.YFS_CHARGE_CATEGORY";
	public static String STERLING_CHARGE_NAME_SELECT = "select max(MODIFYTS) from THD01.YFS_CHARGE_NAME";
	public static String STERLING_COMMON_CODE_SELECT = "select max(MODIFYTS) from THD01.YFS_COMMON_CODE";
	public static String STERLING_COMMON_CODE_ATTRIBUTE_SELECT = "select max(MODIFYTS) from THD01.YFS_COMMON_CODE_ATTRIBUTE";
	public static String STERLING_ENTERPRISE_SELECT = "select max(MODIFYTS) from THD01.YFS_ENTERPRISE";
	public static String STERLING_ERROR_CODE_SELECT = "select max(MODIFYTS) from THD01.YFS_ERROR_CODE";
	public static String STERLING_HOLD_TYPE_SELECT = "select max(MODIFYTS) from THD01.YFS_HOLD_TYPE";
	public static String STERLING_INBOX_REFERENCE_SELECT = "select max(MODIFYTS) from THD01.YFS_INBOX_REFERENCES";
	public static String STERLING_INVENTORY_ITEM_SELECT = "select max(MODIFYTS) from THD01.YFS_INVENTORY_ITEM";
	public static String STERLING_INVENTORY_NODE_CONTROL_SELECT = "select max(MODIFYTS) from THD01.YFS_INVENTORY_NODE_CONTROL";
	public static String STERLING_INVENTORY_SUPPLY_TYPE_SELECT = "select max(MODIFYTS) from THD01.YFS_INVENTORY_SUPPLY_TYPE";
	public static String STERLING_ITEM_SELECT = "select max(MODIFYTS) from THD01.YFS_ITEM";
	public static String STERLING_ITEM_ALIAS_SELECT = "select max(MODIFYTS) from THD01.YFS_ITEM_ALIAS";
	public static String STERLING_ITEM_NODE_DEFN_SELECT = "select max(MODIFYTS) from THD01.YFS_ITEM_NODE_DEFN";
	public static String STERLING_ITEM_UOM_SELECT = "select max(MODIFYTS) from THD01.YFS_ITEM_UOM";
	public static String STERLING_LINE_RELATIONSHIP_TYPE_SELECT = "select max(MODIFYTS) from THD01.YFS_LINE_RELATIONSHIP_TYPE";
	public static String STERLING_ORDER_LINE_TYPE_SELECT = "select max(MODIFYTS) from THD01.YFS_ORDER_LINE_TYPE";
	public static String STERLING_ORGANIZATION_SELECT = "select max(MODIFYTS) from THD01.YFS_ORGANIZATION";
	public static String STERLING_ORG_ENTERPRISE_SELECT = "select max(MODIFYTS) from THD01.YFS_ORG_ENTERPRISE";
	public static String STERLING_PAYMENT_TYPE_SELECT = "select max(MODIFYTS) from THD01.YFS_PAYMENT_TYPE";
	public static String STERLING_SCAC_SELECT = "select max(MODIFYTS) from THD01.YFS_SCAC";
	public static String STERLING_SCAC_AND_SERVICE_SELECT = "select max(MODIFYTS) from THD01.YFS_SCAC_AND_SERVICE";
	public static String STERLING_SHIPNODE_SELECT = "select max(MODIFYTS) from THD01.YFS_SHIP_NODE"; 
	public static String STERLING_STATUS_SELECT = "select max(MODIFYTS) from THD01.YFS_STATUS";
	public static String STERLING_UOM_SELECT = "select max(MODIFYTS) from THD01.YFS_UOM";
	public static String STERLING_UOM_CONVERSION_SELECT = "select max(MODIFYTS) from THD01.YFS_UOM_CONVERSION";
	public static String STERLING_HD_INBOX_SELECT = "select max(MODIFYTS) from THD01.HD_INBOX";  
	public static String STERLING_ORDER_LINE_SELECT = "select max(MODIFYTS) from THD01.YFS_ORDER_LINE"; 
	public static String STERLING_INSTRUCTION_DETAIL_SELECT = "select max(MODIFYTS) from THD01.YFS_INSTRUCTION_DETAIL";
	public static String STERLING_HD_W_C_INSTRUCTION_SELECT = "select max(MODIFYTS) from THD01.HD_W_C_INSTRUCTION";
	public static String STERLING_PERSON_INFO_SELECT = "select max(MODIFYTS) from THD01.YFS_PERSON_INFO";
	public static String STERLING_HD_WILL_CALL_SELECT = "select max(MODIFYTS) from THD01.HD_WILL_CALL";
	public static String STERLING_HD_SPL_ORD_SELECT = "select max(MODIFYTS) from THD01.HD_SPL_ORD";
	public static String STERLING_HD_EVENT_SELECT = "select max(MODIFYTS) from THD01.HD_EVENT"; 
	public static String STERLING_WORK_ORDER_SELECT = "select max(MODIFYTS) from THD01.YFS_WORK_ORDER";
	public static String STERLING_HD_RELATED_WORK_ORDER_SELECT = "select max(MODIFYTS) from THD01.HD_RELATED_WORK_ORDER";

	// ODS Queries
	public static String ODS_ORDER_HEADER_KEY_SELECT = "select max(order_header_key) from CODSSC01.YFS_ORDER_HEADER";
	public static String ODS_BASE_COMMON_CODE_SELECT = "select max(MODIFYTS) from CODSSC01.YFS_BASE_COMMON_CODE";
	public static String ODS_CHARGE_CATEGORY_SELECT = "select max(MODIFYTS) from CODSSC01.YFS_CHARGE_CATEGORY";
	public static String ODS_CHARGE_NAME_SELECT = "select max(MODIFYTS) from CODSSC01.YFS_CHARGE_NAME";
	public static String ODS_COMMON_CODE_SELECT = "select max(MODIFYTS) from CODSSC01.YFS_COMMON_CODE";
	public static String ODS_COMMON_CODE_ATTRIBUTE_SELECT = "select max(MODIFYTS) from CODSSC01.YFS_COMMON_CODE_ATTRIBUTE";
	public static String ODS_ENTERPRISE_SELECT = "select max(MODIFYTS) from CODSSC01.YFS_ENTERPRISE";
	public static String ODS_ERROR_CODE_SELECT = "select max(MODIFYTS) from CODSSC01.YFS_ERROR_CODE";
	public static String ODS_HOLD_TYPE_SELECT = "select max(MODIFYTS) from CODSSC01.YFS_HOLD_TYPE";
	public static String ODS_INBOX_REFERENCE_SELECT = "select max(MODIFYTS) from CODSSC01.YFS_INBOX_REFERENCES";
	public static String ODS_INVENTORY_ITEM_SELECT = "select max(MODIFYTS) from CODSSC01.YFS_INVENTORY_ITEM";
	public static String ODS_INVENTORY_NODE_CONTROL_SELECT = "select max(MODIFYTS) from CODSSC01.YFS_INVENTORY_NODE_CONTROL";
	public static String ODS_INVENTORY_SUPPLY_TYPE_SELECT = "select max(MODIFYTS) from CODSSC01.YFS_INVENTORY_SUPPLY_TYPE";
	public static String ODS_ITEM_SELECT = "select max(MODIFYTS) from CODSSC01.YFS_ITEM";
	public static String ODS_ITEM_ALIAS_SELECT = "select max(MODIFYTS) from CODSSC01.YFS_ITEM_ALIAS";
	public static String ODS_ITEM_NODE_DEFN_SELECT = "select max(MODIFYTS) from CODSSC01.YFS_ITEM_NODE_DEFN";
	public static String ODS_ITEM_UOM_SELECT = "select max(MODIFYTS) from CODSSC01.YFS_ITEM_UOM";	
	public static String ODS_LINE_RELATIONSHIP_TYPE_SELECT = "select max(MODIFYTS) from CODSSC01.YFS_LINE_RELATIONSHIP_TYPE";
	public static String ODS_ORDER_LINE_TYPE_SELECT = "select max(MODIFYTS) from CODSSC01.YFS_ORDER_LINE_TYPE";
	public static String ODS_ORGANIZATION_SELECT = "select max(MODIFYTS) from CODSSC01.YFS_ORGANIZATION";
	public static String ODS_ORG_ENTERPRISE_SELECT = "select max(MODIFYTS) from CODSSC01.YFS_ORG_ENTERPRISE";	
	public static String ODS_PAYMENT_TYPE_SELECT = "select max(MODIFYTS) from CODSSC01.YFS_PAYMENT_TYPE";
	public static String ODS_SCAC_SELECT = "select max(MODIFYTS) from CODSSC01.YFS_SCAC";
	public static String ODS_SCAC_AND_SERVICE_SELECT = "select max(MODIFYTS) from CODSSC01.YFS_SCAC_AND_SERVICE";
	public static String ODS_SHIPNODE_SELECT = "select max(MODIFYTS) from CODSSC01.YFS_SHIP_NODE"; 
	public static String ODS_STATUS_SELECT = "select max(MODIFYTS) from CODSSC01.YFS_STATUS";
	public static String ODS_UOM_SELECT = "select max(MODIFYTS) from CODSSC01.YFS_UOM";
	public static String ODS_UOM_CONVERSION_SELECT = "select max(MODIFYTS) from CODSSC01.YFS_UOM_CONVERSION";
	public static String ODS_HD_INBOX_SELECT = "select max(MODIFYTS) from CODSSC01.HD_INBOX";  
	public static String ODS_ORDER_LINE_SELECT = "select max(MODIFYTS) from CODSSC01.YFS_ORDER_LINE"; 
	public static String ODS_INSTRUCTION_DETAIL_SELECT = "select max(MODIFYTS) from CODSSC01.YFS_INSTRUCTION_DETAIL";
	public static String ODS_HD_W_C_INSTRUCTION_SELECT = "select max(MODIFYTS) from CODSSC01.HD_W_C_INSTRUCTION";
	public static String ODS_PERSON_INFO_SELECT = "select max(MODIFYTS) from CODSSC01.YFS_PERSON_INFO";
	public static String ODS_HD_WILL_CALL_SELECT = "select max(MODIFYTS) from CODSSC01.HD_WILL_CALL";	
	public static String ODS_HD_SPL_ORD_SELECT = "select max(MODIFYTS) from CODSSC01.HD_SPL_ORD";
	public static String ODS_HD_EVENT_SELECT = "select max(MODIFYTS) from CODSSC01.HD_EVENT"; 
	public static String ODS_WORK_ORDER_SELECT = "select max(MODIFYTS) from CODSSC01.YFS_WORK_ORDER";
	public static String ODS_HD_RELATED_WORK_ORDER_SELECT = "select max(MODIFYTS) from CODSSC01.HD_RELATED_WORK_ORDER";
	
	

	
}
