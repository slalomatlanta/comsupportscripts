package com.homedepot.di.dl.support.util;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import org.apache.log4j.Logger;

public class OrderUtil {

	private static final Logger logger = Logger.getLogger(OrderUtil.class);
	private static String url = "http://multichannel-custom.homedepot.com/COMOrder/rs/getOrderDetail";
	private static String headerValue = "F4EJPD2Z7TbyLUP4llcrQsxPaE6yMjM0y8KkbvP8cwCN9YKBs7Qxdm3lrRppaPiGzrq7djLoSp5IfA8pJMFYtuVCHsTWEX4W6v5GpfL6E8J57j0ZfJQmhwbZJla1cbzPKvaAShYH1KIxhhmxg1RsesWqVH54fyPZRviJFURRVhfx0FuqPYxdaaGSL4GgnPym1da7lti6bOblN0EOxHK45fm5gkf5vV3eQmNaq5C7NqtSVbiaMIZUJgkGTdrdeY7HVqx7FmFNj8rphDVQuNfeUHSt2euMu6zeB4y";
	private static String headerName = "THDService-Auth";
	
	private static Client client = null;

	public static Client getClient(){
		if(client == null){
			client = Client.create();
		}
		return client;		
	}
	
	

	/**
	 * This method calls the COMOrder - getOrderDetail service and requests for
	 * a lock and returns its value
	 */

	public static String getOrderDetailWithLock(String extnHostOrderRef,
			String shipnodeKey) {
		String extnLockID = null;
		//String url = "http://multichannel-custom.homedepot.com/COMOrder/rs/getOrderDetail";
		String inputXML = "<Order DocumentType='0001' EnterpriseCode='HDUS'><Extn ExtnHostOrderReference='"
				+ extnHostOrderRef
				+ "' ExtnPutOrderOnLock='Y' ExtnStoreRequestingLock='"
				+ shipnodeKey.trim()
				+ "' ExtnUserId='COMSupport' ExtnIncludeEventDetails='Y'/></Order>";

		//String headerValue = "F4EJPD2Z7TbyLUP4llcrQsxPaE6yMjM0y8KkbvP8cwCN9YKBs7Qxdm3lrRppaPiGzrq7djLoSp5IfA8pJMFYtuVCHsTWEX4W6v5GpfL6E8J57j0ZfJQmhwbZJla1cbzPKvaAShYH1KIxhhmxg1RsesWqVH54fyPZRviJFURRVhfx0FuqPYxdaaGSL4GgnPym1da7lti6bOblN0EOxHK45fm5gkf5vV3eQmNaq5C7NqtSVbiaMIZUJgkGTdrdeY7HVqx7FmFNj8rphDVQuNfeUHSt2euMu6zeB4y";

		//String headerName = "THDService-Auth";

		Client client = getClient();

		WebResource webResource = ((com.sun.jersey.api.client.Client) client)
				.resource(url);

		String outputXML = webResource.header(headerName, headerValue)
				.type("text/xml").post(String.class, inputXML);

		if (outputXML.contains("ResponseDescription=\"SUCCESS\"")) {
			extnLockID = getLockId(outputXML);
		} else if (outputXML.contains("Order locked by COMSupport at store")) {
			inputXML = "<Order DocumentType='0001' EnterpriseCode='HDUS'><Extn ExtnHostOrderReference='"
					+ extnHostOrderRef
					+ "' ExtnPutOrderOnLock='N' ExtnStoreRequestingLock='"
					+ shipnodeKey.trim()
					+ "' ExtnUserId='COMSupport' ExtnIncludeEventDetails='Y'/></Order>";
			outputXML = null;
			outputXML = webResource.header(headerName, headerValue)
					.type("text/xml").post(String.class, inputXML);
			extnLockID = getLockId(outputXML);
		} else {
			logger.info("getOrderDetail failed for " + extnHostOrderRef);
			extnLockID = "getOrderDetail failed for " + extnHostOrderRef;
		}

		return outputXML;
	}

	/**
	 * This method looks for the attribute ExtnLockID in the input provided and
	 * returns its value
	 */

	public static String getLockId(String str) {
		// logger.info(str);
		String extnLockID = null;
		if (str.contains("ExtnLockID")) {
			String dupStr = str;
			String lock = null;
			int firstIndex = dupStr.indexOf("ExtnLockID");
			firstIndex = firstIndex + 12;
			int lastIndex = firstIndex + 11;
			lock = dupStr.substring(firstIndex, lastIndex);
			lastIndex = lock.indexOf("\"");
			lock = lock.substring(0, lastIndex);
			logger.info(lock);
			extnLockID = lock;
		}
		return extnLockID;
	}

	/**
	 * Method to call OrderModifyService with release request and get the
	 * response
	 * */
	public static String callOrderModifyService(String inputXML) {

		//String url = "http://multichannel-custom.homedepot.com/COMOrder/rs/modifySalesOrderForServicesSynchronously";

		//String headerValue = "F4EJPD2Z7TbyLUP4llcrQsxPaE6yMjM0y8KkbvP8cwCN9YKBs7Qxdm3lrRppaPiGzrq7djLoSp5IfA8pJMFYtuVCHsTWEX4W6v5GpfL6E8J57j0ZfJQmhwbZJla1cbzPKvaAShYH1KIxhhmxg1RsesWqVH54fyPZRviJFURRVhfx0FuqPYxdaaGSL4GgnPym1da7lti6bOblN0EOxHK45fm5gkf5vV3eQmNaq5C7NqtSVbiaMIZUJgkGTdrdeY7HVqx7FmFNj8rphDVQuNfeUHSt2euMu6zeB4y";

		//String headerName = "THDService-Auth";

		Client client = getClient();

		WebResource webResource = ((com.sun.jersey.api.client.Client) client)
				.resource(url);

		String outputXML = webResource.header(headerName, headerValue)
				.type("text/xml").post(String.class, inputXML);

		// logger.debug("Response: " + outputXML);

		return outputXML;
	}
	
	/**
	 * Method to check the response whether it is a success or failure
	 * */
	public static String checkResponseType(String response) {

		String result = "";
		if (response == null || "".equalsIgnoreCase(response)) {
			result = "No response";
		} else if (response.contains("ResponseDescription=\"SUCCESS\">")) {
			result = "SUCCESS";
		} else if (response.contains("Error")) {
			int firstIndex = response.indexOf("ResponseDescription");
			firstIndex = firstIndex + 21;
			int lastIndex = response.indexOf("<Errors>");
			result = "Failure - "
					+ response.substring(firstIndex, lastIndex - 5);
		}

		return result;

	}

	
}
