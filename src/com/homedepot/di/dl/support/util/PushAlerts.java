package com.homedepot.di.dl.support.util;

import javax.ws.rs.core.MultivaluedMap;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.core.util.MultivaluedMapImpl;
import org.apache.log4j.Logger;

public class PushAlerts {
	private static final Logger logger = Logger.getLogger(PushAlerts.class);
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		PushAlerts pa = new PushAlerts();
		String status = pa.postAppUpdate("3q6YrKbMK7KxeQuHG", false, "Purush", "The Techie");
		logger.info(status);
	}
	
	public String postAppUpdate(String appID,boolean alert, String text,String value){
		
		Client client = Client.create();
        WebResource webResource = client.resource("https://hell.apps-np.homedepot.com/monitors/update");
        MultivaluedMap<String, String> map = new MultivaluedMapImpl();
        map.add("id", appID);
        map.add("alert", alert+"");
        map.add("text", text);
        map.add("value", value);            
        ClientResponse response = webResource.type("application/x-www-form-urlencoded").post(ClientResponse.class, map);        
        return response.toString();
		
		
		
	}
	
//public String postAppUpdate(String appID,boolean alert, String text,String value){
//		
//		String url = "http://C9100970EE83CDE.amer.homedepot.com:8081/MongoConnect/rs/updateDoc";
//		String inputXML = "inputXML=<UpdateRequest>" +
//				"<id>"+appID+"</id>" +
//				"<alert>"+alert+"</alert>" +
//				"<text>"+text+"</text>" +
//				"<value>"+value+"</value>" +
//				"</UpdateRequest>";
//		
//		Client client = ClientManager.getClient();
//
//		WebResource webResource = ((com.sun.jersey.api.client.Client) client)
//				.resource(url);
//		String status = webResource.type("application/xml").post(
//				String.class, inputXML);
//		
//		
//		return status;
//	}

}
