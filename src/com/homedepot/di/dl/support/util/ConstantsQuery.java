package com.homedepot.di.dl.support.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class ConstantsQuery 
{
	/*BOSS Health Check Queries*/
	public  String bossPO = "select Extn_host_order_ref,extn_profile_id,createts from thd01.yfs_order_header where document_type='0005' and extn_profile_id='BOSS' order by createts desc";
	public  String sthPO = "select Extn_host_order_ref,extn_profile_id,createts from thd01.yfs_order_header where document_type='0005' and extn_profile_id='STH' order by createts desc";
	
	public  String bossEJ = "select yoh.extn_host_order_ref,yoh.extn_profile_id,ej.createts from thd01.hd_related_e_j_keys ej,thd01.yfs_order_header yoh,"
					+ " thd01.yfs_order_line yol where yoh.order_header_key=yol.order_header_key and yol.order_line_key=ej.order_line_key and" 
					+ " ej.createuserid='HDProcessOrderUpdatesServer'"
					+ " order by createts desc";
	public  String sthEJ = "select yoh.extn_host_order_ref,yoh.extn_profile_id,ej.createts from thd01.hd_related_e_j_keys ej,thd01.yfs_order_header yoh,"
					+ " thd01.yfs_order_line yol where yoh.order_header_key=yol.order_header_key and yol.order_line_key=ej.order_line_key and "
					+ " ej.createuserid='HDDropShipServer'"
					+ "order by createts desc";
	DateFormat dateFormat = new SimpleDateFormat(
			"yyyyMMdd");
	// get current date time with Date()
	Date today = new Date();
	String date = dateFormat.format(today);
	
	
	
	
	public  String hourlyBOSSPO = "select substr(order_header_key,0,10), count(*) from thd01.yfs_order_header where document_type='0005' and order_header_key like '"+date+"%' and extn_profile_id='BOSS' group by substr(order_header_key,0,10) order by substr(order_header_key,0,10) asc";
	public  String hourlySTHPO = "select substr(order_header_key,0,10), count(*) from thd01.yfs_order_header where document_type='0005' and order_header_key like '"+date+"%' and extn_profile_id='STH' group by substr(order_header_key,0,10) order by substr(order_header_key,0,10) asc";
	
	public  String hourlyBOSSej = "select substr(ej.hd_ej_key,0,10),count(*) from thd01.hd_related_e_j_keys ej,thd01.yfs_order_header yoh,"
						+ " thd01.yfs_order_line yol where yoh.order_header_key=yol.order_header_key and yol.order_line_key=ej.order_line_key "
						+ " and ej.createuserid='HDProcessOrderUpdatesServer'"
						+ " and ej.hd_ej_key like '"+date+"%' "
						+ " group by substr(ej.hd_ej_key,0,10) order by substr(ej.hd_ej_key,0,10) asc";
	
	public  String hourlySTHej = "select substr(ej.hd_ej_key,0,10),count(*) from thd01.hd_related_e_j_keys ej,thd01.yfs_order_header yoh,"
						+ " thd01.yfs_order_line yol where yoh.order_header_key=yol.order_header_key and yol.order_line_key=ej.order_line_key "
						+ " and ej.createuserid='HDDropShipServer'"
						+ " and ej.hd_ej_key like '"+date+"%' "
						+ " group by substr(ej.hd_ej_key,0,10) order by substr(ej.hd_ej_key,0,10) asc";
	
	
	public  String bossExc = "select flow_name, ERRORCODE, ERRORSTRING, count(*) from thd01.yfs_reprocess_error "
			+ "where flow_name in ('HDProcessOrderUpdates','HDReceiveCreatePO') "
			+ "and ERRORTXNID > '"+date+"' group by flow_name, ERRORCODE, ERRORSTRING order by flow_name";
	
	public  String sthExc = "select flow_name, ERRORCODE, ERRORSTRING, count(*) from thd01.yfs_reprocess_error "
			+ "where flow_name in ('HDDropShipServer','HDReceiveSTHCreatePO') "
			+ "and ERRORTXNID > '"+date+"' group by flow_name, ERRORCODE, ERRORSTRING order by flow_name";
	
	
	
	/*Hourly Statistics Queries*/
	


	public final String s2sFirstPart = "select 'S2S' order_type, count(distinct yoh.order_header_key) count from thd01.yfs_order_header yoh,thd01.yfs_order_line yol where " +
			"yoh.extn_host_order_ref like 'H%' and yoh.document_type = '0001' and yoh.order_header_key = yol.order_header_key and  yoh.order_header_key like '";
	
	public final String s2sLastPart = "%' and yol.extn_create_src_process = 14";
	
	public final String bopisFirstPart = "select distinct order_type, count(*) from (select distinct(order_header_key), decode (line_type_counts, 1,'BOPIS') order_type from " +
			"(select yol.order_header_key, count (distinct yol.line_type) line_type_counts from thd01.yfs_order_line yol, thd01.yfs_order_header yoh where " +
			"yoh.order_header_key = yol.order_header_key and yol.line_type = 'SL'  and yoh.document_type = '0001' and yol.order_header_key like '";
	
	public final String bopisLastPart = "%' and yol.extn_create_src_process = 9 and yoh.seller_organization_code = '8119' and  yol.EXTN_PROFILE_ID not in ('STH') group by yol.order_header_key) " +
			"where line_type_counts =1) group by order_type";
	
	public final String bossFirstPart = "select distinct order_type, count(*) from (select distinct(order_header_key), decode (line_type_counts, 1,'BOSS') order_type from " +
			"(select yol.order_header_key, count (distinct yol.line_type) line_type_counts from thd01.yfs_order_line yol, thd01.yfs_order_header yoh where " +
			"yoh.order_header_key = yol.order_header_key and yol.line_type = 'SO' and  yoh.document_type = '0001' and yol.order_header_key like '";
	
	public final String bossLastPart = "%' and yol.extn_create_src_process = 9 and yoh.seller_organization_code = '8119' and yol.EXTN_PROFILE_ID not in ('STH') group by yol.order_header_key) " +
			"where line_type_counts =1) group by order_type";
	
	public final String bopisBossFirstPart = "select distinct order_type, count(*) from (select distinct(order_header_key), decode (line_type_counts, 2,'BOPIS-BOSS') order_type from " +
			"(select yol.order_header_key, count (distinct yol.line_type) line_type_counts from thd01.yfs_order_line yol, thd01.yfs_order_header yoh where " +
			"yoh.order_header_key = yol.order_header_key and yol.line_type in ('SL','SO') and yoh.document_type = '0001' and yol.order_header_key like '";
	
	public final String bopisBossLastPart = "%' and yol.extn_create_src_process = 9 and yoh.seller_organization_code = '8119' and yol.EXTN_PROFILE_ID not in ('STH') group by yol.order_header_key) " +
			"where line_type_counts = 2) group by order_type";
	
	public final String splOrderFirstPart = "select count (distinct yoh.order_header_key) Special_order_count from thd01.yfs_order_line yol, thd01.yfs_order_header yoh where " +
			"yoh.extn_host_order_ref like 'H%' and yoh.order_header_key = yol.order_header_key and yoh.document_type = '0001' and yol.order_header_key like '";
	
	public final String splOrderLastPart = "%' and yol.extn_create_src_process in (0, 32)";
	
	public final String bodfsFirstPart ="select count (distinct yoh.order_header_key) BODFS_ORDER_COUNT from yfs_order_line yol, yfs_order_header yoh "+
"where yoh.document_type = '0001' and yoh.order_header_key = yol.order_header_key  and yoh.extn_profile_id in ('BODFS') and    yol.order_header_key like '";
  
	public final String bodfsLastPart =  "%' and yol.extn_create_loc_type = 'VIRTUAL' and yoh.SELLER_ORGANIZATION_CODE='8119' AND yoh.EXTN_ORDER_TYPE='BODFS_PROD' and yoh.EXTN_HOST_SRC_PROCESS='9'";
	
	public final String puresthFirstPart ="select count (distinct yoh.order_header_key) Pure_STH from thd01.yfs_order_line yol, thd01.yfs_order_header yoh "+
"where yoh.document_type = '0001'  and yoh.order_header_key = yol.order_header_key   and yoh.extn_profile_id in ('STH')   and yoh.order_header_key like '";

	public final String  puresthLastPart ="%' and yol.extn_create_src_process = 9  and yoh.seller_organization_code = '8119'   and yol.shipnode_key='8119'";

	public final String MixsthFirstPart =" select (select  count(*) from (select distinct(order_header_key), decode (line_type_counts, 3,'MIX STH', 2,'MIX STH') order_type "+ 
  " from(select yol.order_header_key, count (distinct yol.EXTN_PROFILE_ID) line_type_counts from thd01.yfs_order_line yol, thd01.yfs_order_header yoh "+
    " where yoh.order_header_key = yol.order_header_key and yol.line_type in ('SO','SL') and yol.EXTN_PROFILE_ID in ('STH','BOSS','BOPIS')"+
      " and yoh.document_type = '0001' and yol.order_header_key like '"; 
    
	public final String MixsthSecondPart ="%' and yol.extn_create_src_process = 9 and yoh.seller_organization_code = '8119' group by yol.order_header_key) where line_type_counts >=2) "+ 
"group by order_type) - (select  count(*) from (select distinct(order_header_key), decode (line_type_counts, 2,'BOPIS-BOSS') order_type "+
  "from (select yol.order_header_key, count (distinct yol.line_type) line_type_counts from thd01.yfs_order_line yol, thd01.yfs_order_header yoh "+ 
    "where yoh.order_header_key = yol.order_header_key and yol.line_type in ('SL','SO') and yoh.document_type = '0001' "+
      "and yol.order_header_key like '"; 
      
	public final String MixsthLastPart ="%' and yol.extn_create_src_process = 9 and yoh.seller_organization_code = '8119' and yol.EXTN_PROFILE_ID not in ('STH') "+
    "group by yol.order_header_key) where line_type_counts = 2) group by order_type) as MIXSTH from dual ";


	
	/*Resync Report Quereis*/
	

	
	public String query1 =  "select count(*) from store where last_compare_dt >= DATE_SUB(CURRENT_DATE, INTERVAL 7 DAY);";
	public String query2 = "select count(*) from resync_audit where store in (select distinct store from store where last_compare_dt >= DATE_SUB(CURRENT_DATE, INTERVAL 7 DAY)) and status = 'Finished' and stop_ts >= DATE_SUB(CURRENT_DATE, INTERVAL 7 DAY);";
	public String query3 = "select count(DISTINCT(LOC_NBR)) from THD01.INV_ADJ_TRNSM where INV_ADJ_OVRLY_IND = 'L' and CRT_TS > (sysdate-7)";
	public String query4 = "select count(DISTINCT(SKU_NBR)) from THD01.INV_ADJ_TRNSM where INV_ADJ_OVRLY_IND = 'L' and CRT_TS > (sysdate-7)";
	public String query5 = "SELECT round(sum(abs(difference))/count(*),2) FROM store_sku_compare where sterling_oh != -99999.0 and store_oh != -99999.0 and Create_dt > DATE_SUB(CURRENT_DATE, INTERVAL 7 DAY);";
	
	
	public String query11 = "select count(*) from store where last_compare_dt >= DATE_SUB(CURRENT_DATE, INTERVAL 14 DAY) and last_compare_dt < DATE_SUB(CURRENT_DATE , INTERVAL 7 DAY);";
	public String query21 = "select count(*) from resync_audit where store in (select distinct store from store where last_compare_dt >= DATE_SUB(CURRENT_DATE, INTERVAL 14 DAY) and last_compare_dt < DATE_SUB(CURRENT_DATE , INTERVAL 7 DAY)) and status = 'Finished' and stop_ts >= DATE_SUB(CURRENT_DATE, INTERVAL 14 DAY) and stop_ts < DATE_SUB(CURRENT_DATE, INTERVAL 7 DAY);";
	public String query31 = "select count(DISTINCT(LOC_NBR)) from THD01.INV_ADJ_TRNSM where INV_ADJ_OVRLY_IND = 'L' and CRT_TS >= (sysdate-14) and CRT_TS < (sysdate-7)";
	public String query41 = "select count(DISTINCT(SKU_NBR)) from THD01.INV_ADJ_TRNSM where INV_ADJ_OVRLY_IND = 'L' and CRT_TS >= (sysdate-14) and CRT_TS < (sysdate-7)";
	public String query51 = "SELECT round(sum(abs(difference))/count(*),2) FROM store_sku_compare where sterling_oh != -99999.0 and store_oh != -99999.0 and Create_dt >= DATE_SUB(CURRENT_DATE, INTERVAL 14 DAY) and Create_dt < DATE_SUB(CURRENT_DATE, INTERVAL 7 DAY);";

	
	public String query12 = "select count(*) from store where last_compare_dt >= DATE_SUB(CURRENT_DATE, INTERVAL 21 DAY) and last_compare_dt < DATE_SUB(CURRENT_DATE , INTERVAL 14 DAY);";
	public String query22 = "select count(*) from resync_audit where store in (select distinct store from store where last_compare_dt >= DATE_SUB(CURRENT_DATE, INTERVAL 21 DAY) and last_compare_dt < DATE_SUB(CURRENT_DATE , INTERVAL 14 DAY)) and status = 'Finished' and stop_ts >= DATE_SUB(CURRENT_DATE, INTERVAL 21 DAY) and stop_ts < DATE_SUB(CURRENT_DATE, INTERVAL 14 DAY);";
	public String query32 = "select count(DISTINCT(LOC_NBR)) from THD01.INV_ADJ_TRNSM where INV_ADJ_OVRLY_IND = 'L' and CRT_TS >= (sysdate-21) and CRT_TS < (sysdate-14)";
	public String query42 = "select count(DISTINCT(SKU_NBR)) from THD01.INV_ADJ_TRNSM where INV_ADJ_OVRLY_IND = 'L' and CRT_TS >= (sysdate-21) and CRT_TS < (sysdate-14)";
	public String query52 = "SELECT round(sum(abs(difference))/count(*),2) FROM store_sku_compare where sterling_oh != -99999.0 and store_oh != -99999.0 and Create_dt >= DATE_SUB(CURRENT_DATE, INTERVAL 21 DAY) and Create_dt < DATE_SUB(CURRENT_DATE, INTERVAL 14 DAY);";


	public String query13 = "select count(*) from store where last_compare_dt >= DATE_SUB(CURRENT_DATE, INTERVAL 28 DAY) and last_compare_dt < DATE_SUB(CURRENT_DATE , INTERVAL 21 DAY);";
	public String query23 = "select count(*) from resync_audit where store in (select distinct store from store where last_compare_dt >= DATE_SUB(CURRENT_DATE, INTERVAL 28 DAY) and last_compare_dt < DATE_SUB(CURRENT_DATE , INTERVAL 21 DAY)) and status = 'Finished' and stop_ts >= DATE_SUB(CURRENT_DATE, INTERVAL 28 DAY) and stop_ts < DATE_SUB(CURRENT_DATE, INTERVAL 21 DAY);";
	public String query33 = "select count(DISTINCT(LOC_NBR)) from THD01.INV_ADJ_TRNSM where INV_ADJ_OVRLY_IND = 'L' and CRT_TS >= (sysdate-28) and CRT_TS < (sysdate-21)";
	public String query43 = "select count(DISTINCT(SKU_NBR)) from THD01.INV_ADJ_TRNSM where INV_ADJ_OVRLY_IND = 'L' and CRT_TS >= (sysdate-28) and CRT_TS < (sysdate-21)";
	public String query53 = "SELECT round(sum(abs(difference))/count(*),2) FROM store_sku_compare where sterling_oh != -99999.0 and store_oh != -99999.0 and Create_dt >= DATE_SUB(CURRENT_DATE, INTERVAL 28 DAY) and Create_dt < DATE_SUB(CURRENT_DATE, INTERVAL 21 DAY);";

	
	//switch to Normal queries
	
	public String yfsExport = "select * from thd01.yfs_export where flow_name = 'HDSyncSuccessMsgToDB' and createts >= sysdate-1/96";
	public String transmissionState = "select * from bth_sess_parm where attr_nm = 'TransmitState'";
	
	public String resyncAudit = "select * from resync_audit where start_ts >= CURRENT_DATE;";

	
	//lag check queries
	
	public String orderHeader = "select max(modifyts) from yfs_order_header";
	public String hdInbox = "select max(modifyts) from HD_INBOX";
	public String orderLine = "select max(modifyts) from yfs_order_line";
	public String instructionDetail = "select max(modifyts) from YFS_INSTRUCTION_DETAIL";
	public String wcInstruction = "select max(modifyts) from HD_W_C_INSTRUCTION";
	public String personInfo = "select max(modifyts) from YFS_PERSON_INFO";
	public String willCall = "select max(modifyts) from HD_WILL_CALL";
	public String splOrd = "select max(modifyts) from HD_SPL_ORD";
	public String hdEvent = "select max(modifyts) from HD_EVENT";
	public String yfsWO = "select max(modifyts) from YFS_WORK_ORDER";
	public String relatedWO = "select max(modifyts) from HD_RELATED_WORK_ORDER";

	
	// Inventory Mismatch report queries
	public static String STG_ITEMIDENTIFIER_SELECT = "SELECT trim(INVI.ITEM_ID) || trim(INVI.UOM) || trim(INVI.PRODUCT_CLASS)|| trim(INVI.ORGANIZATION_CODE),INVI.INVENTORY_ITEM_KEY,IA.alias_value "
			+ "FROM THD01.YFS_ITEM IT, THD01.YFS_ITEM_ALIAS IA,THD01.YFS_INVENTORY_ITEM INVI "
			+ "WHERE INVI.ITEM_ID=IT.ITEM_ID AND INVI.UOM=IT.UOM  AND IT.ITEM_KEY = ia.ITEM_KEY  AND ia.alias_name='SKU'  AND INVI.ORGANIZATION_CODE = 'HDUS' AND ia.alias_value IN (";
	/*public static String STORE_SKUQUERY_SELECT = "select sku_nbr,mer_dept_nbr,mer_class_nbr,mer_sub_class_nbr,oh_qty, sku_typ_cd from sku "
			+ "where sku_typ_cd != 'B' and sku_typ_cd != 'F' and sku_typ_cd != 'S' and (CURR_RMETH_CD != 4 "
			+ "or CURR_RMETH_CD is null) and oh_qty != 0";*/
	
	public static String STORE_SKUQUERY_SELECT = "select sku.sku_nbr,sku.mer_dept_nbr,sku.mer_class_nbr,sku.mer_sub_class_nbr,sku.oh_qty,sku.sku_typ_cd,NVL(cust_sku_dmnd.cord_rsvd_qty,0) + NVL(cust_sku_dmnd.cord_alloc_qty,0) as dmnd_qty"
            + "  from sku left outer join cust_sku_dmnd on ( sku.sku_nbr = cust_sku_dmnd.sku_nbr )"
            + " where sku_typ_cd != 'B' and sku_typ_cd != 'F' and sku_typ_cd != 'S' and (CURR_RMETH_CD != 4"
            + "     or CURR_RMETH_CD is null) and oh_qty != 0";
	
	
}