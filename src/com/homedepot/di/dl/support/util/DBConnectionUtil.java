package com.homedepot.di.dl.support.util;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.sql.*;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.naming.*;
import javax.sql.*;

import org.apache.log4j.Logger;

public class DBConnectionUtil extends Constants {
	private static final Logger logger = Logger
			.getLogger(DBConnectionUtil.class);

	public Connection getJNDIConnection(String contextName) {
		String DATASOURCE_CONTEXT = contextName;

		Connection conn = null;
		try {
			Context initialContext = new InitialContext();
			Context envContext = (Context) initialContext
					.lookup("java:comp/env");

			DataSource datasource = (DataSource) envContext
					.lookup(DATASOURCE_CONTEXT);
			if (datasource != null) {
				conn = datasource.getConnection();
				//logger.info("Connected: " + conn);
			} else {
				logger.info("Failed to lookup datasource.");
			}
		} catch (NamingException ex) {
			logger.info("Cannot get connection: " + ex);
			ex.printStackTrace();
		} catch (Exception e) {
			logger.info("Cannot get connection:: " + e);
			e.printStackTrace();
		}
		return conn;
	}
	
	public static String createQueryString(String interopApiName,
			String inputXML) {

		String URLEncodedInputXML = null;
		try {
			URLEncodedInputXML = URLEncoder.encode(inputXML, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			System.out.println("In createQueryString(): Problem encoding with UTF-8");

		}
		StringBuilder queryString = new StringBuilder();
		queryString.append("InteropApiName=");
		queryString.append(interopApiName);
		queryString.append("&IsFlow=Y");
		queryString.append("&YFSEnvironment.userId=admin");
		queryString.append("&InteropApiData=");
		queryString.append(URLEncodedInputXML);

		return queryString.toString();
	}

	
	public static String removeXMLDeclaration(String xml) {
		Pattern p = Pattern.compile("<\\?xml.*?\\?>");
		Matcher m = p.matcher(xml);
		return m.replaceFirst("");
	}
	
	public static String getToken() {

		String token = null;
		String lcp = "PR";

		if ("PR".equalsIgnoreCase(lcp.trim())) {
			token = "FdYshd1y8XxHXkJBkN78uoSRh2NVesoRcDI8yKpxSCZWNv8FzTN7y5dROPOc864ureuT712XRAXMURw9HAcgonoYGsht3BWqLexYs1OKRyT5gzb8UGlUo13Zra3WW6T3n8v475Y47bVYkSH3M6iNaxNeXec31FhuyoAMDoYvhl60BYmp79JAd3A8whg92PCaAHyrdV2oBFNR3aKVlwdy3ZOLxQ5SjgZP9lGx5KpobkqzMJnrte3LWB2QKXxFyAlhJsCTr4LLWgQHK5IkOmNK6Mrf6LnFWCwfy93";
		} else {
			token = "0UF3C4qryss7pIXu2A2G6iE13eYSiyKtgQ08A2x4PlnOJchufHxU1GsC5NA8MaJJgrq8X46A3B7QS9rXnyPRSPla9ZyVfUZWwmUf6XEvZMAFUeXtXgY61QEnYXSSGpHJ79JvnrpgRa2Bc3WRL9ZXRIKPdtxto1uauZbYuTK9vRPg9u8arRRedWmqYXmjjCaa7ZPVn3OqIdHLWZnyTGEvGr92fZv8mAgqwHvrmFjWsgts7ICJvw96NiU2FhrWwuDFl0tQwLIBDldLXd1uKXXISG9lshDLKkhPr661";
		}

		return token;
	}
	
	public Connection getStoreconnection(String store) 
	{
		final String driverClass = Constants.driverClass;

		String strDBUser = Constants.userID;
		String userPassword = Constants.userPassword;
		String connectionURLThin = String.format(Constants.storeJDBCUrl,store); 
				
				
		CipherEncrypter encrypter = new CipherEncrypter("StoreDBPwdEncryption"); 
		
		Connection con = null;

		try {
			Class.forName(driverClass);
			Properties properties = new Properties();
			properties.put("connectTimeout", "60000");

			con = DriverManager.getConnection(connectionURLThin,userID,encrypter.decrypt(userPassword));
		}

		catch (Exception e) {
			
			logger.info("Store : "+ store + " : "+e);
		}
		return con;
	}
	
	/**
	 * This method will close the SQL connection resources (Connection, 
	 * PreparedStatement, ResultSet).
	 */
	public void closeResources (PreparedStatement ps, ResultSet rs, Connection conn) throws SQLException
	{
		if(null != ps)
		{
			ps.close();
		}
		if(null != rs)
		{
			rs.close();
		}
		if(null != conn)
		{
			conn.close();
		}
	}
	
}
