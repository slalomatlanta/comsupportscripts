package com.homedepot.di.dl.support.confirm.dto;

public class DelConfirmDTO {
	
	
	String workOrderNo = null;
	String workOrderKey = null;
	
	String sterlingResponse = null;
	
	
	public String getWorkOrderNo() {
		return workOrderNo;
	}
	public void setWorkOrderNo(String workOrderNo) {
		this.workOrderNo = workOrderNo;
	}
	public String getWorkOrderKey() {
		return workOrderKey;
	}
	public void setWorkOrderKey(String workOrderKey) {
		this.workOrderKey = workOrderKey;
	}
	
	public String getSterlingResponse() {
		return sterlingResponse;
	}
	public void setSterlingResponse(String sterlingResponse) {
		this.sterlingResponse = sterlingResponse;
	}
	
	
	
}
