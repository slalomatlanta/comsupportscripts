package com.homedepot.di.dl.support.confirm.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.homedepot.di.dl.support.confirm.dao.FixCustomWO;

/**
 * Servlet implementation class ConfirmCustomDelivery
 */
public class ConfirmCustomDelivery extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = Logger
			.getLogger(ConfirmCustomDelivery.class);
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ConfirmCustomDelivery() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		
		
//		DelConfirmDAO dao = new DelConfirmDAO();
		FixCustomWO dao = new FixCustomWO();
		
		try {
			//inputAPIList = dao.getOrdersPendingDelConfirmation(inputFileLocation);
			//outputAPIList = dao.submitDataToSterling(inputAPIList);
			//dao.writeToCSV(outputFileLocation, outputAPIList);
			
			dao.confirmWO();
			
		}catch (Exception e) {
			e.getMessage();
			 logger.debug("confirmWO Exception Occured " + e.getStackTrace());
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
