package com.homedepot.di.dl.support.confirm.web;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.homedepot.di.dl.support.confirm.dao.DelConfirmDAO;
import com.homedepot.di.dl.support.confirm.dto.DelConfirmDTO;

/**
 * Servlet implementation class ConfirmDelivery
 */
@WebServlet("/ConfirmDelivery")
public class ConfirmDelivery extends HttpServlet {
	private static final long serialVersionUID = 1L;
     
	private static final Logger logger = Logger
			.getLogger(ConfirmDelivery.class);
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ConfirmDelivery() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		
		
		DelConfirmDAO dao = new DelConfirmDAO();
		
		try {
			//inputAPIList = dao.getOrdersPendingDelConfirmation(inputFileLocation);
			//outputAPIList = dao.submitDataToSterling(inputAPIList);
			//dao.writeToCSV(outputFileLocation, outputAPIList);
			
			dao.confirmWO();
			
		}catch (Exception e) {
			e.getMessage();
			 logger.debug("confirmWO Exception Occured " + e.getStackTrace());
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
