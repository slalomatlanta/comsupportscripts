package com.homedepot.di.dl.support.confirm.dao;


import org.apache.log4j.Logger;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.homedepot.di.dl.support.util.DBConnectionUtil;

	
	/**
	 * Jersey Client to call the Sterling services for the Order Interface
	 * <br>
	 * This class handles the communication to and from the service
	 *  
	 */
	public class SterlingJerseyClient {
		
		private static final Logger logger = Logger
				.getLogger(SterlingJerseyClient.class);
		
		/**
		 * This method creates a jersey client and hits the Sterling. It
		 * gets the response for the postRequest sent.
		 * 
		 * @param url
		 * @param postRequest
		 * @throws ProcessingException
		 */
		public String send(String url, String postRequest)	{	
			
			long startTime = System.currentTimeMillis();
			
			//Map<KeyNameEnum, Object> infoLogger = OrangeLogManager.getLoggerMap(Thread.currentThread().getName());
			
			
			// Create Jersey client
			Client client = Client.create();
			client.setConnectTimeout(60000);
			client.setReadTimeout(60000);
			String token = DBConnectionUtil.getToken();
			
			// Create WebResource 
			WebResource webResource = client.resource(url);
			ClientResponse response = null;
			
			try {				
				response = webResource.header("THDService-Auth", token)
				.type("application/x-www-form-urlencoded")
				.post(ClientResponse.class, postRequest);
			} 
			catch (Exception e)
			{
				logger.info("Unexpected error from webresource: " + e.getMessage());
				logger.info("Sterling Response Time: " + (System.currentTimeMillis()-startTime));
				
			}
			
			logger.info("Sterling response: " + response);
			// Retrieve status and check if it is OK
			int status = response.getStatus();
			
			logger.info("Callout HTTP Response: " + status);

			if (status != 200) {
				logger.info("Unsuccesful Status response from Sterling: " + status);
				logger.info("Sterling Response Time: " + (System.currentTimeMillis()-startTime));
				 
			}
			String sterlingResponse = response.getEntity(String.class);
			
			if (sterlingResponse == null) {
				logger.info("Application Error during call to Sterling - Response is null");
				logger.info("Sterling Response Time: " + (System.currentTimeMillis()-startTime));
				}
			 
			// Log results
			
			logger.info("Sterling Response Time: " + (System.currentTimeMillis()-startTime));
			
			return sterlingResponse;
		}
}
