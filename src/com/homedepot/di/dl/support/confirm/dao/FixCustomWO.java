package com.homedepot.di.dl.support.confirm.dao;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeMessage.RecipientType;

import org.apache.log4j.Logger;

import au.com.bytecode.opencsv.CSVReader;

import com.homedepot.di.dl.support.util.Constants;
import com.homedepot.di.dl.support.util.DBConnectionUtil;
import com.homedepot.di.dl.support.confirm.dao.SterlingJerseyClient;
import com.homedepot.di.dl.support.confirm.dto.DelConfirmDTO;

public class FixCustomWO {

	private static final Logger logger = Logger
			.getLogger(FixCustomWO.class);
	
	/*public static void main() throws Exception
	{
		
		DelConfirmDAO.confirmWO();
	}*/
	
	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void confirmWO()  {
		// TODO Auto-generated method stub
		//String inputFileLocation = "C:\\test\\input.csv";
		//String outputFile = "c:\\Tax\\EOC\\EOCPendingPublish.csv";
								
		String outputFileLocation = "C:\\test\\SterlingWo.csv";
//		String outputFileLocation = "/opt/isv/apache-tomcat/temp/WorkOrder.csv";
		try
		{
			
		FileWriter fw = new FileWriter(outputFileLocation, false);
		fw.append("ExtnHostOrderReference");
		fw.append(',');
		fw.append("WorkOrderNo");
		fw.append(',');
		fw.append("WorkOrderType");
		fw.append(',');
		fw.append("modifyts");
		fw.append(',');
		fw.append("Status");
		fw.append("\n");
		String response = null;
		String truncResponse = null;
		try {
			//inputAPIList = getOrdersPendingDelConfirmation(inputFileLocation);
			
			DBConnectionUtil dbConn = new DBConnectionUtil();

			//Connection con = dbConn
			//.getJNDIConnection(Constants.STERLING_SSC_RO_JNDI);
			Connection con = null;
			final String driverClass = "oracle.jdbc.driver.OracleDriver";
			final String connectionURLThin = "jdbc:oracle:thin://@aprbgor12-scan.homedepot.com:1521/dpr77mm_sro01";
			
			final String userID = "MMUSR01";
			final String userPassword = "COMS_MMUSR01";
			
			Class.forName(driverClass);
			con = DriverManager.getConnection(connectionURLThin, userID,
					userPassword);
			Statement stmt = null;
			ResultSet rs = null;

			stmt = con.createStatement();
			
			String getOrder = "SELECT RELATED_WORK_ORDER_KEY, "
					+ "  HOST_ORDER_REF, "
					+ "  RELATED_WORK_ORDER_NO, "
					+ "  MODIFYTS "
					+ "FROM thd01.hd_related_work_order "
					+ "WHERE modifyts < (sysdate - 12/24) "
					+ "AND status     ='1100.675' ";
//					+ "AND RELATED_WORK_ORDER_KEY in ('20160929204803840482926','20160929164626820926860','201610040851111171892530','20160928133014706156224','20160929090234769861361','201610030640551074270183','201610030643311074321110','201610030818271077217326','20160928153450721318528','20160930051847856337859') ";
							
			//logger.info(getOrder);
			rs = stmt.executeQuery(getOrder);
			

			
			while(rs.next())
			{
				String WorkOrderno = rs.getString(3).trim();
				String WorkOrderKey = rs.getString(1).trim();
//				String workOrdertype = rs.getString(4);
				//logger.info("WorkOrderno : "+ WorkOrderno +"     WorkOrderKey : "+ WorkOrderKey +             "workOrdertype : "+ workOrdertype);
				
				fw.append(rs.getString(2));
				fw.append(",");
				fw.append(rs.getString(3));
				fw.append(",");
				fw.append(rs.getString(4));
				fw.append(",");
//				fw.append(rs.getString(5));
//				fw.append(",");
				
			 response = submitDataToSterling(WorkOrderno,WorkOrderKey);
			 truncResponse = CheckResponseType(response);
			fw.append(truncResponse.trim());
			fw.append("\n");
			}
			
			rs.close();
			stmt.close();
			con.close();
			
			
		}catch (Exception e) {
			e.getStackTrace();
			 logger.debug("Exception Occured " + e.getStackTrace());
		}
		fw.flush();
		fw.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		
		
		BufferedReader br = null;
		 int count = 0;
		try {
			br = new BufferedReader(new FileReader(outputFileLocation));
		
		String line = "";
	   
	    
			while ((line = br.readLine()) != null) 
			{
				count+=1;
			}
		
		
	    if(count > 1)
	    {
	    	
	    	sendAttachment();
	    }
	    
	    else
	    {
	    	logger.info("No records found");
	    }
	    
	}
	catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
		
	}
	
	/*public static List<DelConfirmDTO> getOrdersPendingDelConfirmation(String fileLocation){
		
		List<DelConfirmDTO> inputAPIList = new ArrayList<DelConfirmDTO>();
		
		String[] row = null;
		CSVReader csvReader = null;
		boolean headerRow = true;
		try {
			csvReader = new CSVReader(new FileReader(fileLocation));
			while ((row = csvReader.readNext()) != null) {
				
					DelConfirmDTO dto = new DelConfirmDTO();
					
					//dto.setOrder(row[0].trim());
					dto.setWorkOrderNo(row[0].trim());
					dto.setWorkOrderKey(row[1].trim());
					//dto.setWorkOrderType(row[3].trim());
					
					inputAPIList.add(dto);
				
			}
			csvReader.close();
		} catch (Exception e) {
			e.getStackTrace();
			 logger.debug("Exception Occured " + e.getStackTrace());
		}
		System.out.println("No of rows read from file : "+inputAPIList.size());
		logger.info("No of rows read from file : "+inputAPIList.size());
		return inputAPIList;
	}
	*/
	
	private static String CheckResponseType(String response) {
		
		String errCode = "";
		String errDesc = "";
		if (response.contains("Error")) {
			int firstIndex = response.indexOf("ErrorCode");
			firstIndex = firstIndex + 11;
			int lastIndex = response.indexOf("ErrorDescription");
			errCode = response.substring(firstIndex, lastIndex-2); 
			
			firstIndex = response.indexOf("ErrorDescription");
			firstIndex = firstIndex + 18;
			lastIndex = response.indexOf("ErrorRelatedMoreInfo");
			errDesc = response.substring(firstIndex, lastIndex-2); 
			
			response = errCode + " - " + errDesc;
		}

		System.out.println("Response status : " + response);
		return response;

	}
	
	public static String submitDataToSterling(String workOrderno, String workOrderKey){
		
		
		String request = null;
		String response = null;
		String woType = "Custom";
		
//		if("Y".equalsIgnoreCase(workOrdertype))
//		{
//			woType = "Custom";
//		}
//		
//		else if("N".equalsIgnoreCase(workOrdertype))
//		{
//			woType = "Sterling";
//		}
		
	
			request = "<WorkOrder WorkOrderNo=\""+workOrderno+"\" WorkOrderKey=\""+workOrderKey+"\" WorkOrderType=\""+woType+"\" CreateReturn=\"Y\" />";
			System.out.println("sterlingInputXML : "+request);
			response = invokeConfirmWOService(request);
			System.out.println("sterlingResponseXML : "+response);
			
		
		return response;
	}
	
	public static String invokeConfirmWOService(String sterlingInputXML) {

		String sterlingResponseXML = "";
		String url = null;

		String queryString = DBConnectionUtil.createQueryString("ConfirmWO",
				sterlingInputXML);
		SterlingJerseyClient sterlingJerseyClient = new SterlingJerseyClient();

		url = Constants.STERLING_URL;
		logger.debug("URL : " + url);

		sterlingResponseXML = sterlingJerseyClient.send(url, queryString);
		System.out.println("sterlingResponseXML : "+sterlingResponseXML);
		sterlingResponseXML = DBConnectionUtil.removeXMLDeclaration(sterlingResponseXML);
		return sterlingResponseXML;

	}
	
	public static void writeToCSV(String fileLocation,
			List<DelConfirmDTO> inputAPIList) {


		FileWriter writer;
		try {
			writer = new FileWriter(fileLocation, true);

			//writer.append("OrderNo");
			//writer.append(",");
			writer.append("WorkOrderNo");
			writer.append(",");
			writer.append("WorkOrderKey");
			writer.append(",");
			//writer.append("WorkOrderType");
			//writer.append(",");
			writer.append("SterlingResponse");
			writer.append("\n");

			for (int i = 0; i < inputAPIList.size(); i++) {
				DelConfirmDTO dto = new DelConfirmDTO();
				dto = inputAPIList.get(i);
				//writer.append(dto.getOrder());
				//writer.append(",");
				writer.append(dto.getWorkOrderNo());
				writer.append(",");
				writer.append("'"+dto.getWorkOrderKey());
				writer.append(",");
				//writer.append(dto.getWorkOrderType());
				//writer.append(",");
				writer.append(dto.getSterlingResponse().trim());
				writer.append("\n");
			}

			writer.flush();
			writer.close();
			logger.info("File created at " + fileLocation);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	public static void sendAttachment() throws Exception{
		 String host = "mail1.homedepot.com";
	  String from = "horizon@cpliisad.homedepot.com";
//	  String[] to = {"_2fc77b@homedepot.com" };
	  String[] to = {"rakesh_j_satya@homedepot.com" };
	  String[] Cc ={"NISHYANTH_R_PINGLE1@homedepot.com"};
	  // Get system properties
	  Properties properties = System.getProperties();

	  // Setup mail server
	  properties.setProperty("mail.smtp.host", host);

	  // Get the default Session object.
	  Session session = Session.getInstance(properties);

	  // Define message
	  Message message = new MimeMessage(session);
	  message.setFrom(new InternetAddress(from));
	  InternetAddress[] addressTo = new InternetAddress[to.length];
	  InternetAddress[] addressCc = new InternetAddress[Cc.length];
	  for (int i = 0; i < to.length; i++) 
	  { addressTo[i] = new InternetAddress(to[i]); 
	  //System.out.println("To Address "+to[i]);
	  } 
	  for (int j = 0; j < Cc.length; j++) 
	  { addressCc[j] = new InternetAddress(Cc[j]); 
//	  System.out.println("Cc Address "+Cc[j]);
	  }
	  message.setRecipients(RecipientType.TO, addressTo); 
	  message.setRecipients(RecipientType.CC, addressCc); 
	  message.setSubject("Confirm Custom WorkOrder");

	  // Create the message part 
	  BodyPart messageBodyPart = new MimeBodyPart();
	  String msgContent = "Hi All,<br/><br/>PFA WorkOrders Processed today<br/> <br/><br/>Thanks<br/>";
  msgContent = msgContent + "COM Multichannel Support";
	  // Fill the message
	  messageBodyPart.setContent(msgContent ,"text/html");
	  	

	  Multipart multipart = new MimeMultipart();
	  multipart.addBodyPart(messageBodyPart);

	  // Part two is attachment
	  messageBodyPart = new MimeBodyPart();
	  String filename = "C:\\test\\SterlingWo.csv";
//	  String filename = "/opt/isv/apache-tomcat/temp/WorkOrder.csv";
	  DataSource source = new FileDataSource(filename);
	  messageBodyPart.setDataHandler(new DataHandler(source));
	  messageBodyPart.setFileName(source.getName());
	  multipart.addBodyPart(messageBodyPart);

	  // Put parts in message
	  message.setContent(multipart);

	  // Send the message
	  Transport.send(message);
	   //System.out.println("Msg Send ....");
	  }

}
