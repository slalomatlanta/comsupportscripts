package com.homedepot.di.dl.support.loadSourcingRules.dto;

public class SourcingBatchDTO {
	private int serialNo;
	private String jobName;
	private String jobId;
	private String jobStatus;
	private String validationStatus;
	private String jobStartTime;
	private String jobEndTime;
	private String reprocessErrorCount;
	private String sterlingTableCount;
	private String comments;
	private String logFileLink;
	private String queueDepth;
	private String reprocessQuery;
	private String sterlingQuery;
	private String queueName;
	
	
	
	public String getQueueDepth() {
		return queueDepth;
	}

	public void setQueueDepth(String queueDepth) {
		this.queueDepth = queueDepth;
	}

	public String getReprocessQuery() {
		return reprocessQuery;
	}

	public void setReprocessQuery(String reprocessQuery) {
		this.reprocessQuery = reprocessQuery;
	}

	public String getSterlingQuery() {
		return sterlingQuery;
	}

	public void setSterlingQuery(String sterlingQuery) {
		this.sterlingQuery = sterlingQuery;
	}

	public String getQueueName() {
		return queueName;
	}

	public void setQueueName(String queueName) {
		this.queueName = queueName;
	}

	public String getLogFileLink() {
		return logFileLink;
	}

	public void setLogFileLink(String logFileLink) {
		this.logFileLink = logFileLink;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getReprocessErrorCount() {
		return reprocessErrorCount;
	}

	public void setReprocessErrorCount(String reprocessErrorCount) {
		this.reprocessErrorCount = reprocessErrorCount;
	}

	public String getSterlingTableCount() {
		return sterlingTableCount;
	}

	public void setSterlingTableCount(String sterlingTableCount) {
		this.sterlingTableCount = sterlingTableCount;
	}

	public String getJobStartTime() {
		return jobStartTime;
	}

	public void setJobStartTime(String jobStartTime) {
		this.jobStartTime = jobStartTime;
	}

	public String getJobEndTime() {
		return jobEndTime;
	}

	public void setJobEndTime(String jobEndTime) {
		this.jobEndTime = jobEndTime;
	}

	public int getSerialNo() {
		return serialNo;
	}

	public void setSerialNo(int serialNo) {
		this.serialNo = serialNo;
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	public String getJobStatus() {
		return jobStatus;
	}

	public void setJobStatus(String jobStatus) {
		this.jobStatus = jobStatus;
	}

	public String getValidationStatus() {
		return validationStatus;
	}

	public void setValidationStatus(String validationStatus) {
		this.validationStatus = validationStatus;
	}

}
