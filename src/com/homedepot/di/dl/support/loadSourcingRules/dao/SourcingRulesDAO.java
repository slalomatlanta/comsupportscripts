package com.homedepot.di.dl.support.loadSourcingRules.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Enumeration;

import javax.jms.Message;
import javax.jms.Queue;
import javax.jms.QueueBrowser;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueSession;
import javax.jms.Session;
import javax.naming.Context;
import javax.naming.InitialContext;

import org.apache.log4j.Logger;

import com.homedepot.di.dl.support.loadSourcingRules.dto.SourcingBatchDTO;
import com.homedepot.di.dl.support.loadSourcingRules.service.CheckSourcingBatchStatus;
import com.homedepot.di.dl.support.util.Constants;
import com.homedepot.di.dl.support.util.DBConnectionUtil;
import com.homedepot.di.dl.support.util.QueueUtil;

public class SourcingRulesDAO {
	private static final Logger logger = Logger
			.getLogger(SourcingRulesDAO.class);
	public static SourcingBatchDTO getQueueDepth(SourcingBatchDTO sb) {
		int sNo = sb.getSerialNo();
		String queueName = "";
		if (sNo == 1) {
			queueName = "DI.DL.STERLING.SRCNGPARAMETERSUPDATE";
		} else if (sNo == 2) {
			queueName = "DI.DL.STERLING.SERVICESKILLUPDATE";
		} else if (sNo == 3) {
			queueName = "DI.DL.STERLING.CARRESERVICELEVELUPDATE";
		} else if (sNo == 4) {
			queueName = "DI.DL.STERLING.CARRIERUPDATE";
		} else if (sNo == 5) {
			queueName = "DI.DL.STERLING.RESOURCEPOOLUPDATE";
		} else if (sNo == 6) {
			queueName = "DI.DL.STERLING.NODENOTIFICATIONPERIOD";
		}

		String resourceName = "jms/" + queueName + "_get";
		String lookIpName = "jms/MQGetQCF";
//		QueueUtil qt = new QueueUtil();
//		int count = qt.getQueueDepth(queueName, resourceName, lookIpName);
		// List<QueueDTO> xmlList = new ArrayList<QueueDTO>();
		logger.debug(queueName);
		Context context = null;
		InitialContext ctx = null;
		Queue queue = null;
		QueueConnectionFactory connFactory = null;
		QueueConnection queueConn = null;
		QueueSession queueSession = null;
		QueueBrowser queueBrowser = null;
		int count = 0;
		try {
			ctx = new InitialContext();
			context = (Context) ctx.lookup("java:comp/env");
			queue = (Queue) context.lookup(resourceName);
			connFactory = (QueueConnectionFactory) context.lookup(lookIpName);
			queueConn = connFactory.createQueueConnection();
			queueSession = queueConn.createQueueSession(false,
					Session.AUTO_ACKNOWLEDGE);
			queueBrowser = queueSession.createBrowser(queue);
			queueConn.start();

			// browse the messages
			Enumeration e = null;
			e = queueBrowser.getEnumeration();

			// count number of messages
			while (e.hasMoreElements()) {
				Message message = (Message) e.nextElement();
				count++;
			}

			queueConn.close();

		} catch (Exception e1) {
			logger.debug("Queue Exception: " + e1);
			e1.printStackTrace();
		} finally {

		}
		logger.debug(queueName + " has " + count + " messages ");
		sb.setQueueDepth(""+count);
		sb.setQueueName(queueName);
		return sb;
	}

	public static SourcingBatchDTO getReprocessErrorCount(SourcingBatchDTO sb) {
		int count = 0;
		String flowname = "";
		int sNo = sb.getSerialNo();
		if (sNo == 1) {
			flowname = "'HDSourcingParameterUpdate', 'HDSourcingParameterUpdateFromQueue'";
		} else if (sNo == 2) {
			flowname = "'HDServiceSkillUpdate', 'HDServiceSkillUpdateFromQueue'";
		} else if (sNo == 3) {
			flowname = "'HDCarrServiceLevelUpdate', 'HDCarrServiceLevelUpdateFromQueue'";
		} else if (sNo == 4) {
			flowname = "'HDCarrierUpdate', 'HDCarrierUpdateFromQueue'";
		} else if (sNo == 5) {
			flowname = "'HDResourcePoolUpdate', 'HDResourcePoolUpdateFromQueue'";
		} else if (sNo == 6) {
			flowname = "'HDManageNodeNotificationPeriod', 'HDManageNodeNotificationPeriodFromQueue'";
		}

		String query = "select count(*) from THD01.YFS_REPROCESS_ERROR where FLOW_NAME in ("
				+ flowname
				+ ") and state='Initial' and Errortxnid > to_char((sysdate-1),'YYYYMMDD')";
		
		DBConnectionUtil dbConn = new DBConnectionUtil();
		Connection sscConn = dbConn.getJNDIConnection(Constants.STERLING_SSC_RO_JNDI);
		Statement stmt = null;
		ResultSet rs = null;
		try {
			stmt = sscConn.createStatement();
			rs = stmt.executeQuery(query);
			while (rs.next()) {
				count = Integer.parseInt(rs.getString(1));
			}
			rs.close();
			stmt.close();
			sscConn.close();
		} catch (SQLException e) {
			logger.debug("Caught exception while executing query" + e);
		}
		sb.setReprocessErrorCount(""+count);
		sb.setReprocessQuery(query);
		return sb;
	}
	
	public static SourcingBatchDTO getModifiedRecords(SourcingBatchDTO sb) {
		int count = 0;
		String query = "";
		int sNo = sb.getSerialNo();
		
		if (sNo == 1) {
			query = "select count(*) from thd01.Yfs_common_code where modifyts > (sysdate - 12/24)";
		} else if (sNo == 2) {
			query = "select count(*) from thd01.Yfs_service_skill where modifyts > (sysdate - 12/24)";
		} else if (sNo == 3) {
			query = "select count(*) from thd01.Yfs_carrier_service where modifyts > (sysdate - 12/24)";
		} else if (sNo == 4) {
			query = "select count(*) from thd01.yfs_organization where modifyts > (sysdate - 12/24)";
		} else if (sNo == 5) {
			query = "select count(*) from thd01.yfs_res_pool where resource_pool_id ='RP_DS' and modifyts > (sysdate - 12/24)";
		} else if (sNo == 6) {
			query = "select count(*) from thd01.yfs_node_notification_perd where modifyts > (sysdate - 12/24)";
		}
		
		DBConnectionUtil dbConn = new DBConnectionUtil();
		Connection sscConn = dbConn.getJNDIConnection(Constants.STERLING_SSC_RO_JNDI);
		Statement stmt = null;
		ResultSet rs = null;
		try {
			stmt = sscConn.createStatement();
			rs = stmt.executeQuery(query);
			while (rs.next()) {
				count = rs.getInt(1);
			}
			rs.close();
			stmt.close();
			sscConn.close();
		} catch (SQLException e) {
			logger.debug("Caught exception while executing query" + e);
		}
		sb.setSterlingTableCount(""+count);
		sb.setSterlingQuery(query);
		return sb;
	}

}
