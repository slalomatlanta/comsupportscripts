package com.homedepot.di.dl.support.loadSourcingRules.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.apache.log4j.Logger;
import org.apache.xerces.parsers.DOMParser;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.homedepot.di.dl.support.loadSourcingRules.dao.SourcingRulesDAO;
import com.homedepot.di.dl.support.loadSourcingRules.dto.SourcingBatchDTO;
import com.homedepot.di.dl.support.loadSourcingRules.web.LoadSourcingRulesServlet;

public class CheckSourcingBatchStatus {
	private static final Logger logger = Logger
			.getLogger(CheckSourcingBatchStatus.class);
	public static ArrayList<SourcingBatchDTO> getJobDetails(
			ArrayList<SourcingBatchDTO> inputDetails) {
		
		
		ArrayList<SourcingBatchDTO> jobDetails = new ArrayList<SourcingBatchDTO>();

		for (SourcingBatchDTO sb : inputDetails) {
			int queueDepth = 0;
			int reprocessErrorCount = 0;
			int sterlingTableCheck = 0;
			String validationStatus = "", reprocessCheck = "";
			if (sb.getJobStatus() != null) {
				if (sb.getJobStatus().equalsIgnoreCase("Queued")) {
					String jobId = triggerJob(sb.getSerialNo()); //Triggering Job
					sb.setJobId(jobId);
					sb = getJobDetails(sb);
					if (sb.getJobStatus().equalsIgnoreCase("Done")) {
						DateFormat dateFormat = new SimpleDateFormat(
								"yyyy-MM-dd HH:mm:ss.SSS");
						Date today = new Date();
						String sysdate = dateFormat.format(today);
						sb.setJobEndTime(sysdate);
						if (sb.getValidationStatus() != null) {
							if (sb.getValidationStatus().equalsIgnoreCase(
									"NotStarted")) {
//								queueDepth = SourcingRulesDAO.getQueueDepth(sb.getSerialNo());
								sb = SourcingRulesDAO.getQueueDepth(sb);
								queueDepth = Integer.parseInt(sb.getQueueDepth());
								if (queueDepth == 0){
									try {
									    Thread.sleep(2000);                 //1000 milliseconds is one second.
									} catch(InterruptedException ex) {
									    Thread.currentThread().interrupt();
									}
//									reprocessErrorCount = SourcingRulesDAO.getReprocessErrorCount (sb.getSerialNo());
//									sterlingTableCheck = SourcingRulesDAO.getModifiedRecords (sb.getSerialNo());
									sb = SourcingRulesDAO.getReprocessErrorCount (sb);
									reprocessErrorCount = Integer.parseInt(sb.getReprocessErrorCount());
									
									sb = SourcingRulesDAO.getModifiedRecords (sb);
									sterlingTableCheck = Integer.parseInt(sb.getSterlingTableCount());
									
//									sb.setSterlingTableCount(""+sterlingTableCheck);
//									sb.setReprocessErrorCount(""+reprocessErrorCount);
									if (reprocessErrorCount == 0){
										reprocessCheck = "Completed";
										sb.setValidationStatus(reprocessCheck);
									}
									else {
										reprocessCheck = "Reprocess errors present. Please contact COM Support team.";
										sb.setValidationStatus("Failed");
										sb.setComments(reprocessCheck);
									}
								}
								else {
									try{
										Date today1 = new Date();
										SimpleDateFormat sdf = new SimpleDateFormat(
												"yyyy-MM-dd HH:mm:ss.SSS");
										Date date = sdf.parse(sb.getJobStartTime());
										long jobTimestamp = date.getTime();
										long sysdate1 = today1.getTime();
										if ((sysdate1 - jobTimestamp) > 600000){
											sb.setValidationStatus("Failed");
											sb.setComments("Queue Depth is not decreasing from last 10 mins. Please contact COM Support team.");
										}
										}catch (Exception e){
											logger.debug("Exception Occurred while parsing a date "+e);
											jobDetails.add(sb);
											break;
										}
									sb.setValidationStatus("InProgress");
									jobDetails.add(sb);
									break;
								}
								}
							}
						}
					jobDetails.add(sb);
					break;
					}
				
				else if ((sb.getJobStatus().equalsIgnoreCase("wait")) || sb.getJobStatus().equalsIgnoreCase("Running")) {
					String status = getJobStatus(sb.getJobId());
					if (status.equalsIgnoreCase("Done")) {
						sb.setJobStatus("Done");
						DateFormat dateFormat = new SimpleDateFormat(
								"yyyy-MM-dd HH:mm:ss.SSS");
						Date today = new Date();
						String sysdate = dateFormat.format(today);
						sb.setJobEndTime(sysdate);
						if (sb.getValidationStatus() != null) {
							if (sb.getValidationStatus().equalsIgnoreCase(
									"NotStarted")) {
								sb = SourcingRulesDAO.getQueueDepth(sb);
								queueDepth = Integer.parseInt(sb.getQueueDepth());
								if (queueDepth == 0){
									try {
									    Thread.sleep(2000);                 //1000 milliseconds is one second.
									} catch(InterruptedException ex) {
									    Thread.currentThread().interrupt();
									}
									sb = SourcingRulesDAO.getReprocessErrorCount (sb);
									reprocessErrorCount = Integer.parseInt(sb.getReprocessErrorCount());
									
									sb = SourcingRulesDAO.getModifiedRecords (sb);
									sterlingTableCheck = Integer.parseInt(sb.getSterlingTableCount());
									
									sb.setSterlingTableCount(""+sterlingTableCheck);
									sb.setReprocessErrorCount(""+reprocessErrorCount);
									if (reprocessErrorCount == 0){
										reprocessCheck = "Completed";
										sb.setValidationStatus(reprocessCheck);
									}
									else {
										reprocessCheck = "Reprocess errors present. Please contact COM Support team.";
										sb.setValidationStatus("Failed");
										sb.setComments(reprocessCheck);
									}
								}
								else {
									try{
										Date today1 = new Date();
										SimpleDateFormat sdf = new SimpleDateFormat(
												"yyyy-MM-dd HH:mm:ss.SSS");
										Date date = sdf.parse(sb.getJobStartTime());
										long jobTimestamp = date.getTime();
										long sysdate1 = today1.getTime();
										if ((sysdate1 - jobTimestamp) > 600000){
											sb.setValidationStatus("Failed");
											sb.setComments("Queue Depth is not decreasing from last 10 mins. Please contact COM Support team.");
										}
										}catch (Exception e){
											logger.debug("Exception Occurred while parsing a date "+e);
											jobDetails.add(sb);
											break;
										}
									sb.setValidationStatus("InProgress");
									jobDetails.add(sb);
									break;
								}
							}
						}
					}
					else if (status.equalsIgnoreCase("Running")){
						sb.setJobStatus("Running");
						try{
						Date today = new Date();
						SimpleDateFormat sdf = new SimpleDateFormat(
								"yyyy-MM-dd HH:mm:ss.SSS");
						Date date = sdf.parse(sb.getJobStartTime());
						long jobTimestamp = date.getTime();
						long sysdate = today.getTime();
						if ((sysdate - jobTimestamp) > 600000){
							sb.setJobStatus("Failed");
							sb.setComments("Job is in running Status for more than 10 mins. Please contact COM Support team.");
						}
						}catch (Exception e){
							logger.debug("Exception Occurred while parsing a date "+e);
							jobDetails.add(sb);
							break;
						}
					}
					else if (status.equalsIgnoreCase("Wait")){
						sb.setJobStatus("Wait");
						try{
							Date today = new Date();
							SimpleDateFormat sdf = new SimpleDateFormat(
									"yyyy-MM-dd HH:mm:ss.SSS");
							Date date = sdf.parse(sb.getJobStartTime());
							long jobTimestamp = date.getTime();
							long sysdate = today.getTime();
							if ((sysdate - jobTimestamp) > 600000){
								sb.setJobStatus("Failed");
								sb.setComments("Job is in wait Status for more than 10 mins. Please contact COM Support team.");
							}
							}catch (Exception e){
								logger.debug("Exception Occurred while parsing a date "+e);
								jobDetails.add(sb);
								break;
							}
					}
					jobDetails.add(sb);
					break;
				}
				
				else if (sb.getJobStatus().equalsIgnoreCase("Done")){
					if (sb.getValidationStatus() != null) {
						if ((sb.getValidationStatus().equalsIgnoreCase("NotStarted")) || (sb.getValidationStatus().equalsIgnoreCase("InProgress"))) {
							sb = SourcingRulesDAO.getQueueDepth(sb);
							queueDepth = Integer.parseInt(sb.getQueueDepth());
							if (queueDepth == 0){
								try {
								    Thread.sleep(2000);                 //1000 milliseconds is one second.
								} catch(InterruptedException ex) {
								    Thread.currentThread().interrupt();
								}
								sb = SourcingRulesDAO.getReprocessErrorCount (sb);
								reprocessErrorCount = Integer.parseInt(sb.getReprocessErrorCount());
								
								sb = SourcingRulesDAO.getModifiedRecords (sb);
								sterlingTableCheck = Integer.parseInt(sb.getSterlingTableCount());
								
								sb.setSterlingTableCount(""+sterlingTableCheck);
								sb.setReprocessErrorCount(""+reprocessErrorCount);
								if (reprocessErrorCount == 0){
									reprocessCheck = "Completed";
									sb.setValidationStatus(reprocessCheck);
								}
								else {
									reprocessCheck = "Reprocess errors present. Please contact COM Support team.";
									sb.setValidationStatus("Failed");
									sb.setComments(reprocessCheck);
								}
							}
							else {
								try{
									Date today = new Date();
									SimpleDateFormat sdf = new SimpleDateFormat(
											"yyyy-MM-dd HH:mm:ss.SSS");
									Date date = sdf.parse(sb.getJobStartTime());
									long jobTimestamp = date.getTime();
									long sysdate = today.getTime();
									if ((sysdate - jobTimestamp) > 600000){
										sb.setValidationStatus("Failed");
										sb.setComments("Queue Depth is not decreasing from last 10 mins. Please contact COM Support team.");
									}
									}catch (Exception e){
										logger.debug("Exception Occurred while parsing a date "+e);
										jobDetails.add(sb);
										break;
									}
								sb.setValidationStatus("InProgress");
								jobDetails.add(sb);
								break;
							}
						}
						else if (sb.getValidationStatus().equalsIgnoreCase("Completed")){
							logger.debug("job status and validation status are success");
						}
						else if (sb.getValidationStatus().equalsIgnoreCase("Failed")){
							logger.debug("validation status is failed");
						}
						jobDetails.add(sb);
					}
				}
			}
		}
		return jobDetails;
	}
	
	public static String getURLOutput(String hitURL) {
		String output = "";
		try {
			URL resumeTrans = new URL(hitURL.trim());
			URLConnection resumeTransConn = resumeTrans.openConnection();
			resumeTransConn.setConnectTimeout(5000);
			BufferedReader in = new BufferedReader(new InputStreamReader(
					resumeTransConn.getInputStream()));
			String inputLine = "";
			while ((inputLine = in.readLine()) != null) {
				if (!(inputLine == null || "".equalsIgnoreCase(inputLine))) {
					output = output + inputLine;
				}
			}
			in.close();
			logger.debug(output);
		} catch (Exception e) {
			logger.debug("Exception occurred " + e);
			e.printStackTrace();
		}
		return output;
	}

	public static String triggerJob(int sNo) {
		String jobId = "";
		String jobURL = "";
		if (sNo == 1) {
			jobURL = "http://webbatch.homedepot.com/COMSourcingSynchronization/SourcingSynchronization?start&initialRun=true&no=1";
		} else if (sNo == 2) {
			jobURL = "http://webbatch.homedepot.com/COMSourcingSynchronization/SourcingSynchronization?start&initialRun=true&no=2";
		} else if (sNo == 3) {
			jobURL = "http://webbatch.homedepot.com/COMSourcingSynchronization/SourcingSynchronization?start&initialRun=true&no=3";
		} else if (sNo == 4) {
			jobURL = "http://webbatch.homedepot.com/COMSourcingSynchronization/SourcingSynchronization?start&initialRun=true&no=4";
		} else if (sNo == 5) {
			jobURL = "http://webbatch.homedepot.com/COMSourcingSynchronization/SourcingSynchronization?start&no=5";
		} else if (sNo == 6) {
			jobURL = "http://webbatch.homedepot.com/COMSourcingSynchronization/SourcingSynchronization?start&no=9";
		}
		jobId = getURLOutput(jobURL);
		jobId = jobId.trim();
		return jobId;
	}

	public static String getJobStatus(String jobId) {
		String status = "";
		boolean check = false;
		String jobURL = "http://webbatch.homedepot.com/COMSourcingSynchronization/SourcingSynchronization?list";
		String jobList = getURLOutput(jobURL);
		DOMParser parser = new DOMParser();
		try {
			parser.parse(new InputSource(new java.io.StringReader(jobList)));
			Document doc = parser.getDocument();
			// String message = doc.getDocumentElement().getTextContent();
			NodeList nodeList = doc.getElementsByTagName("job");
			for (int i = 0; i < nodeList.getLength(); i++) {
				Node node = nodeList.item(i);

				if (node.hasAttributes()) {
					Attr attr = (Attr) node.getAttributes().getNamedItem("id");
					Attr attr1 = (Attr) node.getAttributes().getNamedItem(
							"status");
					if (attr != null) {
						String jobIdFromList = attr.getValue();
						jobIdFromList = jobIdFromList.trim();
//						logger.debug("Job ID: " + jobIdFromList);
						if (jobIdFromList.equalsIgnoreCase(jobId)) {
							if (attr1 != null) {
								status = attr1.getValue();
								logger.debug("Status: " + status);
								check = true;
							}
						}
					}
				}
			}
		} catch (SAXException e) {
			logger.error("Exception Occurred in getJobStatus method " + e);
		} catch (IOException e) {
			logger.error("Exception Occurred in getJobStatus method " + e);
		}
		if (!check) {
			logger.debug("Job id " + jobId + " not found from the list");
		}
		return status;
	}

	public static SourcingBatchDTO getJobDetails(SourcingBatchDTO sb) {
		String status = "";
		String jobId = sb.getJobId();
		boolean check = false;
		String jobURL = "http://webbatch.homedepot.com/COMSourcingSynchronization/SourcingSynchronization?list";
		String jobList = getURLOutput(jobURL);
		DOMParser parser = new DOMParser();
		try {
			parser.parse(new InputSource(new java.io.StringReader(jobList)));
			Document doc = parser.getDocument();
			// String message = doc.getDocumentElement().getTextContent();
			NodeList nodeList = doc.getElementsByTagName("job");
			for (int i = 0; i < nodeList.getLength(); i++) {
				Node node = nodeList.item(i);

				if (node.hasAttributes()) {
					Attr attr = (Attr) node.getAttributes().getNamedItem("id");
					Attr attr1 = (Attr) node.getAttributes().getNamedItem(
							"status");
					Attr attr2 = (Attr) node.getAttributes().getNamedItem(
							"creationTime");
					if (attr != null) {
						String jobIdFromList = attr.getValue();
						jobIdFromList = jobIdFromList.trim();
//						logger.debug("Job ID: " + jobIdFromList);
						if (jobIdFromList.equalsIgnoreCase(jobId)) {
							check = true;
							if (attr1 != null) {
								status = attr1.getValue();
								sb.setJobStatus(status);
								logger.debug("Status: " + status);
							}
							if (attr2 != null) {
								String startTime = attr2.getValue();
								sb.setJobStartTime(startTime);
							}
						}
					}
				}
			}
		} catch (SAXException e) {
			logger.debug("Exception Occurred in getJobDetails method "
					+ e);
		} catch (IOException e) {
			logger.debug("Exception Occurred in getJobDetails method "
					+ e);
		}
		if (!check) {
			logger.debug("Job id " + jobId + " not found from the list");
		}
		return sb;
	}
}
