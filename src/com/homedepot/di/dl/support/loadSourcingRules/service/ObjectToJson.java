package com.homedepot.di.dl.support.loadSourcingRules.service;

import java.util.ArrayList;

import com.homedepot.di.dl.support.loadSourcingRules.dto.SourcingBatchDTO;

public class ObjectToJson {
	 boolean triggerBatch = false;
	 boolean BatchStarted = false;
	 ArrayList<SourcingBatchDTO> jobDetails = new ArrayList<SourcingBatchDTO>();
	public boolean isTriggerBatch() {
		return triggerBatch;
	}
	public void setTriggerBatch(boolean triggerBatch) {
		this.triggerBatch = triggerBatch;
	}
	public boolean isBatchStarted() {
		return BatchStarted;
	}
	public void setBatchStarted(boolean batchStarted) {
		BatchStarted = batchStarted;
	}
	public ArrayList<SourcingBatchDTO> getBatchDetails() {
		return jobDetails;
	}
	public void setBatchDetails(ArrayList<SourcingBatchDTO> batchDetails) {
		this.jobDetails = batchDetails;
	}
	 
	 
	

	
	
}
