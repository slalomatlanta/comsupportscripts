package com.homedepot.di.dl.support.loadSourcingRules.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.xerces.parsers.DOMParser;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.homedepot.di.dl.support.loadSourcingRules.service.CheckSourcingBatchStatus;

/**
 * Servlet implementation class TestParsingXML
 */
public class TestParsingXML extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = Logger
			.getLogger(TestParsingXML.class);
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TestParsingXML() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		response.setContentType("text/html");
		PrintWriter out = null;
		out = response.getWriter();

		String sampleXML = "<jobs><job id=\"2B9A14AC1450194594770\" contextRoot=\"/COMSourcingSynchronization\" servletName=\"SourcingSynchronization\" portNumber=\"13107\" portSecure=\"false\" retryCount=\"0\" creationTime=\"2015-12-15 10:49:54.77\" status=\"DONE\"/>"
				+ "<job id=\"2B9A14AC1450194594771\" contextRoot=\"/COMSourcingSynchronization\" servletName=\"SourcingSynchronization\" portNumber=\"13107\" portSecure=\"false\" retryCount=\"0\" creationTime=\"2015-12-15 10:49:54.77\" status=\"DONE\"/></jobs>";
		DOMParser parser = new DOMParser();
		try {
		    parser.parse(new InputSource(new java.io.StringReader(sampleXML)));
		    Document doc = parser.getDocument();
//		    String message = doc.getDocumentElement().getTextContent();
		    NodeList nodeList = doc.getElementsByTagName("job");
		    for (int i = 0; i < nodeList.getLength(); i++) {
				Node node = nodeList.item(i);

				if (node.hasAttributes()) {
					Attr attr = (Attr) node.getAttributes().getNamedItem(
							"id");
					Attr attr1 = (Attr) node.getAttributes().getNamedItem(
							"status");
					if (attr != null) {
						String attribute = attr.getValue();
						logger.debug("Job ID: " + attribute);
						out.println(attribute);
					}
					if (attr1 != null) {
						String attribute = attr1.getValue();
						logger.debug("Status: " + attribute);
						out.println(attribute);
					}
				}
			}
//		    System.out.println(message);
		} catch (SAXException e) {
		    // handle SAXException 
		} catch (IOException e) {
		    // handle IOException 
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
