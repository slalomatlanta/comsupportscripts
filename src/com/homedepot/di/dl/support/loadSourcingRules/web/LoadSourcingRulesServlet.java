package com.homedepot.di.dl.support.loadSourcingRules.web;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import au.com.bytecode.opencsv.CSVReader;

import com.google.gson.Gson;
import com.homedepot.di.dl.support.ddInstrUpdate.dao.DDInstrUpdateDAO;
import com.homedepot.di.dl.support.loadSourcingRules.dto.SourcingBatchDTO;
import com.homedepot.di.dl.support.loadSourcingRules.service.CheckSourcingBatchStatus;
import com.homedepot.di.dl.support.loadSourcingRules.service.ObjectToJson;

/**
 * Servlet implementation class LoadSourcingRulesServlet
 */
public class LoadSourcingRulesServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = Logger
			.getLogger(LoadSourcingRulesServlet.class);

	/**
	 * Default constructor.
	 */
	public LoadSourcingRulesServlet() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException{
		// TODO Auto-generated method stub
		logger.debug("Inside Servlet");

		ObjectToJson data = new ObjectToJson();
		ArrayList<SourcingBatchDTO> jobDetails = new ArrayList<SourcingBatchDTO>();

		ArrayList<SourcingBatchDTO> inputDetails = new ArrayList<SourcingBatchDTO>();
		boolean triggerBatch = false;
		String parameter = request.getParameter("triggerBatch").trim();
		if (parameter.equalsIgnoreCase("true")) {
			triggerBatch = true;
		}
		DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
		Date today = new Date();
		String sysdate = dateFormat.format(today);
		long sysdateInLongMillis = today.getTime();
//		String filelocation = "C:\\test\\BatchDetails_" + sysdate + ".csv";
//		String tempFilelocation = "C:\\test\\TempBatch_" + sysdateInLongMillis + ".csv";
		
		String filelocation = "/opt/isv/tomcat/temp/BatchDetails_" + sysdate + ".csv";
		String tempFilelocation = "/opt/isv/tomcat/temp/TempBatch_" + sysdateInLongMillis + ".csv";

		if (triggerBatch) {

			File f = new File(filelocation);
			if (f.exists() && !f.isDirectory()) {
				logger.debug("File Exists");
				inputDetails = getDetailsFromCSV(filelocation);
				jobDetails = CheckSourcingBatchStatus
						.getJobDetails(inputDetails);

			} else {
				logger.debug("File does not Exists");
				FileWriter writer = new FileWriter(filelocation);

				writer.append("Serial Number");
				writer.append(",");
				writer.append("Job Name");
				writer.append(",");
				writer.append("Job ID");
				writer.append(",");
				writer.append("Job Status");
				writer.append(",");
				writer.append("Start Time");
				writer.append(",");
				writer.append("End Time");
				writer.append(",");
				writer.append("Reprocess Error Count");
				writer.append(",");
				writer.append("Sterling Table Count");
				writer.append(",");
				writer.append("Validation Status");
				writer.append(",");
				writer.append("Comments");
				writer.append(",");
				writer.append("Logs");
				writer.append(",");
				writer.append("Reprocess Query");
				writer.append(",");
				writer.append("Sterling Query");
				writer.append(",");
				writer.append("Queue Depth");
				writer.append(",");
				writer.append("Queue Name");
				writer.append(",");
				writer.append("\n");

				for (int i = 1; i <= 6; i++) {
					writer.append("" + i);
					writer.append(",");

					if (i == 1) {
						writer.append("Common Code");
					} else if (i == 2) {
						writer.append("Service Skill");
					} else if (i == 3) {
						writer.append("Service Types");
					} else if (i == 4) {
						writer.append("Carrier Types");
					} else if (i == 5) {
						writer.append("Resource Pool");
					} else if (i == 6) {
						writer.append("Node Notification");
					}

					writer.append(",");
					writer.append("NA");
					writer.append(",");
					writer.append("Queued");
					writer.append(",");
					writer.append("NA");
					writer.append(",");
					writer.append("NA");
					writer.append(",");
					writer.append("NA");
					writer.append(",");
					writer.append("NA");
					writer.append(",");
					writer.append("NotStarted");
					writer.append(",");
					writer.append("NA");
					writer.append(",");
					writer.append("NA");
					writer.append(",");
					writer.append("NA");
					writer.append(",");
					writer.append("NA");
					writer.append(",");
					writer.append("NA");
					writer.append(",");
					writer.append("NA");
					writer.append(",");
					writer.append("\n");
				}

				writer.flush();
				writer.close();
				logger.debug("File created at " + filelocation);
				inputDetails = getDetailsFromCSV(filelocation);
				jobDetails = CheckSourcingBatchStatus
						.getJobDetails(inputDetails);
			}
		}

		else {
			File f = new File(filelocation);
			if (f.exists() && !f.isDirectory()) {
				logger.debug("File Exists");
				inputDetails = getDetailsFromCSV(filelocation);
				jobDetails = CheckSourcingBatchStatus
						.getJobDetails(inputDetails);
			}
		}

		if (jobDetails.size() > 0) {
			int emptyRows = (6 - jobDetails.size());
			FileWriter writer = new FileWriter(tempFilelocation);

			writer.append("Serial Number");
			writer.append(",");
			writer.append("Job Name");
			writer.append(",");
			writer.append("Job ID");
			writer.append(",");
			writer.append("Job Status");
			writer.append(",");
			writer.append("Start Time");
			writer.append(",");
			writer.append("End Time");
			writer.append(",");
			writer.append("Reprocess Error Count");
			writer.append(",");
			writer.append("Sterling Table Count");
			writer.append(",");
			writer.append("Validation Status");
			writer.append(",");
			writer.append("Comments");
			writer.append(",");
			writer.append("Logs");
			writer.append(",");
			writer.append("Reprocess Query");
			writer.append(",");
			writer.append("Sterling Query");
			writer.append(",");
			writer.append("Queue Depth");
			writer.append(",");
			writer.append("Queue Name");
			writer.append(",");
			writer.append("\n");
			
			for (SourcingBatchDTO sb : jobDetails) {
				writer.append("" + sb.getSerialNo());
				writer.append(",");
				writer.append(sb.getJobName());
				writer.append(",");
				writer.append(sb.getJobId());
				writer.append(",");
				writer.append(sb.getJobStatus());
				writer.append(",");
				writer.append(sb.getJobStartTime());
				writer.append(",");
				writer.append(sb.getJobEndTime());
				writer.append(",");
				writer.append(sb.getReprocessErrorCount());
				writer.append(",");
				writer.append(sb.getSterlingTableCount());
				writer.append(",");
				writer.append(sb.getValidationStatus());
				writer.append(",");
				writer.append(sb.getComments());
				writer.append(",");
				
				try{
				String logLink = getLogLink(sb);
				sb.setLogFileLink(logLink);
				}catch (Exception e){
					logger.debug("Exception Occurred while creating a log file:"+e);
				}
				
				writer.append(sb.getLogFileLink());
				writer.append(",");
				String updatedReprocessQuery = (sb.getReprocessQuery().replace(',','-'));
				writer.append(updatedReprocessQuery);
				writer.append(",");
				writer.append(sb.getSterlingQuery());
				writer.append(",");
				writer.append(sb.getQueueDepth());
				writer.append(",");
				writer.append(sb.getQueueName());
				writer.append(",");
				writer.append("\n");
			}

			for (int i = (jobDetails.size() + 1); i <= 6; i++) {
				writer.append("" + i);
				writer.append(",");

				if (i == 1) {
					writer.append("Common Code");
				} else if (i == 2) {
					writer.append("Service Skill");
				} else if (i == 3) {
					writer.append("Service Types");
				} else if (i == 4) {
					writer.append("Carrier Types");
				} else if (i == 5) {
					writer.append("Resource Pool");
				} else if (i == 6) {
					writer.append("Node Notification");
				}

				writer.append(",");
				writer.append("NA");
				writer.append(",");
				writer.append("Queued");
				writer.append(",");
				writer.append("NA");
				writer.append(",");
				writer.append("NA");
				writer.append(",");
				writer.append("NA");
				writer.append(",");
				writer.append("NA");
				writer.append(",");
				writer.append("NotStarted");
				writer.append(",");
				writer.append("NA");
				writer.append(",");
				writer.append("NA");
				writer.append(",");
				writer.append("NA");
				writer.append(",");
				writer.append("NA");
				writer.append(",");
				writer.append("NA");
				writer.append(",");
				writer.append("NA");
				writer.append(",");
				writer.append("\n");
			}

			writer.flush();
			writer.close();
			logger.debug("Temporary File created at " + tempFilelocation);
			File f = new File(filelocation);
			if (f.exists() && !f.isDirectory()) {
				logger.debug("Deleting Old File");
				if (f.delete()) {
					File oldFileName = new File(tempFilelocation);
					File newFileName = new File(filelocation);
					try {
						if (oldFileName.renameTo(newFileName)) {
							logger.debug("File renamed successfull !");
							inputDetails = getDetailsFromCSV(filelocation);
							// ObjectToJson finalObject = new ObjectToJson();
							data.setBatchDetails(inputDetails);
							data.setBatchStarted(true);
							data.setTriggerBatch(true);
							// finalObject = ObjectToJson.getFinalObject
							// (inputDetails, true, true);
							Gson gson = new Gson();
							String json = gson.toJson(data);
							logger.debug(json);
							response.setContentType("application/json");
							response.getWriter().write(json.toString());
						} else {
							logger.error("File rename operation failed !");
						}

					} catch (Exception e) {
						e.printStackTrace();
					}
				} else {
					logger.debug("Error in deleting old file");
				}
			}
		} else {
			File f = new File(filelocation);
			if (f.exists() && !f.isDirectory()) {
				logger.debug("File Exists");
			} else {
				logger.debug("Job not Started. No file Created");
				// ObjectToJson finalObject = ObjectToJson.getFinalObject
				// (inputDetails, false, false);
				// finalObject = ObjectToJson.getFinalObject (inputDetails,
				// false, false);
				data.setBatchDetails(inputDetails);
				data.setBatchStarted(false);
				data.setTriggerBatch(false);
				Gson gson = new Gson();
				String json = gson.toJson(data);
				logger.debug(json);
				response.setContentType("application/json");
				response.getWriter().write(json.toString());
			}
		}
	}

	public static ArrayList<SourcingBatchDTO> getDetailsFromCSV(
			String fileLocation) {
		ArrayList<SourcingBatchDTO> inputDetails = new ArrayList<SourcingBatchDTO>();
		String[] row = null;
		CSVReader csvReader = null;
		boolean headerRow = true;
		try {
			csvReader = new CSVReader(new FileReader(fileLocation));
			while ((row = csvReader.readNext()) != null) {
				if (headerRow) {
					headerRow = false;
				} else {
					SourcingBatchDTO sb = new SourcingBatchDTO();
					sb.setSerialNo(Integer.parseInt(row[0].trim()));
					sb.setJobName(row[1].trim());
					sb.setJobId(row[2].trim());
					sb.setJobStatus(row[3].trim());
					sb.setJobStartTime(row[4].trim());
					sb.setJobEndTime(row[5].trim());
					sb.setReprocessErrorCount(row[6].trim());
					sb.setSterlingTableCount(row[7].trim());
					sb.setValidationStatus(row[8].trim());
					sb.setComments(row[9].trim());
					sb.setLogFileLink(row[10].trim());
					sb.setReprocessQuery(row[11].trim());
					sb.setSterlingQuery(row[12].trim());
					sb.setQueueDepth(row[13].trim());
					sb.setQueueName(row[14].trim());
					inputDetails.add(sb);
				}
			}
			csvReader.close();
		} catch (Exception e) {
			logger.debug("Exception Occured " + e);
		}
		return inputDetails;
	}

	public static String getLogLink(SourcingBatchDTO sb) throws Exception {
//		String logFileLocation = "C:/opt/isv/tomcat-6.0/grid/webapps/COMSupportScripts/data/JobLog_" + sb.getSerialNo()
//				+ ".txt";
		String logFileLocation = "/opt/isv/tomcat/webapps/COMSupportScripts/data/JobLog_" + sb.getSerialNo()
				+ ".txt";
		String clientPath = "NA";
		if (((sb.getJobStatus().equalsIgnoreCase("Done")) || (sb.getJobStatus()
				.equalsIgnoreCase("Failed")))
				&& ((sb.getValidationStatus().equalsIgnoreCase("Completed")) || (sb
						.getValidationStatus().equalsIgnoreCase("Failed")))) {
			File f = new File(logFileLocation);
			if (f.exists() && !f.isDirectory()) {
				if (f.delete()) {
					logger.debug("Existing Log File Deleted");
					FileWriter writer = new FileWriter(logFileLocation);
					writer.append("Job Status : " + sb.getJobStatus());
					writer.append("\n");
					writer.append("Monitor the same using below URL");
					writer.append("\n");
					writer.append("http://webbatch.homedepot.com/COMSourcingSynchronization/SourcingSynchronization?list");
					writer.append("\n\n");
					writer.append("Reprocess Error Count : "
							+ sb.getReprocessErrorCount());
					writer.append("\n");
					writer.append("Query used: " + sb.getReprocessQuery());
					writer.append("\n\n");
					writer.append("Sterling Table Count : "
							+ sb.getSterlingTableCount());
					writer.append("\n");
					writer.append("Query used: " + sb.getSterlingQuery());
					writer.append("\n\n");
					writer.append("Sterling Queue Depth: " + sb.getQueueDepth());
					writer.append("\n");
					writer.append("Queue Name: " + sb.getQueueName());
					writer.append("\n");

					writer.flush();
					writer.close();
					logger.info("Log File created at " + logFileLocation);
				}				
			}
			else{
				FileWriter writer = new FileWriter(logFileLocation);
				writer.append("Job Status : " + sb.getJobStatus());
				writer.append("\n");
				writer.append("Monitor the same using below URL");
				writer.append("\n");
				writer.append("http://webbatch.homedepot.com/COMSourcingSynchronization/SourcingSynchronization?list");
				writer.append("\n\n");
				writer.append("Reprocess Error Count : "
						+ sb.getReprocessErrorCount());
				writer.append("\n");
				writer.append("Query used: " + sb.getReprocessQuery()+" --Note: Replace - with COMMA in the query");
				writer.append("\n\n");
				writer.append("Sterling Table Count : "
						+ sb.getSterlingTableCount());
				writer.append("\n");
				writer.append("Query used: " + sb.getSterlingQuery());
				writer.append("\n\n");
				writer.append("Sterling Queue Depth: " + sb.getQueueDepth());
				writer.append("\n");
				writer.append("Queue Name: " + sb.getQueueName());
				writer.append("\n");
				writer.append("Comments: " + sb.getComments());
				writer.append("\n");

				writer.flush();
				writer.close();
				logger.debug("Log File created at " + logFileLocation);
			}
		}
		else{
			logger.debug("No log file generated as Job is incomplete");
			logFileLocation = logFileLocation;
		}
		int lastIndex = logFileLocation.lastIndexOf('/');
		clientPath = "/COMSupportScripts/data/"+logFileLocation.substring(lastIndex+1);
		logger.debug("Client path is "+clientPath);
		return clientPath;
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
