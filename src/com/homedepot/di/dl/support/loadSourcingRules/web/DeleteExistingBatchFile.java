package com.homedepot.di.dl.support.loadSourcingRules.web;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.homedepot.di.dl.support.loadSourcingRules.service.CheckSourcingBatchStatus;

/**
 * Servlet implementation class DeleteExistingBatchFile
 */
public class DeleteExistingBatchFile extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = Logger
			.getLogger(DeleteExistingBatchFile.class);
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeleteExistingBatchFile() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
		Date today = new Date();
		String sysdate = dateFormat.format(today);
		String filelocation = "/opt/isv/tomcat/temp/BatchDetails_" + sysdate + ".csv";
		String tempFilelocation = "/opt/isv/tomcat/temp/TempBatch_" + sysdate + ".csv";
		
//		String filelocation = "C:\\test\\BatchDetails_" + sysdate + ".csv";
//		String tempFilelocation = "C:\\test\\TempBatch_" + sysdate + ".csv";
		
		File f = new File(filelocation);
		if (f.exists() && !f.isDirectory()) {
			logger.debug("File Exists");
			if (f.delete()){
				logger.debug(filelocation+" deleted successfully");
			}
			else{
				logger.debug(filelocation+" deletion failed");
			}
		}
		else{
			logger.debug(filelocation+" does not exists");
		}
		
		File f1 = new File(tempFilelocation);
		if (f1.exists() && !f1.isDirectory()) {
			logger.debug("File Exists");
			if (f1.delete()){
				logger.debug(tempFilelocation+" deleted successfully");
			}
			else{
				logger.debug(tempFilelocation+" deletion failed");
			}
		}
		else{
			logger.debug(tempFilelocation+" does not exists");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
