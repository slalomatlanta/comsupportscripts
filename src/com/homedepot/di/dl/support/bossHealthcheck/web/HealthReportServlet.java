package com.homedepot.di.dl.support.bossHealthcheck.web;


import java.io.IOException;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Properties;

//import javax.jms.Session;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage.RecipientType;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.homedepot.di.dl.support.ErrorDeltas.web.DeltaServlet;
import com.homedepot.di.dl.support.bossHealthcheck.dao.QueueDAO;
import com.homedepot.di.dl.support.bossHealthcheck.dao.Report;

/**
 * Servlet implementation class HealthReportServlet
 */
public class HealthReportServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	final static Logger logger = Logger.getLogger(HealthReportServlet.class);
    /**
     * @see HttpServlet#HttpServlet()
     */
    public HealthReportServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		logger.info("BOSS/STH PO Create and EJ Updates Started : "+ GregorianCalendar.getInstance().getTime());
		String message = "";
		String poAndej ="";
		message = message
				+ "<!DOCTYPE html>"
				+ "<html>"
				+ "<body>"
				+"<p>Hi Team,</p>"
				+"<p>Please find below health check stats on (BOSS/STH) PO Create and EJ Updates.</p>"
				+"<br/>";
				
		
		Report rp =new Report();
		QueueDAO queue= new QueueDAO();
		String msgContent = queue.getQueueDetails();
		try {
			poAndej = rp.getReport();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.debug("BOSS/STH PO Create and EJ Updates Exception : "+ GregorianCalendar.getInstance().getTime() + e);
		}
		
		message = message + poAndej + msgContent + "</body></html><br/><br/>Thanks,<br/>COM Multichannel Support";
		
		sendReport(message);
		
		logger.info("BOSS/STH PO Create and EJ Updates Completed : "+ GregorianCalendar.getInstance().getTime());
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}
	
	public static void sendReport(String msgContent) {
		final String user = "horizon@cpliisad.homedepot.com";// change
																// accordingly
		final String password = "xxx";// change accordingly

		String host = "mail1.homedepot.com";
		Properties props = new Properties();
		props.put("mail.host", host);
		Session session = Session.getInstance(props,
				new javax.mail.Authenticator() {
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(user, password);
					}
				});

		try {

			String[] to = {"_2fc77b@homedepot.com"};
			//String[] to = {"anandhakumar_chinnadurai@homedepot.com" };
			//String[] Cc = {"venkataganesh_kona@homedepot.com"};
			//String[] Cc = {""};
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress(user));
			DateFormat dateFormat = new SimpleDateFormat(
					"MM/dd/yyyy hh:mm:ss a");
			// get current date time with Date()
			Date today = new Date();
			String date1 = dateFormat.format(today);
			logger.info(date1);
			message.setSubject("BOSS/STH PO and EJ Health Check Report - " + date1);
			// message.setText("This is simple program of sending email using JavaMail API");
			message.setFrom(new InternetAddress(user));
			InternetAddress[] addressTo = new InternetAddress[to.length];
			//InternetAddress[] addressCc = new InternetAddress[Cc.length];
			for (int i = 0; i < to.length; i++) {
				addressTo[i] = new InternetAddress(to[i]);
				logger.debug("To Address " + to[i]);
			}
//			for (int j = 0; j < Cc.length; j++) {
//				addressCc[j] = new InternetAddress(Cc[j]);
//				System.out.println("Cc Address " + Cc[j]);
//			}
			message.setRecipients(RecipientType.TO, addressTo);
			//message.setRecipients(RecipientType.CC, addressCc);
			message.setContent(msgContent, "text/html");
			Transport.send(message);
			for (int i = 0; i < to.length; i++) {
				addressTo[i] = new InternetAddress(to[i]);
				logger.debug("Message delivered to--- " + to[i]);
			}
//			for (int j = 0; j < Cc.length; j++) {
//				addressCc[j] = new InternetAddress(Cc[j]);
//				System.out.println("Message delivered to cc recepients--- "
//						+ Cc[j]);
//			}

		} catch (MessagingException e) {
			e.printStackTrace();
		}

	}
	

}
