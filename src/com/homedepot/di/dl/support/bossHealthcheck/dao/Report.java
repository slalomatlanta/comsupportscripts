package com.homedepot.di.dl.support.bossHealthcheck.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.log4j.Logger;


import com.homedepot.di.dl.support.util.*;



public class Report {
	
	final static Logger logger = Logger.getLogger(QueueDAO.class);
	/**
	 * @param args
	 * 
	 * 
	 */
	
	ConstantsQuery constants = new ConstantsQuery();
		public String getReport() throws SQLException
		{
			//System.out.println("main");
			Connection sscConn = null;
			try
			{
				DBConnectionUtil dbConn = new DBConnectionUtil();
				sscConn = dbConn.getJNDIConnection(Constants.STERLING_SSC_RO_JNDI);
	            
			}
			catch(Exception e)
			{
				logger.debug("Exception : "+e.getMessage());
			}
			String latestBossSth =""
					
					+ getLatestPOandEJ(sscConn)
					+ getHourlyBOSSPO(sscConn)
					+ getHourlySTHPO(sscConn)
					+ getHourlyBOSSEJ(sscConn)
					+ getHourlySTHEJ(sscConn)
					+ getBOSSexception(sscConn)
					+ getSTHexception(sscConn);
			//System.out.println(latestBossSth);
			sscConn.close();
			return latestBossSth;
		}
		
		public String getLatestPOandEJ(Connection con)
		
		{
			
			String latestBossSth="<table border=\"1\" cellpadding=\"2\" cellspacing=\"2\" width=\"500px\" style=\"border-collapse:collapse;\">"
					+ "<tr><th>				</th><th>Order Number</th><th>Time stamp</tr>";
			try
			{
				
	            Statement stmt = null;
	            stmt = con.createStatement();
	            ResultSet result = null;
	            
	            result = stmt.executeQuery(constants.bossPO);
	            if (result.next())
	            {
	            	
	            	latestBossSth = latestBossSth + "<tr><td><b>Latest BOSS PO Create</b></td><td>"+result.getString(1)+"</td>"
	            				+ "<td>"+result.getString(3)+"</td></tr>";
	            	}
	            
	            
	            result = stmt.executeQuery(constants.sthPO);
	            if (result.next())
	            {
	            	
	            	latestBossSth = latestBossSth + "<tr><td><b>Latest STH PO Create</b></td><td>"+result.getString(1)+"</td>"
	            				+ "<td>"+result.getString(3)+"</td></tr>";
	            	}
	            
	            
	            result = stmt.executeQuery(constants.bossEJ);
	            if (result.next())
	            {
	            	
	            	latestBossSth = latestBossSth + "<tr><td><b>Latest BOSS EJ Update</b></td><td>"+result.getString(1)+"</td>"
	            				+ "<td>"+result.getString(3)+"</td></tr>";
	            	}
	            
	            
	            result = stmt.executeQuery(constants.sthEJ);
	            if (result.next())
	            {
	            	
	            	latestBossSth = latestBossSth + "<tr><td><b>Latest STH EJ Update</b></td><td>"+result.getString(1)+"</td>"
	            				+ "<td>"+result.getString(3)+"</td></tr>";
	            	}
	            
	            latestBossSth = latestBossSth + "</table>";
	            
	           //System.out.println(latestBossSth);
	            result.close();
				stmt.close();
				
			}
			
			catch(Exception e)
			{
				logger.debug("Exception : "+e.getMessage());
			}
			
			return latestBossSth;
			
		}
		
		
		public  String getHourlyBOSSPO(Connection con) 
		{
			
			String latestBossSth="<p><br/><b>BOSS PO creates hourly counts</b></p>"
					+"<table border=\"1\" cellpadding=\"2\" cellspacing=\"2\" width=\"300px\" style=\"border-collapse:collapse;\">";
			try
			{
				
	            Statement stmt = null;
	            stmt = con.createStatement();
	            ResultSet result = null;
	            
	            result = stmt.executeQuery(constants.hourlyBOSSPO);
	            while (result.next())
	            {
	            	
	            	latestBossSth = latestBossSth + "<tr><td>"+result.getString(1)+"</td>"
	            				+ "<td>"+result.getString(2)+"</td></tr>";
	            }
	            
	            latestBossSth = latestBossSth + "</table>";
	            
	            //System.out.println(latestBossSth);
	            result.close();
				stmt.close();
	            
			}
			catch(Exception e)
			{
				logger.debug("Exception : "+e.getMessage());
			}
			
			
			return latestBossSth;
			
		}
		
		
		public  String getHourlySTHPO(Connection con) 
		{
			
			String latestBossSth="<p><br/><b>STH PO creates hourly counts</b></p>"
					+"<table border=\"1\" cellpadding=\"2\" cellspacing=\"2\" width=\"300px\" style=\"border-collapse:collapse;\">";
			try
			{
				
	            Statement stmt = null;
	            stmt = con.createStatement();
	            ResultSet result = null;
	            
	            result = stmt.executeQuery(constants.hourlySTHPO);
	            
	            while (result.next())
	            {
	            	
	            	latestBossSth = latestBossSth + "<tr><td>"+result.getString(1)+"</td>"
	            				+ "<td>"+result.getString(2)+"</td></tr>";
	            }
	            
	            latestBossSth = latestBossSth + "</table>";
	            
	            //System.out.println(latestBossSth);
	            result.close();
				stmt.close();
	            
			}
			catch(Exception e)
			{
				logger.debug("Exception : "+e.getMessage());
			}
			
			
			return latestBossSth;
			
		}
		
		
		public  String getHourlyBOSSEJ(Connection con) 
		{
			
			String latestBossSth="<p><br/><b>BOSS EJ updates hourly counts</b></p><br/>"
					+"<table border=\"1\" cellpadding=\"2\" cellspacing=\"2\" width=\"300px\" style=\"border-collapse:collapse;\">";
			try
			{
				
	            Statement stmt = null;
	            stmt = con.createStatement();
	            ResultSet result = null;
	            
	            result = stmt.executeQuery(constants.hourlyBOSSej);
	            while (result.next())
	            {
	            	
	            	latestBossSth = latestBossSth + "<tr><td>"+result.getString(1)+"</td>"
	            				+ "<td>"+result.getString(2)+"</td></tr>";
	            }
	            
	            latestBossSth = latestBossSth + "</table>";
	            
	            //System.out.println(latestBossSth);
	            result.close();
				stmt.close();
	            
			}
			catch(Exception e)
			{
				logger.debug("Exception : "+e.getMessage());
			}
			
			
			return latestBossSth;
			
		}
		
		
		public  String getHourlySTHEJ(Connection con) 
		{
			
			String latestBossSth="<p><br/><b>STH EJ updates hourly counts</b></p><br/>"
					+"<table border=\"1\" cellpadding=\"2\" cellspacing=\"2\" width=\"300px\" style=\"border-collapse:collapse;\">";
			try
			{
				
	            Statement stmt = null;
	            stmt = con.createStatement();
	            ResultSet result = null;
	            
	            result = stmt.executeQuery(constants.hourlySTHej);
	            while (result.next())
	            {
	            	
	            	latestBossSth = latestBossSth + "<tr><td>"+result.getString(1)+"</td>"
	            				+ "<td>"+result.getString(2)+"</td></tr>";
	            }
	            
	            latestBossSth = latestBossSth + "</table>";
	            
	            //System.out.println(latestBossSth);
	            result.close();
				stmt.close();
	            
			}
			catch(Exception e)
			{
				logger.debug("Exception : "+e.getMessage());
			}
			
			
			return latestBossSth;
			
		}
		
		public  String getBOSSexception(Connection con) 
		{
			
			String latestBossSth="<p><br/><b>BOSS Exception Counts"
					+ "</b></p><br/>"
					+"<table border=\"1\" cellpadding=\"2\" cellspacing=\"2\" width=\"500px\" style=\"border-collapse:collapse;\">"
					+ "<tr><th>Flow Name</th><th>Error Code</th><th width=\"1000px\">Error String</th><th>Count</th>";
			try
			{
				
	            Statement stmt = null;
	            stmt = con.createStatement();
	            ResultSet result = null;
	            
	            result = stmt.executeQuery(constants.bossExc);
	            while (result.next())
	            {
	            	
	            	latestBossSth = latestBossSth + "<tr><td>"+result.getString(1)+"</td>"
	            				+ "<td>"+result.getString(2)+"</td>" 
	            				+ "<td>"+result.getString(3)+"</td>" 
	            				+ "<td>"+result.getString(4)+"</td></tr>";
	            }
	            
	            latestBossSth = latestBossSth + "</table>";
	            
	            //System.out.println(latestBossSth);
	            result.close();
				stmt.close();
	            
			}
			catch(Exception e)
			{
				logger.debug("Exception : "+e.getMessage());
			}
			
			
			return latestBossSth;
			
		}
		
		
		public  String getSTHexception(Connection con) 
		{
			
			String latestBossSth="<p><br/><b>STH Exception Counts"
					+ "</b></p><br/>"
					+"<table border=\"1\" cellpadding=\"2\" cellspacing=\"2\" width=\"500px\" style=\"border-collapse:collapse;\">"
					+ "<tr><th>Flow Name</th><th>Error Code</th><th width=\"1000px\">Error String</th><th>Count</th>";
			try
			{
				
	            Statement stmt = null;
	            stmt = con.createStatement();
	            ResultSet result = null;
	            
	            result = stmt.executeQuery(constants.sthExc);
	            while (result.next())
	            {
	            	
	            	latestBossSth = latestBossSth + "<tr><td>"+result.getString(1)+"</td>"
	            				+ "<td>"+result.getString(2)+"</td>" 
	            				+ "<td>"+result.getString(3)+"</td>" 
	            				+ "<td>"+result.getString(4)+"</td></tr>";
	            }
	            
	            latestBossSth = latestBossSth + "</table>";
	            
	            //System.out.println(latestBossSth);
	            result.close();
				stmt.close();
	            
			}
			catch(Exception e)
			{
				logger.debug("Exception : "+e.getMessage());
			}
			
			
			return latestBossSth;
			
		}
	
		
		
}
