package com.homedepot.di.dl.support.updateForJIRA.dao;

import java.io.File;
import java.io.FileWriter;
import java.net.URL;
import java.net.URLClassLoader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.Properties;
import java.util.ResourceBundle;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeMessage.RecipientType;

import com.homedepot.di.dl.support.util.Constants;
import com.homedepot.di.dl.support.util.DBConnectionUtil;
import org.apache.log4j.Logger;
public class UpdateJIRA2877 {
	
	final static Logger logger = Logger.getLogger(UpdateJIRA2877.class);
	/**
	 * @param args
	 */
	
			public static void main(String[] args) throws Exception {
				//System.out.println("Main");
				UpdateJIRA2877Fix();
			}
				
				public static  void UpdateJIRA2877Fix() throws 
				Exception {
					 
					 
			StringBuilder emailText = new StringBuilder();
			int total1=0;
			int UpdateQuery1=0;
				try {
			
			//String app="";
				//FileWriter writer = new FileWriter("C:\\store\\JIRA2877.csv");
				FileWriter writer = new FileWriter("/opt/isv/tomcat-6.0.18/temp/JIRA2877.csv");

				
				emailText.append(System.getProperty("line.separator"));
				
				Calendar cal = Calendar.getInstance();
				
				DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
				String appToDate = dateFormat.format(cal.getTime());
				logger.debug(appToDate);

				// String inputFile = "/opt/isv/tomcat-6.0.18/temp/Stores.csv"
				DBConnectionUtil dbConn = new DBConnectionUtil();
				
				Connection sscConn = dbConn.getJNDIConnection(Constants.STERLING_SSC_RO_JNDI);
				
//				Class.forName(driverClass);
//				 sterCon = DriverManager.getConnection(connectionURL, uName,
//						uPassword);
				
				writer.append("Order_Header_key");
				writer.append(',');
				writer.append("poh.Extn_PO_number");
				writer.append(',');
				writer.append("poh.Next_alert_TS");
				writer.append(',');
				writer.append("poh.OrderNo");
				writer.append(',');
				writer.append("Pol.Req_ship_date");
				writer.append(',');
				writer.append("st.status_name");
				writer.append(',');
				writer.append("poh.extn_svs_order_status");
				writer.append(',');
				writer.append("poh.extn_svs_order_status_desc");
				writer.append(',');
				writer.append("poh.extn_profile_id");
				writer.append(',');
				writer.append("poh.Extn_Tran_Mech_code");
				writer.append(',');
				writer.append("poh.Extn_Tran_Status_code");
				writer.append(',');
				writer.append("poh.Extn_Tran_Type_code");
				writer.append(',');
				writer.append("poh.modifyts");
				writer.append(',');
				writer.append("poh.modifyuserid");
				writer.append(',');
				writer.append("poh.*");				
				
				writer.append('\n');
				
				Statement st = null;
				st = sscConn.createStatement();
				ResultSet result = null;
				
				String OrderHeaderKey=null;
				String Query = "select distinct poh.order_header_key,poh.Extn_Po_Number, poh.Next_Alert_TS, poh.extn_host_order_ref, pol.Req_ship_date, " 
							+ " st.status_name, poh.extn_svs_order_status, poh.extn_svs_order_status_desc,poh.extn_profile_id, "
							+ " poh.EXTN_TRANSMISSION_MECH_CODE, poh.EXTN_TRANSMISSION_STATUS_CODE, poh.EXTN_TRANSMISSION_TYPE_CODE,poh.modifyts,poh.modifyuserid,poh.* "
							+ " FROM thd01.YFS_ORDER_HEADER oh, thd01.YFS_ORDER_HEADER poh, thd01.yfs_order_line pol, thd01.yfs_order_release_status pors, thd01.yfs_status st "
							+ " WHERE oh.document_type='0001' "
							+ " and poh.document_type='0005'  "
							+ " and poh.order_header_key > '20151202' "
							+ " and poh.draft_order_flag = 'N' "
							+ " and oh.extn_host_order_ref = poh.extn_host_order_ref "
							+ " and poh.Extn_Transmission_Mech_code='20' "
							+ " and poh.Extn_Transmission_Status_code in ('10','20','40') "
							+ " and pol.order_header_key=poh.order_header_key "
							+ " and pol.order_line_key = pors.order_line_key "
							+ " and pors.status_quantity > 0 "
							+ " and pors.status=st.status "
							+ " and pors.status='1200.150' "
							+ " and st.process_type_key='PO_FULFILLMENT' "
							+ " order by poh.ORDER_HEADER_KEY " ;
				
				
				logger.debug(Query);
				result = st.executeQuery(Query);

				

				
				int totalRowsupdated=0;
				while (result.next()) {
					//System.out.println("OrderHeaderKey:"+result.getString(1));
					//String app1=Integer.toString(i);
					
					
					writer.append("'"+result.getString(1));
					OrderHeaderKey=result.getString(1);
					writer.append(',');
					writer.append(result.getString(2));
					writer.append(',');
					writer.append("'"+result.getString(3));
					writer.append(',');
					writer.append(result.getString(4));
					writer.append(',');
					writer.append(result.getString(5));					
					writer.append(',');
					writer.append(result.getString(6));
					writer.append(',');
					writer.append(result.getString(7));
					writer.append(',');
					writer.append(result.getString(8));
					writer.append(',');
					writer.append(result.getString(9));					
					writer.append(',');
					writer.append(result.getString(10));
					writer.append(',');
					writer.append(result.getString(11));
					writer.append(',');
					writer.append(result.getString(12));
					writer.append(',');
					writer.append("'" +result.getString(13));
					writer.append(',');
					writer.append(result.getString(14));
					writer.append(',');
					writer.append("'" + result.getString(15));					
					writer.append(',');
					writer.append('\n');
					
					
					try
					{
					
						Connection atcConn = dbConn.getJNDIConnection(Constants.STERLING_ATC_RW_JNDI);
		                
		               
		                
		                final String UpdateQuery = "update YFS_ORDER_HEADER set Next_Alert_Ts=sysdate,EXTN_SUPRESS_TRANSMISSION='N',modifyts=sysdate,modifyuserid='JIRA2877FIX' " 
		                							+ " where order_header_key ='"
		                                                                     + OrderHeaderKey
		                                                                     + "'";
		                                                         

		                //Connection con1 = null;

		              //  con1 = DriverManager.getConnection(connectionURL1, uName1,
		                        //     uPassword1);
		                Statement stmt5 = null;
		                stmt5 = atcConn.createStatement();
		                logger.debug(UpdateQuery);
		                 UpdateQuery1 = stmt5.executeUpdate(UpdateQuery);
		                total1 = total1 + UpdateQuery1;
		                atcConn.commit();
		                stmt5.close();
		                atcConn.close();
		               logger.info(UpdateQuery + " Rows Updated");

					} catch (Exception e) {
		                // TODO Auto-generated catch block
		                e.printStackTrace();
		                
		         }
					
					
		         
		                      }
				
				//System.out.println(("Selected"));
				result.close();
				st.close();
				sscConn.close();
				writer.flush();
				writer.close();
				}
				catch (Exception e) {
		            // TODO Auto-generated catch block
		            e.printStackTrace();
		            
		            
		            
		     } 
				sendAttachment(total1);
		                      
				 }
				

					
			

			public static void sendAttachment(int rowsUpdated) throws Exception{String host = "mail1.homedepot.com";
			  String from = "horizon@cpliisad.homedepot.com";
			  String[] to = {"_2fc77b@homedepot.com"};
			  String[] Cc ={"_275769@homedepot.com"};
			 // String[] to = {"Anandhakumar_Chinnadurai@homedepot.com"};
			//  String[] Cc ={"mohanraj_gurusamy@homedepot.com"};
			  // Get system properties
			  Properties properties = System.getProperties();

			  // Setup mail server
			  properties.setProperty("mail.smtp.host", host);

			  // Get the default Session object.
			  Session session = Session.getDefaultInstance(properties);

			  // Define message
			  Message message = new MimeMessage(session);
			  message.setFrom(new InternetAddress(from));
			  InternetAddress[] addressTo = new InternetAddress[to.length];
			  InternetAddress[] addressCc = new InternetAddress[Cc.length];
			  for (int i = 0; i < to.length; i++) 
			  { addressTo[i] = new InternetAddress(to[i]); 
			  logger.debug("To Address "+to[i]);
			  } 
			  for (int j = 0; j < Cc.length; j++) 
			  { addressCc[j] = new InternetAddress(Cc[j]); 
			  logger.debug("Cc Address "+Cc[j]);
			  }
			  message.setRecipients(RecipientType.TO, addressTo); 
			  message.setRecipients(RecipientType.CC, addressCc); 
			  message.setSubject("Update required for JIRA2877");

			  // Create the message part 
			  BodyPart messageBodyPart = new MimeBodyPart();
			  String msgContent = "Hi All,<br/><br/>PFA report - Update required for JIRA2877.<br/>Number of rows Updated: "+rowsUpdated+"<br/><br/>Thanks<br/>";
		      msgContent = msgContent + "COM Multichannel Support";
			  // Fill the message
			  messageBodyPart.setContent(msgContent ,"text/html");
			  	

			  Multipart multipart = new MimeMultipart();
			  multipart.addBodyPart(messageBodyPart);

			  // Part two is attachment
			  messageBodyPart = new MimeBodyPart();
			 // String filename = "C:\\store\\JIRA2877.csv";
			  String filename = "/opt/isv/tomcat-6.0.18/temp/JIRA2877.csv";
			  DataSource source = new FileDataSource(filename);
			  messageBodyPart.setDataHandler(new DataHandler(source));
			  messageBodyPart.setFileName(source.getName());
			  multipart.addBodyPart(messageBodyPart);

			  // Put parts in message
			  message.setContent(multipart);

			  // Send the message
			  Transport.send(message);
			  logger.info("Msg Send ...."); }

				


	

}

