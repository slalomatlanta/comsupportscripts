package com.homedepot.di.dl.support.updateForJIRA.web;


import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.homedepot.di.dl.support.updateForJIRA.dao.UpdateJIRA2877;

import org.apache.log4j.Logger;


/**
 * Servlet implementation class UpdateForJIRA2877Fix
 */
public class UpdateForJIRA2877Fix extends HttpServlet {
	private static final long serialVersionUID = 1L;

	final static Logger logger = Logger.getLogger(UpdateForJIRA2877Fix.class);
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateForJIRA2877Fix() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		
		UpdateJIRA2877 ikey=new UpdateJIRA2877();
		try {
			UpdateJIRA2877.UpdateJIRA2877Fix();
		} catch (SQLException e) {
			logger.error("JIRA ::"+e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			logger.error("JIRA ::"+e.getMessage());
			e.printStackTrace();
		}
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}

