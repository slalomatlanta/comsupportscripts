package com.homedepot.di.dl.support.WorkOrderApptKeyUpdate.web;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.homedepot.di.dl.support.WorkOrderApptKeyUpdate.dao.*;
import org.apache.log4j.Logger;
/**
 * Servlet implementation class WorkOrderApptKeyUpdateServlet
 */
public class WorkOrderApptKeyUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	final static Logger logger = Logger.getLogger(WorkOrderApptKeyUpdateServlet.class);
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public WorkOrderApptKeyUpdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		WorkOrderApptKeyUpdate apptkeyUpdate =new WorkOrderApptKeyUpdate();
		try {
			apptkeyUpdate.updateWorkOrderApptKey();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		logger.info("WorkOrderApptKey successfully Updated");
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
