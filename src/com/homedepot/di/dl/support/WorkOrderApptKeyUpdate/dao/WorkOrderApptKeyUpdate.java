package com.homedepot.di.dl.support.WorkOrderApptKeyUpdate.dao;

import java.io.FileWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeMessage.RecipientType;

import com.homedepot.di.dl.support.util.Constants;
import com.homedepot.di.dl.support.util.DBConnectionUtil;
import org.apache.log4j.Logger;


public class WorkOrderApptKeyUpdate 
{	

	final static Logger logger = Logger.getLogger(WorkOrderApptKeyUpdate.class);
	//public static void main(String args[])
	public void updateWorkOrderApptKey() throws Exception
	{
	int rowsUpdated=0;
	int totalRowsupdated=0;
	try
	{
		
		//FileWriter writer = new FileWriter("C:\\store\\WorkOrderApptKey update.csv");
		FileWriter writer = new FileWriter("/opt/isv/tomcat-6.0.18/temp/WorkOrderApptKey update.csv");
	
		writer.append("HOST_ORDER_REF");
		writer.append(',');
		writer.append("ORDER_HEADER_KEY");
		writer.append(',');
		writer.append("Shipment_No");
		writer.append(',');
		writer.append("Work_order_appt_key");
		writer.append(',');
		writer.append("Work_order_key");
		writer.append(',');
		writer.append('\n');
		
	DBConnectionUtil dbConn = new DBConnectionUtil();
	Connection sscConn = dbConn.getJNDIConnection(Constants.STERLING_SSC_RO_JNDI);
	
	Statement st = null;
	st = sscConn.createStatement();
	ResultSet result = null;
	
	String query= "With RWO As (select wopd.order_header_key order_header_key, wopd.order_line_key order_line_key, wo.host_order_ref host_order_ref, "
			+" wopd.host_order_line_ref host_order_line_ref, sum(wopd.Loaded_qty) Loaded_qty "
			+" from thd01.HD_RELATED_WO_PROD_DEL wopd, thd01.HD_RELATED_WORK_ORDER wo"
			+" where wo.related_work_order_key = wopd.related_work_order_key"
			+" and wo.status = '1100.675'"
			+" group by wopd.order_header_key, wopd.order_line_key, wo.host_order_ref, wopd.host_order_line_ref"
			+" union"
			+" select wopd.order_header_key order_header_key, wopd.order_line_key order_line_key, wo.extn_host_order_ref host_order_ref, "
			+" wopd.extn_host_order_line_ref host_order_line_ref, sum(wopd.extn_Loaded_qty) Loaded_qty "
			+" from thd01.YFS_WORK_ORDER_PROD_DEL wopd, thd01.YFS_WORK_ORDER wo"
			+" where wo.work_order_key = wopd.work_order_key"
			+" and wo.status = '1100.675'"
			+" and Extn_has_related_wo_flag='N'"
			+" group by wopd.order_header_key, wopd.order_line_key, wo.extn_host_order_ref, wopd.extn_host_order_line_ref),"
			+" ORS_CS As"
			+" (Select rs.Order_header_key, rs.Order_Line_Key, sum(rs.status_quantity) ConfirmedShipmentQty"
			+" from thd01.yfs_order_release_status rs, RWO"
			+" where rs.order_header_key = RWO.order_header_key"
			+" and rs.order_line_key = RWO.order_line_key"
			+" and rs.status = '3350.900'"
			+" group by rs.Order_header_key, rs.Order_Line_Key)"
			+" Select RWO.host_order_ref, RWO.order_header_key, trim(sh.shipment_no), sh.WORK_ORDER_APPT_KEY, sh.WOrk_Order_Key"
			+" from RWO,  ORS_CS, thd01.yfs_order_line ol, thd01.yfs_shipment sh, thd01.yfs_shipment_line shl"
			+" where RWO.order_header_key = ol.order_header_key"
			+" and RWO.order_line_key = ol.order_line_key"
			+" and ORS_CS.order_header_key(+) = ol.order_header_key"
			+" and ORS_CS.order_line_key(+) = ol.order_line_key"
			+" and RWO.Loaded_Qty=ORS_CS.ConfirmedShipmentQty"
			+" and shl.order_header_key = RWO.order_header_key"
			+" and shl.order_line_key = RWO.order_line_key"
			+" and sh.shipment_key = shl.shipment_key"
			+" and sh.WORK_ORDER_APPT_KEY is null"
			+" order by RWO.order_header_key,RWO.host_order_line_ref ";
	
	logger.info("Selelct query: "+ query);
	
	result = st.executeQuery(query);
	
	StringBuilder queryBuilder = new StringBuilder();
	
	queryBuilder.append("(");
	
	while (result.next()) 
	{
		String shipmentNo = result.getString(3).trim();
		String orderNo = result.getString(1).trim();
		queryBuilder.append("('");
		queryBuilder.append(shipmentNo);
		queryBuilder.append("','");
		queryBuilder.append(orderNo);
		queryBuilder.append("'),");
		
		writer.append("'"+result.getString(1));
		writer.append(',');
		writer.append("'"+result.getString(2));
		writer.append(',');
		writer.append("'"+result.getString(3));
		writer.append(',');
		writer.append("'"+result.getString(4));
		writer.append(',');
		writer.append("'"+result.getString(5));
		writer.append(',');
		writer.append('\n');
	}
	
	String shipmentQuery = "select ywo.extn_host_order_ref, ys.shipment_key, max(ywo.WORK_ORDER_KEY), max(ywoa.WORK_ORDER_APPT_KEY),"
			+" 'update thd01.yfs_shipment set WORK_ORDER_KEY='''||max(ywo.WORK_ORDER_KEY)||''',WORK_ORDER_APPT_KEY='''||max(ywoa.WORK_ORDER_APPT_KEY)||''' ,"
			+" MODIFYTS=sysdate,MODIFYUSERID =''COMSupport'' where SHIPMENT_KEY='''||ys.shipment_key||''''"
			+" From thd01.yfs_work_order ywo, THD01.YFS_SHIPMENT ys, THD01.YFS_WORK_ORDER_APPT ywoa"
			+" where (ys.SHIPMENT_NO, ywo.extn_host_order_ref) in "
			+queryBuilder.substring(0, (queryBuilder.length()-1))
			+")"
			+" and ywoa.work_order_key = ywo.WORK_ORDER_KEY"
			+" group by ywo.extn_host_order_ref, ys.shipment_key"; 
	
	logger.info("shipment query : "+shipmentQuery);
	
	result = st.executeQuery(shipmentQuery);
	
	while(result.next())
	{
	String updateQuery = result.getString(5); 
    logger.debug(updateQuery);
    
    
	Connection atcConn = dbConn.getJNDIConnection(Constants.STERLING_ATC_RW_JNDI);
	
	Statement st1 = null;
	st1 = atcConn.createStatement();
	
	rowsUpdated = st1.executeUpdate(updateQuery);
	totalRowsupdated = totalRowsupdated + rowsUpdated;
    atcConn.commit();
	st1.close();
	atcConn.close();
	
	}
	result.close();
	st.close();
	sscConn.close();
    
	writer.close();
	}
	
	catch(Exception e)
	{
		e.printStackTrace();
	}
	sendAttachment(totalRowsupdated);
	}
	
	public static void sendAttachment(int rowsUpdated) throws Exception{String host = "mail1.homedepot.com";
	  String from = "horizon@cpliisad.homedepot.com";
	  String[] to = {"_2fc77b@homedepot.com"};
	  //String[] to = {"mohanraj_gurusamy@homedepot.com"};
	  //String[] Cc ={""};
	  // Get system properties
	  Properties properties = System.getProperties();

	  // Setup mail server
	  properties.setProperty("mail.smtp.host", host);

	  // Get the default Session object.
	  Session session = Session.getInstance(properties);

	  // Define message
	  Message message = new MimeMessage(session);
	  message.setFrom(new InternetAddress(from));
	  InternetAddress[] addressTo = new InternetAddress[to.length];
	 // InternetAddress[] addressCc = new InternetAddress[Cc.length];
	  for (int i = 0; i < to.length; i++) 
	  { addressTo[i] = new InternetAddress(to[i]); 
	  logger.debug("To Address "+to[i]);
	  } 
//	  for (int j = 0; j < Cc.length; j++) 
//	  { addressCc[j] = new InternetAddress(Cc[j]); 
//	  System.out.println("Cc Address "+Cc[j]);
//	  }
	  message.setRecipients(RecipientType.TO, addressTo); 
	  //message.setRecipients(RecipientType.CC, addressCc); 
	  message.setSubject("Complete delivery orders from delivery confirmation obtained status- Scenario 3");

	  // Create the message part 
	  BodyPart messageBodyPart = new MimeBodyPart();
	  String msgContent = "Hi All,<br/><br/>PFA before update shipment records for which the WorkOrderApptKey is updated.<br/>Number of rows Updated: "+rowsUpdated+"<br/><br/>Thanks<br/>";
   msgContent = msgContent + "COM Multichannel Support";
	  // Fill the message
	  messageBodyPart.setContent(msgContent ,"text/html");
	  	

	  Multipart multipart = new MimeMultipart();
	  multipart.addBodyPart(messageBodyPart);

	  // Part two is attachment
	  messageBodyPart = new MimeBodyPart();
	  //String filename = "C:\\store\\WorkOrderApptKey update.csv";
	  String filename = "/opt/isv/tomcat-6.0.18/temp/WorkOrderApptKey update.csv";
	  DataSource source = new FileDataSource(filename);
	  messageBodyPart.setDataHandler(new DataHandler(source));
	  messageBodyPart.setFileName(source.getName());
	  multipart.addBodyPart(messageBodyPart);

	  // Put parts in message
	  message.setContent(multipart);

	  // Send the message
	  Transport.send(message);
	   logger.info("Msg Send ...."); }
}
