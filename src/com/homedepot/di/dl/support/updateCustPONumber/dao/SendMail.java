package com.homedepot.di.dl.support.updateCustPONumber.dao;

import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeMessage.RecipientType;

import org.apache.log4j.Logger;

public class SendMail {
	
	private static final Logger logger = Logger
			.getLogger(SendMail.class);
	
	public void sendAttachment(int rowsUpdated) throws Exception{
	  String host = "mail1.homedepot.com";
	  String from = "horizon@cpliisad.homedepot.com";
	  /*String[] to = {"saranya_devarajan@homedepot.com"};
	  String[] Cc ={"rajasekar_veeran@homedepot.com"};*/
	  String[] to = {"_2fc77b@homedepot.com"};
	  
	  // Get system properties
	  Properties properties = System.getProperties();

	  // Setup mail server
	  properties.setProperty("mail.smtp.host", host);

	  // Get the default Session object.
	  Session session = Session.getDefaultInstance(properties);

	  // Define message
	  Message message = new MimeMessage(session);
	  message.setFrom(new InternetAddress(from));
	  InternetAddress[] addressTo = new InternetAddress[to.length];
	  //InternetAddress[] addressCc = new InternetAddress[Cc.length];
	  for (int i = 0; i < to.length; i++) 
	  { addressTo[i] = new InternetAddress(to[i]); 
	  logger.debug("To Address "+to[i]);
	  } 
	 /* for (int j = 0; j < Cc.length; j++) 
	  { addressCc[j] = new InternetAddress(Cc[j]); 
	 logger.debug("Cc Address "+Cc[j]);
	  }*/
	  message.setRecipients(RecipientType.TO, addressTo); 
	  //message.setRecipients(RecipientType.CC, addressCc); 
	  message.setSubject("Customer PO Number Update � DC PALS issue");

	  // Create the message part 
	  BodyPart messageBodyPart = new MimeBodyPart();
	  String msgContent = "Hi All,<br/><br/>PFA report for updated orders for which customer po number has been updated.<br/>Number of rows Updated: "+rowsUpdated+"<br/><br/>Thanks<br/>";
  msgContent = msgContent + "COM Multichannel Support";
	  // Fill the message
	  messageBodyPart.setContent(msgContent ,"text/html");
	  	

	  Multipart multipart = new MimeMultipart();
	  multipart.addBodyPart(messageBodyPart);

	  // Part two is attachment
	  messageBodyPart = new MimeBodyPart();
	  //String filename = "C:\\store\\CustomerPONumber.csv";
	  String filename = "/opt/isv/apache-tomcat/temp/CustomerPONumber.csv";
	  DataSource source = new FileDataSource(filename);
	  messageBodyPart.setDataHandler(new DataHandler(source));
	  messageBodyPart.setFileName("CustomerPONumber.csv");
	  multipart.addBodyPart(messageBodyPart);

	  // Put parts in message
	  message.setContent(multipart);

	  // Send the message
	  Transport.send(message);
	   logger.info("Msg Send ...."); 
	   }
}
