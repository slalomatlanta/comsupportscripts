package com.homedepot.di.dl.support.updateCustPONumber.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.apache.log4j.Logger;

public class DBConnectionUtil {
	private static final Logger logger = Logger
			.getLogger(DBConnectionUtil.class);

	public Connection getJNDIConnection(String contextName) {
		String DATASOURCE_CONTEXT = contextName;

		Connection conn = null;
		try {
			Context initialContext = new InitialContext();
			Context envContext = (Context) initialContext
					.lookup("java:comp/env");

			DataSource datasource = (DataSource) envContext
					.lookup(DATASOURCE_CONTEXT);
			if (datasource != null) {
				conn = datasource.getConnection();
				//logger.info("Connected: " + conn);
			} else {
				logger.info("Failed to lookup datasource.");
			}
		} catch (NamingException ex) {
			logger.info("Cannot get connection: " + ex);
		} catch (SQLException ex) {
			logger.info("Cannot get connection: " + ex);
		}
		return conn;
	}
	
}
