package com.homedepot.di.dl.support.updateCustPONumber.dao;

public class CustomerPODTO {
	String orderNumber;
	String lineKey;
	String lineRef;
	String custPoLineNumber;
	
	
	
	public CustomerPODTO(String orderNumber, String lineKey, String lineRef,
			String custPoLineNumber) {
		super();
		this.orderNumber = orderNumber;
		this.lineKey = lineKey;
		this.lineRef = lineRef;
		this.custPoLineNumber = custPoLineNumber;
	}
	
	public String getOrderNumber() {
		return orderNumber;
	}
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}
	public String getLineKey() {
		return lineKey;
	}
	public void setLineKey(String lineKey) {
		this.lineKey = lineKey;
	}
	public String getLineRef() {
		return lineRef;
	}
	public void setLineRef(String lineRef) {
		this.lineRef = lineRef;
	}
	public String getCustPoLineNumber() {
		return custPoLineNumber;
	}
	public void setCustPoLineNumber(String custPoLineNumber) {
		this.custPoLineNumber = custPoLineNumber;
	}
	
}
