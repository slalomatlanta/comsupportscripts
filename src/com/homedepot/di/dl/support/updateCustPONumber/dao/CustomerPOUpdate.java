package com.homedepot.di.dl.support.updateCustPONumber.dao;

import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import org.apache.log4j.Logger;

public class CustomerPOUpdate {

	private static final Logger logger = Logger
			.getLogger(CustomerPOUpdate.class);

	public void updateCustomePO() {
		
		SendMail mailObj = new SendMail();

		ArrayList<CustomerPODTO> dtoList = new ArrayList<CustomerPODTO>();
		dtoList = getPODetails();
		int noOfRowsUpdated = 0;

		try {
			
			//String outputPath = "C:\\store\\CustomerPONumber.csv";
			String outputPath = "/opt/isv/apache-tomcat/temp/CustomerPONumber.csv";
			FileWriter fw = new FileWriter(outputPath);

			fw.append("ORDER_NUMBER");
			fw.append(',');
			fw.append("LINE_NUMBER");
			fw.append(',');
			fw.append("CUSTOMER_PO_NUMBER");
			fw.append('\n');

			for (CustomerPODTO dto : dtoList) {
				//System.out.println("DTO : " + dto.getOrderNumber() + ", " + dto.getLineKey() + ", " + dto.getCustPoLineNumber());
				noOfRowsUpdated = noOfRowsUpdated + updatePODetails(dto);
				
				fw.append(dto.getOrderNumber());
				fw.append(',');
				fw.append(dto.getLineRef());
				fw.append(',');
				fw.append(dto.getCustPoLineNumber());
				fw.append('\n');
			}
			
			fw.close();

		} catch (IOException e) {
			logger.debug(e);
			e.printStackTrace();
		}
		
		try {
			if (dtoList.size() != 0){
			mailObj.sendAttachment(noOfRowsUpdated);
			}
		} catch (Exception e) {
			logger.debug(e);
			e.printStackTrace();
		}

	}

	private ArrayList<CustomerPODTO> getPODetails() {

		ArrayList<CustomerPODTO> dtoList = new ArrayList<CustomerPODTO>();

		final String url = Constants.STERLING_ATC_ZONEA_URL;
		final String uName = Constants.ZONEA_USERNAME;
		final String uPassword = Constants.ZONEA_PASSWORD;

		String query = Constants.GET_CUSTOMER_PO_DETAILS_QUERY;

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			
			Class.forName("oracle.jdbc.driver.OracleDriver");
			conn = DriverManager.getConnection(url, uName, uPassword);
			stmt = conn.createStatement();
			logger.info("DB connection success");
			//System.out.println("DB connection success");
			System.out.println(query);
			rs = stmt.executeQuery(query);

			while (rs.next()) {			
				dtoList.add(new CustomerPODTO(rs.getString(1), rs.getString(2),rs.getString(3),rs.getString(4)));
			}
			rs.close();
			stmt.close();

			conn.close();

		} catch (Exception e) {
			logger.debug(e);
			e.printStackTrace();
		}

		return dtoList;
	}

	private int updatePODetails(CustomerPODTO dto) {
		
		int noOfRowsUpdated = 0;
		String lineKey = dto.getLineKey();
		String custPONumber = dto.getCustPoLineNumber();

		String query = "update thd01.yfs_order_line set CUSTOMER_PO_NO='"
				+ custPONumber
				+ "' ,modifyts=sysdate, MODIFYUSERID='COMSupport' where order_line_key='"
				+ lineKey + "'";
		
		//System.out.println("query : " + query);

		DBConnectionUtil dbConn = new DBConnectionUtil();
		Connection conn = dbConn
				.getJNDIConnection(Constants.STERLING_ATC_RW_JNDI);

		Statement stmt = null;

		try {

			stmt = conn.createStatement();
			noOfRowsUpdated = stmt.executeUpdate(query);

			stmt.close();
			if (!conn.getAutoCommit()) {
				conn.commit();
			}
			conn.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return noOfRowsUpdated;
	}

}
