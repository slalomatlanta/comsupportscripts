package com.homedepot.di.dl.support.updateCustPONumber.dao;

public class Constants {
	
	//JNDI Connection Strings
	public static String STERLING_SSC_RO_JNDI = "jdbc/ORCL.PPRMM78X-DPR077MM.001"; // Sterling SSC Read only DB Connection
	public static String STERLING_ATC_RW_JNDI = "jdbc/ORCL.PPRMM77X-DPR077MM.001";//Sterling ATC Read Write DB Connection
	public static String COMT_SSC_RO_JNDI = "jdbc/ORCL.PPRMM78X-DPR078MM.001";// Comt SSC Read only DB connection
	public static String COMT_ATC_RO_JNDI = "jdbc/ORCL.PPRMM77X-DPR078MM.001";// Comt ATC Read write DB connection
	public static String COM_SUPPORT_MYSQL_JNDI = "jdbc/MYSQL-DB";// COM Support MySql DB connection
	public static String COM_SUPPORT_APPLLOG_JNDI = "jdbc/APPLLOG-DB";// COM Support APPL LOG DB connection
	public static String ODS_STAND_BY_JNDI = "jdbc/ORCL.PPRMM77X-DPR078MM-ODS-RO";// ODS standby DB connection
	public static String ODS_JNDI = "jdbc/ORCL.PPRMM77X-DPR078MM-ODS.001";// ODS standby REad Write	
	
	public static String STERLING_ATC_RW1_JNDI = "jdbc/ORCL.PPRMM77X-DPR77MM_SRW01.001";
	public static String STERLING_ATC_RW2_JNDI = "jdbc/ORCL.PPRMM77X-DPR77MM_SRW02.001";
	public static String STERLING_ATC_RW3_JNDI = "jdbc/ORCL.PPRMM77X-DPR77MM_SRW03.001";
	public static String COM_ATC_RW1_JNDI = "jdbc/ORCL.PPRMM77X-DPR78MM_SRW01.001";
	
	
	public static final String STERLING_ATC_ZONEA_URL = "jdbc:oracle:thin://@apragor11-scan.homedepot.com:1521/dpr77mm_sro01";
	public static final String ZONEA_USERNAME = "MMUSR01";
	public static final String ZONEA_PASSWORD = "COMS_MMUSR01";
	
	public static final String GET_CUSTOMER_PO_DETAILS_QUERY = "select h.EXTN_HOST_ORDER_REF order_nbr,l.order_line_key line_key, l.EXTN_HOST_ORDER_LINE_REF line_nbr,l.customer_po_line_no,wc.WILL_CALL_TYPE,wc.WILL_CALL_LINE_NUMBER, l.LINE_TYPE, rs.STATUS,s.DESCRIPTION status_desc," 
+" rs.STATUS_QUANTITY, l.ORIGINAL_ORDERED_QTY,l.CUSTOMER_PO_NO, h.SELLER_ORGANIZATION_CODE, l.SHIPNODE_KEY, h.CREATETS"
+" from thd01.YFS_ORDER_HEADER h, thd01.YFS_ORDER_RELEASE_STATUS rs, thd01.YFS_STATUS s, thd01.yfs_order_line l" 
+" left join thd01.HD_WILL_CALL wc on l.ORDER_HEADER_KEY=wc.ORDER_HEADER_KEY"
+" where l.SHIPNODE_KEY in ('5078','6175','6177') and l.ORDER_HEADER_KEY = h.ORDER_HEADER_KEY and h.DOCUMENT_TYPE='0001' and h.DRAFT_ORDER_FLAG='N'"
+" and l.LINE_TYPE = 'SO'  and h.EXTN_HOST_ORDER_REF like 'H%'"
+" and l.ORDER_LINE_KEY=rs.ORDER_LINE_KEY and l.ORDER_HEADER_KEY=rs.ORDER_HEADER_KEY and rs.STATUS_QUANTITY > 0 and rs.STATUS=s.STATUS"
+" and s.PROCESS_TYPE_KEY='ORDER_FULFILLMENT' and l.CUSTOMER_PO_NO =' '  order by h.CREATETS asc";
	
}
