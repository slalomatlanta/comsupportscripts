package com.homedepot.di.dl.support.updateCustPONumber.web;

import java.io.IOException;
import java.util.GregorianCalendar;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.homedepot.di.dl.support.updateCustPONumber.dao.CustomerPOUpdate;

/**
 * Servlet implementation class UpdateCustPONoServlet
 */
public class UpdateCustPONoServlet extends HttpServlet {
	
	private static final Logger logger = Logger
			.getLogger(UpdateCustPONoServlet.class);
	
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateCustPONoServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		CustomerPOUpdate custPOUpdate = new CustomerPOUpdate();
		
		logger.info("UpdateCustPONoServlet - Started : "+GregorianCalendar.getInstance().getTime());
		
		custPOUpdate.updateCustomePO();
		
		logger.info("UpdateCustPONoServlet - Completed : "+GregorianCalendar.getInstance().getTime());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
