package com.homedepot.di.dl.support.blackFridaymonitor.dao;

import java.io.File;
import java.net.URL;
import java.net.URLClassLoader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Locale;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;

import com.homedepot.di.dl.support.blockingLockmonitor.dao.MonitorLock;
import com.homedepot.di.dl.support.util.Constants;
import com.homedepot.di.dl.support.util.DBConnectionUtil;

public class ItemCountReport {

	final static Logger logger = Logger.getLogger(ItemCountReport.class);
	
	/**
	 * @param args
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public static void main(String[] args) throws SQLException,
			ClassNotFoundException {
		// TODO Auto-generated method stub
		StringBuilder emailText = new StringBuilder();
		emailText = getItemCountReport();
		logger.info(emailText.toString());

	}

	public static StringBuilder getItemCountReport() throws SQLException,
			ClassNotFoundException {
		StringBuilder emailText = new StringBuilder();
		try {
			DBConnectionUtil dbConn = new DBConnectionUtil();
//			File file = new File("C:\\Tax");
			/*File file = new File("/opt/isv/tomcat-6.0.18/webapps/ConnProperties");
			URL[] urls = { file.toURI().toURL() };
			ClassLoader loader = new URLClassLoader(urls);
			ResourceBundle bundle = ResourceBundle.getBundle("DBConnection",
					Locale.getDefault(), loader);

			final String driverClass = bundle.getString("oracleDriverClass");
			final String connectionURL = bundle.getString("sterlingConnectionURL_SSC");
			final String uName = bundle.getString("UserID");
			final String uPassword = bundle.getString("UserPassword");*/

//			final String driverClass = "oracle.jdbc.driver.OracleDriver";
//			final String connectionURL = "jdbc:oracle:thin://@pprmm77x.homedepot.com:1521/dpr77mm_srw02";
//			final String uName = "MMUSR01";
//			final String uPassword = "COMS_MMUSR01";
			//Connection sterCon = null;

			// emailText.append("Hello All,");
			// emailText.append(System.getProperty("line.separator"));
			// emailText.append(System.getProperty("line.separator"));
			// emailText.append("	Below is the Regular Item count Report.");
			// emailText.append(System.getProperty("line.separator"));
			emailText.append(System.getProperty("line.separator"));

			String sterItemCnt = "select count(1) from thd01.yfs_item where "
					+ "item_key not in (select item_key from thd01.yfs_kit_item) and "
					+ "item_key not in (select component_item_key from thd01.yfs_kit_item)";

			// try
			// {
			/*Class.forName(driverClass);
			// Sterling connection opens here
			sterCon = DriverManager.getConnection(connectionURL, uName,
					uPassword);*/
			Connection sscConnster = dbConn.getJNDIConnection(Constants.STERLING_SSC_RO_JNDI);
			Statement stmt1 = null;
			stmt1 = sscConnster.createStatement();
			ResultSet result1 = null;
			result1 = stmt1.executeQuery(sterItemCnt);
			while (result1.next()) {
				String count = result1.getString(1);
				emailText.append("Sterling Regular Item count: " + count);
			}
			stmt1.close();
			result1.close();
			sscConnster.close();

			// emailText.append(System.getProperty("line.separator"));
			// emailText.append(System.getProperty("line.separator"));
			// emailText.append("Thanks,");
			// emailText.append(System.getProperty("line.separator"));
			// emailText.append("COM Multi-Channel Support");
			// emailText.append(System.getProperty("line.separator"));
			// emailText.append(System.getProperty("line.separator"));
			// emailText.append(System.getProperty("line.separator"));
			// emailText.append("Note: This is a system-generated e-mail. Please do NOT reply to it.");

		} catch (Exception e) {
			e.printStackTrace();
		}

		return emailText;

	}

}
