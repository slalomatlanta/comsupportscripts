package com.homedepot.di.dl.support.blackFridaymonitor.dao;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;

import com.homedepot.di.dl.support.switchToNormal.dao.SwitchTransmissionState;
import com.homedepot.di.dl.support.util.Constants;
import com.homedepot.di.dl.support.util.ConstantsQuery;
import com.homedepot.di.dl.support.util.DBConnectionUtil;

public class HourlyAdjReport {
	final static Logger logger = Logger.getLogger(HourlyAdjReport.class);
	/**
	 * @param args
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public static void main(String[] args) throws SQLException,
			ClassNotFoundException {
		// TODO Auto-generated method stub

		StringBuilder emailText = new StringBuilder();
		emailText = getHourlyReport();
		logger.info(emailText.toString());
	}

//	public static StringBuilder getHourlyReport(String hourStart, String dayStart) throws SQLException,
//			ClassNotFoundException {
	
	public static StringBuilder getHourlyReport() throws SQLException,
		ClassNotFoundException {	
		
		ConstantsQuery qc = new ConstantsQuery();
		
		String comAdjDay = null;
		String sterAdjDay = null;
		String s2sDay = null;
		String bopisDay = null;
		String bopisHour = null;
		String bossHour = null;
		String bossDay = null;
		String splOrderHour = null;
		String splOrderDay = null;
		String bopisAndBosshour = null;
		String bopisAndBossDay = null; 
		String PureSTHDay = null; 
		String PureSTHhour = null; 
		String MixSTHhour = null; 
		String MixSTHday = null; 
		String BODFShour = null; 
		String BODFSday = null; 

		StringBuilder emailText = new StringBuilder();

		try{
			
			
		// Getting the DB connection properties from propeties file. /opt/isv/tomcat-6.0.18/webapps/ConnProperties 	
		//File file = new File("C:\\test");
		

		/*ResourceBundle bundle = ResourceBundle.getBundle("DBConnection", Locale.getDefault(), loader);
		final String comDriverClass = bundle.getString("oracleDriverClass");
		final String comConnectionURLThin = bundle.getString("comtConnectionURL_SSC");
		final String comUserID = bundle.getString("UserID");
		final String comUserPassword = bundle.getString("UserPassword");*/
		
		DBConnectionUtil dbConn = new DBConnectionUtil();
		
		
		//Connection ccon = null;
		
//		final String comDriverClass = "oracle.jdbc.driver.OracleDriver";
//		final String comConnectionURLThin = "jdbc:oracle:thin:@pprmm78x.homedepot.com:1521/dpr78mm_sro01"; 
//
//		final String comUserID = "MMUSR01";
//		final String comUserPassword = "COMS_MMUSR01";
//		
//		final String connectionURL = "jdbc:oracle:thin:@//pprmm78x.homedepot.com:1521/dpr77mm_sro01";
//		final String uName = "MMUSR01";
//		final String uPassword = "COMS_MMUSR01";
		
			
		/*final String connectionURL = bundle.getString("sterlingConnectionURL_SSC");
		final String uName = bundle.getString("UserID");
		final String uPassword = bundle.getString("UserPassword");
		
		Connection sterCon = null;*/
		
		
		

//		StringBuilder emailText = new StringBuilder();
		emailText.append("Hello All,");
		emailText.append(System.getProperty("line.separator"));
		emailText.append(System.getProperty("line.separator"));
		emailText.append("	Below are the Hourly Statistics.");
		emailText.append(System.getProperty("line.separator"));
		emailText.append(System.getProperty("line.separator"));

//		try {
			Calendar caldar = Calendar.getInstance();
			SimpleDateFormat simpDate;
			
			Connection sscConncomt = dbConn.getJNDIConnection(Constants.COMT_SSC_RO_JNDI);
			
			simpDate = new SimpleDateFormat("hh:00 a");
			String hEnd = simpDate.format(caldar.getTime());
			caldar.add(Calendar.HOUR_OF_DAY, -1);
			String hStart = simpDate.format(caldar.getTime());
			
			String invAdjQuery = "select count(*) from thd01.inv_adj_trnsm where inv_adj_ovrly_ind='A' and "
					+ "trnsm_subm_ts >= sysdate-1/24 and trnsm_subm_ts<=sysdate";

			String invAdjQuery1 = "select count(*) from thd01.inv_adj_trnsm where inv_adj_ovrly_ind='A' and "
					+ "trnsm_subm_ts >= trunc(sysdate) and trnsm_subm_ts<=sysdate";

			if ("12:00 AM".equalsIgnoreCase(hEnd)) {
				invAdjQuery1 = "select count(*) from thd01.inv_adj_trnsm where inv_adj_ovrly_ind='A' and "
						+ "trnsm_subm_ts >= sysdate-1 and trnsm_subm_ts<=sysdate";
			}

			emailText.append("----------------------------------------");
			emailText.append(System.getProperty("line.separator"));
			emailText.append("For Last 1 Hour from " + hStart + " to " + hEnd);
			emailText.append(System.getProperty("line.separator"));
			emailText.append("----------------------------------------");
			emailText.append(System.getProperty("line.separator"));

			Statement stmt = null;
			stmt = sscConncomt.createStatement();
			ResultSet result = null;
			result = stmt.executeQuery(invAdjQuery);
			while (result.next()) {
				String count = result.getString(1);
				emailText.append("COM Adjustments count	: " + count);
				sendDataToGraphana("com_adjust.hourly.count "+count);
				emailText.append(System.getProperty("line.separator"));
			}
			stmt.close();
			result.close();

			Statement stmt3 = null;
			stmt3 = sscConncomt.createStatement();
			ResultSet result3 = null;
			result3 = stmt3.executeQuery(invAdjQuery1);
			// emailText.append(System.getProperty("line.separator"));
			while (result3.next()) {
				comAdjDay = result3.getString(1);
			}
			stmt3.close();
			result3.close();
			sscConncomt.close();
			// COM Tier connection closed

//			Logic to get hourStart and dayStart
			Calendar cal = Calendar.getInstance();
			DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHH");
			String date = dateFormat.format(cal.getTime());
//			String hourEnd = date + "";
			cal.add(Calendar.HOUR_OF_DAY, -1);
			date = dateFormat.format(cal.getTime());
			String hourStart = date + "";
			DateFormat df = new SimpleDateFormat("yyyyMMdd");
			date = df.format(cal.getTime());
			String dayStart = date + "";
//			End of Logic to get hourStart and dayStart
			
			
			// Queries to fetch last hour count from sterling
			String sterInvAdjCnt = "select count(1) from THD01.yfs_inventory_audit "
					+ "where inventory_audit_key like '"
					+ hourStart
					+ "%' and "
					+ "transaction_type like '%ADJUSTMENT%'";
			String orderCount = qc.s2sFirstPart + hourStart + qc.s2sLastPart;
//			System.out.println("orderCount: "+orderCount);
//			String sterBopisOrBoss = "select distinct order_type, count(*) from "
//					+ "(select distinct decode (line_type, 'SL', 'BOPIS','SO', 'BOSS') order_type, order_header_key "
//					+ "from yfs_order_line where order_header_key in(select order_header_key from "
//					+ "(select order_header_key, count (distinct line_type) line_type_counts "
//					+ "from yfs_order_line where line_type in ('SL','SO') "
//					+ "and order_header_key like '"
//					+ hourStart
//					+ "%' "
//					+ "group by order_header_key) where line_type_counts =1)) group by order_type";
			String sterBopisAndBoss = qc.bopisBossFirstPart + hourStart + qc.bopisBossLastPart;
//			System.out.println("sterBopisOrBoss : "+sterBopisOrBoss);
//			System.out.println("sterBopisAndBoss : "+sterBopisAndBoss);
			// Queries to fetch day count from sterling
			String sterInvAdjCnt1 = "select count(1) from THD01.yfs_inventory_audit  "
					+ "where inventory_audit_key like '"
					+ dayStart
					+ "%' and "
					+ "transaction_type like '%ADJUSTMENT%'";
			String orderCount1 = qc.s2sFirstPart + dayStart + qc.s2sLastPart;
			
			/*String sterBopisOrBoss1 = "select distinct order_type, count(*) from "
					+ "(select distinct decode (line_type, 'SL', 'BOPIS','SO', 'BOSS') order_type, order_header_key "
					+ "from yfs_order_line where order_header_key in(select order_header_key from "
					+ "(select order_header_key, count (distinct line_type) line_type_counts "
					+ "from yfs_order_line where line_type in ('SL','SO') "
					+ "and order_header_key like '"
					+ daystart
					+ "%' "
					+ "group by order_header_key) where line_type_counts =1)) group by order_type";*/
			String sterBopisAndBoss1 = qc.bopisBossFirstPart + dayStart + qc.bopisBossLastPart;

			String hourlyBopis = qc.bopisFirstPart + hourStart + qc.bopisLastPart;

			String dayBopis = qc.bopisFirstPart + dayStart + qc.bopisLastPart;
			
			String hourlyBoss = qc.bossFirstPart + hourStart + qc.bossLastPart;

			String dayBoss = qc.bossFirstPart + dayStart + qc.bossLastPart;
			
			String hourlySplOrder = qc.splOrderFirstPart + hourStart + qc.splOrderLastPart;

			String daySplOrder = qc.splOrderFirstPart + dayStart + qc.splOrderLastPart;
			
			String hourlybodfsOrder = qc.bodfsFirstPart + hourStart + qc.bodfsLastPart;

			String daybodfsOrder = qc.bodfsFirstPart + dayStart + qc.bodfsLastPart;
			
			String hourlyPuresthOrder = qc.puresthFirstPart + hourStart + qc.puresthLastPart;
			String dayPuresthOrder = qc.puresthFirstPart + dayStart + qc.puresthLastPart;
			
			String hourlymixsthOrder = qc.MixsthFirstPart + hourStart + qc.MixsthSecondPart + hourStart + qc.MixsthLastPart;
			String daymixsthOrder = qc.MixsthFirstPart + dayStart + qc.MixsthSecondPart + dayStart + qc.MixsthLastPart;

			String hourlyLockout = "select count(1) from THD01.yfs_reprocess_error where errorcode='YFC0101' " +
					"and to_char(createts,'YYYYMMDDHH') = '"+hourStart+"'";
			
			String dayLockout = "select count(1) from THD01.yfs_reprocess_error where errorcode='YFC0101' " +
					"and to_char(createts,'YYYYMMDD') = '"+dayStart+"'";


			String hourlyDeadlock= "select count(1) from thd01.yfs_inbox i, THD01.YFS_INBOX_REFERENCES ir where i.INBOX_KEY like '"+hourStart+
					"%' and ERROR_REASON='YFC0100' and ir.INBOX_KEY=i.INBOX_KEY and ir.NAME='SQLStatement'";

			String dayDeadlock= "select count(1) from thd01.yfs_inbox i, THD01.YFS_INBOX_REFERENCES ir where i.INBOX_KEY like '"+dayStart+
					"%' and ERROR_REASON='YFC0100' and ir.INBOX_KEY=i.INBOX_KEY and ir.NAME='SQLStatement'";
			
			// Sterling connection opens here
			Connection sscConnster = dbConn.getJNDIConnection(Constants.STERLING_SSC_RO_JNDI);
			//System.out.println(sscConnster);
			Statement stmt1 = null;
			stmt1 = sscConnster.createStatement();
			ResultSet result1 = null;
//			System.out.println("sterInvAdjCnt: "+sterInvAdjCnt);
			result1 = stmt1.executeQuery(sterInvAdjCnt);
			while (result1.next()) {
				String count = result1.getString(1);
				emailText.append("Sterling Adjustments count	: " + count);
				sendDataToGraphana("sterling_adjust.hourly.count "+count);
				emailText.append(System.getProperty("line.separator"));
			}
			stmt1.close();
			result1.close();

			Statement stmt4 = null;
			stmt4 = sscConnster.createStatement();
			ResultSet result4 = null;
			result4 = stmt4.executeQuery(sterInvAdjCnt1);
			while (result4.next()) {
				sterAdjDay = result4.getString(1);
			}
			stmt4.close();
			result4.close();
			
			String s2sCount = null;
			Statement stmt2 = null;
			stmt2 = sscConnster.createStatement();
			ResultSet result2 = null;
//			System.out.println("S2S orderCount : "+orderCount);
			result2 = stmt2.executeQuery(orderCount);
			
			while (result2.next()) {
				if ("S2S".equalsIgnoreCase(result2.getString(1))) {
					s2sCount = result2.getString(2);
				}
			}
			if (s2sCount == null) {
				s2sCount = "0";
			}
			
			stmt2.close();
			result2.close();

			// BOPIS
			Statement stmt6 = null;
			stmt6 = sscConnster.createStatement();
			ResultSet result5 = null;
			result5 = stmt6.executeQuery(hourlyBopis);
//			System.out.println("BOPIS, BOSS  :"+hourlyBopis);
			while (result5.next()) {
				if ("BOPIS".equalsIgnoreCase(result5.getString(1))) {
					bopisHour = result5.getString(2);
				}
			}
			if (bopisHour == null) {
				bopisHour = "0";
			}
			stmt6.close();
			result5.close();
			
//			BOSS
			Statement stmtBoss = null;
			stmtBoss = sscConnster.createStatement();
			ResultSet resultBoss = null;
			resultBoss = stmtBoss.executeQuery(hourlyBoss);
//			System.out.println("BOPIS, BOSS  :"+hourlyBoss);
			while (resultBoss.next()) {
				if ("BOSS".equalsIgnoreCase(resultBoss.getString(1))) {
					bossHour = resultBoss.getString(2);
				}
			}
			if (bossHour == null) {
				bossHour = "0";
			}
			stmtBoss.close();
			resultBoss.close();
//			End of BOSS

//			BOPIS-BOSS
			stmt6 = null;
			stmt6 = sscConnster.createStatement();
			result5 = null;
//			System.out.println("sterBopisAndBoss : "+sterBopisAndBoss);
			result5 = stmt6.executeQuery(sterBopisAndBoss);
//			System.out.println("BOPIS-BOSS :"+ sterBopisAndBoss);
			while (result5.next()) {// BOPIS-BOSS
				if ("BOPIS-BOSS".equalsIgnoreCase(result5.getString(1))) {
					bopisAndBosshour = result5.getString(2);
				}
			}
			if (bopisAndBosshour == null) {
				bopisAndBosshour = "0";
			}
			stmt6.close();
			result5.close();
			
//			Special order count
			Statement stmtSplOrd = null;
			stmtSplOrd = sscConnster.createStatement();
			ResultSet resultSplOrd = null;
			resultSplOrd = stmtSplOrd.executeQuery(hourlySplOrder);
//			System.out.println("hourlySplOrder  :"+hourlySplOrder);
			while (resultSplOrd.next()) {
				splOrderHour = resultSplOrd.getString(1);
			}
			stmtSplOrd.close();
			resultSplOrd.close();
//			End of Special order count
			
			//Bodfs order count
			Statement stmtBodfsOrd = null;
			stmtBodfsOrd = sscConnster.createStatement();
			ResultSet resultbodfsOrd = null;
			resultbodfsOrd = stmtBodfsOrd.executeQuery(hourlybodfsOrder);
//			System.out.println("hourlySplOrder  :"+hourlySplOrder);
			while (resultbodfsOrd.next()) {
				BODFShour = resultbodfsOrd.getString(1);
			}
			stmtBodfsOrd.close();
			resultbodfsOrd.close();
//			End of bodfs order count
			
			
			//Mix STH  order count
			Statement stmtMIXSTHOrd = null;
			stmtMIXSTHOrd = sscConnster.createStatement();
			ResultSet resultMIXSTHOrd = null;
			resultMIXSTHOrd = stmtMIXSTHOrd.executeQuery(hourlymixsthOrder);
//			System.out.println("hourlySplOrder  :"+hourlySplOrder);
			while (resultMIXSTHOrd.next()) {
				MixSTHhour = resultMIXSTHOrd.getString(1);
			}
			stmtMIXSTHOrd.close();
			resultMIXSTHOrd.close();
//			End of Mix sth order count
			
			//Pure STH  order count
			Statement stmtpureSTHOrd = null;
			stmtpureSTHOrd = sscConnster.createStatement();
			ResultSet resultpureSTHOrd = null;
			resultpureSTHOrd = stmtpureSTHOrd.executeQuery(hourlyPuresthOrder);
//			System.out.println("hourlySplOrder  :"+hourlySplOrder);
			while (resultpureSTHOrd.next()) {
				PureSTHhour = resultpureSTHOrd.getString(1);
			}
			stmtpureSTHOrd.close();
			resultpureSTHOrd.close();
//			End of Mix sth order count

			// Appending S2S, BOPIS, BOSS and BOPIS-BOSS orders count
			emailText.append("S2S Orders count		: " + s2sCount);
			emailText.append(System.getProperty("line.separator"));
			emailText.append("BOPIS Orders count		: " + bopisHour);
			emailText.append(System.getProperty("line.separator"));
			emailText.append("BOSS Orders count		: " + bossHour);
			emailText.append(System.getProperty("line.separator"));
			emailText.append("BOPIS-BOSS Orders count	: " + bopisAndBosshour);
			emailText.append(System.getProperty("line.separator"));
			emailText.append("Special Orders count		: " + splOrderHour);
			emailText.append(System.getProperty("line.separator"));
			emailText.append("BODFS Orders count		: " + BODFShour);
			emailText.append(System.getProperty("line.separator"));
			emailText.append("MixSTH Orders count		: " + MixSTHhour);
			emailText.append(System.getProperty("line.separator"));
			emailText.append("PureSTH Orders count		: " + PureSTHhour);
			emailText.append(System.getProperty("line.separator"));
			emailText.append("PureSTH Orders count		: " + PureSTHhour);
			emailText.append(System.getProperty("line.separator"));
			
			sendDataToGraphana("s2s.hourly.count "+s2sCount);
			sendDataToGraphana("bopis.hourly.count "+bopisHour);
			sendDataToGraphana("boss.hourly.count "+bossHour);
			sendDataToGraphana("bopis_boss.hourly.count "+bopisAndBosshour);
			sendDataToGraphana("esvs.hourly.count "+splOrderHour);
			
			stmt6 = null;
			stmt6 = sscConnster.createStatement();
			result5 = null;
			result5 = stmt6.executeQuery(dayBopis);
			while (result5.next()) {
				if ("BOPIS".equalsIgnoreCase(result5.getString(1))) {
					bopisDay = result5.getString(2);
				}
			}
			if (bopisDay == null) {
				bopisDay = "0";
			}
			stmt6.close();
			result5.close();
			
//			day BOSS
			stmt6 = null;
			stmt6 = sscConnster.createStatement();
			result5 = null;
			result5 = stmt6.executeQuery(dayBoss);
			while (result5.next()) {
				if ("BOSS".equalsIgnoreCase(result5.getString(1))) {
					bossDay = result5.getString(2);
				}
			}
			if (bossDay == null) {
				bossDay = "0";
			}
			stmt6.close();
			result5.close();
//			End of day BOSS 
			

			stmt6 = null;
			stmt6 = sscConnster.createStatement();
			result5 = null;
			result5 = stmt6.executeQuery(sterBopisAndBoss1);
			while (result5.next()) {
				if ("BOPIS-BOSS".equalsIgnoreCase(result5.getString(1))) {
					bopisAndBossDay = result5.getString(2);
				}
			}
			if (bopisAndBossDay == null) {
				bopisAndBossDay = "0";
			}
			stmt6.close();
			result5.close();
			// END BOPIS AND BOSS
			Statement stmt5 = null;
			stmt5 = sscConnster.createStatement();
			result5 = null;
			result5 = stmt5.executeQuery(orderCount1);
			while (result5.next()) {
				if ("S2S".equalsIgnoreCase(result5.getString(1))) {
					s2sDay = result5.getString(2);
				}
			}
			
			if (s2sDay == null) {
				s2sDay = "0";
			}
			
			stmt5.close();
			result5.close();
			
//			Special order count
			stmtSplOrd = null;
			stmtSplOrd = sscConnster.createStatement();
			resultSplOrd = null;
			resultSplOrd = stmtSplOrd.executeQuery(daySplOrder);
//			System.out.println("hourlySplOrder  :"+daySplOrder);
			while (resultSplOrd.next()) {
				splOrderDay = resultSplOrd.getString(1);
			}
			stmtSplOrd.close();
			resultSplOrd.close();
//			End of Special order count
			
//			BODFS order count
			stmtBodfsOrd = null;
			stmtBodfsOrd = sscConnster.createStatement();
			resultbodfsOrd = null;
			resultbodfsOrd = stmtBodfsOrd.executeQuery(daybodfsOrder);
//			System.out.println("hourlySplOrder  :"+daySplOrder);
			while (resultbodfsOrd.next()) {
				BODFSday = resultbodfsOrd.getString(1);
			}
			stmtBodfsOrd.close();
			resultbodfsOrd.close();
//			End of BODFS order count
			
//			MIX STH order count
			stmtMIXSTHOrd = null;
			stmtMIXSTHOrd = sscConnster.createStatement();
			resultMIXSTHOrd = null;
			resultMIXSTHOrd = stmtMIXSTHOrd.executeQuery(daymixsthOrder);
//			System.out.println("hourlySplOrder  :"+daySplOrder);
			while (resultMIXSTHOrd.next()) {
				MixSTHday = resultMIXSTHOrd.getString(1);
			}
			stmtMIXSTHOrd.close();
			resultMIXSTHOrd.close();
//			End of mix sth order count
			
	
//			Pure STH order count
			stmtpureSTHOrd = null;
			stmtpureSTHOrd = sscConnster.createStatement();
			resultpureSTHOrd = null;
			resultpureSTHOrd = stmtpureSTHOrd.executeQuery(dayPuresthOrder);
//			System.out.println("hourlySplOrder  :"+daySplOrder);
			while (resultpureSTHOrd.next()) {
				PureSTHDay = resultpureSTHOrd.getString(1);
			}
			stmtpureSTHOrd.close();
			resultpureSTHOrd.close();
//			End of pure sth order count

			
//			Adding the deadlock count in the report 

			stmt5 = null;
			stmt5 = sscConnster.createStatement();
			result5 = null;
			result5 = stmt5.executeQuery(hourlyDeadlock);
			while (result5.next()) {
				String hourlyDeadLock = result5.getString(1);
				emailText.append("DeadLock count		: "+ hourlyDeadLock);
				sendDataToGraphana("DeadLock.hourly.count "+ hourlyDeadLock);
				emailText.append(System.getProperty("line.separator"));
				emailText.append(System.getProperty("line.separator"));
			}
			stmt5.close();
			result5.close();
			
			String dayDeadlockCount = null;
			stmt5 = null;
			stmt5 = sscConnster.createStatement();
			result5 = null;
			result5 = stmt5.executeQuery(dayDeadlock);
			while (result5.next()) {
				dayDeadlockCount = result5.getString(1);
			}
			stmt5.close();
			result5.close();
			
//			End of the code for adding deadlockcount
			
			
			sscConnster.close();

			emailText.append("----------------------------------------");
			emailText.append(System.getProperty("line.separator"));
			emailText.append("For the Day since 12:00 AM ");
			emailText.append(System.getProperty("line.separator"));
			emailText.append("----------------------------------------");
			emailText.append(System.getProperty("line.separator"));
			emailText.append("COM Adjustments count	: " + comAdjDay);
			emailText.append(System.getProperty("line.separator"));
			emailText.append("Sterling Adjustments count	: " + sterAdjDay);
			emailText.append(System.getProperty("line.separator"));
			emailText.append("S2S Orders count		: " + s2sDay);
			emailText.append(System.getProperty("line.separator"));
			emailText.append("BOPIS Orders count		: " + bopisDay);
			emailText.append(System.getProperty("line.separator"));
			emailText.append("BOSS Orders count		: " + bossDay);
			emailText.append(System.getProperty("line.separator"));
			emailText.append("BOPIS-BOSS Orders count	: " + bopisAndBossDay);
			emailText.append(System.getProperty("line.separator"));
			emailText.append("Special Orders count		: " + splOrderDay);
			emailText.append(System.getProperty("line.separator"));
			emailText.append("BODFS Orders count		: " + BODFSday);
			emailText.append(System.getProperty("line.separator"));
			emailText.append("MIX STH Orders count		: " + MixSTHday);
			emailText.append(System.getProperty("line.separator"));
			emailText.append("Pure STH Orders count		: " + PureSTHDay);
			emailText.append(System.getProperty("line.separator"));
			emailText.append("DeadLock count		: "+dayDeadlockCount);
			emailText.append(System.getProperty("line.separator"));
			
			sendDataToGraphana("com_adjust.daily.count "+comAdjDay);
			sendDataToGraphana("sterling_adjust.daily.count "+sterAdjDay);
			sendDataToGraphana("s2s.daily.count "+s2sDay);
			sendDataToGraphana("bopis.daily.count "+bopisDay);
			sendDataToGraphana("boss.daily.count "+bossDay);
			sendDataToGraphana("bopis_boss.daily.count "+bopisAndBossDay);
			sendDataToGraphana("esvs.daily.count "+splOrderDay);
						sendDataToGraphana("deadlock.daily.count "+dayDeadlockCount);

			Calendar calen = Calendar.getInstance();
//			DateFormat dateFmat = new SimpleDateFormat("yyyy-MM-dd");

			int hour = calen.get(Calendar.HOUR_OF_DAY);
			if (hour == 8 || hour == 14 || hour == 19) {
				StringBuilder itemCountText = new StringBuilder();
//				ItemCountReport dao = new ItemCountReport();
				itemCountText = ItemCountReport.getItemCountReport();
				emailText.append(itemCountText);
			}

			emailText.append(System.getProperty("line.separator"));
			emailText.append(System.getProperty("line.separator"));
			emailText.append("Thanks,");
			emailText.append(System.getProperty("line.separator"));
			emailText.append("COM Multi-Channel Support");
			emailText.append(System.getProperty("line.separator"));
			emailText.append(System.getProperty("line.separator"));
			emailText.append(System.getProperty("line.separator"));
			emailText.append("Note: This is a system-generated e-mail. Please do NOT reply.");
			
			/*emailText.append(System.getProperty("line.separator"));
			emailText.append(System.getProperty("line.separator"));
			emailText.append("Hour start : "+hourStart);
			emailText.append(System.getProperty("line.separator"));
			emailText.append("Day start : "+dayStart);
			emailText.append(System.getProperty("line.separator"));*/
			
		} catch (Exception e) {
			e.printStackTrace();
		}

		return emailText;

	}
	
	public static void sendDataToGraphana(String value) {
		File wd = new File("/home/tomcat/temp");
		Process proc = null;

		try {
			String command = "echo \"prod.retail.com.sterling.order_count."+value+"\" `date +%s` | nc graphite.homedepot.com 2003";

			proc = Runtime.getRuntime().exec(command, null, wd);
			//System.out.println(command);
			//System.out.println("sendDataToGraphana() completed");
		} catch (Exception e) {
			logger.error("Hourly Statistics - sendDataToGraphana() : "+e);

		}
	}

}
