package com.homedepot.di.dl.support.blackFridaymonitor.web;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Calendar;
import java.util.Locale;
import java.util.Properties;
import java.util.ResourceBundle;

import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.homedepot.di.dl.support.blackFridaymonitor.dao.HourlyAdjReport;

/**
 * Servlet implementation class HourlyAdjServlet
 */
public class HourlyAdjServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public HourlyAdjServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String hourStart = null;
		String dayStart = null;
		
		StringBuilder text = new StringBuilder();
		HourlyAdjReport dao = new HourlyAdjReport();
		String eMailSubject = "Hourly Statistics ";

		Session session = null;
		MimeMessage mimeMessage = null;
		MimeBodyPart mimeBodyPart = null;
		Multipart multiPart = null;
		String date = null;
		Calendar calendar = null;
		Properties properties = null;

		String msg = null;
		response.setContentType("text/html");
		PrintWriter out = null;
		
		try
		{
//			hourStart = request.getParameter("hourStart");
//			dayStart = request.getParameter("dayStart");
//			text = dao.getHourlyReport(hourStart, dayStart);
			
			//File file = new File("C:\\test");
			/*File file = new File("/opt/isv/tomcat-6.0.18/webapps/ConnProperties");  
			URL[] urls = {file.toURI().toURL()};   
			ClassLoader loader = new URLClassLoader(urls);

			ResourceBundle bundle = ResourceBundle.getBundle("DBConnection", Locale.getDefault(), loader);*/
			
			text = dao.getHourlyReport();
			calendar = Calendar.getInstance();
			date = calendar.getTime().toString();
			properties = System.getProperties();
			properties.put("mail.host", "mail1.homedepot.com");
			session = Session.getInstance(properties, null);
			mimeMessage = new MimeMessage(session);
			mimeMessage.setFrom(new InternetAddress("horizon@cpliisad.homedepot.com"));
			mimeMessage.setRecipients(Message.RecipientType.TO, InternetAddress.parse("_2fc77b@homedepot.com", false));
//			mimeMessage.setRecipients(Message.RecipientType.TO, InternetAddress.parse(bundle.getString("EMail_Id"), false));
			
			mimeMessage.setSubject(eMailSubject + date);
			mimeBodyPart = new MimeBodyPart();
			mimeBodyPart.setText(text.toString());
			multiPart = new MimeMultipart();
			multiPart.addBodyPart(mimeBodyPart);
			mimeMessage.setContent(multiPart);
			Transport.send(mimeMessage);
			
			out = response.getWriter();
			/*
			* Write the HTML to the response
			*/
			msg = "Success";
		}
		catch (Exception e) 
		{
			msg = "Failure";
			e.printStackTrace();
		}
		out.println(msg);
		out.close();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
