package com.homedepot.di.dl.support.resyncSku.web;

import java.io.IOException;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeMessage.RecipientType;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.homedepot.di.dl.support.resyncSku.dao.LimitedResync;

/**
 * Servlet implementation class LimitedResyncServlet
 */
public class LimitedResyncServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	final static Logger logger = Logger.getLogger(LimitedResyncServlet.class);
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LimitedResyncServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		logger.info("Limited Resync Servlet Started :: "+GregorianCalendar.getInstance().getTime());
		LimitedResync resync= new LimitedResync();
		try {
			resync.startLimitedResync();
			
			try {
				sendAttachment();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				logger.info("Limited Resync Servlet :: "+GregorianCalendar.getInstance().getTime()+e);
				e.printStackTrace();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			logger.info("Limited Resync Servlet :: "+GregorianCalendar.getInstance().getTime()+e);
			e.printStackTrace();
		}
		catch(Exception e)
		{
			logger.error("Limited Resync Servlet :: "+GregorianCalendar.getInstance().getTime()+e);
			e.printStackTrace();
		}
		logger.info("Limited Resync Servlet Completed:: "+GregorianCalendar.getInstance().getTime());
		}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}
	
	public static void sendAttachment() throws Exception {
		String host = "mail1.homedepot.com";
		String from = "horizon@cpliisad.homedepot.com";
		//String[] to = { "mohanraj_gurusamy@homedepot.com" };
		//String[] Cc ={"jagatdeep_chakraborty@homedepot.com"};
		String[] to ={"MANJUNATHA_VENKATARAMAIAH@homedepot.com","NIRAL_PATEL2@homedepot.com","jagatdeep_chakraborty@homedepot.com","DENNIS_JONES@homedepot.com"};
		String[] Cc ={"_2fc77b@homedepot.com"};
//Get Date
		 DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a");
         
         Date today = new Date();
         
         String date1 = dateFormat.format(today);

		// Get system properties
		Properties properties = System.getProperties();

		// Setup mail server
		properties.setProperty("mail.smtp.host", host);

		// Get the default Session object.
		Session session = Session.getInstance(properties);

		// Define message
		Message message = new MimeMessage(session);
		message.setFrom(new InternetAddress(from));
		InternetAddress[] addressTo = new InternetAddress[to.length];
		InternetAddress[] addressCc = new InternetAddress[Cc.length];
		for (int i = 0; i < to.length; i++) {
			addressTo[i] = new InternetAddress(to[i]);
			logger.debug("To Address " + to[i]);
		}
		for (int j = 0; j < Cc.length; j++)
		 { addressCc[j] = new InternetAddress(Cc[j]);
		 logger.debug("Cc Address "+Cc[j]);
		 }
		message.setRecipients(RecipientType.TO, addressTo);
		message.setRecipients(RecipientType.CC, addressCc);
		message.setSubject("LimitedResync - "+ date1);

		// Create the message part
		BodyPart messageBodyPart = new MimeBodyPart();
		String msgContent = "Hi All,<br/><br/>PFA report for LimitedResync. <br/><br/>Thanks<br/>";
		msgContent = msgContent + "COM Multichannel Support";
		// Fill the message
		messageBodyPart.setContent(msgContent, "text/html");

		Multipart multipart = new MimeMultipart();
		multipart.addBodyPart(messageBodyPart);

		// Part two is attachment
		messageBodyPart = new MimeBodyPart();
		//String filename = "C:\\store\\LimitedResync.csv";
		String filename = "/opt/isv/apache-tomcat/temp/LimitedResync.csv";
		DataSource source = new FileDataSource(filename);
		messageBodyPart.setDataHandler(new DataHandler(source));
		messageBodyPart.setFileName(source.getName());
		multipart.addBodyPart(messageBodyPart);

		// Put parts in message
		message.setContent(multipart);

		// Send the message
		Transport.send(message);
		logger.debug("Msg Send ....");
	}

}
