package com.homedepot.di.dl.support.resyncSku.dao;
import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.homedepot.di.dl.support.util.Constants;
import com.homedepot.di.dl.support.util.DBConnectionUtil;

public class LimitedResync {
	final static Logger logger = Logger.getLogger(LimitedResync.class);
	/**
	 * @param args
	 * @throws SQLException 
	 * @throws IOException 
	 */
	
	
	public static void startLimitedResync() throws SQLException, IOException
	{

		// TODO Auto-generated method stub
		
		
		//String outputFile = "/opt/isv/tomcat-6.0.18/temp/comOrdersFuturePick.csv";

		
		
		HashMap <String, String> map = new HashMap<String, String>();
		//Connection sscConn = null;
		DBConnectionUtil dbConn = new DBConnectionUtil();		
		Connection sscConn = dbConn.getJNDIConnection(Constants.STERLING_SSC_RO_JNDI);
		
		Statement st = null;
		st = sscConn.createStatement();
		ResultSet result = null;
		
		String query= "SELECT yia.inventory_item_key,yia.item,ya.alias_value,yia.uom,yia.ship_node,yia.quantity,"
+" yia.on_hand_qty,yia.createuserid,yia.modifyuserid from thd01.yfs_inventory_audit yia,thd01.yfs_item_alias ya,thd01.yfs_item yi" 
+" where yia.on_hand_qty=0 and yia.modifyts > sysdate-2 and yia.segment = ' ' and yia.transaction_type ='SHIPMENT'" 
+" and yia.ship_node not like 'MVNDR%'"
//+" and yia.ship_node in ('0205','0207','0209','0210','0218','0222','0253','0255','0283','0284','0370','0526','0540','0555','0561','0565','0929','1034','1037','1202','1215','1512','1750','1919','1921','1922','2003','2009','3409','3488','3602','3620','3645','3801','4007','4025','4101','4105','4129','4409','6310','6325','6555','6593','6601','6688','6831','6923','8456','8519','8941')"
+" and yi.item_key=ya.item_key and yi.item_id=yia.item and ya.alias_name='SKU' order by yia.ship_node asc";
		
		
		logger.info(query);
		result = st.executeQuery(query);
		String skuList = null;
		String dupStore=null;
		ArrayList<ResyncObject> RList= new ArrayList<ResyncObject>();
		
		while(result.next())
			
		{   
			String store = result.getString(5).trim();
			String skuNumber = result.getString(3);
			//System.out.println(store);
			//System.out.println(skuNumber);
			if(store.equals(dupStore) || dupStore==null)
			{
				//System.out.println("Inside if");
				if(skuList!=null)
				{
				 skuList=skuList +","+ skuNumber;
				} else {
					skuList=skuNumber;
				}
				 dupStore=store;
				 //System.out.println("skuList"+skuList);
				// System.out.println("dupStore"+dupStore);
				 
			}else if(!store.equals(dupStore)){
				//System.out.println("Inside else if");
				ResyncObject StoreSku = new ResyncObject();
				
				StoreSku.setStore(dupStore);
				StoreSku.setSku(skuList);
				RList.add(StoreSku);
				//map.put(dupStore,skuList);
				dupStore=store;
				skuList=null;
				skuList=skuNumber;
			}
		}
			ResyncObject StoreSku = new ResyncObject();
		
			StoreSku.setStore(dupStore);
			StoreSku.setSku(skuList);
			RList.add(StoreSku);
				//map.put(dupStore, skuList);
			try
			{
			performLimitedResync(RList);
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
	
	}	
	
	
		
	public static String performLimitedResync(ArrayList<ResyncObject> RList) throws IOException

	{
		
		//String outputFile = "C:\\store\\LimitedResync.csv";
		String outputFile = "/opt/isv/apache-tomcat/temp/LimitedResync.csv";

		FileWriter fw = new FileWriter(outputFile, false);

		fw.append("Stores");
		fw.append(',');
		fw.append("Skus");
		fw.append('\n');
		
		String response=null;
		for(int i =0;i<RList.size();i++)
		{		
			ResyncObject robj= RList.get(i); 
			String Store=robj.getStore();
			String Sku=robj.getSku();
			//if(Sku.substring(0)== ",")
			//{
				//Sku=Sku.substring(1);
			//}
		String url = "http://st" + Store
				+ ".homedepot.com:12030/StoreCOMInventory/rs/limitedResync?skus="+Sku;

		//String headerValue = "COMSupport";

		//String headerName = "Referer";
		fw.append(Store);
		fw.append(',');	
		logger.info("URL:"+url);
		int TIMEOUT_VALUE = 1000;
		try{
					
		URL resumeTrans = new URL(url);
		URLConnection resumeTransConn = resumeTrans.openConnection();
		resumeTransConn.setConnectTimeout(TIMEOUT_VALUE);
		resumeTransConn.setReadTimeout(TIMEOUT_VALUE);
		BufferedReader in = new BufferedReader(
				new InputStreamReader(
						resumeTransConn.getInputStream()));
		String inputLine = "", output = "";
		while ((inputLine = in.readLine()) != null) {
			if (!(inputLine == null || ""
					.equalsIgnoreCase(inputLine))) {
				output = output + inputLine;
			}
		}
		in.close();
		
		
		logger.info("Store:"+Store+" Sku:"+Sku+":"+output);
		
		
			
		fw.append(Sku.replace(",",";"));	
		fw.append("\n");
}
		
		catch(Exception e)
		{
			logger.error("Unable to connect to store : " + Store);
			fw.append(Sku.replace(",",";"));
			fw.append(',');
			fw.append("Unable to connect to store");
			fw.append("\n");
		}
		
		
			
		}
		
		fw.flush();
		fw.close();
		return response;
		
		
		}

	
	}


