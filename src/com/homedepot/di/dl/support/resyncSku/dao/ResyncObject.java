package com.homedepot.di.dl.support.resyncSku.dao;

public class ResyncObject {

	String Sku;
	String Store;
	
	public String getSku() {
		return Sku;
	}
	public void setSku(String sku) {
		Sku = sku;
	}
	public String getStore() {
		return Store;
	}
	public void setStore(String store) {
		Store = store;
	}
}
