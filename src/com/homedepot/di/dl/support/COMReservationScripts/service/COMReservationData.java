package com.homedepot.di.dl.support.COMReservationScripts.service;

public class COMReservationData {

	public String getCOMReservationCBAttempts() {
		return COMReservationCBAttempts;
	}
	public void setCOMReservationCBAttempts(String cOMReservationCBAttempts) {
		COMReservationCBAttempts = cOMReservationCBAttempts;
	}
	public String getCOMReservationCBSkuNbr() {
		return COMReservationCBSkuNbr;
	}
	public void setCOMReservationCBSkuNbr(String cOMReservationCBSkuNbr) {
		COMReservationCBSkuNbr = cOMReservationCBSkuNbr;
	}
	public String getCOMReservationCBWaittime() {
		return COMReservationCBWaittime;
	}
	public void setCOMReservationCBWaittime(String cOMReservationCBWaittime) {
		COMReservationCBWaittime = cOMReservationCBWaittime;
	}
	public String getHDResvNonOfflineErrors() {
		return HDResvNonOfflineErrors;
	}
	public void setHDResvNonOfflineErrors(String hDResvNonOfflineErrors) {
		HDResvNonOfflineErrors = hDResvNonOfflineErrors;
	}
	public String getCOMWatermarkMode() {
		return COMWatermarkMode;
	}
	public void setCOMWatermarkMode(String cOMWatermarkMode) {
		COMWatermarkMode = cOMWatermarkMode;
	}
	public String getCOMReservationCBMode() {
		return COMReservationCBMode;
	}
	public void setCOMReservationCBMode(String cOMReservationCBMode) {
		COMReservationCBMode = cOMReservationCBMode;
	}
	public String getCOMReservationMode() {
		return COMReservationMode;
	}
	public void setCOMReservationMode(String cOMReservationMode) {
		COMReservationMode = cOMReservationMode;
	}
	public String getCOMRtiMode() {
		return COMRtiMode;
	}
	public void setCOMRtiMode(String cOMRtiMode) {
		COMRtiMode = cOMRtiMode;
	}
	public String getCOMRtiMaoId() {
		return COMRtiMaoId;
	}
	public void setCOMRtiMaoId(String cOMRtiMaoId) {
		COMRtiMaoId = cOMRtiMaoId;
	}
	private String COMReservationCBAttempts;
	private String COMReservationCBSkuNbr;
	private String COMReservationCBWaittime;
	private String HDResvNonOfflineErrors;
	private String COMWatermarkMode;
	private String COMReservationCBMode;
	private String COMReservationMode;
	private String COMRtiMode;
	private String COMRtiMaoId;
}
