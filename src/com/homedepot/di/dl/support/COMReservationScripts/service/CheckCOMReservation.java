package com.homedepot.di.dl.support.COMReservationScripts.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage.RecipientType;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;

public class CheckCOMReservation {

	private static final Logger logger = Logger
			.getLogger(CheckCOMReservation.class);
	private static final Logger oLogger = Logger.getLogger("splunkLog");

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		checkCOMReservationMode();
	}
	
	public static void checkCOMReservationMode() {

		String msgContent = "";
					logger.info("checking COMReservation svc : ");

					//check the COMReservation svc
					String url = "http://webapps.homedepot.com/COMReservation/service/control/getParameter?output=json";
					Client client = Client.create();
					WebResource webResource = client.resource(url);

					// you can map it to a pojo, no need to have a string or map
					String s = webResource.get(String.class);


					
					System.out.println(s);
					
					Gson gson = new Gson();
					COMReservationData comReservationData = gson.fromJson(s, COMReservationData.class);
					
					System.out.println("comReservationData.getCOMReservationMode()="+comReservationData.getCOMReservationMode());
					
					msgContent = "status of COMReservation Mode.<br/><br/>"
							+comReservationData.getCOMReservationMode() + "<br/><br/>"+
							"To view control params:" +
								" http://multichannel-custom.homedepot.com/COMReservation/service/control/"+"<br/><br/>"+
								 
								"To set COMReservationMode to Online:   https://multichannel-custom.homedepot.com/COMReservation/service/control/setParameter?name=COMReservationMode&value=ONLINE"
;

					if("OFFLINE".equals(comReservationData.getCOMReservationMode())){
						sendMail(msgContent,true); // Sending email report as critical
					}
					else if("SYNC".equals(comReservationData.getCOMReservationMode())){
						sendMail(msgContent,false);
					}

					oLogger.info("status of COMReservation Mode.");

				
			
	}



	public static void sendMail(String msgContent,boolean isHipri) {

		logger.info("checkCOMReservationMode: Preparing to send email");
		final String user = "horizon@cpliisad.homedepot.com";
		final String password = "xxx";// change accordingly
		String host = "mail1.homedepot.com";
		Properties props = new Properties();
		props.put("mail.host", host);
		
		String sub = "WARN";
		String  pri = "3";
		
		if(isHipri)
		{
			sub = "CRITICAL";
			pri="1";
		}
		
		Session session = Session.getInstance(props,
				new javax.mail.Authenticator() {
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(user, password);
					}
				});
		try {

			String[] Cc = {};
			String[] to = { "_2fc77b@homedepot.com" };
			//String[] to = { "manoj_sadangi@homedepot.com" };
			// String[] Cc = {"manoj_sadangi@homedepot.com"};
			MimeMessage message = new MimeMessage(session);
			message.setHeader("X-Priority", pri);
			message.setFrom(new InternetAddress(user));
			DateFormat dateFormat = new SimpleDateFormat(
					"MM/dd/yyyy hh:mm:ss a");
			Date today = new Date();
			String date1 = dateFormat.format(today);
			message.setSubject("CRITICAL  COMReservation Mode " + date1);
			// message.setText("This is simple program of sending email using JavaMail API");
			message.setFrom(new InternetAddress(user));
			InternetAddress[] addressTo = new InternetAddress[to.length];
			InternetAddress[] addressCc = new InternetAddress[Cc.length];
			for (int i = 0; i < to.length; i++) {
				addressTo[i] = new InternetAddress(to[i]);
				logger.warn("To Address " + to[i]);
			}
			for (int j = 0; j < Cc.length; j++) {
				addressCc[j] = new InternetAddress(Cc[j]);
				logger.warn("Cc Address " + Cc[j]);
			}
			message.setRecipients(RecipientType.TO, addressTo);
			message.setRecipients(RecipientType.CC, addressCc);
			message.setContent(msgContent, "text/html");
			Transport.send(message);
			for (int i = 0; i < to.length; i++) {
				addressTo[i] = new InternetAddress(to[i]);
				logger.warn("Message delivered to--- " + to[i]);
			}
			for (int j = 0; j < Cc.length; j++) {
				addressCc[j] = new InternetAddress(Cc[j]);
				logger.warn("Message delivered to cc recepients--- " + Cc[j]);
			}

		} catch (MessagingException e) {
			e.printStackTrace();
		}

	}

	
}
