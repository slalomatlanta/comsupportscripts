package com.homedepot.di.dl.support.rtamstat.dao;


import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.log4j.Logger;

import com.homedepot.di.dl.support.deletekeys.util.Constants;
import com.homedepot.di.dl.support.util.DBConnectionUtil;


public class RTAMStatisticsDAO {

	/**
	 * @param args
	 */
	private static final Logger logger = Logger
			.getLogger(RTAMStatisticsDAO.class);
	
	public static void main(String[] args) throws SQLException,
			ClassNotFoundException, IOException {
		StringBuilder emailText = new StringBuilder();
		emailText = getRTAMMetrics();
		logger.info(emailText.toString());

	}

	public static StringBuilder getRTAMMetrics() throws SQLException,
			ClassNotFoundException, MalformedURLException {
		// public static ArrayList<StringBuilder> getS2SMetrics() throws
		// SQLException, ClassNotFoundException{
		Connection con = null;
		ResultSet rs = null;
		Statement stmt = null;
//		Calendar cal = Calendar.getInstance();
//		DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
//		cal.add(Calendar.DATE, -7);

		// Test
		
		StringBuilder emailText = new StringBuilder();
		StringBuilder startText = new StringBuilder();
		String eMailSubject = null;
		startText.append("<html><body><table>");
		startText.append("<font face=\"calibri\"> <tr>");
		startText.append("<p>Hello All,</p>");
		startText.append("<p>	Below are the hourly RTAM statistics.</p><br></tr>");
		// startText.append(System.getProperty("line.separator"));

		try {
			// Getting the DB connection properties from propeties file. /opt/isv/tomcat-6.0.18/webapps/ConnProperties 	
//			File file = new File("C:\\temp\\ConnProperties");
//			File file = new File("/opt/isv/tomcat-6.0.18/webapps/ConnProperties");  
//			URL[] urls = {file.toURI().toURL()};   
//			ClassLoader loader = new URLClassLoader(urls);
//			ResourceBundle bundle = ResourceBundle.getBundle("DBConnection", Locale.getDefault(), loader);
			
//			final String comDriverClass = bundle.getString("oracleDriverClass");
//			final String connectionURL = bundle.getString("sterlingConnectionURL_SSC");
//			final String uName = bundle.getString("UserID");
//			final String uPassword = bundle.getString("UserPassword");
			
			final String driverClass = "oracle.jdbc.driver.OracleDriver";
			
			Class.forName(driverClass);
			String STERLING_SSC_RO_JNDI = Constants.STERLING_SSC_RO_JNDI;
			DBConnectionUtil db = new DBConnectionUtil();
			con = db.getJNDIConnection(STERLING_SSC_RO_JNDI);;
			stmt = con.createStatement();
			String query = "with Avgstat as(" +
					"select hostname, min(start_time_stamp) start_ts, max(end_time_stamp) end_ts, cast(avg(Average) as decimal(10,2)) Average from (" +
					"select i.hostname, i.start_time_stamp, i.end_time_stamp, i.statistic_value as Average " +
					"from THD01.yfs_statistics_detail i where i.server_name = 'HDRTAMAgent' and " +
					"to_char(start_time_stamp,'YYYYMMDDHH24') = to_char((sysdate-1/24),'YYYYMMDDHH24') and " +
					"i.statistic_name in ('Average') " +
					" order by i.start_time_stamp) group by hostname), " +
					"Maxstat as( " +
					"select hostname, min(start_time_stamp), max(end_time_stamp), cast(avg(Maximum) as decimal(10,2)) Maximum from (" +
					"select i.hostname, i.start_time_stamp, i.end_time_stamp, i.statistic_value as Maximum " +
					"from THD01.yfs_statistics_detail i where i.server_name = 'HDRTAMAgent' and " +
					"to_char(start_time_stamp,'YYYYMMDDHH24') = to_char((sysdate-1/24),'YYYYMMDDHH24') and " +
					"i.statistic_name in ('Maximum') " +
					" order by i.start_time_stamp) group by hostname), " +
					"Minstat as( " +
					"select hostname, min(start_time_stamp), max(end_time_stamp), cast(avg(Minimum) as decimal(10,2)) Minimum from (" +
					"select i.hostname, i.start_time_stamp, i.end_time_stamp, i.statistic_value as Minimum " +
					"from THD01.yfs_statistics_detail i where i.server_name = 'HDRTAMAgent' and " +
					"to_char(start_time_stamp,'YYYYMMDDHH24') = to_char((sysdate-1/24),'YYYYMMDDHH24') and " +
					"i.statistic_name in ('Minimum') " +
					" order by i.start_time_stamp) group by hostname) " +
					"select t.hostname, t.start_ts ,t.end_ts,e.Minimum,b.Maximum,t.Average from Avgstat t, Maxstat b,Minstat e " +
					"where t.hostname=b.hostname and t.hostname=e.hostname order by t.hostname";
//			logger.info("RTAM Statistics query : "+query);
			
			
			rs = stmt.executeQuery(query);

			emailText.append("<tr><table border=\"1\">\n");
			emailText.append("<font face=\"calibri\">");

			if (rs.next()) {

				emailText.append("<tr bgcolor=\"#768976\">");
				emailText.append("<th>");
				emailText.append("Server Name");
				emailText.append("</th>");
				emailText.append("<th>");
				emailText.append("Start Time");
				emailText.append("</th>");
				emailText.append("<th>");
				emailText.append("End Time");
				emailText.append("</th>");
				emailText.append("<th>");
				emailText.append("Minimum");
				emailText.append("</th>");
				emailText.append("<th>");
				emailText.append("Maximum");
				emailText.append("</th>");
				emailText.append("<th>");
				emailText.append("Average");
				emailText.append("</th>");
				emailText.append("</tr>");
				emailText.append('\n');
				do {
					emailText.append("<font face=\"calibri\">");
					emailText.append("<tr>");
					emailText.append("<td>");
					String server = rs.getString(1);
					emailText.append(server);
					emailText.append("</td>");
					emailText.append("<td>");
					emailText.append(rs.getString(2));
					emailText.append("</td>");
					emailText.append("<td>");
					emailText.append(rs.getString(3));
					emailText.append("</td>");
					emailText.append("<td>");
					String min = rs.getString(4);
					emailText.append(min);
					emailText.append("</td>");
					emailText.append("<td>");
					String max = rs.getString(5);
					emailText.append(max);
					emailText.append("</td>");
					emailText.append("<td>");
					String avg = rs.getString(6);
					emailText.append(avg);
					emailText.append("</td>");
					emailText.append("</tr></tr>");
					
//					sendDataToGraphana(server, min, max, avg);

				} while (rs.next());
			}
			emailText.append("</table>");

//			emailText.append("<p>&nbsp;</p>");
//			emailText.append('\n');
			
			//Adding unprocessed inv transactions from file
//			String invQuery = "select count(*) from THD01.yfs_inventory_activity where processed_flag = 'N' and modifyts > (sysdate-1) and modifyts < (sysdate-5/1440)";
//			stmt = con.createStatement();
//			rs = stmt.executeQuery(invQuery);
//			int unProcessedInvcount = 0;
//			while (rs.next()){
//				unProcessedInvcount = Integer.parseInt(rs.getString(1));
//				emailText.append("Unprocessed inventory count : "+unProcessedInvcount);
//				if(unProcessedInvcount > 5000){
//					emailText.append('\n');
//					emailText.append("<p>&nbsp;</p>");
//					emailText.append('\n');
//					emailText.append("CRITICAL ALERT: Unprocessed inventory count in YFS_INVENTORY_ALERTS table is > 5000");
//					emailText.append("<p>&nbsp;</p>");
//					emailText.append('\n');
//					emailText.append("This email is being sent with High Priority...");
//					emailText.append('\n');
//				}
//				sendUnprocessedCountToGraphana(rs.getString(1));
//			}
			//End of Adding unprocessed inv transactions from file
//			emailText.append("<p>&nbsp;</p>");
//			emailText.append('\n');
			//Adding check to the get the modified record count in YFS_INVENTORY_ALERTS
//			String alertsQuery = "select count(*) from THD01.YFS_INVENTORY_ALERTS where modifyts > sysdate-1/48";
//			String alertsCount = null;
//			stmt = con.createStatement();
//			rs = stmt.executeQuery(alertsQuery);
//			while (rs.next()){
//				alertsCount = rs.getString(1);
////				emailText.append("YFS_INVENTORY_ALERTS table has been moved to Cassandra database.");
//				emailText.append("Count of records modified in YFS_INVENTORY_ALERTS table during last 30 mins : "+alertsCount);
//				if(Integer.parseInt(alertsCount) == 0){
//					emailText.append('\n');
//					emailText.append("<p>&nbsp;</p>");
//					emailText.append('\n');
//					emailText.append("No records got updated in YFS_INVENTORY_ALERTS table during last 30 mins. Start a call immediately!");
//					emailText.append("<p>&nbsp;</p>");
//					emailText.append('\n');
//					emailText.append("This email is being sent with High Priority...");
//					emailText.append('\n');
//				}
//			}
			//End of Adding check to the get the modified record count in YFS_INVENTORY_ALERTS
			
			stmt.close();
			rs.close();
			con.close();
			
			emailText.append("<p>&nbsp;</p>");
			emailText.append('\n');
			// emailText.append('\n');
			emailText.append("<tr><p>Thanks,</p></tr>");
			// emailText.append('\n');
			emailText.append("<tr><p>COM Multi-Channel Support</p><br></tr>");
			// emailText.append(System.getProperty("line.separator"));
			emailText.append(System.getProperty("line.separator"));
			emailText
					.append("<p>Note: This is a system-generated e-mail. Please do NOT reply to it.</p><br>");
			emailText.append("</table></body>");
			emailText.append("</html>");
			// finalList.add(sb);
		} catch(Exception e){
			logger.info(e.getStackTrace());
//			stmt.close();
//			rs.close();
//			if(!con.isClosed()){
//				con.close();
//			}
			
		}finally {
			
		}
		return startText.append(emailText);
		// return sb;
		// return finalList;
	}
	
	public static void sendDataToGraphana(String server, String min, String max, String avg) {
		File wd = new File("/home/tomcat/temp");
		Process proc = null;

		try {
			
			String serverName = (server.trim()).substring(0, 8);
			String minimum = "echo \"prod.retail.com.sterling.rtam."+serverName+".hourly.count.min "+min+"\" `date +%s` | nc graphite.homedepot.com 2003";
			String maximum = "echo \"prod.retail.com.sterling.rtam."+serverName+".hourly.count.max "+max+"\" `date +%s` | nc graphite.homedepot.com 2003";
			String average = "echo \"prod.retail.com.sterling.rtam."+serverName+".hourly.count.avg "+avg+"\" `date +%s` | nc graphite.homedepot.com 2003";

			proc = Runtime.getRuntime().exec(minimum, null, wd);
			proc = Runtime.getRuntime().exec(maximum, null, wd);
			proc = Runtime.getRuntime().exec(average, null, wd);
			/*logger.info(minimum);
			logger.info(maximum);
			logger.info(average);*/
			//logger.info("sendDataToGraphana() completed");
		} catch (Exception e) {
			logger.info("RTAM Statistics - sendDataToGraphana() : "+e);

		}
	}
	
	public static void sendUnprocessedCountToGraphana(String value) {
		File wd = new File("/home/tomcat/temp");
		Process proc = null;

		try {
			String command = "echo \"prod.retail.com.sterling.unprocessed_inv.hourly.count "+value+"\" `date +%s` | nc graphite.homedepot.com 2003";

			proc = Runtime.getRuntime().exec(command, null, wd);
			//logger.info(command);
			//logger.info("sendDataToGraphana() completed");
		} catch (Exception e) {
			logger.info("RTAM Statistics - sendUnprocessedCountToGraphana() : "+e);

		}
	}
	
//	private static Connection getJNDIConnection(String contextName) {
//		String DATASOURCE_CONTEXT = contextName;
//
//		Connection conn = null;
//		try {
//			Context initialContext = new InitialContext();
//			Context envContext = (Context) initialContext
//					.lookup("java:comp/env");
//
//			DataSource datasource = (DataSource) envContext
//					.lookup(DATASOURCE_CONTEXT);
//			if (datasource != null) {
//				conn = datasource.getConnection();
//				logger.info("Connected: " + conn);
//			} else {
//				logger.info("Failed to lookup datasource.");
//			}
//		} catch (NamingException ex) {
//			logger.info("Cannot get connection: " + ex);
//		} catch (SQLException ex) {
//			logger.info("Cannot get connection: " + ex);
//		}
//		return conn;
//	}
	
}
