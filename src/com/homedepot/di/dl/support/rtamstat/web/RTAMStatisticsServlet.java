package com.homedepot.di.dl.support.rtamstat.web;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.homedepot.di.dl.support.rtamstat.dao.RTAMStatisticsDAO;
import org.apache.log4j.Logger;

/**
 * Servlet implementation class RTAMStatisticsServlet
 */
public class RTAMStatisticsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	final static Logger logger = Logger.getLogger(RTAMStatisticsServlet.class);
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RTAMStatisticsServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		// TODO Auto-generated method stub

		// TODO Auto-generated method stub
		// ArrayList<StringBuilder> finalList = new ArrayList<StringBuilder>();
		StringBuilder text = new StringBuilder();
		String eMailSubject = null;
		RTAMStatisticsDAO dao = new RTAMStatisticsDAO();
		eMailSubject = "Hourly RTAM Statistics ";

		String emailRecipient = "_2fc77b@homedepot.com"; //COM Multichannel DL
//		String emailRecipient = "mahesh_vyakaranam@Homedepot.com";//custom recipient
		
		Session session = null;
		MimeMessage mimeMessage = null;
		MimeBodyPart mimeBodyPart = null;
		Multipart multiPart = null;
		String date = null;
		Calendar calendar = null;
		Properties properties = null;

		String msg = null;
		response.setContentType("text/html");
		PrintWriter out = null;
		
		
//		File file = new File("C:\\temp\\ConnProperties");
//		File file = new File("/opt/isv/tomcat-6.0.18/webapps/ConnProperties");
//		URL[] urls = {file.toURI().toURL()};   
//		ClassLoader loader = new URLClassLoader(urls);
//		ResourceBundle bundle = ResourceBundle.getBundle("DBConnection", Locale.getDefault(), loader);

		try {
			msg = "Success";
			text = dao.getRTAMMetrics();
			// finalList = dao.getS2SMetrics();
			calendar = Calendar.getInstance();
			date = calendar.getTime().toString();
			properties = System.getProperties();
			properties.put("mail.host", "mail1.homedepot.com");
			session = Session.getInstance(properties, null);
			mimeMessage = new MimeMessage(session);
			mimeMessage.setFrom(new InternetAddress(
					"horizon@cpliisad.homedepot.com"));
			mimeMessage.setRecipients(Message.RecipientType.TO,
					InternetAddress.parse(emailRecipient, false));
			mimeMessage.setSubject(eMailSubject + date);
			mimeBodyPart = new MimeBodyPart();
			mimeBodyPart.setText(text.toString());
			multiPart = new MimeMultipart();
			multiPart.addBodyPart(mimeBodyPart);
			// multiPart.addBodyPart(mbp1);
			// mimeMessage.setText(text.toString(), "utf-8", "html");
			mimeMessage.setContent(text.toString(), "text/html");
			// mimeMessage.setContent(multiPart);
			if(text.length() > 0){
				if(text.toString().contains("High Priority")){
					mimeMessage.setHeader("X-Priority", "1");
				}
				else{
					mimeMessage.setHeader("X-Priority", "3");
				}
			}
			
			Transport.send(mimeMessage);

			/*
			 * Write the HTML to the response
			 */
		} catch (Exception e) {
			msg = "Failure";
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		out = response.getWriter();
		out.println(msg);
		out.close();
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
