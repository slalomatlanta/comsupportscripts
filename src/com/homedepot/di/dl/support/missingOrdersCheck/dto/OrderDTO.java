package com.homedepot.di.dl.support.missingOrdersCheck.dto;

public class OrderDTO {
	private String store;
	private String order;
	private String legacyOrder;
	private String salesDate;
	private String register;
	private String posTransID;
	private String posStgTransID;
	private String paymentType;
	private String cordSrcPrcss;
	private String sumPaymentAmt;
	private String sumPosTransId;
	
	
	
	public String getSumPaymentAmt() {
		return sumPaymentAmt;
	}

	public void setSumPaymentAmt(String sumPaymentAmt) {
		this.sumPaymentAmt = sumPaymentAmt;
	}

	public String getSumPosTransId() {
		return sumPosTransId;
	}

	public void setSumPosTransId(String sumPosTransId) {
		this.sumPosTransId = sumPosTransId;
	}

	public String getCordSrcPrcss() {
		return cordSrcPrcss;
	}

	public void setCordSrcPrcss(String cordSrcPrcss) {
		this.cordSrcPrcss = cordSrcPrcss;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public String getStore() {
		return store;
	}

	public void setStore(String store) {
		this.store = store;
	}

	public String getOrder() {
		return order;
	}

	public void setOrder(String order) {
		this.order = order;
	}

	public String getLegacyOrder() {
		return legacyOrder;
	}

	public void setLegacyOrder(String legacyOrder) {
		this.legacyOrder = legacyOrder;
	}

	public String getSalesDate() {
		return salesDate;
	}

	public void setSalesDate(String salesDate) {
		this.salesDate = salesDate;
	}

	public String getRegister() {
		return register;
	}

	public void setRegister(String register) {
		this.register = register;
	}

	public String getPosTransID() {
		return posTransID;
	}

	public void setPosTransID(String posTransID) {
		this.posTransID = posTransID;
	}

	public String getPosStgTransID() {
		return posStgTransID;
	}

	public void setPosStgTransID(String posStgTransID) {
		this.posStgTransID = posStgTransID;
	}
}
