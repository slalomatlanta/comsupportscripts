package com.homedepot.di.dl.support.missingOrdersCheck.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.homedepot.di.dl.support.insertHDEventsAndServiceValidation.service.InsertHDEventsAndUpdateHDPSI;
import com.homedepot.di.dl.support.missingOrdersCheck.dao.CheckMissingOrders;

/**
 * Servlet implementation class MissingOrdersCheckServlet
 */
public class MissingOrdersCheckServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	final static Logger logger = Logger.getLogger(MissingOrdersCheckServlet.class);
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MissingOrdersCheckServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		logger.info("CheckMissingOrders program started at : "
				+ java.util.GregorianCalendar.getInstance().getTime());
		CheckMissingOrders.getOrders();
		logger.info("CheckMissingOrders program completed at : "
				+ java.util.GregorianCalendar.getInstance().getTime());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
