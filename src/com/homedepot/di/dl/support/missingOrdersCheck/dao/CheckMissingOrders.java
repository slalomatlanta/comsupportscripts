package com.homedepot.di.dl.support.missingOrdersCheck.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;

import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage.RecipientType;

import org.apache.log4j.Logger;

import com.homedepot.di.dl.support.missingOrdersCheck.dto.OrderDTO;
import com.homedepot.di.dl.support.switchToNormal.dao.SwitchTransmissionState;
import com.homedepot.di.dl.support.util.CipherEncrypter;
import com.homedepot.di.dl.support.util.Constants;
import com.homedepot.di.dl.support.util.DBConnectionUtil;

public class CheckMissingOrders {

	final static Logger logger = Logger.getLogger(CheckMissingOrders.class);

	public static void main(String args[]) {
		logger.info("CheckMissingOrders program started at : "
				+ java.util.GregorianCalendar.getInstance().getTime());
		getOrders();
		logger.info("CheckMissingOrders program completed at : "
				+ java.util.GregorianCalendar.getInstance().getTime());
	}

	public static void getOrders() {
		ArrayList<OrderDTO> PR6Data = new ArrayList<OrderDTO>();
		ArrayList<OrderDTO> sterlingData = new ArrayList<OrderDTO>();
		ArrayList<OrderDTO> finalList = new ArrayList<OrderDTO>();
		ArrayList<OrderDTO> ordersPresentInSterling = new ArrayList<OrderDTO>();
		ArrayList<OrderDTO> ordersNotPresentInSterling = new ArrayList<OrderDTO>();
		ArrayList<OrderDTO> ordersNewPOSIssue = new ArrayList<OrderDTO>();
		PR6Data = getDataFromPR6();
		sterlingData = getDataFromSterling();
		logger.info("No of rows fetched from PR6: " + PR6Data.size());
		logger.info("No of rows fetched from Sterling: " + sterlingData.size());
		logger.info("Comparing both the results . . .");
		for (OrderDTO PR6Row : PR6Data) {
			boolean orderPresent = false;
			for (OrderDTO sterlingRow : sterlingData) {
				if ((PR6Row.getOrder().equals(sterlingRow.getOrder()))
						// && PR6Row.getStore().equals(sterlingRow.getStore())
						&& PR6Row.getSalesDate().equals(
								sterlingRow.getSalesDate())
						&& PR6Row.getPosTransID().equals(
								sterlingRow.getPosTransID())
						// && PR6Row.getPosStgTransID().equals(
						// sterlingRow.getPosStgTransID())
						&& PR6Row.getRegister().equals(
								sterlingRow.getRegister())) {
					orderPresent = true;
					// PR6Row.setPaymentType(sterlingRow.getPaymentType());
				}
			}
			if (!orderPresent) {
				finalList.add(PR6Row);
			}
		}
		for (OrderDTO od : finalList) {
			boolean orderPresent = false;
			for (OrderDTO sterlingRow : sterlingData) {
				if (od.getOrder().equals(sterlingRow.getOrder())) {
					orderPresent = true;
				}
			}
			if (!orderPresent) {
				if (checkOrderPresentInSterlingDB(od.getOrder())) {
					if ((od.getSumPosTransId() == null)
							|| (od.getSumPosTransId().equals(""))
							|| (od.getSumPosTransId().equals("0"))) {
						ordersPresentInSterling.add(od);
					} else {
						ordersNewPOSIssue.add(od);
					}
				} else {
					ordersNotPresentInSterling.add(od);
				}
			} else {
				if ((od.getSumPosTransId() == null)
						|| (od.getSumPosTransId().equals(""))
						|| (od.getSumPosTransId().equals("0"))) {
					ordersPresentInSterling.add(od);
				} else {
					ordersNewPOSIssue.add(od);
				}
			}
		}

		String tableHeader = "", table1 = "", table2 = "", body = "", mailContent = "", table3 = "";
		body = "<html><head><body><font face=\"calibri\"><h3>Order Create missing in Sterling</h3>";
		tableHeader = "<table border=\"1\" cellpadding=\"0\" cellspacing=\"0\" width=\"200px\" style=\"border-collapse:collapse;\">"
				+ "<font face=\"calibri\">"
				+ "<tr>"
				+ "<th>Store Number</th>"
				+ "<th>Order Number</th>"
				+ "<th>Legacy Order</th>"
				+ "<th>Sales Date</th>"
				+ "<th>Register</th>"
				+ "<th>POS Trans ID</th>"
				+ "<th>POS STG Trans ID</th>"
				+ "<th>cord_src_prcss_cd</th>"
				+ "<th>Sum of Payments</th>"
				+ "<th>Sum of POS Trans ID</th>" + "</tr>";
		for (OrderDTO od : ordersNotPresentInSterling) {
			table1 = table1 + "<tr>" + "<td>" + od.getStore() + "</td>"
					+ "<td>" + od.getOrder() + "</td>" + "<td>"
					+ od.getLegacyOrder() + "</td>" + "<td>"
					+ od.getSalesDate() + "</td>" + "<td>" + od.getRegister()
					+ "</td>" + "<td>" + od.getPosTransID() + "</td>" + "<td>"
					+ od.getPosStgTransID() + "</td>" + "<td>"
					+ od.getCordSrcPrcss() + "</td>" + "<td>"
					+ od.getSumPaymentAmt() + "</td>" + "<td>"
					+ od.getSumPosTransId() + "</td> </tr>";
		}
		table1 = table1 + "</table>";
		for (OrderDTO od : ordersPresentInSterling) {
			table2 = table2 + "<tr>" + "<td>" + od.getStore() + "</td>"
					+ "<td>" + od.getOrder() + "</td>" + "<td>"
					+ od.getLegacyOrder() + "</td>" + "<td>"
					+ od.getSalesDate() + "</td>" + "<td>" + od.getRegister()
					+ "</td>" + "<td>" + od.getPosTransID() + "</td>" + "<td>"
					+ od.getPosStgTransID() + "</td>" + "<td>"
					+ od.getCordSrcPrcss() + "</td>" + "<td>"
					+ od.getSumPaymentAmt() + "</td>" + "<td>"
					+ od.getSumPosTransId() + "</td> </tr>";
		}
		table2 = table2 + "</table>";
		// Added newly for New POS issue
		for (OrderDTO od : ordersNewPOSIssue) {
			table3 = table3 + "<tr>" + "<td>" + od.getStore() + "</td>"
					+ "<td>" + od.getOrder() + "</td>" + "<td>"
					+ od.getLegacyOrder() + "</td>" + "<td>"
					+ od.getSalesDate() + "</td>" + "<td>" + od.getRegister()
					+ "</td>" + "<td>" + od.getPosTransID() + "</td>" + "<td>"
					+ od.getPosStgTransID() + "</td>" + "<td>"
					+ od.getCordSrcPrcss() + "</td>" + "<td>"
					+ od.getSumPaymentAmt() + "</td>" + "<td>"
					+ od.getSumPosTransId() + "</td> </tr>";
		}
		table3 = table3 + "</table>";

		mailContent = body
				+ tableHeader
				+ table1
				+ "<br/>"
				+ "<h3>Payments missing in Sterling</h3>"
				+ tableHeader
				+ table2
				+ "<br/>"
				+ "<h3>New POS Issue</h3>"
				+ tableHeader
				+ table3
				+ "<p><i>Note: This is system generated mail. Please do not respond to this.</i></p>"
				+ "</body></head></html>";
		// System.out.println(mailContent);
		// System.out.println("Final Output builded");
		// if ((ordersNotPresentInSterling.size() > 0)
		// || (ordersPresentInSterling.size() > 0)) {
		sendMail(mailContent); // Sending Mail
		// }
	}

	public static ArrayList<OrderDTO> getDataFromPR6() {
		ArrayList<OrderDTO> PR6Data = new ArrayList<OrderDTO>();
		final String driverClass = "com.ibm.db2.jcc.DB2Driver";
		final String connectionURLThin = "jdbc:db2://ibdyprw0.sysplex.homedepot.com:5142/PR6";
		final String hostUserId = "cprjs";
		CipherEncrypter encrypter = new CipherEncrypter("PR6_Rakesh"); // key
																		// for
																		// encryption

		final String hostPassword = encrypter.decrypt("eARtAoCHyLc=");
		//final String hostPassword = "2z5382z1";

		Connection con = null;
		Statement stmt = null;
		ResultSet rset = null;
		String query = null;
		try {
			Class.forName(driverClass);
			con = DriverManager.getConnection(connectionURLThin, hostUserId,
					hostPassword);
			stmt = con.createStatement();
			DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DATE, -1);
			String yesterday = dateFormat.format(cal.getTime());
//			ArrayList<String> YModeStoreList = getYModeStores();
//			String storeList = "";
//			for (String store : YModeStoreList) {
//				storeList = storeList + "'" + store + "',";
//			}
//			storeList = storeList.substring(0, storeList.length() - 1);
			// System.out.println(storeList);
			query = "select rsm.str_nbr, rsmx.extnl_ref_nbr, rsm.cust_ord_nbr, "
					+ " rsm.sls_dt, rsm.rgstr_nbr, rsm.pos_trans_id, rsm.pos_stg_trans_id, rsmx.cord_src_prcss_cd, "
					+ " sum(ta.paymt_amt * -1), sum(pmt.pos_trans_id) "

					+ " from prhdw.posj_cosrc_rsm_x rsmx, prhdw.pos_jrnl_rsm rsm, prhdw.posj_tndr_alloc ta, prhdw.pos_jrnl pj "
					+ " LEFT OUTER JOIN prhdw.pos_jrnl_paymt_v pmt ON pmt.str_nbr = pj.str_nbr "
					+ " and pmt.sls_dt = pj.sls_dt "
					+ " and pmt.rgstr_nbr = pj.rgstr_nbr "
					+ " and pmt.pos_trans_id = pj.pos_trans_id "
					+ " and pmt.pos_seq_nbr = 0 "
					+ " where rsmx.extnl_ref_nbr like 'H%' "
					+ "and rsmx.sls_dt = '"
					+ yesterday
					+ "' "
					/*+" and rsmx.sls_dt      >= '09/02/2016' "
					+" and rsmx.sls_dt      < '09/03/2016' "*/
					+ " and rsmx.str_nbr      = rsm.str_nbr "
					+ " and rsmx.sls_dt       = rsm.sls_dt "
					+ " and rsmx.rgstr_nbr    = rsm.rgstr_nbr "
					+ " and rsmx.pos_trans_id = rsm.pos_trans_id "
					+ " and rsmx.pos_seq_nbr  = rsm.pos_seq_nbr "
					+ " and rsm.str_nbr       = pj.str_nbr "
					+ " and rsm.sls_dt        = pj.sls_dt "
					+ " and rsm.rgstr_nbr     = pj.rgstr_nbr "
					+ " and rsm.pos_trans_id  = pj.pos_trans_id "
					+ " and pj.void_flg       = 'N' "

					+ " AND ta.str_nbr        =  rsm.str_nbr "
					+ " AND ta.sls_dt         =   rsm.sls_dt "
					+ " AND ta.rgstr_nbr      =   rsm.rgstr_nbr "
					+ " AND ta.pos_trans_id   =   rsm.pos_trans_id "
					+ " AND ta.rsm_seq_nbr    =   rsm.pos_seq_nbr "
//					+ "and rsm.str_nbr in ("
//					+ storeList
//					+ ") "
					+ " group by rsm.str_nbr, rsmx.extnl_ref_nbr, rsm.cust_ord_nbr, "
					+ " rsm.sls_dt, rsm.rgstr_nbr, rsm.pos_trans_id, rsm.pos_stg_trans_id, rsmx.cord_src_prcss_cd "

					+ " order by 10 desc";

			logger.info("CheckMissingOrders: Executing PR6 query . . ." + query);
			rset = stmt.executeQuery(query);
			logger.info("Looping through results");
			while (rset.next()) {
				OrderDTO od = new OrderDTO();
				od.setStore(rset.getString(1).trim());
				od.setOrder(rset.getString(2).trim());
				od.setLegacyOrder(rset.getString(3).trim());
				od.setSalesDate(rset.getString(4).trim());
				od.setRegister(rset.getString(5).trim());
				od.setPosTransID(rset.getString(6).trim());
				String posStgTransID = rset.getString(7).trim();
				od.setCordSrcPrcss(rset.getString(8).trim());
				if (!posStgTransID.equals("")) {
					if (posStgTransID.substring(0, 2).equalsIgnoreCase("01")) {
						od.setPosStgTransID(posStgTransID);
						// PR6Data.add(od);
					}
				} else {
					od.setPosStgTransID(posStgTransID);
					// PR6Data.add(od);
				}
				od.setSumPaymentAmt(rset.getString(9).trim());
				if (null == rset.getString(10)) {
					od.setSumPosTransId("");
				} else {
					od.setSumPosTransId(rset.getString(10).trim());
				}
				PR6Data.add(od);
			}

			rset.close();
			stmt.close();
			con.close();
		} catch (Exception e) {
			logger.error("Exception occured: " + e);
			e.printStackTrace();
		}
		return PR6Data;
	}

	public static ArrayList<OrderDTO> getDataFromSterling() {
		ArrayList<OrderDTO> sterlingData = new ArrayList<OrderDTO>();
		// Connection con = null;
		Statement stmt = null;
		ResultSet rset = null;
		try {
			// final String driverClass = "oracle.jdbc.driver.OracleDriver";
			// final String connectionURL =
			// "jdbc:oracle:thin:@pprmm78x.homedepot.com:1521/DPR78MM_RO_CODS_01";
			// final String uName = "CODSRO01";
			// final String uPassword = "juspu6Ew";
			// Class.forName(driverClass);
			// con = DriverManager.getConnection(connectionURL, uName,
			// uPassword);
			DBConnectionUtil dbConn = new DBConnectionUtil();
			Connection con = dbConn
					.getJNDIConnection(Constants.ODS_STAND_BY_JNDI);
			stmt = con.createStatement();
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			Calendar cal = Calendar.getInstance();
			String today = dateFormat.format(cal.getTime());
			cal.add(Calendar.DATE, -1);
			String yesterday = dateFormat.format(cal.getTime());
			String query = "select hdr.seller_organization_code, hdr.extn_host_order_ref, hdr.extn_legacy_ord_ref, cc.tran_return_code, cc.reference1, yp.PAYMENT_TYPE "
					+ "from yfs_order_header hdr, yfs_charge_transaction pmt, yfs_credit_card_transaction cc, yfs_payment yp "
					+ "where extn_host_order_ref like 'H%' "
					+ "and hdr.order_header_key = pmt.order_header_key "
					+ "and pmt.charge_transaction_key = cc.charge_transaction_key "
					+ "and yp.PAYMENT_KEY = pmt.PAYMENT_KEY "
					+ "and hdr.document_type = '0001' "
					// + "and hdr.EXTN_HOST_ORDER_REF = 'H1806-26314' "
					// + "and hdr.ORDER_HEADER_KEY > = '20151021' "
					+ "and reference1 like '%" + yesterday + "%' "
					// +
					// "and (reference1 like '%2016-04-11%' OR reference1 like '%2016-04-12%' OR reference1 like '%2016-04-13%')"
					// + "and (reference1 not like '%2016-02-15%' )"
					//+ "and (reference1 like '%2016-09-02%' )"
					// +
					// "and hdr.seller_organization_code in ('0179','0359','0406','0458','0468','0472','0475','0483','1805','3302','8519','0367','0404','3322','0151','3901','3902','3907','3908','0413','0447','0455','0459','1405','1807','6861','0373','0385','3306','3318','3324','2901','6528','6564','6940','0365','0422','0471','0526','1401','1407','1804','1809','4929','0375','0401','0139','3904','6521','6941','8924','0122','0132','0356','0358','0415','0489','0516','1412','3915','6577','6584','0349','0364','0420','0424','0441','0453','0456','0469','0476','1801','6948','0371','0386','0405','3307','3315','3911','3913','3917','8584','8922','0352','0357','0477','0485','2910','3301','0370','0376','0389','3308','4025','6835','6942','0133','0360','0362','0411','0417','0464','0473','1749','6972','2915','3303','6862','0380','0381','0383','3314','3903','3909','6984','8582','0457','0470','0480','0488','0506','1402','1802','1806','8527','0368','0378','3305','3316','3919','0143','0170','1115','3906','8941','8919') "
					+ "group by hdr.seller_organization_code, hdr.extn_host_order_ref, hdr.extn_legacy_ord_ref, cc.tran_return_code, cc.reference1, yp.PAYMENT_TYPE";
			logger.info("CheckMissingOrders: Executing Sterling query . . ."
					+ query);
			rset = stmt.executeQuery(query);
			logger.info("Looping through results");
			while (rset.next()) {
				OrderDTO od = new OrderDTO();
				try {
					od.setStore(rset.getString(1).trim());
					od.setOrder(rset.getString(2).trim());
					if (rset.getString(3) == null) {
						od.setLegacyOrder("");
					} else {
						od.setLegacyOrder(rset.getString(3).trim());
					}
					if (rset.getString(4) == null) {
						od.setPosStgTransID("");
					} else {
						od.setPosStgTransID(rset.getString(4).trim());
					}
					String reference = rset.getString(5).trim();
					String[] ref = reference.split(";", -1);
					od.setRegister(ref[0].trim());
					od.setSalesDate(ref[2].trim());
					String transId = ref[3].trim();
					String paymentType = rset.getString(6).trim();
					od.setPaymentType(paymentType);
					if (paymentType.equalsIgnoreCase("MN")) {
						transId = transId.substring(0, (transId.length() - 1));
						od.setPosTransID(transId);
					} else {
						od.setPosTransID(transId);
					}
					sterlingData.add(od);
				} catch (Exception e) {
					logger.error("Exception occured: " + e);
					logger.error("While fetching for order: " + od.getOrder());
					continue;
				}
			}
			rset.close();
			stmt.close();
			con.close();
		} catch (Exception e) {
			logger.error("Exception occured: " + e);
			e.printStackTrace();
		}
		return sterlingData;
	}

	public static boolean checkOrderPresentInSterlingDB(String order) {
		// Connection con = null;
		Statement stmt = null;
		ResultSet rset = null;
		boolean isOrderPresent = false;
		try {
			// final String driverClass = "oracle.jdbc.driver.OracleDriver";
			// final String connectionURL =
			// "jdbc:oracle:thin:@pprmm78x.homedepot.com:1521/DPR78MM_RO_CODS_01";
			// final String uName = "CODSRO01";
			// final String uPassword = "juspu6Ew";
			// Class.forName(driverClass);
			// con = DriverManager.getConnection(connectionURL, uName,
			// uPassword);
			DBConnectionUtil dbConn = new DBConnectionUtil();
			Connection con = dbConn
					.getJNDIConnection(Constants.ODS_STAND_BY_JNDI);
			stmt = con.createStatement();
			String query = "select * from yfs_order_header where extn_host_order_ref='"
					+ order + "' and document_type='0001' ";
			rset = stmt.executeQuery(query);
			while (rset.next()) {
				isOrderPresent = true;
			}
			rset.close();
			stmt.close();
			con.close();
		} catch (Exception e) {
			logger.error("Exception Occurred: " + e);
			e.printStackTrace();
		}
		return isOrderPresent;
	}

	public static ArrayList<String> getYModeStores() {

		ArrayList<String> storeList = new ArrayList<String>();
		try {
			// final String driverClass = "oracle.jdbc.driver.OracleDriver";
			// final String connectionURL =
			// "jdbc:oracle:thin:@pprmm78x.homedepot.com:1521/dpr78mm_sro01";
			// final String uName = "MMUSR01";
			// final String uPassword = "COMS_MMUSR01";
			// Class.forName(driverClass);
			// Connection con = null;
			// con = DriverManager.getConnection(connectionURL, uName,
			// uPassword);
			DBConnectionUtil dbConn = new DBConnectionUtil();
			Connection con = dbConn
					.getJNDIConnection(Constants.COMT_SSC_RO_JNDI);
			Statement stmt = null;
			stmt = con.createStatement();
			ResultSet result = null;
			String query = "select distinct loc_nbr from thd01.comt_loc_capbl where comt_capbl_id = '10'";
			result = stmt.executeQuery(query);
			if (result != null) {
				while (result.next()) {
					storeList.add(result.getString(1));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		logger.info("Current Y Mode store count: " + storeList.size());
		return storeList;
	}

	public static void sendMail(String msgContent) {
		// logger.info("CheckMissingOrders: Preparing to send email");
		final String user = "horizon@cpliisad.homedepot.com";
		final String password = "xxx";// change accordingly
		String host = "mail1.homedepot.com";
		Properties props = new Properties();
		props.put("mail.host", host);
		Session session = Session.getInstance(props,
				new javax.mail.Authenticator() {
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(user, password);
					}
				});
		try {
			String[] to = { "Chris_Kelley@homedepot.com",
					"Reid_Tarentino@HomeDepot.com", "ADAM_FLEET@homedepot.com",
					"SERGIO_GIL@homedepot.com", "DENNIS_JONES@homedepot.com",
					"APRIL_M_RAAB@homedepot.com", "NIRAL_PATEL2@homedepot.com",
					"David_Cooper@homedepot.com",
					"REMYA_PALANEEPAN@homedepot.com",
					"AMANDA_K_ODUM@homedepot.com",
					"SANTHOSH_KUMAR_R@homedepot.com",
					"VINAY_D_NARAYANAMURTHY1@homedepot.com",
					"RAGEESH_THEKKEYIL1@homedepot.com",
					"SWATI_RAWAL@homedepot.com", "_2fc77b@homedepot.com",
					"stephen_agyepong1@homedepot.com",
					"Keisha_Allen@HomeDepot.com", "ANKUR_BHATIA@homedepot.com",
					"JEYA_KAMARAJ@homedepot.com",
					"SIVAKRISHNA_KOGILI@homedepot.com",
					"LYNNE_M_LUCAS@homedepot.com",
					"CHAITHRA_NARASIMHASETTY@homedepot.com" };
			String[] Cc = {};
			 /*String[] to = {
			 "rakesh_j_satya@homedepot.com","purushothaman_raghunath@homedepot.com", "MAHESH_VYAKARANAM@homedepot.com","_275769@homedepot.com"
			 };*/
			// String[] Cc = {};
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress(user));
			DateFormat dateFormat = new SimpleDateFormat(
					"MM/dd/yyyy hh:mm:ss a");
			Date today = new Date();
			String date1 = dateFormat.format(today);
			message.setSubject("Payments Missing Report - " + date1);
			// message.setText("This is simple program of sending email using JavaMail API");
			message.setFrom(new InternetAddress(user));
			InternetAddress[] addressTo = new InternetAddress[to.length];
			InternetAddress[] addressCc = new InternetAddress[Cc.length];
			for (int i = 0; i < to.length; i++) {
				addressTo[i] = new InternetAddress(to[i]);
				logger.warn("To Address " + to[i]);
			}
			for (int j = 0; j < Cc.length; j++) {
				addressCc[j] = new InternetAddress(Cc[j]);
				logger.warn("Cc Address " + Cc[j]);
			}
			message.setRecipients(RecipientType.TO, addressTo);
			message.setRecipients(RecipientType.CC, addressCc);
			message.setContent(msgContent, "text/html");
			Transport.send(message);
			for (int i = 0; i < to.length; i++) {
				addressTo[i] = new InternetAddress(to[i]);
				logger.warn("Message delivered to--- " + to[i]);
			}
			for (int j = 0; j < Cc.length; j++) {
				addressCc[j] = new InternetAddress(Cc[j]);
				logger.warn("Message delivered to cc recepients--- " + Cc[j]);
			}

		} catch (MessagingException e) {
			e.printStackTrace();
		}

	}
}
