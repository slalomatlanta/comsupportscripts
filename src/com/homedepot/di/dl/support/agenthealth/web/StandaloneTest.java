package com.homedepot.di.dl.support.agenthealth.web;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

public class StandaloneTest {
	private static final Logger logger = Logger.getLogger(AgentHealthBL.class);

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String profServerLocn = "C:\\temp\\server_profile.csv";
		String profAgentLocn = "C:\\temp\\agent_profile.csv";
		Map<String, List<String>> profServersMapping = new HashMap<String, List<String>>();
		Map<String, List<String>> profAgentsMapping = new HashMap<String, List<String>>();
		Map<String, String> allServerAgents = new HashMap<String, String>();
		Map<String, String> serverAgentsreportingUp = new HashMap<String, String>();
		Map<String, String> serverAgentsreportingDown = new HashMap<String, String>();
		try {
			AgentHealthBL obj = new AgentHealthBL();
			profServersMapping = obj.readFromCSV(profServerLocn);
			logger.info(profServersMapping.size() + " profile-servers fetched");
			if (profServersMapping.size() > 0) {
				profAgentsMapping = obj.readFromCSV(profAgentLocn);
				logger.info(profAgentsMapping.size() + " prof Agents fetched");
				if (profAgentsMapping.size() > 0) {
					allServerAgents = obj.formatServerAgent(profServersMapping,
							profAgentsMapping);
					logger.info(allServerAgents.size()
							+ " all server-agent combinations from CSV");
					serverAgentsreportingUp = obj.getAgentsReportingUp();
					logger.info(serverAgentsreportingUp.size()
							+ " server-agent combinations reporting up");
					serverAgentsreportingDown = obj.findServersReportingDown(
							allServerAgents, serverAgentsreportingUp);
					if (serverAgentsreportingDown.size() > 0) {
						logger.info("Below are the agents reporting down");
						for (String key : serverAgentsreportingDown.keySet()) {
							logger.info(key.split("-")[1] + " on "
									+ key.split("-")[0] + " is "+serverAgentsreportingDown.get(key));
						}
					} else {
						logger.info("All agents up");
					}
				} else {
					logger.info("profAgentsMapping not found");
				}
			} else {
				logger.info("profServersMapping not found");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// logger.info(serverAgentsreportingUp.get("s"));

		// TODO Auto-generated method stub

	}

}
