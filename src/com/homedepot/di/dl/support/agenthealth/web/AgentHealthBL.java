package com.homedepot.di.dl.support.agenthealth.web;

import java.util.HashMap;
import java.util.List;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import au.com.bytecode.opencsv.CSVReader;

public class AgentHealthBL {

	private static final Logger logger = Logger.getLogger(AgentHealthBL.class);
	private static final Logger oLogger = Logger.getLogger("splunkLog");

	public void agentHealthCheck() {
		// TODO Auto-generated method stub
		String profServerLocn = "//opt//isv//tomcat//temp//server_profile.csv";
		String profAgentLocn = "//opt//isv//tomcat//temp//agent_profile.csv";
		//String profServerLocn = "C://temp//server_profile.csv";
		//String profAgentLocn = "C://temp//agent_profile.csv";
		logger.debug("Profile-Server Location:"+profServerLocn);
		logger.debug("Agent-Profile Location:"+profAgentLocn);
		Map<String, String> orangeLogMsgMap = new HashMap<String, String>();
		Map<String, List<String>> profServersMapping = new HashMap<String, List<String>>();
		Map<String, List<String>> profAgentsMapping = new HashMap<String, List<String>>();
		Map<String, String> allServerAgents = new HashMap<String, String>();
		Map<String, String> serverAgentsreportingUp = new HashMap<String, String>();
		Map<String, String> serverAgentsreportingDown = new HashMap<String, String>();
		try {
			AgentHealthBL obj = new AgentHealthBL();
			profServersMapping = obj.readFromCSV(profServerLocn);
			logger.info(profServersMapping.size() + " profile-servers fetched");
			if (profServersMapping.size() > 0) {
				profAgentsMapping = obj.readFromCSV(profAgentLocn);
				logger.info(profAgentsMapping.size() + " prof Agents fetched");
				if (profAgentsMapping.size() > 0) {
					allServerAgents = obj.formatServerAgent(profServersMapping,
							profAgentsMapping);
					logger.info(allServerAgents.size()
							+ " all server-agent combinations from CSV");
					serverAgentsreportingUp = obj.getAgentsReportingUp();
					logger.info(serverAgentsreportingUp.size()
							+ " server-agent combinations reporting up");
					serverAgentsreportingDown = obj.findServersReportingDown(
							allServerAgents, serverAgentsreportingUp);
					if (serverAgentsreportingDown.size() > 0) {
						logger.info("Below are the agents reporting down");
						for (String key : serverAgentsreportingDown.keySet()) {
							logger.info(key.split("-")[1] + " on "
									+ key.split("-")[0] + " is "
									+ serverAgentsreportingDown.get(key));
							orangeLogMsgMap.put(key,key.split("-")[1] + " on "
									+ key.split("-")[0] + " is "
									+ serverAgentsreportingDown.get(key));
						}
					} else {
						logger.info("All agents up");
					}
				} else {
					logger.info("profAgentsMapping not found");
				}
			} else {
				logger.info("profServersMapping not found");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		for(String key : orangeLogMsgMap.keySet()){
			oLogger.info("Program=SterlingServerHealth server="+key.split("-")[0] +" agent="+key.split("-")[1]+" status="+serverAgentsreportingDown.get(key).replace(" ", "_"));
		}
		
//		oLogger.info("Program : SterlingServerHealth " + orangeLogMsgMap);
		// logger.info(serverAgentsreportingUp.get("s"));

		// TODO Auto-generated method stub

	}

	public Map<String, List<String>> readFromCSV(String fileLocation) {
		// logger.info("readFromCSV() invoked");
		Map<String, List<String>> mapping = new HashMap<String, List<String>>();
		List<String> strList = null;
		String[] row = null;
		CSVReader csvReader = null;
		boolean headerRow = true;
		try {
			csvReader = new CSVReader(new FileReader(fileLocation));
			while ((row = csvReader.readNext()) != null) {
				if (headerRow) {
					headerRow = false;
				} else {
					String key = row[0].trim();
					String value = row[1].trim();
					logger.info(key + value);
					if (mapping.containsKey(key)) {
						strList = new ArrayList<String>();
						strList = mapping.get(key);
						strList.add(value);
						mapping.put(key, strList);
						// strList.clear();
					} else {
						strList = new ArrayList<String>();
						strList.add(value);
						mapping.put(key, strList);

					}

				}
			}
			csvReader.close();
		} catch (Exception e) {
			e.getStackTrace();
			logger.debug("Exception Occured while reading CSV : "
					+ e.getStackTrace());
		}
		return mapping;
	}

	public Map<String, String> getAgentsReportingUp() throws SQLException,
			ClassNotFoundException {
		Map<String, String> serverAgentsreportingUp = new HashMap<String, String>();
		String host = null;
		String agent = null;
		String hostAgent = null;
		final String driverClass = "oracle.jdbc.driver.OracleDriver";
		final String connectionURL = "jdbc:oracle:thin:@//spragor10-scan.homedepot.com:1521/dpr77mm_sro01";
		final String uName = "MMUSR01";
		final String uPassword = "COMS_MMUSR01";
		final String query = "select distinct host_name, server_name from THD01.YFS_HEARTBEAT where status = '00' "
				+ "order by host_name";
		Class.forName(driverClass);
		try {
			Connection con = null;
			con = DriverManager.getConnection(connectionURL, uName, uPassword);
			Statement stmt = null;
			stmt = con.createStatement();
			ResultSet result = null;
			result = stmt.executeQuery(query);
			while (result.next()) {
				host = result.getString(1).trim();
				agent = result.getString(2).trim();
				hostAgent = host + "-" + agent;
				serverAgentsreportingUp.put(hostAgent, "UP");
			}
			stmt.close();
			result.close();
			con.close();
		} catch (SQLException e1) {
			e1.printStackTrace();
			// logger.debug("Exception Occured while reading from Sterling DB : "
			// + e.getStackTrace());
		}
		return serverAgentsreportingUp;
	}

	public Map<String, String> formatServerAgent(
			Map<String, List<String>> profServersMapping,
			Map<String, List<String>> profAgentsMapping) throws SQLException,
			ClassNotFoundException {
		String serverAgent = null;
		Map<String, String> serverAgentsreporting = new HashMap<String, String>();
		List<String> serverList = null;
		List<String> agentList = null;
		for (String prof : profServersMapping.keySet()) {
			serverList = new ArrayList<String>();
			serverList = profServersMapping.get(prof);
			for (String server : serverList) {
				agentList = new ArrayList<String>();
				agentList = profAgentsMapping.get(prof);
				if (null != agentList) {
					for (String agent : agentList) {
						serverAgent = server + "-" + agent;
						serverAgentsreporting.put(serverAgent, "DOWN");
					}
				}

			}
		}
		return serverAgentsreporting;
	}

	public Map<String, String> findServersReportingDown(
			Map<String, String> allServerAgents,
			Map<String, String> serverAgentsreportingUp) {
		Map<String, String> serverAgentsreportingDown = new HashMap<String, String>();
		String serverAgentStatus = null;
		for (String serverAgentUp : serverAgentsreportingUp.keySet()) {
			if (allServerAgents.containsKey(serverAgentUp)) {
				allServerAgents.put(serverAgentUp, "UP");
			} else {
				allServerAgents.put(serverAgentUp,
						"UP - not listed in agentlist properties");
			}
		}
		for (String key : allServerAgents.keySet()) {
			serverAgentStatus = allServerAgents.get(key);
			if (serverAgentStatus.contains("DOWN")) {
				serverAgentsreportingDown.put(key, serverAgentStatus);
			}
		}
		return allServerAgents;
	}
}
