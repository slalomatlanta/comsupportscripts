package com.homedepot.di.dl.support.deltaDashboard.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.homedepot.di.dl.support.compareDataReplication.web.CompareDataReplicationServlet;
import com.homedepot.di.dl.support.deltaDashboard.dto.StoreDeltaDTO;
import com.homedepot.di.dl.support.deltaDashboard.service.DeltaCountFromStoreService;

/**
 * Servlet implementation class GetDeltaCountFromStoreServlet
 */
public class GetDeltaCountFromStoreServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	final static Logger logger = Logger.getLogger(GetDeltaCountFromStoreServlet.class);
	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public GetDeltaCountFromStoreServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		String store = request.getParameter("store");
		response.setContentType("text/html");
		PrintWriter out = null;
		out = response.getWriter();
		if (store != null) {
			ArrayList<StoreDeltaDTO> deltaDetailList = DeltaCountFromStoreService
					.getDeltasFromStore(store);
			Gson gson = new Gson();
			String json = gson.toJson(deltaDetailList);
			logger.debug(json);			
			out.println(json);			
		}
		else{
			out.println("please pass parameter \"store\" in the service url");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
