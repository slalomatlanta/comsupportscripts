package com.homedepot.di.dl.support.deltaDashboard.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.homedepot.di.dl.support.compareDataReplication.web.CompareDataReplicationServlet;
import com.homedepot.di.dl.support.deltaDashboard.service.DeltaByStateService;
import com.homedepot.di.dl.support.util.Constants;
import com.homedepot.di.dl.support.util.PushAlerts;
import com.homedepot.ta.aa.log4j.OrangeLogHelper;

/**
 * Servlet implementation class DeltaCountByStateServlet
 */
public class DeltaCountByStateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger oLogger = Logger.getLogger("splunkLog");
	final static Logger logger = Logger.getLogger(DeltaCountByStateServlet.class);
   
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeltaCountByStateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		logger.info("DeltaByStateService started at "+java.util.GregorianCalendar.getInstance().getTime());
		
		Map<String,String> orangeLogMsgMap = new HashMap<String, String>();
		

		HashMap<String, String> finalCountMap = DeltaByStateService.getDeltaCountFromStores();
		String mailContent = "<html><body><font face=\"calibri\"><h3>Delta count by state</h3><table border=\"1\" cellpadding=\"0\" cellspacing=\"0\" width=\"200px\" style=\"border-collapse:collapse;\">"
				+ "<tr><th>Delta State</th><th>Count</th></tr>";
		int grandTotal = 0;
		for (String key : finalCountMap.keySet()){
			grandTotal = grandTotal + Integer.parseInt(finalCountMap.get(key));
			orangeLogMsgMap.put(key, finalCountMap.get(key));
			mailContent = mailContent + "<tr><td>"+key+"</td><td>"+finalCountMap.get(key)+"</td></tr>";
		}
		
		
		
		PushAlerts pushAl= new PushAlerts();		
	    //pushAl.postAppUpdate("rmo7RFWbkSptprWk7", true, " Found across all Stores", grandTotal+"");
	    orangeLogMsgMap.put("Total", grandTotal+"");
		mailContent = mailContent + "<tr><th>Grand Total</th><th>"+grandTotal+"</th></tr></table></font></body></html>";
		response.setContentType("text/html");
		PrintWriter out = null;
		out = response.getWriter();
		out.println(mailContent);
		DeltaByStateService.sendMail(mailContent);
		oLogger.info("Program=DeltaByStateService DeltaCountByState=" + orangeLogMsgMap);
		logger.info("DeltaByStateService completed at "+java.util.GregorianCalendar.getInstance().getTime());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
