package com.homedepot.di.dl.support.deltaDashboard.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;
import java.util.concurrent.Callable;
import org.apache.log4j.Logger;

import com.homedepot.di.dl.support.util.DBConnectionUtil;


public class DeltaCallable implements Callable<HashMap<String, String>>{
	private static final Logger logger = Logger.getLogger(DeltaCallable.class);
	public String store;

	public DeltaCallable(String storeNum) {
		this.store = storeNum;
	}
	
	@Override
	public HashMap<String, String> call() throws Exception {
		HashMap<String, String> deltaCountMap = new HashMap<String, String>();
		// long sum = 0;
		// for (long i = 0; i <= 100; i++) {
		// sum += i;
		// }
		
		DBConnectionUtil dbConn = new DBConnectionUtil();
		Connection con = dbConn.getStoreconnection(this.store);
		
		String query = "select d.trnsm_stat_ind,COUNT(*) from comt_ord_chg_msg d group by d.trnsm_stat_ind";
		Statement stmt = null;
		ResultSet rs = null;
		stmt = con.createStatement();

		rs = stmt.executeQuery(query);
		if (rs != null) {
			while (rs.next()) {
				deltaCountMap.put(rs.getString(1).trim(), rs.getString(2).trim());
			}
		}
		rs.close();
		stmt.close();
		con.close();
		logger.debug(this.store + " has " + deltaCountMap.size()
				+ " deltas");

		return deltaCountMap;
	}

}
