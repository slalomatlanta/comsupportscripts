package com.homedepot.di.dl.support.deltaDashboard.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.homedepot.di.dl.support.deltaDashboard.dto.DeltaDetailsDTO;
import com.homedepot.di.dl.support.util.Constants;
import com.homedepot.di.dl.support.util.DBConnectionUtil;

public class ConcurrencyExample {
	private static final Logger logger = Logger.getLogger(ConcurrencyExample.class);
	
	private static final int NTHREDS = 88;

	public static void main(String[] args) {
		String jsonString = StoreConcurrency();
	}
	
	public static String StoreConcurrency(){


		ExecutorService executor = Executors.newFixedThreadPool(NTHREDS);
		HashMap<String, HashMap<String, DeltaDetailsDTO>> deltaDetailsMap = new HashMap<String, HashMap<String, DeltaDetailsDTO>>();

		List<Future<HashMap<String, DeltaDetailsDTO>>> list = new ArrayList<Future<HashMap<String, DeltaDetailsDTO>>>();

		List<String> storeList = new ArrayList<String>();
		 storeList = getStores();
//		storeList.add("0403");
//		storeList.add("0116");
//		storeList.add("0111");

		for (String store : storeList) {
			Callable<HashMap<String, DeltaDetailsDTO>> worker = new MyCallable(
					store);
			Future<HashMap<String, DeltaDetailsDTO>> submit = executor
					.submit(worker);
			list.add(submit);
		}
		// long sum = 0;
		logger.info(list.size());

		// now retrieve the result
		for (Future<HashMap<String, DeltaDetailsDTO>> future : list) {
			try {

				String storeNum = "";
				HashMap<String, DeltaDetailsDTO> storeDetailsMap = future.get();

				if (storeDetailsMap.size() > 0) {
					for (String key : storeDetailsMap.keySet()) {
						DeltaDetailsDTO od = storeDetailsMap.get(key);
						storeNum = od.getStoreNumber();
						break;
					}
					if (!(deltaDetailsMap.containsKey(storeNum))) {
						deltaDetailsMap.put(storeNum, storeDetailsMap);
					}
				}

				logger.debug("This is inside Executor method -- "+ storeDetailsMap.size());

			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (ExecutionException e) {
				e.printStackTrace();
			}
		}

		Gson gson = new Gson();
		String json = gson.toJson(deltaDetailsMap);
		logger.debug(json);

		executor.shutdown();
		return json;
	}

	public static ArrayList<String> getStores() {

		ArrayList<String> storeList = new ArrayList<String>();
		try {
//			DBConnectionUtil dbConn = new DBConnectionUtil();
//			Connection con = dbConn
//					.getJNDIConnection(Constants.COMT_SSC_RO_JNDI);
			final String driverClass = "oracle.jdbc.driver.OracleDriver";
			final String connectionURL = "jdbc:oracle:thin:@spragor10-scan.homedepot.com:1521/dpr78mm_sro01";
			final String uName = "MMUSR01";
			final String uPassword = "COMS_MMUSR01";
			Class.forName(driverClass);
			Connection con = null;
			con = DriverManager.getConnection(connectionURL, uName, uPassword);
			Statement stmt = null;
			stmt = con.createStatement();
			ResultSet result = null;
			String query = "select distinct loc_nbr from thd01.comt_loc_capbl where comt_capbl_id = '7'";
			result = stmt.executeQuery(query);
			if (result != null) {
				while (result.next()) {
					storeList.add(result.getString(1));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		logger.info("Total store count: " + storeList.size());
		return storeList;
	}
}