package com.homedepot.di.dl.support.deltaDashboard.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import org.apache.log4j.Logger;

import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage.RecipientType;



public class DeltaByStateService {
	private static final Logger logger = Logger.getLogger(DeltaByStateService.class);
	private static final int NTHREDS = 88;
	
	public static void main (String args[]){
		logger.info("DeltaByStateService started at "+java.util.GregorianCalendar.getInstance().getTime());
		HashMap<String, String> finalCountMap = getDeltaCountFromStores();
		String mailContent = "<html><body><font face=\"calibri\"><h3>Delta count by state</h3><table border=\"1\" cellpadding=\"0\" cellspacing=\"0\" width=\"200px\" style=\"border-collapse:collapse;\">"
				+ "<tr><th>Delta State</th><th>Count</th></tr>";
		int grandTotal = 0;
		for (String key : finalCountMap.keySet()){
			grandTotal = grandTotal + Integer.parseInt(finalCountMap.get(key));
			mailContent = mailContent + "<tr><td>"+key+"</td><td>"+finalCountMap.get(key)+"</td></tr>";
		}
		mailContent = mailContent + "<tr><th>Grand Total</th><th>"+grandTotal+"</th></tr></table></font></body></html>";
		sendMail(mailContent);
		logger.info("DeltaByStateService completed at "+java.util.GregorianCalendar.getInstance().getTime());
	}
	
	public static HashMap<String, String> getDeltaCountFromStores(){
		HashMap<String, String> finalCountMap = new HashMap<String, String>();
		List<Future<HashMap<String, String>>> list = new ArrayList<Future<HashMap<String, String>>>();
		ExecutorService executor = Executors.newFixedThreadPool(NTHREDS);
		List<String> storeList = new ArrayList<String>();
		 storeList = ConcurrencyExample.getStores();
//		storeList.add("0097");
//		storeList.add("0054");
//		storeList.add("0052");
		
		for (String store : storeList) {
			Callable<HashMap<String, String>> worker = new DeltaCallable(
					store);
			Future<HashMap<String, String>> submit = executor
					.submit(worker);
			list.add(submit);
		}
		logger.info(list.size());
		
		for (Future<HashMap<String, String>> future : list) {
			try {

				String storeNum = "";
				HashMap<String, String> deltaCountMap = future.get();

				if (deltaCountMap.size() > 0) {
					for (String key : deltaCountMap.keySet()) {
						if(finalCountMap.containsKey(key)){
							int count =  Integer.parseInt(finalCountMap.get(key)) + Integer.parseInt(deltaCountMap.get(key));
							finalCountMap.remove(key);
							finalCountMap.put(key, ""+count);
						}
						else {
							finalCountMap.put(key, deltaCountMap.get(key));
						}
					}
				}

				logger.debug("This is inside Executor method -- "+ deltaCountMap.size());

			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (ExecutionException e) {
				e.printStackTrace();
			}
		}

//		Gson gson = new Gson();
//		String json = gson.toJson(finalCountMap);
//		System.out.println(json);

		executor.shutdown();
		
		return finalCountMap;
	}
	
	public static void sendMail(String msgContent) {
		logger.info("CheckMissingOrders: Preparing to send email");
		final String user = "horizon@cpliisad.homedepot.com";
		final String password = "xxx";// change accordingly
		String host = "mail1.homedepot.com";
		Properties props = new Properties();
		props.put("mail.host", host);
		Session session = Session.getInstance(props,
				new javax.mail.Authenticator() {
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(user, password);
					}
				});
		try {
//			String[] to = {"_2fc77b@homedepot.com" };
			String[] Cc = {};
			 String[] to = { "DINESH_E@homedepot.com","NIRAL_PATEL2@homedepot.com","DENNIS_JONES@homedepot.com",
			 "MANOJ_SHUNMUGAM@homedepot.com" };
			// String[] Cc = {};
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress(user));
			DateFormat dateFormat = new SimpleDateFormat(
					"MM/dd/yyyy hh:mm:ss a");
			Date today = new Date();
			String date1 = dateFormat.format(today);
			message.setSubject("Delta Count Report - " + date1);
			// message.setText("This is simple program of sending email using JavaMail API");
			message.setFrom(new InternetAddress(user));
			InternetAddress[] addressTo = new InternetAddress[to.length];
			InternetAddress[] addressCc = new InternetAddress[Cc.length];
			for (int i = 0; i < to.length; i++) {
				addressTo[i] = new InternetAddress(to[i]);
				logger.info("To Address " + to[i]);
			}
			for (int j = 0; j < Cc.length; j++) {
				addressCc[j] = new InternetAddress(Cc[j]);
				logger.info("Cc Address " + Cc[j]);
			}
			message.setRecipients(RecipientType.TO, addressTo);
			message.setRecipients(RecipientType.CC, addressCc);
			message.setContent(msgContent, "text/html");
			Transport.send(message);
			for (int i = 0; i < to.length; i++) {
				addressTo[i] = new InternetAddress(to[i]);
				logger.info("Message delivered to--- " + to[i]);
			}
			for (int j = 0; j < Cc.length; j++) {
				addressCc[j] = new InternetAddress(Cc[j]);
				logger.info("Message delivered to cc recepients--- "
						+ Cc[j]);
			}

		} catch (MessagingException e) {
			e.printStackTrace();
		}

	}

}
