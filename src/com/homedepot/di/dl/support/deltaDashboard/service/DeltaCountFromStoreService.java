package com.homedepot.di.dl.support.deltaDashboard.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.homedepot.di.dl.support.compareDataReplication.web.CompareDataReplicationServlet;
import com.homedepot.di.dl.support.deltaDashboard.dto.StoreDeltaDTO;
import com.homedepot.di.dl.support.util.DBConnectionUtil;

public class DeltaCountFromStoreService {
	final static Logger logger = Logger.getLogger(DeltaCountFromStoreService.class);
	
	public static void main(String args[]) {
		String store = "0054";
//		HashMap<String, String> deltaDetailMap = getDeltasFromStore(store);
		ArrayList<StoreDeltaDTO> deltaDetailList = getDeltasFromStore(store);
		Gson gson = new Gson();
		String json = gson.toJson(deltaDetailList);
		logger.info(json);
	}

	public static ArrayList<StoreDeltaDTO> getDeltasFromStore(String storeNum) {
		ArrayList<StoreDeltaDTO> deltaDetailList = new ArrayList<StoreDeltaDTO>();
		try {
			DBConnectionUtil dbConn = new DBConnectionUtil();
			Connection con = dbConn.getStoreconnection(storeNum);
			
			String query = "select d.cust_ord_id, d.trnsm_stat_ind, COUNT(*) from comt_ord_chg_msg d group by d.cust_ord_id, d.trnsm_stat_ind";
			Statement stmt = null;
			ResultSet rs = null;
			stmt = con.createStatement();
			rs = stmt.executeQuery(query);
			if (rs != null) {
				while (rs.next()) {
					StoreDeltaDTO sd = new StoreDeltaDTO();
					
					sd.setOrderNumber(rs.getString(1));
					sd.setState(rs.getString(2));
					sd.setCount(rs.getString(3));
					
					deltaDetailList.add(sd);
				}
			}
			rs.close();
			stmt.close();
			con.close();
			logger.info(storeNum + " completed");

		} catch (Exception e) {
			logger.error("Exception Occurred : " + e.getStackTrace().toString());
		}
		return deltaDetailList;
	}

	

}
