package com.homedepot.di.dl.support.deltaDashboard.service;

import java.sql.Connection;
import com.homedepot.di.dl.support.util.DBConnectionUtil;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;
import java.util.concurrent.Callable;
import org.apache.log4j.Logger;


import com.homedepot.di.dl.support.deltaDashboard.dto.DeltaDetailsDTO;

public class MyCallable implements Callable<HashMap<String, DeltaDetailsDTO>> {

	private static final Logger logger = Logger.getLogger(MyCallable.class);
	public String store;

	public MyCallable(String storeNum) {
		this.store = storeNum;
	}

	@Override
	public HashMap<String, DeltaDetailsDTO> call() throws Exception {
		HashMap<String, DeltaDetailsDTO> storeDetailsMap = new HashMap<String, DeltaDetailsDTO>();
		// long sum = 0;
		// for (long i = 0; i <= 100; i++) {
		// sum += i;
		// }

		DBConnectionUtil dbConn = new DBConnectionUtil();
		
		Connection con = dbConn.getStoreconnection(this.store);
		String query = "select d.cust_ord_id, e.trnsltn_excpt_txt, d.trnsm_stat_ind, e.last_upd_ts from comt_ord_chg_msg d left outer join comt_ord_trnsltn_err e on d.cust_ord_id = e.cust_ord_id";
		Statement stmt = null;
		ResultSet rs = null;
		stmt = con.createStatement();

		rs = stmt.executeQuery(query);
		if (rs != null) {
			while (rs.next()) {

				DeltaDetailsDTO od = new DeltaDetailsDTO();
				od.setStoreNumber(this.store);
				od.setOrderNumber(rs.getString(1));
				od.setCount(1);
				
				if (rs.getString(2) == null){
					od.setError("No Error Reason");
				}
				else{
					od.setError(rs.getString(2));
				}
				
				od.setState(rs.getString(3));

				String key = od.getOrderNumber().trim() + "|"
						+ od.getState().trim();

				if (storeDetailsMap.containsKey(key)) {
					od = storeDetailsMap.get(key);
					od.setCount(od.getCount() + 1);
				} else {
					storeDetailsMap.put(key, od);
				}
			}
		}
		rs.close();
		stmt.close();
		con.close();
		logger.debug(this.store + " has " + storeDetailsMap.size()	+ " deltas");
		return storeDetailsMap;
	}

	

}