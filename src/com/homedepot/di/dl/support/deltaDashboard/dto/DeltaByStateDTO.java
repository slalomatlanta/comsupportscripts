package com.homedepot.di.dl.support.deltaDashboard.dto;

public class DeltaByStateDTO {
	private String state;
	private int count;
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	
	
}
