package com.homedepot.di.dl.support.deltaDashboard.dto;

public class StoreDeltaDTO {
	
	private String orderNumber;
	private String state;
	private String count;
	
	public String getOrderNumber() {
		return orderNumber;
	}
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCount() {
		return count;
	}
	public void setCount(String count) {
		this.count = count;
	}
	
	

}
