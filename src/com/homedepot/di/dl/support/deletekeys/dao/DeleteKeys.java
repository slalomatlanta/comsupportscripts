package com.homedepot.di.dl.support.deletekeys.dao;


import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.Properties;
import java.util.ResourceBundle;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeMessage.RecipientType;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.homedepot.di.dl.support.deletekeys.util.Constants;
import com.homedepot.di.dl.support.deletekeys.util.DBConnectionUtil;
import com.homedepot.di.dl.support.deletekeys.web.DeleteKeysServlet;
/**
 * Servlet implementation class DeleteKeys
 */
public class DeleteKeys extends Constants {
	private static final long serialVersionUID = 1L;

	final static Logger logger = Logger.getLogger(DeleteKeys.class);
	/**
	 * @see HttpServlet#HttpServlet()
	 */
public static void main(String[] args) throws SQLException, IOException, Exception {
		
		
		//System.out.println("Main");
		DeleteKey();
	}
	
		
		 public static void DeleteKey() throws 
		Exception {
		
		// TODO Auto-generated constructor stub
	

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	
		// TODO Auto-generated method stub
		
		try {
			// FileWriter writer = new FileWriter("C:\\store\\InboxKey.csv");
			FileWriter writer = new FileWriter(
					"/opt/isv/tomcat-6.0.18/temp/InboxKey.csv");
			// Connection sterCon = getConnection("sterlingConnectionURL_SSC");
			/*final String driverClass = "oracle.jdbc.driver.OracleDriver";
			final String connectionURL2 = "jdbc:oracle:thin:@//pprmm78x.homedepot.com:1521/dpr77mm_sro01";
			final String uName2 = "MMUSR01";
			final String uPassword2 = "COMS_MMUSR01";
			Connection sterCon = null;
			sterCon = DriverManager.getConnection(connectionURL2, uName2,
					uPassword2);*/
			DBConnectionUtil dbConn = new DBConnectionUtil();
            
            Connection sscConn = dbConn.getJNDIConnection(Constants.STERLING_SSC_RO_JNDI);

			Statement st = null;
			st = sscConn.createStatement();
			ResultSet result = null;
			String Query = "select * from thd01.hd_inbox where inbox_key in (select h.inbox_key from thd01.hd_inbox h, thd01.yfs_work_order w where h.inbox_key > '201404' "
					+ " and h.alert_trans_lvl_type='DELIVERY' and h.work_order_type='O' "
					+ " and h.work_order_no=w.work_order_no "
					+ " and w.status in ('1400','1600')) or inbox_key in ( select h.inbox_key from thd01.hd_inbox h, THD01.hd_related_work_order w where h.inbox_key > '201404' and h.alert_trans_lvl_type='DELIVERY' and h.work_order_type='C' and h.work_order_no=w.related_work_order_no "
					+ " and w.status in ('1400','1600')) "
					+ " or "
					+ " inbox_key in "
					+ "(select  hdi.inbox_key "
					+ " from thd01.yfs_order_line yol, thd01.hd_inbox hdi, THD01.yfs_order_release_status yors, thd01.yfs_order_header yoh "
					+ " where yol.order_header_key = yors.order_header_key and yol.order_line_key = yors.order_line_key and "
					+ " yol.order_header_key = hdi.order_header_key and yol.order_line_key = hdi.order_line_key "
					+ " and hdi.exception_type not like 'MANUAL_ALERT%' and yol.line_type in ('IN','ME') "
					+ " and yol.order_header_key > '201404' and yors.status in ('3700.7777', '3700','9000') and yol.extn_svs_line_status in ( '1000', '1300') "
					+ " and yors.status_quantity > 0 and yol.order_header_key=yoh.order_header_key ) or "
					+ " inbox_key in "
					+ " (select hi.inbox_key from "
					+ " thd01.yfs_order_header oh, thd01.yfs_order_release_status ors, thd01.hd_inbox hi where oh.order_header_key > '20140401' "
					+ " and ors.order_release_status_key > '20140401'and hi.inbox_key > '20140401' and oh.document_type = '0005' and oh.extn_svs_order_status = '1000' "
					+ " and ors.order_header_key = oh.order_header_key and ors.status >= '3900' and ors.status_quantity > '0' and oh.order_header_key = hi.PO_HDR_KEY "
					+ " and hi.status = 'OPEN' ) or "
					+ " inbox_key in "
					+ " (select hi.inbox_key from thd01.hd_inbox hi, THD01.YFS_WORK_ORDER w "
					+ " where hi.INBOX_KEY > '201504' and hi.ALERT_TRANS_LVL_TYPE='DELIVERY' and hi.WORK_ORDER_KEY=w.WORK_ORDER_KEY and w.SERVICE_ITEM_GROUP_CODE='PS')";
			
			//System.out.println(Query);
			result = st.executeQuery(Query);

			writer.append("InboxKey");
			writer.append(',');
			writer.append("Generated ON");
			writer.append(',');
			writer.append("ShipNodeKey");
			writer.append(',');
			writer.append("ExceptionType");
			writer.append(',');
			writer.append("Status");
			writer.append(',');
			writer.append("FollowupDate");
			writer.append(',');
			writer.append("ErrorReason");
			writer.append(',');
			writer.append("CreateTs");
			writer.append(',');
			writer.append("ModifyTs");
			writer.append(',');
			writer.append("CreateUserId");
			writer.append(',');
			writer.append("ModifyUserId");
			writer.append(',');
			writer.append("CreateProgId");
			writer.append(',');
			writer.append("ModifyProgId");
			writer.append(',');
			writer.append("OrderHeaderKey");
			writer.append(',');
			writer.append("OrderNo");
			writer.append(',');
			writer.append("OrderLineKey");
			writer.append(',');
			writer.append("WorkOrderKey");
			writer.append(',');
			writer.append("WorkOrderNo");
			writer.append(',');
			writer.append("ServiceType");
			writer.append(',');
			writer.append("POHeaderKey");
			writer.append(',');
			writer.append("HDWillCallKey");
			writer.append(',');
			writer.append("AlertTransLevelType");
			writer.append(',');
			writer.append("WorkOrderType");
			writer.append(',');
			writer.append("CustomWorkOrderRef");
			writer.append(',');
			writer.append("ROHdrKey");
			writer.append(',');
			writer.append("ROLineKey");
			writer.append(',');
			writer.append("TagId");
			writer.append(',');
			writer.append("SellerOrganisationCode");

			writer.append('\n');

			while (result.next()) {
				//System.out.println("Inbox key:" + result.getString(1));
				// String app1=Integer.toString(i);
				writer.append("'" + result.getString(1));
				writer.append(',');
				writer.append(result.getString(2));
				writer.append(',');
				writer.append(result.getString(3));
				writer.append(',');
				writer.append(result.getString(4));
				writer.append(',');
				writer.append(result.getString(5));
				writer.append(',');
				writer.append(result.getString(6));
				writer.append(',');
				writer.append(result.getString(7));
				writer.append(',');
				writer.append(result.getString(8));
				writer.append(',');
				writer.append(result.getString(9));
				writer.append(',');
				writer.append(result.getString(10));
				writer.append(',');
				writer.append(result.getString(11));
				writer.append(',');
				writer.append(result.getString(12));
				writer.append(',');
				writer.append(result.getString(13));
				writer.append(',');
				writer.append("'" + result.getString(14));
				writer.append(',');
				writer.append(result.getString(15));
				writer.append(',');
				writer.append("'" + result.getString(16));
				writer.append(',');
				writer.append("'" + result.getString(17));
				writer.append(',');
				writer.append(result.getString(18));
				writer.append(',');
				writer.append(result.getString(19));
				writer.append(',');
				writer.append("'" + result.getString(20));
				writer.append(',');
				writer.append("'" + result.getString(21));
				writer.append(',');
				writer.append(result.getString(22));
				writer.append(',');
				writer.append(result.getString(23));
				writer.append(',');
				writer.append(result.getString(24));
				writer.append(',');
				writer.append("'" + result.getString(25));
				writer.append(',');
				writer.append("'" + result.getString(26));
				writer.append(',');
				writer.append(result.getString(27));
				writer.append(',');
				writer.append(result.getString(24));
				writer.append(',');
				writer.append('\n');

			}
			//System.out.println(("Selected"));
			result.close();
			st.close();
			sscConn.close();
			writer.flush();
			writer.close();

			// final String driverClass = "oracle.jdbc.driver.OracleDriver";
			Connection atcConn = dbConn.getJNDIConnection(Constants.STERLING_ATC_RW_JNDI);

			final String DelQuery = "delete from thd01.hd_inbox where inbox_key in (select h.inbox_key from thd01.hd_inbox h, thd01.yfs_work_order w where h.inbox_key > '201404' "
					+ " and h.alert_trans_lvl_type='DELIVERY' and h.work_order_type='O' "
					+ " and h.work_order_no=w.work_order_no "
					+ " and w.status in ('1400','1600')) or inbox_key in ( select h.inbox_key from thd01.hd_inbox h, THD01.hd_related_work_order w where h.inbox_key > '201404' and h.alert_trans_lvl_type='DELIVERY' and h.work_order_type='C' and h.work_order_no=w.related_work_order_no "
					+ " and w.status in ('1400','1600')) "
					+ " or "
					+ " inbox_key in "
					+ "(select  hdi.inbox_key "
					+ " from thd01.yfs_order_line yol, thd01.hd_inbox hdi, THD01.yfs_order_release_status yors, thd01.yfs_order_header yoh "
					+ " where yol.order_header_key = yors.order_header_key and yol.order_line_key = yors.order_line_key and "
					+ " yol.order_header_key = hdi.order_header_key and yol.order_line_key = hdi.order_line_key "
					+ " and hdi.exception_type not like 'MANUAL_ALERT%' and yol.line_type in ('IN','ME') "
					+ " and yol.order_header_key > '201404' and yors.status in ('3700.7777', '3700','9000') and yol.extn_svs_line_status in ( '1000', '1300') "
					+ " and yors.status_quantity > 0 and yol.order_header_key=yoh.order_header_key ) or "
					+ " inbox_key in "
					+ " (select hi.inbox_key from "
					+ " thd01.yfs_order_header oh, thd01.yfs_order_release_status ors, thd01.hd_inbox hi where oh.order_header_key > '20140401' "
					+ " and ors.order_release_status_key > '20140401'and hi.inbox_key > '20140401' and oh.document_type = '0005' and oh.extn_svs_order_status = '1000' "
					+ " and ors.order_header_key = oh.order_header_key and ors.status >= '3900' and ors.status_quantity > '0' and oh.order_header_key = hi.PO_HDR_KEY "
					+ " and hi.status = 'OPEN' ) or"
					+ " inbox_key in "
					+ " (select hi.inbox_key from thd01.hd_inbox hi, THD01.YFS_WORK_ORDER w "
					+ " where hi.INBOX_KEY > '201504' and hi.ALERT_TRANS_LVL_TYPE='DELIVERY' and hi.WORK_ORDER_KEY=w.WORK_ORDER_KEY and w.SERVICE_ITEM_GROUP_CODE='PS')";
					
			Connection con1 = null;

			
			Statement stmt1 = null;
			stmt1 = atcConn.createStatement();
			int rowsDeleted = stmt1.executeUpdate(DelQuery);
			atcConn.commit();
			stmt1.close();
			atcConn.close();
			//System.out.println(rowsDeleted + " Rows Deleted");

			sendAttachment(rowsDeleted);
		} catch (Exception e) {
						
			logger.debug("Error occured while Executing DeleteKeys Servlet under DeleteUnclosedInboxKeys Project : "
					+ GregorianCalendar.getInstance() + e);
			e.printStackTrace();
		}
	}

	public static void sendAttachment(int rowsDeleted) throws Exception {
		String host = "mail1.homedepot.com";
		String from = "horizon@cpliisad.homedepot.com";
		String[] to = { "VINAY_D_NARAYANAMURTHY1@homedepot.com",
				"RAGEESH_THEKKEYIL1@homedepot.com",
				"ANAND_RATHINAM@homedepot.com",
				"DAVIS_THUDIYAN_ANTONY1@homedepot.com" };
		String[] Cc = { "_2fc77b@homedepot.com" };
		// String[] to = { "shwetha_ravi@homedepot.com" };
		 //String[] Cc = { "shwetha_ravi@homedepot.com" };
		// Get system properties
		Properties properties = System.getProperties();

		// Setup mail server
		properties.setProperty("mail.smtp.host", host);

		// Get the default Session object.
		Session session = Session.getDefaultInstance(properties);

		// Define message
		Message message = new MimeMessage(session);
		message.setFrom(new InternetAddress(from));
		InternetAddress[] addressTo = new InternetAddress[to.length];
		InternetAddress[] addressCc = new InternetAddress[Cc.length];
		for (int i = 0; i < to.length; i++) {
			addressTo[i] = new InternetAddress(to[i]);
			//System.out.println("To Address " + to[i]);
		}
		for (int j = 0; j < Cc.length; j++) {
			addressCc[j] = new InternetAddress(Cc[j]);
			//System.out.println("Cc Address " + Cc[j]);
		}
		message.setRecipients(RecipientType.TO, addressTo);
		message.setRecipients(RecipientType.CC, addressCc);
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a");
		// get current date time with Date()
		Date today = new Date();
		String date1 = dateFormat.format(today);
		message.setSubject("Worklist Cleanup for Orders in terminal status/Install Orders @ "
				+ date1);

		// Create the message part
		BodyPart messageBodyPart = new MimeBodyPart();
		String msgContent = "Hi All,<br/><br/>PFA report for inbox keys which were deleted today.<br/>Number of rows deleted: "
				+ rowsDeleted + "<br/><br/>Thanks<br/>";
		msgContent = msgContent + "COM Multichannel Support";
		// Fill the message
		messageBodyPart.setContent(msgContent, "text/html");

		Multipart multipart = new MimeMultipart();
		multipart.addBodyPart(messageBodyPart);

		// Part two is attachment
		messageBodyPart = new MimeBodyPart();
		 //String filename = "C:\\store\\InboxKey.csv";
		String filename = "/opt/isv/tomcat-6.0.18/temp/InboxKey.csv";
		DataSource source = new FileDataSource(filename);
		messageBodyPart.setDataHandler(new DataHandler(source));
		messageBodyPart.setFileName(filename);
		multipart.addBodyPart(messageBodyPart);

		// Put parts in message
		message.setContent(multipart);

		// Send the message
		Transport.send(message);
		//System.out.println("Msg Send ....");
	}

	public static Connection getConnection(String db) throws Exception {
		Connection con = null;
		try {
			// File file = new File("C:\\store");
			File file = new File(
					"/opt/isv/tomcat-6.0.18/webapps/ConnProperties");
			URL[] urls = { file.toURI().toURL() };
			ClassLoader loader = new URLClassLoader(urls);
			ResourceBundle bundle = ResourceBundle.getBundle("DBConnection",
					Locale.getDefault(), loader);

			final String comDriverClass = bundle.getString("oracleDriverClass");
			final String connectionURL = bundle.getString(db);
			final String uName = bundle.getString("UserID");
			final String uPassword = bundle.getString("UserPassword");
			Class.forName(comDriverClass);

			con = DriverManager.getConnection(connectionURL, uName, uPassword);

		} catch (Exception e) {
			
			logger.debug("Exception while creating " + db
					+ " connection : " + e);
			throw e;
		}
		return con;
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
