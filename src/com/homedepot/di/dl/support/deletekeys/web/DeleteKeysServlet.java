package com.homedepot.di.dl.support.deletekeys.web;





import java.io.IOException;
import java.sql.SQLException;
import java.util.GregorianCalendar;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.homedepot.di.dl.support.ErrorDeltas.web.DeltaServlet;
import com.homedepot.di.dl.support.ddInstrUpdate.dao.DDInstrUpdateDAO;
import com.homedepot.di.dl.support.deletekeys.dao.DeleteKeys;
import com.homedepot.di.dl.support.updateOrdersToDoneStatus.dao.ZeroRetail;

/**
 * Servlet implementation class DDInstrUpdateServlet
 */
public class DeleteKeysServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	final static Logger logger = Logger.getLogger(DeleteKeysServlet.class);
    /**
     * Default constructor. 
     */
    public DeleteKeysServlet() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		logger.info("Delete Keys Servlet Started : "+ GregorianCalendar.getInstance().getTime());
		DeleteKeys ikey=new DeleteKeys();
		try {
			DeleteKeys.DeleteKey();
		} catch (SQLException e) {
			logger.debug("Delete Keys Servlet Exception : "+ GregorianCalendar.getInstance().getTime() + e);
			e.printStackTrace();
		} catch (Exception e) {
			logger.debug("Delete Keys Servlet Exception : "+ GregorianCalendar.getInstance().getTime() + e);
			e.printStackTrace();
		}
		logger.info("Delete Keys Servlet Completed : "+ GregorianCalendar.getInstance().getTime());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}

