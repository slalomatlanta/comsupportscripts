package com.homedepot.di.dl.support.psmRefundPaymentFailures.dao;

import java.io.IOException;
import java.net.MalformedURLException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.homedepot.di.dl.support.util.Constants;
import com.homedepot.di.dl.support.util.DBConnectionUtil;
import com.homedepot.ta.aa.log4j.OrangeLogHelper;

/**
 *  
 * @author MXC4563 | COM Support Scripts | 07-13-2016
 *
 */
public class PsmPaymentDAO {

	private static final Logger logger = Logger.getLogger(PsmPaymentDAO.class);
	String threadName = Thread.currentThread().getName();
	//private static final Logger oLogger = Logger.getLogger(OrangeLogHelper.getLoggerNameUsingTomcatClassLoaderAndTheStack());
	private static final Logger oLogger = Logger.getLogger("splunkLog");
	static Map<String,String> orangeLogMsgMap = new HashMap<String, String>();	

	public static void main(String[] args) throws SQLException,
			ClassNotFoundException, IOException {
		StringBuilder emailText = new StringBuilder();
		emailText = getPaymentFailures();
		logger.info(emailText.toString());
	}

	public static StringBuilder getPaymentFailures() throws SQLException,
			ClassNotFoundException, MalformedURLException {
		
		Connection con = null;
		ResultSet rs = null;
		Statement stmt = null;
		StringBuilder emailText = new StringBuilder();
		StringBuilder startText = new StringBuilder();
	
		try {

			final String driverClass = "oracle.jdbc.driver.OracleDriver";
			Class.forName(driverClass);
			String STERLING_SSC_RO_JNDI = Constants.STERLING_SSC_RO_JNDI;
			DBConnectionUtil db = new DBConnectionUtil();
			con = db.getJNDIConnection(STERLING_SSC_RO_JNDI);
			stmt = con.createStatement();
		
			String query = "select oh.EXTN_HOST_ORDER_REF,  hi.INVOICE_NO , hi.PAY_TRAN_TYPE , hi.STATUS "
					+" from THD01.hd_invoice hi, THD01.yfs_order_header oh "
					+ " where hi.pay_tran_type in ('PSM', 'REFUND') and hi.status in ('PENDING', 'FAILED') "
					+ " and hi.order_header_key=oh.order_header_key"
					+ " and hi.MODIFYTS>= sysdate-12/24";
			logger.info("****QUERY: "+query);
			
			
			rs = stmt.executeQuery(query);
			if (rs.next()) {

				emailText.append("<tr bgcolor=\"#768976\">");
				emailText.append("<th>");
				emailText.append("Extn Host Order Reference");
				emailText.append("</th>");
				emailText.append("<th>");
				emailText.append("INVOICE_NO");
				emailText.append("</th>");
				emailText.append("<th>");
				emailText.append("PAY_TRAN_TYPE");
				emailText.append("</th>");
				emailText.append("<th>");
				emailText.append("STATUS");
				emailText.append("</th>");
				emailText.append("</tr>");
				emailText.append('\n');
				do {
					emailText.append("<font face=\"calibri\">");
					emailText.append("<tr>");
					emailText.append("<td>");
					String orderNum = rs.getString(1);
					emailText.append(orderNum);
					orangeLogMsgMap.put("Extn Host Order Reference",orderNum );
					
					emailText.append("</td>");
					emailText.append("<td>");
					String invoiceNum = rs.getString(2);
					emailText.append(invoiceNum);
					orangeLogMsgMap.put("INVOICE_NO",invoiceNum );

					emailText.append("</td>");
					emailText.append("<td>");
					String tranType = rs.getString(3);
					emailText.append(tranType);
					orangeLogMsgMap.put("PAY_TRAN_TYPE",tranType );

					emailText.append("</td>");
					emailText.append("<td>");
					String status = rs.getString(4);
					emailText.append(status);
					orangeLogMsgMap.put("STATUS",status );
					emailText.append("</td>");
					emailText.append("</tr></tr>");
				} 
				while (rs.next());				
			}
			stmt.close();
			rs.close();
			con.close();

			logger.info("******Final ********EmailText: "+emailText);
			oLogger.info("Program=getPaymentFailures Orders=" + orangeLogMsgMap);
		} catch (Exception e) {
			logger.info(e.getStackTrace());
		} finally {

		}
		return startText.append(emailText);

	}	
}


