package com.homedepot.di.dl.support.psmRefundPaymentFailures.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;

import com.homedepot.di.dl.support.psmRefundPaymentFailures.dao.PsmPaymentDAO;

/**
 *  
 * @author MXC4563 | COM Support Scripts | 07-13-2016
 *
 */
public class PaymentFailureStatusServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	final static Logger logger = Logger
			.getLogger(PaymentFailureStatusServlet.class);

		public PaymentFailureStatusServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		String msg = null;
		response.setContentType("text/html");
		PrintWriter out = null;
		MimeMessage message = null;
		try {
			msg = "Success";

			StringBuilder daoText = PsmPaymentDAO.getPaymentFailures();
			logger.debug("daoText: " + daoText);
			if(daoText.length() > 0){
				logger.debug("Inside Send Email...sending the email now");
				message = this.buildEmailMessage(daoText);
				Transport.send(message);
			} else {
				logger.debug("dao returned null rows...not sending the email");
			}
		} catch (Exception e) {
			msg = "Failure";
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		out = response.getWriter();
		out.println(msg);
		out.close();

	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
	}

	private MimeMessage buildEmailMessage(StringBuilder messageTxt) {

		String eMailSubject = null;
		PsmPaymentDAO dao = new PsmPaymentDAO();
		eMailSubject = "PSM, REFUND Transactions which are in Failed or Pending Status";

		String emailRecipient = "_2fc77b@homedepot.com"; // COM Multichannel DL

		Session session = null;
		MimeMessage mimeMessage = null;
		MimeBodyPart mimeBodyPart = null;
		Multipart multiPart = null;
		String date = null;
		Calendar calendar = null;
		Properties properties = null;

		StringBuilder text = new StringBuilder();

		text.append("<html><body><table>");
		text.append("<font face=\"calibri\"> <tr>");
		text.append("<p>Hello All,</p>");
		text.append("<p>	Below are the orders PSM/REFUND Transaction Types which failed.</p><br></tr>");

		text.append("<tr><table border=\"1\">\n");
		text.append("<font face=\"calibri\">");

		text.append(messageTxt);

		text.append("</table>");
		text.append("<p>&nbsp;</p>");
		text.append('\n');
		text.append("<tr><p>Thanks,</p></tr>");
		text.append("<tr><p>COM Multi-Channel Support</p><br></tr>");
		text.append(System.getProperty("line.separator"));
		text.append("<p>Note: This is a system-generated e-mail. Please do NOT reply to it.</p><br>");
		text.append("</table></body>");
		text.append("</html>");

		calendar = Calendar.getInstance();
		date = calendar.getTime().toString();
		properties = System.getProperties();
		properties.put("mail.host", "mail1.homedepot.com");
		session = Session.getInstance(properties, null);

		try {
			mimeMessage = new MimeMessage(session);
			mimeMessage.setFrom(new InternetAddress(
					"horizon@cpliisad.homedepot.com"));
			mimeMessage.setRecipients(Message.RecipientType.TO,
					InternetAddress.parse(emailRecipient, false));
			mimeMessage.setSubject(eMailSubject + date);
			mimeBodyPart = new MimeBodyPart();
			mimeBodyPart.setText(text.toString());
			multiPart = new MimeMultipart();
			multiPart.addBodyPart(mimeBodyPart);
			mimeMessage.setContent(text.toString(), "text/html");
		} catch (Exception e) {

		}

		return mimeMessage;
	}

}
