package com.homedepot.di.dl.support.memManager.service;

import java.io.File;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.log4j.Logger;

import com.google.gson.Gson;

import com.homedepot.di.dl.support.memManager.dto.MemManagerDTO;
import com.homedepot.di.dl.support.switchToNormal.dao.SwitchTransmissionState;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;

public class ExecuteThread {
	final static Logger logger = Logger.getLogger(ExecuteThread.class);

	public static void main(String args[])

	{
		String res = loadMetadata("QA");
		ArrayList<MemManagerDTO> finalList = memManagerWithThreads("QA");
	}
	
	public static String loadMetadata(String env)
	{
		
		//String url = "http://multichannel-custom.homedepot.com/COMOrder/rs/modifySalesOrderForServicesSynchronously";
		
		String url = "";
		if (env.equals("PR")) {
			url = "http://multichannel-custom.homedepot.com/COM-COMMemManagerService/rest/load/MetaData?APIContextType=TranslateBarCode&HashType=skucode";
		} else {
			url = "http://multichannel-custom-"+env+".homedepot.com/COM-COMMemManagerService/rest/load/MetaData?APIContextType=TranslateBarCode&HashType=skucode";
		}

				
		String source = "database&datastore:hash&outputformat:string&separator:|&strategy:endswith&userownum:y&rownumrange:5000&validateloaddata:y&hashtype:skucode&buckethash:Y&key:alias_value&"
				+ "sqlquery:select alias_value as \"0\", item_id as \"1\", uom as \"2\", kit_code as \"3\", status as \"4\" from (select w.alias_value, b.item_id,b.uom, b.status, b.kit_code, ROW_NUMBER() OVER (order by w.alias_value) R from yfs_item_alias w, yfs_item b WHERE w.alias_name  = 'SKU' AND w.item_key = b.item_key and trim(b.item_id) like '?')";

		Client client = ClientManager.getClient();

		WebResource webResource = ((com.sun.jersey.api.client.Client) client)
				.resource(url);

		String response = webResource.type("text/plain").post(String.class, source);
		
		
		return response;
	}
	
	public static ArrayList<MemManagerDTO> memManagerWithThreads(String env){
//		String filelocation = "C:\\test\\MemManager\\Q2result.csv";
//		boolean check = deleteFile(filelocation);
		ArrayList<MemManagerDTO> finalList = new ArrayList<MemManagerDTO>();
		MemmanagerQuery1 memObj = new MemmanagerQuery1(); 
		
//		if (check) {
			for (int b = 0; b <= 9; b++)

			{
				MemmanagerQuery1 R1 = new MemmanagerQuery1(b, env);
				R1.start();
			}
			
			for (Thread t : memObj.threadList){
				try {
					// starting from the first wait for each one to finish.
					t.join();
				} catch (InterruptedException e) {
					logger.error("Exception occurred in thread join: "	+ e);
				}
			}
			
			finalList = memObj.outputList;
			
//		}
		return finalList;
	}

//	public static boolean deleteFile(String filelocation) {
//		boolean check = false;
//		File f = new File(filelocation);
//		if (f.exists() && !f.isDirectory()) {
//			System.out.println("Deleting Old File");
//			if (f.delete()) {
//				check = true;
//				System.out.println("Old file deleted");
//			} else {
//				System.out.println("Error in deleting file");
//			}
//		} else {
//			System.out
//					.println("File doesn't exists - Proceeding with next steps");
//			check = true;
//		}
//		return check;
//	}

}
