package com.homedepot.di.dl.support.memManager.service;

import java.io.FileWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeMessage.RecipientType;

import org.apache.log4j.Logger;

import com.homedepot.di.dl.support.memManager.dto.MemManagerDTO;
import com.homedepot.di.dl.support.switchToNormal.dao.SwitchTransmissionState;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;

public class MemmanagerQuery {
	final static Logger logger = Logger.getLogger(MemmanagerQuery.class);

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		ArrayList<MemManagerDTO> outputList = memManagerWithOutThreads("Q1");
	}

	public static ArrayList<MemManagerDTO> memManagerWithOutThreads(String env) {
		ArrayList<MemManagerDTO> outputList = new ArrayList<MemManagerDTO>();
		int b = 0;
		
		try {
//			FileWriter writer = new FileWriter("C:\\store\\Q2result.csv");
//			writer.append("BatchNo");
//			writer.append(',');
//			writer.append("FailedKeyCount");
//			writer.append(',');
//			writer.append("TotalKeyCount");
//			writer.append('\n');

			for (b = 0; b < 10; b++)

			{

				StringBuilder emailText = new StringBuilder();

				// String app="";

				// FileWriter writer = new
				// FileWriter("/opt/isv/tomcat-6.0.18/temp/Q2result.csv");

				emailText.append(System.getProperty("line.separator"));

				Calendar cal = Calendar.getInstance();
				
				DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
				String appToDate = dateFormat.format(cal.getTime());
				logger.debug(appToDate);

				// String inputFile = "/opt/isv/tomcat-6.0.18/temp/Stores.csv"

				String requestxml = "<?xml version='1.0' encoding='UTF-8' standalone='yes'?>"
						+ "<hdDataLoadRequest>"
						+ "<APIContextType>TranslateBarCode</APIContextType>"
						+ "<batchNo>"
						+ b
						+ "</batchNo>"
						+ "<hashType>skucode</hashType>"
						+ "</hdDataLoadRequest>";

				logger.debug("requestxml:" + requestxml);

				String responseXml = callQ2Service(requestxml, env);

				logger.debug(responseXml);

				String responsefailedkeycount = CheckResponseType(responseXml);
				String responseTotalKeyCount = CheckResponseType1(responseXml);

				logger.info(responsefailedkeycount);
				logger.info(responseTotalKeyCount);

				String app1 = Integer.toString(b);
				
				MemManagerDTO md = new MemManagerDTO();
				md.setsNo(app1);
				md.setFailureCount(responsefailedkeycount);
				md.setTotalKey(responseTotalKeyCount);
				outputList.add(md);
				
//				writer.append(app1);
//				writer.append(',');
//				writer.append(responsefailedkeycount);
//				writer.append(',');
//				writer.append(responseTotalKeyCount);
//				writer.append(',');
//				writer.append('\n');

			}

//			writer.flush();
//			writer.close();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}
		try {
//			sendAttachment();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return outputList;

	}

	private static String callQ2Service(String inputXML, String env) {

		String url = "";
		if (env.equals("PR")) {
			url = "http://multichannel-custom.homedepot.com/COM-COMMemManagerService/rest/load/All";
		} else {
			url = "http://multichannel-custom-"+env+".homedepot.com/COM-COMMemManagerService/rest/load/All";
		}
		
		logger.debug(url);
		
		Client client = ClientManager.getClient();

		WebResource webResource = ((com.sun.jersey.api.client.Client) client)
				.resource(url);
		String outputXML = webResource.type("application/xml").post(
				String.class, inputXML);
		return outputXML;
	}

	private static String CheckResponseType(String response) {

		String result = "";
		if (response == null || "".equalsIgnoreCase(response)) {
			result = "No response";
		} else if (response
				.contains("<failedKeyCount>0</failedKeyCount><totalKeyCount>")) {
			int firstIndex = response
					.indexOf("<hdDataLoadResponse><failedKeyCount");
			firstIndex = firstIndex + 20;
			int lastIndex = response.indexOf("<totalKeyCount>");
			result = response.substring(firstIndex, lastIndex - 0);
		}

		logger.debug("Response status : " + result);
		return result;

	}

	private static String CheckResponseType1(String response1) {

		String result1 = "";
		if (response1 == null || "".equalsIgnoreCase(response1)) {
			result1 = "No response";
		} else if (response1
				.contains("<failedKeyCount>0</failedKeyCount><totalKeyCount>")) {
			int firstIndex = response1.indexOf("</failedKeyCount>");
			firstIndex = firstIndex + 17;
			int lastIndex = response1.indexOf("</hdDataLoadResponse>");
			result1 = response1.substring(firstIndex, lastIndex - 0);
		}

		logger.debug("Response1 status : " + result1);
		return result1;

	}

//	public static void sendAttachment() throws Exception {
//		String host = "mail1.homedepot.com";
//		String from = "horizon@cpliisad.homedepot.com";
//		// String[] to = {"_2fc77b@homedepot.com"};
//		// String[] Cc ={"_275769@homedepot.com"};
//		String[] to = { "Anandhakumar_Chinnadurai@homedepot.com" };
//		String[] Cc = { "Anandhakumar_Chinnadurai@homedepot.com" };
//		// Get system properties
//		Properties properties = System.getProperties();
//
//		// Setup mail server
//		properties.setProperty("mail.smtp.host", host);
//
//		// Get the default Session object.
//		Session session = Session.getDefaultInstance(properties);
//
//		// Define message
//		Message message = new MimeMessage(session);
//		message.setFrom(new InternetAddress(from));
//		InternetAddress[] addressTo = new InternetAddress[to.length];
//		InternetAddress[] addressCc = new InternetAddress[Cc.length];
//		for (int i = 0; i < to.length; i++) {
//			addressTo[i] = new InternetAddress(to[i]);
//			System.out.println("To Address " + to[i]);
//		}
//		for (int j = 0; j < Cc.length; j++) {
//			addressCc[j] = new InternetAddress(Cc[j]);
//			System.out.println("Cc Address " + Cc[j]);
//		}
//		message.setRecipients(RecipientType.TO, addressTo);
//		message.setRecipients(RecipientType.CC, addressCc);
//		message.setSubject("Q2Result-MemManagerLoadQuery");
//
//		// Create the message part
//		BodyPart messageBodyPart = new MimeBodyPart();
//		String msgContent = "Hi All,<br/><br/>PFA report <br/><br/>Thanks<br/>";
//		msgContent = msgContent + "COM Multichannel Support";
//		// Fill the message
//		messageBodyPart.setContent(msgContent, "text/html");
//
//		Multipart multipart = new MimeMultipart();
//		multipart.addBodyPart(messageBodyPart);
//
//		// Part two is attachment
//		messageBodyPart = new MimeBodyPart();
//		String filename = "C:\\store\\Q2result.csv";
//		// String filename = "/opt/isv/tomcat-6.0.18/temp/Q2result.csv";
//		DataSource source = new FileDataSource(filename);
//		messageBodyPart.setDataHandler(new DataHandler(source));
//		messageBodyPart.setFileName(source.getName());
//		multipart.addBodyPart(messageBodyPart);
//
//		// Put parts in message
//		message.setContent(multipart);
//
//		// Send the message
//		Transport.send(message);
//		System.out.println("Msg Send ....");
//	}
}
