package com.homedepot.di.dl.support.memManager.service;

import java.io.FileWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import org.apache.log4j.Logger;

import com.homedepot.di.dl.support.insertHDEventsAndServiceValidation.service.InsertHDEventsAndUpdateHDPSI;
import com.homedepot.di.dl.support.memManager.dto.MemManagerDTO;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;

public class MemmanagerQuery1 implements Runnable

{
	final static Logger logger = Logger.getLogger(MemmanagerQuery1.class);
	public void run() {
		// TODO Auto-generated method stub
		logger.debug("run :" + threadName);
		try {
//			FileWriter writer = new FileWriter(
//					"C:\\test\\MemManager\\Q2result.csv", true);
//			writer.append("BatchNo");
//			writer.append(',');
//			writer.append("FailedKeyCount");
//			writer.append(',');
//			writer.append("TotalKeyCount");
//			writer.append('\n');

			StringBuilder emailText = new StringBuilder();

			// String app="";

			// FileWriter writer = new
			// FileWriter("/opt/isv/tomcat-6.0.18/temp/Q2result.csv");

			emailText.append(System.getProperty("line.separator"));

			Calendar cal = Calendar.getInstance();

			DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
			String appToDate = dateFormat.format(cal.getTime());
			logger.debug(appToDate);

			// String inputFile = "/opt/isv/tomcat-6.0.18/temp/Stores.csv"

			logger.debug(threadName);

			String requestxml = "<?xml version='1.0' encoding='UTF-8' standalone='yes'?>"
					+ "<hdDataLoadRequest>"
					+ "<APIContextType>TranslateBarCode</APIContextType>"
					+ "<batchNo>"
					+ threadName
					+ "</batchNo>"
					+ "<hashType>skucode</hashType>" + "</hdDataLoadRequest>";

			logger.debug("requestxml:" + requestxml);

			String responseXml = callQ2Service(requestxml, envName);

			logger.debug(responseXml);

			String responsefailedkeycount = CheckResponseType(responseXml);
			String responseTotalKeyCount = CheckResponseType1(responseXml);

			logger.debug(responsefailedkeycount);
			logger.debug(responseTotalKeyCount);

			MemManagerDTO md = new MemManagerDTO();
			md.setsNo(threadName);
			md.setFailureCount(responsefailedkeycount);
			md.setTotalKey(responseTotalKeyCount);
			outputList.add(md);
			
			
//			writer.append(threadName);
//			writer.append(',');
//			writer.append(responsefailedkeycount);
//			writer.append(',');
//			writer.append(responseTotalKeyCount);
//			writer.append(',');
//			writer.append('\n');
//
//			writer.flush();
//			writer.close();

		}

		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private Thread t;
	private String threadName;
	private String envName;
	public static ArrayList<MemManagerDTO> outputList = new ArrayList<MemManagerDTO>();
	public static ArrayList<Thread> threadList = new ArrayList<Thread>();

	public MemmanagerQuery1(int c, String env) {
		// TODO Auto-generated constructor stub
		threadName = Integer.toString(c);
		this.envName = env;
		logger.debug("Creating " + threadName);

	}
	
	public MemmanagerQuery1() {
		// TODO Auto-generated constructor stub
		outputList.clear();
		threadList.clear();
	}

	public void start() {
		
		logger.debug("Starting " + threadName);
		if (t == null) {
			t = new Thread(this, threadName);
			threadList.add(t);
			t.start();
		}
	}

	private static String callQ2Service(String inputXML, String env) {

		String url = "";
		if (env.equals("PR")) {
			url = "http://multichannel-custom.homedepot.com/COM-COMMemManagerService/rest/load/All";
		} else {
			url = "http://multichannel-custom-"+env+".homedepot.com/COM-COMMemManagerService/rest/load/All";
		}
		
		logger.debug(url);

		Client client = ClientManager.getClient();

		WebResource webResource = ((com.sun.jersey.api.client.Client) client)
				.resource(url);
		String outputXML = webResource.type("application/xml").post(
				String.class, inputXML);
		return outputXML;
	}

	private static String CheckResponseType(String response) {

		String result = "";
		if (response == null || "".equalsIgnoreCase(response)) {
			result = "No response";
		} else if (response
				.contains("<failedKeyCount>0</failedKeyCount><totalKeyCount>")) {
			int firstIndex = response
					.indexOf("<hdDataLoadResponse><failedKeyCount");
			firstIndex = firstIndex + 20;
			int lastIndex = response.indexOf("<totalKeyCount>");
			result = response.substring(firstIndex, lastIndex - 0);
		}

		logger.debug("Response status : " + result);
		return result;

	}

	private static String CheckResponseType1(String response1) {

		String result1 = "";
		if (response1 == null || "".equalsIgnoreCase(response1)) {
			result1 = "No response";
		} else if (response1
				.contains("<failedKeyCount>0</failedKeyCount><totalKeyCount>")) {
			int firstIndex = response1.indexOf("</failedKeyCount>");
			firstIndex = firstIndex + 17;
			int lastIndex = response1.indexOf("</hdDataLoadResponse>");
			result1 = response1.substring(firstIndex, lastIndex - 0);
		}

		logger.debug("Response1 status : " + result1);
		return result1;

	}

}
