package com.homedepot.di.dl.support.memManager.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.homedepot.di.dl.support.memManager.dto.MemManagerDTO;
import com.homedepot.di.dl.support.memManager.service.ExecuteThread;
import com.homedepot.di.dl.support.memManager.service.MemmanagerQuery;
import com.homedepot.di.dl.support.memManager.service.MemmanagerQuery1;


/**
 * Servlet implementation class MemManagerLoadServlet
 */
public class MemManagerLoadServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	final static Logger logger = Logger.getLogger(MemManagerLoadServlet.class);   
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MemManagerLoadServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html");
		PrintWriter out = null;
		out = response.getWriter();
		String outputString = "";
		out.println(outputString);
		if (request.getParameter("env")!=null){
			String env = request.getParameter("env");
			out.println("Environment : "+env);
			out.println("</br>");
			String status = ExecuteThread.loadMetadata(env);
			if("OK".equalsIgnoreCase(status))
			{
			if (request.getParameter("threads")!=null){
				String threads = request.getParameter("threads");
				boolean isScriptUsingThreads = Boolean.parseBoolean(threads);
				out.println("Using Threads : "+isScriptUsingThreads);
				if (isScriptUsingThreads){
//					calling mem manager program using threads
					ArrayList<MemManagerDTO> outputList = ExecuteThread.memManagerWithThreads(env);
					out.println("</br>");
					out.println("<html><body><font face=\"calibri\"><table border=\"1\" cellpadding=\"0\" cellspacing=\"0\" width=\"200px\" style=\"border-collapse:collapse;\">"
							+ "<tr><td>Batch No</td><td>Failed Key Count</td><td>Total Key Count</td></tr>");
					for (MemManagerDTO md : outputList){
						out.println("<tr><td>"+md.getsNo()+"</td><td>"+md.getFailureCount()+"</td><td>"+md.getTotalKey()+"</td></tr>");
					}
					out.println("</table></font></body></html>");
				}
				else{
//					calling mem manager program with out using threads
					ArrayList<MemManagerDTO> outputList = MemmanagerQuery.memManagerWithOutThreads(env);
					out.println("</br>");
					out.println("<html><body><font face=\"calibri\"><table border=\"1\" cellpadding=\"0\" cellspacing=\"0\" width=\"200px\" style=\"border-collapse:collapse;\">"
							+ "<tr><td>Batch No</td><td>Failed Key Count</td><td>Total Key Count</td></tr>");
					for (MemManagerDTO md : outputList){
						out.println("<tr><td>"+md.getsNo()+"</td><td>"+md.getFailureCount()+"</td><td>"+md.getTotalKey()+"</td></tr>");
					}
					out.println("</table></font></body></html>");
				}
			}
			else{
				out.println("threads paramaeter is not passed");
			}
		}
			
			else
			{
				logger.debug("MetaData load failed");
			}
	}
		else{
			out.println("env paramaeter is not passed");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
