package com.homedepot.di.dl.support.memManager.dto;

public class MemManagerDTO {
	private String failureCount;  
	private String totalKey;
	private String sNo;
	public String getFailureCount() {
		return failureCount;
	}
	public void setFailureCount(String failureCount) {
		this.failureCount = failureCount;
	}
	
	public String getTotalKey() {
		return totalKey;
	}
	public void setTotalKey(String totalKey) {
		this.totalKey = totalKey;
	}
	public String getsNo() {
		return sNo;
	}
	public void setsNo(String sNo) {
		this.sNo = sNo;
	}
	
	
}
