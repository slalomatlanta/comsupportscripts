package com.homedepot.di.dl.support.blockingLockmonitor.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.GregorianCalendar;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.homedepot.di.dl.support.blockingLockmonitor.dao.MonitorLock;
import com.homedepot.di.dl.support.bossHealthcheck.web.HealthReportServlet;

/**
 * Servlet implementation class MonitorLockServlet
 */
public class MonitorLockServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	final static Logger logger = Logger.getLogger(MonitorLockServlet.class);  
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MonitorLockServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		// TODO Auto-generated method stub
		logger.info("Blocking Lock Monitor Started : "+ GregorianCalendar.getInstance().getTime());
		MonitorLock lock = new MonitorLock();
        String mail = request.getParameter("mailFlag");
		String display = "<html><head><body bgcolor=\"#676767\">" + "<font face=\"calibri\">";
    	display = display + "<h2><center><b><font color=\"White\">Sterling Blocking Lock Details</b></center></h2><br/>";
    	display = display + "<h3><font color=\"White\">DPR77mm </h3></p>";
		String tableHdr = "<table border=\"1\" cellpadding=\"1\" cellspacing=\"0\" width=\"200px\" style=\"border-collapse:collapse;\" bgcolor=\"#F2F2F2\">"
				+ "<font face=\"calibri\">";
		
		String session = "";
		try {
			session = lock.checkForblockingLocks(mail);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.debug("Blocking Lock Monitor Exception : "+ GregorianCalendar.getInstance().getTime()+" : "+e.getMessage());
			e.printStackTrace();
		}
		
		String displayContent = display + tableHdr + session + "</table></center></body><html>";
		PrintWriter out = response.getWriter(); 
	    response.setContentType("text/html"); 
	    out.println(displayContent);
	    out.close();
		logger.info("Blocking Lock Monitor Completed : "+ GregorianCalendar.getInstance().getTime());
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
