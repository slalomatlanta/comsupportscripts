package com.homedepot.di.dl.support.blockingLockmonitor.dto;

public class MonitorLockDTO 
{

	public Boolean ifExists;
	public String session;
	public String response;
	public String[] mailID;

	public String[] getMailID() {
		return mailID;
	}

	public void setMailID(String[] mailID) {
		this.mailID = mailID;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public String getSession() {
		return session;
	}

	public void setSession(String session) {
		this.session = session;
	}

	public Boolean getIfExists() {
		return ifExists;
	}

	public void setIfExists(Boolean ifExists) {
		this.ifExists = ifExists;
	}
	
}
