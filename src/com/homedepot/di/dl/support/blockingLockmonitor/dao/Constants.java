package com.homedepot.di.dl.support.blockingLockmonitor.dao;

public class Constants {
	
	public String blockingQuery = "select /*+ rule */"
			+" 'EXECUTE KILL_GIVEN_SESSION(' || a.sid || ',' || a.serial# || ',' || a.inst_id || ',' || '''' || 'DXJ6077' || '''' || ',' || '''' || 'Blocking Session' || '''' || ')' as QueryToExecute,"
			+" a.inst_id ||',' || a.sid || ',' || a.serial# blk_sess,a.status  BLOCKER_STATUS,"
			+" a.username blocker,a.machine,a.module,"
			+" w.ctime DURATION_SECONDS,"
			+"'select * from '||OWNER||'.'||OBJECT_NAME||' where rowid = '''||"
			+" dbms_rowid.rowid_create( 1, b.ROW_WAIT_OBJ#,b.ROW_WAIT_FILE#,b.ROW_WAIT_BLOCK#,b.ROW_WAIT_ROW#)||''';' Record, "
			+" b.inst_id||','||b.sid || ',' || b.serial# wtr_sess,b.status WAITER_STATUS,"
			+" b.username waiter,b.ROW_WAIT_FILE#,b.ROW_WAIT_BLOCK#,b.ROW_WAIT_ROW#,"
			+" o.owner || '.' || o.object_name ||"
			+" nvl2 (subobject_name, '.' || subobject_name, null) blocked_object,"
			+" a.client_info  BLOCKER_CLIENT_INFO,b.client_info WAITER_CLIENT_INFO"
			+" from gv$lock h, gv$lock w, gv$session a, gv$session b, dba_objects o"
			+" where h.block   != 0"
			+" and h.lmode   != 0"
			+" and h.lmode   != 1"
			+" and w.request != 0 "
			+" and w.id1     = h.id1"
			+" and w.id2     = h.id2"
			+" and h.sid     = a.sid"
			+" and w.sid     = b.sid and h.inst_id = a.inst_id"
			+" and decode (w.type, 'TX', b.row_wait_obj#,"
			+" 'TM', w.id1)"
			+" = o.object_id"
			+" order by w.ctime desc";
	
	

}
