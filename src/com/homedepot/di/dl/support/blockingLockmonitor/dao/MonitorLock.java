package com.homedepot.di.dl.support.blockingLockmonitor.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage.RecipientType;

import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

import com.homedepot.di.dl.support.blockingLockmonitor.dto.MonitorLockDTO;
import com.homedepot.di.dl.support.blockingLockmonitor.web.MonitorLockServlet;
import com.homedepot.di.dl.support.util.*;
import com.homedepot.di.dl.support.util.Constants;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;



public class MonitorLock {

	final static Logger logger = Logger.getLogger(MonitorLock.class);
	
	/**
	 * @param args
	 * @throws Exception 
	 */
	private static Client client = null;
	public static String checkForblockingLocks(String mail) throws Exception {
		// TODO Auto-generated method stub
		
		
		
		MonitorLockDTO dto = new MonitorLockDTO();
		String session = "";
		String sessionTable="<html>"
				+ "<body>"
				+"<p>Blocking Session details.</p>"
				+"<br/>"
				+"<b>Note:</b> Sessions older than 3 minutes are killed by this script. If not killed, hi-pri will be sent to the primary on-call"
				+"<br/>"
				+"<table border=\"1\" cellpadding=\"2\" cellspacing=\"2\" width=\"500px\" style=\"border-collapse:collapse;\">";
		
		
		
		String headerDetails = "";
		String header="";
		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sessionDetails = "";
		String sessions = "";
		Boolean sendMailflag = false;
		Boolean isHipri = false;
		Boolean isSessionsKilled=false;
		
DBConnectionUtil dbConn = new DBConnectionUtil();
		
		 
		try {
			
			
			
			con = dbConn.getJNDIConnection(Constants.STERLING_ATC_RW1_JNDI);
			
			stmt = con.createStatement();
			
			rs = stmt.executeQuery(Constants.blockingQuery);
			ResultSetMetaData rsmd = rs.getMetaData();
			int colCount = rsmd.getColumnCount();
			for (int i = 1; i <= colCount; i++) 
			{
				headerDetails = headerDetails + "<th>" + rsmd.getColumnName(i) +"</th>";
			}
			
			header = "<tr bgcolor=\"#FF8C26\">" + headerDetails + "</tr>";
			
			String sessionDetails1="";
			
			
			while(rs.next())
			{	
				sessionDetails = "";
								
				for (int i = 1; i <= colCount; i++)
				{
					
					sessionDetails = sessionDetails +"<td>" + rs.getString(i) + "</td>";
									
				}
				
				if(rs.getInt(7) >= 120 && rs.getInt(7) < 180)
				{
					sendMailflag = true;
				}
				
				
				
				if(rs.getInt(7) >= 180 && "true".equalsIgnoreCase(mail))
				{
					isSessionsKilled =  true;
					sendMailflag = true;
					logger.info("killing session : "+rs.getString(1));
					try{
					killQuery(con,rs.getString(1));
					}
					catch(Exception e)
					{
						logger.debug("Kill Failed : "+e.getMessage());
					}
					//stmt.executeUpdate(rs.getString(1));
					con.commit();
				}
				
				sessionDetails1 = sessionDetails1 + "<tr>" + sessionDetails + "</tr>"; 
				
				
				
			}
			
			if(isSessionsKilled)
			{
				
			dto = checkIflockExists();
			
			if(dto.getIfExists())
			{
				sendMailflag = true;
				isHipri = true;
				sessionDetails1 = dto.getSession();
			}
			}
			sessions = sessions + sessionDetails1;
			
			
		}
		catch(Exception e)
		{
			logger.debug("MonitorLock Exception : "+e.getMessage());
			e.printStackTrace();
		}
		
		finally
		{
			logger.info("Closing DB Connection...");
		rs.close();
		stmt.close();
		con.close();
		}
		//sessions = "Hi COM Multichannel";
		
		//sessionTable = sessionTable + header + sessions + "</body></html><br/><br/>Thanks,<br/>COM Multichannel Support" ;
		
		
		if(sendMailflag && "true".equalsIgnoreCase(mail))
        {
            logger.info("sending mail.......");
            //sendMail(sessionTable,isHipri);
            if(isHipri)
            {
            	dto = getPrimaryDetails();
            	String[] to = dto.getMailID();
            	if(dto.getResponse()==null)
            	{
            		sessionTable = sessionTable + header + sessions + "<br/> Note : Couldn't fetch on-call details. Mailing Order Managment Support" + "<br/><br/>Thanks,<br/>COM Multichannel Support</body></html>" ;
              	}
            	else
            	{
            		sessionTable = sessionTable + header + sessions + "<br/><br/>Thanks,<br/>COM Multichannel Support</body></html>" ;
            	}
            		
            	 
            	sendMail(to,sessionTable,isHipri);
            }
            else
            {
            	String[] to = {"_2fc77b@homedepot.com"};
            	sessionTable = sessionTable + header + sessions + "<br/><br/>Thanks,<br/>COM Multichannel Support</body></html>" ;
            	sendMail(to,sessionTable,isHipri);
            }
        }
		logger.info(sessionTable);
		session =header + sessions;
		
		return session;
		

	}
	
	public static MonitorLockDTO checkIflockExists() throws Exception {
		// TODO Auto-generated method stub
		Constants consts = new Constants();
		MonitorLockDTO dto = new MonitorLockDTO();
		DBConnectionUtil dbConn = new DBConnectionUtil();
		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;
		
		String sessionDetails="";
		String sessionDetails1="";
		
		Boolean ifExists=false;
		try {
			
			
			con = dbConn.getJNDIConnection(Constants.STERLING_ATC_RW1_JNDI);
			
			stmt = con.createStatement();
			
			rs = stmt.executeQuery(Constants.blockingQuery);
			
			ResultSetMetaData rsmd = rs.getMetaData();
			int colCount = rsmd.getColumnCount();
			
			while(rs.next())
			{
				sessionDetails = "";
				
				for (int i = 1; i <= colCount; i++)
				{
					
					sessionDetails = sessionDetails +"<td>" + rs.getString(i) + "</td>";
					
									
				}
				
				if(rs.getInt(7) >= 180)
				{
					ifExists = true;
					
					
				}
				
				sessionDetails1 = sessionDetails1 + "<tr>" + sessionDetails + "</tr>"; 
			}
			
			dto.setIfExists(ifExists);
			dto.setSession(sessionDetails1);
		}
		catch(Exception e)
		{
			logger.debug("MonitorLock Exception : " + e.getMessage());
		}
		
		finally
		{
			logger.info("Closing DB Connection...");
			rs.close();
			stmt.close();
			con.close();
		}
		return dto;
		
	}

	/*public static Connection getDBConnection() {
		// TODO Auto-generated method stub
		
		final String driverClass = "oracle.jdbc.driver.OracleDriver";
		final String connectionURL = "jdbc:oracle:thin://@apragor11-scan.homedepot.com:1521/dpr77mm_srw04";
		final String uName = "MMUSR01";
		final String uPassword = "COMS_MMUSR01";
		Connection con = null;
		logger.info("Opening DB Connection..."+connectionURL);
		try {
			Class.forName(driverClass);
		
		con = DriverManager.getConnection(connectionURL, uName,uPassword);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.debug("Error connecting db : " + e.getMessage());
			
		}
		return con;
		
	}*/

	public static MonitorLockDTO getPrimaryDetails() 
	
	{
		
		MonitorLockDTO dto = new MonitorLockDTO();
		String[] mailID =  {"Order_Mgmt_Support@homedepot.com"};
		String response = null;
		
		String url = "http://cpliis6t.homedepot.com:5001/api/groups/ORDER_MANAGEMENT_SUPPORT";
		
		Client client = getClient();

		try {
		WebResource webResource = ((com.sun.jersey.api.client.Client) client)
				.resource(url);

		response = webResource.type("text/plain").get(String.class);
		
		JSONObject xmlJSONObj = new JSONObject(response);
		
	     
		
	       String primary = xmlJSONObj.getString("Primary").toString();
	       
	       int firstIndex = primary.indexOf(";") +1;
	       int lastIndex = primary.length();
	       
	       mailID = new String[] {primary.substring(firstIndex, lastIndex)};
	       
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.debug("Lock Monitor - Getting primary mail details : "+e.getMessage());
		}
		dto.setResponse(response);
		dto.setMailID(mailID);
		
		return dto;
	}

	public static void killQuery(Connection con,String killQuery) {
		// TODO Auto-generated method stub
		logger.info("Kill Query Executed : " + killQuery);
		
		killQuery = killQuery.replace("EXECUTE", "");
		
		try {
			CallableStatement stmt = con.prepareCall("{call "+killQuery+"}");
			stmt.execute();
			con.commit();
			stmt.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			logger.debug("Kill Block : "+e.getMessage());
			e.printStackTrace();
		}
		
		
		
	}

	
	public static Client getClient(){
		if(client == null){
			client = Client.create();
		}
		return client;		
	}
	
	public static void sendMail(String[] to, String msgContent, Boolean isHipri) throws Exception {
		final String user = "HorizonApp@homedepot.com";// change accordingly
		final String password = "xxx";// change accordingly
		String sub = "WARN";
		String  pri = "3";
		
		if(isHipri)
		{
			sub = "CRITICAL";
			pri="1";
		}
		
			
		String host = "mail1.homedepot.com";
		Properties props = new Properties();
		props.put("mail.host", host);
		Session session = Session.getInstance(props,
				new javax.mail.Authenticator() {
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(user, password);
					}
				});

		try {

			//com dl
			//String[] to = {"_275769@homedepot.com"};
			//String[] to = {"mohanraj_gurusamy@homedepot.com"};
			MimeMessage message = new MimeMessage(session);
			message.setHeader("X-Priority", pri);
			message.setFrom(new InternetAddress(user));
			DateFormat dateFormat = new SimpleDateFormat(
					"MM/dd/yyyy hh:mm:ss a");
			// get current date time with Date()
			Date today = new Date();
			String date1 = dateFormat.format(today);
			message.setSubject(sub + " : Sterling DB Blocking Locks: " + date1);
			message.setFrom(new InternetAddress(user));
			 InternetAddress[] addressTo = new InternetAddress[to.length];
			// InternetAddress[] addressCc = new InternetAddress[Cc.length];
			 for (int i = 0; i < to.length; i++) {
			 addressTo[i] = new InternetAddress(to[i]);
			 logger.info("To Address " + to[i]);
			 }
			// for (int j = 0; j < Cc.length; j++) {
			// addressCc[j] = new InternetAddress(Cc[j]);
			// }
			message.setRecipients(RecipientType.TO, addressTo);
			// message.setRecipients(RecipientType.CC, addressCc);
			message.setContent(msgContent, "text/html");
			Transport.send(message);
			// for (int i = 0; i < to.length; i++) {
			// addressTo[i] = new InternetAddress(to[i]);
			// System.out.println("Message delivered to--- " + to[i]);
			// }
			// for (int j = 0; j < Cc.length; j++) {
			// addressCc[j] = new InternetAddress(Cc[j]);
			// }

		} catch (MessagingException e) {
			e.printStackTrace();
		}

	}

}
