package com.homedepot.di.dl.support.POwithoutSOCleanup.web;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.sql.SQLException;
import java.util.GregorianCalendar;

import javax.mail.MessagingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.homedepot.di.dl.support.ErrorDeltas.web.DeltaServlet;
import com.homedepot.di.dl.support.POwithoutSOCleanup.dao.DropMessage;

/**
 * Servlet implementation class DropMessageServlet
 */
public class DropMessageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	final static Logger logger = Logger.getLogger(DropMessageServlet.class);
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DropMessageServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		logger.info("PO Without SO Cleanup Started : "+ GregorianCalendar.getInstance().getTime());
		DropMessage dm=new DropMessage();
		try {
			dm.postMessage();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.debug("PO Without SO Cleanup Exception : "+ GregorianCalendar.getInstance().getTime()+ e);
			e.printStackTrace();
		}
		logger.info("PO Without SO Cleanup Completed : "+ GregorianCalendar.getInstance().getTime());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		
}
}
