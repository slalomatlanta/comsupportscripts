package com.homedepot.di.dl.support.POwithoutSOCleanup.dao;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.jms.JMSException;
import javax.jms.Queue;
import javax.jms.QueueConnectionFactory;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.log4j.Logger;


/**
 * This class is an implementation of the Service Locator pattern. It is used to
 * lookup resources such as DataSources, JMS Queues, etc. This implementation
 * uses the "singleton" strategy and also the "caching" strategy.
 * 
 */
public final class QueueContextCache {

	/**
	 * Singleton instance
	 */
	private static QueueContextCache instance;
	/**
	 * JNDI context
	 */
	private Context context;
	/**
	 * Used to cache JNDI objects
	 */
	private Map<String, Object> cache;
	/**
	 * Logger instance
	 */
	private static final Logger logger = Logger.getLogger(QueueContextCache.class);

	/*
	 * Default Constructor
	 */
	private QueueContextCache() throws Exception {
		String contextName = "java:comp/env";

		try {
			// initialize the JNDI context
			context = (Context) new InitialContext().lookup(contextName);
			// initialize the cache
			cache = Collections.synchronizedMap(new HashMap<String, Object>());

		} catch (NamingException ne) {

			throw new ProcessingException(ErrorCodeEnum.APPLICATION_ERROR, "Error looking up context: " + contextName, ne,false);
		}
	}

	/**
	 *  Synchronized Singleton access method
	 * 
	 * @return The singleton instance of ServiceLocator Class
	 * @throws Exception 
	 */
	public static synchronized QueueContextCache getInstance()
			 {
		
		// see if the singleton instance has been initialized yet
		if (instance == null) {

			// synchronize this object so only one thread can create the
			// instance
			try {
				instance = new QueueContextCache();
			} catch (JMSException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			logger.debug("ServiceLocator instance initialized successfully");

		}
		
		// return singleton
		return instance;
	}

	/**
	 * Lookup the specified queue. The cache is checked before performing the
	 * JNDI lookup
	 * 
	 * @param jndiName
	 *            The JNDI name of the queue
	 * 
	 * @return Queue
	 * @throws ProcessingException
	 *             Thrown if an exception is encountered retrieving the Queue
	 *             from the JNDI context
	 */
	public Queue getQueue(String jndiName) throws ProcessingException {
		Queue queue = null;

		try {
			
			synchronized(cache)
			{
					// check the cache, if found return it
					if (cache.containsKey(jndiName)) {
					queue = (Queue) cache.get(jndiName);
						if (logger.isDebugEnabled()) {
							logger.debug("Cache contains Queue '" + jndiName + "', returning from cache");
						}
		
					} else 
					{
						if (logger.isDebugEnabled()) {
							logger.debug("Cache doesn't contain Queue '" + jndiName + "', attempting to retrieve it from the container context");
						}
						// otherwise get it from the JNDI context and add it to the cache.
						queue = (Queue) context.lookup(jndiName);
						cache.put(jndiName, queue);
						if (logger.isDebugEnabled()) {
							logger.debug("Queue '" + jndiName + "' successfully added to the cache");
						}
					}
			}
			
		} catch (NamingException ne) {
			
			throw new ProcessingException(ErrorCodeEnum.APPLICATION_ERROR, "Error looking up jndi name: " + jndiName, ne,false);
			
		}

		return queue;
	}

	/**
	 * Lookup the specified queue. The cache is checked before performing the
	 * JNDI lookup
	 * 
	 * @param jndiName
	 *            The JNDI name of the queue
	 * 
	 * @return Queue
	 * @throws ProcessingException
	 *             Thrown if an exception is encountered retrieving the Queue
	 *             from the JNDI context
	 */
	public QueueConnectionFactory getQueueConnectionFactory(String jndiName) throws ProcessingException {
		
		QueueConnectionFactory queueConnectionFactory = null;

		try {
			
			synchronized(cache)
					{
						// check the cache, if found return it
						if (cache.containsKey(jndiName))
						{
							
							queueConnectionFactory = (QueueConnectionFactory) cache.get(jndiName);
							if (logger.isDebugEnabled()) 
							{
								logger.debug("Cache contains Queue Connection Factory '" + jndiName + "', returning from cache");
							}
						}
						else
						{
							if (logger.isDebugEnabled()) {
								logger.debug("Cache doesn't contain Queue Connection Factory '" + jndiName + "', attempting to get it from the container context");
							}
			
							// otherwise get it from the JNDI context and add it to the cache.
							queueConnectionFactory = (QueueConnectionFactory) context.lookup(jndiName);
							cache.put(jndiName, queueConnectionFactory);
			
							if (logger.isDebugEnabled()) {
								logger.debug("Queue Connection Factory '" + jndiName + "' successfully added to the cache");
							}
						}
					}
			
		} catch (NamingException ne) {
			
			throw new ProcessingException(ErrorCodeEnum.APPLICATION_ERROR, "Error looking up jndi name: " + jndiName, ne,false);
			
		}

		return queueConnectionFactory;
	}

}
