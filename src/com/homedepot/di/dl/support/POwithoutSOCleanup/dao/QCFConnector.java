package com.homedepot.di.dl.support.POwithoutSOCleanup.dao;

import javax.jms.JMSException;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;

import org.apache.log4j.Logger;

//import com.homedepot.ta.aa.log4j.ApplLogMessage;

/**
 * Utility class for working with Queue Connection Factory.
 * Class with two methods, first method receives the QCF name and creates the connection and second method closes the connection
 * 
 * @author sandeep_vattikonda@homedepot.com
 */
public final class QCFConnector {
	
	private static final Logger LOGGER = Logger.getLogger( QCFConnector.class );

	/**
	 * Private Constructor to prevent the class from instantiation
	 */
	private QCFConnector(){};
	
	/**
	 * Gets a queue connection
	 * 
	 * @param jndiName The queue connection factory jndi name
	 * @return Connection The queue connection 
	 */
	public static final synchronized  QueueConnection getConnection( String qcfJndiName )
		throws ProcessingException {
		
		QueueConnection qConnection = null;
		
		QueueContextCache locator = QueueContextCache.getInstance();
		QueueConnectionFactory factory = locator.getQueueConnectionFactory(qcfJndiName);
		
		try {
			
			qConnection = factory.createQueueConnection();
			
		} catch ( JMSException jmsError ) {


			// The JMS LinkedException contains the root cause of the error
			// including implementation specific error codes, so it's very important
			// to log this information in order to troubleshoot
			if ( jmsError.getLinkedException() != null ) {
				LOGGER.error("JMS Linked Exception", jmsError.getLinkedException());
			}

			throw new ProcessingException(ErrorCodeEnum.JMS_ERROR, "Error getting queue connection: " + jmsError.getMessage(), jmsError,false);
			
		}
		
		return qConnection;
		
	}

	/**
	 * Closes the given Connection, logging any exception that may occur
	 * 
	 * @param connection The Connection to close
	 */
	public static final synchronized void close( QueueConnection qConnection ) {
		
		try {
			
			if ( qConnection != null ) {
				qConnection.close();
			}
			
		} catch ( JMSException jmsError ) {


			// The JMS LinkedException contains the root cause of the error
			// including implementation specific error codes, so it's very important
			// to log this information in order to troubleshoot
			if ( jmsError.getLinkedException() != null ) {
				LOGGER.warn("JMS Linked Exception", jmsError.getLinkedException());
			}

		}
		
	}

}
