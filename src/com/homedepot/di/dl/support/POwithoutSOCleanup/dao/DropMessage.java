package com.homedepot.di.dl.support.POwithoutSOCleanup.dao;

import java.io.File;
import java.io.FileWriter;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeMessage.RecipientType;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;


import com.homedepot.di.dl.support.POwithoutSOCleanup.web.DropMessageServlet;
import com.homedepot.di.dl.support.util.Constants;
import com.homedepot.di.dl.support.util.DBConnectionUtil;

public class DropMessage {
	final static Logger logger = Logger.getLogger(DropMessage.class);

	public static void postMessage() throws Exception {
		String table4 = "<br/>Orders failed under re-ordered exception but re-ordered line is not in shipped/Return Received status.Report them:<br/>" + "<table border=\"1\" cellpadding=\"0\" cellspacing=\"0\" width=\"200px\" style=\"border-collapse:collapse;\"><tr><th>Order No</th><th>Line No</th><th>Status</th></tr>";
		String msgContent2="";
		String rowdel=null;
		String UserOrder=null;
		String ReOrderlineStatus=null;
		String exportKey=null;
		
		int total = 0;
		int rowsDeleted = 0 ;
		try {
			
			
			
			
			
		//FileWriter writer = new FileWriter("C:\\store\\POdelete.csv");
			 
			FileWriter writer = new FileWriter("/opt/isv/tomcat-6.0.18/temp/POdelete.csv");
			// Connection sscConn = getConnection("sterlingConnectionURL_SSC");
			
			DBConnectionUtil dbConn = new DBConnectionUtil();
			
			Connection sscConn = dbConn.getJNDIConnection(Constants.STERLING_SSC_RO_JNDI);
			
			Statement st = null;
			st = sscConn.createStatement();
			ResultSet result = null;
			String Query = " select x.Order_No,x.LINE_NUM,x.errorstring,x.Re_orderedLine,y.user_reference,y.LINE_NUM,y.export_key,y.flow_name,y.sub_flow_name,y.status, "
							+ " y.priority,y.createuserid,y.modifyuserid,y.createprogid,y.modifyprogid,y.createts,y.modifyts,y.Lockid,y.message from "
							+ " (select c.*,b.*,d.*,a.errorstring from (select message,errorstring from thd01.yfs_reprocess_error where "
							+ " flow_name = 'HDProcessOrderUpdates' and  errorstring='Reordered Quantity is  greater than allowed limit' "
							+ " and errorcode='EXTN_80003' ) a, "
							+ " xmltable('//Order/Extn' passing xmltype(a.message) columns Order_No varchar2(40) path '@ExtnHostOrderReference')c, "
							+ " xmltable('//Order/OrderLineRelationships/OrderLineRelationship/ChildLine/Extn' passing xmltype(a.message) columns LINE_NUM varchar2(40) path '@ExtnHostOrderLineReference')b, " 
							+ " xmltable('//Order/OrderLineRelationships/OrderLineRelationship/ParentLine/Extn' passing xmltype(a.message) columns Re_orderedLine varchar2(40) path '@ExtnHostOrderLineReference')d)x, "
							+ " (select b.*,a.user_reference,a.export_key, a.flow_name,a.sub_flow_name,a.status, "
							+ " a.priority,a.createuserid,a.modifyuserid,a.createprogid,a.modifyprogid,a.createts,a.modifyts,a.Lockid,a.message from " 
							+ " (select user_reference,export_key,flow_name,sub_flow_name, "
							+ " status,priority,createuserid,modifyuserid,createprogid,modifyprogid,createts,modifyts,Lockid,message "
							+ " from THD01.yfs_export where "
							+ " flow_name ='HDStorePOCreate' and createts > sysdate-3 and status='00' ) a, "
							+" xmltable('//Order/OrderLines/OrderLine/Extn' passing xmltype(a.message) columns LINE_NUM varchar2(40) path '@ExtnHostOrderLineReference')b)y "
							+" where y.user_reference=x.Order_No and y.LINE_NUM=x.LINE_NUM ";

			//System.out.println(Query);
			result = st.executeQuery(Query);

			writer.append("Reprocess table Order_No");
			writer.append(',');
			writer.append("LineNo");
			writer.append(',');
			writer.append("errorstring");
			writer.append(',');
			writer.append("Re_orderedLine");
			writer.append(',');
			writer.append("Export table Order_No");
			writer.append(',');
			writer.append("LineNo");
			writer.append(',');
			writer.append("export_key");
			writer.append(',');
			writer.append("flow_name");
			writer.append(',');
			writer.append("sub_flow_name");
			writer.append(',');
			writer.append("status");
			writer.append(',');				
			writer.append("priority");
			writer.append(',');
			writer.append("createuserid");
			writer.append(',');
			writer.append("modifyuserid");
			writer.append(',');
			writer.append("createprogid");
			writer.append(',');
			writer.append("ModifyProgId");
			writer.append(',');
			writer.append("createts");
			writer.append(',');
			writer.append("modifyts");
			writer.append(',');
			writer.append("Lockid");
			writer.append(',');
			writer.append("message");
			

			writer.append('\n');
			
			String s1=null;
			String s2=null;
			
			while (result.next()) {
				
				
				
				//System.out.println("Output:" + result.getString(1));
				UserOrder=result.getString(1);
				// String app1=Integer.toString(i);
				writer.append( result.getString(1));
				writer.append(',');
				writer.append(result.getString(2));
				writer.append(',');
				writer.append(result.getString(3));
				writer.append(',');
				ReOrderlineStatus=result.getString(4);
				writer.append(result.getString(4));
				writer.append(',');
				writer.append( result.getString(5));
				writer.append(',');
				writer.append(result.getString(6));
				writer.append(',');
				exportKey=result.getString(7);
				writer.append("'" +result.getString(7));
				writer.append(',');
				writer.append(result.getString(8));
				writer.append(',');
				writer.append(result.getString(9));
				writer.append(',');
				writer.append(result.getString(10));
				writer.append(',');
				writer.append(result.getString(11));
				writer.append(',');
				writer.append(result.getString(12));
				writer.append(',');
				writer.append(result.getString(13));
				writer.append(',');
				writer.append( result.getString(14));
				writer.append(',');
				writer.append(result.getString(15));
				writer.append(',');
				writer.append(result.getString(16));
				writer.append(',');
				writer.append(result.getString(17));
				writer.append(',');
				writer.append(result.getString(18));
				writer.append(',');
				s1=result.getString(19);
				writer.append(s2=s1.replaceAll("[\n\r]", ""));
				writer.append(',');
				
				writer.append('\n');
				
				Statement stmtcheck1 = null;
				ResultSet rsetcheck1 = null;
				
				stmtcheck1 = sscConn.createStatement();
				String Ordercheck1=null;
				
				Ordercheck1= " select   yors.status,ys.description "
						+ " from THD01.yfs_order_header yoh, THD01.yfs_order_line yol, THD01.yfs_order_release_status yors, THD01.yfs_status ys "
						+ " where yoh.order_header_key = yol.order_header_key and yol.order_line_key = yors.order_line_key "
						+ " and yors.status=ys.status and ys.process_type_key='ORDER_FULFILLMENT' "
						+ " and yoh.extn_host_order_ref ='"
					+ UserOrder
					+ "' and yol.extn_host_order_line_ref ='"
					+ReOrderlineStatus
					+ "' and yoh.document_type= '0001' "
						+ " and yors.status_quantity > 0 ";
				 
				 //System.out.println(Ordercheck1);
				 
				 rsetcheck1 = stmtcheck1.executeQuery(Ordercheck1);
				 
				 
						String status1=null;
						String desc1=null;
					
					
						
						if (rsetcheck1.next()) {
						
						status1=rsetcheck1.getString(1);
						desc1= rsetcheck1.getString(2);
						
																
						if((status1.contains("3700") && desc1.contains("Shipped")) || (status1.contains("3700.02") && desc1.contains("Return Received")))
						{
				
			
			Statement stmt = null;
			ResultSet rset = null;
				
			Connection atcConn = dbConn.getJNDIConnection(Constants.STERLING_ATC_RW_JNDI);

				final String DelQuery = " delete from thd01.yfs_export where user_reference ='"
										+ UserOrder
										+ "' and export_key ='"
										+ exportKey
										+ "' and  flow_name ='HDStorePOCreate' and  status='00' ";
										

				

				
				Statement stmt1 = null;
				stmt1 = atcConn.createStatement();
				//System.out.println(DelQuery);
				rowsDeleted = stmt1.executeUpdate(DelQuery);
				total = total + rowsDeleted;
				atcConn.commit();
				stmt1.close();
				atcConn.close();
				//System.out.println(rowsDeleted + " Rows Deleted");
				

			}
					
						else
						{
							Ordercheck1= " select   yors.status,ys.description "
									+ " from THD01.yfs_order_header yoh, THD01.yfs_order_line yol, THD01.yfs_order_release_status yors, THD01.yfs_status ys "
									+ " where yoh.order_header_key = yol.order_header_key and yol.order_line_key = yors.order_line_key "
									+ " and yors.status=ys.status and ys.process_type_key='ORDER_FULFILLMENT' "
									+ " and yoh.extn_host_order_ref ='"
								+ UserOrder
								+ "' and yol.extn_host_order_line_ref ='"
								+ReOrderlineStatus
								+ "' and yoh.document_type= '0001' "
									+ " and yors.status_quantity > 0 ";
							 
							 //System.out.println(Ordercheck1);
							 
							 rsetcheck1 = stmtcheck1.executeQuery(Ordercheck1);
							 
							 if (rsetcheck1.next()) {
								 
								 String descr=null;
									
									//System.out.println("Other status:" +rsetcheck1.getString(1));
									//System.out.println("Other Desc:" + rsetcheck1.getString(2));
									descr=rsetcheck1.getString(2);
							 
									table4 = table4 
											+ "<tr><td>"
											+ UserOrder
											+ "</td>"
											+ "<td>"
											+ ReOrderlineStatus
											+"<td>"
											+ descr
											+ "</td>"
											+ "</td></tr>";
									
						}
						}
						
			}
						
						else
						{
							//System.out.println("No result from status query");
						}
						
			}
						
			//System.out.println(("Selected"));
			result.close();
			st.close();
			sscConn.close();
			writer.flush();
			writer.close();

			// final String driverClass = "oracle.jdbc.driver.OracleDriver";
			
			
			rowdel="Number of rows deleted:" + total;
			
			table4 = table4 + "</table></br>";
			msgContent2 = rowdel+ msgContent2 + table4 + "</body></html>";
			//sendAttachment(msgContent2);

			sendAttachment(msgContent2);
			
			POcreate();
			
		} catch (Exception e) {
			
			logger.debug("Error occured while Executing PO delete-dropmessageQueue Servlet  "
					+ GregorianCalendar.getInstance() + e);
			e.printStackTrace();
		}
		
		
	}
	
	public static void sendAttachment(String msgContent2) throws Exception {
		String host = "mail1.homedepot.com";
		String from = "horizon@cpliisad.homedepot.com";
		String[] Cc = {  };
		String[] to = { "_2fc77b@homedepot.com" };
		// String[] to = { "shwetha_ravi@homedepot.com" };
		// String[] Cc = { "shwetha_ravi@homedepot.com" };
		// Get system properties
		Properties properties = System.getProperties();

		// Setup mail server
		properties.setProperty("mail.smtp.host", host);

		// Get the default Session object.
		Session session = Session.getDefaultInstance(properties);

		// Define message
		Message message = new MimeMessage(session);
		message.setFrom(new InternetAddress(from));
		InternetAddress[] addressTo = new InternetAddress[to.length];
		InternetAddress[] addressCc = new InternetAddress[Cc.length];
		for (int i = 0; i < to.length; i++) {
			addressTo[i] = new InternetAddress(to[i]);
			logger.debug("To Address " + to[i]);
		}
		for (int j = 0; j < Cc.length; j++) {
			addressCc[j] = new InternetAddress(Cc[j]);
			logger.debug("Cc Address " + Cc[j]);
		}
		message.setRecipients(RecipientType.TO, addressTo);
		message.setRecipients(RecipientType.CC, addressCc);
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a");
		// get current date time with Date()
		Date today = new Date();
		String date1 = dateFormat.format(today);
		message.setSubject("PO delete from yfs_export table for SO lines falls under re-ordered exceptions "
				+ date1);

		// Create the message part
		BodyPart messageBodyPart = new MimeBodyPart();
		String msgContent = "Hi All,<br/><br/>PFA report for PO create for SO lines which falls under re-ordered exceptions and were deleted from yfs_export table today.<br/> "
				+ msgContent2 + "<br/><br/>Thanks<br/>";
		msgContent = msgContent + "COM Multichannel Support";
		// Fill the message
		messageBodyPart.setContent(msgContent, "text/html");

		Multipart multipart = new MimeMultipart();
		multipart.addBodyPart(messageBodyPart);

		// Part two is attachment
		messageBodyPart = new MimeBodyPart();
		//String filename = "C:\\store\\POdelete.csv";
		String filename = "/opt/isv/tomcat-6.0.18/temp/POdelete.csv";
		DataSource source = new FileDataSource(filename);
		messageBodyPart.setDataHandler(new DataHandler(source));
		messageBodyPart.setFileName(filename);
		multipart.addBodyPart(messageBodyPart);

		// Put parts in message
		message.setContent(multipart);

		// Send the message
		Transport.send(message);
		logger.debug("Msg Send ....");
	}

	
	
	public static void POcreate() throws Exception
	{
		
		//FileWriter writer1 = new FileWriter("C:\\store\\POCreate.csv");
	FileWriter writer1 = new FileWriter("/opt/isv/tomcat-6.0.18/temp/POCreate.csv");
		
		int total1 = 0;
		int rowsDeleted1 = 0 ;
		
		String rowcreated=null;

		
		String table1 = "Order lines are not in created status,so delete the message from yfs_export table:<br/>" + "<table border=\"1\" cellpadding=\"0\" cellspacing=\"0\" width=\"200px\" style=\"border-collapse:collapse;\"><tr><th>Order No</th><th>Line No</th><th>Status</th></tr>"; 
		String table2 = "Orders are present but the below lines are not created at sterling but PO create received for these lines.Do not delete the message from yfs_export table and report them:<br/>" + "<table border=\"1\" cellpadding=\"0\" cellspacing=\"0\" width=\"200px\" style=\"border-collapse:collapse;\"><tr><th>Order No</th><th>Re-orderedLine</th><th>Line(Billed)</th><th> line(Billed) status</th></tr>";
		String table3 = "Order is not created/present in sterling but PO create received for these orders.Do not delete the message from yfs_export table.Report this to WCS Team<br/>" + "<table border=\"1\" cellpadding=\"0\" cellspacing=\"0\" width=\"200px\" style=\"border-collapse:collapse;\"><tr><th>Order No</th></tr>";
		
		String msgContent1="";
		msgContent1 = msgContent1
				+ "<!DOCTYPE html>"
				+ "<html>"
				+ "<body>"
				+ "<br/>";

		
		String OrderLineNotPresent=null;
		String OrderNotPresent=null;
		String OrderStatus=null;
		
		String Value=null;
		String OrderNos=null;
		
		
		//Connection con = null;
		DBConnectionUtil dbConn = new DBConnectionUtil();
		
		Connection sscConn = dbConn.getJNDIConnection(Constants.STERLING_SSC_RO_JNDI);
		try {

			
			Statement stmt1 = null;
			ResultSet rset1 = null;
			
			stmt1 = sscConn.createStatement();
			
			HashMap<String, String> hmap = new HashMap<String, String>();
			
			Map<String, String> map1 = new HashMap<String, String>();
			Map<String, String> map = new HashMap<String, String>();
			
			List<String> list = new ArrayList<String>();
			
			String ExportQuery= "select b.*,a.user_reference from (select user_reference,message from THD01.yfs_export where "
					         + "flow_name ='HDStorePOCreate' and status='00' and createts > sysdate-3 ) a, "
					         + "xmltable('//Order/OrderLines/OrderLine/Extn' passing xmltype(a.message) columns LINE_NUM varchar2(40) path '@ExtnHostOrderLineReference')b ";
			
			
			rset1 = stmt1.executeQuery(ExportQuery);
			
			logger.debug(ExportQuery);
			
			
			String OrderNo=null;
			String LineNo=null;
			
			
							
			boolean rescheck = false;
			
						
			
			while (rset1.next()) {
				
				
				LineNo=rset1.getString(1);
				OrderNo=rset1.getString(2);
					
				
				
				
						
				hmap.put(OrderNo, LineNo);
				
			
			
			
			 Set set = hmap.entrySet();
		      Iterator iterator = set.iterator();
		      while(iterator.hasNext()) {
		         Map.Entry mentry = (Map.Entry)iterator.next();
		         logger.debug("key is: "+ mentry.getKey() + " & Value is: ");
		         logger.debug(mentry.getValue());
		         
		         

					Statement stmt2 = null;
					ResultSet rset2 = null;
					
					stmt2 = sscConn.createStatement();
					String Orderheader=null;
					
					
						
						
						 Orderheader= "select  distinct yoh.extn_host_order_ref,yol.extn_host_order_line_ref "
								 + "from thd01.yfs_order_header yoh,thd01.yfs_order_line yol "
								 + " where yoh.extn_host_order_ref ='"
										+ mentry.getKey()
										+ "' and yol.extn_host_order_line_ref ='"
										+mentry.getValue()
										+ "' and yol.order_header_key=yoh.order_header_key and yoh.document_type='0001'";
						 
						// System.out.println(Orderheader);
						 
						 rset2 = stmt2.executeQuery(Orderheader);
								
								if (rset2.next()) {
								
									logger.debug("Sterling Order: " + rset2.getString(1));
									logger.debug("Sterling Order: " + rset2.getString(2));
								//System.out.println("Sterling messageStatus: " + rset1.getString(2));
								
								 Statement stmtcheck = null;
									ResultSet rsetcheck = null;
									
									stmtcheck = sscConn.createStatement();
									String Ordercheck=null;
									
									Ordercheck= " select   yors.status,ys.description "
											+ " from THD01.yfs_order_header yoh, THD01.yfs_order_line yol, THD01.yfs_order_release_status yors, THD01.yfs_status ys "
											+ " where yoh.order_header_key = yol.order_header_key and yol.order_line_key = yors.order_line_key "
											+ " and yors.status=ys.status and ys.process_type_key='ORDER_FULFILLMENT' "
											+ " and yoh.extn_host_order_ref ='"
										+ mentry.getKey()
										+ "' and yol.extn_host_order_line_ref ='"
										+mentry.getValue()
										+ "' and yoh.document_type= '0001' "
											+ " and yors.status_quantity > 0 ";
									 
									logger.debug(Ordercheck);
									 
									 rsetcheck = stmtcheck.executeQuery(Ordercheck);
									 
									 
											String status=null;
											String desc=null;
										
										
											
											if (rsetcheck.next()) {
											
											status=rsetcheck.getString(1);
											desc= rsetcheck.getString(2);
											
																					
											if(status.contains("1100") && desc.contains("Created"))
											{
									
								
								Statement stmt = null;
								ResultSet rset = null;
								
								String s3=null;
								String s4=null;
								
								stmt = sscConn.createStatement();
								
								String queryString ="select c.message,c.user_reference,c.LINE_NUM,c.export_key "
													+ "from thd01.yfs_order_header yoh, thd01.yfs_order_line yol, "
													+ "(select b.*,a.user_reference,a.message,a.export_key from (select user_reference,message,export_key from THD01.yfs_export where "
													+ "flow_name like '%HDStorePOCreate%' and  status='00' and  user_reference  ='"
										+ mentry.getKey()
										+ "' order by user_reference asc) a, "
													+ "xmltable('//Order/OrderLines/OrderLine/Extn' passing xmltype(a.message) columns LINE_NUM varchar2(40) path '@ExtnHostOrderLineReference')b)c "
													+ "where yoh.extn_host_order_ref  ='"
										+ mentry.getKey()
										+ "' and yoh.document_type='0001' and yol.order_header_key=yoh.order_header_key and "
													+ "yol.extn_po_number is null and yol.line_type='SO' "
													+ "and c.LINE_NUM=yol.extn_host_order_line_ref and c.user_reference=yoh.extn_host_order_ref and  c.LINE_NUM  ='"
										+ mentry.getValue()
										+ "' order by yol.order_header_key asc ";
								
								rset = stmt.executeQuery(queryString);
								
								//System.out.println(queryString);
								
								writer1.append("Message");
								writer1.append(',');
								writer1.append("OrderNo");
								writer1.append(',');
								writer1.append("LineNum");
								writer1.append(',');
								writer1.append("export_key");
								writer1.append('\n');
								
										String Message=null;
										
										String Ordernum=null;
										String export=null;
										
										
								
								while (rset.next()) {
									Message= rset.getString(1);
									s3=rset.getString(1);
									writer1.append(s4=s3.replaceAll("[\n\r]", ""));
									//writer1.append( rset.getString(1));
									writer1.append(',');
									Ordernum=rset.getString(2);
									writer1.append( rset.getString(2));
									writer1.append(',');
									writer1.append( rset.getString(3));
									writer1.append(',');
									export= rset.getString(4);
									writer1.append("'"+ rset.getString(4));
									writer1.append(',');
									
									writer1.append('\n');
									//System.out.println("Sterling messageStatus: " + rset.getString(1));
									
									
									try {
										putMessages(Message);
										
										Connection atcConn = dbConn.getJNDIConnection(Constants.STERLING_ATC_RW_JNDI);

										final String DelQuery1 = " delete from thd01.yfs_export where user_reference ='"
																+ Ordernum
																+ "' and export_key ='"
																+ export
																+ "' and  flow_name ='HDStorePOCreate' and  status='00' ";
																

										//Connection con1 = null;

									
										Statement stmt5 = null;
										stmt5 = atcConn.createStatement();
										logger.debug(DelQuery1);
										rowsDeleted1= stmt5.executeUpdate(DelQuery1);
										total1 = total1 + rowsDeleted1;
										atcConn.commit();
										stmt5.close();
										atcConn.close();
										logger.debug(rowsDeleted1 + " Rows Deleted1");
										
									} catch (ProcessingException e) {
										// TODO Auto-generated catch block
										
										logger.debug("Exception " +e);
										e.printStackTrace();
										
									}
									
											}
											}
								
								else 
								{

									logger.info("OrderLine is not in created status at sterling,so PO cannot be created" + mentry.getKey() + " Line " + mentry.getValue());
									
									 OrderNos=(String) mentry.getKey();
									 String lineNo=(String) mentry.getValue();
									
									OrderStatus= "  " + OrderStatus + "  " + "  " + mentry.getKey() + "  " + mentry.getValue() + " "+ desc;
									
									
									

									//for putting values in map:
//									map1.put(OrderNos,lineNo );
									
									table1 = table1 
											+ "<tr><td>"
											+ mentry.getKey()
											+ "</td>"
											+ "<td>"
											+ mentry.getValue()
											+"<td>"
											+ desc
											+ "</td>"
											+ "</td></tr>";
									
									
									
									
									
								
								}
								
											}
											
											}
								
											
										
							
							
		      
							
							else  
							{
								
								
								// sscConn = dbConn.getJNDIConnection(Constants.STERLING_SSC_RO_JNDI);
								

								
										
										Statement st = null;
										st = sscConn.createStatement();
										ResultSet result = null;
										String ord="";
										String ReordLine="";
										String Line_NUM="";
										
										String Query = " select x.Order_No,x.Re_orderedLine,x.LINE_NUM from "
														+ " (select c.*,b.*,d.*,a.errorstring from (select message,errorstring from thd01.yfs_reprocess_error where message like '%"
												+ mentry.getKey()
												+ "%'  and flow_name = 'HDProcessOrderUpdates' and errorstring='Reordered Quantity is  greater than allowed limit' "
														+ " and errorcode='EXTN_80003'  ) a, "
														+ " xmltable('//Order/Extn' passing xmltype(a.message) columns Order_No varchar2(40) path '@ExtnHostOrderReference')c, "
														+ " xmltable('//Order/OrderLineRelationships/OrderLineRelationship/ChildLine/Extn' passing xmltype(a.message) columns LINE_NUM varchar2(40) path '@ExtnHostOrderLineReference')b, " 
														+ " xmltable('//Order/OrderLineRelationships/OrderLineRelationship/ParentLine/Extn' passing xmltype(a.message) columns Re_orderedLine varchar2(40) path '@ExtnHostOrderLineReference')d)x, "
														+ " (select b.*,a.user_reference,a.export_key, a.flow_name,a.sub_flow_name,a.status, "
														+ " a.priority,a.createuserid,a.modifyuserid,a.createprogid,a.modifyprogid,a.createts,a.modifyts,a.Lockid,a.message from " 
														+ " (select user_reference,export_key,flow_name,sub_flow_name, "
														+ " status,priority,createuserid,modifyuserid,createprogid,modifyprogid,createts,modifyts,Lockid,message "
														+ " from THD01.yfs_export where "
														+ " flow_name ='HDStorePOCreate' and createts > sysdate -3 and status='00' and user_reference ='"
												+ mentry.getKey()
												+ "'  order by user_reference asc) a, "
														+" xmltable('//Order/OrderLines/OrderLine/Extn' passing xmltype(a.message) columns LINE_NUM varchar2(40) path '@ExtnHostOrderLineReference')b)y "
														+" where y.user_reference=x.Order_No and y.LINE_NUM=x.LINE_NUM ";

										logger.debug(Query);
										result = st.executeQuery(Query);
										
										if (result.next()) {
											
											
											
											//logger.debug("Output:" + result.getString(1));
											//logger.debug("Output:" + result.getString(3));
											ord=result.getString(1);
											ReordLine=result.getString(2);
											Line_NUM=result.getString(3);
											
											Statement stmtcheck1 = null;
											ResultSet rsetcheck1 = null;
											
											stmtcheck1 = sscConn.createStatement();
											String Ordercheck1=null;
											
											Ordercheck1= " select   yors.status,ys.description "
													+ " from THD01.yfs_order_header yoh, THD01.yfs_order_line yol, THD01.yfs_order_release_status yors, THD01.yfs_status ys "
													+ " where yoh.order_header_key = yol.order_header_key and yol.order_line_key = yors.order_line_key "
													+ " and yors.status=ys.status and ys.process_type_key='ORDER_FULFILLMENT' and description <> 'Backordered From Node' "
													+ " and yoh.extn_host_order_ref ='"
												+ ord
												+ "' and yol.extn_host_order_line_ref ='"
												+ReordLine
												+ "' and yoh.document_type= '0001' "
													+ " and yors.status_quantity > 0 ";
											 
											logger.debug(Ordercheck1);
											 
											 rsetcheck1 = stmtcheck1.executeQuery(Ordercheck1);
											 
											 
													String status1=null;
													String desc1=null;
												
												
													
													if (rsetcheck1.next()) {
													
													status1=rsetcheck1.getString(1);
													desc1= rsetcheck1.getString(2);
													
													table2 = table2 
															+ "<tr><td>"
															+ ord
															+ "</td>"
															+ "<td>"
															+Line_NUM
															+ "</td>"
															+ "<td>"
															+ ReordLine
															+ "</td>"
															+ "<td>"
															+ desc1
															+ "</td></tr>";
													
													}
											
										}
										
										else
										{
										
										Statement stmt3 = null;
										ResultSet rset3 = null;
										String Orderheader1= "select  distinct yoh.extn_host_order_ref "
												 + "from thd01.yfs_order_header yoh,thd01.yfs_order_line yol "
												 + " where yoh.extn_host_order_ref ='"
														+ mentry.getKey()
														+ "'  and yol.order_header_key=yoh.order_header_key and yoh.document_type='0001'";
										 
										 //System.out.println(Orderheader1);
										 
										 
											
															
										
												
												rset3 = stmt2.executeQuery(Orderheader1);
												
												
												
												if (rset3.next()) {
													
													 Value=(String) mentry.getValue();
													
												
													 logger.debug("Order is present in sterling but line is not present: " + rset3.getString(1) + " Line " + mentry.getValue());
												
												OrderLineNotPresent = OrderLineNotPresent+ "  " + rset3.getString(1) + " " + mentry.getValue() ;
												
												//h2map.put(rset3.getString(1),Value);
												
												

												//for putting values in map:
												//map.put(rset3.getString(1),Value );
												
												table2 = table2 
														+ "<tr><td>"
														+ rset3.getString(1)
														+ "</td>"
														+ "<td>"
														+ mentry.getValue()
														+ "</td></tr>";

												}
										
		      
	
										
										else
										{
											
											 
											logger.debug("Order is not present in sterling: " +  mentry.getKey());
											
											
											
											OrderNotPresent = OrderNotPresent +"  " + mentry.getKey();
											
										//list.add(OrderNotPresent);	
											
										table3 = table3 
												+ "<tr><td>"
												+ mentry.getKey()
												+ "</td></tr>";
											
										}
										}
										
										
							}
					
		      }
		      
		      
		      
		      hmap.clear();
			}
				
			writer1.flush();
			writer1.close();
			
			rowcreated= "PO created and then deleted: " +total1; 
						
			sscConn.close();
		

		
		
		
		}catch (Exception e) {
			
			logger.debug("Exception " +e);
			e.printStackTrace();
			
			}
		
		
		
		

		table1 = table1 + "</table></br>";
		table2 = table2 + "</table></br>";
		table3 = table3 + "</table></br>";
		
		//System.out.println(table1);
		//System.out.println(table2);
		//System.out.println(table3);

		msgContent1 = rowcreated + msgContent1 + table1 + table2 + table3 + "</body></html>";
		sendAttachment2(msgContent1);	
	
	}
	
	public static void putMessages(String str) throws ProcessingException {

		logger.debug("Message going to be put in queue");

		JmsDestination destination = null;
		destination = new JmsDestination("jms/MQPutQCF_Str",
				"jms/DI.DL.STERLING.POCREATE_put");

		destination.open();

		try {
			destination.send(str);
			logger.debug(str);
			logger.debug("Message successfully put in queue");

		} finally {

			destination.close();
		}

	}

	public static void sendAttachment2(String msgContent1) throws Exception {

		String host = "mail1.homedepot.com";
		String from = "horizon@cpliisad.homedepot.com";
		// String[] to = { "_275769@homedepot.com" };
		String[] Cc = {  };
		String[] to = { "_2fc77b@homedepot.com" };
		// String[] Cc = { "shwetha_ravi@homedepot.com" };
		// Get system properties
		Properties properties = System.getProperties();

		// Setup mail server
		properties.setProperty("mail.smtp.host", host);

		// Get the default Session object.
		Session session = Session.getDefaultInstance(properties);

		// Define message
		Message message = new MimeMessage(session);
		message.setFrom(new InternetAddress(from));
		InternetAddress[] addressTo = new InternetAddress[to.length];
		InternetAddress[] addressCc = new InternetAddress[Cc.length];
		for (int i = 0; i < to.length; i++) {
			addressTo[i] = new InternetAddress(to[i]);
			logger.debug("To Address " + to[i]);
		}
		for (int j = 0; j < Cc.length; j++) {
			addressCc[j] = new InternetAddress(Cc[j]);
			logger.debug("Cc Address " + Cc[j]);
		}
		message.setRecipients(RecipientType.TO, addressTo);
		message.setRecipients(RecipientType.CC, addressCc);
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a");
		// get current date time with Date()
		Date today = new Date();
		String date1 = dateFormat.format(today);
		message.setSubject("SO without PO Monitor " + date1);

		// Create the message part
		BodyPart messageBodyPart = new MimeBodyPart();
		String msgContent = "Hi All,<br/><br/> PFA report" + " " + msgContent1 + " "+ "<br/><br/>Thanks<br/>";

		msgContent = msgContent + "COM Multichannel Support";
		// Fill the message
		messageBodyPart.setContent(msgContent, "text/html");

		Multipart multipart = new MimeMultipart();
		multipart.addBodyPart(messageBodyPart);

		//Part two is attachment
		 messageBodyPart = new MimeBodyPart();
		// String filename = "C:\\store\\POCreate.csv";
		String filename = "/opt/isv/tomcat-6.0.18/temp/POCreate.csv";
		 DataSource source = new FileDataSource(filename);
		 messageBodyPart.setDataHandler(new DataHandler(source));
	     messageBodyPart.setFileName(filename);
		 multipart.addBodyPart(messageBodyPart);

		// Put parts in message
		message.setContent(multipart);

		// Send the message
		Transport.send(message);
		logger.debug("Msg Send ....");

	}
	
	
	
	

}
