package com.homedepot.di.dl.support.POwithoutSOCleanup.dao;

/**
 *  
 * @author david_rogers@homedepot.com
 */
public class ProcessingException extends Exception {
	
	private ErrorCodeEnum errorCode;
	private boolean retryable;
	
	/**
	 * Serial Id
	 */
	private static final long serialVersionUID = -3103857515154217668L;
	
	/**
	 * Constructor
	 * 
	 * @param message The exception message
	 */
	public ProcessingException(ErrorCodeEnum errorCode, String message,boolean retryable) {
		super(message);
		this.errorCode = errorCode;
		this.retryable = retryable;
	}

	/**
	 * Constructor
	 * 
	 * @param message The exception message
	 * @param cause The root cause of the exception
	 */
	public ProcessingException(ErrorCodeEnum errorCode, String message, Throwable cause,boolean retryable) {
		super(message, cause);
		this.errorCode = errorCode;
		this.retryable = retryable;
	}

	/**
	 * Gets the response code
	 * @return short The response code
	 */
	public ErrorCodeEnum getErrorCode() {
		return this.errorCode;
	}
	
	/**
	 * Returns if the message is retry able
	 * @return the boolean condition
	 */
	public boolean isRetryable() {
		return retryable;
	}

}
