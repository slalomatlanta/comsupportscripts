package com.homedepot.di.dl.support.invReport.dao;

public class InvData {
	
	private String storeNo;
	private long storeSkuCount = 0 ;
	private long comparedSKU = 0 ;
	private String storeStatus = "Not Started";
	private long mismatchCount = 0 ;
	private String dataURL = "";
	
	
	public String getStoreNo() {
		return storeNo;
	}
	public void setStoreNo(String storeNo) {
		this.storeNo = storeNo;
	}
	public long getStoreSkuCount() {
		return storeSkuCount;
	}
	public void setStoreSkuCount(long storeSkuCount) {
		this.storeSkuCount = storeSkuCount;
	}
	public long getComparedSKU() {
		return comparedSKU;
	}
	public void setComparedSKU(long comparedSKU) {
		this.comparedSKU = comparedSKU;
	}
	public String getStoreStatus() {
		return storeStatus;
	}
	public void setStoreStatus(String storeStatus) {
		this.storeStatus = storeStatus;
	}
	public long getMismatchCount() {
		return mismatchCount;
	}
	public void setMismatchCount(long mismatchCount) {
		this.mismatchCount = mismatchCount;
	}
	public String getDataURL() {
		return dataURL;
	}
	public void setDataURL(String dataURL) {
		this.dataURL = dataURL;
	}
	
	
	
}
