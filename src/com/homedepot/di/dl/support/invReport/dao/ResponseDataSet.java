package com.homedepot.di.dl.support.invReport.dao;

import java.util.ArrayList;

public class ResponseDataSet {

	private boolean serviceStarted = false;
	private ArrayList<InvData> invDataList = new ArrayList<InvData>();
	private String currentStore = "";
	private String overallStatus = "Not Started";
	private String summaryDataURL;

	public String getOverallStatus() {
		return overallStatus;
	}

	public void setOverallStatus(String overallStatus) {
		this.overallStatus = overallStatus;
	}

	public String getCurrentStore() {
		return currentStore;
	}

	public void setCurrentStore(String currentStore) {
		this.currentStore = currentStore;
	}

	public boolean isServiceStarted() {
		return serviceStarted;
	}

	public void setServiceStarted(boolean serviceStarted) {
		this.serviceStarted = serviceStarted;
	}

	public ArrayList<InvData> getInvDataList() {
		return invDataList;
	}

	public void setInvDataList(ArrayList<InvData> invdata) {
		this.invDataList = invdata;
	}

	public void createInvData() {
		InvData current = new InvData();
		this.invDataList.add(current);
	}

	public InvData getInvData() {
		return invDataList.get(invDataList.size() - 1);
	}

	public String getSummaryDataURL() {
		return summaryDataURL;
	}

	public void setSummaryDataURL(String summaryDataURL) {
		this.summaryDataURL = summaryDataURL;
	}
}
