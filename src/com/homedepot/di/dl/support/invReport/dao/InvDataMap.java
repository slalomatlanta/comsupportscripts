package com.homedepot.di.dl.support.invReport.dao;

import com.homedepot.di.dl.support.invReport.util.AsyncStaticData;
import com.homedepot.di.dl.support.invReport.util.InvRepConstants;
import com.homedepot.di.dl.support.switchToNormal.dao.SwitchTransmissionState;

import org.apache.log4j.Logger;

public class InvDataMap extends InvRepConstants {
	final static Logger logger = Logger.getLogger(InvDataMap.class);
	private StringBuffer buffer = new StringBuffer();
	private int sterlingCounter = 1;
	private StringBuffer cassBuffer = new StringBuffer();
	private int cassCounter = 1;
	private boolean scanCompleted = false;

	// ArrayList<String> scannedInventoryKeyList = new ArrayList<String>();

	public void setCasOH(String invKey, Double OH) {
		if (cassendraDetails.get(invKey) != null) {
			cassendraDetails.get(invKey).setCassOH(OH.floatValue());
			InvDataSet currentData = skuDetails.get(cassendraDetails
					.get(invKey).getSku() + CURRENTSTORE);
			if (currentData != null) {
				currentData.setCassendraOH(OH.floatValue());
				float s = currentData.getdifference();
			}
		}
	}

	public void setSterling(String itemIdentifier, String invKey, String sku,
			boolean scanCompletedStg) {
		if (itemIdentifier.isEmpty()) {
			if (scanCompleted && scanCompletedStg) {
				cassList.add((cassBuffer.toString()).substring(0,
						cassBuffer.length() - 2));
				cassCounter = 1;
				cassBuffer = new StringBuffer();
			}
		} else {
			InvDataSet currentData = skuDetails.get(sku + CURRENTSTORE);
			currentData.setItemIdentifier(itemIdentifier);
			currentData.setInvKey(invKey);
			if (cassCounter < 40000 && !InvRepConstants.scanCompleted) {
				cassBuffer.append("'" + itemIdentifier + "', ");
				cassCounter++;
			} else if (cassCounter == 40000) {
				buffer.append("'" + itemIdentifier + "' ");
				cassList.add(cassBuffer.toString());
				cassCounter = 1;
				cassBuffer = new StringBuffer();
			}
			cassendraDetails.put(invKey, new CassDataSet(sku, itemIdentifier));
		}
	}

	public void setSterlingDemand(String sku, float skuDemand) {
		InvDataSet currentData = skuDetails.get(sku);
		if (currentData != null) {
			currentData.setSkuDemand(skuDemand);
		}
	}

	public void checkCache(String sku) {
		if (!skuCache.containsKey(sku)) {
			if (sterlingCounter < 999 && !InvRepConstants.scanCompleted) {
				buffer.append("'" + sku + "', ");
				sterlingCounter++;
			} else if (sterlingCounter == 999) {
				buffer.append("'" + sku + "' ");
				itemList.add(buffer.toString());
				sterlingCounter = 1;
				buffer = new StringBuffer();
			}
		} else {
			SkuCacheDataSet currentData = skuCache.get(sku);
			setSterling(currentData.getItemIdentifier(),
					currentData.getInvKey(), sku, false);
		}
	}

	public void setStoreSku(String sku, float storeOH, boolean scanCompleted) {
		this.scanCompleted = scanCompleted;
		if (!sku.isEmpty()) {
			skuDetails.put(sku + CURRENTSTORE, new InvDataSet(sku, storeOH,
					CURRENTSTORE));
			checkCache(sku.trim());
			InvRepConstants.skuSet.add(sku);
		} else {
			try {
				itemList.add((buffer.toString()).substring(0,
						buffer.length() - 2));
			} catch (StringIndexOutOfBoundsException e) {
				// SKU list empty for store...
			}
			sterlingCounter = 1;
			buffer = new StringBuffer();
		}
	}

	/**
	 * Async Methods -Used for the Resync report implementation
	 */

	/**
	 * Get the on hand data for sku/store combinations from Cassandra DB
	 * 
	 * @param invKey
	 * @param OH
	 */
	public void setCasOHAsync(String invKey, Double OH) {
		if (AsyncStaticData.cassendraDetails.get(invKey) != null) {
			AsyncStaticData.cassendraDetails.get(invKey).setCassOH(
					OH.floatValue());
			InvDataSet currentData = AsyncStaticData.skuDetails
					.get(AsyncStaticData.cassendraDetails.get(invKey).getSku()
							+ AsyncStaticData.currentStore);
			if (currentData != null) {
				currentData.setCassendraOH(OH.floatValue());
				float s = currentData.getdifference();
			}
		}
	}

	/**
	 * Populate itemIdentifier data from Sterling for all sku data obtained from
	 * store
	 * 
	 * @param itemIdentifier
	 * @param invKey
	 * @param sku
	 * @param scanCompletedStg
	 */
	public void setSterlingAsync(String itemIdentifier, String invKey,
			String sku, boolean scanCompletedStg) {
		if (itemIdentifier.isEmpty() || itemIdentifier == null) {
			if (scanCompleted && scanCompletedStg) {
				AsyncStaticData.cassList.add((cassBuffer.toString()).substring(
						0, cassBuffer.length() - 2));
				cassCounter = 1;
				cassBuffer = new StringBuffer();
			}
		} else {
			InvDataSet currentData = AsyncStaticData.skuDetails.get(sku
					+ AsyncStaticData.currentStore);
			currentData.setItemIdentifier(itemIdentifier);
			currentData.setInvKey(invKey);
			if (cassCounter < 40000 && !AsyncStaticData.scanCompleted) {
				cassBuffer.append("'" + itemIdentifier + "', ");
				cassCounter++;
				// } else {
			} else if (cassCounter == 40000) {
				buffer.append("'" + itemIdentifier + "' ");
				AsyncStaticData.cassList.add(cassBuffer.toString());
				cassCounter = 1;
				cassBuffer = new StringBuffer();
			}
			AsyncStaticData.cassendraDetails.put(invKey, new CassDataSet(sku,
					itemIdentifier));
		}
	}

	/**
	 * Get Sku demand from Sterling DB.
	 * 
	 * @param sku
	 * @param skuDemand
	 */
	public void setSterlingDemandAsync(String sku, float skuDemand) {
		InvDataSet currentData = AsyncStaticData.skuDetails.get(sku);
		if (currentData != null) {
			currentData.setSkuDemand(skuDemand);
		}
	}

	/**
	 * Check if itemIdentifier data already exists in cache, so we don't have to
	 * query the DB again to retrieve it.
	 * 
	 * @param sku
	 */
	public void checkCacheAsync(String sku) {
		if (!AsyncStaticData.skuCache.containsKey(sku)) {
			if (sterlingCounter < 999 && !scanCompleted) {
				buffer.append("'" + sku + "', ");
				sterlingCounter++;
			} else if (sterlingCounter == 999) {
				buffer.append("'" + sku + "' ");
				AsyncStaticData.itemList.add(buffer.toString());
				sterlingCounter = 1;
				buffer = new StringBuffer();
			}
		} else {
			SkuCacheDataSet currentData = AsyncStaticData.skuCache.get(sku);
			setSterlingAsync(currentData.getItemIdentifier(),
					currentData.getInvKey(), sku, false);
		}
	}

	/**
	 * Populate data for store/sku combinations.
	 * 
	 * @param sku
	 * @param storeOH
	 * @param scanCompleted
	 */
	public void setStoreSkuAsync(String sku, float storeOH,
			boolean scanCompleted) {
		this.scanCompleted = scanCompleted;
		if (!sku.isEmpty()) {
			AsyncStaticData.skuDetails.put(sku + AsyncStaticData.currentStore,
					new InvDataSet(sku, storeOH, AsyncStaticData.currentStore));
			checkCacheAsync(sku.trim());
			AsyncStaticData.skuSet.add(sku);
		} else {
			try {
				AsyncStaticData.itemList.add((buffer.toString()).substring(0,
						buffer.length() - 2));
			} catch (IndexOutOfBoundsException ie) {
				// Catch and ignore. All items for store existed in cache so no
				// new items to add to itemlist.
			}
			sterlingCounter = 1;
			buffer = new StringBuffer();
		}
	}
}