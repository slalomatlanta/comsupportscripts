package com.homedepot.di.dl.support.invReport.dao;

import java.util.ArrayList;

/**
 * This class represents a row in the summary sheet of the Inventory Mismatch
 * report.
 * 
 * @author JXA8571
 *
 */
public class MismatchSummary implements Comparable<MismatchSummary> {
	private String storeNo;
	private int totalReviewed = 0;
	private int outOfSync = 0;
	private double outOfSyncRate = 0.0;
	private int sterlingGStoreCount = 0;
	private double sterlingGStoreDelta = 0.0;
	private int storeGSterlingCount = 0;
	private double storeGSterlingDelta = 0.0;

	private ArrayList<String> skusNotInSterling = new ArrayList<String>();

	public MismatchSummary(String storeNo) {
		this.storeNo = storeNo;
	}

	public String getStoreNo() {
		return storeNo;
	}

	public void setStoreNo(String storeNo) {
		this.storeNo = storeNo;
	}

	public int getTotalReviewed() {
		return totalReviewed;
	}

	public void setTotalReviewed(int totalReviewed) {
		this.totalReviewed = totalReviewed;
	}

	public int getOutOfSync() {
		return outOfSync;
	}

	public void setOutOfSync(int outOfSync) {
		this.outOfSync = outOfSync;
	}

	public double getOutOfSyncRate() {
		return outOfSyncRate;
	}

	public void setOutOfSyncRate(double outOfSyncRate) {
		this.outOfSyncRate = outOfSyncRate;
	}

	public int getSterlingGStoreCount() {
		return sterlingGStoreCount;
	}

	public void setSterlingGStoreCount(int sterlingGStoreCount) {
		this.sterlingGStoreCount = sterlingGStoreCount;
	}

	public double getSterlingGStoreDelta() {
		return sterlingGStoreDelta;
	}

	public void setSterlingGStoreDelta(double sterlingGStoreDelta) {
		this.sterlingGStoreDelta = sterlingGStoreDelta;
	}

	public int getStoreGSterlingCount() {
		return storeGSterlingCount;
	}

	public void setStoreGSterlingCount(int storeGSterlingCount) {
		this.storeGSterlingCount = storeGSterlingCount;
	}

	public double getStoreGSterlingDelta() {
		return storeGSterlingDelta;
	}

	public void setStoreGSterlingDelta(double storeGSterlingDelta) {
		this.storeGSterlingDelta = storeGSterlingDelta;
	}

	public ArrayList<String> getSkusNotInSterling() {
		return skusNotInSterling;
	}

	public void setSkusNotInSterling(ArrayList<String> skusNotInSterling) {
		this.skusNotInSterling = skusNotInSterling;
	}

	@Override
	public int compareTo(MismatchSummary o) {
		return this.storeNo.compareTo(o.storeNo);
	}
}