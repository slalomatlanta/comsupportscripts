package com.homedepot.di.dl.support.invReport.dao;

public class CassDataSet {

	public CassDataSet(String sku, String itemIdentifier) {
		
		this.sku = sku;		
		this.itemIdentifier = itemIdentifier;
	} 

	private String sku;
	private float cassOH;
	private String itemIdentifier;

	
	public String getSku() {
		return sku;
	}
	public void setSku(String sku) {
		this.sku = sku;
	}
	public float getCassOH() {
		return cassOH;
	}
	public void setCassOH(float cassOH) {
		this.cassOH = cassOH;
	}
	public String getItemIdentifier() {
		return itemIdentifier;
	}
	public void setItemIdentifier(String itemIdentifier) {
		this.itemIdentifier = itemIdentifier;
	}	
	

}
