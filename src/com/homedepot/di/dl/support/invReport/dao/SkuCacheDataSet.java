package com.homedepot.di.dl.support.invReport.dao;

public class SkuCacheDataSet {
	
	private String invKey;
	private String itemIdentifier;
	
	public SkuCacheDataSet(String invKey,String itemIdentifier) {
		this.invKey = invKey;
		this.itemIdentifier = itemIdentifier;
	}	
	
	public String getInvKey() {
		return invKey;
	}
	
	public String getItemIdentifier() {
		return itemIdentifier;
	}	

}
