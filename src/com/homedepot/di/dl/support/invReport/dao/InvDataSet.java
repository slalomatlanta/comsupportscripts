package com.homedepot.di.dl.support.invReport.dao;

public class InvDataSet {
	
    public InvDataSet(String sku,float storeOH,String storeNo) {	
		
		this.storeOH = storeOH;
		this.sku =  sku;
		this.storeNo =  storeNo;
		
	} 
	
	private String sku;
	private float storeOH;
	private float skuDemand;
	private String invKey;
	private String itemIdentifier;
	private float cassendraOH;
	private String storeNo;
	private float difference = 0;
	
	public float getSkuDifference() {
		return difference;
	}
	public String getStoreNo() {
		return storeNo;
	}
	public void setStoreNo(String storeNo) {
		this.storeNo = storeNo;
	}
	public String getSku() {
		return sku;
	}
	public void setSku(String sku) {
		this.sku = sku;
	}
	public float getStoreOH() {
		return storeOH;
	}
	public void setStoreOH(float storeOH) {
		this.storeOH = storeOH;
	}
	public float getSkuDemand() {
		return skuDemand;
	}
	public void setSkuDemand(float skuDemand) {
		this.skuDemand = skuDemand;
	}
	public String getInvKey() {
		return invKey;
	}
	public void setInvKey(String invKey) {
		this.invKey = invKey;
	}
	public String getItemIdentifier() {
		return itemIdentifier;
	}
	public void setItemIdentifier(String itemIdentifier) {
		this.itemIdentifier = itemIdentifier;
	}
	public float getCassendraOH() {
		return cassendraOH;
	}
	public void setCassendraOH(float cassendraOH) {
		this.cassendraOH = cassendraOH;
	}	
	
	public float getdifference() {		
		return (this.difference = this.storeOH - (this.cassendraOH + this.skuDemand));
	}	
	
	
}
