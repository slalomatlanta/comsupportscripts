package com.homedepot.di.dl.support.invReport.util;

import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.streaming.SXSSFCell;
import org.apache.poi.xssf.streaming.SXSSFRow;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

import com.homedepot.di.dl.support.invReport.dao.InvDataSet;
import com.homedepot.di.dl.support.invReport.dao.MismatchSummary;
import com.homedepot.di.dl.support.invReport.dao.ResponseDataSet;

/**
 * Generates and saves an excel report for inventory mismatches.
 * 
 * @author JXA8571
 *
 */
public class ExcelWriter {
	private SXSSFWorkbook wb = new SXSSFWorkbook();
	private Sheet s = null;
	private Row r = null;
	private Cell c = null;
	private CellStyle headerStyle = null;
	private CellStyle decimalStyle = null;
	private CellStyle percentStyle = null;

	private int rownum = 0;
	private int cellNum = 0;

	private String path;
	private GenericSelector gs;
	private ResponseDataSet rsp;

	// To be used for TOTAL row on Summary sheet.
	private int summaryTotalReviewed = 0;
	private int summaryOutOfSync = 0;
	private int summarySterlingGStoreCount = 0;
	private int summaryStoreGSterlingCount = 0;

	// To be used for negative store OH sheet.
	private ArrayList<InvDataSet> negativeStoreOHList = new ArrayList<InvDataSet>();

	private String file = "";

	public ExcelWriter() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Initialize the excel report.
	 */
	public ExcelWriter(String path, GenericSelector gs, ResponseDataSet rsp) {
		this(path);
		this.gs = gs;
		this.rsp = rsp;

		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		file = path + "MismatchSummary" + df.format(new Date().getTime())
				+ ".xlsx";

		InvRepConstants.latestFile = file;
	}

	public ExcelWriter(String path) {
		DataFormat dataF = wb.createDataFormat();
		this.path = path;

		wb.createSheet("Mismatch Summary");
		wb.createSheet("Mismatch Details");
		wb.createSheet("SKUs Not in Sterling");
		wb.createSheet("Negative Store OH");

		// Setup the header style
		headerStyle = wb.createCellStyle();
		headerStyle.setWrapText(true);
		headerStyle.setAlignment(CellStyle.ALIGN_CENTER);

		// Setup the style for decimal values
		decimalStyle = wb.createCellStyle();
		decimalStyle.setDataFormat(dataF.getFormat("#0.00"));

		// Setup the style for percent values
		percentStyle = wb.createCellStyle();
		percentStyle.setDataFormat(dataF.getFormat("#0.00%"));

		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		file = path + "ResyncMismatchSummary" + df.format(new Date().getTime())
				+ ".xlsx";
		AsyncStaticData.latestFile = file;
	}

	/**
	 * Append the data from a single store to the Summary sheet. If this is the
	 * first store, generate the header first.
	 * 
	 * The Summary sheet provides general statistics at a store level.
	 * 
	 * @param ms
	 */
	public void writeSummary(MismatchSummary ms) {
		Sheet s = wb.getSheetAt(0);
		if (s.getLastRowNum() == 0) {
			rownum = 0;
			cellNum = 0;
			r = s.createRow(rownum);
			rownum++;

			// Create header
			c = (SXSSFCell) r.createCell(cellNum);
			c.setCellValue("Store No");
			c.setCellStyle(headerStyle);
			cellNum++;

			c = r.createCell(cellNum);
			c.setCellValue("Total SKUs Reviewed");
			c.setCellStyle(headerStyle);
			cellNum++;

			c = r.createCell(cellNum);
			c.setCellValue("SKUs Out of Sync");
			c.setCellStyle(headerStyle);
			cellNum++;

			c = r.createCell(cellNum);
			c.setCellValue("Out of Sync Rate");
			c.setCellStyle(headerStyle);
			cellNum++;

			c = r.createCell(cellNum);
			c.setCellValue("Count of SKUs Sterling OH > Store OH");
			c.setCellStyle(headerStyle);
			cellNum++;

			c = r.createCell(cellNum);
			c.setCellValue("Avg Unit Delta");
			c.setCellStyle(headerStyle);
			cellNum++;

			c = r.createCell(cellNum);
			c.setCellValue("Count of SKUs Store OH > Sterling OH");
			c.setCellStyle(headerStyle);
			cellNum++;

			c = r.createCell(cellNum);
			c.setCellValue("Avg Unit Delta");
			c.setCellStyle(headerStyle);
			cellNum++;

			// set column widths (units for width is 1/256th of a character's
			// width)
			s.setColumnWidth(0, 10 * 256);
			s.setColumnWidth(1, 10 * 256);
			s.setColumnWidth(2, 10 * 256);
			s.setColumnWidth(3, 10 * 256);
			s.setColumnWidth(4, 15 * 256);
			s.setColumnWidth(5, 10 * 256);
			s.setColumnWidth(6, 15 * 256);
			s.setColumnWidth(7, 10 * 256);
		}

		// Generate Data Rows
		rownum = s.getLastRowNum() + 1;
		r = s.createRow(rownum);
		cellNum = 0;

		c = r.createCell(cellNum);
		c.setCellValue(ms.getStoreNo());
		cellNum++;

		c = r.createCell(cellNum);
		c.setCellValue(ms.getTotalReviewed());
		summaryTotalReviewed += ms.getTotalReviewed();
		cellNum++;

		c = r.createCell(cellNum);
		c.setCellValue(ms.getOutOfSync() - ms.getSkusNotInSterling().size());
		summaryOutOfSync += (ms.getOutOfSync() - ms.getSkusNotInSterling()
				.size());
		cellNum++;

		c = r.createCell(cellNum);
		c.setCellValue((((double) ms.getOutOfSync() - (double) ms
				.getSkusNotInSterling().size()) / (double) ms
				.getTotalReviewed()));
		c.setCellStyle(percentStyle);
		cellNum++;

		c = r.createCell(cellNum);
		c.setCellValue(ms.getSterlingGStoreCount());
		cellNum++;

		c = r.createCell(cellNum);
		c.setCellValue(ms.getSterlingGStoreDelta());
		c.setCellStyle(decimalStyle);
		cellNum++;

		c = r.createCell(cellNum);
		c.setCellValue(ms.getStoreGSterlingCount());
		cellNum++;

		c = r.createCell(cellNum);
		c.setCellValue(ms.getStoreGSterlingDelta());
		c.setCellStyle(decimalStyle);
		rownum++;
	}

	/**
	 * Append the TOTAL row to the Summary sheet.
	 */
	public void writeSummaryTotal(int storeListSize) {
		Sheet s = wb.getSheetAt(0);

		// Generate Data Rows
		rownum = s.getLastRowNum() + 2;
		r = s.createRow(rownum);
		cellNum = 0;

		c = r.createCell(cellNum);
		c.setCellValue("TOTAL");
		cellNum++;

		c = r.createCell(cellNum);
		c.setCellValue(summaryTotalReviewed);
		cellNum++;

		c = r.createCell(cellNum);
		c.setCellValue(summaryOutOfSync);
		cellNum++;

		c = r.createCell(cellNum);
		// c.setCellValue(100 * ((double) summaryOutOfSync / (double)
		// summaryTotalReviewed));
		c.setCellType(SXSSFCell.CELL_TYPE_FORMULA);
		c.setCellFormula("C" + (storeListSize + 3) + "/" + "B"
				+ (storeListSize + 3));
		c.setCellStyle(percentStyle);
		cellNum++;

		c = r.createCell(cellNum);
		c.setCellValue(summarySterlingGStoreCount);
		cellNum++;

		String formula = "AVERAGEIF('Mismatch Details'!F2:F"
				+ (1 + summaryOutOfSync);

		c = r.createCell(cellNum);
		// c.setCellValue(summarySterlingGStoreCount != 0 ? (double)
		// summarySterlingGStoreMismatch
		// / (double) summarySterlingGStoreCount
		// : 0.0);
		c.setCellType(SXSSFCell.CELL_TYPE_FORMULA);
		c.setCellFormula(formula + ",\"<0\")");
		c.setCellStyle(decimalStyle);
		cellNum++;

		c = r.createCell(cellNum);
		c.setCellValue(summaryStoreGSterlingCount);
		cellNum++;

		c = r.createCell(cellNum);
		// c.setCellValue(summaryStoreGSterlingCount != 0 ? (double)
		// summaryStoreGSterlingMismatch
		// / (double) summaryStoreGSterlingCount
		// : 0.0);

		c.setCellType(SXSSFCell.CELL_TYPE_FORMULA);
		c.setCellFormula(formula + ",\">0\")");
		c.setCellStyle(decimalStyle);
		rownum++;
	}

	/**
	 * Append the data from a single store to the Detail sheet. If this is the
	 * first store, generate the Header as well.
	 * 
	 * The Detail sheet contains all the store/sku combos that are mismatched.
	 * 
	 * @param store
	 * @param ms
	 * @param detailList
	 */
	public void writeDetail(String store, MismatchSummary ms,
			ArrayList<InvDataSet> detailList) {
		Sheet s = wb.getSheetAt(1);
		if (s.getLastRowNum() == 0) {
			rownum = 0;
			cellNum = 0;
			r = s.createRow(rownum);
			rownum++;

			// Create header
			c = r.createCell(cellNum);
			c.setCellValue("Store No");
			c.setCellStyle(headerStyle);
			cellNum++;

			c = r.createCell(cellNum);
			c.setCellValue("SKU No");
			c.setCellStyle(headerStyle);
			cellNum++;

			c = r.createCell(cellNum);
			c.setCellValue("Store OH");
			c.setCellStyle(headerStyle);
			cellNum++;

			c = r.createCell(cellNum);
			c.setCellValue("Sterling OH");
			c.setCellStyle(headerStyle);
			cellNum++;

			c = r.createCell(cellNum);
			c.setCellValue("Sterling Demand");
			c.setCellStyle(headerStyle);
			cellNum++;

			c = r.createCell(cellNum);
			c.setCellValue("Difference");
			c.setCellStyle(headerStyle);
			cellNum++;

			// set column widths (units for width is 1/256th of a character's
			// width)
			s.setColumnWidth(0, 10 * 256);
			s.setColumnWidth(1, 10 * 256);
			s.setColumnWidth(2, 10 * 256);
			s.setColumnWidth(3, 11 * 256);
			s.setColumnWidth(4, 10 * 256);
			s.setColumnWidth(5, 11 * 256);
		}

		rownum = s.getLastRowNum() + 1;

		int inSyncCount = 0;
		int storeGSterlingCount = 0;
		double storeGSterlingDelta = 0.0;
		int sterlingGStoreCount = 0;
		double sterlingGStoreDelta = 0.0;
		for (InvDataSet detail : detailList) {
			if (!ms.getSkusNotInSterling().contains(detail.getSku())) {
				double difference = detail.getdifference();
				if (detail.getStoreOH() < 0 && detail.getCassendraOH() == 0) {
					// Add negative store OH to list for display on negative OH
					// sheet.
					negativeStoreOHList.add(detail);
					inSyncCount++;
				} else if (difference != 0.0) {
					r = s.createRow(rownum);
					cellNum = 0;

					c = r.createCell(cellNum);
					c.setCellValue(detail.getStoreNo());
					cellNum++;

					c = r.createCell(cellNum);
					c.setCellValue(detail.getSku());
					cellNum++;

					c = r.createCell(cellNum);
					c.setCellValue(detail.getStoreOH());
					cellNum++;

					c = r.createCell(cellNum);
					c.setCellValue(detail.getCassendraOH());
					cellNum++;

					c = r.createCell(cellNum);
					c.setCellValue(detail.getSkuDemand());
					cellNum++;

					c = r.createCell(cellNum);
					c.setCellValue(difference);
					rownum++;

					if (difference > 0) {
						AsyncStaticData.currentMismatch++;
						InvRepConstants.CurrentMisMatch++;
						storeGSterlingCount++;
						storeGSterlingDelta += difference;
					} else {
						AsyncStaticData.currentMismatch++;
						InvRepConstants.CurrentMisMatch++;
						sterlingGStoreCount++;
						sterlingGStoreDelta += difference;
					}
				} else {
					inSyncCount++;
				}
			}
		}

		summaryStoreGSterlingCount += storeGSterlingCount;
		// summaryStoreGSterlingMismatch += storeGSterlingDelta;
		summarySterlingGStoreCount += sterlingGStoreCount;
		// summarySterlingGStoreMismatch += sterlingGStoreDelta;

		ms.setOutOfSync(ms.getTotalReviewed() - inSyncCount);
		ms.setStoreGSterlingCount(storeGSterlingCount);
		ms.setStoreGSterlingDelta(storeGSterlingCount == 0 ? 0
				: storeGSterlingDelta / storeGSterlingCount);
		ms.setSterlingGStoreCount(sterlingGStoreCount);
		ms.setSterlingGStoreDelta(sterlingGStoreCount == 0 ? 0
				: sterlingGStoreDelta / sterlingGStoreCount);
	}

	/**
	 * Append the data from a single store to the Missing SKUs sheet. If this is
	 * the first store, generate the Header as well.
	 * 
	 * The Missing SKUs sheet provides store/sku combos that are missing in
	 * Sterling.
	 * 
	 * @param ms
	 */
	public void writeMissingSKUs(MismatchSummary ms) {
		// Generate the SKUs not in Sterling sheet
		s = wb.getSheetAt(2);
		if (s.getLastRowNum() == 0) {
			rownum = 0;
			cellNum = 0;
			r = s.createRow(rownum);
			rownum++;

			// Create header
			c = r.createCell(cellNum);
			c.setCellValue("Store No");
			c.setCellStyle(headerStyle);
			cellNum++;

			c = r.createCell(cellNum);
			c.setCellValue("SKU No");
			c.setCellStyle(headerStyle);
			cellNum++;

			// set column widths (units for width is 1/256th of a character's
			// width)
			s.setColumnWidth(0, 10 * 256);
			s.setColumnWidth(1, 10 * 256);
		}

		rownum = s.getLastRowNum() + 1;
		for (String sku : ms.getSkusNotInSterling()) {
			r = s.createRow(rownum);
			cellNum = 0;

			c = r.createCell(cellNum);
			c.setCellValue(ms.getStoreNo());
			cellNum++;

			c = r.createCell(cellNum);
			c.setCellValue(sku);
			cellNum++;
			rownum++;
		}
	}

	/**
	 * Generate the sheet for Store/SKUs that have a negative Store OH. Do not
	 * include them in the mismatch details.
	 */
	public void writeNegativeStoreOH() {
		Sheet s = wb.getSheetAt(3);
		if (s.getLastRowNum() == 0) {
			rownum = 0;
			cellNum = 0;
			r = s.createRow(rownum);
			rownum++;

			// Create header
			c = r.createCell(cellNum);
			c.setCellValue("Store No");
			c.setCellStyle(headerStyle);
			cellNum++;

			c = r.createCell(cellNum);
			c.setCellValue("SKU No");
			c.setCellStyle(headerStyle);
			cellNum++;

			c = r.createCell(cellNum);
			c.setCellValue("Store OH");
			c.setCellStyle(headerStyle);
			cellNum++;

			c = r.createCell(cellNum);
			c.setCellValue("Sterling OH");
			c.setCellStyle(headerStyle);
			cellNum++;

			// set column widths (units for width is 1/256th of a character's
			// width)
			s.setColumnWidth(0, 10 * 256);
			s.setColumnWidth(1, 10 * 256);
			s.setColumnWidth(2, 10 * 256);
			s.setColumnWidth(3, 11 * 256);
		}

		rownum = s.getLastRowNum() + 1;

		for (InvDataSet detail : negativeStoreOHList) {
			r = s.createRow(rownum);
			cellNum = 0;

			c = r.createCell(cellNum);
			c.setCellValue(detail.getStoreNo());
			cellNum++;

			c = r.createCell(cellNum);
			c.setCellValue(detail.getSku());
			cellNum++;

			c = r.createCell(cellNum);
			c.setCellValue(detail.getStoreOH());
			cellNum++;

			c = r.createCell(cellNum);
			c.setCellValue(detail.getCassendraOH());
			cellNum++;
			rownum++;
		}
	}

	/**
	 * Save the generated excel report to the server and return the path +
	 * filename.
	 * 
	 * @return
	 */
	public String saveExcel() {
		String file = saveExcelAsync();

		rsp.setSummaryDataURL(file);
		// gs.sentMessageToClient(rsp);
		return file;
	}

	public String saveExcelAsync() {
		try {
			FileOutputStream out = new FileOutputStream(file);
			wb.write(out);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return file;
	}
}