package com.homedepot.di.dl.support.invReport.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import com.homedepot.di.dl.support.invReport.dao.CassDataSet;
import com.homedepot.di.dl.support.invReport.dao.InvDataSet;
import com.homedepot.di.dl.support.invReport.dao.SkuCacheDataSet;
import com.homedepot.di.dl.support.invReport.service.AsyncReportThread;

public class AsyncStaticData {
	// Is AsyncReport currently running
	public static boolean reportRunning = false;

	public static boolean scanCompleted = false;

	// Setup for Threads
	private static int poolSize = 1;
	public static BlockingQueue<Runnable> pool = new ArrayBlockingQueue<Runnable>(
			poolSize, true);
	public static ThreadPoolExecutor executor = new ThreadPoolExecutor(
			poolSize, poolSize, Long.MAX_VALUE, TimeUnit.MINUTES, pool);

	// Queue of stores to process
	public static Queue<String> storeQueue = new LinkedList<String>();

	// Excel report being built
	public static ExcelWriter asyncReport = null;

	//
	public static Map<String, InvDataSet> skuDetails = new HashMap<String, InvDataSet>();
	public static Map<String, CassDataSet> cassendraDetails = new HashMap<String, CassDataSet>();
	public static Map<String, SkuCacheDataSet> skuCache = new HashMap<String, SkuCacheDataSet>();
	public static ArrayList<String> itemList = new ArrayList<String>();
	public static ArrayList<String> storeList = new ArrayList<String>();
	public static ArrayList<String> cassList = new ArrayList<String>();
	public static int currentMismatch = 0;
	public static String currentStore;

	//
	public static HashSet<String> skuSet = new HashSet<String>();
	public static HashMap<String, String> keyToSkuMap = new HashMap<String, String>();
	public static int csvCount = 0;
	public static String latestFile = "";

	public static void processAsyncRequest() {
		// If executor is empty, start a thread to process stores
		if (executor.getActiveCount() < poolSize) {
			executor.execute(new AsyncReportThread());
		}
	}
}