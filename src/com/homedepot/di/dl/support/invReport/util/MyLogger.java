package com.homedepot.di.dl.support.invReport.util;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;

public class MyLogger {
	
	private File logFile;
	
	public MyLogger(){
		
	}
	
	public MyLogger(String fileName) {
		logFile = new File(fileName);
	}
	
	public MyLogger(File f) {
		logFile = f;
		
	}
	
public void log(String s, boolean logDate, boolean newLine) {
		
		try {
			FileWriter fw = new FileWriter(this.logFile,true);
			String date = "";
			if (logDate) {
				date = new Date().toString() + " : ";
			}
			
			fw.write(date + s);
			
			if (newLine) {
				fw.write(System.getProperty("line.separator"));				
			}

			
			fw.close();
		} catch (IOException ex) {
			System.err.println("Couldn't log this: "+s);
		}
		
	}

	public void deleteFile(String OutputFile) {
		logFile.delete();
		
	}

}
