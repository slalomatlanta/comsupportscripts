package com.homedepot.di.dl.support.invReport.util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.log4j.Logger;
import org.json.JSONObject;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Metadata;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
//import com.google.gson.Gson;
import com.homedepot.di.dl.support.invReport.dao.InvDataMap;
import com.homedepot.di.dl.support.invReport.dao.InvDataSet;
//import com.homedepot.di.dl.support.invReport.dao.ResponseDataSet;
import com.homedepot.di.dl.support.invReport.dao.SkuCacheDataSet;
import com.homedepot.di.dl.support.util.Constants;
import com.homedepot.di.dl.support.util.ConstantsQuery;
import com.homedepot.di.dl.support.util.DBConnectionUtil;

//import com.homedepot.di.dl.support.invReport.web.ConnectSocket;

public class GenericSelector extends InvRepConstants {
	private static final Logger logger = Logger
			.getLogger(GenericSelector.class);
	final static MyLogger mylogger = new MyLogger("/opt/isv/apache-tomcat/logs/InvReport.log");

	InvDataMap currentData = new InvDataMap();

	public String getSterlingData(String sql, boolean isDemand,
			boolean isLastSet) throws SQLException {
		DBConnectionUtil dbConn = new DBConnectionUtil();
		String resultJson = "";
		// Connection sscConn =
		// dbConn.getJNDIConnection(Constants.STERLING_QA_RO_JNDI);
		// // QA
		// Connection sscConn =
		// dbConn.getJNDIConnection(Constants.STERLING_PR_RO_JNDI);
		// // PR
//		Connection sscConn = dbConn
//				.getJNDIConnection(Constants.STERLING_SSC_RO_JNDI); // PR
		
/*Creating SSC Connection*/
		 final String driverClass = "oracle.jdbc.driver.OracleDriver";
					 final String connectionURL =
					 "jdbc:oracle:thin://@spragor10-scan.homedepot.com:1521/dpr77mm_sro01";
					 final String uName = "MMUSR01";
					 final String uPassword = "COMS_MMUSR01";
					 
		
					 Connection sscConn = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			
			mylogger.log("Establishing connection to Sterling db - Start", true, true);
			Class.forName(driverClass);
			sscConn = DriverManager.getConnection(connectionURL, uName,
			 uPassword);
			
			mylogger.log("Establishing connection to Sterling db - Complete/Established", true, true);
			
			stmt = sscConn.createStatement();
			
			mylogger.log("Sterling Query Execution - Start", true, true);
			
			rs = stmt.executeQuery(sql);
			
			mylogger.log("Sterling Query Execution - End", true, true);
			
			if (isDemand) {
				while (rs.next()) {
					currentData.setSterlingDemand(rs.getString(1),
							rs.getFloat(2));
				}
			} else {
				while (rs.next()) {
					skuCache.put(rs.getString(3), new SkuCacheDataSet(rs
							.getString(2).trim(), rs.getString(1)));
					currentData.setSterling(rs.getString(1), rs.getString(2)
							.trim(), rs.getString(3), false);

					InvRepConstants.keyToSkuMap.put(rs.getString(2).trim(),
							rs.getString(3));
					// Constants.skuSet.remove(rs.getString(3));
				}
				if (isLastSet) {
					currentData.setSterling("", "", "", true);
				}
			}
		} catch (SQLException e) {
			logger.debug("Caught exception while executing query" + e);
			mylogger.log("Caught exception while executing query" + e, true, true);
		} catch (NullPointerException e) {
			e.printStackTrace();
			logger.debug("Null Pointer while accessing DB" + e);
			mylogger.log("Null Pointer while accessing DB" + e, true, true);
		} 
		catch (Exception e) {
			e.printStackTrace();
			logger.debug("Exception occured" + e);
			mylogger.log("Exception occured" + e, true, true);
		}
		finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					// Ignore
				}
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					// Ignore
				}
			}
			if (sscConn != null) {
				try {
					sscConn.close();
					mylogger.log("Connection to Sterling DB - closed", true, true);
				} catch (Exception e) {
					// Ignore
				}
			}
		}

		return resultJson;

	}

	public void getStoreSkuData() {
		
		Connection con = null;
		Statement stmt = null;
		ResultSet sku_rs1 = null;
		
	mylogger.log("Beginning Store Query Execution", true, true);
		try {
			mylogger.log("Connecting to Store - Begin", true, true);
			DBConnectionUtil dbConn = new DBConnectionUtil();
			con = dbConn.getStoreconnection(CURRENTSTORE);

			mylogger.log("Connecting to Store - Complete", true, true);
			
			stmt = con.createStatement();
			mylogger.log("Creating Store query statement" + stmt, true, true);
			mylogger.log("Executing Store Query - Begin", true, true);
			sku_rs1 = stmt.executeQuery(ConstantsQuery.STORE_SKUQUERY_SELECT);

			mylogger.log("Executing Store Query - End", true, true);
			//mylogger.log("Executing Store Query - End", true, true);
			while (sku_rs1.next()) {
				currentData.setStoreSku(sku_rs1.getString(1),
						sku_rs1.getFloat(5), false);
			}
			currentData.setStoreSku("", 0, true);
		} catch (SQLException e) {
			logger.error("Store:" + CURRENTSTORE + " has exception :" + e);
			mylogger.log("Store:" + CURRENTSTORE + " has exception :" + e, true, true);
			
		} finally {
			if (sku_rs1 != null) {
				try {
					sku_rs1.close();
				} catch (SQLException e) {
					// Ignore
					mylogger.log("SQL Exception During Execution" + e, true, true);
				}
			}
			if (stmt != null) {
				try {
					stmt.close();
					
					
				} catch (SQLException e) {
					mylogger.log("SQL Exception During Execution" + e, true, true);
					// Ignore
				}
			}
			if (con != null) {
				try {
					con.close();
					mylogger.log("Store Connection closed", true, true);
				} catch (SQLException e) {
					// Ignore
					mylogger.log("SQL Exception During Execution" + e, true, true);
				}
			}
			mylogger.log("End of Store Query Execution", true, true);
		}
	}

	public void getCassandraDetails(String item) {
		Cluster cluster = null;
		try {
			// Temp fix for trailing comma that randomly shows up
			// Need to track down where it comes from
			if (item.trim().charAt(item.trim().length() - 1) == ',') {
				item = item.substring(0, item.trim().length() - 1);
			}
			// final String server = "gsqa0504"; // QA
			final String server = "cclidic3"; // PR
			final String keyspace = "availabilitycache";
			final String query = "SELECT  avl_data  FROM availabilitycache.INV_INVENTORY_AVAILABILITY "
					+ "WHERE  ITEM_IDENTIFIER in ("
					+ item
					+ ")  AND "
					+ "ORG_CODE = '{node-level}'  AND  LOC_ID IN ( '"
					+ CURRENTSTORE + "' );";

			cluster = Cluster.builder()
					.withCredentials("eocreader", "Qi2Gr20xdwjWyr28") // PR
					// .withCredentials("eocreader", "eocreader") // QA
					.addContactPoints(server).build();
			Session session = cluster.connect(keyspace);
			Metadata metadata = cluster.getMetadata();

			final com.datastax.driver.core.ResultSet results = (com.datastax.driver.core.ResultSet) session
					.execute(query);

			for (Row rows : results) {
				String currentRows = rows.toString();
				JSONObject obj = new JSONObject(currentRows.substring(4,
						(currentRows.length() - 1)));
				currentData.setCasOH(obj.getString("InvKey").trim(),
						obj.getDouble("OHQty"));

				InvRepConstants.skuSet.remove(InvRepConstants.keyToSkuMap
						.get(obj.getString("InvKey").trim()));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (cluster != null) {
				try {
					cluster.close();
				} catch (Exception e) {
					// Ignore
				}
			}
		}
	}

	public String generateCSV(String path) {
		FileWriter writer;

		// String outputFile = "st" + CURRENTSTORE + "-" + new Date().getTime()
		// + ".csv";
		String outputFile = "test.csv";
		try {

			writer = new FileWriter(path + outputFile);
			writer.append("Store No");
			writer.append(',');
			writer.append("SKU No");
			writer.append(',');
			writer.append("Store OH");
			writer.append(',');
			writer.append("Sterling OH");
			writer.append(',');
			writer.append("Sterling Demand");
			writer.append(',');
			writer.append("Difference");
			writer.append('\n');
			// Constants.skuDetails.forEach((k, result) -> {
			for (String sku : InvRepConstants.skuDetails.keySet()) {
				InvDataSet result = InvRepConstants.skuDetails.get(sku);

				if (result.getdifference() != 0 && result.getStoreOH() >= 0) {
					try {
						CurrentMisMatch++;
						writer.append(result.getStoreNo());
						writer.append(',');
						writer.append(result.getSku());
						writer.append(',');
						writer.append(Float.toString(result.getStoreOH()));
						writer.append(',');
						writer.append(Float.toString(result.getCassendraOH()));
						writer.append(',');
						writer.append(Float.toString(result.getSkuDemand()));
						writer.append(',');
						writer.append(Float.toString(result.getdifference()));
						writer.append('\n');
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}

			}
			// }) ;

			writer.flush();
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {

		}
		return "/InventoryReport/data/" + outputFile;

	}

	/*
	 * Read test data from a CSV file.
	 */
	public void getCSVStoreData() {
		try {
			BufferedReader br = null;
			String file = "";
			// br = new BufferedReader(new FileReader("c:\\Script\\sata.csv"));

			if (InvRepConstants.csvCount == 0) {
				file = "c:\\Script\\data1.csv";
			} else if (InvRepConstants.csvCount == 1) {
				file = "c:\\Script\\data2.csv";
			} else if (InvRepConstants.csvCount == 2) {
				file = "c:\\Script\\data3.csv";
			} else if (InvRepConstants.csvCount == 3) {
				file = "c:\\Script\\data4.csv";
			} else if (InvRepConstants.csvCount == 4) {
				file = "c:\\Script\\data5.csv";
			} else if (InvRepConstants.csvCount == 5) {
				file = "c:\\Script\\data6.csv";
			} else if (InvRepConstants.csvCount == 6) {
				file = "c:\\Script\\data7.csv";
			} else if (InvRepConstants.csvCount == 7) {
				file = "c:\\Script\\data8.csv";
			} else if (InvRepConstants.csvCount == 8) {
				file = "c:\\Script\\data9.csv";
			} else if (InvRepConstants.csvCount == 9) {
				file = "c:\\Script\\data10.csv";
			} else if (InvRepConstants.csvCount == 10) {
				file = "c:\\Script\\data11.csv";
			} else if (InvRepConstants.csvCount == 11) {
				file = "c:\\Script\\data12.csv";
			} else if (InvRepConstants.csvCount == 12) {
				file = "c:\\Script\\data13.csv";
			} else if (InvRepConstants.csvCount == 13) {
				file = "c:\\Script\\data14.csv";
			} else if (InvRepConstants.csvCount == 14) {
				file = "c:\\Script\\data15.csv";
			} else {
				file = "c:\\Script\\data1.csv";
			}

			br = new BufferedReader(new FileReader(file));

			String line = "";
			while ((line = br.readLine()) != null) {
				if (!(",,".equalsIgnoreCase(line))) {
					String tokens[] = line.split(",");
					currentData.setStoreSku(tokens[0].trim(),
							Float.parseFloat(tokens[4].trim()), false);
				}
			}
			currentData.setStoreSku("", 0, true);
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// public void sentMessageToClient(ResponseDataSet data) {
	// try {
	// Gson json = new Gson();
	// ConnectSocket.broadcastMessage(json.toJson(data));
	// } catch (Exception e1) {
	// System.out.println("Socket message Failed - "
	// + GregorianCalendar.getInstance().getTime());
	// e1.printStackTrace();
	// }
	// }

	public InvDataMap getCurrentData() {
		return currentData;
	}
}