package com.homedepot.di.dl.support.invReport.util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.log4j.Logger;
import org.json.JSONObject;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Metadata;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import com.homedepot.di.dl.support.invReport.dao.InvDataMap;
import com.homedepot.di.dl.support.invReport.dao.InvDataSet;
import com.homedepot.di.dl.support.invReport.dao.SkuCacheDataSet;
import com.homedepot.di.dl.support.util.Constants;
import com.homedepot.di.dl.support.util.ConstantsQuery;
import com.homedepot.di.dl.support.util.DBConnectionUtil;

public class DBSelector {
	private static final Logger logger = Logger.getLogger(DBSelector.class);

	InvDataMap currentData = new InvDataMap();

	public String getSterlingData(String sql, boolean isDemand,
			boolean isLastSet) throws SQLException {
		DBConnectionUtil dbConn = new DBConnectionUtil();
		String resultJson = "";
		// Connection sscConn =
		// dbConn.getJNDIConnection(Constants.STERLING_QA_RO_JNDI);
		// QA
		// Connection sscConn = dbConn.getJNDIConnection(STERLING_PR_RO_JNDI);
		// // PR
		Connection sscConn = dbConn
				.getJNDIConnection(Constants.STERLING_SSC_RO_JNDI); // PR
		Statement stmt = null;
		ResultSet rs = null;
		try {
			stmt = sscConn.createStatement();
			rs = stmt.executeQuery(sql);
			if (isDemand) {
				while (rs.next()) {
					currentData.setSterlingDemandAsync(rs.getString(1),
							rs.getFloat(2));
				}
			} else {
				while (rs.next()) {
					AsyncStaticData.skuCache.put(
							rs.getString(3),
							new SkuCacheDataSet(rs.getString(2).trim(), rs
									.getString(1)));
					currentData.setSterlingAsync(rs.getString(1),
							rs.getString(2).trim(), rs.getString(3), false);

					AsyncStaticData.keyToSkuMap.put(rs.getString(2).trim(),
							rs.getString(3));

					// AsyncStaticData.skuSet.remove(rs.getString(3));
				}
				if (isLastSet) {
					currentData.setSterlingAsync("", "", "", true);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
			logger.debug("Caught exception while executing query " + e);
		} catch (NullPointerException e) {
			e.printStackTrace();
			logger.debug("Null Pointer while accessing DB " + e);
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					// Ignore
				}
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					// Ignore
				}
			}
			if (sscConn != null) {
				try {
					sscConn.close();
				} catch (Exception e) {
					// Ignore
				}
			}
		}

		return resultJson;
	}

	public void getStoreSkuData() {
		
		Connection con = null;
		Statement stmt = null;
		ResultSet sku_rs1 = null;
		
		try {
			DBConnectionUtil dbConn = new DBConnectionUtil();
			con = dbConn.getStoreconnection(AsyncStaticData.currentStore);
			stmt = con.createStatement();
			sku_rs1 = stmt.executeQuery(ConstantsQuery.STORE_SKUQUERY_SELECT);

			while (sku_rs1.next()) {
				currentData.setStoreSkuAsync(sku_rs1.getString(1),
						sku_rs1.getFloat(5), false);
			}
			currentData.setStoreSkuAsync("", 0, true);
		} catch (SQLException e) {
			logger.error("Store:" + AsyncStaticData.currentStore
					+ " has exception :" + e);
		} finally {
			if (sku_rs1 != null) {
				try {
					sku_rs1.close();
				} catch (SQLException e) {
					// Ignore
				}
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					// Ignore
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					// Ignore
				}
			}
		}
	}

	public void getCassandraDetails(String item) {
		Cluster cluster = null;
		try {
			// Temp fix for trailing comma that randomly shows up
			// Need to track down where it comes from
			if (item.trim().charAt(item.trim().length() - 1) == ',') {
				item = item.substring(0, item.trim().length() - 1);
			}
//			 final String server = "gsqa0504"; // QA
			final String server = "cclidic3"; // PR
			final String keyspace = "availabilitycache";
			final String query = "SELECT  avl_data  FROM availabilitycache.INV_INVENTORY_AVAILABILITY "
					+ "WHERE  ITEM_IDENTIFIER in ("
					+ item
					+ ")  AND "
					+ "ORG_CODE = '{node-level}'  AND  LOC_ID IN ( '"
					+ AsyncStaticData.currentStore + "' );";

			cluster = Cluster.builder()
					.withCredentials("eocreader", "Qi2Gr20xdwjWyr28") // PR
//					 .withCredentials("eocreader", "eocreader") // QA
					.addContactPoints(server).build();
			Session session = cluster.connect(keyspace);
			Metadata metadata = cluster.getMetadata();

			final com.datastax.driver.core.ResultSet results = (com.datastax.driver.core.ResultSet) session
					.execute(query);

			for (Row rows : results) {
				String currentRows = rows.toString();
				JSONObject obj = new JSONObject(currentRows.substring(4,
						(currentRows.length() - 1)));
				currentData.setCasOHAsync(obj.getString("InvKey").trim(),
						obj.getDouble("OHQty"));

				AsyncStaticData.skuSet.remove(AsyncStaticData.keyToSkuMap
						.get(obj.getString("InvKey").trim()));
			}
		} catch (Exception e) {
			logger.error("Exception connecting to Cassandra DB " + e);
			e.printStackTrace();
		} finally {
			if (cluster != null) {
				try {
					cluster.close();
				} catch (Exception e) {
					// Ignore
				}
			}
		}
	}

	public String generateCSV(String path) {
		FileWriter writer;

		// String outputFile = "st" + CURRENTSTORE + "-" + new Date().getTime()
		// + ".csv";
		String outputFile = "test.csv";
		try {

			writer = new FileWriter(path + outputFile);
			writer.append("Store No");
			writer.append(',');
			writer.append("SKU No");
			writer.append(',');
			writer.append("Store OH");
			writer.append(',');
			writer.append("Sterling OH");
			writer.append(',');
			writer.append("Sterling Demand");
			writer.append(',');
			writer.append("Difference");
			writer.append('\n');
			for (String sku : AsyncStaticData.skuDetails.keySet()) {
				InvDataSet result = AsyncStaticData.skuDetails.get(sku);

				if (result.getdifference() != 0 && result.getStoreOH() >= 0) {
					try {
						AsyncStaticData.currentMismatch++;
						writer.append(result.getStoreNo());
						writer.append(',');
						writer.append(result.getSku());
						writer.append(',');
						writer.append(Float.toString(result.getStoreOH()));
						writer.append(',');
						writer.append(Float.toString(result.getCassendraOH()));
						writer.append(',');
						writer.append(Float.toString(result.getSkuDemand()));
						writer.append(',');
						writer.append(Float.toString(result.getdifference()));
						writer.append('\n');
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
			// }) ;

			writer.flush();
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {

		}
		return "/InventoryReport/data/" + outputFile;
	}

	/*
	 * Read test data from a CSV file.
	 */
	public void getCSVStoreData() {
		try {
			BufferedReader br = null;
			String file = "";

			if (InvRepConstants.csvCount == 0) {
				file = "c:\\Script\\data1.csv";
			} else if (InvRepConstants.csvCount == 1) {
				file = "c:\\Script\\data2.csv";
			} else if (InvRepConstants.csvCount == 2) {
				file = "c:\\Script\\data3.csv";
			} else if (InvRepConstants.csvCount == 3) {
				file = "c:\\Script\\data4.csv";
			} else if (InvRepConstants.csvCount == 4) {
				file = "c:\\Script\\data5.csv";
			} else if (InvRepConstants.csvCount == 5) {
				file = "c:\\Script\\data6.csv";
			} else if (InvRepConstants.csvCount == 6) {
				file = "c:\\Script\\data7.csv";
			} else if (InvRepConstants.csvCount == 7) {
				file = "c:\\Script\\data8.csv";
			} else if (InvRepConstants.csvCount == 8) {
				file = "c:\\Script\\data9.csv";
			} else if (InvRepConstants.csvCount == 9) {
				file = "c:\\Script\\data10.csv";
			} else if (InvRepConstants.csvCount == 10) {
				file = "c:\\Script\\data11.csv";
			} else if (InvRepConstants.csvCount == 11) {
				file = "c:\\Script\\data12.csv";
			} else if (InvRepConstants.csvCount == 12) {
				file = "c:\\Script\\data13.csv";
			} else if (InvRepConstants.csvCount == 13) {
				file = "c:\\Script\\data14.csv";
			} else if (InvRepConstants.csvCount == 14) {
				file = "c:\\Script\\data15.csv";
			} else {
				file = "c:\\Script\\data1.csv";
			}

			br = new BufferedReader(new FileReader(file));

			String line = "";
			while ((line = br.readLine()) != null) {
				if (!(",,".equalsIgnoreCase(line))) {
					String tokens[] = line.split(",");
					currentData.setStoreSkuAsync(tokens[0].trim(),
							Float.parseFloat(tokens[4].trim()), false);
				}
			}
			currentData.setStoreSkuAsync("", 0, true);
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public InvDataMap getCurrentData() {
		return currentData;
	}
}