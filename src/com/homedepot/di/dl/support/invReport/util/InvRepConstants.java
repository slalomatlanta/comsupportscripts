package com.homedepot.di.dl.support.invReport.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;

import com.homedepot.di.dl.support.invReport.dao.CassDataSet;
import com.homedepot.di.dl.support.invReport.dao.InvDataSet;
import com.homedepot.di.dl.support.invReport.dao.SkuCacheDataSet;

public class InvRepConstants {
	// Constants
	public static String CURRENTSTORE = "";
	public static boolean scanCompleted = false;
	public static long CurrentMisMatch = 0;
	public static int currentSterlingGStore = 0;
	public static double sterlingGStoreDelta = 0.0;
	public static int currentStoreGSterling = 0;
	public static double storeGSterlingDelta = 0.0;
	public static String filePath = "/opt/isv/apache-tomcat/webapps/COMSupportScripts/invt/data/";

	// PR
	// public static String STERLING_SSC_RO_JNDI =
	// "jdbc/ORCL.PPRMM78X-DPR077MM.001"; // Sterling
	// SSC
	// Read
	// only
	// DB
	// Connection
	// public static String STERLING_ATC_RW_JNDI =
	// "jdbc/ORCL.PPRMM77X-DPR077MM.001";// Sterling
	// ATC
	// Read
	// Write
	// DB
	// Connection
	// public static String COMT_SSC_RO_JNDI =
	// "jdbc/ORCL.PPRMM78X-DPR078MM.001";// Comt
	// SSC
	// Read
	// only
	// DB
	// connection
	// public static String COMT_ATC_RO_JNDI =
	// "jdbc/ORCL.PPRMM77X-DPR078MM.001";// Comt
	// ATC
	// Read
	// write

	// public static String STERLING_PR_RO_JNDI = "jdbc/STERLING_PR_RO_JNDI";

	// Global Data
	public static Map<String, InvDataSet> skuDetails = new HashMap<String, InvDataSet>();
	public static Map<String, CassDataSet> cassendraDetails = new HashMap<String, CassDataSet>();
	public static Map<String, SkuCacheDataSet> skuCache = new HashMap<String, SkuCacheDataSet>();
	public static ArrayList<String> itemList = new ArrayList<String>();
	public static ArrayList<String> storeList = new ArrayList<String>();
	public static ArrayList<String> cassList = new ArrayList<String>();

	public static HashSet<String> skuSet = new HashSet<String>();
	public static HashMap<String, String> keyToSkuMap = new HashMap<String, String>();
	public static int csvCount = 0;
	public static String latestFile = "";

	// QA
	// public static String COMT_QA_RO_JNDI = "jdbc/ORCL.PPRMM77Z-DPR077.001";
	// public static String STERLING_QA_RO_JNDI =
	// "jdbc/ORCL.PPRMM77Z-DPR077.002";

	// LCP
	// public static String QA = "QA";
	// public static String PR = "PR";

	// Databases
	// public static String COM = "COM";
	// public static String Sterling = "STG";
	// public static String Store = "STR";

	// Queries
	// PR
	// public static String STG_ITEMIDENTIFIER_SELECT =
	// "SELECT trim(INVI.ITEM_ID) || trim(INVI.UOM) || trim(INVI.PRODUCT_CLASS)|| trim(INVI.ORGANIZATION_CODE),INVI.INVENTORY_ITEM_KEY,IA.alias_value "
	// +
	// "FROM THD01.YFS_ITEM IT ,THD01.YFS_ITEM_ALIAS IA,THD01.YFS_INVENTORY_ITEM INVI "
	// +
	// "WHERE INVI.ITEM_ID=IT.ITEM_ID AND INVI.UOM=IT.UOM  AND IT.ITEM_KEY = ia.ITEM_KEY  AND ia.alias_name='SKU'  AND INVI.ORGANIZATION_CODE = 'HDUS' AND ia.alias_value IN (";

	// QA
	// public static String STG_ITEMIDENTIFIER_SELECT =
	// "SELECT trim(INVI.ITEM_ID) || trim(INVI.UOM) || trim(INVI.PRODUCT_CLASS)|| trim(INVI.ORGANIZATION_CODE),INVI.INVENTORY_ITEM_KEY,IA.alias_value "
	// + "FROM YFS_ITEM IT ,YFS_ITEM_ALIAS IA,YFS_INVENTORY_ITEM INVI "
	// +
	// "WHERE INVI.ITEM_ID=IT.ITEM_ID AND INVI.UOM=IT.UOM  AND IT.ITEM_KEY = ia.ITEM_KEY  AND ia.alias_name='SKU'  AND INVI.ORGANIZATION_CODE = 'HDUS' AND ia.alias_value IN (";

	// public static String STORE_SKUQUERY_SELECT =
	// "select sku_nbr,mer_dept_nbr,mer_class_nbr,mer_sub_class_nbr,oh_qty, sku_typ_cd from sku "
	// +
	// "where sku_typ_cd != 'B' and sku_typ_cd != 'F' and sku_typ_cd != 'S' and (CURR_RMETH_CD != 4 "
	// + "or CURR_RMETH_CD is null) and oh_qty != 0";

	// public static String STG_ITEMIDENTIFIER_SELECT =
	// "SELECT trim(INVI.ITEM_ID) || trim(INVI.UOM) || trim(INVI.PRODUCT_CLASS)|| trim(INVI.ORGANIZATION_CODE),INVI.INVENTORY_ITEM_KEY,IA.alias_value "
	// + "FROM YFS_ITEM IT ,YFS_ITEM_ALIAS IA,YFS_INVENTORY_ITEM INVI "
	// +
	// "WHERE INVI.ITEM_ID=IT.ITEM_ID AND INVI.UOM=IT.UOM  AND IT.ITEM_KEY = ia.ITEM_KEY  AND ia.alias_name='SKU'  AND INVI.ORGANIZATION_CODE = 'HDUS' AND ia.alias_value IN (";

	public static String STG_DEMAND_SELECT(String str) {
		// PR
		return ("select trim(yia.ALIAS_VALUE) || trim(yid.SHIPNODE_KEY), sum(yid.QUANTITY)  from "
				+ "THD01.yfs_inventory_item yii, THD01.yfs_inventory_demand yid, THD01.yfs_item_alias yia, THD01.yfs_item yi "
				+ "where yii.INVENTORY_ITEM_KEY = yid.INVENTORY_ITEM_KEY "
				+ "and yii.ITEM_ID = yi.ITEM_ID and yi.ITEM_KEY = yia.item_key "
				+ "and yia.ALIAS_NAME = 'SKU' and yid.DEMAND_TYPE <> 'RESERVED' and yid.SHIPNODE_KEY = '"
				+ str + "' group by yid.SHIPNODE_KEY, yia.ALIAS_VALUE " + "order by yid.SHIPNODE_KEY, yia.ALIAS_VALUE ");

		// QA
		// return
		// ("select trim(yia.ALIAS_VALUE) || trim(yid.SHIPNODE_KEY), sum(yid.QUANTITY)  from "
		// +
		// "yfs_inventory_item yii, yfs_inventory_demand yid, yfs_item_alias yia, yfs_item yi "
		// + "where yii.INVENTORY_ITEM_KEY = yid.INVENTORY_ITEM_KEY "
		// + "and yii.ITEM_ID = yi.ITEM_ID and yi.ITEM_KEY = yia.item_key "
		// +
		// "and yia.ALIAS_NAME = 'SKU' and yid.DEMAND_TYPE <> 'RESERVED' and yid.SHIPNODE_KEY = '"
		// + str + "' group by yid.SHIPNODE_KEY, yia.ALIAS_VALUE " +
		// "order by yid.SHIPNODE_KEY, yia.ALIAS_VALUE ");
	}
}