package com.homedepot.di.dl.support.invReport.service;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;

import org.apache.log4j.Logger;

import com.homedepot.di.dl.support.invReport.dao.InvDataSet;
import com.homedepot.di.dl.support.invReport.dao.MismatchSummary;
import com.homedepot.di.dl.support.invReport.util.AsyncStaticData;
import com.homedepot.di.dl.support.invReport.util.DBSelector;
import com.homedepot.di.dl.support.invReport.util.ExcelWriter;
import com.homedepot.di.dl.support.invReport.util.InvRepConstants;
import com.homedepot.di.dl.support.invReport.util.MyLogger;
import com.homedepot.di.dl.support.util.ConstantsQuery;

public class AsyncReportThread implements Runnable {
	final static Logger logger = Logger.getLogger(AsyncReportThread.class);
	final static MyLogger mylogger = new MyLogger ("/opt/isv/apache-tomcat/logs/invrpt.log");

	@Override
	public void run() {
		// Processing
		DBSelector ds = new DBSelector();
		MismatchSummary ms;
		InvRepConstants.csvCount = 0;

		if (AsyncStaticData.latestFile.trim().isEmpty()) {
			AsyncStaticData.storeList.clear();
			//
			// Thread t = new Thread(new Runnable() {
			// @Override
			// public void run() {
			AsyncStaticData.asyncReport = new ExcelWriter(
					InvRepConstants.filePath);
			// }
			// });
			// t.start();

			// System.out.println("Report created");
		}

		while (AsyncStaticData.storeQueue.peek() != null) {
			// System.out.println("storeQueue: " + AsyncStaticData.storeQueue);

			AsyncStaticData.skuSet.clear();
			AsyncStaticData.currentStore = AsyncStaticData.storeQueue.poll();
			String currentStore = AsyncStaticData.currentStore;
			AsyncStaticData.storeList.add(currentStore);

			logger.debug(new Date().toString()
					+ ": Beginning Inventory Mismatch analysis for store "
					+ currentStore);

			ms = new MismatchSummary(currentStore);

			// System.out.println("storeQueue after: "
			// + AsyncStaticData.storeQueue);
			// System.out.println("currentStore: " + currentStore);

			// Read Data from CSV for Production Store when running in
			// Local.
			// ds.getCSVStoreData();
			// InvRepConstants.csvCount++;

			logger.debug(new Date().toString()
					+ ": Obtaining SKU and OH data from Store");
			// Read Data from DB for Production
			ds.getStoreSkuData();

			ms.setTotalReviewed(AsyncStaticData.skuDetails.size());

			int isLast = 0;

			logger.debug(new Date().toString()
					+ ": Obtaining item identifier data from Sterling");
			// System.out.println("Before itemidentifier.");
			// Obtain sterling itemIdentifier information for each sku
			for (String item : AsyncStaticData.itemList) {
				try {
					ds.getSterlingData(ConstantsQuery.STG_ITEMIDENTIFIER_SELECT
							+ item + ")", false,
							isLast == AsyncStaticData.itemList.size() - 1);
					isLast++;
				} catch (SQLException e) {
					logger.error("Call to Sterling ItemIdentifier Service failed - "
							+ GregorianCalendar.getInstance().getTime());
					e.printStackTrace();
				}
			}
			// System.out.println("After itemidentifier.");

			// If store contained no new SKUs, we need to trigger that scan is
			// completed.
			if (AsyncStaticData.itemList.isEmpty()) {
				ds.getCurrentData().setSterlingAsync("", "", "", true);
			}

			logger.debug(new Date().toString()
					+ ": Obtaining demand data from Sterling");
			// System.out.println("Before demand.");
			try {
				ds.getSterlingData(
						InvRepConstants.STG_DEMAND_SELECT(currentStore), true,
						true);

			} catch (SQLException e1) {
				logger.error("Call to Sterling Demand Service failed - "
						+ GregorianCalendar.getInstance().getTime());
				e1.printStackTrace();
			}
			// System.out.println("After demand.");

			logger.debug(new Date().toString()
					+ ": Obtaining OH data from Cassandra");
			// System.out.println("Before cassandra.");
			// System.out.println("cassList: " +
			// AsyncStaticData.cassList.size());
			// Obtain on hand information in Cassandra from itemIdentifiers
			for (String item : AsyncStaticData.cassList) {
				try {
					ds.getCassandraDetails(item);
				} catch (Exception e) {
					logger.error("Call to Cassandra Service failed - "
							+ GregorianCalendar.getInstance().getTime());
					e.printStackTrace();
				}
			}
			// System.out.println("After cassandra.");

			ms.setSkusNotInSterling(new ArrayList<String>(
					AsyncStaticData.skuSet));

			// System.out.println("Before report.");
			// Wait until report is created if first run...
			// long startTime = new Date().getTime();
			// while (AsyncStaticData.latestFile == null
			// || AsyncStaticData.latestFile.isEmpty()) {
			// // Check if desired time has passed
			// if (new Date().getTime() - startTime > (60 * 1000)) {
			// break;
			// }
			// }
			// System.out.println("After report.");

			logger.debug(new Date().toString()
					+ ": Generating Inventory Mismatch report for store "
					+ currentStore);
			// System.out.println("Current Store: " + currentStore);
			// System.out.println("ms: " + ms);
			// System.out.println("Skus: " + AsyncStaticData.skuDetails
			// .values());
			// Write to excel file
			AsyncStaticData.asyncReport.writeDetail(
					currentStore,
					ms,
					new ArrayList<InvDataSet>(AsyncStaticData.skuDetails
							.values()));
			AsyncStaticData.asyncReport.writeSummary(ms);
			AsyncStaticData.asyncReport.writeMissingSKUs(ms);

			// Clear out data for next run
			AsyncStaticData.skuDetails.clear();
			AsyncStaticData.itemList.clear();
			AsyncStaticData.cassList.clear();
			AsyncStaticData.scanCompleted = false;
			AsyncStaticData.currentStore = null;
			// System.out.println("End of Thread");

			// System.out.println(new Date().toString()
			// + ": AsyncStaticData.storeQueue is empty...");
			logger.debug(new Date().toString()
					+ ": Inventory Mismatch analysis complete for store "
					+ currentStore);
		}
	}
}