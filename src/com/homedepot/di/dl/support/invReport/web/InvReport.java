package com.homedepot.di.dl.support.invReport.web;

import java.io.IOException;
import java.net.URLDecoder;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;
//import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.homedepot.di.dl.support.insertHDEventsAndServiceValidation.service.InsertHDEventsAndUpdateHDPSI;
import com.homedepot.di.dl.support.invReport.dao.InvData;
import com.homedepot.di.dl.support.invReport.dao.InvDataSet;
import com.homedepot.di.dl.support.invReport.dao.MismatchSummary;
import com.homedepot.di.dl.support.invReport.dao.ResponseDataSet;
import com.homedepot.di.dl.support.invReport.util.AsyncStaticData;
import com.homedepot.di.dl.support.invReport.util.ExcelWriter;
import com.homedepot.di.dl.support.invReport.util.GenericSelector;
import com.homedepot.di.dl.support.invReport.util.InvRepConstants;
import com.homedepot.di.dl.support.invReport.util.MyLogger;
import com.homedepot.di.dl.support.util.ConstantsQuery;

/**
* Servlet implementation class invReport
*/
// @WebServlet("/inv")
public class InvReport extends HttpServlet {
                private static final long serialVersionUID = 1L;
                final static Logger logger = Logger.getLogger(InvReport.class);
                final static MyLogger mylogger = new MyLogger("/opt/isv/apache-tomcat/logs/InvReport.log");

                /**
                * Default constructor.
                */
                public InvReport() {
                                // TODO Auto-generated constructor stub
                }

                /**
                * @return
                * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
                *      response)
                */
                protected void doGet(HttpServletRequest request,
                                                HttpServletResponse response) throws ServletException, IOException {
                                Date start = new Date();
                                GenericSelector gs = new GenericSelector();
                                ResponseDataSet rsp = new ResponseDataSet();
                                mylogger.deleteFile("/opt/isv/apache-tomcat/logs/InvReport.log");

                                // Get store list from query string
                                String st = URLDecoder.decode(request.getParameter("st"), "UTF-8");
                                InvRepConstants.storeList.addAll(Arrays.asList(st.split(",")));
                                Collections.sort(InvRepConstants.storeList);
                                // System.out.println("Stores: " + InvRepConstants.storeList);
                                rsp.setServiceStarted(true);
                                rsp.setOverallStatus("Running");
                                // gs.sentMessageToClient(rsp);

                                logger.debug(new Date().toString() + ": Request to analyze inventory mismatch for store(s): " + InvRepConstants.storeList);
                                // System.out.println("StoreList: " + Constants.storeList);
                                mylogger.log("Request to analyze inventory mismatch for store(s): " + InvRepConstants.storeList, true, true);
                                mylogger.log("========================",false,true);
                                
                                MismatchSummary ms;
                                ExcelWriter ew = new ExcelWriter(InvRepConstants.filePath, gs, rsp);

                                InvRepConstants.csvCount = 0;
                                for (String store : InvRepConstants.storeList) {
                                                InvRepConstants.CurrentMisMatch = 0;
                                                InvRepConstants.CURRENTSTORE = store;
                                                
                                                logger.debug(new Date().toString()
                                                                                + ": Beginning Inventory Mismatch analysis for store "
                                                                                + store);
                                                
                                                mylogger.log("Beginning Inventory Mismatch analysis for store", true, true);

                                                ms = new MismatchSummary(store);

                                                rsp.setCurrentStore(store);
                                                rsp.createInvData();
                                                InvData current = rsp.getInvData();
                                                current.setStoreNo(store);
                                                current.setStoreStatus("Started");
                                                // gs.sentMessageToClient(rsp);

                                                InvRepConstants.skuSet.clear();

                                                // Read Data from CSV for Production Store when running in Local.
                                                // gs.getCSVStoreData();
                                                // InvRepConstants.csvCount++;

                                                logger.debug(new Date().toString()
                                                                                + ": Obtaining SKU and OH data from Store");
                                                mylogger.log("Obtaining SKU and OH data from Store - Start", true, true);
                                                // Read Data from DB for Production
                                                gs.getStoreSkuData();

                                                current.setStoreSkuCount(InvRepConstants.skuDetails.size());
                                                ms.setTotalReviewed(InvRepConstants.skuDetails.size());
                                                // gs.sentMessageToClient(rsp);

                                                mylogger.log("Obtaining SKU and OH data from Store - End", true, true);
                                                
                                                int isLast = 0;
                                                current.setStoreStatus("Pulling Sterling Item Identifier");
                                                // gs.sentMessageToClient(rsp);
                                                
                                                mylogger.log("Obtaining item identifier data from Sterling - Start", true, true);
                                                logger.debug(new Date().toString()
                                                                                + ": Obtaining item identifier data from Sterling");
                                                
                                                // Obtain sterling itemIdentifier information for each sku
                                                for (String item : InvRepConstants.itemList) {
                                                                try {
                                                                                gs.getSterlingData(ConstantsQuery.STG_ITEMIDENTIFIER_SELECT
                                                                                                                + item + ")", false,
                                                                                                                isLast == InvRepConstants.itemList.size() - 1);
                                                                                isLast++;
                                                                } catch (SQLException e) {
                                                                                logger.error("Call to Sterling ItemIdentifier Service failed - "
                                                                                                                + GregorianCalendar.getInstance().getTime());
                                                                                e.printStackTrace();
                                                                }
                                                }
                                                
                                                mylogger.log("Obtaining item identifier data from Sterling - End", true, true);

                                                // If store contained no new SKUs, we need to trigger that scan is
                                                // completed.
                                                if (InvRepConstants.itemList.isEmpty()) {
                                                                gs.getCurrentData().setSterling("", "", "", true);
                                                }
                                                mylogger.log("Obtaining demand data from Sterling - Start", true, true);
                                                logger.debug(new Date().toString()
                                                                                + ": Obtaining demand data from Sterling");
                                                try {
                                                                current.setStoreStatus("Pulling Sterling Demand");
                                                                // gs.sentMessageToClient(rsp);

                                                                gs.getSterlingData(InvRepConstants
                                                                                                .STG_DEMAND_SELECT(InvRepConstants.CURRENTSTORE), true,
                                                                                                true);

                                                } catch (SQLException e1) {
                                                                logger.error("Call to Sterling Demand Service failed - "
                                                                                                + GregorianCalendar.getInstance().getTime());
                                                                e1.printStackTrace();
                                                }
                                                mylogger.log("Obtaining demand data from Sterling - End", true, true);

                                                mylogger.log("Obtaining OH data from Cassandra - Start", true, true);
                                                logger.debug(new Date().toString()
                                                                                + ": Obtaining OH data from Cassandra");
                                                // Obtain on hand information in Cassandra from itemIdentifiers
                                                for (String item : InvRepConstants.cassList) {
                                                                try {
                                                                                current.setStoreStatus("Comparing with Cassandra");
                                                                                // gs.sentMessageToClient(rsp);
                                                                                gs.getCassandraDetails(item);
                                                                } catch (Exception e) {
                                                                                logger.error("Call to Cassandra Service failed - "
                                                                                                                + GregorianCalendar.getInstance().getTime());
                                                                                mylogger.log("Error: Call to Cassandra Service Failed", true, true);
                                                                                e.printStackTrace();
                                                                }
                                                }
                                                
                                                mylogger.log("Obtaining OH data from Cassandra - End", true, true);

                                                // current.setStoreStatus("Generating CSV file");
                                                // current.setDataURL(Url);

                                                mylogger.log("Generating Reporting Statistics - set store status", true, true);
                                                
                                                ms.setSkusNotInSterling(new ArrayList<String>(
                                                                                InvRepConstants.skuSet));

                                                current.setStoreStatus("Generating Reporting Statistics");
                                                // gs.sentMessageToClient(rsp);

                                                logger.debug(new Date().toString()
                                                                                + ": Generating Inventory Mismatch report for store "
                                                                                + store);
                                                
                                                mylogger.log("Start of generating Inventory Mismatch report for store: " + store, true, true);
                                                // Write to excel file
                                                ew.writeDetail(
                                                                                InvRepConstants.CURRENTSTORE,
                                                                                ms,
                                                                                new ArrayList<InvDataSet>(InvRepConstants.skuDetails
                                                                                                                .values()));
                                                ew.writeSummary(ms);
                                                ew.writeMissingSKUs(ms);

                                                // Set current store's mismatches
                                                current.setMismatchCount(InvRepConstants.CurrentMisMatch);
                                                current.setStoreStatus("Completed");
                                                // gs.sentMessageToClient(rsp);

                                                // Clear out data for next run
                                                InvRepConstants.skuDetails.clear();
                                                InvRepConstants.itemList.clear();
                                                InvRepConstants.cassList.clear();
                                                InvRepConstants.scanCompleted = false;
                                                
                                                logger.debug(new Date().toString()
                                                                                + ": Inventory Mismatch analysis complete for store "
                                                                                + store);
                                                
                                                mylogger.log("End of Inventory Mismatch analysis complete for store " + store, true, true);
                                }
                                // gs.sentMessageToClient(rsp);

                                // Complete the generation of the report and save it to the server
                                ew.writeNegativeStoreOH();
                                ew.writeSummaryTotal(InvRepConstants.storeList.size());
                                ew.saveExcel();
                                
                                mylogger.log("End of generating Inventory Mismatch report for stores", true, true);

                                // Clear Data for every Service Call
                                InvRepConstants.itemList.clear();
                                InvRepConstants.storeList.clear();
                                InvRepConstants.cassendraDetails.clear();
                                rsp.setServiceStarted(false);
                                rsp.setOverallStatus("Completed");
                                // gs.sentMessageToClient(rsp);
                                // Gson json = new Gson();
                                // response.setContentType("application/json");
                                // response.setCharacterEncoding("UTF-8");
                                //
                                // ArrayList<InvDataSet> reducedSkuDetails =new ArrayList<InvDataSet>();
                                // Constants.skuDetails.forEach((k,v) ->
                                // {
                                // if(v.getdifference() != 0)
                                // reducedSkuDetails.add(v);
                                // });
                                //
                                //
                                // response.getWriter().write(json.toJson(reducedSkuDetails));

                                InvRepConstants.skuCache.clear();
                                InvRepConstants.skuDetails.clear();
                                
                                logger.debug(new Date().toString()
                                                                + ": Inventory Mismatch analysis complete for all stores.");
                                
                                mylogger.log("Inventory Mismatch analysis complete for all stores.", true, true);
                }

                /**
                * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
                *      response)
                */
                protected void doPost(HttpServletRequest request,
                                                HttpServletResponse response) throws ServletException, IOException {
                                // TODO Auto-generated method stub
                }
}
