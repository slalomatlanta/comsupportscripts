package com.homedepot.di.dl.support.invReport.web;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.poi.util.IOUtils;

import com.homedepot.di.dl.support.invReport.util.AsyncStaticData;
import com.homedepot.di.dl.support.invReport.util.DBSelector;

/**
 * This servlet will return the last generated Inventory Mismatch Report in
 * excel format.
 * 
 * @author JXA8571
 *
 */
// @WebServlet("/excelDownloadAsync")
public class ExcelDownloadAsync extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = Logger.getLogger(ExcelDownloadAsync.class);
	
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		if (AsyncStaticData.storeQueue.peek() == null
				&& AsyncStaticData.executor.getActiveCount() == 0) {
			// Complete the generation of the report and save it to the server
			AsyncStaticData.asyncReport.writeNegativeStoreOH();
			AsyncStaticData.asyncReport
					.writeSummaryTotal(AsyncStaticData.storeList.size());
			AsyncStaticData.asyncReport.saveExcelAsync();

			FileInputStream in = new FileInputStream(AsyncStaticData.latestFile);

			response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Disposition",
					"attachment; filename=MismatchReport.xlsx");

			ServletOutputStream out = response.getOutputStream();
			IOUtils.copy(in, out);
			out.flush();
			in.close();
			out.close();
			
			logger.debug(new Date().toString() + ": Inventory Mismatch Report generated @ " + AsyncStaticData.latestFile);
			
			AsyncStaticData.latestFile = "";
			AsyncStaticData.storeList = new ArrayList<String>();
			AsyncStaticData.storeQueue = new LinkedList<String>();
			AsyncStaticData.skuCache.clear();
			AsyncStaticData.skuDetails.clear();
		} else {
			// response.setHeader("Content-Type", "text/plain");
			response.setContentType("text/plain");
			PrintWriter writer = response.getWriter();
			writer.write("Report is still processing... Please try again in a few minutes.");
		}
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}
}