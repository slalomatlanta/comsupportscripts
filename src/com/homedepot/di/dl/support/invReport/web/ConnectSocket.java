//package com.homedepot.di.dl.support.invReport.web;
//
//import java.io.IOException;
//import java.sql.SQLException;
//import java.util.Collections;
//import java.util.GregorianCalendar;
//import java.util.HashSet;
//import java.util.Set;
//
//import javax.websocket.OnClose;
//import javax.websocket.OnMessage;
//import javax.websocket.OnOpen;
//import javax.websocket.Session;
//import javax.websocket.server.ServerEndpoint;
//
//import com.homedepot.di.dl.support.invReport.util.Constants;
//import com.homedepot.di.dl.support.invReport.util.GenericSelector;
//
///**
// * Servlet implementation class ConnectSocket
// */
//@ServerEndpoint("/invendpoint")
//public class ConnectSocket {
//	private static final long serialVersionUID = 1L;
//	int storeList = 0;
//
//	/**
//	 * @see HttpServlet#HttpServlet()
//	 */
//	public ConnectSocket() {
//		super();
//		// TODO Auto-generated constructor stub
//	}
//
//	private static final Set<Session> peers = Collections
//			.synchronizedSet(new HashSet<Session>());
//
//	@OnMessage
//	public String onMessage(String message) {
//		if (message != null) {
//			if (message.equals("started") & Constants.storeList.size() > 0) {
//				return "Already a mismatch report is being run. Please check after sometime";
//			}
//		}
//		return "";
//	}
//
//	@OnOpen
//	public void onOpen(Session peer) {
//		peers.add(peer);
//	}
//
//	@OnClose
//	public void onClose(Session peer) {
//		peers.remove(peer);
//	}
//
//	public static void broadcastMessage(String message) {
//		for (Session s : peers) {
//			if (s.isOpen()) {
//				try {
//					s.getBasicRemote().sendText(message);
//				} catch (IOException e) {
//					e.printStackTrace();
//				}
//			}
//		}
//	}
//}