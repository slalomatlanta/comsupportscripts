package com.homedepot.di.dl.support.invReport.web;

import java.io.IOException;
import java.net.URLDecoder;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import javax.servlet.ServletException;
//import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.homedepot.di.dl.support.invReport.dao.InvData;
import com.homedepot.di.dl.support.invReport.dao.InvDataSet;
import com.homedepot.di.dl.support.invReport.dao.MismatchSummary;
import com.homedepot.di.dl.support.invReport.dao.ResponseDataSet;
import com.homedepot.di.dl.support.invReport.util.AsyncStaticData;
import com.homedepot.di.dl.support.invReport.util.DBSelector;
import com.homedepot.di.dl.support.invReport.util.InvRepConstants;
import com.homedepot.di.dl.support.invReport.util.ExcelWriter;
import com.homedepot.di.dl.support.invReport.util.GenericSelector;

/**
 * Servlet implementation class InvReportAsync
 */
// @WebServlet("/invAsync")
public class InvReportAsync extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = Logger.getLogger(InvReportAsync.class);

	/**
	 * Default constructor.
	 */
	public InvReportAsync() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// Get store(s) from query string and add it to the processing queue
		String store = URLDecoder.decode(request.getParameter("st"), "UTF-8");

		logger.debug(new Date().toString() + ": Request to analyze inventory mismatch for store " + store);

		// System.out.println("Start");

		// Block duplicate
		if (AsyncStaticData.storeQueue.contains(store)
				|| AsyncStaticData.storeList.contains(store)) {
			return;
		}

		AsyncStaticData.storeQueue.offer(store);

		// Process the report request
		AsyncStaticData.processAsyncRequest();

		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		while (AsyncStaticData.storeQueue.contains(store)
				|| AsyncStaticData.currentStore == store) {
			// Wait for store to complete.
		}

		// System.out.println("Finished " + store);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}
}