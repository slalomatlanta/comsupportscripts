package com.homedepot.di.dl.support.invReport.web;

import java.io.FileInputStream;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
//import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.util.IOUtils;

import com.homedepot.di.dl.support.invReport.util.InvRepConstants;

/**
 * This servlet will return the last generated Inventory Mismatch Report in
 * excel format.
 * 
 * @author JXA8571
 *
 */
//@WebServlet("/excelDownload")
public class ExcelDownload extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		FileInputStream in = new FileInputStream(InvRepConstants.latestFile);

		response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		// response.setHeader("Expires", "0");
		// response.setHeader("Cache-Control",
		// "must-revalidate, post-check=0, pre-check=0");
		// response.setHeader("Pragma", "public");
		response.setHeader("Content-Disposition",
				"attachment; filename=MismatchReport.xlsx");

		ServletOutputStream out = response.getOutputStream();
		IOUtils.copy(in, out);
		out.flush();
		in.close();
		out.close();
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}
}
