package com.homedepot.di.dl.support.getStoresStatus.web;

import java.io.IOException;
import java.util.GregorianCalendar;

import javax.servlet.ServletException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.homedepot.di.dl.support.getStoresStatus.dao.ExecuteThread;
import com.homedepot.di.dl.support.getStoresStatus.dto.StoreStatusDTO;

/**
 * Servlet implementation class StoreStatusServlet
 */

public class StoreStatusServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	final static Logger logger = Logger.getLogger(StoreStatusServlet.class);
	private static final Logger oLogger = Logger.getLogger("splunkLog");
    /**
     * Default constructor. 
     */
    public StoreStatusServlet() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) {
		// TODO Auto-generated method stub
		logger.info("StoreStatusServlet Started : "+ GregorianCalendar.getInstance().getTime());
		
		ExecuteThread thread = new ExecuteThread(); 
		StoreStatusDTO dto = new StoreStatusDTO();
		String json="";
		try {
			dto = thread.getStorestatus();
			
			Gson gson = new Gson();
			json = gson.toJson(dto);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.debug("StoreStatusServlet Exception : "+ GregorianCalendar.getInstance().getTime()+" : "+e.getMessage());
			oLogger.debug("StoreStatusServlet Exception : "+ GregorianCalendar.getInstance().getTime()+" : "+e.getMessage());
			e.printStackTrace();
		}
		
		logger.info(json);
		oLogger.info(json);
		logger.info("StoreStatusServlet Completed : "+ GregorianCalendar.getInstance().getTime());
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
