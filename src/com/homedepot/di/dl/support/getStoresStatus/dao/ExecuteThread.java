package com.homedepot.di.dl.support.getStoresStatus.dao;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeMessage.RecipientType;

import org.apache.log4j.Logger;

import com.homedepot.di.dl.support.getStoresStatus.dto.StoreStatusDTO;





public class ExecuteThread 
{
	
	final static Logger logger = Logger.getLogger(ExecuteThread.class);
	private static final Logger oLogger = Logger.getLogger("splunkLog");
	public static StoreStatusDTO getStorestatus()  
	
	{
		
		StoreStatusDTO dto = new StoreStatusDTO();
		int NTHREADS = 10;
		//String outputFileLocation = "C:\\Users\\mxg8654\\Desktop\\Unused\\StoresInSuspendStatus.csv";
		String outputFileLocation = "/opt/isv/apache-tomcat/temp/StoresInSuspendStatus.csv";
		File f = new File(outputFileLocation);
		if (f.exists()) {
			f.delete();
			logger.info(f + " File Deleted");
		}
		ArrayList<String> storeList = new ArrayList<String>();
		logger.info("Getting Store List......");
		final String driverClass1 = "oracle.jdbc.driver.OracleDriver";
		final String connectionURL = "jdbc:oracle:thin:@spragor10-scan.homedepot.com:1521/dpr78mm_sro01";
		final String uName = "MMUSR01";
		final String uPassword = "COMS_MMUSR01";
		

		Connection con1 = null;
		
		try{
			
		Class.forName(driverClass1);
		con1 = DriverManager.getConnection(connectionURL, uName, uPassword);

		Statement stmt1 = null;
		stmt1 = con1.createStatement();
		ResultSet rs = stmt1
				.executeQuery("select loc_nbr from THD01.comt_loc_capbl where comt_capbl_id=7 and loc_nbr not in ('3706') order by loc_nbr");
		while (rs.next()) {

			storeList.add(rs.getString(1));

		}
		rs.close();
		stmt1.close();
		con1.close();
		
		logger.info("Executing in threads......");
		ExecutorService executor = Executors.newFixedThreadPool(NTHREADS);
		
		List<Future<String>> list = new ArrayList<Future<String>>();
		
		for (String store : storeList) {
			try{	
				Callable<String> worker = new StoreStatus(store);

				Future<String> submit = executor.submit(worker);
				list.add(submit);
			} catch (Exception e) {
				e.printStackTrace();
			}
				
			}
		
		for (Future<String> future : list) {
			try {

				String futuredto = future.get();
				
				

			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (ExecutionException e) {
				e.printStackTrace();
			}
		}
		
		
		logger.info("Execution Complete.. Mailing details ,if any");
		
		ArrayList<String> dbConnect = new ArrayList<String>();
		ArrayList<String> normalState = new ArrayList<String>();
		BufferedReader br = new BufferedReader(new FileReader(outputFileLocation));
		String line = "";
		String finalList = "";
	    int count = 0;
	    String store;
	    String storeUnableToConnectDB = "";
		String storeNotInNormal = "";
	    while ((line = br.readLine()) != null) 
	    {
	    	if (!(",,".equalsIgnoreCase(line))) {
                int noOfRowsUpdated = 0;
                String tokens[] = line.split(",");
                finalList = finalList +"<br/>"+ tokens[0].trim();
                
                store = tokens[0].trim();
                
                if(store.contains("Error Connecting DB"))
                {
                	storeUnableToConnectDB = storeUnableToConnectDB + store + ",";
                	dbConnect.add(store);
                }
                else
                {
                	storeNotInNormal = storeNotInNormal + store + ",";
                	normalState.add(store);
                }
	    	}
	    	count++;
	    }
	    
	    	    
	    DateFormat dFormat = new SimpleDateFormat("yyyy-MM-dd-HH:mm:ss");
		Date date = new Date();
		String currentTime = dFormat.format(date);
	    dto.setProgram("Store Transmission Status");
	    dto.setDb("STORE");
	    dto.setLastUpdTs(currentTime);		
	    
	    if(count > 0)
	    {
		
		
		if(normalState.size() > 0 && normalState.size() <=50 && dbConnect.size() > 0 && dbConnect.size() <= 50)
		{
			storeUnableToConnectDB = storeUnableToConnectDB.substring(0,storeUnableToConnectDB.length()-1);
			storeNotInNormal = storeNotInNormal.substring(0,storeNotInNormal.length()-1);
		    storeUnableToConnectDB = storeUnableToConnectDB.replace("-Error Connecting DB", "");
		    dto.setStoreUnableToConnectToDB(storeUnableToConnectDB);
		    dto.setStoreNotInNormal(storeNotInNormal);

		}
		
		else if (normalState.size() > 50 && dbConnect.size() > 50)
		{
			storeUnableToConnectDB = "More than 50 stores have DB connectivity issue : " + dbConnect.size();
			storeNotInNormal = "More than 50 stores are not in NORMAL state : " + normalState.size();
			dto.setStoreUnableToConnectToDB(storeUnableToConnectDB);
		    dto.setStoreNotInNormal(storeNotInNormal);
		}
		
		else if (normalState.size() > 50 && dbConnect.size() <= 0)
		{
			storeUnableToConnectDB = "DB Connectivity Check passed on all stores";
			storeNotInNormal = "More than 50 stores are not in NORMAL state : " + normalState.size();
			dto.setStoreUnableToConnectToDB(storeUnableToConnectDB);
		    dto.setStoreNotInNormal(storeNotInNormal);
		}
		
		else if (normalState.size() <= 0 && dbConnect.size() > 50)
		{
			storeUnableToConnectDB = "More than 50 stores have DB connectivity issue : " + dbConnect.size();
			storeNotInNormal = "All Stores in NORMAL State";
			dto.setStoreUnableToConnectToDB(storeUnableToConnectDB);
		    dto.setStoreNotInNormal(storeNotInNormal);
		}
		
		else if (normalState.size() > 0 && normalState.size() <=50 && dbConnect.size() <= 0)
		{
			storeNotInNormal = storeNotInNormal.substring(0,storeNotInNormal.length()-1);
		    dto.setStoreUnableToConnectToDB("DB Connectivity Check passed on all stores");
		    dto.setStoreNotInNormal(storeNotInNormal);
		}
		
		else if (normalState.size() <= 0 && dbConnect.size() > 0 && dbConnect.size() <= 50)
		{
			storeUnableToConnectDB = storeUnableToConnectDB.substring(0,storeUnableToConnectDB.length()-1);
			storeUnableToConnectDB = storeUnableToConnectDB.replace("-Error Connecting DB", "");
		    dto.setStoreUnableToConnectToDB(storeUnableToConnectDB);
		    dto.setStoreNotInNormal("All Stores in NORMAL State");
			
		}
		
		sendAttachment(finalList);
		
	    }
	    
	    
	    else
	    {
		    dto.setStoreUnableToConnectToDB("DB Connectivity Check passed on all stores");
		    dto.setStoreNotInNormal("All Stores in NORMAL State");
	    }
	    
	    executor.shutdown();
		}
		
		catch(Exception e)
		{
			e.printStackTrace();
		}
	    return dto;
		
	 }
	
	
	public static void sendAttachment(String storeList) {
		String host = "mail1.homedepot.com";
		String from = "horizon@cpliisad.homedepot.com";
		//String[] to = { "mohanraj_gurusamy@homedepot.com","steven_l_kaneti@homedepot.com" };
		String[] to ={"_2fc77b@homedepot.com"};
		//String[] Cc ={"RAKESH_J_SATYA@homedepot.com"};

		// Get system properties
		Properties properties = System.getProperties();

		// Setup mail server
		properties.setProperty("mail.smtp.host", host);

		// Get the default Session object.
		Session session = Session.getInstance(properties);

		// Define message
		Message message = new MimeMessage(session);
		try {
			message.setFrom(new InternetAddress(from));
		
		InternetAddress[] addressTo = new InternetAddress[to.length];
		 //InternetAddress[] addressCc = new InternetAddress[Cc.length];
		for (int i = 0; i < to.length; i++) {
			addressTo[i] = new InternetAddress(to[i]);
			
		}
		 /*for (int j = 0; j < Cc.length; j++)
		 { addressCc[j] = new InternetAddress(Cc[j]);
		// System.out.println("Cc Address "+Cc[j]);
		 }*/
		message.setRecipients(RecipientType.TO, addressTo);
		//message.setRecipients(RecipientType.CC, addressCc);
		message.setSubject("Stores not in NORMAL status");

		// Create the message part
		BodyPart messageBodyPart = new MimeBodyPart();
		String msgContent = "Hi All,<br/><br/>Below stores are not in NORMAL transmission state <br/>"+storeList+"<br/><br/>Thanks<br/>";
		msgContent = msgContent + "COM Multichannel Support";
		// Fill the message
		messageBodyPart.setContent(msgContent, "text/html");

		Multipart multipart = new MimeMultipart();
		multipart.addBodyPart(messageBodyPart);

		// Part two is attachment
		//messageBodyPart = new MimeBodyPart();
		//String filename = "C:\\store\\Response.csv";
		//String filename = "/opt/isv/tomcat-6.0.18/temp/ResponseLock.csv";
		/*DataSource source = new FileDataSource(outputFileLocation);
		messageBodyPart.setDataHandler(new DataHandler(source));
		messageBodyPart.setFileName(source.getName());*/
		//multipart.addBodyPart(messageBodyPart);

		// Put parts in message
		message.setContent(multipart);

		// Send the message
		Transport.send(message);
		//System.out.println("Msg Send ....");
		
		} catch (AddressException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	public void writeToCSV(String fileLocation,
			ArrayList<String> outputList) {
		boolean writeHeader = false;
		FileWriter writer;
		try {
			File f = new File(fileLocation);
			if (f.exists()) {
				writeHeader = true;
			}
			writer = new FileWriter(fileLocation, true);
			
			for (String store  : outputList) {
				

				writer.append(store);
				writer.append("\n");
			}

			writer.flush();
			writer.close();
			

		} catch (IOException e) {
			// TODO Auto-generated catch block
			
		}
	}

}
