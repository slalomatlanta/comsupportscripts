package com.homedepot.di.dl.support.getStoresStatus.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.Callable;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathFactory;

import org.apache.log4j.Logger;
import com.homedepot.di.dl.support.util.DBConnectionUtil;




public class StoreStatus implements Callable<String> {
	
	final static Logger logger = Logger.getLogger(StoreStatus.class);	
	
	public String str;
	String storesInsuspendStatus = null;
	public String strStatus;
	ArrayList<String> outputList = new ArrayList<String>();
	
	public StoreStatus(String store) {
		this.str = store;
	}
	
	@Override
	public String call() throws Exception {
		//System.out.println("running1");
		// TODO Auto-generated method stub
		//System.out.println("run :" + threadName);
		
		/*ArrayList<String> storeList = new ArrayList<String>();
		
		storeList.add("0121");
		storeList.add("6172");
		storeList.add("6202");
		storeList.add("6175");
		
		for (String store : storeList)
		{*/
		try 
		{
			String status = null;
			
	        Connection con = null;
	        ResultSet result = null;
	        Statement stmt = null;
			try
			{
				DBConnectionUtil dbConn = new DBConnectionUtil();
				con = dbConn.getStoreconnection(str);
	            //System.out.println(ContantsQuery.transmissionState);
	            stmt = con.createStatement();
	            result = stmt.executeQuery(ContantsQuery.transmissionState);
	            
	            while(result.next())
	            {
	            	status=result.getString(6).trim();
	            	
	            	if (!("NORMAL".equalsIgnoreCase(status)))
	            	{
	            		logger.info(str+" - "+status);
	            		outputList.add(str+"-"+status);
	            	}
	            }
	            
			}
			
			
			catch(Exception e)
			{
				logger.debug(str+" - "+"Error Connecting DB");
				//e.printStackTrace();
				outputList.add(str+"-"+"Error Connecting DB");
				
			}
			
			
		
		}
		
		
	
	catch (Exception e) 
	{
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
		
		finally
		{
			
			//String outputFileLocation = "C:\\Users\\mxg8654\\Desktop\\Unused\\StoresInSuspendStatus.csv";
			String outputFileLocation = "/opt/isv/apache-tomcat/temp/StoresInSuspendStatus.csv";
			ExecuteThread dao = new ExecuteThread();
			
			dao.writeToCSV(outputFileLocation, outputList);
		}
		return storesInsuspendStatus;
		
		
		//}
		
	}


	
}
