package com.homedepot.di.dl.support.getStoresStatus.dto;

public class StoreStatusDTO 
{
	public String program;
	public String db;
	public String lastUpdTs;
	public String getLastUpdTs() {
		return lastUpdTs;
	}
	public void setLastUpdTs(String lastUpdTs) {
		this.lastUpdTs = lastUpdTs;
	}
	public String getDb() {
		return db;
	}
	public void setDb(String db) {
		this.db = db;
	}
	public String storeNotInNormal;
	public String storeUnableToConnectToDB;
	
	public String getProgram() {
		return program;
	}
	public void setProgram(String program) {
		this.program = program;
	}
	
	public String getStoreNotInNormal() {
		return storeNotInNormal;
	}
	public void setStoreNotInNormal(String storeNotInNormal) {
		this.storeNotInNormal = storeNotInNormal;
	}
	public String getStoreUnableToConnectToDB() {
		return storeUnableToConnectToDB;
	}
	public void setStoreUnableToConnectToDB(String storeUnableToConnectToDB) {
		this.storeUnableToConnectToDB = storeUnableToConnectToDB;
	}

}
