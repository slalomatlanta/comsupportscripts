package com.homedepot.di.dl.support.SkuReport;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import org.apache.log4j.Logger;

import com.homedepot.di.dl.support.util.DBConnectionUtil;

public class Store {
	final static Logger logger = Logger.getLogger(Store.class);
	public Store() {

	}
	
	public HashMap getStoreOH(String store, String sku, HashMap<Integer, Integer> storemap) {
	 // Import properties file
		

	 
	
	Properties prop = new Properties();
	HashMap<String, Integer> StoreOH = new HashMap<String, Integer>();
	 InputStream input = null;
	 int SoH = 0;
	 int count=0;
	 input = getClass().getClassLoader().getResourceAsStream(
	 "/properties/OHconfig.properties");
	 try {
	 prop.load(input);
	 } catch (IOException e1) {
	
	 e1.printStackTrace();
	 }
	 
	Iterator it = storemap.entrySet().iterator();
	while (it.hasNext()) {

		Map.Entry pair = (Map.Entry) it.next();
		String value = pair.getValue().toString();
//		System.out.println(value);
		if (value.length() == 3) {
			value = "0" + value;

		}
	
	count++;
	 // Connection to Informix Database
	
	 try {
	 	
	logger.info("jdbc:informix-sqli://ispb.st"+value+".homedepot.com:25001/store:INFORMIXSERVER=onconfig_tcp");
	logger.info("Sku Inventory program. Store Number= "+ value+ "store count= "+count);
	
	DBConnectionUtil dbConn = new DBConnectionUtil();
	Connection sterCon = dbConn.getStoreconnection(value);
	 
	 Statement st = null;
	 st = sterCon.createStatement();
	 ResultSet result = null;
	
	 String Query = "select oh_qty" + " from sku where sku_nbr='" + sku + "'";
	
	 result = st.executeQuery(Query);
	
	 //Get the store OH from the Resulset
	 while (result.next()) {
	
	 SoH = result.getInt("oh_qty");
	 // System.out.println("Store OH " + SoH);
	
	 }
	 StoreOH.put(value, SoH);
	 logger.info("Sku Inventory program. Made it through db call. Store Number= "+ value+ "store count= "+count+ "StoreOH= "+SoH);
	
	 }
	
	 
	 
	 catch (SQLException e) {
	logger.error("Sku Inventory Error SQL Exception");
	 e.printStackTrace();
	 }
	
	 }
	return StoreOH;

	}

	public HashMap getStores() {

		// Load Property file
		Properties prop = new Properties();
		InputStream input = null;
		HashMap<String, Integer> hmap = new HashMap<String, Integer>();
		input = getClass().getClassLoader().getResourceAsStream(
				"/properties/OHconfig.properties");

		try {
			prop.load(input);
		} catch (IOException e1) {

			e1.printStackTrace();
		}

		// Connect to Com Tier Database to retrieve store numbers
		try {
			Class.forName(prop.getProperty("ComDriver"));

			Connection sterCon = DriverManager.getConnection(
					prop.getProperty("ComConnection"),
					prop.getProperty("ComUserID"), prop.getProperty("ComPass"));

			Statement st = null;
			st = sterCon.createStatement();
			ResultSet result = null;

			// Sql Query
			String Query = "select distinct loc_nbr"
					+ " from thd01.comt_loc_capbl" + " where COMT_CAPBL_ID = 7";

			result = st.executeQuery(Query);

			// place the stores into a Hashmap
			while (result.next()) {

				int Sn = result.getInt("loc_nbr");
				String StoreNum = Integer.toString(Sn);
				hmap.put(StoreNum, Sn);

			}

		}

		// Catch Exceptions

		catch (ClassNotFoundException e) {

			e.printStackTrace();
		}

		catch (SQLException e) {

			e.printStackTrace();
		}
		return hmap;

	}

	public String Start()
	{
		DateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");
		Date date = new Date();
		String StartTime=dateFormat.format(date);
		logger.info(StartTime);
		return StartTime;
		
		
	}
	
	public static String removeCharAt(String s, int pos) {
		return s.substring(0, pos) + s.substring(pos + 1);
	}

}
