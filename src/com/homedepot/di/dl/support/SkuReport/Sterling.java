package com.homedepot.di.dl.support.SkuReport;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.sql.Array;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Properties;
import java.util.Random;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class Sterling

{

	public Sterling() {

	}

	public HashMap getSterlingOH(String sku) {

		Properties prop = new Properties();
		InputStream input = null;
		HashMap<String, Integer> SterlOHmap = new HashMap<String, Integer>();

		input = getClass().getClassLoader().getResourceAsStream(
				"/properties/OHconfig.properties");
		try {
			prop.load(input);
		} catch (IOException e1) {

			e1.printStackTrace();
		}
		int OH = 0;
		String storeNum;

		try {
			Class.forName(prop.getProperty("SterlingDriver"));

			Connection sterCon = DriverManager.getConnection(
					prop.getProperty("SterlingConnection"),
					prop.getProperty("SterlingID"),
					prop.getProperty("SterlingPass"));

			Statement st = null;
			st = sterCon.createStatement();
			ResultSet result = null;

			// Sql Query
			String Query = "select quantity, THD01.yfs_inventory_supply.SHIPNODE_KEY "
					+ " from THD01.yfs_item_alias, THD01.yfs_item,THD01.yfs_inventory_item , THD01.yfs_inventory_supply "
					+ " where THD01.yfs_item_alias.ITEM_KEY=THD01.yfs_item.ITEM_KEY"
					+ " and THD01.yfs_item.ITEM_ID=THD01.yfs_inventory_item.ITEM_ID"
					+ " and THD01.yfs_inventory_item.INVENTORY_ITEM_KEY=THD01.yfs_inventory_supply.INVENTORY_ITEM_KEY"
//					+ " and THD01.yfs_inventory_demand.SHIPNODE_KEY='"+store+"'"
					+ " and THD01.yfs_item_alias.ALIAS_VALUE='" + sku + "'";

			result = st.executeQuery(Query);

			// place the stores into a Hashmap
			while (result.next()) {

				OH = result.getInt("quantity");
				storeNum = result.getString("SHIPNODE_KEY").replaceAll("\\s",
						"");


					SterlOHmap.put(storeNum, OH);
				}


		}

		catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (NullPointerException e) {

		}

		return SterlOHmap;

	}
		
		

	

	public HashMap getDemand(String store, String sku) {

		Properties prop = new Properties();
		InputStream input = null;
		HashMap<String, Integer> Demandmap = new HashMap<String, Integer>();

		input = getClass().getClassLoader().getResourceAsStream(
				"/properties/OHconfig.properties");
		try {
			prop.load(input);
		} catch (IOException e1) {

			e1.printStackTrace();
		}
		int demand = 0;
		String storeNum;

		try {
			Class.forName(prop.getProperty("SterlingDriver"));

			Connection sterCon = DriverManager.getConnection(
					prop.getProperty("SterlingConnection"),
					prop.getProperty("SterlingID"),
					prop.getProperty("SterlingPass"));

			Statement st = null;
			st = sterCon.createStatement();
			ResultSet result = null;

			// Sql Query
			String Query = "select quantity, THD01.yfs_inventory_demand.SHIPNODE_KEY "
					+ " from THD01.yfs_item_alias, THD01.yfs_item,THD01.yfs_inventory_item , THD01.yfs_inventory_demand "
					+ " where THD01.yfs_item_alias.ITEM_KEY=THD01.yfs_item.ITEM_KEY"
					+ " and THD01.yfs_item.ITEM_ID=THD01.yfs_inventory_item.ITEM_ID"
					+ " and THD01.yfs_inventory_item.INVENTORY_ITEM_KEY=THD01.yfs_inventory_demand.INVENTORY_ITEM_KEY"
					// +
					// " and THD01.yfs_inventory_demand.SHIPNODE_KEY='"+store+"'"
					+ " and THD01.yfs_item_alias.ALIAS_VALUE='" + sku + "'";

			result = st.executeQuery(Query);

			// place the stores into a Hashmap
			while (result.next()) {

				demand = result.getInt("quantity");
				storeNum = result.getString("SHIPNODE_KEY").replaceAll("\\s",
						"");

				if (Demandmap.containsKey(storeNum)) {

//					System.out.println("Dupe");
//					System.out.println(Demandmap.get(storeNum));

					int CurrentVal = Demandmap.get(storeNum);
					int NewVal = demand + CurrentVal;
//					System.out.println("This is the new Value" + NewVal);
					Demandmap.put(storeNum, NewVal);

				} else {

					Demandmap.put(storeNum, demand);
				}

				
			}

		}

		catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (NullPointerException e) {

		}

		return Demandmap;

	}

public HashMap getInflight(String sku)	
{
	
	Properties prop = new Properties();
	InputStream input = null;
	HashMap<String, Integer> InFlight = new HashMap<String, Integer>();

	input = getClass().getClassLoader().getResourceAsStream(
			"/properties/OHconfig.properties");
	try {
		prop.load(input);
	} catch (IOException e1) {

		e1.printStackTrace();
	}
	int flight = 0;
	String storeNum;

	try {
		Class.forName(prop.getProperty("SterlingDriver"));

		Connection sterCon = DriverManager.getConnection(
				prop.getProperty("SterlingConnection"),
				prop.getProperty("SterlingID"),
				prop.getProperty("SterlingPass"));

		Statement st = null;
		st = sterCon.createStatement();
		ResultSet result = null;

		// Sql Query
		String Query = "select THD01.yfs_inventory_audit.quantity, THD01.yfs_inventory_audit.SHIP_NODE "
				+ " from THD01.yfs_item_alias, THD01.yfs_item,THD01.yfs_inventory_item , THD01.yfs_inventory_audit "
				+ " where THD01.yfs_item_alias.ITEM_KEY=THD01.yfs_item.ITEM_KEY"
				+ " and THD01.yfs_item.ITEM_ID=THD01.yfs_inventory_item.ITEM_ID"
				+ " and THD01.yfs_inventory_item.INVENTORY_ITEM_KEY=THD01.yfs_inventory_audit.INVENTORY_ITEM_KEY"
				+ " and THD01.yfs_inventory_audit.modifyts>=(sysdate - 1/24)"
				+ " and THD01.yfs_item_alias.ALIAS_VALUE='" + sku + "'";

		result = st.executeQuery(Query);

		// place the stores into a Hashmap
		while (result.next()) {

			flight = result.getInt("quantity");
			storeNum = result.getString("SHIP_NODE").replaceAll("\\s","");

			if (InFlight.containsKey(storeNum)) {

//				System.out.println("Dupe");
//				System.out.println(Demandmap.get(storeNum));

				int CurrentVal = InFlight.get(storeNum);
				int NewVal = flight + CurrentVal;
//				System.out.println("This is the new Value" + NewVal);
				InFlight.put(storeNum, NewVal);

			} else {

				InFlight.put(storeNum, flight);
			}
			

			
		}

	}

	catch (ClassNotFoundException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	catch (NullPointerException e) {

	}

	return InFlight;

}

	
}




