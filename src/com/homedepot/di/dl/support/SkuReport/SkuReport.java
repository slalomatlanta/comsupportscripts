package com.homedepot.di.dl.support.SkuReport;

import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.homedepot.di.dl.support.SkuReport.web.SkuInventoryServlet;

import org.apache.log4j.Logger;

public class SkuReport {
	final static Logger logger = Logger.getLogger(SkuReport.class);
	public static String Execute(String sku) {

		HashMap<Integer, Integer> Storemap = new HashMap<Integer, Integer>();
		HashMap<String, Integer> demand = new HashMap<String, Integer>();
		HashMap<String, Integer> sohs = new HashMap<String, Integer>();
		HashMap<String, Integer> InFlight = new HashMap<String, Integer>();
		HashMap<String, Integer> SterlOH = new HashMap<String, Integer>();
		HashMap<String, String> store = new HashMap<String, String>();
		BufferedReader br = null;

		// Returns all the stores into a Hashmap
		Store Getstores = new Store();
		Storemap = Getstores.getStores();

		Sterling oh = new Sterling();
		Store Soh = new Store();
		BusinessLayer bl = new BusinessLayer();
		boolean alreadyExecuted = false;
		boolean SohalreadyExecuted = false;
		boolean IFalreadyExecuted = false;
		boolean SterlOHExecuted = false;
		Integer Dem = null;
		Integer Flight = null;
		Integer SOHs = null;
		Integer Sterling = null;
		String SOH = null;
		String In_Sync = null;
		String StartTime = null;
		String CurrentSku = null;
		boolean NewPage = false;
		int count = 1;
		int i = 0;
		double sync_counter = 0;
		double Storecount = 0;
		double Sterlcount = 0;
		double FlightSterl = 0;
		double FlightStore = 0;
		double SterlAvg = 0;
		double StoreAvg = 0;
		double Flightcount = 0;
		String fileName = "";

		// Loops thru the List of Skus and stores returning On Hand and
		// Demand Information

		Iterator it = Storemap.entrySet().iterator();
		demand.clear();

		alreadyExecuted = false;
		IFalreadyExecuted = false;
		SohalreadyExecuted = false;
		SterlOHExecuted = false;
		i++;

		while (it.hasNext()) {

			Map.Entry pair = (Map.Entry) it.next();
			String value = pair.getValue().toString();
			logger.info(value);
			if (value.length() == 3) {
				value = "0" + value;

			}

			if (!SterlOHExecuted) {
				SterlOH = oh.getSterlingOH(sku);
				SterlOHExecuted = true;
			}

			if (!SohalreadyExecuted) {
				StartTime = Soh.Start();
				logger.info(StartTime);
				sohs = Soh.getStoreOH(value, (sku), Storemap);
				// store = Soh.getStoreOH(args[i]);
				SohalreadyExecuted = true;
			}

			// System.out.println(sohs);

			if (!alreadyExecuted) {
				demand = oh.getDemand(value, (sku));
				alreadyExecuted = true;
			}

			if (!IFalreadyExecuted) {
				InFlight = oh.getInflight((sku));
				IFalreadyExecuted = true;
			}

			if (demand.containsKey(value)) {

				Dem = demand.get(value);

			} else {

				Dem = 0;
			}

			if (sohs.containsKey(value)) {

				SOHs = sohs.get(value);

			} else {

				SOHs = 0;
			}

			if (InFlight.containsKey(value)) {

				Flight = InFlight.get(value);

			} else {

				Flight = 0;
			}

			if (SterlOH.containsKey(value)) {

				Sterling = SterlOH.get(value);

			} else {

				Sterling = 0;
			}

			CurrentSku = (sku);
			int storeNum = Integer.parseInt(value);
			int currentSku = Integer.parseInt(CurrentSku);
			// int Sterl = Integer.parseInt(sterl);

			String StoreReport = ("Store-" + value + " Sku-" + (sku)
					+ " Store OH =" + SOHs + " Sterling OH =" + Sterling
					+ " Demand =" + Dem + " In Flight=" + Flight
					+ "                                                    " + count);
			// Writes line to file

			if (SOHs == Sterling + Dem) {

				In_Sync = "Y";

			} else {

				if (Sterling + Dem > SOHs) {

					Sterlcount++;
					SterlAvg += Sterling - SOHs;
					In_Sync = "N-Sterling>";

				} else {

					Storecount++;
					StoreAvg += SOHs - Sterling;
					In_Sync = "N-Store>";
				}

				sync_counter++;

			}

			if (Flight != 0)

			{
				if (SOHs + Flight == Sterling + Dem)

					Flightcount++;

				if (SOHs + Flight > Sterling + Dem)

					FlightStore++;

				if (SOHs + Flight < Sterling + Dem)

					FlightSterl++;

			}

			//
			// System.out.println("sterling= "+SterlAvg+" Store= "+StoreAvg);
			// System.out.println("sterling= "+Sterlcount+" Store= "+Storecount);
			fileName = bl.WriteFile(storeNum, currentSku, SOHs, Sterling, Dem,
					count, In_Sync, NewPage, sync_counter, Sterlcount,
					Storecount, SterlAvg, StoreAvg, Flight, Flightcount,
					FlightStore, FlightSterl, StartTime);
			NewPage = false;
			// System.out.println(count);
			// System.out.println(Storemap.size());
			logger.info(StoreReport);
			// System.out.println(Flightcount + "  " + FlightStore + "  "
			// + FlightSterl);
			// System.out.println(SterlAvg);
			count++;
		}
		SkuInventoryServlet is = new SkuInventoryServlet();
		NewPage = true;
		count = 1;
		sync_counter = 0;
		Sterlcount = 0;
		Storecount = 0;
		SterlAvg = 0;
		StoreAvg = 0;
		Flightcount = 0;
		FlightStore = 0;
		FlightSterl = 0;

		return fileName;

	}

}
