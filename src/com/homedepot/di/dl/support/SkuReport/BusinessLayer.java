package com.homedepot.di.dl.support.SkuReport;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;

import org.apache.log4j.Logger;

public class BusinessLayer extends HttpServlet  {
	final static Logger logger = Logger.getLogger(BusinessLayer.class);
	public BusinessLayer() {

	}

	public void CreateFile() {

	}

	public String WriteFile  (int store, int sku, int SOH, int Sterl, int dem,
			int count, String sync, boolean newPage, double synccounter,
			double sterlcount, double storecount, double sterlAvg,
			double storeAvg, double flight, double flightcount2, double fstore,
			double fsterl, String start) 
	
	{
		FileOutputStream fileOut = null;
		HSSFWorkbook workbook = null;
		HSSFSheet worksheet = null;
		
		String filePath="/opt/isv/tomcat/webapps/COMSupportScripts/Inventory_Report-" + sku+".xls";
		
		
		File f = new File(filePath);
		
		String fileName = "Inventory_Report-" + sku+".xls";
		
        if (!f.exists()) {
				//System.out.println("no exists");
				try {
				fileOut = new FileOutputStream(filePath);
				workbook = new HSSFWorkbook();
				worksheet = workbook.createSheet("Inventory_Report- " + sku);

				HSSFRow row1 = worksheet.createRow((short) 0);
				HSSFRow row2 = worksheet.createRow((short) 1);
				HSSFFont hSSFFont = workbook.createFont();
				HSSFCell cellA1 = row1.createCell((short) 0);
				cellA1.setCellValue("Store#");
				HSSFCellStyle cellStyle = workbook.createCellStyle();
				cellStyle
						.setFillForegroundColor(HSSFColor.LIGHT_CORNFLOWER_BLUE.index);
				cellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);

				hSSFFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
				cellStyle.setFont(hSSFFont);
				cellA1.setCellStyle(cellStyle);

				HSSFCell cellA2 = row2.createCell((short) 0);
				cellA2.setCellValue(store);
				cellStyle = workbook.createCellStyle();

				cellA2.setCellStyle(cellStyle);

				HSSFCell cellB1 = row1.createCell((short) 1);
				cellB1.setCellValue("Sku");
				cellStyle = workbook.createCellStyle();
				cellStyle
						.setFillForegroundColor(HSSFColor.LIGHT_CORNFLOWER_BLUE.index);
				cellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
				hSSFFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
				cellStyle.setFont(hSSFFont);
				cellB1.setCellStyle(cellStyle);

				HSSFCell cellB2 = row2.createCell((short) 1);
				cellB2.setCellValue(sku);
				cellStyle = workbook.createCellStyle();

				cellA2.setCellStyle(cellStyle);

				HSSFCell cellC1 = row1.createCell((short) 2);
				cellC1.setCellValue("Store OH");
				cellStyle = workbook.createCellStyle();
				cellStyle
						.setFillForegroundColor(HSSFColor.LIGHT_CORNFLOWER_BLUE.index);
				cellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
				hSSFFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
				cellStyle.setFont(hSSFFont);
				cellC1.setCellStyle(cellStyle);

				HSSFCell cellC2 = row2.createCell((short) 2);
				cellC2.setCellValue(SOH);
				cellStyle = workbook.createCellStyle();

				cellA2.setCellStyle(cellStyle);

				HSSFCell cellD1 = row1.createCell((short) 3);

				cellD1.setCellValue("Sterling OH");
				cellStyle = workbook.createCellStyle();
				cellStyle
						.setFillForegroundColor(HSSFColor.LIGHT_CORNFLOWER_BLUE.index);
				cellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
				hSSFFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
				cellStyle.setFont(hSSFFont);
				cellD1.setCellStyle(cellStyle);

				HSSFCell cellD2 = row2.createCell((short) 3);
				cellD2.setCellValue(Sterl);
				cellStyle = workbook.createCellStyle();

				cellD2.setCellStyle(cellStyle);

				HSSFCell cellE1 = row1.createCell((short) 4);
				cellE1.setCellValue("Demand");
				cellStyle = workbook.createCellStyle();
				cellStyle
						.setFillForegroundColor(HSSFColor.LIGHT_CORNFLOWER_BLUE.index);
				cellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
				hSSFFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
				cellStyle.setFont(hSSFFont);
				cellE1.setCellStyle(cellStyle);

				HSSFCell cellE2 = row2.createCell((short) 4);
				cellE2.setCellValue(dem);
				cellStyle = workbook.createCellStyle();

				cellE2.setCellStyle(cellStyle);

				HSSFCell cellF1 = row1.createCell((short) 5);
				cellF1.setCellValue("In-Sync");
				cellStyle = workbook.createCellStyle();
				cellStyle
						.setFillForegroundColor(HSSFColor.LIGHT_CORNFLOWER_BLUE.index);
				cellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
				hSSFFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
				cellStyle.setFont(hSSFFont);
				cellF1.setCellStyle(cellStyle);

				HSSFCell cellF2 = row2.createCell((short) 5);
				cellF2.setCellValue(sync);
				cellStyle = workbook.createCellStyle();

				HSSFCell cellF6 = row1.createCell((short) 6);
				cellF6.setCellValue("In-Flight");
				cellStyle = workbook.createCellStyle();
				cellStyle
						.setFillForegroundColor(HSSFColor.LIGHT_CORNFLOWER_BLUE.index);
				cellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
				hSSFFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
				cellStyle.setFont(hSSFFont);
				cellF6.setCellStyle(cellStyle);

				HSSFCell cellF7 = row2.createCell((short) 6);
				cellF7.setCellValue(flight);
				cellStyle = workbook.createCellStyle();

				cellF2.setCellStyle(cellStyle);
				worksheet.autoSizeColumn((short) 5);
				worksheet.autoSizeColumn((short) 1);
				workbook.write(fileOut);
				fileOut.flush();
				fileOut.close();
			}
				catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}


			else {

				FileInputStream Appendfile;
					try {
						
						
				Appendfile = new FileInputStream(filePath);
				workbook = new HSSFWorkbook(Appendfile);
				worksheet = workbook.getSheet("Inventory_Report- " + sku);

				if (count == 1980) {

					
					DateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");
					Date date = new Date();
					logger.info(dateFormat.format(date));
					
					HSSFRow row1=worksheet.getRow(4);
					HSSFRow row2 =worksheet.getRow(5);
//					HSSFRow row1 = worksheet.createRow((short) 4);
//					HSSFRow row2 = worksheet.createRow((short) 5);
				
					HSSFFont hSSFFont = workbook.createFont();

					HSSFCell cellA = row1.createCell((short) 8);
					cellA.setCellValue("Stores");
					HSSFCellStyle cellStyle = workbook.createCellStyle();
					cellStyle
							.setFillForegroundColor(HSSFColor.LIGHT_CORNFLOWER_BLUE.index);
					cellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);

					hSSFFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
					cellStyle.setFont(hSSFFont);
					cellA.setCellStyle(cellStyle);

					HSSFCell cellAa = row2.createCell((short) 8);
					cellAa.setCellValue(1980);
					cellStyle = workbook.createCellStyle();

					HSSFCell cellA3 = row1.createCell((short) 17);
					cellA3.setCellValue("Start");
					
					cellStyle
							.setFillForegroundColor(HSSFColor.LIGHT_CORNFLOWER_BLUE.index);
					cellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
					hSSFFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
					cellStyle.setFont(hSSFFont);
					cellStyle.setWrapText(true);
					cellA3.setCellStyle(cellStyle);

					HSSFCell cellA6 = row2.createCell((short) 17);
					cellA6.setCellValue("Start");


					HSSFCell cellA8 = row1.createCell((short) 18);
					cellA8.setCellValue("Finish");

					cellStyle
							.setFillForegroundColor(HSSFColor.LIGHT_CORNFLOWER_BLUE.index);
					cellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
					hSSFFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
					cellStyle.setFont(hSSFFont);
					cellStyle.setWrapText(true);
					cellA8.setCellStyle(cellStyle);
					
					HSSFCell cellA65 = row2.createCell((short) 17);
					cellA65.setCellValue(start);


					HSSFCell cellA66 = row2.createCell((short) 18);
					cellA66.setCellValue(dateFormat.format(date));


					HSSFCell cellA1 = row1.createCell((short) 9);
					cellA1.setCellValue("Stores out of Sync");
					
					cellStyle
							.setFillForegroundColor(HSSFColor.LIGHT_CORNFLOWER_BLUE.index);
					cellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);

					hSSFFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
					cellStyle.setFont(hSSFFont);
					cellStyle.setWrapText(true);
					cellA1.setCellStyle(cellStyle);

					HSSFCell cellA2 = row2.createCell((short) 9);
					cellA2.setCellValue(synccounter);
			

					HSSFCell cellB1 = row1.createCell((short) 11);
					cellB1.setCellValue("Out of Sync Rate");
				
					cellStyle
							.setFillForegroundColor(HSSFColor.LIGHT_CORNFLOWER_BLUE.index);
					cellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
					hSSFFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
					cellStyle.setFont(hSSFFont);
					cellStyle.setWrapText(true);
					cellB1.setCellStyle(cellStyle);

					HSSFCell cellB2 = row2.createCell((short) 11);

					double ratio = Math.round(synccounter / 1980 * 100);

					cellB2.setCellValue(ratio + "%");
					

					HSSFCell cellB6 = row1.createCell((short) 12);
					cellB6.setCellValue("Out of Sync Rate w/In-Flight");
					
					cellStyle
							.setFillForegroundColor(HSSFColor.LIGHT_CORNFLOWER_BLUE.index);
					cellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
					hSSFFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
					cellStyle.setFont(hSSFFont);
					cellStyle.setWrapText(true);
					cellB6.setCellStyle(cellStyle);

					HSSFCell cellB7 = row2.createCell((short) 12);

					double IFratio = Math
							.round((synccounter - flightcount2) / 1980 * 100);

					cellB7.setCellValue(IFratio + "%");
					
					HSSFCell cellC1 = row1.createCell((short) 13);
					cellC1.setCellValue("Count of Store OH> Steerling OH");
					
					cellStyle
							.setFillForegroundColor(HSSFColor.LIGHT_CORNFLOWER_BLUE.index);
					cellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
					hSSFFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
					cellStyle.setFont(hSSFFont);
					cellStyle.setWrapText(true);
					cellC1.setCellStyle(cellStyle);

					HSSFCell cellC2 = row2.createCell((short) 13);
					cellC2.setCellValue(storecount);
					

					HSSFCell cellD1 = row1.createCell((short) 14);

					cellD1.setCellValue("Count of Sterling OH> Store OH");
					
					cellStyle
							.setFillForegroundColor(HSSFColor.LIGHT_CORNFLOWER_BLUE.index);
					cellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
					hSSFFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
					cellStyle.setWrapText(true);
					cellStyle.setFont(hSSFFont);

					cellD1.setCellStyle(cellStyle);

					HSSFCell cellD2 = row2.createCell((short) 14);

					cellD2.setCellValue(sterlcount);
					

					HSSFCell cellD12 = row1.createCell((short) 15);

					cellD12.setCellValue("Average Store Mismatch");
					
					cellStyle
							.setFillForegroundColor(HSSFColor.LIGHT_CORNFLOWER_BLUE.index);
					cellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
					hSSFFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
					cellStyle.setFont(hSSFFont);
					cellStyle.setWrapText(true);
					cellD12.setCellStyle(cellStyle);

					HSSFCell cellD212 = row2.createCell((short) 15);

					logger.info("Store mistmatch number= " + storeAvg
							+ "store count= " + storecount);

					double StoAvg = (storeAvg / storecount);

					cellD212.setCellValue(StoAvg);

				

					HSSFCell cellD13 = row1.createCell((short) 16);

					cellD13.setCellValue("Average Sterling Mismatch");
					
					cellStyle
							.setFillForegroundColor(HSSFColor.LIGHT_CORNFLOWER_BLUE.index);
					cellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
					hSSFFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
					cellStyle.setFont(hSSFFont);
					cellStyle.setWrapText(true);
					cellD13.setCellStyle(cellStyle);

					HSSFCell cellD213 = row2.createCell((short) 16);

					logger.info("Sterling mistmatch number= " + sterlAvg
							+ "store count= " + sterlcount);

					double SterAvg = (sterlAvg / sterlcount);

					cellD213.setCellValue(SterAvg);
				

					HSSFCell cellD14 = row1.createCell((short) 10);

					cellD14.setCellValue("Stores out of sync/In-Flight");
					
					cellStyle
							.setFillForegroundColor(HSSFColor.LIGHT_CORNFLOWER_BLUE.index);
					cellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
					hSSFFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
					cellStyle.setFont(hSSFFont);
					cellStyle.setWrapText(true);
					cellD14.setCellStyle(cellStyle);

					HSSFCell cellD215 = row2.createCell((short) 10);
					double flightcount = synccounter - flightcount2;
					cellD215.setCellValue(flightcount);
					
					worksheet.setColumnWidth((short) 8, (short) 4500);
					worksheet.setColumnWidth((short) 9, (short) 4500);
					worksheet.setColumnWidth((short) 10, (short) 4000);
					worksheet.setColumnWidth((short) 11, (short) 4500);
					worksheet.setColumnWidth((short) 13, (short) 4000);
					worksheet.setColumnWidth((short) 14, (short) 4000);
					worksheet.setColumnWidth((short) 15, (short) 4000);
					worksheet.setColumnWidth((short) 16, (short) 4000);
					worksheet.setColumnWidth((short) 17, (short) 5500);
					worksheet.setColumnWidth((short) 18, (short) 5500);
					worksheet.setColumnWidth((short) 12, (short) 5500);
					fileOut = new FileOutputStream(filePath);
					workbook.write(fileOut);
					fileOut.flush();
					fileOut.close();
					
			  logger.info("The sku is"+sku);	
					
//				InventoryServlet is=new InventoryServlet();
//				is.download(sku,response);
//				
					
				}
			
					else 
					{
					HSSFRow row2 = worksheet.createRow((short) count);

					HSSFCell cellA2 = row2.createCell((short) 0);
					cellA2.setCellValue(store);
				

					HSSFCell cellB2 = row2.createCell((short) 1);
					cellB2.setCellValue(sku);
					
					HSSFCell cellC2 = row2.createCell((short) 2);
					cellC2.setCellValue(SOH);

					HSSFCell cellD2 = row2.createCell((short) 3);

					cellD2.setCellValue(Sterl);
				

					HSSFCell cellE2 = row2.createCell((short) 4);
					cellE2.setCellValue(dem);
					

					HSSFCell cellF2 = row2.createCell((short) 5);
					cellF2.setCellValue(sync);
					
					HSSFCell cellF3 = row2.createCell((short) 6);
					cellF3.setCellValue(flight);
			

					fileOut = new FileOutputStream(filePath);
//					worksheet.autoSizeColumn((short) 3);
//					worksheet.autoSizeColumn((short) 5);
					workbook.write(fileOut);
					fileOut.flush();
					fileOut.close();

					}
				} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}
		return fileName;
			
}
			}
			
