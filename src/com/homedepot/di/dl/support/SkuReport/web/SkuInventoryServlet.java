package com.homedepot.di.dl.support.SkuReport.web;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.homedepot.di.dl.support.SkuReport.SkuReport;
import org.apache.log4j.Logger;

/**
 * Servlet implementation class SkuInventoryServlet
 */
public class SkuInventoryServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	final static Logger logger = Logger.getLogger(SkuInventoryServlet.class);
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SkuInventoryServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	logger.info("get Method Called");
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		logger.info("post Method Called");

		String skus = request.getParameter("skuNumber");

		SkuReport mskuReport = new SkuReport();

		
		
		String fileName = mskuReport.Execute(skus);

		response.setContentType("application/ms-excel");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().print(fileName);
		
	}

}
