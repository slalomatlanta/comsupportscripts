package com.homedepot.di.dl.support.paymentStageinfoUpdate.web;

import java.io.IOException;
import java.util.GregorianCalendar;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.homedepot.di.dl.support.ErrorDeltas.web.DeltaServlet;
import com.homedepot.di.dl.support.paymentStageinfoUpdate.dao.PaymentStageInfoUpdate;



/**
 * Servlet implementation class PaymentStageInfoUpdateServlet
 */
public class PaymentStageInfoUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	final static Logger logger = Logger.getLogger(PaymentStageInfoUpdateServlet.class);
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PaymentStageInfoUpdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		logger.info("PaymentStageInfoUpdateServlet :: Started"+GregorianCalendar.getInstance().getTime());
		
		
		try {
			PaymentStageInfoUpdate.paymentStageupdate();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.info("PaymentStageInfoUpdateServlet Exception ::"+GregorianCalendar.getInstance().getTime()+e);
		}
		logger.info("PaymentStageInfoUpdateServlet :: Completed"+GregorianCalendar.getInstance().getTime());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
