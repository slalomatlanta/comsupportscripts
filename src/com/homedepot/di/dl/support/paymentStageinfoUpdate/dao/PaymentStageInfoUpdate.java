package com.homedepot.di.dl.support.paymentStageinfoUpdate.dao;

import java.io.FileWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeMessage.RecipientType;

import com.homedepot.di.dl.support.util.Constants;
import com.homedepot.di.dl.support.util.DBConnectionUtil;



public class PaymentStageInfoUpdate {

	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		paymentStageupdate();

	}
	
	
	 public static  void paymentStageupdate() throws 
		Exception {
			 
			 
	StringBuilder emailText = new StringBuilder();
	int totalRowsupdated=0;
	int rowsUpdated=0;
		try {
	
	//String app="";
		//FileWriter writer = new FileWriter("C:\\store\\HDPaymentStageInfo.csv");
		FileWriter writer = new FileWriter("/opt/isv/tomcat-6.0.18/temp/HDPaymentStageInfo.csv");
		
		emailText.append(System.getProperty("line.separator"));
		
		Calendar cal = Calendar.getInstance();
		
		
		// String inputFile = "/opt/isv/tomcat-6.0.18/temp/Stores.csv"
		DBConnectionUtil dbConn = new DBConnectionUtil();
		
		Connection sterCon = dbConn.getJNDIConnection(Constants.STERLING_SSC_RO_JNDI);
		
//		Class.forName(driverClass);
//		 sterCon = DriverManager.getConnection(connectionURL, uName,
//				uPassword);
		
		writer.append("ORDER_HEADER_KEY");
		writer.append(',');
		writer.append("HOST_ORDER_LINE_REF");
		writer.append(',');
		writer.append("CURRENT_PAYMENT_STAGE_INFO_KEY");
		writer.append(',');
		writer.append("CURRENT_TAX_CITY");
		writer.append(',');
		writer.append("CURRENT_TAX_COUNTY");
		writer.append(',');
		writer.append("CURRENT_TAX_STATE");
		writer.append(',');
		writer.append("CURRENT_POSTAL_CODE");
		writer.append(',');
		writer.append("CURRENT_COUNTRY_CODE");
		writer.append(',');
		writer.append("CURRENT_TAX_RATE");
		writer.append(',');
		writer.append("TO_BE_PAYMENT_STAGE_INFO_KEY");
		writer.append(',');
		writer.append("TO_BE_TAX_CITY");
		writer.append(',');
		writer.append("TO_BE_TAX_COUNTY");
		writer.append(',');
		writer.append("TO_BE_TAX_STATE");
		writer.append(',');
		writer.append("TO_BE_POSTAL_CODE");
		writer.append(',');
		writer.append("TO_BE_COUNTRY_CODE");
		writer.append(',');
		writer.append("TO_BE_TAX_RATE");
		writer.append(',');
		writer.append('\n');
		
		Statement st = null;
		st = sterCon.createStatement();
		ResultSet result = null;
		
		
		String Query = "With psi_withouttax As (select hd_payment_stage_info_key, order_header_key, Host_Order_Line_Ref,tax_city, tax_county, tax_state, "
				+ "postal_code, country_code, tax_rate from thd01.hd_payment_stage_info where order_header_key > '20150915' and TAX_City is null), "
				+ "maxpsi_withtax As (select psi.order_header_key, psi.Host_Order_Line_Ref, psi_withouttax.hd_payment_stage_info_key, psi_withouttax.tax_city, "
				+ "psi_withouttax.tax_county, psi_withouttax.tax_state, psi_withouttax.postal_code, psi_withouttax.country_code, psi_withouttax.tax_rate, "
				+ "max(psi.hd_payment_stage_info_key) max_hd_payment_stage_info_key from thd01.HD_PAYMENT_STAGE_INFO psi, psi_withouttax where psi.order_header_key=psi_withouttax.order_header_key "
				+ "and psi.Host_Order_Line_Ref=psi_withouttax.Host_Order_Line_Ref and psi.tax_city is not null and "
				+ "psi.hd_payment_stage_info_key < psi_withouttax.hd_payment_stage_info_key group by psi.order_header_key, psi.Host_Order_Line_Ref,psi_withouttax.hd_payment_stage_info_key, "
				+ "psi_withouttax.tax_city, psi_withouttax.tax_county, psi_withouttax.tax_state, psi_withouttax.postal_code, psi_withouttax.country_code, psi_withouttax.tax_rate) "
				+ "select psi.order_header_key, psi.Host_Order_Line_Ref, maxpsi_withtax.hd_payment_stage_info_key current_payment_stage_info_key, maxpsi_withtax.tax_city current_tax_city, "
				+ "maxpsi_withtax.tax_county current_tax_county, maxpsi_withtax.tax_state current_tax_state, maxpsi_withtax.postal_code current_postal_code, "
				+ "maxpsi_withtax.country_code current_country_code, maxpsi_withtax.tax_rate current_tax_rate, psi.hd_payment_stage_info_key "
				+ "to_be_payment_stage_info_key, psi.tax_city to_be_tax_city, psi.tax_county to_be_tax_county, psi.tax_state to_be_tax_state, "
				+ "psi.postal_code to_be_postal_code, psi.country_code to_be_country_code, psi.tax_rate to_be_tax_rate, "
				+ "'update thd01.hd_payment_stage_info set tax_city = '''||psi.tax_city||''', tax_county = '''||psi.tax_county||''', "
				+ "tax_state = '''||psi.tax_state||''', postal_code = '''||psi.postal_code||''', country_code = '''||psi.country_code||''', tax_rate = '''||psi.tax_rate||''' "
				+ ",modifyts = sysdate, modifyuserid = ''COMSupport'' where "
				+ "hd_payment_stage_info_key = '''||maxpsi_withtax.hd_payment_stage_info_key||'''' from thd01.HD_PAYMENT_STAGE_INFO psi, "
				+ "maxpsi_withtax where psi.hd_payment_stage_info_key = maxpsi_withtax.max_hd_payment_stage_info_key order by psi.order_header_key, psi.Host_Order_Line_Ref";
		
		
		
		result = st.executeQuery(Query);

		

		
		
		while (result.next()) {
			
			
			writer.append("'"+result.getString(1));
			writer.append(',');
			writer.append("'"+result.getString(2));
			writer.append(',');
			writer.append("'"+result.getString(3));
			writer.append(',');
			writer.append("'"+result.getString(4));
			writer.append(',');
			writer.append("'"+result.getString(5));
			writer.append(',');
			writer.append("'"+result.getString(6));
			writer.append(',');
			writer.append("'"+result.getString(7));
			writer.append(',');
			writer.append("'"+result.getString(8));
			writer.append(',');
			writer.append("'"+result.getString(9));
			writer.append(',');
			writer.append("'"+result.getString(10));
			writer.append(',');
			writer.append("'"+result.getString(11));
			writer.append(',');
			writer.append("'"+result.getString(12));
			writer.append(',');
			writer.append("'"+result.getString(13));
			writer.append(',');
			writer.append("'"+result.getString(14));
			writer.append(',');
			writer.append("'"+result.getString(15));
			writer.append(',');
			writer.append("'"+result.getString(16));
			writer.append(',');
			
			writer.append('\n');
			
			
			
			
		    
		    try
			{
			
				
             
            String updateQuery = result.getString(17); 
            //System.out.println(updateQuery);

            DBConnectionUtil dbConn1 = new DBConnectionUtil();
    		
    		Connection con1 = dbConn1.getJNDIConnection(Constants.STERLING_ATC_RW_JNDI);
             Statement stmt2 = null;
             stmt2 = con1.createStatement();
             
              rowsUpdated = stmt2.executeUpdate(updateQuery);
              totalRowsupdated = totalRowsupdated + rowsUpdated;
             con1.commit();
             stmt2.close();
             con1.close();
             //System.out.println(rowsUpdated + " Rows Updated");

			} catch (Exception e) {
             // TODO Auto-generated catch block
             e.printStackTrace();
             
      }
        }
		
		writer.flush();
		writer.close();
}
catch (Exception e) {
// TODO Auto-generated catch block
e.printStackTrace();

} 
		if(totalRowsupdated > 0)	
			
		sendAttachment(totalRowsupdated);
                   
		 }
	 
	 
	 public static void sendAttachment(int rowsUpdated) throws Exception{
		 String host = "mail1.homedepot.com";
	  String from = "horizon@cpliisad.homedepot.com";
	  String[] to = {"_2fc77b@homedepot.com" };
	  //String[] Cc ={""};
	  // Get system properties
	  Properties properties = System.getProperties();

	  // Setup mail server
	  properties.setProperty("mail.smtp.host", host);

	  // Get the default Session object.
	  Session session = Session.getInstance(properties);

	  // Define message
	  Message message = new MimeMessage(session);
	  message.setFrom(new InternetAddress(from));
	  InternetAddress[] addressTo = new InternetAddress[to.length];
	 // InternetAddress[] addressCc = new InternetAddress[Cc.length];
	  for (int i = 0; i < to.length; i++) 
	  { addressTo[i] = new InternetAddress(to[i]); 
	  //System.out.println("To Address "+to[i]);
	  } 
//	  for (int j = 0; j < Cc.length; j++) 
//	  { addressCc[j] = new InternetAddress(Cc[j]); 
//	  System.out.println("Cc Address "+Cc[j]);
//	  }
	  message.setRecipients(RecipientType.TO, addressTo); 
	  //message.setRecipients(RecipientType.CC, addressCc); 
	  message.setSubject("Payment Stage Info records update - Defect/Story #: COM-1908");

	  // Create the message part 
	  BodyPart messageBodyPart = new MimeBodyPart();
	  String msgContent = "Hi All,<br/><br/>PFA report for updated payment stage info records.<br/>Number of rows Updated: "+rowsUpdated+"<br/><br/>Thanks<br/>";
     msgContent = msgContent + "COM Multichannel Support";
	  // Fill the message
	  messageBodyPart.setContent(msgContent ,"text/html");
	  	

	  Multipart multipart = new MimeMultipart();
	  multipart.addBodyPart(messageBodyPart);

	  // Part two is attachment
	  messageBodyPart = new MimeBodyPart();
	  //String filename = "C:\\store\\HDPaymentStageInfo.csv";
	  String filename = "/opt/isv/tomcat-6.0.18/temp/HDPaymentStageInfo.csv";
	  DataSource source = new FileDataSource(filename);
	  messageBodyPart.setDataHandler(new DataHandler(source));
	  messageBodyPart.setFileName(source.getName());
	  multipart.addBodyPart(messageBodyPart);

	  // Put parts in message
	  message.setContent(multipart);

	  // Send the message
	  Transport.send(message);
	   //System.out.println("Msg Send ....");
	  }

}
	 
