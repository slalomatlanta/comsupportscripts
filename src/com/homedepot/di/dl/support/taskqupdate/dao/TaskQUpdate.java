package com.homedepot.di.dl.support.taskqupdate.dao;

import java.io.FileWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeMessage.RecipientType;

import com.homedepot.di.dl.support.util.Constants;
import com.homedepot.di.dl.support.util.DBConnectionUtil;



public class TaskQUpdate {

	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		getUpdateTaskQQueryList();

	}
	
	
	 public static void getUpdateTaskQQueryList() throws 
		Exception {
			 
		 
		 final String sscDriverClass = "oracle.jdbc.driver.OracleDriver";
			DBConnectionUtil dbConn = new DBConnectionUtil();
			final String sscConnectionURL = "jdbc:oracle:thin://@apragor11-scan.homedepot.com:1521/dpr77mm_sro01";
			final String sscUName = "MMUSR01";
			final String sscUPassword = "COMS_MMUSR01";
			//final String atcDriverClass = "oracle.jdbc.driver.OracleDriver";
			final String atcConnectionURL = "jdbc:oracle:thin://@aprbgor12-scan.homedepot.com:1521/dpr77mm_srw03";
			final String atcUName = "THD03";
			final String atcUPassword = "ra1nySumm3r"; 
			 
	StringBuilder emailText = new StringBuilder();
	int totalRowsupdated=0;
	int rowsUpdated=0;
		try {
	
	//String app="";
		FileWriter writer = new FileWriter("C:\\store\\TaskQUpdate.csv");
		//FileWriter writer = new FileWriter("/opt/isv/tomcat-6.0.18/temp/HDPaymentStageInfo.csv");
		
		emailText.append(System.getProperty("line.separator"));
		
		Calendar cal = Calendar.getInstance();
		
		
		// String inputFile = "/opt/isv/tomcat-6.0.18/temp/Stores.csv"
		//DBConnectionUtil dbConn = new DBConnectionUtil();
		
		//Connection sterCon = dbConn.getJNDIConnection(Constants.STERLING_SSC_RO_JNDI);
		
	
	Class.forName(sscDriverClass);
Connection sterCon = DriverManager.getConnection(sscConnectionURL, sscUName,
		sscUPassword);
		
		writer.append("ORDER_HEADER_KEY");		
		writer.append('\n');
		
		Statement st = null;
		st = sterCon.createStatement();
		ResultSet result = null;
		
		
		String query  =" select distinct order_header_key, count(order_line_schedule_key), t.lockid from THD01.yfs_order_line_schedule sc, thd01.yfs_task_q t where "
				+" t.data_key=sc.order_header_key and t.transaction_key = 'SCHEDULE.0001' and t.hold_flag='N' "
				+" and sc.order_header_key > to_char((sysdate-1/24),'YYYYMMDDHH24') "
				+" group by sc.order_header_key, t.lockid order by count(order_line_schedule_key) desc" ;
		
		
		result = st.executeQuery(query);

		

		
		
		while (result.next()) {
			
			
			writer.append("'"+result.getString(1));
			writer.append('\n');
			

		    try
			{
			
				
             
            String updateQuery = "update THD01.yfs_task_q set available_date= available_date +60,hold_flag='Y', modifyts=sysdate, modifyuserid='COMSupport' " 
					+" where transaction_key = 'SCHEDULE.0001' and lockid > 1000 and hold_flag='N' and data_key='"+result.getString(1)+"'" ;
            //System.out.println(updateQuery);
            Connection atcsterCon = DriverManager.getConnection(atcConnectionURL, atcUName,
            		atcUPassword);
            		
            		
            		Statement st1 = null;
//            		st1 = atcsterCon.createStatement();
           /* DBConnectionUtil dbConn1 = new DBConnectionUtil();
    		
    		Connection con1 = dbConn1.getJNDIConnection(Constants.STERLING_ATC_RW_JNDI);
             Statement stmt2 = null;
             stmt2 = con1.createStatement();
             */
              rowsUpdated = st1.executeUpdate(updateQuery);
              totalRowsupdated = totalRowsupdated + rowsUpdated;
              atcsterCon.commit();
              st1.close();
              atcsterCon.close();
             //System.out.println(rowsUpdated + " Rows Updated");

			} catch (Exception e) {
             // TODO Auto-generated catch block
             e.printStackTrace();
             
      }
        }
		
		writer.flush();
		writer.close();
		result.close();
		st.close();
		sterCon.close();
		
		
}
catch (Exception e) {
// TODO Auto-generated catch block
e.printStackTrace();



} 
			
			
		sendAttachment(totalRowsupdated);
                   
		 }
	 
	 
	 public static void sendAttachment(int rowsUpdated) throws Exception{
		 String host = "mail1.homedepot.com";
	  String from = "horizon@cpliisad.homedepot.com";
	  String[] to = {"shipra_sultania@homedepot.com" };
	  //String[] Cc ={""};
	  // Get system properties
	  Properties properties = System.getProperties();

	  // Setup mail server
	  properties.setProperty("mail.smtp.host", host);

	  // Get the default Session object.
	  Session session = Session.getInstance(properties);

	  // Define message
	  Message message = new MimeMessage(session);
	  message.setFrom(new InternetAddress(from));
	  InternetAddress[] addressTo = new InternetAddress[to.length];
	 // InternetAddress[] addressCc = new InternetAddress[Cc.length];
	  for (int i = 0; i < to.length; i++) 
	  { addressTo[i] = new InternetAddress(to[i]); 
	  //System.out.println("To Address "+to[i]);
	  } 
//	  for (int j = 0; j < Cc.length; j++) 
//	  { addressCc[j] = new InternetAddress(Cc[j]); 
//	  System.out.println("Cc Address "+Cc[j]);
//	  }
	  message.setRecipients(RecipientType.TO, addressTo); 
	  //message.setRecipients(RecipientType.CC, addressCc); 
	  message.setSubject("Updating TaskQ Table for lock id greater than 1000");

	  // Create the message part 
	  BodyPart messageBodyPart = new MimeBodyPart();
	  String msgContent = "Hi All,<br/><br/>PFA report for updated taskqkey records.<br/>Number of rows Updated: "+rowsUpdated+"<br/><br/>Thanks<br/>";
     msgContent = msgContent + "COM Multichannel Support";
	  // Fill the message
	  messageBodyPart.setContent(msgContent ,"text/html");
	  	

	  Multipart multipart = new MimeMultipart();
	  multipart.addBodyPart(messageBodyPart);

	  // Part two is attachment
	  messageBodyPart = new MimeBodyPart();
	  String filename = "C:\\store\\TaskQUpdate.csv";
	  //String filename = "/opt/isv/tomcat-6.0.18/temp/HDPaymentStageInfo.csv";
	  DataSource source = new FileDataSource(filename);
	  messageBodyPart.setDataHandler(new DataHandler(source));
	  messageBodyPart.setFileName(source.getName());
	  multipart.addBodyPart(messageBodyPart);

	  // Put parts in message
	  message.setContent(multipart);

	  // Send the message
	  Transport.send(message);
	   //System.out.println("Msg Send ....");
	  }

}
	 
