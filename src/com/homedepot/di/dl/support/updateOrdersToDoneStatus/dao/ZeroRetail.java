package com.homedepot.di.dl.support.updateOrdersToDoneStatus.dao;


import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.mail.*;
import javax.activation.*;
import javax.mail.internet.*;
import javax.mail.internet.MimeMessage.RecipientType;

import com.homedepot.di.dl.support.util.Constants;
import com.homedepot.di.dl.support.util.DBConnectionUtil;

import org.apache.log4j.Logger;
public class ZeroRetail {	
	final static Logger logger = Logger.getLogger(ZeroRetail.class);
	public static void main(String[] args) throws SQLException, IOException, Exception {
		
		
		//System.out.println("Main");
		ZeroRetailamt();
	}
	
		
		 public static void ZeroRetailamt() throws 
		Exception {
			 
			 
	StringBuilder emailText = new StringBuilder();
	int total1=0;
	int UpdateQuery1=0;
		try {
	
	//String app="";
		//FileWriter writer = new FileWriter("C:\\store\\ZeroRetail.csv");
		FileWriter writer = new FileWriter("/opt/isv/tomcat-6.0.18/temp/ZeroRetail.csv");

		
		emailText.append(System.getProperty("line.separator"));
		
		Calendar cal = Calendar.getInstance();
		
		DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
		String appToDate = dateFormat.format(cal.getTime());
		logger.info(appToDate);

		// String inputFile = "/opt/isv/tomcat-6.0.18/temp/Stores.csv"
		DBConnectionUtil dbConn = new DBConnectionUtil();
		
		Connection sscConn = dbConn.getJNDIConnection(Constants.STERLING_SSC_RO_JNDI);
		
//		Class.forName(driverClass);
//		 sterCon = DriverManager.getConnection(connectionURL, uName,
//				uPassword);
		
		writer.append("psi.UNIT_NET_RETAIL");
		writer.append(',');
		writer.append(" psi.HOST_ORDER_LINE_REF");
		writer.append(',');
		writer.append("psi.STATUS");
		writer.append(',');
		writer.append("psi.order_header_key");
		writer.append(',');
		writer.append("oh.extn_host_order_ref");
		writer.append(',');
		writer.append("count");
		writer.append('\n');
		
		Statement st = null;
		st = sscConn.createStatement();
		ResultSet result = null;
		
		String OrderHeaderkey=null;
		String Query = "select psi.UNIT_NET_RETAIL, psi.HOST_ORDER_LINE_REF, psi.STATUS, psi.order_header_key, oh.extn_host_order_ref, count(*) "
+ " from THD01.HD_PAYMENT_STAGE_INFO psi, THD01.YFS_ORDER_HEADER oh "
+ " where psi.UNIT_NET_RETAIL = 0  "
  + "and psi.STATUS = 0 "
  + " and psi.POS_TRANS_ID is null "
  + "and oh.ORDER_HEADER_KEY = psi.ORDER_HEADER_KEY "
  + " and oh.DOCUMENT_TYPE = 0001 " 
  + "and to_char(oh.modifyts,'YYYYMMDD') > '20151028' "
  + "and oh.EXTN_SVS_ORDER_STATUS = '1150' " 
+ "group by psi.UNIT_NET_RETAIL, psi.HOST_ORDER_LINE_REF, psi.STATUS, psi.order_header_key, oh.extn_host_order_ref "
+ "having count(*) > 1 ";
		
		
		logger.debug(Query);
		result = st.executeQuery(Query);

		

		
		int totalRowsupdated=0;
		while (result.next()) {
			//System.out.println("RetailAmt:"+result.getString(1));
			//String app1=Integer.toString(i);
			String instructionDetailkey=result.getString(1);
			
			writer.append(result.getString(1));
			writer.append(',');
			
			
			writer.append(result.getString(2));
			writer.append(',');
			writer.append(result.getString(3));
			writer.append(',');
			OrderHeaderkey=result.getString(4);
			writer.append("'"+result.getString(4));
			writer.append(',');
			writer.append(result.getString(5));
			writer.append(',');
			writer.append(result.getString(6));
			writer.append(',');
			writer.append('\n');
			
			
			try
			{
								
				Connection atcConn = dbConn.getJNDIConnection(Constants.STERLING_ATC_RW_JNDI);
				               
               
                
                final String UpdateQuery = "update THD01.YFS_ORDER_HEADER set EXTN_SVS_ORDER_STATUS='1000', EXTN_SVS_ORDER_STATUS_DESC='Done', "
                							+ "modifyts=sysdate, MODIFYUSERID='COMSupport_DVScript' where ORDER_HEADER_KEY ='"
                                                                     + OrderHeaderkey
                                                                     + "'";
                                                         

                
                Statement stmt5 = null;
                stmt5 = atcConn.createStatement();
                logger.info(UpdateQuery);
                 UpdateQuery1 = stmt5.executeUpdate(UpdateQuery);
                total1 = total1 + UpdateQuery1;
                atcConn.commit();
                stmt5.close();
                atcConn.close();
                logger.info(UpdateQuery + " Rows Updated");

			} catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                
         }
			
			
         
                      }
		
		//System.out.println(("Selected"));
		result.close();
		st.close();
		sscConn.close();
		writer.flush();
		writer.close();
		}
		catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            
            
            
     } 
		sendAttachment(total1);
                      
		 }
		

			
	

	public static void sendAttachment(int rowsUpdated) throws Exception{String host = "mail1.homedepot.com";
	  String from = "horizon@cpliisad.homedepot.com";
	  String[] to = {"_2fc77b@homedepot.com"};
	  String[] Cc ={"_275769@homedepot.com"};
	  // Get system properties
	  Properties properties = System.getProperties();

	  // Setup mail server
	  properties.setProperty("mail.smtp.host", host);

	  // Get the default Session object.
	  Session session = Session.getDefaultInstance(properties);

	  // Define message
	  Message message = new MimeMessage(session);
	  message.setFrom(new InternetAddress(from));
	  InternetAddress[] addressTo = new InternetAddress[to.length];
	  InternetAddress[] addressCc = new InternetAddress[Cc.length];
	  for (int i = 0; i < to.length; i++) 
	  { addressTo[i] = new InternetAddress(to[i]); 
	  logger.debug("To Address "+to[i]);
	  } 
	  for (int j = 0; j < Cc.length; j++) 
	  { addressCc[j] = new InternetAddress(Cc[j]); 
	 logger.debug("Cc Address "+Cc[j]);
	  }
	  message.setRecipients(RecipientType.TO, addressTo); 
	  message.setRecipients(RecipientType.CC, addressCc); 
	  message.setSubject("Orders stuck in Done Validate Status");

	  // Create the message part 
	  BodyPart messageBodyPart = new MimeBodyPart();
	  String msgContent = "Hi All,<br/><br/>PFA report for updated orders on Zero Retail Amount-Orders stuck in Done Validate Status.<br/>Number of rows Updated: "+rowsUpdated+"<br/><br/>Thanks<br/>";
      msgContent = msgContent + "COM Multichannel Support";
	  // Fill the message
	  messageBodyPart.setContent(msgContent ,"text/html");
	  	

	  Multipart multipart = new MimeMultipart();
	  multipart.addBodyPart(messageBodyPart);

	  // Part two is attachment
	  messageBodyPart = new MimeBodyPart();
	  //String filename = "C:\\store\\ZeroRetail.csv";
	  String filename = "/opt/isv/tomcat-6.0.18/temp/ZeroRetail.csv";
	  DataSource source = new FileDataSource(filename);
	  messageBodyPart.setDataHandler(new DataHandler(source));
	  messageBodyPart.setFileName(filename);
	  multipart.addBodyPart(messageBodyPart);

	  // Put parts in message
	  message.setContent(multipart);

	  // Send the message
	  Transport.send(message);
	   logger.info("Msg Send ...."); }



}


