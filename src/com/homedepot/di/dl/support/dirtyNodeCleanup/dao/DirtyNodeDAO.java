package com.homedepot.di.dl.support.dirtyNodeCleanup.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.homedepot.di.dl.support.dirtyNodeCleanup.dto.DetailDTO;
import com.homedepot.di.dl.support.util.Constants;
import com.homedepot.di.dl.support.util.DBConnectionUtil;

public class DirtyNodeDAO {
	private static final Logger logger = Logger.getLogger(DirtyNodeDAO.class);

	public static ArrayList<DetailDTO> getQueryDetails() {
		ArrayList<DetailDTO> skuNodeList = new ArrayList<DetailDTO>();
		
		Statement stmt = null;
		ResultSet rset = null;
		String query = "select distinct trim(ol.extn_sku_code), trim(inc.NODE_KEY), '<API Name=\"manageInventoryNodeControl\"><Input><InventoryNodeControl InventoryPictureCorrect=\"Y\" ItemID=\"'|| "
				+ "trim(inc.item_id)||'\" Node=\"'||trim(inc.NODE_KEY)||'\" NodeControlType=\"ON_HOLD\" OrganizationCode=\"HDUS\" ProductClass=\"'||trim(inc.product_class)|| "
				+ "'\" UnitOfMeasure=\"'||inc.UOM||'\"/></Input></API>' "
				+ "from thd01.YFS_INVENTORY_NODE_CONTROL inc,thd01.yfs_order_line ol  "
				+ "where ol.item_id=inc.item_id  "
				+ "  and inc.INVENTORY_NODE_CONTROL_KEY >'20160411'  "
				+ "  and ol.CUSTOMER_PO_LINE_NO <> ' '  "
				+ "  and ol.ORDERED_QTY=0  "
				+ "  and ol.EXTN_TENDER_QUANTITY=0 ";
		try {
//			DBConnectionUtil dbConn = new DBConnectionUtil();
//			Connection con = dbConn
//					.getJNDIConnection(Constants.STERLING_SSC_RO_JNDI);
			final String sscDriverClass = "oracle.jdbc.driver.OracleDriver";
			final String sscConnectionURL = "jdbc:oracle:thin:@//spragor10-scan.homedepot.com:1521/dpr77mm_sro01";
			final String sscUName = "MMUSR01";
			final String sscUPassword = "COMS_MMUSR01";
			Class.forName(sscDriverClass);
			Connection con = DriverManager.getConnection(sscConnectionURL,
					sscUName, sscUPassword);
			stmt = con.createStatement();
			rset = stmt.executeQuery(query);
			if ((rset) != null) {
				while (rset.next()) {
					DetailDTO dd = new DetailDTO();
					dd.setSku(rset.getString(1));
					dd.setNode(rset.getString(2));
					dd.setApi(rset.getString(3));
					skuNodeList.add(dd);
				}
			}
		} catch (Exception e) {
			logger.debug("Exception Occurred in DAO class " + e);
			e.printStackTrace();
		}

		return skuNodeList;
	}

}
