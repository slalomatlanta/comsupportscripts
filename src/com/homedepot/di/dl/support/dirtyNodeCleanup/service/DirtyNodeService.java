package com.homedepot.di.dl.support.dirtyNodeCleanup.service;

import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Properties;

import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage.RecipientType;

import org.apache.log4j.Logger;

import com.homedepot.di.dl.support.dirtyNodeCleanup.dao.DirtyNodeDAO;
import com.homedepot.di.dl.support.dirtyNodeCleanup.dto.DetailDTO;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class DirtyNodeService {
	private static final Logger logger = Logger
			.getLogger(DirtyNodeService.class);

	public static void main(String args[]) {
		logger.debug("DirtyNodeService Started at "
				+ java.util.GregorianCalendar.getInstance().getTime());
		runCleanup();
		logger.debug("DirtyNodeService Completed at "
				+ java.util.GregorianCalendar.getInstance().getTime());
	}

	public static void runCleanup() {
		ArrayList<DetailDTO> skuNodeList = new ArrayList<DetailDTO>();
		skuNodeList = DirtyNodeDAO.getQueryDetails();
		
		if (skuNodeList.size() > 0) {
			String multiApiInput = formMultiApi(skuNodeList);
			logger.debug(multiApiInput);
			boolean multiApiPut = processXML (multiApiInput);
			logger.debug("Multi API hit is "+ multiApiPut);
			if (multiApiPut){
				String mailContent = "<html> <head> <body> <font face=\"calibri\"> <h3>Dirty Nodes cleaned up for below SKU and Nodes</h3> <table border=\"1\" cellpadding=\"0\" cellspacing=\"0\" width=\"200px\" style=\"border-collapse:collapse;\"> <tr> <th>SKU</th> <th>Node</th> </tr> ";
				for (DetailDTO dd : skuNodeList){
					mailContent = mailContent + "<tr> <td>"+dd.getSku()+"</td> <td>"+dd.getNode()+"</td> </tr> ";
				}
				mailContent = mailContent + "</table> <p> <i>Note: This is system generated mail. Please do not respond to this.</i> </p> </font> </body> </head> </html>";
				sendMail (mailContent);
			}
			else{
				String mailContent = "<html> <head> <body> <font face=\"calibri\"> <h3>No records fetched for cleanup</h3> <p> <i>Note: This is system generated mail. Please do not respond to this.</i> </p> </font> </body> </head> </html>";
				sendMail (mailContent);
			}
			
		} else {
			logger.debug("No Records fetched from the query");
		}
	}

	public static String formMultiApi(ArrayList<DetailDTO> skuNodeList) {
		String output = "<MultiApi>";

		for (DetailDTO dd : skuNodeList) {
			output = output + dd.getApi();
		}
		output = output + "</MultiApi>";

		return output;
	}
	
	public static boolean processXML(String requestXML) {
		boolean xmlPut = false;
//		String url = "http://ccliqas4:15400/smcfs/interop/InteropHttpServlet";
		 String url =
		 "http://cclidis2:15400/smcfs/interop/InteropHttpServlet";

		String URLEncodedInputXML = null;
		try {
			URLEncodedInputXML = URLEncoder.encode(requestXML, "UTF-8");
		} catch (Exception ex) {
			logger.debug("Exception occurred in send method: " + ex);
		}

		StringBuilder queryString = new StringBuilder();
		queryString.append("InteropApiName=");
		queryString.append("multiApi");
		queryString.append("&IsFlow=N");
		queryString.append("&YFSEnvironment.userId=" + "admin");
		queryString.append("&InteropApiData=");
		queryString.append(URLEncodedInputXML);

		String outXml = send(url, queryString.toString());
		if (outXml.contains("MultiApi")) {
			xmlPut = true;
		}
		logger.debug("Response XML: " + outXml);
		return xmlPut;
	}

	public static String send(String url, String postRequest) {
		String sterlingResponse = null;
		// Create Jersey client
		Client client = Client.create();

//		String token = "eS6AxTugxondtSBi8KhzUnE2NVPEB4jj1wB7liUQEOUKf4Bn9UWopWx7zJnDbqzN53BG7mkXOgWoJuLH0Py37Ekohwol41hn7qUWh5KU4Mmw14KwQwfOOIgpKFwRnFbSkNk86nYH0g";
		 String token =
		 "F4EJPD2Z7TbyLUP4llcrQsxPaE6yMjM0y8KkbvP8cwCN9YKBs7Qxdm3lrRppaPiGzrq7djLoSp5IfA8pJMFYtuVCHsTWEX4W6v5GpfL6E8J57j0ZfJQmhwbZJla1cbzPKvaAShYH1KIxhhmxg1RsesWqVH54fyPZRviJFURRVhfx0FuqPYxdaaGSL4GgnPym1da7lti6bOblN0EOxHK45fm5gkf5vV3eQmNaq5C7NqtSVbiaMIZUJgkGTdrdeY7HVqx7FmFNj8rphDVQuNfeUHSt2euMu6zeB4y";
		// Create WebResource
		WebResource webResource = client.resource(url);
		ClientResponse response = null;
		try {
			response = webResource.header("THDService-Auth", token)
					.type("application/x-www-form-urlencoded")
					.post(ClientResponse.class, postRequest);

			// Retrieve status and check if it is OK
			int status = 0;
			if (response != null) {
				status = response.getStatus();
			}
		} catch (Exception ae) {
			logger.debug("Exception occurred in send method: " + ae);
		}

		if (response != null) {
			sterlingResponse = response.getEntity(String.class);
		}
		return sterlingResponse;
	}
	
	
	public static void sendMail(String msgContent) {
		logger.debug("Dirty Node Cleanup: Preparing to send email");
		final String user = "horizon@cpliisad.homedepot.com";
		final String password = "xxx";// change accordingly
		String host = "mail1.homedepot.com";
		Properties props = new Properties();
		props.put("mail.host", host);
		Session session = Session.getInstance(props,
				new javax.mail.Authenticator() {
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(user, password);
					}
				});
		try {
			String[] to = { "_2fc77b@homedepot.com" , "TRACEY_PHILLIPS@homedepot.com", "SANTHANAKRISHNAN_JOTHI@homedepot.com","MANISH_BANSAL@homedepot.com","JAYOSTU_VISHWAKARMA@homedepot.com" };
			String[] Cc = {};
//			 String[] to = { "rakesh_j_satya@homedepot.com",
//			 "jagatdeep_chakraborty@homedepot.com" };
			// String[] Cc = {};
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress(user));
			DateFormat dateFormat = new SimpleDateFormat(
					"MM/dd/yyyy hh:mm:ss a");
			Date today = new Date();
			String date1 = dateFormat.format(today);
			message.setSubject("Dirty Node Cleanup - " + date1);
			// message.setText("This is simple program of sending email using JavaMail API");
			message.setFrom(new InternetAddress(user));
			InternetAddress[] addressTo = new InternetAddress[to.length];
			InternetAddress[] addressCc = new InternetAddress[Cc.length];
			for (int i = 0; i < to.length; i++) {
				addressTo[i] = new InternetAddress(to[i]);
				logger.debug("To Address " + to[i]);
			}
			for (int j = 0; j < Cc.length; j++) {
				addressCc[j] = new InternetAddress(Cc[j]);
				logger.debug("Cc Address " + Cc[j]);
			}
			message.setRecipients(RecipientType.TO, addressTo);
			message.setRecipients(RecipientType.CC, addressCc);
			message.setContent(msgContent, "text/html");
			Transport.send(message);
			for (int i = 0; i < to.length; i++) {
				addressTo[i] = new InternetAddress(to[i]);
				logger.debug("Message delivered to--- " + to[i]);
			}
			for (int j = 0; j < Cc.length; j++) {
				addressCc[j] = new InternetAddress(Cc[j]);
				logger.debug("Message delivered to cc recepients--- "
						+ Cc[j]);
			}

		} catch (MessagingException e) {
			e.printStackTrace();
		}

	}
	
}
