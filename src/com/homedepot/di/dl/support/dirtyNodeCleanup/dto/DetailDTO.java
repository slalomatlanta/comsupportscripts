package com.homedepot.di.dl.support.dirtyNodeCleanup.dto;

public class DetailDTO {
	private String sku;
	private String node;
	private String api;

	public String getSku() {
		return sku;
	}

	public void setSku(String sku) {
		this.sku = sku;
	}

	public String getNode() {
		return node;
	}

	public void setNode(String node) {
		this.node = node;
	}

	public String getApi() {
		return api;
	}

	public void setApi(String api) {
		this.api = api;
	}

}
