package com.homedepot.di.dl.support.resyncReport.dao;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;

import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage.RecipientType;

import org.apache.log4j.Logger;
import org.joda.time.Chronology;
import org.joda.time.DateTime;
import org.joda.time.chrono.GregorianChronology;

import com.homedepot.di.dl.support.resyncReport.web.COMResyncReportServlet;
import com.homedepot.di.dl.support.util.*;

public class COMResyncReport 
{
	final static Logger logger = Logger.getLogger(COMResyncReport.class);
	public static void main(String[] args) throws SQLException, IOException,
	Exception {
ResyncReport();
}

	public static void ResyncReport() throws SQLException, IOException,
	Exception 
	{
		Calendar cal = Calendar.getInstance();
		
		int lengthOfweek = 7*24*60*60*1000;
		
		Chronology chrono = GregorianChronology.getInstance();
		DateTime dt = new DateTime(2016, 02, 01, 0, 0, 0, 0, chrono);
		long febTime= dt.getMillis(); 
		DateTime dt1 = new DateTime();
		long curTime= dt1.getMillis();
		
		long millis = dt1.getMillis()-dt.getMillis();
		int weeks = (int) (millis/lengthOfweek) + 1;
		
		//System.out.println("current week:" +weeks);
		
		int prevWeek = 0;
		int prevWeek1=0;
		int prevWeek2=0;
		int curWeek=weeks%52;
		//System.out.println(curWeek);
		if (curWeek == 0)
		{
			curWeek=52;
			prevWeek=curWeek-1;
			prevWeek1=curWeek-2;
			prevWeek2=curWeek-3;
		}
		
		else if (curWeek == 1)
		{
			prevWeek=52;
			prevWeek1=51;
			prevWeek2=50;
		}
		else if (curWeek == 2)
		{
			prevWeek=1;
			prevWeek1=52;
			prevWeek2=51;
		}
		else if (curWeek == 3)
		{
			prevWeek=2;
			prevWeek1=1;
			prevWeek2=52;
		}
		
		else 
		{
			prevWeek=curWeek-1;
			prevWeek1=curWeek-2;
			prevWeek2=curWeek-3;
		}
		
		
		//System.out.println("current week:" +prevWeek);
		//System.out.println("current week:" +prevWeek1);
		//System.out.println("current week:" +prevWeek2);
		//System.out.println("Inside DAO");
		ConstantsQuery cons = new ConstantsQuery();
		String storeReviewed = "";
		String fullResync = "";
		String partialUniqueStores = "";
		String partialTotalSku = "";
		String avgOH="";
		
		String storeReviewed1 = "";
		String fullResync1 = "";
		String partialUniqueStores1 = "";
		String partialTotalSku1 = "";
		String avgOH1 ="";
		
		String storeReviewed2 = "";
		String fullResync2 = "";
		String partialUniqueStores2 = "";
		String partialTotalSku2 = "";
		String avgOH2 ="";
		
		String storeReviewed3 = "";
		String fullResync3 = "";
		String partialUniqueStores3 = "";
		String partialTotalSku3 = "";
		String avgOH3 ="";
		
		try
		{
			
			
			
		DBConnectionUtil mySqldbConn = new DBConnectionUtil();
					
					Connection mysql = mySqldbConn.getJNDIConnection(Constants.COM_SUPPORT_MYSQL_JNDI);
		Statement st = null;
		st = mysql.createStatement();
		ResultSet result = null;
		
		result = st.executeQuery(cons.query1);

		while (result.next()) {
			storeReviewed = result.getString(1);

			//System.out.println(storeReviewed);
		}
		
		result = st.executeQuery(cons.query5);

		while (result.next()) {
			avgOH = result.getString(1);

			//System.out.println(avgOH);
		}
		
		result = st.executeQuery(cons.query2);
		while (result.next()) {
			fullResync = result.getString(1);

			//System.out.println(fullResync);
		}
		
		
		result = st.executeQuery(cons.query11);

		while (result.next()) {
			storeReviewed1 = result.getString(1);

			//System.out.println(storeReviewed);
		}
		
		result = st.executeQuery(cons.query51);

		while (result.next()) {
			avgOH1 = result.getString(1);

			//System.out.println(avgOH);
		}
		
		result = st.executeQuery(cons.query21);
		while (result.next()) {
			fullResync1 = result.getString(1);

			//System.out.println(fullResync);
		}
		
		
		result = st.executeQuery(cons.query12);

		while (result.next()) {
			storeReviewed2 = result.getString(1);

			//System.out.println(storeReviewed);
		}
		
		result = st.executeQuery(cons.query52);

		while (result.next()) {
			avgOH2 = result.getString(1);

			//System.out.println(avgOH);
		}
		
		result = st.executeQuery(cons.query22);
		while (result.next()) {
			fullResync2 = result.getString(1);

			//System.out.println(fullResync);
		}


		
		result = st.executeQuery(cons.query13);

		while (result.next()) {
			storeReviewed3 = result.getString(1);

			//System.out.println(storeReviewed);
		}
		
		result = st.executeQuery(cons.query53);

		while (result.next()) {
			avgOH3 = result.getString(1);

			//System.out.println(avgOH);
		}
		
		result = st.executeQuery(cons.query23);
		while (result.next()) {
			fullResync3 = result.getString(1);

			//System.out.println(fullResync);
		}
		result.close();
		st.close();
		mysql.close();
		
		try {
			DBConnectionUtil dbConn = new DBConnectionUtil();
			Connection sscConn = dbConn.getJNDIConnection(Constants.COMT_SSC_RO_JNDI);
			
			
			Statement stmt1 = null;
			stmt1 = sscConn.createStatement();
			ResultSet result1 = null;
			logger.info(cons.query2);
			// String query =
			// "select distinct loc_nbr from thd01.comt_loc_capbl where comt_capbl_id = '10'";
			result1 = stmt1.executeQuery(cons.query3);
			while (result1.next()) {
				partialUniqueStores = result1.getString(1);

				//System.out.println(partialUniqueStores);
			}

			result1 = stmt1.executeQuery(cons.query4);
			while (result1.next()) {
				partialTotalSku = result1.getString(1);

				//System.out.println(partialTotalSku);
			}
			result1 = stmt1.executeQuery(cons.query31);
			while (result1.next()) {
				partialUniqueStores1 = result1.getString(1);

				//System.out.println(partialUniqueStores);
			}

			result1 = stmt1.executeQuery(cons.query41);
			while (result1.next()) {
				partialTotalSku1 = result1.getString(1);

				//System.out.println(partialTotalSku);
			}

			
			result1 = stmt1.executeQuery(cons.query32);
			while (result1.next()) {
				partialUniqueStores2 = result1.getString(1);

				//System.out.println(partialUniqueStores);
			}

			result1 = stmt1.executeQuery(cons.query42);
			while (result1.next()) {
				partialTotalSku2 = result1.getString(1);

				//System.out.println(partialTotalSku);
			}
			
			
			result1 = stmt1.executeQuery(cons.query33);
			while (result1.next()) {
				partialUniqueStores3 = result1.getString(1);

				//System.out.println(partialUniqueStores);
			}

			result1 = stmt1.executeQuery(cons.query43);
			while (result1.next()) {
				partialTotalSku3 = result1.getString(1);

				//System.out.println(partialTotalSku);
			}
			result1.close();
			stmt1.close();
			sscConn.close();

		}

		catch (Exception e) {
			e.printStackTrace();
		}
		String msgContent = "";
		// System.out.println(msgContent);

		msgContent = msgContent
				+ "<!DOCTYPE html>"
				+ "<html>"
				+ "<body>"
				+"<p>Hi All,</p>"
				+"<p>Please find the resync report for the last 4 weeks below.</p>"
				+"<br/>"
				+ "<table border=\"1\" cellpadding=\"0\" cellspacing=\"0\" width=\"200px\" style=\"border-collapse:collapse;\">";
		// "<table border=\"1\" cellpadding=\"0\" cellspacing=\"0\" width=\"200px\" style=\"border-collapse:collapse;\">"
		// ;
		
		msgContent = msgContent + "<tr><th>SUBJECT</th>";
		msgContent = msgContent + "<th>FW#" + 1   + "</th>";
		msgContent = msgContent + "<th>FW#" + 52  + "</th>";
		msgContent = msgContent + "<th>FW#" + 51 + "</th>";
		msgContent = msgContent + "<th>FW#" + 50 +"</th></tr>";
		msgContent = msgContent + "<tr>";
		msgContent = msgContent + "<td>Stores reviewed</td>";
		msgContent = msgContent + "<td>" + storeReviewed + "</td>";
		msgContent = msgContent + "<td>" + storeReviewed1 + "</td>";
		msgContent = msgContent + "<td>" + storeReviewed2 + "</td>";
		msgContent = msgContent + "<td>" + storeReviewed3 + "</td>";
		msgContent = msgContent + "</tr>";
		msgContent = msgContent + "<tr>";
		msgContent = msgContent + "<td>Full Resync (store count)</td>";
		msgContent = msgContent + "<td>" + fullResync + "</td>";
		msgContent = msgContent + "<td>" + fullResync1 + "</td>";
		msgContent = msgContent + "<td>" + fullResync2 + "</td>";
		msgContent = msgContent + "<td>" + fullResync3 + "</td>";
		msgContent = msgContent + "</tr>";
		msgContent = msgContent + "<tr>";
		msgContent = msgContent + "<td>Partial Resync (unique stores)</td>";
		msgContent = msgContent + "<td>" + partialUniqueStores + "</td>";
		msgContent = msgContent + "<td>" + partialUniqueStores1 + "</td>";
		msgContent = msgContent + "<td>" + partialUniqueStores2 + "</td>";
		msgContent = msgContent + "<td>" + partialUniqueStores3 + "</td>";
		msgContent = msgContent + "</tr>";
		msgContent = msgContent + "<tr>";
		msgContent = msgContent + "<td>Partial Resync (total SKUs)</td>";
		msgContent = msgContent + "<td>" + partialTotalSku + "</td>";
		msgContent = msgContent + "<td>" + partialTotalSku1 + "</td>";
		msgContent = msgContent + "<td>" + partialTotalSku2 + "</td>";
		msgContent = msgContent + "<td>" + partialTotalSku3 + "</td>";
		msgContent = msgContent + "</tr>";
		msgContent = msgContent + "<tr>";
		msgContent = msgContent + "<td>Avg. OH Discrepancy</td>";
		msgContent = msgContent + "<td>" + avgOH + "</td>";
		msgContent = msgContent + "<td>" + avgOH1 + "</td>";
		msgContent = msgContent + "<td>" + avgOH2 + "</td>";
		msgContent = msgContent + "<td>" + avgOH3 + "</td>";
		msgContent = msgContent + "</tr>";

		msgContent = msgContent
				+ "</table><br/><br/>Thanks,<br/>COM Multichannel Support";
		//
		sendReport(msgContent);
		
		}
		
		catch(Exception e)
		{
			
		}
		
	
	}
	
	public static void sendReport(String msgContent) {
		final String user = "horizon@cpliisad.homedepot.com";// change
																// accordingly
		final String password = "xxx";// change accordingly

		String host = "mail1.homedepot.com";
		Properties props = new Properties();
		props.put("mail.host", host);
		Session session = Session.getInstance(props,
				new javax.mail.Authenticator() {
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(user, password);
					}
				});

		try {

			String[] to = {"_2fc77b@homedepot.com" };
			String[] Cc = {"JAVIER_A_GIBBS@homedepot.com","ELIZABETH_A_DIXON@homedepot.com"};
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress(user));
			DateFormat dateFormat = new SimpleDateFormat(
					"MM/dd/yyyy hh:mm:ss a");
			// get current date time with Date()
			Date today = new Date();
			String date1 = dateFormat.format(today);
			message.setSubject("Resync Report - " + date1);
			// message.setText("This is simple program of sending email using JavaMail API");
			message.setFrom(new InternetAddress(user));
			InternetAddress[] addressTo = new InternetAddress[to.length];
			InternetAddress[] addressCc = new InternetAddress[Cc.length];
			for (int i = 0; i < to.length; i++) {
				addressTo[i] = new InternetAddress(to[i]);
				//System.out.println("To Address " + to[i]);
			}
			for (int j = 0; j < Cc.length; j++) {
				addressCc[j] = new InternetAddress(Cc[j]);
				//System.out.println("Cc Address " + Cc[j]);
			}
			message.setRecipients(RecipientType.TO, addressTo);
			message.setRecipients(RecipientType.CC, addressCc);
			message.setContent(msgContent, "text/html");
			Transport.send(message);
			for (int i = 0; i < to.length; i++) {
				addressTo[i] = new InternetAddress(to[i]);
				//System.out.println("Message delivered to--- " + to[i]);
			}
			/*for (int j = 0; j < Cc.length; j++) {
				addressCc[j] = new InternetAddress(Cc[j]);
				System.out.println("Message delivered to cc recepients--- "
						+ Cc[j]);
			}*/

		} catch (MessagingException e) {
			e.printStackTrace();
		}

	}

}
