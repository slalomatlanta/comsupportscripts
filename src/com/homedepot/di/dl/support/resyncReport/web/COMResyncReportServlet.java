package com.homedepot.di.dl.support.resyncReport.web;

import java.io.IOException;
import java.sql.SQLException;
import java.util.GregorianCalendar;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.joda.time.Chronology;
import org.joda.time.DateTime;
import org.joda.time.chrono.GregorianChronology;

import com.homedepot.di.dl.support.ErrorDeltas.web.DeltaServlet;
import com.homedepot.di.dl.support.resyncReport.dao.COMResyncReport;


/**
 * Servlet implementation class COMResyncReportServlet
 */
public class COMResyncReportServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	final static Logger logger = Logger.getLogger(COMResyncReportServlet.class);  
    /**
     * @see HttpServlet#HttpServlet()
     */
    public COMResyncReportServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//COMResyncReportDAO resyncReport= new COMResyncReportDAO(); 
	
		logger.info("COMResyncReportServlet Started : "+GregorianCalendar.getInstance().getTime());
		
		
		try {
			COMResyncReport.ResyncReport();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.debug("COMResyncReportServlet::"+GregorianCalendar.getInstance().getTime()+e);
		}
		logger.info("COMResyncReportServlet Completed : "+GregorianCalendar.getInstance().getTime());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
