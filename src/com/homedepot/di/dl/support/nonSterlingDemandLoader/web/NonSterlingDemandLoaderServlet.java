package com.homedepot.di.dl.support.nonSterlingDemandLoader.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.mail.Session;
import javax.mail.MessagingException;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage.RecipientType;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.xerces.parsers.DOMParser;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.homedepot.di.dl.support.nonSterlingDemandLoader.dao.NonSterlingDemandLoader;


/**
 * Servlet implementation class NonSterlingDemandLoaderServlet
 */
public class NonSterlingDemandLoaderServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public NonSterlingDemandLoaderServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		long start = System.currentTimeMillis();
		
		response.setContentType("text/html");
		PrintWriter out = null;
		out = response.getWriter();
		
		//call ComNonSterlingDemandLoader Service
		String res = NonSterlingDemandLoader.getDetails();

		//parse xml output response
		DOMParser parser = new DOMParser();
		try {
			parser.parse(new InputSource(new java.io.StringReader(res)));
			Document doc = parser.getDocument();
			Element rootNode = doc.getDocumentElement();
			NodeList nodeList = rootNode.getChildNodes();
			if((nodeList.getLength()<=1) && rootNode.getNodeType() == Node.ELEMENT_NODE){
				out.println("NonSterlingDemandLoaderServlet: No Jobs are currently running.");
				System.out.println("NonSterlingDemandLoaderServlet: No Jobs are currently running.");
			}

			else{
				long mins =0;
				List diffMins = new ArrayList();
				//List l = new ArrayList();
				StringBuilder result = new StringBuilder();
				result.append("<h3>Alert: NonSterlingDemandLoader Jobs running over 20 minutes</h3> ");
				result.append("\n");
				result.append("\n");
				result.append("<h4>Below are the job details:</h4> ");
				result.append("\n");
				result.append("\n");
				result.append("<html>");
				result.append("<body>");
				result.append("<table BORDER=1 CELLPADDING=0 CELLSPACING=0 WIDTH=60% >");
				result.append("<tr>");
				result.append("<th>");
				result.append("Job ID");
				result.append("</th>");
				result.append("<th>");
				result.append("Status");
				result.append("</th>");
				result.append("<th>");
				result.append("Creation Time");
				result.append("</th>");
				result.append("<th>");
				result.append("Diff in Minutes");
				result.append("</th>");
				result.append("</tr>\n");
				for (int i = 0; i < nodeList.getLength(); i++) {
					Node currentNode = nodeList.item(i);
					List<String> list = new ArrayList<String>();
					if (currentNode.getNodeType() == Node.ELEMENT_NODE) {
						NamedNodeMap map= currentNode.getAttributes();
						if((map.getNamedItem("status").toString().contains("RUNNING"))||(map.getNamedItem("status").toString().contains("WAIT"))){
							String creationtime = (map.getNamedItem("creationTime").getNodeValue());
							Timestamp jobcreationTimeStamp = Timestamp.valueOf(creationtime);
							//System time
							Date date=new Date();
							Timestamp SystemTimeStamp = new Timestamp(date.getTime());
							//compare job creation time stamp with System time stamp
							long diff = SystemTimeStamp.getTime()-jobcreationTimeStamp.getTime();
							long seconds = diff/1000;
							mins = seconds/60;
							String min =  Long.toString(mins);
							list.add(map.getNamedItem("id").getNodeValue());
							list.add(map.getNamedItem("status").getNodeValue());
							list.add(map.getNamedItem("creationTime").getNodeValue());
							if (mins > 20) {    
								diffMins.add(mins);
								list.add(min);
								result.append("<tr>");
								for(int k=0; k<list.size(); k++) {
									String str = list.get(k);
									result.append("<td>");
									result.append(str);
									result.append("</td>");
								}
								result.append("</tr>\n");
							}
						}
					}
				}
				result.append("</table>");
				result.append("</body>");
				result.append("</html>");

				if(diffMins.size() <= 0) {
					out.println("NonSterlingDemandLoaderServlet: No jobs running over 20 minutes.");
					System.out.println("NonSterlingDemandLoaderServlet: No jobs running over 20 minutes.");
				}
				//Call sendMail method to Send eMail
				if((diffMins.size()) > 0){
					out.println(result.toString());
					System.out.println(result.toString());
					sendMail(result.toString());
				}	
			}
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		long elapsedTime = System.currentTimeMillis() - start;
		
		long seconds = (long) (elapsedTime / 1000) ;
		long minutes = (long) (elapsedTime / (1000*60));

		System.out.println("NonSterlingDemandLoaderServlet execution time is: " + minutes + " minutes and " + seconds  + " seconds.");
		
		System.out.println("NonSterlingDemandLoaderServlet ran successfully");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	
	   public static void sendMail(String bodyDetails) {

		    Session session = null;
			Properties props = null;
			
			final String user = "horizon@cpliisad.homedepot.com";

			String host = "mail1.homedepot.com";
			
			//_2fc77b@homedepot.com -- Com Support multi channel
			String[] to = { "_2fc77b@homedepot.com"};
			String subject = "CRITICAL ALERT: NonSterlingDemandLoader ";

			try {
				
				props = System.getProperties();
				props.put("mail.host", host);
				session = Session.getInstance(props, null);

			
				MimeMessage message = new MimeMessage(session);
				message.setFrom(new InternetAddress(user));
				DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a");
						
				// get current date time with Date()
				Date today = new Date();
				String date1 = dateFormat.format(today);
				message.setSubject(subject + " - " + date1);
		
				message.setFrom(new InternetAddress(user));
				InternetAddress[] addressTo = new InternetAddress[to.length];

				for (int i = 0; i < to.length; i++) {
					addressTo[i] = new InternetAddress(to[i]);
				}

				message.setRecipients(RecipientType.TO, addressTo);
				message.setContent(bodyDetails, "text/html");
				Transport.send(message);
				for (int i = 0; i < to.length; i++) {
					addressTo[i] = new InternetAddress(to[i]);
					System.out.println("Message delivered to--- " + to[i]);
				}

			} catch (MessagingException e) {
				e.printStackTrace();
			}

		}
}
