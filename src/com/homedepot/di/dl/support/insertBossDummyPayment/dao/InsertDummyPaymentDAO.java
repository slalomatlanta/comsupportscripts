package com.homedepot.di.dl.support.insertBossDummyPayment.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import org.apache.log4j.Logger;

import com.homedepot.di.dl.support.compareDataReplication.web.CompareDataReplicationServlet;
import com.homedepot.di.dl.support.insertBossDummyPayment.dto.OrderDTO;
import com.homedepot.di.dl.support.util.Constants;
import com.homedepot.di.dl.support.util.DBConnectionUtil;

public class InsertDummyPaymentDAO {
	
	final static Logger logger = Logger.getLogger(InsertDummyPaymentDAO.class);
	
	public static OrderDTO getOrderDetails(OrderDTO od){
		Statement stmt = null;
		ResultSet rset = null;
		boolean isOrderPresent = false;
		try {
			DBConnectionUtil dbConn = new DBConnectionUtil();
			Connection con = dbConn
					.getJNDIConnection(Constants.STERLING_SSC_RO_JNDI);
			
			stmt = con.createStatement();
			String query = "select extn_host_order_ref, extn_host_order_line_ref, extn_po_number, extn_vendor_stock_number, extn_po_line_reference,extn_sku_code,line_total,shipnode_key, createts, ordered_qty "
					+ "from ( "
					+ "    select yoh.extn_host_order_ref, so.extn_host_order_line_ref, yoh.extn_po_number, ej.hd_ej_key, so.extn_vendor_stock_number, po.extn_po_line_reference,so.line_total, so.extn_sku_code, so.shipnode_key, so.createts, so.ordered_qty "
					+ "    from thd01.yfs_order_header yoh, thd01.yfs_order_line po, thd01.yfs_order_release_status yors, thd01.yfs_order_line so "
					+ "    left join thd01.hd_related_e_j_keys ej on so.order_line_key=ej.order_line_key "
					+ "    where yoh.document_type='0005'  "
					+ "          and yoh.order_header_key=po.order_header_key "
					+ "          and po.derived_from_order_line_key=so.order_line_key "
					+ "          and so.extn_create_src_process in ('9','109') "
					+ "          and so.line_type='SO' "
					+ "          and so.SHIPNODE_KEY != '8119' "
					+ "          and so.line_total > 0 "
					+ "          and (yoh.extn_host_order_ref, so.extn_host_order_line_ref) in (('"+od.getOrderRef().trim()+"','"+od.getLineRef().trim()+"')) "
					+ "          and so.order_header_key=yors.order_header_key "
					+ "          and so.order_line_key=yors.order_line_key "
					+ "          and yors.status_quantity>0 "
					+ "          and yors.status not in ('9000','3700.02','3700.01')) "
					+ "where hd_ej_key is null ";
			rset = stmt.executeQuery(query);
			while (rset.next()) {
				isOrderPresent = true;
				od.setIsOrderPresent("Y");
				
				String poNum = rset.getString(3);
				if (poNum.length() == 8) {
					od.setPoNumber(poNum);
				} else {
					od.setPoNumber("0" + poNum);
				}
				
				od.setVendorStockNumber(rset.getString(4));
				od.setPoLineRef(rset.getString(5));
				od.setSku(rset.getString(6));
				od.setLineTotal(rset.getFloat(7));
				
				String store = rset.getString(8);
				if ((store.trim()).length() == 4) {
					od.setShipNode(store);
				} else {
					od.setShipNode("0" + store);
				}
				
				od.setCreateTs(rset.getString(9));
				od.setOrderedQty(rset.getInt(10));
			}
			rset.close();
			stmt.close();
			
			if(!isOrderPresent){
				stmt = null;
				rset = null;
				stmt = con.createStatement();
				query = "select extn_host_order_ref, extn_host_order_line_ref, extn_po_number, extn_vendor_stock_number, extn_po_line_reference,extn_sku_code,line_total,shipnode_key, createts, ordered_qty "
						+ "from ( "
						+ "    select yoh.extn_host_order_ref, so.extn_host_order_line_ref, yoh.extn_po_number, ej.hd_ej_key, so.extn_vendor_stock_number, po.extn_po_line_reference,so.line_total, so.extn_sku_code, so.shipnode_key, so.createts, so.ordered_qty "
						+ "    from thd01.yfs_order_header_h yoh, thd01.yfs_order_line_h po, thd01.yfs_order_release_status yors, thd01.yfs_order_line so "
						+ "    left join thd01.hd_related_e_j_keys ej on so.order_line_key=ej.order_line_key "
						+ "    where yoh.document_type='0005'  "
						+ "          and yoh.order_header_key=po.order_header_key "
						+ "          and po.derived_from_order_line_key=so.order_line_key "
						+ "          and so.extn_create_src_process in ('9','109') "
						+ "          and so.line_type='SO' "
						+ "          and so.SHIPNODE_KEY != '8119' "
						+ "          and so.line_total > 0 "
						+ "          and (yoh.extn_host_order_ref, so.extn_host_order_line_ref) in (('"+od.getOrderRef().trim()+"','"+od.getLineRef().trim()+"')) "
						+ "          and so.order_header_key=yors.order_header_key "
						+ "          and so.order_line_key=yors.order_line_key "
						+ "          and yors.status_quantity>0 "
						+ "          and yors.status not in ('9000','3700.02','3700.01')) "
						+ "where hd_ej_key is null ";
				rset = stmt.executeQuery(query);
				while (rset.next()) {
					isOrderPresent = true;
					od.setIsOrderPresent("Y");
					
					String poNum = rset.getString(3);
					if (poNum.length() == 8) {
						od.setPoNumber(poNum);
					} else {
						od.setPoNumber("0" + poNum);
					}
					
					od.setVendorStockNumber(rset.getString(4));
					od.setPoLineRef(rset.getString(5));
					od.setSku(rset.getString(6));
					od.setLineTotal(rset.getFloat(7));
					
					String store = rset.getString(8);
					if ((store.trim()).length() == 4) {
						od.setShipNode(store);
					} else {
						od.setShipNode("0" + store);
					}
					
					od.setCreateTs(rset.getString(9));
					od.setOrderedQty(rset.getInt(10));
				}
				rset.close();
				stmt.close();
			}
			
			if(!isOrderPresent){
				stmt = null;
				rset = null;
				stmt = con.createStatement();
				query = "select extn_host_order_ref, extn_host_order_line_ref, extn_po_number, extn_vendor_stock_number, extn_po_line_reference,extn_sku_code,line_total,shipnode_key, createts, ordered_qty "
						+ "from ( "
						+ "    select yoh.extn_host_order_ref, so.extn_host_order_line_ref, yoh.extn_po_number, ej.hd_ej_key, so.extn_vendor_stock_number, po.extn_po_line_reference,so.line_total, so.extn_sku_code, so.shipnode_key, so.createts, so.ordered_qty "
						+ "    from thd01.yfs_order_header_h yoh, thd01.yfs_order_line_h po, thd01.yfs_order_release_status_h yors, thd01.yfs_order_line_h so "
						+ "    left join thd01.hd_related_e_j_keys ej on so.order_line_key=ej.order_line_key "
						+ "    where yoh.document_type='0005'  "
						+ "          and yoh.order_header_key=po.order_header_key "
						+ "          and po.derived_from_order_line_key=so.order_line_key "
						+ "          and so.extn_create_src_process in ('9','109') "
						+ "          and so.line_type='SO' "
						+ "          and so.SHIPNODE_KEY != '8119' "
						+ "          and so.line_total > 0 "
						+ "          and (yoh.extn_host_order_ref, so.extn_host_order_line_ref) in (('"+od.getOrderRef().trim()+"','"+od.getLineRef().trim()+"')) "
						+ "          and so.order_header_key=yors.order_header_key "
						+ "          and so.order_line_key=yors.order_line_key "
						+ "          and yors.status_quantity>0 "
						+ "          and yors.status not in ('9000','3700.02','3700.01')) "
						+ "where hd_ej_key is null ";
				rset = stmt.executeQuery(query);
				while (rset.next()) {
					isOrderPresent = true;
					od.setIsOrderPresent("Y");
					
					String poNum = rset.getString(3);
					if (poNum.length() == 8) {
						od.setPoNumber(poNum);
					} else {
						od.setPoNumber("0" + poNum);
					}
					
					od.setVendorStockNumber(rset.getString(4));
					od.setPoLineRef(rset.getString(5));
					od.setSku(rset.getString(6));
					od.setLineTotal(rset.getFloat(7));
					
					String store = rset.getString(8);
					if ((store.trim()).length() == 4) {
						od.setShipNode(store);
					} else {
						od.setShipNode("0" + store);
					}
					
					od.setCreateTs(rset.getString(9));
					od.setOrderedQty(rset.getInt(10));
				}
				rset.close();
				stmt.close();
			}
			
			con.close();
			
			if(!isOrderPresent){
				od.setIsOrderPresent("N");
			}
			
		} catch (Exception e) {
			logger.error("Exception Occurred: " + e.getStackTrace().toString());
		}
		return od;
	}

}
