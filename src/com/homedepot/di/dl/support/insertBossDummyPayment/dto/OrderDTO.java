package com.homedepot.di.dl.support.insertBossDummyPayment.dto;

public class OrderDTO {
	private String orderRef;
	private String lineRef;
	private String poNumber;
	private String vendorStockNumber;
	private String poLineRef;
	private String sku;
	private float lineTotal;
	private String shipNode;
	private String createTs;
	private int orderedQty;
	private String isQueuePutSuccess;
	private String comment;
	private String isOrderPresent;
	
	
	
	public String getIsOrderPresent() {
		return isOrderPresent;
	}
	public void setIsOrderPresent(String isOrderPresent) {
		this.isOrderPresent = isOrderPresent;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getIsQueuePutSuccess() {
		return isQueuePutSuccess;
	}
	public void setIsQueuePutSuccess(String isQueuePutSuccess) {
		this.isQueuePutSuccess = isQueuePutSuccess;
	}
	public String getOrderRef() {
		return orderRef;
	}
	public void setOrderRef(String orderRef) {
		this.orderRef = orderRef;
	}
	public String getLineRef() {
		return lineRef;
	}
	public void setLineRef(String lineRef) {
		this.lineRef = lineRef;
	}
	public String getPoNumber() {
		return poNumber;
	}
	public void setPoNumber(String poNumber) {
		this.poNumber = poNumber;
	}
	public String getVendorStockNumber() {
		return vendorStockNumber;
	}
	public void setVendorStockNumber(String vendorStockNumber) {
		this.vendorStockNumber = vendorStockNumber;
	}
	public String getPoLineRef() {
		return poLineRef;
	}
	public void setPoLineRef(String poLineRef) {
		this.poLineRef = poLineRef;
	}
	public String getSku() {
		return sku;
	}
	public void setSku(String sku) {
		this.sku = sku;
	}
	public String getShipNode() {
		return shipNode;
	}
	public void setShipNode(String shipNode) {
		this.shipNode = shipNode;
	}
	public String getCreateTs() {
		return createTs;
	}
	public void setCreateTs(String createTs) {
		this.createTs = createTs;
	}
	public int getOrderedQty() {
		return orderedQty;
	}
	public void setOrderedQty(int orderedQty) {
		this.orderedQty = orderedQty;
	}
	public float getLineTotal() {
		return lineTotal;
	}
	public void setLineTotal(float lineTotal) {
		this.lineTotal = lineTotal;
	}
	
	
}