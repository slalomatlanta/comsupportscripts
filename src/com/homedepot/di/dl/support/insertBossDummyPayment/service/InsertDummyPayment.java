package com.homedepot.di.dl.support.insertBossDummyPayment.service;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.apache.log4j.Logger;

import au.com.bytecode.opencsv.CSVReader;

import com.homedepot.di.dl.support.insertBossDummyPayment.dao.InsertDummyPaymentDAO;
import com.homedepot.di.dl.support.insertBossDummyPayment.dto.OrderDTO;

public class InsertDummyPayment {
	private static final Logger logger = Logger
			.getLogger(InsertDummyPayment.class);

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		logger.debug("InsertDummyPayment Program started");
		processPayment();
	}

	public static void processPayment() {
		Date today = new Date();
		long sysdateInLongMillis = today.getTime();
		String fileLocation = "C:/test/DummyPayment.csv";
		String outputFileLocation = "C:/test/DummyPayment_"+sysdateInLongMillis+".csv";
		ArrayList<OrderDTO> orderList = readInputSpreadSheet(fileLocation);
		for (OrderDTO od : orderList) {
			od = InsertDummyPaymentDAO.getOrderDetails(od); // Getting details
															// from sterling
															// database
			if (od.getIsOrderPresent().equalsIgnoreCase("Y")) {				
				String dummyEJXML = createEJXML(od);
				logger.debug(dummyEJXML);
				String isQueuePutSuccess = putMessageInQueue(dummyEJXML);
				od.setIsQueuePutSuccess(isQueuePutSuccess);
				if (isQueuePutSuccess.equalsIgnoreCase("Y")) {
					od.setComment("Queue Put is successful");
					logger.debug(od.getOrderRef() + " - " + od.getLineRef()
							+ " - Queue Put is successful");
				} else {
					od.setComment("Queue Put failed");
					logger.debug(od.getOrderRef() + " - " + od.getLineRef()
							+ " - Queue Put is NOT success");
				}
			}
			else if (od.getIsOrderPresent().equalsIgnoreCase("N")){
				od.setComment("Orderline not eligible or not present in database");
			}
			else{
				od.setComment("Error");
			}
		}
		
		generateReport(orderList, outputFileLocation);
	}

	public static String createEJXML(OrderDTO od) {		
		Date curDate = new Date();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		SimpleDateFormat salesDtFormat = new SimpleDateFormat("yyyy-MM-dd");
		String sysdate = format.format(curDate);
		String salesDate = salesDtFormat.format(curDate);
		// ExtnSuppressFI=\"Y\" ExtnPickCompleted=\"Y\"
		String ejXML = "<Orders ExtnMsgGeneratedTS=\""
				+ sysdate
				+ "\"><Order DocumentType=\"0001\" EnterpriseCode=\"HDUS\">"
				+ "<Instructions>			"
				+ "<Instruction InstructionText=\"The customer was charged for the open balance on this order by the Call Center team. A payment has been inserted on the order using 88 8888 to clear the balance and release the will call.\" InstructionType=\"F\" Createuserid=\"COMSupport\"> "
				+ "<Extn ExtnIsTicklerBin=\"Y\" ExtnStore=\"8119\" ExtnTimeStamp=\""
				+ sysdate
				+ "\" ExtnUserID=\"COMSupport\" ExtnFulfillingStore=\"8119\" ExtnCrtModID=\"BOSSMissingPayment\" ExtnAssgAssocUserId=\"BOSSMissingPayment\" ExtnCloseUserID=\"COMSupport\"/> "
				+ "</Instruction>"
				+ "</Instructions> "
				+ "<PaymentMethods><PaymentMethod PaymentReference1=\""
				+ od.getOrderRef().trim()
				+ "\" PaymentType=\"MC\"><PaymentDetailsList><PaymentDetails ChargeType=\"CHARGE\" EJDate=\""
				+ salesDate
				+ "\" EJRegister=\"88\" EJSeqNo=\"8888\" EJStore=\""
				+ od.getShipNode().trim()
				+ "\"/></PaymentDetailsList></PaymentMethod></PaymentMethods><Extn ExtnDerivedTransactionType=\"ORDER_MODIFY\" ExtnHostOrderReference=\""
				+ od.getOrderRef().trim()
				+ "\" ExtnHostOrderSystemIdentifier=\"10\"/><OrderLines><OrderLine Action=\"Manage\" OrderedQty=\""
				+ od.getOrderedQty()
				+ "\" ShipNode=\""
				+ od.getShipNode().trim()
				+ "\"><Extn ExtnHostOrderLineReference=\""
				+ od.getLineRef().trim()
				+ "\" ExtnSKUCode=\""
				+ od.getSku().trim()
				+ "\" ExtnSellingLocation=\"8119\" ExtnVendorStockNumber=\""
				+ od.getVendorStockNumber().trim()
				// SupressFI flag comes here ExtnSuppressFI=\"Y\"
				// ExtnPickCompleted=\"Y\"
				+ "\" ExtnSuppressFI=\"Y\" ExtnPickCompleted=\"Y\" /></OrderLine></OrderLines></Order><Order BuyerOrganizationCode=\"HDUS\" DocumentType=\"0005\" EnterpriseCode=\"HDUS\" ><Extn ExtnDerivedTransactionType=\"ORDER_MODIFY\" ExtnHostOrderReference=\""
				+ od.getOrderRef().trim()
				+ "\" ExtnHostOrderSystemIdentifier=\"10\" ExtnPONumber=\""
				+ od.getPoNumber().trim()
				+ "\"><HDRelatedEJKeysList><HDRelatedEJKeys EJDate=\""
				+ salesDate
				+ "\" EJRegister=\"88\" EJSeqNo=\"8888\" EJStore=\""
				+ od.getShipNode().trim()
				+ "\"/></HDRelatedEJKeysList></Extn><OrderLines><OrderLine LineStatus=\"Shipped\" OrderedQty=\""
				+ od.getOrderedQty()
				+ "\" ReceivingNode=\""
				+ od.getShipNode().trim()
				+ "\"><Extn ExtnHostOrderLineReference=\""
				+ od.getLineRef().trim()
				+ "\" ExtnPOLineReference=\""
				+ od.getPoLineRef().trim()
				+ "\" ExtnSKUCode=\""
				+ od.getSku().trim()
				+ "\" ExtnVendorStockNumber=\""
				+ od.getVendorStockNumber().trim()
				+ "\"/><Item ProductClass=\"GOOD\"/></OrderLine></OrderLines></Order></Orders>";

		return ejXML;
	}

	public static String putMessageInQueue(String dummyEJXML) {
		String isQueuePutSuccess = "N";
		JmsDestination destination = null;
		destination = new JmsDestination("jms/MQPutQCF_Str",
				"jms/DI.DL.STERLING.CHANGEORDER_put");
		try {
			destination.open();
			destination.send(dummyEJXML);
			isQueuePutSuccess = "Y";
			destination.close();
		} catch (ProcessingException e) {
			logger.debug("Caught exception while putting into queue");
		}
		return isQueuePutSuccess;
	}

	public static ArrayList<OrderDTO> readInputSpreadSheet(String fileLocation) {
		ArrayList<OrderDTO> orderList = new ArrayList<OrderDTO>();
		String[] row = null;
		CSVReader csvReader = null;
		try {
			csvReader = new CSVReader(new FileReader(fileLocation));
			while ((row = csvReader.readNext()) != null) {
					OrderDTO od = new OrderDTO();
					od.setOrderRef(row[0]);
					od.setLineRef(row[1]);
					orderList.add(od);
			}
			csvReader.close();
		} catch (FileNotFoundException e) {
			return orderList;
		} catch (IOException e) {
			return orderList;
		} catch (Exception e) {
			return orderList;
		}
		return orderList;
	}
	
	public static void generateReport(ArrayList<OrderDTO> orderList, String outputFileLocation){
		try{
			FileWriter writer = new FileWriter(outputFileLocation);
			
			writer.append("Order Number");
			writer.append(",");
			writer.append("Line Number");
			writer.append(",");
			writer.append("Comments");
			writer.append(",");
			writer.append("\n");
			
			for (OrderDTO od : orderList){
				writer.append(od.getOrderRef());
				writer.append(",");
				writer.append(od.getLineRef());
				writer.append(",");
				writer.append(od.getComment());
				writer.append(",");
				writer.append("\n");
			}
			
			writer.flush();
			writer.close();
			logger.debug("File created at " + outputFileLocation);
			
		}catch (Exception e){
			logger.debug("Exception Occurred while generating report : "+e);
			e.printStackTrace();
		}
		
	}

}
