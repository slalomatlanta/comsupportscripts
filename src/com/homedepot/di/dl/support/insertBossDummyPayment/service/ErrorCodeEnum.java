package com.homedepot.di.dl.support.insertBossDummyPayment.service;

public enum ErrorCodeEnum {

	JMS_ERROR                          ( -5, "JMS Error"),
	DATABASE_ERROR                     ( -2, "Database Error"),
	APPLICATION_ERROR                  ( -1, "Application Error"),
	XSLT_ERROR						   ( -3, "XSLT Error"),
	INVALID_ARGUMENT				   ( -4, "Invalid Argument Passed"),

	SUCCESS                            (  0, "Request was processed successfully"),
	CUSTOMER_ORDER_ID_REQUIRED         (  1, "/Order/Extn/@ExtnHostOrderReference is required"),
	NO_ORDER_LINES					   (  2, "/Order/OrderLines/OrderLine is required" ),
	INVALID_DATE_FORMAT				   (  3, "//OrderLine/@ReqShipDate"),
	SHIPNODE_REQUIRED                  (  4, "//OrderLine/@ShipNode"   ),
	LINETYPE_REQUIRED				   (  5, "//OrderLine/@LineType"   ),
	;
	
	private int code;
	private String description;
	
	private ErrorCodeEnum(int code, String description) {
		this.code = code;
		this.description = description;
	}
	
	public int getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}

}
