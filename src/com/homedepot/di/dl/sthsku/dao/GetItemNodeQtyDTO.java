package com.homedepot.di.dl.sthsku.dao;

public class GetItemNodeQtyDTO {
	
	String invItemKey = null;
	String item = null;
	String shipNode = null;
	int qty = 0;
	String uom = null;
	String prodClass = null;
	
	
	public String getUom() {
		return uom;
	}
	public void setUom(String uom) {
		this.uom = uom;
	}
	public String getProdClass() {
		return prodClass;
	}
	public void setProdClass(String prodClass) {
		this.prodClass = prodClass;
	}
	public String getInvItemKey() {
		return invItemKey;
	}
	public void setInvItemKey(String invItemKey) {
		this.invItemKey = invItemKey;
	}
	public String getItem() {
		return item;
	}
	public void setItem(String item) {
		this.item = item;
	}
	public String getShipNode() {
		return shipNode;
	}
	public void setShipNode(String shipNode) {
		this.shipNode = shipNode;
	}
	public int getQty() {
		return qty;
	}
	public void setQty(int qty) {
		this.qty = qty;
	}
	
	
}
