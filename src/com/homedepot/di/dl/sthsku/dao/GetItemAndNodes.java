package com.homedepot.di.dl.sthsku.dao;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import au.com.bytecode.opencsv.CSVReader;

public class GetItemAndNodes {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
//		String fileLocation = "C:\\test\\ItemShipnodeQty.csv";
		String outputFileLocation = "C:\\test\\AdjWithSameQty.csv";
		List<GetItemNodeQtyDTO> itemNodeQtyList = new ArrayList<GetItemNodeQtyDTO>();
		List<GetItemNodeQtyDTO> finalIemNodeQtyList = new ArrayList<GetItemNodeQtyDTO>();
		
		try{
		
//		itemNodeQtyList = readFromCSV(fileLocation); //if we want to feed input from input file
		itemNodeQtyList  = getItemsWithZeroQtyFromSterling();
		finalIemNodeQtyList = compareWithCOMDB(itemNodeQtyList);
		for (GetItemNodeQtyDTO dto : finalIemNodeQtyList) {
			System.out.println(dto.getInvItemKey() + "   "
					+ dto.getItem() + "   " + dto.getShipNode() + "   "
					+ dto.getShipNode().replace("MVNDR-", "") + "   "
					+ dto.getQty());
		}
		writeToCSV(outputFileLocation, finalIemNodeQtyList);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	

	public static List<GetItemNodeQtyDTO> readFromCSV(String fileLocation) {

		// logger.info("readFromCSV() invoked");

		List<GetItemNodeQtyDTO> itemNodeQtyList = new ArrayList<GetItemNodeQtyDTO>();
		String[] row = null;
		CSVReader csvReader = null;
		boolean headerRow = true;
		try {
			csvReader = new CSVReader(new FileReader(fileLocation));
			while ((row = csvReader.readNext()) != null) {
				if (headerRow) {
					headerRow = false;
				} else {
					GetItemNodeQtyDTO dto = new GetItemNodeQtyDTO();
					dto.setInvItemKey(row[0].trim());
					dto.setItem(row[1].trim());
					dto.setShipNode(row[2].trim());
					dto.setQty(Math.abs(Integer.parseInt(row[3])));
					dto.setUom(row[4].trim());
					dto.setProdClass(row[5].trim());
//					System.out.println(dto.getInvItemKey() + "   "
//							+ dto.getItem() + "   " + dto.getShipNode() + "   "
//							+ dto.getShipNode().replace("MVNDR-", "") + "   "
//							+ dto.getQty());
					itemNodeQtyList.add(dto);
				}
			}
			csvReader.close();
		} catch (Exception e) {
			e.getStackTrace();
			// logger.debug("Exception Occured " + e.getStackTrace());
		}
		System.out.println(itemNodeQtyList.size());
		return itemNodeQtyList;
	}

	public static List<GetItemNodeQtyDTO> compareWithCOMDB(
			List<GetItemNodeQtyDTO> itemNodeQtyList) {

		// logger.info("readFromCSV() invoked");

		List<GetItemNodeQtyDTO> finalIemNodeQtyList = new ArrayList<GetItemNodeQtyDTO>();

		try {

			for (GetItemNodeQtyDTO dto : itemNodeQtyList) {
				
				if (checkIfSameAdjQty(dto)) {
					finalIemNodeQtyList.add(dto);
				}
				
			}
		} catch (Exception e) {
			e.getStackTrace();
			// logger.debug("Exception Occured " + e.getStackTrace());
		}
		return finalIemNodeQtyList;
	}

	public static boolean checkIfSameAdjQty(GetItemNodeQtyDTO dto)
			throws SQLException, ClassNotFoundException {
		boolean compare = false;
		int comAdjQty = 0;
		final String driverClass = "oracle.jdbc.driver.OracleDriver";
		final String connectionURL = "jdbc:oracle:thin:@//spragor10-scan.homedepot.com:1521/dpr78mm_sro01";
		final String uName = "MMUSR01";
		final String uPassword = "COMS_MMUSR01";
		final String query = "select x.inv_adj_qty from thd01.stg_comt_pvndr_inv x, THD01.comt_mvndr_inv_msg z, "
				+ "(select a.mer_item_nbr, b.inv_adj_trstat_cd, max(b.trnsm_subm_ts) max_trnsm_subm_ts from THD01.comt_mvndr_inv_msg a, THD01.stg_comt_pvndr_inv b "
				+ "where a.file_nm=b.file_nm and a.line_nbr=b.line_nbr and a.mer_item_nbr='"
				+ dto.getItem()
				+ "' and a.mvndr_nbr='"
				+ dto.getShipNode().replace("MVNDR-", "")
				+ "' and b.trnsm_subm_ts > sysdate -15 "
				+ "group by a.mer_item_nbr, b.inv_adj_trstat_cd) y "
				+ "where x.trnsm_subm_ts = y.max_trnsm_subm_ts and x.file_nm=z.file_nm and x.line_nbr=z.line_nbr and z.mer_item_nbr='"
				+ dto.getItem()
				+ "' and z.mvndr_nbr='"
				+ dto.getShipNode().replace("MVNDR-", "") + "'";
		try {
			Connection con = null;
			con = DriverManager.getConnection(connectionURL, uName, uPassword);
			Statement stmt = null;
			stmt = con.createStatement();
			ResultSet result = null;
			result = stmt.executeQuery(query);
			while (result.next()) {
				comAdjQty = result.getInt(1);
			}
			stmt.close();
			result.close();
			con.close();
			System.out.println(comAdjQty+"   "+dto.getQty());
			if (comAdjQty == dto.getQty()) {
				compare = true;
			}

			// writer.append("Sterling Connection closed");
			// writer.append("Program End Time:"+dateFormat.format(date));
		} catch (SQLException e1) {
			e1.printStackTrace();
			System.out.println(e1.getMessage());
		}
		return compare;
	}
	
	public static void writeToCSV(String fileLocation,
			List<GetItemNodeQtyDTO> finalIemNodeQtyList) {


		FileWriter writer;
		try {
			writer = new FileWriter(fileLocation);

			writer.append("INVENTORY_ITEM_KEY");
			writer.append(",");
			writer.append("ITEM_ID");
			writer.append(",");
			writer.append("SHIP_NODE");
			writer.append(",");
			writer.append("QUANTITY");
			writer.append(",");
			writer.append("UOM");
			writer.append(",");
			writer.append("PRODUCT_CLASS");
			writer.append("\n");

			for (int i = 0; i < finalIemNodeQtyList.size(); i++) {
				GetItemNodeQtyDTO dto = new GetItemNodeQtyDTO();
				dto = finalIemNodeQtyList.get(i);
				writer.append(dto.getInvItemKey());
				writer.append(",");
				writer.append(dto.getItem());
				writer.append(",");
				writer.append(dto.getShipNode());
				writer.append(",");
				writer.append(dto.getQty() + "");
				writer.append(",");
				writer.append(dto.getUom());
				writer.append(",");
				writer.append(dto.getProdClass());
				writer.append("\n");
				System.out.println("<API Name=\"createInventoryActivity\"><Input> <InventoryActivity OrganizationCode=\"HDUS\"  " +
						"ItemID=\""+dto.getItem()+"\" " +
						"Node=\""+dto.getShipNode()+"\" " +
						"UnitOfMeasure=\""+dto.getUom()+"\" " +
						"ProductClass=\""+dto.getProdClass()+"\"/>  " +
						"</Input></API>");
			}

			writer.flush();
			writer.close();
			System.out.println("File created at " + fileLocation);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	public static List<GetItemNodeQtyDTO> getItemsWithZeroQtyFromSterling()
			throws SQLException, ClassNotFoundException {
		
		List<GetItemNodeQtyDTO> itemNodeQtyList = new ArrayList<GetItemNodeQtyDTO>();
		
		boolean compare = false;
		int comAdjQty = 0;
		final String driverClass = "oracle.jdbc.driver.OracleDriver";
		final String connectionURL = "jdbc:oracle:thin:@//spragor10-scan.homedepot.com:1521/dpr77mm_sro01";
		final String uName = "MMUSR01";
		final String uPassword = "COMS_MMUSR01";
		final String query = "SELECT /*+ PARALLEL*/ INVENTORY_ITEM_KEY, ITEM, SHIP_NODE, QUANTITY, UOM, PRODUCT_CLASS from thd01.yfs_inventory_audit where on_hand_qty=0 " +
				"and modifyts > sysdate-30 " +
				"and segment = ' ' and transaction_type ='SHIPMENT' and ship_node like 'MVNDR%'";
		
		try {
			Connection con = null;
			con = DriverManager.getConnection(connectionURL, uName, uPassword);
			
			System.out.println("Connected to Sterling DB...");
			Statement stmt = null;
			stmt = con.createStatement();
			ResultSet result = null;
			System.out.println("Running the query to get the adjustments with zero qty ... : "+query);
			
			result = stmt.executeQuery(query);
			GetItemNodeQtyDTO dto = null;
			while (result.next()) {
				dto = new GetItemNodeQtyDTO();
				dto.setInvItemKey(result.getString(1).trim());
				dto.setItem(result.getString(2).trim());
				dto.setShipNode(result.getString(3).trim());
				dto.setQty(Math.abs(Integer.parseInt(result.getString(4).trim())));
				dto.setUom(result.getString(5).trim());
				dto.setProdClass(result.getString(6).trim());

				itemNodeQtyList.add(dto);
			}
			stmt.close();
			result.close();
			con.close();
			
			System.out.println(itemNodeQtyList.size()+" records fetched from Sterling DB");
			
			// writer.append("Sterling Connection closed");
			// writer.append("Program End Time:"+dateFormat.format(date));
		} catch (SQLException e1) {
			e1.printStackTrace();
			System.out.println(e1.getMessage());
		}
		return itemNodeQtyList;
	}
}
