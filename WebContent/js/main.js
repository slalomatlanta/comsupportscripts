// 1. Handle Service Failure Scenario - DONE
// 2. Handle Page Refresh - DONE


var mockDataForThisTest = "json=" + encodeURI(JSON.stringify());

var app = angular.module('myApp', []);

app.controller('BatchCtrl',function($scope, $http, $interval) {
    $scope.startTime = $scope.endTime = "Not Started";
    $scope.batchStarted = "";
    $scope.startTimeCSS="alert-warning";
    $scope.endTimeCSS="alert-warning";
    $scope.serviceError = "hide";
    $scope.BatchStarted = true;
    var timer;
   
    $scope.startBatch = function(startbatch) {
     if (angular.isDefined(timer) ) return; 
      $scope.serviceData(startbatch);
       timer=$interval(function(){
            if ($scope.BatchStarted || (angular.isDefined(startbatch) && startbatch)) {
              $scope.serviceData(startbatch)
            } else {
              $scope.stopFight();
            }
          },30000);            
    };   

    $scope.stopFight = function(){
      if (angular.isDefined(timer)) {
            $interval.cancel(timer);
            timer = undefined;
      }
    };

    $scope.serviceData = function(triggerBatch){
         $scope.batchData = [];
         var httpRequest = $http({
                method: 'GET',               
                url: '/COMSupportScripts/LoadSourcingRulesServlet?triggerBatch='+triggerBatch,
                data: mockDataForThisTest
                //Send Trigger batch data in the data after integration

         }).success(function(data, status) {
               $scope.BatchStarted = data.BatchStarted;
               $scope.serviceError = "hide";
                if(data.BatchStarted){
                   _.each(data.jobDetails,function(obj){ 
                        obj.jobStartTime = _.isEqual("na",angular.lowercase(obj.jobStartTime))? obj.jobStartTime:moment(obj.jobStartTime).format('MMMM Do YYYY, h:mm:ss a');
                        obj.jobEndTime = _.isEqual("na",angular.lowercase(obj.jobEndTime))? obj.jobEndTime :moment(obj.jobEndTime).format('MMMM Do YYYY, h:mm:ss a');
                        return;                        
                   });
                   $scope.batchData = data.jobDetails;
                   $scope.batchStarted = "disabled";

                   $scope.startTime = data.jobDetails[0].jobStartTime;
                   $scope.startTimeCSS="alert-success";
                   
                   if ($scope.checkCompleted()){
                        $scope.endTime = " Completed " + data.jobDetails[5].jobEndTime;
                        $scope.endTimeCSS="alert-success";
                        $scope.BatchStarted = false;
                   } else if(_.filter($scope.batchData, function(job){ return (_.contains(["failed","failure","error"],angular.lowercase(job.jobStatus))); }).length > 0){
                    $scope.endTimeCSS="alert-danger";
                    $scope.endTime =  " " + _.filter($scope.batchData, function(job){ return (_.contains(["failed","failure","error"],angular.lowercase(job.jobStatus))); }).length + " Job Failed ";

                   }else{
                        $scope.endTime = moment().calendar() + " -  Running." ;
                   }                  
                }else{
                	timer = undefined;
                }             

         }).error(function(data, status) {
           $scope.serviceError = "";
         });                 
    };

    $scope.checkCompleted = function(){
       var allJobDone =  _.filter($scope.batchData, function(job){ return (angular.lowercase(job.jobEndTime) !="na" && _.contains(["completed","done","success"],angular.lowercase(job.jobStatus)) && _.contains(["completed","failed","error","done","success"],angular.lowercase(job.validationStatus))); });
       if(angular.isDefined(allJobDone)){
        return (allJobDone.length == 6);
       }else{
         return false;
        }
    };

    $scope.download = function(resource){
    window.open(resource);
}
});