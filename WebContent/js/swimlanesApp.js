'use strict';

var app = angular.module('app', []);
app.controller('controller', ['$scope', '$http', function($scope, $http) {
	$scope.version = 1.9;
	$scope.projectNum = 1719;
	$scope.sprints = [''];
	$scope.laneTypes = ['unstarted', 'started', 'finished', 'delivered', 'rejected', 'accepted'];
	
	$scope.init = function() {
		var labelsUrl = "https://tracker.run.homedepot.com/services/v5/projects/" + $scope.projectNum + "/labels";
		var ownersUrl = "https://tracker.run.homedepot.com/services/v5/projects/" + $scope.projectNum + "?fields=memberships";
		
		$http.get(labelsUrl).then(function(response) {
			for (var i = 0; i < response.data.length; i++) {
				$scope.sprints.push(response.data[i].name);
			}
		});
		
		$http.get(ownersUrl).then(function(response) {
			var list = [];
	
			for (var i = 0; i < response.data.memberships.length; i++) {
				list.push({'id' : response.data.memberships[i].person.id, 
					'username' : response.data.memberships[i].person.username, 
					'initials' : response.data.memberships[i].person.initials});
			}
			
			$scope.members = list;
		});
	}();
	
	$scope.populateSwimLanes = function(userName, id) {
		var user = userFactory(userName);
		
		$scope.searchedUserId = id;
		
		$http.get(user.stories).then(function(response) {
			var stories = document.getElementById('All').checked ? response.data : response.data.stories.stories;
			
			scroll(0,0);
			setupLanes();
			stories.sort(storySorter);

			for (var i = 0; i < stories.length; i++) {
				normalizeState(stories[i]);
				
				if(user.decisionStrategy(stories[i])) {
					populateLane(stories[i]);
				}
			}
			
			$scope.navBarHeight = document.querySelector('#stickyTag').offsetHeight;
		});
	};
	
	$scope.findOwner = function(owner_id) {
		for (var i = 0; i < $scope.members.length; i++) {
			if ($scope.members[i].id === owner_id) {
				return $scope.members[i].initials;
			}
		}
	};
	
	function userFactory(userName) {
		var user = {
			'Unassigned': {
				'stories': "https://tracker.run.homedepot.com/services/v5/projects/" +
					$scope.projectNum + "/search?query=owner%3A%20%22%22",
				'decisionStrategy': unassignedDecision
			},
			'All': {
				'stories': "https://tracker.run.homedepot.com/services/v5/projects/" +
					$scope.projectNum + "/stories?limit=10000",
				'decisionStrategy': unassignedDecision
			},
			'default': {
				'stories': "https://tracker.run.homedepot.com/services/v5/projects/" +
					$scope.projectNum + "/search?query=%22" + userName + "%22",
				'decisionStrategy': memberDecision
			}
		};
		
		return user[(userName !== 'Unassigned' && userName !== 'All') ? 'default' : userName];
	};

	function unassignedDecision(story) {
		return $scope.swimlanes.hasOwnProperty(story.current_state);
	};
	
	function memberDecision(story) {
		return (unassignedDecision(story) && story.owner_ids.indexOf($scope.searchedUserId) !== -1);
	};
	
	function setupLanes() {
		$scope.swimlanes = {};

		for (var i = 0; i < $scope.laneTypes.length; i++) {
			$scope.swimlanes[$scope.laneTypes[i]] = [];
		}
	};
	
	function storySorter(a, b) {
		var aName = $scope.findOwner(a.owner_ids[0]);
		var bName = $scope.findOwner(b.owner_ids[0]);

		return (aName > bName) ? 1 : ((bName > aName) ? -1 : 0);
	}
	
	function normalizeState(story) {
		if (story.current_state === 'planned' || story.current_state === 'unscheduled') {
			story.current_state = 'unstarted';
		}
	};
	
	function populateLane(story) {
		var labels = [];
		
		if (story.labels.length < 1) {
			labels.push('');
		} else {
			for (var i = 0; i < story.labels.length; i++) {
				labels.push(story.labels[i].name);
			}
		}

		story['labelList'] = labels;

		$scope.swimlanes[story.current_state].push(story);
	};
}]);

app.directive('scroll', function ($window) {
	return function(scope, element, attrs) {
		angular.element($window).bind('scroll', function() {
			var navBar = document.querySelector('#stickyTag');
			var origOffsetY = navBar.offsetTop + 129;
			
			if (window.scrollY >= origOffsetY) {
				navBar.classList.add('sticky');
			} else {
				navBar.classList.remove('sticky');
			}
			
			scope.$apply();
		});
	};
});

app.directive('resize', function ($window) {
	return function(scope, element, attrs) {
		angular.element($window).bind('resize', function() {
			var navBar = document.querySelector('#stickyTag');

			scope.navBarHeight = navBar.offsetHeight;
			
			scope.$apply();
		});
	};
});