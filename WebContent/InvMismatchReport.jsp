<html>
<head>
<meta name="viewport" content="width=device-width, maximum-scale=1.0" />
<meta http-equiv="Content-Type"
	content="text/html; charset=iso-8859-1; charset=windows-1252" />
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" />
<title>THD COM Multichannel Support Development</title>
<link type="text/css" href="css/THDstyle.css" rel="stylesheet" />
<link type="text/css" href="css/swimlanesMain.css" rel="stylesheet" />
<link rel="shortcut icon" href="images/THDlogo.ico" />
<script
	src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
<script src="js/swimlanesApp.js"></script>
<script type="text/javascript" src="js/lib/jquery-1.12.3.min.js"></script>
</head>
<body id="thdBody" ng-app="app" scroll resize ng-controller="controller">
	<div id="thdWebApp" class="ieGrad">
		<table id="header" border="0" width="100%">
			<tr>
				<td valign="top" nowrap="nowrap" class="brandBox">
					<table border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td><img src="images/THD_logo.png" alt="Go Home"
								class="logo" width="57" height="57" /></td>
							<td nowrap="nowrap" style="vertical-align: center"><span
								style="position: relative; top: 5px"> <span id="appName">Inventory Mismatch Report</span> <br> <span id="appTagline">Multichannel Support</span>
							</span></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<div id="messageBar" style="width: 100%">The Home Depot</div>
		<div id="appBody">
			<div class="stickyTag-wrapper" style="height: {{navBarHeight">
				<div class="stickyTag" id="stickyTag">
					<div id="mainDiv">

						<center>
							<h3>Inventory Mismatch Report</h3>
							<br />
							<textarea
								id="storeNumbers" cols="40" rows="7" autofocus="autofocus"
								title="Please provide comma seperated list of store numbers. (Ex: 0122,0143,8584)"></textarea>
							<br /> <input type=submit value="Submit" align="center"
								id="submit" /> <br /> <b><i>Note: Processing may take
									several minutes based on number of stores.</i></b>
					</div>
					<br>
					<hr>
				</div>
				
				<div id="downloadDiv">
				</div>
			</div>
		</div>
		<div id="copyright">Copyright � 2016 The Home Depot - Customer
			Order Management Multichannel Support</div>
	</div>

<script>
    var OrderList;
    jQuery("#submit").click(function() {
                       StoreList = jQuery("textarea#storeNumbers").val();
                       $("#downloadDiv").empty();
                       
                       $.ajax({
							    type: "GET",
							    async: false,
								url: "http://cpliisad.homedepot.com:8080/COMSupportScripts/InventoryReport",
// 								url: "http://localhost:8085/COMSupportScripts/InventoryReport",
							    data: {st : StoreList },
							    success: function (data) {
							    	$("#downloadDiv").append('<center><a href="http://cpliisad.homedepot.com:8080/COMSupportScripts/InventoryReportDownload">Download Mismatch Report</a><br/>');
// 							    	$("#downloadDiv").append('<center><a href="http://localhost:8085/COMSupportScripts/InventoryReportDownload">Download Mismatch Report</a><br/>');
							    }
							});
    			}
			);
</script>
</body>
</html>