// 1. Handle Service Failure Scenario - DONE
// 2. Handle Page Refresh - DONE


var mockDataForThisTest = "json=" + encodeURI(JSON.stringify());

var app = angular.module('myApp', ['ngWebSocket']);

app.controller('InvCtrl',function($scope, $websocket,$http) {
      var dataStream = $websocket('ws://localhost:8085/InventoryReport/invendpoint');

      var collection = {};
      $scope.liveUpdates = {};

      dataStream.onMessage(function(message) {
        $scope.liveUpdates =  JSON.parse(message.data);        
      });

      $scope.storelist = [];
      // $scope.storelist = ['9301','9302','9303','9304'];
      // $scope.storelist = ['1001', '1706', '1710', '3033', '6310'];
      
      $scope.removeStore = function(id){
        $scope.storelist = _.without($scope.storelist, id);
      };

      $scope.addStore = function(id){
        $scope.storelist.push(id);
      };       
    

    $scope.startCompare = function(){
          var str = '';
         _.each($scope.storelist, function(st){ str = str + st +',';})
         str.substring(0, str.length - 1);
         var httpRequest = $http({
                method: 'GET',               
                url: '/InventoryReport/inv?st='+str,
                data: mockDataForThisTest
                //Send Trigger batch data in the data after integration

         }).success(function(data, status) {
               

         }).error(function(data, status) {
           $scope.serviceError = "";
         });                 
    }; 
    
    $scope.downloadFile = function(){
    	    	window.open("/InventoryReport/excelDownload", "_blank");
    }
    
    // $scope.download = function(resource){
    //     window.open(resource);
    // }
});